var db = openDatabase('timetrack', '1.1', 'Time track database', 2 * 1024 * 1024);

// ID - unique autoincrement IDentificator of task
// project_title - project name or caption of the project
// task_title - name of taks or caption of task
// task_url - url of taks or caption of task
// time - keep cumulative time from begining to STOP press
// start - we need some guIDe for calculate time increase
// running - task is in progress now

function createTable() {
    db.transaction(function (tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS tasks(id INTEGER PRIMARY KEY, task_title TEXT, task_url TEXT, time INTEGER, start DATETIME, running BOOLEAN)', [], null, onError); // table creation
    });

}
//// 1.0 => 2.0
if (db.version == '1.0') {
    db.changeVersion(db.version, '1.1', function (tx) {
        tx.executeSql("ALTER TABLE tasks REMOVE project_title TEXT AFTER id");
    });
}

/**
 * Delete all records (drop table)
 */
function dropTaskTable() {
    db.transaction(function (tx) {
        tx.executeSql("DROP TABLE tasks", [], function (tx, results) {
            alert('Table tasks was droped');
        }, onError);
    });
}
// dropTaskTable();

/**
 * Exception hook
 */
function onError(tx, error) {
    console.log(tx);
    console.log(error);
    alert(error.message);
}

var current_tab;
document.addEventListener('DOMContentLoaded', function () {
    chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
        current_tab = tabs[0];
    });

});

function getTabUrl() {
    return current_tab.url;
}
function contains(value, searchFor)
{
    return (value || '').indexOf(searchFor) > -1;
}

function copyToClipboard(taskTimer) {


    var aux = document.createElement("input");
    aux.setAttribute("value", taskTimer.text());
    document.body.appendChild(aux);
    aux.select();
    document.execCommand("copy");

    document.body.removeChild(aux);

}
$(function () {
    console.log(' ent');
    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });
});
var tasks = {
    insert: function (task_title, task_url, task_start, task_runing) {
        db.transaction(function (tx) {
            tx.executeSql("INSERT INTO tasks ( task_title, task_url, time, start, running) VALUES ( ?, ?, ?, ?, ?)", [task_title, task_url, 0, task_start, task_runing],
                    function (tx, result) {
                        taskInterface.index();
                    },
                    onError);
        });
    },
    update: function () {

    },
    remove: function (ID) {
        db.transaction(function (tx) {
            tx.executeSql("DELETE FROM tasks WHERE id=?", [ID],
                    function (tx, result) {
                        window.clearInterval(taskInterface.intervals[ID]);
                        taskInterface.index();
                    },
                    onError);
        });
    },
    removeall: function () {
        db.transaction(function (tx) {
            tx.executeSql("DELETE FROM tasks", [], function (tx, results) {

                for (idd in taskInterface.intervals) {
                    window.clearInterval(taskInterface.intervals[idd]);
                }

                taskInterface.index();
            }, onError);
        });
    }


}


/**
 * Time tracking user interface Javascript
 */
var taskInterface = {
    intervals: new Array,
    bind: function () {
//         $('[data-toggle="tooltip"]').tooltip(function(){
//             console.log(' tool');
//         });
        /* common elements
         ------------------------------------------------------------------------ */

        // cancel buttons click
        $(".form").on("click", ".button-cancel", function (e) {
            e.preventDefault();
            $(this).closest(".form").hide();
            $("#task-list").show();
        });

        /* create task
         ------------------------------------------------------------------------ */

        // create new task
        $(".button-new").on("click", function (e) {
            $(".form").hide();
           
                $("#form-create").slideDown().find("input[name='task-title']").focus();
            
        });

        // create new task > confirm click
        $("#button-create").on("click", function (e) {
            e.preventDefault();
            var start, running;

            if ($("#form-create :input[name='start_now']").is(":checked")) {
                start = new Date();
                running = 1;
            } else {
                start = new Date();
                running = 0;
            }

            tasks.insert($("#form-create :input[name='task-title']").val(), $("#form-create :input[name='task-url']").val(), start, running);
            $("#form-create").hide().find("input:text").val("");
        });

        // create new task > enter press
        $('#task-title').keydown(function (e) {
            var start, running;

            if ($("#form-create :input[name='start_now']").is(":checked")) {
                start = new Date();
                running = 1;
            } else {
                start = new Date();
                running = 0;
            }
            if (e.keyCode === 13) {
                tasks.insert($("#form-create :input[name='task-title']").val(), $("#form-create :input[name='task-url']").val(), start, running);
                $("#form-create").hide().find("input:text").val("");
            }
        });

        /* control task
         ------------------------------------------------------------------------ */
//        console.log('bind play');
        // Play  task
        $("#task-list").on("click", ".play", function (e) {
//            console.log('clickou');
//            console.log($(this).data("id"));
            e.preventDefault();
            taskInterface.toggleTimer($(this).data("id"));
        });

        /* delete one task
         ------------------------------------------------------------------------ */

        // delete task
        $("#task-list").on("click", ".remove", function (e) {
            e.preventDefault();
            $(".form").hide();
            $("#button-remove").data("id", $(this).data("id"));
            $("#remove-confirm").html("Are you sure? You want to <strong>delete " + $(this).data("task-title") + "</strong>?");
            $("#form-remove").show();
        });

        // delete task > confirm deletion
        $("#button-remove").on("click", function () {
            $("#form-remove").hide();
            tasks.remove($(this).data("id"));
        });

        /* delete all tasks
         ------------------------------------------------------------------------ */

        // remove all tasks
        $(".remove-all").on("click", function (e) {
            e.preventDefault();
            $(".form").hide();
            $("#form-remove-all").slideDown();
        });

        // remove all tasks > confirm deletion
        $("#button-remove-all").on("click", function () {
            $("#form-remove-all").hide();
            tasks.removeall();
        });

        /* export all tasks
         ------------------------------------------------------------------------ */

        // export all tasks
        $(".export-all").on("click", function (e) {
            db.transaction(function (tx) {
                tx.executeSql('SELECT * FROM tasks ORDER BY ID DESC', [], function (tx, results) {
                    var out = '';
                    var len = results.rows.length, i;
                    out += 'ID,Title,Task URL,Time,Started on' + '\n';
                    if (len > 0) {
                        for (i = 0; i < len; i++) {
                            var task = results.rows.item(i);
                            if (task.running === 1) {
                                var start = new Date(task.start);
                                var dif = Number(task.time) + Math.floor((new Date().getTime() - start.getTime()) / 1000);
                                out += task.id + ',' + task.task_title + ',' + task.task_url + ',' + taskInterface.hms(dif);
                            } else {
                                out += task.id + ',' + task.task_title + ',' + task.task_url + ',' + taskInterface.hms(task.time);
                            }
                            var start = new Date(task.start);
                            out += ',' + start.getDate() + '-' + (parseInt(start.getMonth()) + 1).toString() + '-' + start.getFullYear() + ' ' + start.getHours() + ':' + start.getMinutes() + ':' + start.getSeconds() + '\n';
                        }
                    } else {
                        out = "No tasks";
                    }
                    console.log(out);
                    var link = document.createElement("a");
                    link.download = 'SimpleTimeTrack - ' + new Date(Date.now()).toLocaleString() + '.csv';
                    link.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(out);
                    link.click();

                }, null);
            });
        });


        /* update task name
         ------------------------------------------------------------------------ */

        // update task

        $("#task-list").on("click", ".update", function (e) {
            e.preventDefault();
            $("#task-list").hide();
//             $("#form-update").show();

            var ID = $(this).data("id");
            // TODO load function
            db.transaction(function (tx) {
                tx.executeSql('SELECT * FROM tasks WHERE id = ?', [ID], function (tx, results) {

                    if (results.rows.length > 0) {
                        $("#form-update :input[name='task-id']").val(ID);
                        $("#form-update :input[name='task-title']").val(results.rows.item(0).task_title);
                        $("#form-update :input[name='task-url']").val(results.rows.item(0).task_url);
                        $("#form-update :input[name='task-time']").val(taskInterface.hms(results.rows.item(0).time));
                        $("#form-update").slideDown();
                    } else {
                        alert("Task " + ID + "not found!");
                    }
                }, null);
            });
        });

        // update task > save
        $("#button-update").on("click", function (e) {
            e.preventDefault();
            $("#form-update").hide();

            var ID = $("#form-update :input[name='task-id']").val(); // get ID
            var task_title = $("#form-update :input[name='task-title']").val(); // get name
            var task_url = $("#form-update :input[name='task-url']").val(); // get name
//            var time = $("#form-update :input[name='task-time']").val(); // get task time

            db.transaction(function (tx) {
                tx.executeSql("UPDATE tasks SET task_title = ?, task_url = ? WHERE id = ?", [task_title, task_url, ID], function (tx, results) {
                    console.log(tx);
                    console.log(results);
                    taskInterface.index();
                }, onError);
            });
        });

        /* reset task
         ------------------------------------------------------------------------ */

        $("#task-list").on("click", ".reset", function (e) {
            e.preventDefault();
            $(".form").hide();

            var ID = $(this).data("id");

            db.transaction(function (tx) {
                tx.executeSql("UPDATE tasks SET time = ? WHERE id = ?", [0, ID], function (tx, results) {
                    taskInterface.index();
                }, onError);
            });

        });


        $("#task-list").on("dblclick", ".timer", function (event) {
            copyToClipboard($(this));
        });





    },
    index: function () {
        var out = "";

        db.transaction(function (tx) {
            tx.executeSql('SELECT * FROM tasks ORDER BY id DESC', [], function (tx, results) {

                var len = results.rows.length, i;

                if (len > 0) {
                    for (i = 0; i < len; i++) {
                        var task = results.rows.item(i);

                        out += '<div class="list-group-item list-group-item-action flex-column align-items-start' + (task.running === 1 ? ' running' : '') + '" id="item' + task.id + '" data-id="' + task.id + '">';
                        out += '<div class="d-flex w-100 justify-content-between">';
                        out += '<h5 class="mb-1">' +
                                '<a class="task-title-url" href="' + (task.task_url != "" ? task.task_url + '" target="_blank"' : '#"') + '>' +
                                (task.task_title !== "" ? task.task_title : "Task:" + task.id) +
                                '</a>' +
                                '</h5>';
                        out += '<small class="timer" data-toggle="tooltip" data-placement="top" data-html="true" data-original-title="double click<br>to copy">';
                        if (task.running === 1) {
                            var start = new Date(task.start);
                            var dif = Number(task.time) + Math.floor((new Date().getTime() - start.getTime()) / 1000);
                            out += taskInterface.hms(dif);
                        } else {
                            out += taskInterface.hms(task.time);
                        }
                        out += '</small>';
                        out += '</div>';

                        out += '<div class="list-actions">';
//                        out += '<h5 class="mb-1"><a href="' + task.task_url + '" target="_blank">' + (task.task_title !== "" ? task.task_title : "Task:" + task.id) + '</a><br/><small>' + task.project_title + '</small></h5>';
                        out += '<a href="#" class="badge badge-outlined update" data-id="' + task.id + '" title="Edit">Edit</a> | ';
                        out += '<a href="#" class="badge badge-outlined reset" data-id="' + task.id + '" title="Reset" >Reset</a> | ';
                        out += '<a href="#" class="badge badge-outlined remove" data-id="' + task.id + '" data-task-title="' + task.task_title + '" title="Delete">Delete</a>';
                        out += '</div>';
//                        out += '<div class="btn-group btn-group-sm" role="group">';
//                        out += '<button type="button" class="btn btn-primary update" data-id="' + task.id + '">Edit</button>';
//                        out += '<button type="button" class="btn btn-primary reset" data-id="' + task.id + '">Reset</button>';
//                        out += '<button type="button" class="btn btn-primary remove" data-id="' + task.id + '">Delete</button>';
//                        out += '</div>';



                        out += '<button type="button" class="btn btn-outline-primary float-right play power"' + (task.running === 1 ? ' running' : '') + '" title="Timer on/off" data-id="' + task.id + '">';
                        out += '<span class="oi ' + (task.running === 1 ? 'oi-media-pause' : 'oi-media-play') + '"></span>';
                        out += '</button>';
//                        out += '<a href="#" class="play power' + (task.running === 1 ? ' running' : '') + '" title="Timer on/off" data-id="' + task.id + '"></a>';
                        out += '</div>';

                        if (task.running === 1) {
                            taskInterface.startTask(task); // start task
                        }
                    }
                } else {
                    out = "<div class=\"no-task\">";
                    out += "<p class=\"text-center\">Great! No tasks for you.</p>";
                    out += "<p class=\"text-center\">Grab a coffee.</p>";
                    out += "<p class=\"text-center\"><img src=\"img/if_Coffee_131901.png\" alt=\"\"/></p>";
                    out += "</div>";
                }

                $("#task-list").empty().append(out).show();

            }, null);
        });
    },
    init: function () {
        createTable();
        this.bind();
        this.index();
        this.toggleRunText();
    },
    toggleTimer: function (ID) {
//        console.log(ID);
        db.transaction(function (tx) {
            tx.executeSql('SELECT * FROM tasks WHERE id = ?', [ID], function (tx, results) {
                if (results.rows.length > 0) {
                    var task = results.rows.item(0);
                    $('#item' + ID).toggleClass('running');
                    $('#item' + ID + ' .power').toggleClass('running');
                    $('#item' + ID + ' .oi-media-play, #item' + ID + ' .oi-media-pause').toggleClass('oi-media-pause oi-media-play');

                    if (task.running === 1) {
                        taskInterface.stopTask(task);
//                        $('#item' + ID).toggleClass('list-group-item-warning');
                    } else {
                        taskInterface.startTask(task);
//                        $('#item' + ID).toggleClass('list-group-item-success');
                    }

                    taskInterface.toggleRunText();

                } else {
                    alert("Task " + ID + " not found sorry!");
                }
            }, null);
        });
    },
    //////////////////////////////////////////////////////////////////////////////
    // start task
    //////////////////////////////////////////////////////////////////////////////

    startTask: function (task) {
//         console.log('start');
        window.clearInterval(taskInterface.intervals[task.id]); // remove timer

        var start = new Date(); // set start to NOW

        if (task.running === 1) {
//             console.log('running');
            start = new Date(task.start);
        } else {
            db.transaction(function (tx) {
                tx.executeSql("UPDATE tasks SET running = ?, start = ? WHERE id = ?", [1, start, task.id], null, onError);
            });
        }

        // setup interval for counter
        taskInterface.intervals[task.id] = window.setInterval(function () {
            var dif = Number(task.time) + Math.floor((new Date().getTime() - start.getTime()) / 1000);
            $('#item' + task.id + ' .timer').text(taskInterface.hms(dif));
        }, 500);
    },
    //////////////////////////////////////////////////////////////////////////////
    // stop task
    //////////////////////////////////////////////////////////////////////////////

    stopTask: function (task) {
//        console.log('stop');
        window.clearInterval(taskInterface.intervals[task.id]); // remove timer

        var start, stop, dif = 0;

        db.transaction(function (tx) {
            tx.executeSql('SELECT * FROM tasks WHERE id = ?', [task.id], function (tx, results) {
                if (results.rows.length > 0) {
                    start = new Date(results.rows.item(0).start); // read from DB
                    stop = new Date(); // now
                    dif = Number(results.rows.item(0).time) + Math.floor((stop.getTime() - start.getTime()) / 1000); // time diff in seconds

                    $('#item' + task.id + ' .timer').text(taskInterface.hms(dif));

                } else {
                    alert('Task ' + task.id + ' not found!');
                }
            }, null, onError);
        });

        // update record
        db.transaction(function (tx) {
            tx.executeSql("UPDATE tasks SET running = ?, time = ? WHERE id = ?", [0, Number(dif), task.id], null, onError);
        });

    },
    //////////////////////////////////////////////////////////////////////////////
    // toggle RUN text on icon
    //////////////////////////////////////////////////////////////////////////////

    toggleRunText: function () {
        db.transaction(function (tx) {
            tx.executeSql('SELECT * FROM tasks WHERE running = ?', [1], function (tx, results) {
                if (results.rows.length > 0) {
                    chrome.browserAction.setBadgeText({
                        text: results.rows.length.toString(),
                    });
                } else {
                    chrome.browserAction.setBadgeText({
                        text: ''
                    });
                }
            }, null, onError);
        });
    },
    //////////////////////////////////////////////////////////////////////////////
    // convert sec to hms
    //////////////////////////////////////////////////////////////////////////////

    hms: function (secs) {
        //secs = secs % 86400; // fix 24:00:00 overlay
        var time = [0, 0, secs], i;
        for (i = 2; i > 0; i--) {
            time[i - 1] = Math.floor(time[i] / 60);
            time[i] = time[i] % 60;
            if (time[i] < 10) {
                time[i] = '0' + time[i];
            }
        }
        return time.join(':');
    },
    //////////////////////////////////////////////////////////////////////////////
    // convert h:m:s to sec
    //////////////////////////////////////////////////////////////////////////////

    sec: function (hms) {
        var t = String(hms).split(":");
        return Number(parseFloat(t[0] * 3600) + parseFloat(t[1]) * 60 + parseFloat(t[2]));
    },
    nextID: function () {
        var id = localStorage['lastID']; // get last ID from local storage
        if (id === undefined) {
            id = 1; // generate first ID
        } else {
            id++; // generate next ID
        }
        localStorage['lastID'] = id; // save to localStorage
        return id;
    },
    getCurrentTabUrl: function () {
        chrome.tabs.query({currentWindow: true, active: true}, function (tabs) {
//            console.log(tabs[0].url);
            return tabs[0].url;
        });
    }

};

