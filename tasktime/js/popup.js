var task1 = {
    // As we've seen, we can easily define functionality for
    // this object literal..
    getInfo: function () {
        //...
    },
    // but we can also populate it to support 
    // further object namespaces containing anything
    // anything we wish:
    models: {
        list: function () {
            db.transaction(function (tx) {
                tx.executeSql('SELECT * FROM tasks ORDER BY ID DESC', [],
                        function (tx, results) {
                            return results.rows;
                        },
                        onError);
            });

        },
        insert: function (ID, project_title, task_title, task_url) {
            db.transaction(function (tx) {
                tx.executeSql("INSERT INTO tasks ( project_title, task_title, task_url, time, start, running) VALUES ( ?, ?, ?, ?, ?, ?)", [project_title, task_title, task_url, 0, new Date(), false],
                        function (tx, result) {
                            taskInterface.index();
                        },
                        onError);
            });
        },
        update: function () {

        },
        remove: function (ID) {
            db.transaction(function (tx) {
                tx.executeSql("DELETE FROM tasks WHERE id=?", [ID],
                        function (tx, result) {
                            window.clearInterval(taskInterface.intervals[ID]);
                            taskInterface.index();
                        },
                        onError);
            });
        },
        removeall: function () {
            db.transaction(function (tx) {
                tx.executeSql("DELETE FROM tasks", [], function (tx, results) {

                    for (idd in taskInterface.intervals) {
                        window.clearInterval(taskInterface.intervals[idd]);
                    }

                    taskInterface.index();
                }, onError);
            });
        }
    },
    views: {
        intervals: new Array,
        bind: function () {
            console.log('bingou');
            /* common elements
             ------------------------------------------------------------------------ */

            // cancel buttons click
            $(".cancel").on("click", function (e) {
                e.preventDefault();
                $("#" + $(this).data("id")).hide().hide().find("input:text").val("");
                $("#form-list").show();
            });

            /* create task
             ------------------------------------------------------------------------ */

            // create new task
            $(".create").on("click", function (e) {
                console.log('create but')

                $(".form").hide();
                $("#form-create").slideDown().find("input[name='task-project-title']").val(current_tab.title).focus();
                $("#form-create :input[name='task-url']").val(current_tab.url);
//            console.log(taskInterface.getCurrentTabUrl());

            });

            // create new task > confirm click
            $("#button-create").on("click", function () {
                tasks.insert(taskInterface.nextID(), $("#form-create :input[name='task-project-title']").val(), $("#form-create :input[name='task-title']").val(), $("#form-create :input[name='task-url']").val());
                $("#form-create").hide().find("input:text").val("");
            });

            // create new task > enter press
            $('#task-title').keydown(function (e) {
                if (e.keyCode == 13) {
                    tasks.insert(taskInterface.nextID(), $("#form-create :input[name='task-project-title']").val(), $("#form-create :input[name='task-title']").val(), $("#form-create :input[name='task-url']").val());
                    $("#form-create").hide().find("input:text").val("");
                }
            });

            /* control task
             ------------------------------------------------------------------------ */

            // Play  task
            $(".play").on("click", function (e) {
                console.log('clickou');
                console.log($(this).data("id"));
                e.preventDefault();
                taskInterface.toggleTimer($(this).data("id"));
            })

            /* delete one task
             ------------------------------------------------------------------------ */

            // delete task
            $(".remove").on("click", function (e) {
                e.preventDefault();
                $(".form").hide();
                $("#button-remove").data("id", $(this).data("id"));
                $("#remove-confirm").html("Are you sure? You want to <strong>delete " + $(this).attr("title") + "</strong>?");
                $("#form-remove").show();
            });

            // delete task > confirm deletion
            $("#button-remove").on("click", function () {
                $("#form-remove").hide();
                tasks.remove($(this).data("id"));
            });

            /* delete all tasks
             ------------------------------------------------------------------------ */

            // remove all tasks
            $(".remove-all").on("click", function (e) {
                e.preventDefault();
                $(".form").hide();
                $("#form-remove-all").slideDown();
            });

            // remove all tasks > confirm deletion
            $("#button-remove-all").on("click", function () {
                $("#form-remove-all").hide();
                tasks.removeall()
            });

            /* export all tasks
             ------------------------------------------------------------------------ */

            // export all tasks
            $(".export-all").on("click", function (e) {
                db.transaction(function (tx) {
                    tx.executeSql('SELECT * FROM tasks ORDER BY ID DESC', [], function (tx, results) {
                        var out = '';
                        var len = results.rows.length, i;

                        if (len > 0) {
                            for (i = 0; i < len; i++) {
                                var task = results.rows.item(i);
                                if (task.running == true) {
                                    var start = new Date(task.start);
                                    var dif = Number(task.time) + Math.floor((new Date().getTime() - start.getTime()) / 1000)
                                    out += task.id + ',' + task.project_title + ',' + task.name + ',' + taskInterface.hms(dif);
                                } else {
                                    out += task.id + ',' + task.project_title + ',' + task.name + ',' + taskInterface.hms(task.time);
                                }
                                var start = new Date(task.start);
                                out += ',' + start.getFullYear() + '-' + (parseInt(start.getMonth()) + 1).toString() + '-' + start.getDate() + '\n';
                            }
                        } else {
                            out = "No tasks"
                        }

                        var link = document.createElement("a");
                        link.download = 'SimpleTimeTrack - ' + new Date(Date.now()).toLocaleString() + '.csv';
                        link.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(out);
                        link.click();

                    }, null);
                });
            });


            /* update task name
             ------------------------------------------------------------------------ */

            // update task

            $(".update").on("click", function (e) {
                e.preventDefault();
                $(".form").hide();

                var ID = $(this).data("id");
                // TODO load function
                db.transaction(function (tx) {
                    tx.executeSql('SELECT * FROM tasks WHERE id = ?', [ID], function (tx, results) {

                        if (results.rows.length > 0) {
                            $("#form-update :input[name='task-id']").val(ID);
                            $("#form-update :input[name='task-project-title']").val(results.rows.item(0).project_title);
                            $("#form-update :input[name='task-title']").val(results.rows.item(0).task_title);
                            $("#form-update :input[name='task-url']").val(results.rows.item(0).task_url);
                            $("#form-update :input[name='task-time']").val(taskInterface.hms(results.rows.item(0).time));
                            $("#form-update").slideDown();
                        } else {
                            alert("Task " + ID + "not found!");
                        }
                    }, null);
                });
            });

            // update task > save
            $("#button-update").on("click", function () {
                $("#form-update").hide();

                var ID = $("#form-update :input[name='task-id']").val(); // get ID
                var project_title = $("#form-update :input[name='task-project-title']").val(); // get name
                var task_title = $("#form-update :input[name='task-title']").val(); // get name
                var task_url = $("#form-update :input[name='task-ur;']").val(); // get name
                var time = $("#form-update :input[name='task-time']").val(); // get task time

                db.transaction(function (tx) {
                    tx.executeSql("UPDATE tasks SET project_title = ?, name = ?, url = ?, time = ? WHERE id = ?", [project_title, task_title, task_url, taskInterface.sec(time), ID], function (tx, results) {
                        taskInterface.index();
                    }, onError);
                });
            });

            /* reset task
             ------------------------------------------------------------------------ */

            $(".reset").on("click", function (e) {
                e.preventDefault();
                $(".form").hide();

                var ID = $(this).data("id");

                db.transaction(function (tx) {
                    tx.executeSql("UPDATE tasks SET time = ? WHERE id = ?", [0, ID], function (tx, results) {
                        taskInterface.index();
                    }, onError);
                });

            });



        },
        index: function () {
            this.rows = tasks.list();
            console.log(rows);
            var out = "";
            var len = rows.length;

            if (len > 0) {
                for (i = 0; i < len; i++) {
                    var task = rows.item(i);

                    out += '<p class="item' + (task.running == true ? ' running' : '') + '" id="item' + task.id + '" data-id="' + task.id + '">';
                    out += '<label>' + (task.task_title != "" ? task.task_title : "Task:" + task.id) + '<br/><small>' + task.project_title + '</small></label>';
                    out += '<a href="#" class="update" data-id="' + task.id + '" title="Edit: ' + task.task_title + '">Edit</a> | ';
                    out += '<a href="#" class="reset" data-id="' + task.id + '" title="Reset: ' + task.task_title + '">Reset</a> | ';
                    out += '<a href="#" class="remove" data-id="' + task.id + '" title="Delete: ' + task.task_title + '">Delete</a>';

                    if (task.running == true) {
                        var start = new Date(task.start);
                        var dif = Number(task.time) + Math.floor((new Date().getTime() - start.getTime()) / 1000)
                        out += '<span class="timer">' + taskInterface.hms(dif) + '</span>';
                    } else {
                        out += '<span class="timer">' + taskInterface.hms(task.time) + '</span>';
                    }

                    out += '<a href="#" class="play power' + (task.running == true ? ' running' : '') + '" title="Timer on/off" data-id="' + task.id + '"></a>';
                    out += '</p>';

                    if (task.running == true) {
                        taskInterface.startTask(task); // start task
                    }
                }
            } else {
                out = "<p class=\"notask\"><label>No tasks</label></p>"
            }

            $("#form-list").empty().append(out).show();


        },
        init: function () {
            createTable();
            this.bind();
            this.index();
            this.toggleRunText();
        },
        toggleTimer: function (ID) {
            console.log(ID);
            db.transaction(function (tx) {
                tx.executeSql('SELECT * FROM tasks WHERE id = ?', [ID], function (tx, results) {
                    if (results.rows.length > 0) {
                        var task = results.rows.item(0);
                        $('#item' + ID).toggleClass('running');
                        $('#item' + ID + ' .power').toggleClass('running');

                        if (task.running == true) {
                            taskInterface.stopTask(task);
                        } else {
                            taskInterface.startTask(task);
                        }

                        taskInterface.toggleRunText();

                    } else {
                        alert("Task " + ID + " not found sorry!");
                    }
                }, null);
            });
        },
        //////////////////////////////////////////////////////////////////////////////
        // start task
        //////////////////////////////////////////////////////////////////////////////

        startTask: function (task) {
            window.clearInterval(taskInterface.intervals[task.id]); // remove timer

            var start = new Date(); // set start to NOW

            if (task.running == true) {
                start = new Date(task.start);
            } else {
                db.transaction(function (tx) {
                    tx.executeSql("UPDATE tasks SET running = ?, start = ? WHERE id = ?", [1, start, task.id], null, onError);
                });
            }

            // setup interval for counter
            taskInterface.intervals[task.id] = window.setInterval(function () {
                var dif = Number(task.time) + Math.floor((new Date().getTime() - start.getTime()) / 1000)
                $('#item' + task.id + ' .timer').text(taskInterface.hms(dif));
            }, 500);
        },
        //////////////////////////////////////////////////////////////////////////////
        // stop task
        //////////////////////////////////////////////////////////////////////////////

        stopTask: function (task) {
            window.clearInterval(taskInterface.intervals[task.id]); // remove timer

            var start, stop, dif = 0;

            db.transaction(function (tx) {
                tx.executeSql('SELECT * FROM tasks WHERE id = ?', [task.id], function (tx, results) {
                    if (results.rows.length > 0) {
                        start = new Date(results.rows.item(0).start); // read from DB
                        stop = new Date(); // now
                        dif = Number(results.rows.item(0).time) + Math.floor((stop.getTime() - start.getTime()) / 1000); // time diff in seconds

                        $('#item' + task.id + ' .timer').text(taskInterface.hms(dif));

                    } else {
                        alert('Task ' + task.id + ' not found!');
                    }
                }, null, onError);
            });

            // update record
            db.transaction(function (tx) {
                tx.executeSql("UPDATE tasks SET running = ?, time = ? WHERE id = ?", [0, Number(dif), task.id], null, onError);
            });

        },
        //////////////////////////////////////////////////////////////////////////////
        // toggle RUN text on icon
        //////////////////////////////////////////////////////////////////////////////

        toggleRunText: function () {
            db.transaction(function (tx) {
                tx.executeSql('SELECT * FROM tasks WHERE running = ?', [1], function (tx, results) {
                    if (results.rows.length > 0) {
                        chrome.browserAction.setBadgeText({
                            text: 'RUN'
                        });
                    } else {
                        chrome.browserAction.setBadgeText({
                            text: ''
                        });
                    }
                }, null, onError);
            });
        },
        //////////////////////////////////////////////////////////////////////////////
        // convert sec to hms
        //////////////////////////////////////////////////////////////////////////////

        hms: function (secs) {
            //secs = secs % 86400; // fix 24:00:00 overlay
            var time = [0, 0, secs], i;
            for (i = 2; i > 0; i--) {
                time[i - 1] = Math.floor(time[i] / 60);
                time[i] = time[i] % 60;
                if (time[i] < 10) {
                    time[i] = '0' + time[i];
                }
            }
            return time.join(':');
        },
        //////////////////////////////////////////////////////////////////////////////
        // convert h:m:s to sec
        //////////////////////////////////////////////////////////////////////////////

        sec: function (hms) {
            var t = String(hms).split(":");
            return Number(parseFloat(t[0] * 3600) + parseFloat(t[1]) * 60 + parseFloat(t[2]));
        },
        nextID: function () {
            var id = localStorage['lastID']; // get last ID from local storage
            if (id == undefined) {
                id = 1; // generate first ID
            } else {
                id++; // generate next ID
            }
            localStorage['lastID'] = id; // save to localStorage
            return id;
        },
        getCurrentTabUrl: function () {
            chrome.tabs.query({currentWindow: true, active: true}, function (tabs) {
//            console.log(tabs[0].url);
                return tabs[0].url;
            });
        }
    },
    collections: {},
};

var task = task || {};
task.utils = {};

(function () {
    var val = 5;

    this.getValue = function () {
        return val;
    };

    this.setValue = function (newVal) {
        val = newVal;
    }

    // also introduce a new sub-namespace
    this.tools = {};

}).apply(myApp.utils);

// inject new behaviour into the tools namespace
// which we defined via the utilities module

(function () {
    this.diagnose = function () {
        return "diagnosis";
    }
}).apply(myApp.utils.tools);