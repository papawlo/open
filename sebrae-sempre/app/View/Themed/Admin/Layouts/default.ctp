<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo $title_for_layout; ?></title>
        <?php
        echo $this->Html->css(array(
            '/admin/css/bootstrap.min.css',
            '/admin/css/dashboard.css',
            'bootstrap-datetimepicker',
        ));
        echo $this->Html->script('//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js');
        ?>
        <style>
            .page-wrapper {
                width: 960px; /* Your page width */
                margin: 0 auto; /* To center your page within the body */
            }
        </style>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        $app = array();
        $app['basePath'] = Router::url('/');
        $app['params'] = array(
            'controller' => $this->params['controller'],
            'action' => $this->params['action'],
            'named' => $this->params['named'],
        );
        echo $this->Html->scriptBlock('var App = ' . $this->Js->object($app) . ';');
        ?>
    </head>

    <body>
        <div class="page-wrapper">

            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <?php echo $this->Html->link(__d('admin', 'Sebrae Sempre'), '/admin', array('class' => 'navbar-brand')); ?>                    
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">                                                                   
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo __('Posts');?> <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><?php echo $this->Html->link(__('List %s', __('Posts')), array('controller' => 'posts', 'action' => 'index')); ?></li>
                                    <li><?php echo $this->Html->link(__('New %s', __('Post')), array('controller' => 'posts', 'action' => 'add')); ?></li>                                                               
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo __('Categories');?> <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><?php echo $this->Html->link(__('List %s', __('Categories')), array('controller' => 'categories', 'action' => 'index')); ?></li>
                                    <li><?php echo $this->Html->link(__('New %s', __('Category')), array('controller' => 'categories', 'action' => 'add')); ?></li>                                                               
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo __('Contacts');?> <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><?php echo $this->Html->link(__('List %s', __('Contacts')), array('controller' => 'contacts', 'action' => 'index')); ?></li>
                                    <li><?php echo $this->Html->link(__('New %s', __('Contact')), array('controller' => 'contacts', 'action' => 'add')); ?></li>                                                               
                                </ul>
                            </li>
                            <li><?php echo $this->Html->link(__('Criar Campanha de Email'), array('controller' => 'digimailing', 'action' => 'index')); ?></li>
                        </ul>

                        <ul class="nav navbar-nav navbar-right">
<!--                            <li><?php // echo $this->Html->link(__d('admin', 'Dashboard'), '/admin'); ?></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php // echo __d('admin', 'Languages'); ?> <b class="caret"></b></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <?php
//                                        echo $this->Html->image("Admin.flags/por.png", array(
//                                            "alt" => __d('admin', "Brazilian"),
//                                            'url' => '/admin/languages/pt-br',
//                                            'class' => 'lang-flag'
//                                        ));
//                                        ?>
                                    </li>
                                    <li>
                                        <?php
//                                        echo $this->Html->image("Admin.flags/eng.png", array(
//                                            "alt" => __d('admin', "English"),
//                                            'url' => '/admin/languages/eng',
//                                            'class' => 'lang-flag'
//                                        ));
                                        ?>
                                    </li>                
                                </ul>
                            </li>-->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo __('User control'); ?> <b class="caret"></b></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <?php echo $this->Html->link(__('Users'), array('plugin' => 'admin', 'controller' => 'users', 'action' => 'index', 'admin' => true)); ?>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <?php echo $this->Html->link(__('Logout'), array('plugin' => 'admin', 'controller' => 'users', 'action' => 'logout', 'admin' => true)); ?>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="container-fluid">
                <div class="row">
                    <?php
                    echo $this->Session->flash();
                    echo $this->Session->flash('auth');
                    ?>

                    <!--<div class="col-sm-12 col-md-12 col-lg-12">-->
                    <h2 class="page-header"><?php echo $title_for_layout; ?></h2>
                    <!--</div>-->
                </div>
                <?php echo $this->fetch('content'); ?>          
            </div>
        </div>
        
        <?php
        echo $this->Html->script('/admin/js/bootstrap.min.js');
        echo $this->Html->script('bootstrap-datetimepicker');
        echo $this->fetch('script');
        ?>
        <script type="text/javascript">
            $(function() {
                $('.lang-flag').each(function(i, val) {
                    $alt = val.alt;
                    $(this).parent('a').append(' ' + $alt);
                });
            });
        </script>
    </body>
</html>
