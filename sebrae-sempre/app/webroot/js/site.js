var clndr = {};

$(function() {

    moment.locale('pt-br');
    var events = [];
    var events_bck = [];

    function get_eventos(mes, ano) {

        $.ajax({
            url: '../home/eventos',
            data: {
                mes: mes,
                ano: ano
            },
            type: 'post',
            dataType: 'json',
            success: function(data) {
//                console.log(data);
                events = [];
                $.each(data, function() {
                    /// do stuff
                    var evento = {date: this.Post.data, title: this.Post.title, body: this.Post.body, link: this.Post.link, category: this.Category.title, category_id: this.Category.id};
                    events.push(evento);
                });
                clndr.setEvents(events);

            },
            error: function(data) {
                console.log(data);

//                        alert("Eorror occured");
            },
            complete: function(data) {
//                alert($.parseJSON(data.responseText).val);
            }
        });
    }


    var currentMonth = moment().format('MM');
    var currentYear = moment().format('YYYY');

    get_eventos(currentMonth, currentYear);


    clndr = $('#full-clndr').clndr({
        template: $('#full-clndr-template').html(),
        events: events,
        clickEvents: {
            click: function(target) {
                console.log(target);
                events_bck = events;
                if ($(target.element).hasClass('event'))
                    clndr.setEvents(target.events);
                else {
                    clndr.setEvents(events_bck);
                }
//                 $('.event-item').show().filter(':not(.cat-' + $(this).data('category') + ')').hide();
//                 if you turn the `constraints` option on, try this out:
//                 if($(target.element).hasClass('inactive')) {
//                 console.log('not a valid datepicker date.');
//                 } else {
//                 console.log('VALID datepicker date.');
//                 }
            },
            onMonthChange: function(month) {
                get_eventos(month.format('MM'), month.format('YYYY'));
            }
        },
        daysOfTheWeek: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
        forceSixRows: true
    });


    $(document).on("click", '.choose-category', function() {
        console.log("clicou", $(this).data('category'));
        if ($(this).data('category') != 'all') {
            clndr.setEvents(events_bck);
            $('.event-item').show().filter(':not(.cat-' + $(this).data('category') + ')').hide();
        } else {
            $('.event-item').show();
        }
    });


//    var events = [
//        {date: currentMonth + '-' + '10', title: 'Persian Kitten Auction', location: 'Center for Beautiful Cats'},
//        {date: currentMonth + '-' + '19', title: 'Cat Frisbee', location: 'Jefferson Park'},
//        {date: currentMonth + '-' + '23', title: 'Kitten Demonstration', location: 'Center for Beautiful Cats'},
//        {date: nextMonth + '-' + '07', title: 'Small Cat Photo Session', location: 'Center for Cat Photography'}
//    ];

//    clndr = $('#full-clndr').clndr({
//        template: $('#full-clndr-template').html(),
//        events: events,
//        forceSixRows: true
//    });

//    $('#mini-clndr').clndr({
//        template: $('#mini-clndr-template').html(),
//        events: events,
//        clickEvents: {
//            click: function(target) {
//                if (target.events.length) {
//                    var daysContainer = $('#mini-clndr').find('.days-container');
//                    daysContainer.toggleClass('show-events', true);
//                    $('#mini-clndr').find('.x-button').click(function() {
//                        daysContainer.toggleClass('show-events', false);
//                    });
//                }
//            }
//        },
//        adjacentDaysChangeMonth: true,
//        forceSixRows: true
//    });
//
//    $('#clndr-3').clndr({
//        template: $('#clndr-3-template').html(),
//        events: events,
//        showAdjacentMonths: false
//    });

    // $('#clndr-4').clndr({
    //   template: $('#clndr-4-template').html(),
    //   events: events,
    //   lengthOfTime: {
    //     days: 7,
    //     interval: 7
    //   }
    // });

    // bind both clndrs to the left and right arrow keys
    $(document).keydown(function(e) {
        if (e.keyCode == 37) {
//            console.log('37 left');
            // left arrow
            clndr.back();
        }
        if (e.keyCode == 39) {
//            console.log('39 right');
            // right arrow
            clndr.forward();

        }
    });
});