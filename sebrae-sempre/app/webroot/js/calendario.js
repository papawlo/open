$(function() {

    $(".lista-eventos").accordion({
        collapsible: true,
        header: ".nome-evento",
        heightStyle: "content",
    });
    $(".lista-eventos").accordion('refresh')

    var pane = $('.content')
    pane.jScrollPane({
        verticalDragMinHeight: 21,
        verticalDragMaxHeight: 21,
        autoReinitialise: true,
    });
    var api = pane.data('jsp');
    api.reinitialise();



    //////////


    dateEventos = [];
    var d = new Date();

    var currentMonth = d.getMonth() + 1;
    var currentYear = d.getFullYear();
    getEventos(currentMonth, currentYear);
    function getEventos(mes, ano, categoria) {
        dateEventos = [];
        jQuery.getJSON('eventos', {mes: mes, ano: ano, categoria: categoria}, function(data) {

            $.each(data.posts, function(index, value) {
                dateEventos[index] = value;
            });

            $(".datepicker").datepicker("refresh");
        });
    }

    $(".datepicker").datepicker({// Cria o datepicker com as informações.
        dateFormat: 'dd-mm-yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        nextText: '',
        prevText: '',
        onSelect: function(date, inst) {
            dmy = inst.currentDay;
            if (dmy < 10) {
                dmy = "0" + dmy;
            }
            dmy += "-";
            dmy += "0" + (inst.currentMonth + 1) + "-";
            dmy += inst.currentYear;


            $(".lista-eventos li").show();
//            if ($(".lista-eventos li").data("date") != dmy) {
//            console.log(dmy);

            var lista = $('.lista-eventos li').filter('[data-date!=' + dmy + ']');
            lista.hide();
//            }
//            $(".lista-eventos li").remove();

            inst.inline = false;
//            $(this).css('background-color', '');
//            alert('Selected: ' + date +
//                    "\n\nid: " + inst.id +
//                    "\nselectedDay: " + inst.selectedDay +
//                    "\nselectedMonth: " + inst.selectedMonth +
//                    "\nselectedYear: " + inst.selectedYear);

//                date.dpDiv.find('.ui-datepicker-current-day a')
//                    .css('background-color', '#000000');

//            $(".ui-datepicker-calendar .ui-datepicker-current-day").removeClass("ui-datepicker-current-day").children().removeClass("ui-state-active");


//            $(".ui-datepicker-calendar TBODY A").each(function() {
//                if ($(this).text() == inst.selectedDay) {
//                    $(this).addClass("ui-state-active");
//                    $(this).parent().addClass("ui-datepicker-current-day");
//                }
//            });

            // $(this).data('datepicker').inline = false; // NÃO APAGUE, PODE SER ÚTIL.
//            if (date in dateEventos) {

//                $.each(dateEventos[date], function(index, value) {
//                    $(".lista-eventos").append($('<li class="' + slugify(value.category) + '"><h3 class="nome-evento">' + value.title + '</h3><div class="conteudo-accordion"><h4 class="descricao-evento">' + value.content + '</h4><footer><h5 class="data-evento"></h5><h5 class="hora-evento">' + value.hour + '</h5></footer></div></li>'));
//                    if ($(".filtros fieldset.active").hasClass("mei")) {
//                        $(".lista-eventos li").not(".mei").remove();
//                    } else if ($(".filtros fieldset.active").hasClass("produtor_rural")) {
//                        $(".lista-eventos li").not(".produtor_rural").remove();
//
//                    } else if ($(".filtros fieldset.active").hasClass("micro_empresario")) {
//                        $(".lista-eventos li").not(".micro_empresario").remove();
//
//                    } else if ($(".filtros fieldset.active").hasClass("potencial_empreendedor")) {
//                        $(".lista-eventos li").not(".potencial_empreendedor").remove();
//
//                    } else if ($(".filtros fieldset.active").hasClass("pequeno_empresario")) {
//                        $(".lista-eventos li").not(".pequeno_empresario").remove();
//
//                    } else if ($(".filtros fieldset.active").hasClass("potencial_empresario")) {
//                        $(".lista-eventos li").not(".potencial_empresario").remove();
//                    }
//                });




//            }

//            if ($(".lista-eventos li").length == 0) {
//                $(".data-nao-encontrada").show();
//                $(".bem-vindo-texto").hide();
//            } else {
//                $(".data-nao-encontrada").hide();
//                $(".bem-vindo-texto").show();
//            }

//            $(".lista-eventos").accordion('refresh');
            switch (inst.currentMonth) {
                case 0:
                    mes = "Janeiro";
                    break;
                case 1:
                    mes = "Fevereiro";
                    break;
                case 2:
                    mes = "Março";
                    break;
                case 3:
                    mes = "Abril";
                    break;
                case 4:
                    mes = "Maio";
                    break;
                case 5:
                    mes = "Junho";
                    break;
                case 6:
                    mes = "Julho";
                    break;
                case 7:
                    mes = "Agosto";
                    break;
                case 8:
                    mes = "Setembro";
                    break;
                case 9:
                    mes = "Outubro";
                    break;
                case 10:
                    mes = "Novembro";
                    break;
                case 11:
                    mes = "Dezembro";
                    break;
            }
            $(".header-principal .titulo-data").html(inst.currentDay + " de " + mes);

            scrollToID('#r-calendar', 750);
        },
        beforeShowDay: function(date) {
            dmy = date.getDate();
            if (dmy < 10) {
                dmy = "0" + dmy;
            }
            dmy += "-";
            dmy += "0" + (date.getMonth() + 1) + "-";
            dmy += date.getFullYear();

            if (dmy in dateEventos) {
                valorClasse = "";
                $.each(dateEventos[dmy], function(index, value) {
                    $(".lista-eventos").append($('<li class="' + slugify(value.category) + '" data-date="' + dmy + '"><h3 class="nome-evento">' + value.title + '</h3><div class="conteudo-accordion"><h4 class="descricao-evento">' + value.content + '</h4><footer><h5 class="data-evento">' + dmy + '</h5><h5 class="hora-evento">' + value.hour + '</h5></footer><a href="' + value.link + '" class="ir-para-a-loja">Ver mais</a></div></li>'));
                    valorClasse += " " + slugify(value.category);
                });
                $(".lista-eventos").accordion('refresh')
                return [true, "active " + valorClasse];
            } else {
                return [false, ""];
            }
        },
        onChangeMonthYear: function(year, month) {
            var categoria = $(".chk-box:checked").val();
            getEventos(month, year, categoria);
            $(".lista-eventos li").remove();
//            $(".filtros fieldset input").removeAttr("checked").parent().removeClass("active");
//            $(".lista-eventos").accordion('refresh');
        }
    });
    function scrollToID(id, speed) {
        var offSet = 0;
        var targetOffset = $(id).offset().top - offSet;
        var mainNav = $('#main-nav');
        $('html,body').animate({scrollTop: targetOffset}, speed);
        if (mainNav.hasClass("open")) {
            mainNav.css("height", "1px").removeClass("in").addClass("collapse");
            mainNav.removeClass("open");
        }
    }


    $(".filtros fieldset input").click(function() {

        var primeiraClasse = $(this).parent().attr("class").split(' ')[0]
        var allClasses = ["mei_active", "produtor_rural_active", "micro_empresario_active", "potencial_empreendedor_active", "pequeno_empresario_active", "potencial_empresario_active"];

        $.each(allClasses, function(index, value) {
            $("td").removeClass(value);
        })
        $("td").removeClass("active");
        $("td." + primeiraClasse).addClass(primeiraClasse + "_active");
        $(".lista-eventos li").not("." + primeiraClasse).hide();
        $(".lista-eventos li." + primeiraClasse).show();

        $(".filtros fieldset").removeClass("active");
        $(this).parent().addClass("active");
    });

    function slugify(str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to = "aaaaeeeeiiiioooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '_'); // collapse dashes

        return str;
    }
});

