<?php
/**
 * Read up on the WP Template Hierarchy for
 * when this file is used
 *
 */
?>
<?php get_header(); ?>

<!-- SECTION 1 SINOPSE -->
<section id="sinopse" class="section sinopse">
    <div class="row">
        <div class="col">
            <h2>Conheça os cinco</h2>
            <span class="separator"></span>
            <h3>A sinopse de OOT</h3>
        </div>
    </div>
    <div class="row">
        <div class="col col-lg-one-half">
            <?php the_field("section_sinopse"); ?>

        </div>
        <div class = "col col-lg-one-half">
            <figure class = "livro block">
                <!--png -->

                <img src = "<?php echo get_stylesheet_directory_uri() ?>/dist/imgs/foto-livro-min.png" srcset = "<?php echo get_stylesheet_directory_uri() ?>/dist/imgs/foto-livro@2x-min.png 2x,
                     <?php echo get_stylesheet_directory_uri() ?>/dist/imgs/foto-livro@3x-min.png 3x" class = "FotoLivro responsive-image">
            </figure>
        </div>
    </div>
    <div class = "row">
        <div class = "col">
            <a href = "#download-options" class = "btn btn-medium">Baixe agora<span><img src = "<?php echo get_stylesheet_directory_uri() ?>/dist/imgs/scroll.svg" class = "scroll" width = "12" height = "17" /></span></a>
        </div>
    </div>
</section>


<!--SECTION 2 DOWNLOADS-->
<section id = "download-options" class = "section download-options">
    <div class = "row">
        <div class = "col">
            <h2>FAÇA O DOWNLOAD OU LEIA NO KINDLE</h2>
            <span class = "separator"></span>
            <h3>Escolha uma das opções abaixo. O download em PDF e ePUB é gratuito</h3>
        </div>
    </div>

    <div class = "row">
        <!--box 1 -->
        <div class = "col col-lg-one-third">
            <div class = "download-info">
                <figure class = "box box-mini">
                    <!--png -->
                    <img src = "<?php echo get_stylesheet_directory_uri() ?>/dist/imgs/pdf-ico-min.png" srcset = "<?php echo get_stylesheet_directory_uri() ?>/dist/imgs/pdf-ico@2x-min.png 2x,
                         <?php echo get_stylesheet_directory_uri() ?>/dist/imgs/pdf-ico@3x-min.png 3x" class = "PdfIco">
                </figure>

                <h4>OOT em PDF</h4>
                <?php the_field("texto_download_pdf"); ?>
                <!--<p>Amém PDF!O formato que abre em quase tudo que tem uma tela. Aqui indicamos o download para a leitura no computador.</p>-->

                <button type = "button" name = "button" class = "btn btn-slim abre-form-download">Download</button>
            </div>
            <div class = "box box-long download-form">
                <a class = "boxclose"></a>
                <h4>OOT em PDF</h4>
                <?php echo do_shortcode('[contact-form-7 id="19" title="Formulário de Download PDF"]');
                ?>

                <a href="<?php the_field('arquivo_pdf', 'option') ?>" class="link" target="_blank" onclick="ga('send', 'event', 'Download', 'PDF', 'Link Direto');">Baixar sem e-mail</a>

            </div>

        </div>
        <!-- fim do box 1 -->
        <!-- box 2 -->
        <div class="col col-lg-one-third">
            <div class="download-info">
                <figure class="box box-mini">
                    <!-- svg -->
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/imgs/EPUB_logo.svg" class="EPUBico">
                </figure>

                <h4>OOT em ePUB</h4>
                <?php the_field("texto_download_epub"); ?>
                <button type="button" name="button" class="btn btn-slim abre-form-download">Download</button>
            </div>
            <div class="box box-long download-form">
                <a class="boxclose"></a>
                <h4>OOT em ePUB</h4>
                <?php echo do_shortcode('[contact-form-7 id="20" title="Formulário de Download EPUB"]'); ?>

                <a href="<?php the_field('arquivo_epub', 'option') ?>" class="link" target="_blank"  onclick="ga('send', 'event', 'Download', 'EPUB', 'Link Direto');">Baixar sem e-mail</a>

            </div>
        </div>

        <div class="col col-lg-one-third">
            <div class="download-info">
                <figure class="box box-mini">
                    <!-- png -->

                    <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/imgs/amazon-icone-min.png" srcset="<?php echo get_stylesheet_directory_uri() ?>/dist/imgs/amazon-icone@2x-min.png 2x,
                         <?php echo get_stylesheet_directory_uri() ?>/dist/imgs/amazon-icone@3x-min.png 3x" class="AmazonIcone">

                </figure>

                <h4>OOT na Amazon</h4>
                <?php the_field("texto_download_amazon"); ?>

                <a href="<?php the_field('link_da_amazon', 'option') ?>" class="btn btn-slim" id="btn-link-amazon" target="_blank"  onclick="ga('send', 'event', 'Download', 'Amazon', 'Link');">Ir para a Amazon <span class="dashicons dashicons-external"></span></a>

            </div>

        </div>
    </div>
</section>

<!-- SECTION 3 ABOUT -->
<section id="about" class="section about">
    <div class="row">
        <div class="col">
            <h2>Sobre o autor</h2>
            <span class="separator"></span>
            <h3>O "E" é de Ermeson</h3>
        </div>
    </div>
    <div class="row">
        <div class="col col-lg-one-fourth">
            <figure class="foto">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/imgs/Ermeson-David_372x465.jpg" alt="Foto Ermerson David" width="312" height="390" class="responsive-image" />
            </figure>
        </div>
        <div class="col col-lg-four-fourth">
            <?php the_field("section_about"); ?>
<!--            <p>
                <b>E. David</b> é formado em Comunicação em Mídias Digitais, cinéfilo e amante assumido de literatura fantástica.
            </p>
            <p>
                David sempre teve paixão em acompanhar as mais variadas obras do mundo da ficção. Dos quadrinhos à TV, dos livros ao cinema. De J.K. Rowling a Tolkien, de Ana Maria Machado a Herman Melville.
            </p>
            <p> Durante a sua adolescência, foi vencedor em dois concursos: um de redação e outro de frases. Na fantasia, se aventura desde os nove anos de idade. Os Outros Terráqueos é uma história que busca mesclar o que há de mais atrativo nos subgêneros
                da fantasia épica e contemporânea. É também o livro de estreia de E. David como autor.
            </p>
            <p>
                Leia por sua conta e risco.
            </p>-->
        </div>
    </div>
</section>

<!-- SECTION 4 CONTACT -->
<section id="contato" class="section contato">
    <div class="row">
        <div class="col">
            <h2>Entre em contato</h2>
            <span class="separator"></span>
            <h3>A sua mensagem será enviada em um <em>sopro</em></h3>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <?php echo do_shortcode('[contact-form-7 id="5" title="Formulário de Contato"]'); ?>

        </div>
    </div>
</section>

<?php get_footer(); ?>
