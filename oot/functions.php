<?php

/**
 * Theme Functions &
 * Functionality
 *
 */
/* =========================================
  ACTION HOOKS & FILTERS
  ========================================= */

/* * --- Actions ---* */

add_action('after_setup_theme', 'theme_setup');

add_action('wp_enqueue_scripts', 'theme_styles');

add_action('wp_enqueue_scripts', 'theme_scripts');

// expose php variables to js. just uncomment line
// below and see function theme_scripts_localize
add_action('wp_enqueue_scripts', 'theme_scripts_localize', 20);

/* * --- Filters ---* */



/* =========================================
  HOOKED Functions
  ========================================= */

/* * --- Actions ---* */


/**
 * Setup the theme
 *
 * @since 1.0
 */
if (!function_exists('theme_setup')) {

    function theme_setup() {

        // Let wp know we want to use html5 for content
        // add_theme_support( 'html5', array(
        // 	'comment-list',
        // 	'comment-form',
        // 	'search-form',
        // 	'gallery',
        // 	'caption'
        // ) );
        // Let wp know we want to use post thumbnails
        /*
          add_theme_support( 'post-thumbnails' );
         */

        // Add Custom Logo Support.
        /*
          add_theme_support( 'custom-logo', array(
          'width'       => 181, // Example Width Size
          'height'      => 42,  // Example Height Size
          'flex-width'  => true,
          ) );
         */

        // Register navigation menus for theme
        /*
          register_nav_menus( array(
          'primary' => 'Main Menu',
          'footer'  => 'Footer Menu'
          ) );
         */


        // Let wp know we are going to handle styling galleries
        /*
          add_filter( 'use_default_gallery_style', '__return_false' );
         */


        // Stop WP from printing emoji service on the front
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');


        // Remove toolbar for all users in front end
        show_admin_bar(false);


        // Add Custom Image Sizes
        /*
          add_image_size( 'ExampleImageSize', 1200, 450, true ); // Example Image Size
          ...
         */




        // Contact Form 7 Configuration needs to be done
        // in wp-config.php. add the following snippet
        // under the line:
        // define( 'WP_DEBUG', false );
        /*
         */

        // //Contact Form 7 Plugin Configuration
        // // define ( 'WPCF7_LOAD_JS',  false ); // Added to disable JS loading
        // define ( 'WPCF7_LOAD_CSS', false ); // Added to disable CSS loading
        // define ( 'WPCF7_AUTOP',    false ); // Added to disable adding <p> & <br> in form output
    }

}


/**
 * Register and/or Enqueue
 * Styles for the theme
 *
 * @since 1.0
 */
if (!function_exists('theme_styles')) {

    function theme_styles() {
        $theme_dir = get_stylesheet_directory_uri();

        wp_enqueue_style('main', "$theme_dir/dist/css/style.min.css", array(), null, 'all');
        wp_enqueue_style('dashicons');
        wp_enqueue_style('contact7-custom', "$theme_dir/dist/css/contact7-custom.css", array('dashicons'), null, 'all');
    }

}


/**
 * Register and/or Enqueue
 * Scripts for the theme
 *
 * @since 1.0
 */
if (!function_exists('theme_scripts')) {

    function theme_scripts() {
        $theme_dir = get_stylesheet_directory_uri();
        wp_enqueue_script('main-js', "$theme_dir/dist/js/main.js", array("jquery"), false, true);
        wp_enqueue_script('scrollreveal-js', "https://unpkg.com/scrollreveal/dist/scrollreveal.min.js", array("jquery"), false, true);
    }

}


/**
 * Attach variables we want
 * to expose to our JS
 *
 * @since 3.12.0
 */
if (!function_exists('theme_scripts_localize')) {

    function theme_scripts_localize() {
        $ajax_url_params = array();

        // You can remove this block if you don't use WPML
        // if ( function_exists( 'wpml_object_id' ) ) {
        // 	/** @var $sitepress SitePress */
        // 	global $sitepress;
        //
		// 	$current_lang = $sitepress->get_current_language();
        // 	wp_localize_script( 'main', 'i18n', array(
        // 		'lang' => $current_lang
        // 	) );
        //
		// 	$ajax_url_params['lang'] = $current_lang;
        // }

        wp_localize_script('main', 'urls', array(
            'home' => home_url(),
            'theme' => get_stylesheet_directory_uri(),
            'ajax' => add_query_arg($ajax_url_params, admin_url('admin-ajax.php'))
        ));
    }

}

/*
 * Interrompe envio de email padrão pelo Plugin Contact 7 Form
 */

function skip_mail($skip_mail, $contact_form) {

    if ($contact_form->id != 5) {

        // Return true if you want to skip sending the mail.
        return true;
    }

    // Return false if you want to send the mail.
    return false;
}

add_filter('wpcf7_skip_mail', 'skip_mail', 10, 2);



/*
 * Adiciona o botão de Download na resposta dos formulários de download.
 */

add_action("wpcf7_ajax_json_echo", "cf7_change_response_message", 10, 2);

function cf7_change_response_message($items, $result) {

    $filename = "";

    if ($result['status'] == 'mail_sent') {

        switch ($result['contact_form_id']) {
            case "19"://form PDF

                $filename = get_field("arquivo_pdf", 'option');
                $items['message'] .= '<br> <a href="' . $filename . '" class="btn btn-medium" id="btn-download-pdf" target="_blank"  onclick="ga(\'send\', \'event\', \'Download\', \'PDF\', \'Link Formulário\');">Download</a>';
                break;

            case "20"://form Epub

                $filename = get_field("arquivo_epub", 'option');
                $items['message'] .= '<br> <a href="' . $filename . '" class="btn btn-medium" id="btn-download-epub" target="_blank" onclick="ga(\'send\', \'event\', \'Download\', \'EPUB\', \'Link Formulário\');">Download</a>';
                break;
            default:

                break;
        }
    }



    return $items;
}

//Using Values from A Form-Tag
add_action('wpcf7_init', 'custom_add_form_tag_datalist');

function custom_add_form_tag_datalist() {
    wpcf7_add_form_tag('datalist', 'custom_datalist_form_tag_handler', array('name-attr' => true));
}

function custom_datalist_form_tag_handler($tag) {
    $atts = array(
        'type' => 'text',
        'name' => $tag->name,
        'list' => $tag->name . '-options',
        'placeholder' => ucfirst($tag->name)
    );


    $input = sprintf(
            '<input %s />', wpcf7_format_atts($atts));

    $datalist = '';

    foreach ($tag->values as $val) {
        $datalist .= sprintf('<option>%s</option>', esc_html($val));
    }

    $datalist = sprintf(
            '<datalist id="%1$s">%2$s</datalist>', $tag->name . '-options', $datalist);

    return $input . $datalist;
}

function my_myme_types($mime_types) {
    $mime_types['svg'] = 'image/svg+xml'; //Adding svg extension
    $mime_types['epub'] = 'application/epub+zip'; //Adding epub extension   'epub|mobi' => 'application/octet-stream'

    return $mime_types;
}

add_filter('upload_mimes', 'my_myme_types', 1, 1);


/*
 * Mudando tela login
 */

function my_custom_login() {
    echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/admin/login/custom-login-styles.css" />';
}

add_action('login_head', 'my_custom_login');

function my_login_logo_url() {
    return get_bloginfo('url');
}

add_filter('login_headerurl', 'my_login_logo_url');

function my_login_logo_url_title() {
    return 'Os Outros Terráqueos';
}

add_filter('login_headertitle', 'my_login_logo_url_title');



/*
 *  Change the Options Page menu to 'Theme Options'
 */


if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Arquivos e Links',
        'menu_title' => 'Arquivos e Links',
        'icon_url' => "dashicons-book",
        'position' => 3
    ));
}

function _remove_script_version($src) {
    $parts = explode('?ver', $src);
    return $parts[0];
}

add_filter('script_loader_src', '_remove_script_version', 15, 1);
add_filter('style_loader_src', '_remove_script_version', 15, 1);

// Remove jquery block render
function addthemejs() { 
    wp_deregister_script( 'jquery' ); // remove standard jquery
    wp_enqueue_script( 'jquery', 'https://code.jquery.com/jquery-2.2.4.min.js' ,false,false, true );
}

//----------------------------------------------------------/
//  call the enqueue hook
//----------------------------------------------------------/

add_action('wp_enqueue_scripts', 'addthemejs');