<?php
/**
 * Header file common to all
 * templates
 *
 */
?>
<!doctype html>
<html class="site no-js" <?php language_attributes(); ?>>
    <head>
        <!--[if lt IE 9]>
                <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
        <![endif]-->

        <meta charset="<?php bloginfo('charset'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,Chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="theme-color" content="#060426">
        
        <title><?php wp_title(); ?></title>

        <link rel="dns-prefetch" href="//fonts.googleapis.com/">
        <style>@font-face{font-family:'Titillium Web';font-style:italic;font-weight:300;src:local('Titillium Web Light Italic'),local('TitilliumWeb-LightItalic'),url(https://fonts.gstatic.com/s/titilliumweb/v5/RZunN20OBmkvrU7sA4GPPhPpE1UvVmSV1Lr9FL5pkyg.ttf) format('truetype')}@font-face{font-family:'Titillium Web';font-style:normal;font-weight:300;src:local('Titillium Web Light'),local('TitilliumWeb-Light'),url(https://fonts.gstatic.com/s/titilliumweb/v5/anMUvcNT0H1YN4FII8wpr93Z6MveExszb-iKWJY7ddA.ttf) format('truetype')}@font-face{font-family:'Titillium Web';font-style:normal;font-weight:400;src:local('Titillium Web Regular'),local('TitilliumWeb-Regular'),url(https://fonts.gstatic.com/s/titilliumweb/v5/7XUFZ5tgS-tD6QamInJTcU3KvHLhcNjEHFQzwNtdMQY.ttf) format('truetype')}@font-face{font-family:'Titillium Web';font-style:normal;font-weight:600;src:local('Titillium Web SemiBold'),local('TitilliumWeb-SemiBold'),url(https://fonts.gstatic.com/s/titilliumweb/v5/anMUvcNT0H1YN4FII8wpr4e2tK5W43RXgBRKkM4A5Qg.ttf) format('truetype')}div.wpcf7{margin:0;padding:0}div.wpcf7-response-output{margin:2em .5em 1em;padding:.2em 1em}div.wpcf7 .screen-reader-response{position:absolute;overflow:hidden;clip:rect(1px,1px,1px,1px);height:1px;width:1px;margin:0;padding:0;border:0}.wpcf7-form-control-wrap{position:relative}.wpcf7-display-none{display:none}@charset "UTF-8";h2,p{font-family:"Titillium Web",sans-serif;color:#8a785e}br,p{line-height:22px}a,h2,h4,p{color:#8a785e}h2,header .subtitulo p{text-transform:uppercase}a.boxclose{float:right}a,body,html{margin:0;padding:0}a{text-decoration:none}figure,footer,header,section{display:block}a,body,html{padding:0}input{border:0}a,input{background:0 0}b,body,div,em,figure,footer,form,h1,h2,h3,h4,header,html,img,label,p,section,span{margin:0;padding:0;border:0;outline:0;font-size:100%;vertical-align:baseline;background:0 0}a{font-size:100%;vertical-align:baseline}.btn-slim,input{vertical-align:middle}h2,h3{font-weight:400;text-align:center}a img{border:none}@font-face{font-family:CharlemagneStd;src:url(http://osoutrosterraqueos.com/wp-content/themes/oot/dist/fonts/CharlemagneStd-Bold.ttf) format("truetype"),url(http://osoutrosterraqueos.com/wp-content/themes/oot/dist/fonts/CharlemagneStd-Bold.otf) format("truetype"),url(http://osoutrosterraqueos.com/wp-content/themes/oot/dist/fonts/CharlemagneStd-Regular.otf) format("truetype")}h2{font-size:20px;letter-spacing:4.1px}h3{font-size:16px;color:#817a6a;margin-bottom:50px}h4{font-size:24px;font-weight:300;text-align:center}p{font-size:16px;font-weight:400;text-align:center;margin-bottom:20px}@media (min-width:768px){h1{font-size:40px;margin-bottom:90px}h2{font-size:20px}h3{font-size:18px;line-height:20px}h4{font-size:24px}p{font-size:18px}}@media only screen and (max-width:640px){h4,p{text-align:center;color:#8a785e}h2{font-size:18px;letter-spacing:normal}h3{font-size:16px;line-height:18px;font-weight:400;text-align:center;color:#817a6a;margin-bottom:30px}h4{font-size:20px;font-weight:300}p{font-family:"Titillium Web",sans-serif;font-size:18px;font-weight:400;margin:20px 0;line-height:20px}br{line-height:22px}}.btn,body{color:#FFF;font-family:"Titillium Web",sans-serif}body{font-size:16px;-webkit-font-smoothing:antialiased;margin:0 auto;-webkit-box-sizing:content-box;box-sizing:content-box;line-height:1}.container{padding:0;margin:0;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-flow:row wrap;flex-flow:row wrap;-ms-flex-pack:distribute;justify-content:space-around}.col,form{-webkit-box-orient:vertical;-webkit-box-direction:normal}.footer,.header,.section{-webkit-box-flex:1;-ms-flex:1 100%;flex:1 100%}.section{padding:50px 60px;-webkit-box-sizing:content-box;box-sizing:content-box}.row{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-pack:distribute;justify-content:space-around}.btn,.separator,figure img{display:block}.col{-ms-flex-direction:col;flex-direction:col}.col-lg-one-half{-webkit-box-flex:1;-ms-flex:1 50%;flex:1 50%;margin-bottom:40px}.col-lg-one-third{-webkit-box-flex:1;-ms-flex:1;flex:1;margin:0 10px}.col-lg-four-fourth,.col-lg-one-fourth{-webkit-box-flex:1;-ms-flex:1 auto;flex:1 auto}.responsive-image{max-width:100%;width:100%;max-height:100%;height:100%}@media (max-width:768px){.section{-webkit-box-sizing:content-box;box-sizing:content-box;padding:50px 30px}.col-lg-one-third{margin:0 10px}}@media only screen and (max-width:640px){.row{-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-flow:column wrap;flex-flow:column wrap}.col-lg-one-half{-webkit-box-flex:inherit;-ms-flex:inherit;flex:inherit}.col-lg-one-third{margin:10px 10px 50px}.section{padding:50px 20px}}.separator{width:43px;height:2px;background-color:#54d154;margin:10px auto}.btn{width:auto;background:#54d154;border-radius:30px;font-weight:300;text-align:center;text-decoration:none;margin:0 auto}.btn-big{font-size:21px;line-height:21px;padding:15px 60px}.btn-medium{font-size:18px;line-height:22px;padding:10px 40px;min-width:185px;max-width:200px;width:80%}.btn-slim{font-size:16px;line-height:36px;height:36px;width:180px;position:absolute;bottom:0;left:50%;margin-left:-90px}form,header{position:relative}.btn-slim span{vertical-align:sub}a.boxclose{color:#8a785e;display:inline-block;font-size:31px;font-weight:700;line-height:0;margin-right:-6px;margin-top:-3px;padding:11px 3px}.boxclose:before{content:"×"}@media only screen and (max-width:640px){.btn-big{width:100px;padding:12px 50px}.btn-medium{padding:10px;margin:20px auto;max-width:206px;display:block;width:100%}.btn-slim{width:100%;max-width:200px}}@media only screen and (max-width:480px){.btn-big{width:100px;padding:12px 50px}}form{-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-ms-flex-direction:column;flex-direction:column}form label{display:block}form :-moz-placeholder,form :-ms-input-placeholder,form ::-moz-placeholder,form ::-webkit-input-placeholder{padding-left:16px;font-family:"Titillium Web",sans-serif;font-size:16px}form input:not([type=submit]),form textarea{border:1px solid rgba(138,120,94,.3);height:47px;margin-bottom:8px;width:100%;-webkit-box-sizing:border-box;box-sizing:border-box;padding-left:16px;color:#8a785e;font-size:16px;font-family:"Titillium Web",sans-serif}form textarea{height:175px}@-moz-document url-prefix(){span.myarrow::after{content:url(http://osoutrosterraqueos.com/wp-content/themes/oot/dist/imgs/setinha-drop.svg);margin-left:-30px;padding:.1em;cursor:pointer}}header{background:url(http://osoutrosterraqueos.com/wp-content/themes/oot/dist/imgs/header-background-min.png) fixed #FFF;background-size:cover;height:100vh;width:100%;text-align:center}header .tracinho{width:42px;height:5px;background-color:#54d154;top:0;display:block;position:absolute;left:50%;margin-left:-21px}header .titulo{margin-bottom:110px}header .titulo h1{display:none;opacity:0;font-size:0;text-indent:-99999}header .titulo figure img{max-width:280px;display:block;margin:20px auto}header .subtitulo{margin-bottom:85px}header .subtitulo p{font-family:CharlemagneStd,serif;font-size:33.7px;line-height:40px;text-align:center;color:#FFF;font-weight:700;width:70%;margin:0 auto}header figure.scroll-wrap{position:absolute;left:0;right:0;bottom:5%;display:block;margin-top:10px}header figure.scroll-wrap img{margin:0 auto}@media only screen and (max-width:640px){header{padding:20px;text-align:center}header .titulo{margin-bottom:30px;margin-top:0}header .subtitulo{margin-bottom:15px}header .subtitulo p{font-size:20px;line-height:30px;font-weight:700;width:100%;margin:0 auto}header figure{margin-top:15px}}@media only screen and (max-width:480px){header{padding:20px;text-align:center}header .titulo{margin-bottom:50px;margin-top:0}header .subtitulo{margin-bottom:40px}header .subtitulo p{font-size:20px;line-height:30px;font-weight:700;width:100%;margin:0 auto}header figure{margin-top:15px}}.sinopse{background-color:#edead9}.sinopse .col-lg-one-half figure{max-width:481px;margin:0 auto;display:block}.sinopse p{margin:0 auto}.sinopse .icons-text{width:66px;height:15px;margin:20px auto;display:block}.footer,.footer .col-4 a,.footer h2{color:#645328}.download-options .box,.download-options .box-long,.download-options .box-mini{border-radius:10px;background-color:#f6f5ec;page-break-after:0 30px;margin:0 auto;display:block}.download-options .download-info{position:relative;height:100%;min-height:380px}.download-options .download-info p{max-width:264px;margin:20px auto 44px}.download-options .box-mini{width:142px;height:142px;border-radius:30px;position:relative;margin-bottom:25px}.download-options .box-mini img{position:absolute;top:50%;left:50%;margin-right:-50%;-webkit-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}.download-options .box-mini img.EPUBico{width:77px}.download-options .box-long{padding:20px}.download-options .box-long.download-form{display:none}.download-options .box-long h4{font-size:16px;text-align:left;margin-bottom:20px}.download-options .box-long .link{display:block;margin:40px auto 0;text-align:center;text-decoration:underline}.about{background-color:#edead9}.about figure{-webkit-box-shadow:inset -.9px 2.9px 60px 0 rgba(72,72,72,.71);box-shadow:inset -.9px 2.9px 60px 0 rgba(72,72,72,.71);border:4px solid rgba(104,77,60,.25);max-width:240px;min-width:200px;margin:0 64px 20px 0;float:right}.about p{text-align:left;width:600px}.contato form{min-width:584px;width:100%;margin:0 auto}@media only screen and (max-width:768px){.about p{width:100%}}@media only screen and (max-width:640px){.about p,.contato form,.sinopse p{width:100%}.download-options .col-lg-one-third{padding:20px}.about figure{min-width:200px;width:100%;margin:0 auto;float:none}.contato form{max-width:100%;min-width:inherit}}.footer{padding:0;background-color:#b09c69}.footer h2{font-size:15px;margin-top:38px}.footer .separator{background-color:#645328}.footer .col-4{margin:20px auto 60px}.footer .col-4 .redes{display:inline-block}.footer .col-4 .redes .FB,.footer .col-4 .redes .Instagram,.footer .col-4 .redes .Twitter,.footer .col-4 .redes .logo-Skoob_20141124124148jpg{-o-object-fit:contain;object-fit:contain}.footer .bottom-dark{background:#322e25;color:#817a6a;height:auto}.footer .bottom-dark p{margin:7px;font-size:14px}.footer .bottom-dark p a{color:#b09c69}@media only screen and (max-width:480px){.footer .col-4{margin:20px auto}.footer .bottom-dark{height:auto}}@font-face{font-family:dashicons;src:url(../fonts/dashicons.eot)}.dashicons{display:inline-block;width:20px;height:20px;font-size:20px;line-height:1;font-family:dashicons;text-decoration:inherit;font-weight:400;font-style:normal;vertical-align:top;text-align:center;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.dashicons-external:before{content:"\f504"}</style>
        <?php wp_head(); ?>
        <style>
            .sr #sinopse figure, .sr .download-info figure, .sr #about figure{
                visibility: hidden;
            }

        </style>
        <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KD787DG');</script>
<!-- End Google Tag Manager -->
    </head>
    <body <?php body_class() ?>>
        <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KD787DG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
        <?php // <body> closes in footer.php ?>

        <div class="container">
            <header class="header">
                <span class="tracinho"></span>
                <div class="row">
                    <div class="col">

                        <div class="titulo">
                            <h1>Os Outros Terráqueos</h1>
                            <figure>
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/imgs/logo.png" alt="Os Outros Terráqueos" class="responsive-image" />
                            </figure>
                        </div>
                        <div class="subtitulo">
                            <p>E se, depois de 17 anos, você descobrisse que nasceu na Terra errada?</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <a href="#download-options" class="btn btn-big">Leia agora</a>

                    </div>
                </div>
                <figure class="scroll-wrap"><img src="<?php echo get_stylesheet_directory_uri() ?>/dist/imgs/scroll.svg" class="scroll" width="12" height="17" /></figure>
            </header>
