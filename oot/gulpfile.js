var gulp = require('gulp'),
        sass = require('gulp-sass'),
        concat = require('gulp-concat'),
        minifyCSS = require('gulp-minify-css'),
        rename = require('gulp-rename'),
        plumber = require('gulp-plumber'),
        watch = require('gulp-watch'),
        autoprefixer = require('gulp-autoprefixer'),
        jshint = require('gulp-jshint'),
        imagemin = require('gulp-imagemin'),
        livereload = require('gulp-livereload');




var gulpPaths = {
    sass: 'dist/scss/',
    cssDist: 'dist/css/'
};

gulp.task('sass', function () {
    gulp.src(gulpPaths.sass + '**/*.scss')
            .pipe(plumber())
            .pipe(sass())
            .pipe(gulp.dest(gulpPaths.cssDist))
            .pipe(autoprefixer({
                browsers: ['last 2 versions', 'ie 8', 'ie 9', '> 1%'],
                cascade: false
            }))
            .pipe(concat(gulpPaths.cssDist + 'style.css'))
            .pipe(gulp.dest('./'))
            .pipe(minifyCSS())
            .pipe(rename('style.min.css'))
            .pipe(gulp.dest(gulpPaths.cssDist))
            .pipe(livereload());
});

gulp.task('js', function () {

    gulp.src('dist/js/main.js')

            .pipe(jshint())

            .pipe(jshint.reporter('fail'))

            .pipe(concat('theme.js'))

            .pipe(gulp.dest('dist/js'));

});
gulp.task('img', function () {

    gulp.src('assets/img/*.{png,jpg,gif}')

            .pipe(imagemin({

                optimizationLevel: 7,

                progressive: true

            }))

            .pipe(gulp.dest('dist/img/'));

});



// Static server
//gulp.task('browser-sync', function () {
//    browserSync.init({
//        server: {
//            baseDir: "./"
//        }
//    });
//});

gulp.task('default', ['sass', 'js', 'img', 'watch']);

gulp.task('watch', function () {
    livereload.listen();

    gulp.watch(['dist/scss/**/*.scss'], ['sass']);

    gulp.watch('dist/js/main.js', ['js']);

    gulp.watch('dist/img/**/*.{png,jpg,gif}', ['img']);

});
