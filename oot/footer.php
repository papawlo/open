<?php
/**
 * Footer file common to all
 * templates
 *
 */
?>


<footer class="footer">

    <div class="row">
        <div class="col">
            <h2>OOT nas Redes</h2>
            <span class="separator"></span>
        </div>
    </div>

    <div class="row">
        <!-- <div class="col"> -->
        <div class="col-4">
            <a href="<?php the_field('link_do_facebook', 'option') ?>" target="_blank">
                <figure class="redes">
                    <!-- svg -->

                    <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/svgs/fb.svg" class="FB">
                </figure>
                <span>/sagaoot</span>
            </a>
        </div>
        <div class="col-4">
            <a href="<?php the_field('link_do_instagram', 'option') ?>" target="_blank">
                <figure class="redes">
                    <!-- png -->

                    <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/svgs/instagram.svg" class="Instagram">
                </figure>
                <span>@sagaoot</span>
            </a>
        </div>
        <div class="col-4">
            <a href="<?php the_field('link_do_twitter', 'option') ?>" target="_blank">
                <figure class="redes">
          
                    <!-- svg -->

                    <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/svgs/twitter.svg" class="Twitter">
                </figure>
                <span>@sagaoot</span>
            </a>
        </div>
        <div class="col-4">
            <a href="<?php the_field('link_do_skoob', 'option') ?>" target="_blank">
                <figure class="redes">
                    <!-- png -->
                    <img
                        src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/pngs/logo-skoob-20141124124148-jpg.png"
                        srcset="<?php echo get_stylesheet_directory_uri() ?>/dist/images/pngs/logo-skoob-20141124124148-jpg@2x.png 2x,
                        <?php echo get_stylesheet_directory_uri() ?>/dist/images/pngs/logo-skoob-20141124124148-jpg@3x.png 3x" class="logo-Skoob_20141124124148jpg" alt="Skoob icon">

                </figure>

            </a>
        </div>
        <!-- </div> -->
    </div>

    <div class="row bottom-dark">
        <p class="feito-por col">
            Feito com carinho por:
            <a href="#">DRN comunicação</a> e <a href="http://www.camilawaz.com" target="_blank">Camila Waz <span style="color:red;">♥</span></a>
        </p>
    </div>

</footer>
</div>


<link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,300i,400,600" rel="stylesheet">
<?php wp_footer(); ?>

<?php // </body> opens in header.php ?>
</body>
</html>
