$(function() {

  $('.bt-launchEditor').bind('click', function(event) {
    event.preventDefault();
    return launchEditor('editableimage1', $(this).attr('rel'));
  });

  /* Commons */
  $("form").validate({
    meta: "validate",
    errorPlacement: function(error, element) {
      element.parents('.control-group').addClass('error');
      //error.insertAfter(element);
      element.parents('.controls').append(error);
    },
    success: function(label) {
      label.parents('.control-group').removeClass('error');
      label.parents('.control-group').find('span.help-inline').remove();
    },
    errorElement: 'span',
    errorClass: 'help-inline'
  });

  $(".money").maskMoney({symbol:'R$ ', showSymbol:true, thousands:'.', decimal:',', symbolStay: false});
  $('#cep').mask('99999-999');

  var config_basic = {
  toolbar: [
    ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', 'Table', '-', 'Link', 'Unlink'],  ['UIColor']
  ]
  };

  $('.ckeditor_basic').ckeditor(config_basic);
  $('.ckeditor_full').ckeditor();

  if ( $('.bt-delete').size() ) {
    $('.bt-delete').bind('click', function (event) {
      event.preventDefault();
      var bt = $(this);

      $('#deleteConfirm').modal();
      $('#deleteConfirm').modal('show');

      $('#deleteConfirm').find('.bt-confirmar').live('click', function() {
        if ( bt.attr('href') ) {
          location.href = bt.attr('href');
        } else {
          return true;
        }
        
      });
    });
  }

  if ( $('.confirmation').size() ) {
    $('.confirmation').bind('click', function (event) {
      event.preventDefault();
      var bt = $(this);

      $('#modalConfirmation').modal();
      $('#modalConfirmation').modal('show');

      $('#modalConfirmation').find('.bt-confirmar').bind('click', function() {
        //location.href = bt.attr('href');
        return true;
      });
    });
  }

  if ( $('.bt-removeImage').size() ) {
    $('.bt-removeImage').bind('click', function(event) {
      event.preventDefault();
      var bt = $(this);

      $('#deleteConfirm').modal();
      $('#deleteConfirm').modal('show');

      $('#deleteConfirm').find('.bt-confirmar').bind('click', function() {
        //location.href = bt.attr('href');
        bt.parents('.control-group').fadeOut(400, function() {
          $(this).remove();
          $('#deleteConfirm').modal('hide')
        });
      });
    })
  }

});

function getCidadesAdmin( estado, cidade, url ) {
  jQuery.ajax({
    url: url,
    type: 'POST',
    data: {estado: estado, cidade: cidade},
    success: function(data, textStatus, xhr) {
      $('.cmb_cidade').attr('disabled', false).html(data);
    }
  });
  
}

function getCepAdmin(cep, url) {

  jQuery.ajax({
    url: base_url+'/extras/cep/buscar.php',
    type: 'GET',
    data: {cep: cep},
    success: function(data, textStatus, xhr) {

      $(data).find('webservicecep').each(function(){
        var $resultado       = $(this).find('resultado').text();
        var $tipo_logradouro = $(this).find('tipo_logradouro').text();
        var $logradouro      = $(this).find('logradouro').text();
        var $bairro          = $(this).find('bairro').text();
        var $cidade          = $(this).find('cidade').text();
        var $uf              = $(this).find('uf').text();

        if ($resultado == 1) {
          $('#endereco').val($tipo_logradouro+' '+$logradouro);
          $('#bairro').val($bairro);
          $('#estado').val($uf);
          getCidadesAdmin( $uf, $cidade, url );
        }
      });
    }
  });

}

function validateDadosAdmin(obj, acao, dado) {

  jQuery.ajax({
    url: 'validate/',
    type: 'POST',
    data: {acao: acao, dado: dado},
    success: function(data, textStatus, xhr) {
      //console.log(data);
      if (data == 'invalid') {
        obj.parents('.control-group').addClass('error');
        obj.parent().append('<span class="help-inline">JÃ¡ existe um cadastro com esse endereÃ§o.</span>');
      } else {
        obj.parents('.control-group').removeClass('error');
        obj.parent().find('.help-inline').remove();
      }
    }
  });
  

}