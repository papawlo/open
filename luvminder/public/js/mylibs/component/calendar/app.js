$(document).ready(function() {

    "use strict";

    var options = {
        events_url: base_url + 'index/events',
        view: 'month',
        tmpl_path: 'index/',
        day: '2013-03-12',
        language: 'pt-BR',
        holidays: {
            '08-03': 'International Women\'s Day',
            '25-12': 'Christmas\'s',
            '01-05': "International labor day"
        },
        first_day: 2
//        onAfterEventsLoad: function(events) {
//            if (!events) {
//                return;
//            }
//            var list = $('#eventlist');
//            list.html('');
//
//            $.each(events, function(key, val) {
//                $(document.createElement('li'))
//                        .html('<a href="' + val.url + '">' + val.title + '</a>')
//                        .appendTo(list);
//            });
//        },
//        onAfterViewLoad: function(view) {
//            $('.page-header h3').text(this.getTitle());
//            $('.btn-group button').removeClass('active');
//            $('button[data-calendar-view="' + view + '"]').addClass('active');
//        },
//        classes: {
//            months: {
//                general: 'label'
//            }
//        }
    };

//    var calendar = $('#calendar').calendar(options);
    var calendar = $('#calendar').calendar({
        events_url: base_url + 'index/events',
        view: 'month',
        tmpl_path: 'index/',
        language: 'pt-BR',
        onAfterEventsLoad: function(events) {
            if (!events) {
                return;
            }
            var list = $('#eventlist');
            list.html('');

            $.each(events, function(key, val) {
                $(document.createElement('li'))
                        .html('<a href="' + val.url + '" data-original-title="' + val.title + '" rel="tooltip" data-cal-view="day" data-cal-date="' + val.date + '">' + val.title + '</a>')
                        .appendTo(list);
            });
        },
        onAfterViewLoad: function(view) {
            $('.page-header h3').text(this.getTitle());
            $('.btn-group button').removeClass('active');
            $('button[data-calendar-view="' + view + '"]').addClass('active');
        },
        classes: {
            months: {
                general: 'label'
            }
        }
    });
    $('.btn-group button[data-calendar-nav]').each(function() {
        var $this = $(this);
        $this.click(function() {
            calendar.navigate($this.data('calendar-nav'));
        });
    });

    $('.btn-group button[data-calendar-view]').each(function() {
        var $this = $(this);
        $this.click(function() {
            calendar.view($this.data('calendar-view'));
        });
    });


}(jQuery));