<?php

class Admin_CampaignController extends App_Controller_Admin {

    public function initialize() {
        $this->model = new Model_Campaign();
        $this->view->pageTitle = 'Campanha';
        $this->view->pageTitleList = 'Listagem';
        $this->view->advertiser_id = $this->_getParam('advertiser', false);
    }

    protected function indexAction() {
        $advertiser_id = $this->_getParam('advertiser', false);
        $page = $this->_getParam('page', 1);
        $per_page = $this->_getParam('per_page', 20);
        $select = $this->select->where('advertiser_id = ?', $advertiser_id);

        $this->view->paginator = $this->paginator($this->model->fetchAll($select), $per_page, $page);

        $this->view->messages = $this->_helper->flashMessenger->getMessages();
        $this->view->advertiser = $advertiser_id;
    }

    protected function visualizarAction() {
        $this->_helper->layout->disableLayout();

        $id = $this->_request->getParam('id', 0);

        $rowSet = $this->model->find($id);
        $row = $rowSet->current();

        $this->view->row = $row;
    }

    protected function beforeSave(&$row, $action) {

        $row->data_inicio = implode("-", array_reverse(explode("/", $row->data_inicio)));
        $row->data_fim = implode("-", array_reverse(explode("/", $row->data_fim)));
    }

    protected function afterSave(&$row, $action) {
        $this->params = array("advertiser" => $row->advertiser_id);
    }

}

