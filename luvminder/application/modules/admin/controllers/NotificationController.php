<?php

class Admin_NotificationController extends App_Controller_Admin {

    public function initialize() {
        $this->model = new Model_Notification();
        $this->view->pageTitle = 'Notificação';
        $this->view->pageTitleList = 'Listagem';
        $this->view->pageTitleNew = 'Novo';
        $this->view->pageTitleEdit = 'Editar';
    }

    protected function indexAction() {
        $relationship_id = $this->_getParam('relationship_id', false);
        $user_id = $this->_getParam('user_id', false);

        $this->view->paginator = array();
        if ($relationship_id) {
            $page = $this->_getParam('page', 1);
            $per_page = $this->_getParam('per_page', 20);

            $relationshipModel = new Model_Relationship();
            $relationshipRowset = $relationshipModel->find($relationship_id);
            $relationshipRow = $relationshipRowset->current();

            $notificationRowset = $relationshipRow->getNotifications();

            $this->view->paginator = $this->paginator($notificationRowset, $per_page, $page);
        }
        if ($user_id) {
            $page = $this->_getParam('page', 1);
            $per_page = $this->_getParam('per_page', 20);

            $userModel = new Model_Users();
            $userRowset = $userModel->find($user_id);
            $userRow = $userRowset->current();

            $notificationRowset = $userRow->getNotificationsByUser();

            $this->view->paginator = $this->paginator($notificationRowset, $per_page, $page);
        }



        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }

    protected function visualizarAction() {
        $this->_helper->layout->disableLayout();

        $id = $this->_request->getParam('id', 0);

        $rowSet = $this->model->find($id);
        $row = $rowSet->current();

        $this->view->row = $row;
    }

    protected function afterSave(&$row, $action) {
        $this->params = array("reminder_id" => $row->reminder_id);
    }

}

