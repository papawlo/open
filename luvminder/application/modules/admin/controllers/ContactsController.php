<?php

class Admin_ContactsController extends App_Controller_Admin {

    public function initialize() {
        $this->model = new Model_Contacts();
        $this->view->pageTitle = 'Contatos';
        $this->view->pageTitleList = 'Listagem';
    }

    protected function indexAction() {
        $user_id = $this->_getParam('user_id', false);
        $this->view->paginator = array();
        if ($user_id) {
            $page = $this->_getParam('page', 1);
            $per_page = $this->_getParam('per_page', 20);

            $userModel = new Model_Users();
            $usersRowset = $userModel->find($user_id);
            $user = $usersRowset->current();

            $contactsRowset = $user->findManyToManyRowset('Model_Contacts', 'Model_UsersHasContacts');

            $this->view->paginator = $this->paginator($contactsRowset, $per_page, $page);
        }


        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }

    protected function visualizarAction() {
        $this->_helper->layout->disableLayout();

        $id = $this->_request->getParam('id', 0);

        $rowSet = $this->model->find($id);
        $row = $rowSet->current();

        $this->view->row = $row;
    }

}

