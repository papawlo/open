<?php

class Admin_ReminderController extends App_Controller_Admin {

    public function initialize() {
        $this->model = new Model_Reminder();
        $this->view->pageTitle = 'Lembrete';
        $this->view->pageTitleList = 'Listagem';
        $this->view->pageTitleNew = 'Novo';
        $this->view->pageTitleEdit = 'Editar';
    }

    protected function indexAction() {
     $relationship_id = $this->_getParam('relationship_id', false);
     $user_id = $this->_getParam('user_id', false);

     $this->view->paginator = array();
     if ($relationship_id) {
        $page = $this->_getParam('page', 1);
        $per_page = $this->_getParam('per_page', 20);

        $relationshipModel = new Model_Relationship();
        $relationshipRowset = $relationshipModel->find($relationship_id);
        $relationshipRow = $relationshipRowset->current();

        $reminderRowset = $relationshipRow->getReminders();

        $this->view->paginator = $this->paginator($reminderRowset, $per_page, $page);
    }
    if ($user_id) {
        $page = $this->_getParam('page', 1);
        $per_page = $this->_getParam('per_page', 20);

        $userModel = new Model_Users();
        $userRowset = $userModel->find($user_id);
        $userRow = $userRowset->current();

        $reminderRowset = $userRow->getRemindersByUser();

        $this->view->paginator = $this->paginator($reminderRowset, $per_page, $page);
    }


    $this->view->messages = $this->_helper->flashMessenger->getMessages();
}

protected function visualizarAction() {
    $this->_helper->layout->disableLayout();

    $id = $this->_request->getParam('id', 0);

    $rowSet = $this->model->find($id);
    $row = $rowSet->current();

    $this->view->row = $row;
}

protected function afterSave(&$row, $action) {
    $this->params = array("relationship_id" => $row->relationship_id);
}

protected function disparoAction(){
   $page = $this->_getParam('page', 1);
   $per_page = $this->_getParam('per_page', 20);
   list($dia_de_hoje,$mes_de_hoje, $ano_de_hoje) = explode("-",date('d-m-Y'));

    // $sql="
    // SELECT * FROM reminder
    // WHERE (`repetir` = 0 AND `data_dia_notification` = 6 AND `data_mes_notification` = 12 AND `data_ano_notification` = 2013)
    // OR (`repetir` = 1 AND `data_dia_notification` = 6 AND `repeticao` = 'm')
    // OR (`repetir` = 1 AND `data_dia_notification` = 6 AND `data_mes_notification` = 12 AND `repeticao` = 'A')
    // ";
   $selectReminder = $this->model->select()
   ->setIntegrityCheck(false)
   ->from(array('rm' => 'reminder'))
   ->joinLeft(array('rl' => 'relationship'), 'rl.id = rm.relationship_id')
   ->joinLeft(array('u' => 'users'), 'u.id = rl.users_id')
   ->where("rl.status= ?", 'ativo')
   ->where("`rm`.`repetir`=0 AND `rm`.`data_dia_notification` =$dia_de_hoje AND `rm`.`data_mes_notification` =$mes_de_hoje AND `rm`.`data_ano_notification` = $ano_de_hoje AND `rm`.`status`='ativo'")
   ->orWhere("`rm`.`repetir`=1 AND `rm`.`data_dia_notification` = $dia_de_hoje AND `rm`.`repeticao` = 'm' AND `rm`.`status`='ativo'")
   ->orWhere("`rm`.`repetir` = 1 AND `rm`.`data_dia_notification` = $dia_de_hoje  AND `rm`.`data_mes_notification` = $mes_de_hoje AND `rm`.`repeticao` = 'a' AND `rm`.`status`='ativo'");

   $this->view->paginator = $this->paginator($this->model->fetchAll($selectReminder), $per_page, $page);
}
}

