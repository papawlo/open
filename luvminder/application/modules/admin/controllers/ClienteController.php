<?php

class Admin_ClienteController extends App_Controller_Admin {

    public function initialize() {
        $this->model = new Model_Usuario();
        $this->view->pageTitle = 'Cliente';
        $this->view->pageTitleList = 'Listagem';
        $this->view->pageTitleNew = 'Novo';
        $this->view->pageTitleEdit = 'Editar';

        $modelEstado = new Model_Estado();
        $this->view->estados = $modelEstado->fetchAll();
        $this->view->logradouros = array('Aeroporto', 'Alameda', 'Área', 'Avenida', 'Campo', 'Chácara', 'Colônia', 'Condomínio', 'Conjunto', 'Distrito', 'Esplanada', 'Estação', 'Estrada', 'Favela', 'Fazenda', 'Feira', 'Jardim', 'Ladeira', 'Lago', 'Lagoa', 'Largo', 'Loteamento', 'Morro', 'Núcleo', 'Parque', 'Passarela', 'Pátio', 'Praça', 'Quadra', 'Recanto', 'Residencial', 'Rodovia', 'Rua', 'Setor', 'Sítio', 'Travessa', 'Trecho', 'Trevo', 'Vale', 'Vereda', 'Via', 'Viaduto', 'Viela', 'Vila');
    }

    protected function indexAction() {

        $page = $this->_getParam('page', 1);
        $per_page = $this->_getParam('per_page', 20);

        $rows = $this->model->fetchAll($this->select);

        $this->view->paginator = $this->paginator($rows, $per_page, $page);

        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }

    protected function visualizarAction() {
        $this->_helper->layout->disableLayout();

        $id = $this->_request->getParam('id', 0);

        $rowSet = $this->model->find($id);
        $row = $rowSet->current();

        $_model = new Model_DadosUsuario();
        $rowSetDadousuario = $_model->fetchRow("id_usuario = $row->id_usuario");

        $rowDadoUsuario = $_model->findWithJoins($rowSetDadousuario->id_dados_usuario);

        $this->view->row = $row;
        $this->view->rowDadoUsuario = $rowDadoUsuario;
    }

    protected function defineSelect() {
        $this->select->where('role IN (?)', array('cliente'));
    }

    protected function beforeSave(&$row, $action) {
        $dados = $this->getPost();



        if ($action == 'new') {
            $row->senha = ($dados['senha']) ? md5($dados['senha']) : md5('123456');
            $row->role = 'cliente';
            $row->id_role = 3; //TODO id cliente
            $row->data_cadastro = date('Y-m-d H:i:s');
            $row->status = 'ativo';
        } else {
            $userAtual = $this->model->find($row->id_usuario)->current();
            $senhaAtual = $userAtual->senha;

            $row->senha = ($dados['senha']) ? md5($dados['senha']) : $senhaAtual;
        }

        if (!$dados['login']) {
            $row->login = $dados['email'];
        }
    }

    public function afterSave(&$row, $action) {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $dados = $this->getPost();

            try {
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->addValidator('Count', false, 1)
                        ->addValidator('Size', false, array('max' => "2MB"))
                        ->addValidator('Extension', false, 'jpg,png,gif')
                        ->addValidator('IsImage', false);

                $files = $adapter->getFileInfo();

                foreach ($files as $fieldname => $fileinfo) {
                    if (($adapter->isUploaded($fileinfo["name"])) && ($adapter->isValid($fileinfo['name']))) {
                        $mediaUpload = new App_Helpers_Media($fileinfo);
                        $dados['foto'] = $mediaUpload->sendFile($this->_request->getControllerName());
                        if ($dados['foto']) {

//                            $modelMedia = new Model_Media();
//                            $modelMedia->removeImagem($row->old_id_media);
                        }
                    }
                }
            } catch (Exception $ex) {
                $this->addFlash("error" , $ex->getMessage());
            }

            $modelDadosUsuario = new Model_DadosUsuario();
            $modelEnderecos = new Model_Enderecos();

            if ($action == 'new') {
                $rowDadoUsuario = $modelDadosUsuario->createRow();

                $dados['id_usuario'] = $row->id_usuario;
            } else {
                $rowDadoUsuario = $this->model->getDados($row->id_usuario);
            }

            //parada do endereço #TENSO
            if (isset($dados['endereco']) && !empty($dados['endereco'])) {

                $enderecoRow = $modelEnderecos->find($dados['endereco']);
            } else {
                $enderecoRow = $modelEnderecos->createRow();

                $estadoModel = new Model_Estado ();
                $enderecoRow->uf = $estadoModel->find($dados['estado'])->current()->uf;

                $enderecoRow->cidade_id = $dados['cidade'];
                $enderecoRow->nomeslog = $dados['endereco_input'];
                $enderecoRow->nomeclog = $dados['logradouro'] . " " . $dados['endereco_input'];
                if (!$dados['bairro']) {

                    $bairroModel = new Model_Bairro();
                    $bairroRow = $bairroModel->createRow();
                    $bairroRow->nome = $dados['bairro_input'];
                    $bairroRow->uf = $estadoModel->find($dados['estado'])->current()->uf;
                    $bairroRow->cidade = $dados['cidade'];
                    $enderecoRow->bairro_id = $bairroRow->save();
                } else {

                    $enderecoRow->bairro_id = $dados['bairro'];
                }

                $enderecoRow->cep = $dados['cep'];
                $enderecoRow->logradouro = $dados['logradouro'];
                $enderecoRow->uf_cod = $dados['estado'];
                $enderecoRow->logracompl = $dados['logradouro'] . " " . $dados['endereco_input'];

                $rowDadoUsuario->endereco = $enderecoRow->save();
            }
            $rowDadoUsuario->setFromArray($dados);
            $rowDadoUsuario->save();
        }
    }

    public function validateAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $dados = $this->getPost();

        if ($dados) {

            $modelEstabelecimento = new Model_Usuario();

            switch ($dados['acao']) {

                case 'email':

                    $select = $modelEstabelecimento->select()->where('email = ?', $dados['dado'])
                            ->where('status != ?', 'deletado')
                            ->where('role IN (?)', 'cliente');

                    $result = $modelEstabelecimento->fetchAll($select);

                    $rowCount = count($result);

                    if ($rowCount > 0) {
                        echo 'invalid';
                    } else {
                        echo 'valid';
                    }

                    break;

                case 'login':

                    $select = $modelEstabelecimento->select()->where('login = ?', $dados['dado'])
                            ->where('status != ?', 'deletado')
                            ->where('role IN (?)', 'cliente');

                    $result = $modelEstabelecimento->fetchAll($select);

                    $rowCount = count($result);

                    if ($rowCount > 0) {
                        echo 'invalid';
                    } else {
                        echo 'valid';
                    }

                    break;
            }
        }
    }

    public function cidadesAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $dados = $this->_request->getPost();

        if ($dados) {
            $modelCidade = new Model_Cidade();

            $result = $modelCidade->getByEstado($dados['estado']);

            $html = '<option value="">Selecione uma Cidade</option>';

            foreach ($result as $item) {
                $sel = '';
                $cidade = $item->nome;
                if ($item->id_cidade == $dados['cidade']) {
                    $sel = ' selected="selected"';
                }

                $html .= '<option value="' . $item->id_cidade . '"' . $sel . '>' . $cidade . '</option>';
            }

            echo $html;
        }
    }

    public function bairrosAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $dados = $this->_request->getPost();

        if ($dados) {
            $modelBairro = new Model_Bairro();

            $result = $modelBairro->getByCidade($dados['cidade']);

            if (count($result)) {
                $html = '<option value="">Selecione um Bairro</option>';

                foreach ($result as $item) {
                    $sel = '';
                    $bairro = $item->nome;
                    if ($item->id_bairro == $dados['bairro']) {
                        $sel = ' selected="selected"';
                    }

                    $html .= '<option value="' . $item->id_bairro . '"' . $sel . '>' . $bairro . '</option>';
                }
            } else {
                return false;
                exit;
            }



            echo $html;
        }
    }

    public function cepAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $cep = $this->_request->getParam('cep');

        if ($cep) {
            $modelEndereco = new Model_Enderecos();

            $result = $modelEndereco->findCep($cep);


            $this->_helper->json($result, array('enableJsonExprFinder' => true));
        }
    }

}

