<?php

class Admin_RoleController extends App_Controller_Admin {

    public function initialize() {
        $this->model = new Model_Role();
        $this->view->pageTitle = 'Roles';
        $this->view->pageTitleList = 'Listagem';
        $this->view->pageTitleNew = 'Novo';
        $this->view->pageTitleEdit = 'Editar';

        $modelRole = new Model_Role();
    }


    protected function viewAction(){
        $this->_helper->layout->disableLayout();
        $id = $this->_getParam('id');
        if ($id) {
            $row = $this->model->fetchRow($id);

            $this->view->row = $row;
        }
    }

    protected function beforeSave(&$row, $action) { }

}