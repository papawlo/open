<?php

class Zend_View_Helper_shownotifications extends Zend_View_Helper_Abstract {

    public function showNotifications($relationship_id) {
        $relationshipModel = new Model_Relationship();
        $notificationsRowset = $relationshipModel->find($relationship_id);
        $notificationRow = $notificationsRowset->current();
        
        return $notificationRow->getNotifications();
    }

}

