<?php

class User_View_Helper_Getreminders extends Zend_View_Helper_Abstract {

    public function getReminders($id) {
        $relationshipModel = new Model_Relationship();
        $remindersRowset = $relationshipModel->find($id);
        $reminderRow = $remindersRowset->current();
        return $reminderRow->getRemindersAtivo();
    }

}

