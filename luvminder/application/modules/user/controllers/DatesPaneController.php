<?php

class User_DatesPaneController extends App_Controller_User {

    public function initialize() {
        $this->model = new Model_Relationship();
        $this->view->pageTitle = 'Minhas Datas';
        $this->view->pageTitleList = 'Listagem';
        $this->view->pageTitleNew = 'Novo';
        $this->view->pageTitleEdit = 'Editar';
    }

    protected function indexAction() {
        $this->view->pageRel = 'dates_pane';
        $auth = Zend_Auth::getInstance();

        $info = $auth->getStorage()->read();

//        unset($_SESSION["token"]);exit;

        if ($info->id) {
            $userModel = new Model_Users();
            $usersRowset = $userModel->find($info->id);
            $userRow = $usersRowset->current();
            // provider like twitter, linkedin, do not provide the user email
            // in this case, we should ask them to complete their profile before continuing
            if (!$userRow->email) {
                $flash = $this->_helper->getHelper('FlashMessenger');
                $flash->addMessage("Informe seu email para continuar.");

                $this->_helper->redirector('index', 'settings', 'user');
            }

            $this->view->rows = $userRow->getRelationshipsAtivo();
            $this->view->tipos = array("1º encontro", "1º beijo", "1ª vez", "Início de namoro", "Início do noivado", "Dia do casamento", "Outros [Digitar]");
        }
    }

    protected function beforeSave(&$row, $action) {
        $filters = array(
            '*' => 'StringTrim',
            'antecedencia' => 'Digits'
        );
        $validators = array(
            'titulo' => array('allowEmpty' => false),
            'com' => array('allowEmpty' => false),
            'data_inicio' => array(
                'allowEmpty' => false
            ),
            'genero' => array(
                new Zend_Validate_InArray(
                        array(
                    'f' => 'f',
                    'm' => 'm'
                        )
                )
            ),
            'antecedencia' => array('digits',
                new Zend_Validate_Digits(),
                'messages' => array(
                    'A antecedencia deve conter apenas digitos'
                )
            )
        );
        $options = array(
            'notEmptyMessage' => "O campo '%%field%%' deve ser preenchido adequadamente"
        );

        $params = $this->getRequest()->getParams();
        $input = new Zend_Filter_Input($filters, $validators, $params, $options);
        if (!$input->isValid()) {


            foreach ($input->getMessages() as $campo => $messages) {
                $campo = str_replace('data_inicio', "Data de Início", $campo);
                $campo = str_replace('titulo', "Título", $campo);
                $campo = str_replace('com', "Parceiro(a)", $campo);
                $campo = str_replace('antecedencia', "antecedencia", $campo);
                foreach ($messages as $key => $message) {
                    $message = str_replace('%data_inicio%', "Data de Início", $message);
                    $message = str_replace('%titulo%', "Título", $message);
                    $message = str_replace('%com%', "Parceiro(a)", $message);
                    $message = str_replace('%antecedencia%', "antecedencia", $message);
                    $retorno = array("tipo" => "error", "tipo_msg" => "Ocorreu um erro no campo $campo!!", "msg_info" => $message);
                }
            }

            $this->_helper->json($retorno);
            exit;
        }

        $auth = Zend_Auth::getInstance();

        $info = $auth->getStorage()->read();


        $row->users_id = $info->id;
//        $data_formatada = str_replace('/', '-', $this->getRequest()->getPost('data_inicio', null));
//        $row->data_inicio = $mysqldate = date('Y-m-d H:i:s', strtotime($data_formatada));
        $data_explodida = explode(' de ', $this->getRequest()->getPost('data_inicio', null));
        // print_r($data_explodida);exit;
        $dia = trim($data_explodida[0]);

        $mes = trim($data_explodida[1]);

        $ano = trim($data_explodida[2]);

        $meses = array(1 => 'janeiro', 2 => 'fevereiro', 3 => 'março', 4 => 'abril', 5 => 'maio', 5 => 'junho',
            7 => 'julho', 8 => 'agosto', 9 => 'setembro', 10 => 'outubro', 11 => 'novembro', 12 => 'dezembro');
        $mes = array_search($mes, $meses);

        $row->data_inicio = date('Y-m-d H:i:s', strtotime($ano . '-' . $mes . '-' . $dia));
        /*  notifications */
        $notification_email = $this->getRequest()->getPost('notification_email', 0);
        if ($notification_email) {
            $row->notification_email = 1;
        } else {
            $row->notification_email = 0;
        }
    }

    protected function afterSave(&$row, $action) {

        $reminder_ids = $this->getRequest()->getPost('reminder_id', null);
        $repeticaos = $this->getRequest()->getPost('repeticao', null);
        $periodo_tempos = $this->getRequest()->getPost('periodo_tempo', null);
        $lembrars = $this->getRequest()->getPost('lembrar', null);

        $reminderModel = new Model_Reminder;

        foreach ($reminder_ids as $key => $reminder_id) {

            if (empty($reminder_id)) {
                $reminderRow = $reminderModel->createRow();
//                $reminderRow->lembrar = $lembrars[$key];
//                $reminderRow->repeticao = $repeticaos[$key] ? $repeticaos[$key] : '';
//                $reminderRow->repetir = $repeticaos[$key] ? 1 : 0;
//                $reminderRow->periodo_tempo = $periodo_tempos[$key];
                $reminderRow->relationship_id = $row->id;

                $periodo_tempo = '';
                if ($lembrars[$key]) {

                    switch ($periodo_tempos[$key]) {
                        case 1:
                            $periodo_tempo = '-1 day';
                            $reminderRow->antecedencia_quantidade = 1;
                            $reminderRow->antecedencia_periodo_tempo = 'd';
                            break;
                        case 2:
                            $periodo_tempo = '-2 day';
                            $reminderRow->antecedencia_quantidade = 2;
                            $reminderRow->antecedencia_periodo_tempo = 'd';
                            break;
                        case 3:
                            $periodo_tempo = '-3 day';
                            $reminderRow->antecedencia_quantidade = 3;
                            $reminderRow->antecedencia_periodo_tempo = 'd';
                            break;
                        case 4:
                            $periodo_tempo = '-4 day';
                            $reminderRow->antecedencia_quantidade = 4;
                            $reminderRow->antecedencia_periodo_tempo = 'd';
                            break;
                        case 5:
                            $periodo_tempo = '-5 day';
                            $reminderRow->antecedencia_quantidade = 5;
                            $reminderRow->antecedencia_periodo_tempo = 'd';
                            break;
                        case 6:
                            $periodo_tempo = '-6 day';
                            $reminderRow->antecedencia_quantidade = 6;
                            $reminderRow->antecedencia_periodo_tempo = 'd';
                            break;
                        case 7:
                            $periodo_tempo = '-1 week';
                            $reminderRow->antecedencia_quantidade = 1;
                            $reminderRow->antecedencia_periodo_tempo = 's';
                            break;
                        case 8:
                            $periodo_tempo = '-2 weeks';
                            $reminderRow->antecedencia_quantidade = 2;
                            $reminderRow->antecedencia_periodo_tempo = 's';
                            break;
                        case 9:
                            $periodo_tempo = '-3 weeks';
                            $reminderRow->antecedencia_quantidade = 3;
                            $reminderRow->antecedencia_periodo_tempo = 's';
                            break;
                        case 10:
                            $periodo_tempo = '-1 month';
                            $reminderRow->antecedencia_quantidade = 1;
                            $reminderRow->antecedencia_periodo_tempo = 'm';
                            break;
                        case 11:

                        default:
                            break;
                    }
                    $reminderRow->antecedencia = 1;
                } else {
                    $reminderRow->antecedencia = 0;
                    $reminderRow->antecedencia_quantidade = NULL;
                    $reminderRow->antecedencia_periodo_tempo = NULL;
                }

                if ($repeticaos[$key]) {
                    $reminderRow->repetir = 1;
                    $reminderRow->repeticao = $repeticaos[$key];
                } else {
                    $reminderRow->repetir = 0;
                    $reminderRow->repeticao = NULL;
                }



                $Date = strtotime($row->data_inicio);

                $data_notificacao = date('d-m-Y', strtotime("-$reminderRow->antecedencia $periodo_tempo", $Date));

                list($dia_notificacao, $mes_notificacao, $ano_notificacao) = explode("-", $data_notificacao);

                if ($reminderRow->repetir) {
                    switch ($reminderRow->repeticao) {

                        case 'm':
                            $reminderRow->data_dia_notification = $dia_notificacao;
                            $reminderRow->data_mes_notification = NULL;
                            $reminderRow->data_ano_notification = NULL;
                            break;
                        case 'a':
                            $reminderRow->data_dia_notification = $dia_notificacao;
                            $reminderRow->data_mes_notification = $mes_notificacao;
                            $reminderRow->data_ano_notification = NULL;
                            break;
                        default:
                            break;
                    }
                } else {

                    $reminderRow->data_dia_notification = $dia_notificacao;
                    $reminderRow->data_mes_notification = $mes_notificacao;

                    //compara se se deve adicionar um lembrete este ano ou no proximo
                    if (strtotime(date('Y') . '-' . $mes_notificacao . '-' . $dia_notificacao) < time()) {
                        $reminderRow->data_ano_notification = date('Y') + 1;
                    } else {
                        $reminderRow->data_ano_notification = date('Y');
                    }
                }
                
//                print_r($reminderRow);exit;


                return  array('reminderId' => $reminderRow->save());
            } else {
                $reminderRow = $reminderModel->find($reminder_id)->current();

                $periodo_tempo = '';
                if ($lembrars[$key]) {

                    switch ($periodo_tempos[$key]) {
                        case 1:
                            $periodo_tempo = '-1 day';
                            $reminderRow->antecedencia_quantidade = 1;
                            $reminderRow->antecedencia_periodo_tempo = 'd';
                            break;
                        case 2:
                            $periodo_tempo = '-2 day';
                            $reminderRow->antecedencia_quantidade = 2;
                            $reminderRow->antecedencia_periodo_tempo = 'd';
                            break;
                        case 3:
                            $periodo_tempo = '-3 day';
                            $reminderRow->antecedencia_quantidade = 3;
                            $reminderRow->antecedencia_periodo_tempo = 'd';
                            break;
                        case 4:
                            $periodo_tempo = '-4 day';
                            $reminderRow->antecedencia_quantidade = 4;
                            $reminderRow->antecedencia_periodo_tempo = 'd';
                            break;
                        case 5:
                            $periodo_tempo = '-5 day';
                            $reminderRow->antecedencia_quantidade = 5;
                            $reminderRow->antecedencia_periodo_tempo = 'd';
                            break;
                        case 6:
                            $periodo_tempo = '-6 day';
                            $reminderRow->antecedencia_quantidade = 6;
                            $reminderRow->antecedencia_periodo_tempo = 'd';
                            break;
                        case 7:
                            $periodo_tempo = '-1 week';
                            $reminderRow->antecedencia_quantidade = 1;
                            $reminderRow->antecedencia_periodo_tempo = 's';
                            break;
                        case 8:
                            $periodo_tempo = '-2 weeks';
                            $reminderRow->antecedencia_quantidade = 2;
                            $reminderRow->antecedencia_periodo_tempo = 's';
                            break;
                        case 9:
                            $periodo_tempo = '-3 weeks';
                            $reminderRow->antecedencia_quantidade = 3;
                            $reminderRow->antecedencia_periodo_tempo = 's';
                            break;
                        case 10:
                            $periodo_tempo = '-1 month';
                            $reminderRow->antecedencia_quantidade = 1;
                            $reminderRow->antecedencia_periodo_tempo = 'm';
                            break;
                        case 11:

                        default:
                            break;
                    }
                    $reminderRow->antecedencia = 1;
                } else {
                    $reminderRow->antecedencia = 0;
                    $reminderRow->antecedencia_quantidade = NULL;
                    $reminderRow->antecedencia_periodo_tempo = NUll;
                }

                if ($repeticaos[$key]) {
                    $reminderRow->repetir = 1;
                    $reminderRow->repeticao = $repeticaos[$key];
                } else {
                    $reminderRow->repetir = 0;
                    $reminderRow->repeticao = NULL;
                }




                $Date = strtotime($row->data_inicio);

                $data_notificacao = date('d-m-Y', strtotime($periodo_tempo, $Date));

                list($dia_notificacao, $mes_notificacao, $ano_notificacao) = explode("-", $data_notificacao);

                if ($reminderRow->repetir) {
                    switch ($reminderRow->repeticao) {

                        case 'm':
                            $reminderRow->data_dia_notification = $dia_notificacao;
                            $reminderRow->data_mes_notification = NULL;
                            $reminderRow->data_ano_notification = NULL;
                            break;
                        case 'a':
                            $reminderRow->data_dia_notification = $dia_notificacao;
                            $reminderRow->data_mes_notification = $mes_notificacao;
                            $reminderRow->data_ano_notification = NULL;
                            break;
                        default:
                            break;
                    }
                } else {

                    $reminderRow->data_dia_notification = $dia_notificacao;
                    $reminderRow->data_mes_notification = $mes_notificacao;

                    //compara se se deve adicionar um lembrete este ano ou no proximo
                    if (strtotime(date('Y') . '-' . $mes_notificacao . '-' . $dia_notificacao) < time()) {
                        $reminderRow->data_ano_notification = date('Y') + 1;
                    } else {
                        $reminderRow->data_ano_notification = date('Y');
                    }
                }


                return $reminderRow->save();
            }
        }
        
    }

    public function addDateFormAction() {
        $this->_helper->layout->disableLayout();

        $this->view->tipos = array("1º encontro", "1º beijo", "1ª vez", "Início de namoro", "Início do noivado", "Dia do casamento", "Outros [Digitar]");
    }

    public function comAutocompleteAction() {
// disable view and layout, we want some fanzy json returned
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $auth = Zend_Auth::getInstance();
        $info = $auth->getStorage()->read();
        $user_id = $info->id;

        $relationshipModel = new Model_Relationship();
        $select = $relationshipModel->select()->distinct()->from($relationshipModel, "com")->where("status = ?", 'ativo')->where("users_id = ?", $user_id);
        $comRow = $relationshipModel->fetchAll($select)->toArray();

        $buscadoRow2 = array();
        if ($comRow) {
            foreach ($comRow as $value) {
                $buscadoRowArray[] = $value['com'];
            }
            $buscadoRow2 = array_values($buscadoRowArray);
        }


        $valuesJson = Zend_Json::encode($buscadoRow2);
        echo $valuesJson;
    }

}
