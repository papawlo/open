<?php

error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', -1);

class CronController extends App_Controller_Controller {

    public function notificationTestAction() {

        $this->_helper->layout->disableLayout();

        $this->_helper->viewRenderer->setNoRender(true);

//envia sms pro estabelecimento
        $reminderModel = new Model_Reminder();
        list($dia_de_hoje,$mes_de_hoje, $ano_de_hoje) = explode("-",date('d-m-Y'));

//pega todos os lembretes ativos do dia
        $selectReminder = $reminderModel->select()
        ->setIntegrityCheck(false)
        ->from(array('rm' => 'reminder'))
        ->joinLeft(array('rl' => 'relationship'), 'rl.id = rm.relationship_id')
        ->joinLeft(array('u' => 'users'), 'u.id = rl.users_id')
        ->where("rl.status= ?", 'ativo')
        ->where("`rm`.`repetir`=0 AND `rm`.`data_dia_notification` =$dia_de_hoje AND `rm`.`data_mes_notification` =$mes_de_hoje AND `rm`.`data_ano_notification` = $ano_de_hoje AND `rm`.`status`='ativo'")
        ->orWhere("`rm`.`repetir`=1 AND `rm`.`data_dia_notification` = $dia_de_hoje AND `rm`.`repeticao` = 'm' AND `rm`.`status`='ativo'")
        ->orWhere("`rm`.`repetir` = 1 AND `rm`.`data_dia_notification` = $dia_de_hoje  AND `rm`.`data_mes_notification` = $mes_de_hoje AND `rm`.`repeticao` = 'a' AND `rm`.`status`='ativo'");

        $remindersRows = $reminderModel->fetchAll($selectReminder);
        // print_r($remindersRows->toArray());
//      dispara as notificações por destino
        foreach ($remindersRows as $reminderRow) {
            if ($reminderRow->notification_email) {
                $this->disparaEmail($reminderRow);
            }
            // if ($reminderRow->notification_facebook) {
            //     $this->disparaFacebook();
            // }
            // if ($reminderRow->notification_twitter) {
            //     $this->disparaTwitter();
            // }
            // if ($reminderRow->notification_gplus) {
            //     $this->disparaGplus();
            // }
        }
    }

    public function disparaFacebook($uid, $template, $href) {

        try {
            require_once( APPLICATION_PATH . "/../hybridauth/Hybrid/thirdparty/Facebook/facebook.php" );
            $fbConfig = array(
                'appId' => '196762330479218',
                'secret' => '85c23b7ea317bd6fbeb8d1c984cdc969'
                );
            $facebook = new Facebook($fbConfig);

            $parameters["access_token"] = "196762330479218|85c23b7ea317bd6fbeb8d1c984cdc969";
            $parameters["href"] = $href;
            $parameters["template"] = $template;

            try {
                $facebook->api("/$uid/notifications", "post", $parameters);
            } catch (FacebookApiException $e) {
                throw new Exception("Notification failed!  returned an error: $e");
            }
        } catch (Exception $e) {
            throw new Exception("Notification failed! Returned an error: $e");
        }
    }

    public function notification1Action() {
        $this->_helper->layout->disableLayout();

        $this->_helper->viewRenderer->setNoRender(true);

        $notification_FB_message = 'The message you want to send as notfication';
        $notification_FB_app_link = '?notification=test';


        $this->disparaFacebook('100000582267035', $notification_FB_message, $notification_FB_app_link);
    }

    public function disparaTwitter($screen_name, $text) {

        require_once(APPLICATION_PATH . "/../twitteroauth/twitteroauth.php"); //Path to twitteroauth library


        $consumer_key = 'gqTghm6K4EiRMUr988iDSg';
        $consumer_secret = 'skczlXQPuwjpOLfm1sAJTx2nCATseg8941CieWL0';
        $oauth_token = '1618224662-ggoKcrcTpUfFI0PSoHLJBH6CgRN8USHn9tRx1Uj';
        $oauth_token_secret = '1rMnFK7Rue8pvNa1HeF1LVJ9hYK1zI1mlvfbTnrBjg';


        try {
// Initialize the connection
            $connection = new TwitterOAuth($consumer_key, $consumer_secret, $oauth_token, $oauth_token_secret);

// Send a direct message
            $parameters = array("screen_name" => $screen_name, "text" => $text);
            $connection->post('direct_messages/new', $parameters);
        } catch (Exception $e) {
            throw new Exception("Notification failed! Returned an error: $e");
        }
    }

    function notificationAction() {

        $this->_helper->layout->disableLayout();

        $this->_helper->viewRenderer->setNoRender(true);
        $this->disparaGPlus();
    }

    function disparaGPlus() {
//        session_destroy();
//        session_unset();
//        $auth = Zend_Auth::getInstance();
//        $info = $auth->getStorage()->read();
        $user_id = 2; //$info->id;

        $googleCalendarAccessModel = new Model_GoogleCalendarAccess();
        $googleCalendarAccessRow = $googleCalendarAccessModel->fetchRow($googleCalendarAccessModel->select()->where("users_id=?", $user_id)->limit(1));

//        print_r($googleCalendarAccessRow);exit;
        $token = json_decode($googleCalendarAccessRow->token);
//        print_r($token->access_token);exit;

        require_once 'google-api-php-client/src/Google_Client.php';
        require_once 'google-api-php-client/src/contrib/Google_CalendarService.php';

//        session_start();

        $client = new Google_Client(array('use_objects' => true));
        $client->setApplicationName("Google Calendar PHP Starter Application");

// Visit https://code.google.com/apis/console?api=calendar to generate your
// client id, client secret, and to register your redirect uri.
        $client->setClientId('761533968443.apps.googleusercontent.com');
        $client->setClientSecret('7Wa7aU9U7YD7EIfcwFDJHJS7');
        $client->setDeveloperKey('AIzaSyBn4IXHmwEJuRpc_K9cBMAaHwqUIXIU6u8');
        $client->setAccessType('offline');


        $service = new Google_CalendarService($client);
        // If Access Token Expired (uses Google_OAuth2 class), refresh access token by refresh token
        if ($client->isAccessTokenExpired()) {
            $client->refreshToken($token->refresh_token);
        }


// If client got access token successfuly - perform operations
        $access_tokens = json_decode($client->getAccessToken());


        $oldToken = json_decode($googleCalendarAccessRow->token);
        $oldToken->access_token = $access_tokens->access_token;
        $oldToken->expires_in = $access_tokens->expires_in;
        $oldToken->created = $access_tokens->created;
        if ($access_tokens) {
            // Update refreshToken and save data if refresh token is received (logged in now)
            if (isset($access_tokens->refresh_token)) {
                // Store in DB refresh token - $access_tokens->refresh_token
                $oldToken->refresh_token = $access_tokens->refresh_token;
            }
        }
        $googleCalendarAccessRow->token = json_encode($oldToken);
        $googleCalendarAccessRow->save();
        //{"access_token":"ya29.AHES6ZTY-dwfWMg1S3NZiQslg8QhO9XRHFBxB1quy0zQ1G0","token_type":"Bearer","expires_in":3600,"refresh_token":"1\/un_epR3m8Oh3AtP7avqQwK53XOgm-9POJ__BfojLinU","created":1375878672,"token":"ya29.AHES6ZRlIudzwL8l2VXw2JXOjpdsiklDUab53NB7b_Vb

        if ($client->getAccessToken()) {

            $calendarId = $googleCalendarAccessRow->calendar_id;
            $calendar = $service->calendars->get($calendarId);

            $time_zone = $calendar->getTimeZone();

//            NOVO EVENTO
            $event = new Google_Event();
            $event->setSummary('Aniversário de Noivado');
            $start = new Google_EventDateTime();
            $start->setTimeZone($time_zone);
            $start->setDateTime('2013-08-07T14:50:00');
            $event->setStart($start);
            $end = new Google_EventDateTime();
            $end->setTimeZone($time_zone);
            $end->setDateTime('2013-08-07T15:39:00');
            $event->setEnd($end);

            $createdEvent = $service->events->insert($calendarId, $event);


            $_SESSION['token'] = $client->getAccessToken();
        } else {

            print "Não tem acesso ao token";
        }
    }

    public function disparaEmail($destinatario) {
        // print_r($destinatario);exit;


        $html = "";
        $html .= '<p>Olá Luver!</p>

        <p>Gostariamos de lembrar essa data</p>
        <h2>'.$destinatario->titulo.'</h2>
        Luvmider<br />
        http://www.luvminder.com/</p>

        <p>Para suporte por favor entre em contato pelo suporte@luvminder.com</p>';

        try {
            $mail = new Zend_Mail();
            $config = array(
                'auth' => 'login',
                'port' => 587,
                'username' => 'mailer@digitarget.com.br',
                'password' => 'sGbYnJE6',
                'ssl' => 'tls'
                );

            $transport = new Zend_Mail_Transport_Smtp('server.digitarget.com.br', $config);

            $mail->SetFrom('mailer@digitarget.com.br', 'Mailer Luvminder');

            $mail->addTo($destinatario->email, $destinatario->first_name);
            $mail->setSubject("[Luvminder] Lembrete");
            $mail->setBodyHtml(utf8_decode($html));

            if($mail->send($transport)){
                echo "enviado: $destinatario->email<br>\n";
            }else{
                echo "error: $destinatario->email<br>\n";
            }
            //fazer log de envio
        } catch (Exception $e) {
            //fazer log
            die($e->getMessage());
        }
    }

    // Check if Access token is expired and get new one using Refresh token
    function checkToken($client) {
        if ($client->isAccessTokenExpired()) {
            $client->refreshToken($refresh_token);
        }
    }

}