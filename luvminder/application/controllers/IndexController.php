<?php

class IndexController extends App_Controller_Controller {

    public function initialize() {
        
    }

    public function indexAction() {
        $this->view->pageRel = 'index';
    }

    public function homeAction() {
        $this->view->pageRel = 'home';
        $this->view->description = 'O Luvminder é isso aí';
        $this->view->keywords = 'some, keywords, comma separated';
        $this->view->section = 'Home';
    }

    public function signinAction() {
        $this->view->pageRel = 'signin';
        $ajax = $this->isXmlHttpRequest();
        if ($ajax) {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
        }

        if ($this->_request->isPost()) {
            $userModel = new Model_Users();
            $email = $this->_request->getPost('email');
            $password = $this->_request->getPost('password');
            $timezone = $this->_request->getPost('timezone');
//            $first_name = $this->_request->getPost('first_name');
//            $last_name = $this->_request->getPost('last_name');

            if (!$email || !$password) {
                if ($ajax) {
                    $return = Zend_Json::encode(array("error" =>"1","msg"=> "Email e password são necessários."));
                    echo $return;
                    exit;
                }
                $this->view->mensagens = array("error" => "Email e password são necessários.");
            } else {
                // check if email is in use?
                $select = $userModel->select()
                        ->where('email = ?', $email)
                        ->where('status= ?', 'ativo');

                $userRow = $userModel->fetchRow($select);
                // if email used on users table, we display an error
                if ($userRow) {
                    if ($ajax) {
                        $return = Zend_Json::encode(array("error" => "1", "msg" => "Email já existe com outra conta."));
                        echo $return;
                        exit;
                    }
                    $this->view->messages = array("error" => "Email já existe com outra conta.");
                } else {
                    // create new user
                    $newUserRow = $userModel->createRow();
                    $newUserRow->email = $email;
//                    $newUserRow->first_name = $first_name;
//                    $newUserRow->last_name = $last_name;
//                    $newUserRow->display_name = $first_name;
                    $newUserRow->password = md5($password);
                    $newUserRow->created_at = date('Y-m-d H:i:s');
                    $newUserRow->timezone = $timezone;
                    $newUserRow->com_code = sha1( uniqid() );
                    $newUserRow->verified = 'n';
                    $newUserRow->status = 'ativo';
                    $new_user_id = $newUserRow->save();


                    // set user connected
                    $result = Model_Users::login($email, md5($password));
                    if ($result) {
                        // 2.2 - redirect to user/profile
                        if ($ajax) {
                            $return = Zend_Json::encode(array('success' => '1'));
                            echo $return;
                            exit;
                        }
                        $this->_helper->redirector('index', 'dates-pane', 'user');
                    } else {
                        if ($ajax) {
                            $return = Zend_Json::encode(array('error' => '1', 'msg' => 'Não foi possível efeutar o login, tente novamente'));
                            echo $return;
                            exit;
                        }
                        $this->view->mensagens = array("error" => "Email e password incorretos.");
                    }
                }
            }
        }
    }

    function authenticatewithAction() {

        // set on application.config.php
        // load hybridauth base file, change the following paths if necessary 
        // note: in your application you probably have to include these only when required.

        $hybridauth_config = APPLICATION_PATH . "/../hybridauth/config.php";

        require_once( APPLICATION_PATH . "/../hybridauth/Hybrid/Auth.php" );

        $provider = $this->_request->getParam('provider', null);
        $error = "";
        try {
            // create an instance for Hybridauth with the configuration file path as parameter
            $hybridauth = new Hybrid_Auth($hybridauth_config);

            // try to authenticate the selected $provider
            $adapter = $hybridauth->authenticate($provider);

            // grab the user profile
            $user_profile = $adapter->getUserProfile();

            // load user and authentication models, we will need them...
            $authenticationModel = new Model_Authentications;

            $userModel = new Model_Users;

            # 1 - check if user already have authenticated using this provider before
            $authentication_info = $authenticationModel->find_by_provider_uid($provider, $user_profile->identifier);

            # 2 - if authentication exists in the database, then we set the user as connected and redirect him to his profile page
            if ($authentication_info) {
                // 2.1 - store user_id in session
//                $userRow = $userModel->find($authentication_info->users_id)->current();
                $result = Model_Users::loginProvider($provider, $user_profile->identifier);

                if ($result) {
                    // get the stored hybridauth data from your storage system
//                    $hybridauth_session_data = $authenticationModel->get_sotred_hybridauth_session($current_user_id);
                    // then call Hybrid_Auth::restoreSessionData() to get stored data
//                    $hybridauth->restoreSessionData($hybridauth_session_data, $provider);
                    // 2.2 - redirect to user/dates-pane
                    $this->_helper->redirector('index', 'dates-pane', 'user');
                } else {
                    //setar exceptions
                    die('não autenticou');
                }
            }

            # 3 - else, here lets check if the user email we got from the provider already exists in our database ( for this example the email is UNIQUE for each user )
            // if authentication does not exist, but the email address returned  by the provider does exist in database, 
            // then we tell the user that the email  is already in use 
            // but, its up to you if you want to associate the authentication with the user having the adresse email in the database
            if ($user_profile->email) {
                $user_info = $userModel->find_by_email($user_profile->email);

                if ($user_info) {
                    die('<br /><b style="color:red">Bem! O e-mail (' . $user_profile->email . ') retornado pelo provedor já existe em nosso banco de dados, portanto, neste caso, você pode usar o seu email e senha para efetuar o <a href="login">Login</a> .</b>');
                }
            }

            # 4 - if authentication does not exist and email is not in use, then we create a new user 
            $provider_uid = $user_profile->identifier;
            $email = $user_profile->email;
            $first_name = $user_profile->firstName;
            $last_name = $user_profile->lastName;
            $display_name = $user_profile->displayName;
            $website_url = $user_profile->webSiteURL;
            $profile_url = $user_profile->profileURL;
            $foto_url = $user_profile->photoURL;
            $gender = $user_profile->gender;
            $birthDay = $user_profile->birthDay;
            $birthMonth = $user_profile->birthMonth;
            $birthYear = $user_profile->birthYear;
            $endereco = $user_profile->address;
            $pais = $user_profile->country;
            $cep = $user_profile->zip;
            $cidade = $user_profile->city;
            $region = $user_profile->region;
            $idioma = $user_profile->language;
            if ($endereco) {
                $cidadeEstadoPais = $this->getAddressByName($endereco);
            } else if ($region) {
                $cidadeEstadoPais = $this->getAddressByName($region);
            }
            if ($gender == 'male') {
                $genero = 'm';
            }
            if ($gender == 'female') {
                $genero = 'f';
            }

            $userRow = $userModel->createRow();
            $newUserArray = array(
                'email' => $email,
                'password' => rand(), # for the password we generate something random
                'display_name' => $display_name,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'gender' => $genero,
                'foto_url' => $foto_url,
                'data_nascimento' => $birthYear . '-' . $birthMonth . "-" . $birthDay,
                'region' => $region,
                'logradouro' => $cidadeEstadoPais['logradouro'],
                'numero' => $cidadeEstadoPais['numero'],
                'bairro' => $cidadeEstadoPais['bairro'],
                'cidade' => $cidade != "" ? $cidade : $cidadeEstadoPais['cidade'],
                'estado' => $cidadeEstadoPais['estado'],
                'pais' => $pais != "" ? $pais : $cidadeEstadoPais['pais'],
                'cep' => $cep != "" ? $cep : $cidadeEstadoPais['cep'],
                'lng' => $cidadeEstadoPais['lng'],
                'lat' => $cidadeEstadoPais['lat'],
                'created_at' => date('Y-m-d H:i:s')
            );

            // 4.1 - create new user
            $userRow->setFromArray($newUserArray);
            $new_user_id = $userRow->save();



            // 4.2 - creat a new authentication for him
//            $authenticationModel->create($new_user_id, $provider, $provider_uid, $email, $display_name, $first_name, $last_name, $profile_url, $website_url, $photo_url);
            $data = array(
                'users_id' => $new_user_id,
                'provider' => $provider,
                'provider_uid' => $provider_uid,
                'email' => $email,
                'display_name' => $display_name,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'profile_url' => $profile_url,
                'photo_url' => $foto_url,
                'website_url' => $website_url,
                'created_at' => date('Y-m-d H:i:s')
            );

            $newAuthenticationRow = $authenticationModel->createRow();
            // Set all the column values at once
            $newAuthenticationRow->setFromArray($data);

            $newAuthenticationRow->save();

            // 4.3 - store the new user_id in session
            // set user connected
            $result = Model_Users::loginProvider($newAuthenticationRow->provider, $newAuthenticationRow->provider_uid);
            if ($result) {
                // call Hybrid_Auth::getSessionData() to get stored data
//                $hybridauth_session_data = $hybridauth->getSessionData();
                // then store it on your database or something
//                sotre_hybridauth_session($current_user_id, $hybridauth_session_data, $provider);
                // 4.4 - redirect to user/date-pane
                $this->_helper->redirector('index', 'settings', 'user');
            } else {
                die('não autenticou 4.5');
            }
        } catch (Exception $e) {
            // Display the recived error
            switch ($e->getCode()) {
                case 0 : $error = "Erro não especificado.";
                    break;
                case 1 : $error = "Erro de configuração Hybriauth.";
                    break;
                case 2 : $error = "Provedor não configurado corretamente.";
                    break;
                case 3 : $error = "Provedor desconhecido ou desabilitado";
                    break;
                case 4 : $error = "Falta credenciais do provedor de aplicação.";
                    break;
                case 5 : $error = "Falha na autenticação. O usuário cancelou a autenticação ou o provedor recusou a conexão.";
                    break;
                case 6 : $error = "Solicitação de perfil de usuário falhou. O mais provável é que o usuário não esteja conectado ao provedor e ele deve se autenticar novamente.";
                    $adapter->logout();
                    break;
                case 7 : $error = "Usuário não conectado ao provedor";
                    $adapter->logout();
                    break;
            }

            // well, basically your should not display this to the end user, just give him a hint and move on..
            $error .= "<br /><br /><b>Original error message:</b> " . $e->getMessage();
            $error .= "<hr /><pre>Trace:<br />" . $e->getTraceAsString() . "</pre>";

            // load error view
            $this->view->message = $error;
            $this->render('error');
        }
    }

//    public function hybridauth() {
//        /* !
//         * HybridAuth
//         * http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
//         * (c) 2009-2012, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html 
//         */
//
//// ------------------------------------------------------------------------
////	HybridAuth End Point
//// ------------------------------------------------------------------------
//        echo "teste";exit;
//        require_once( APPLICATION_PATH . "/../hybridauth/Hybrid/Auth.php" );
//        require_once( APPLICATION_PATH . "/../hybridauth/Hybrid/Endpoint.php" );
//
//        Hybrid_Endpoint::process();
//    }

    public function loginAction() {
        $ajax = $this->isXmlHttpRequest();
        if ($ajax) {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
        }

        // check if a user is already logged
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $user = $auth->getStorage()->read();


            if (isset($user->role) && $user->role == 'user') {
                if ($ajax) {
                    $return = Zend_Json::encode(array('success' => '1'));
                    echo $return;
                    exit;
                }

                $this->_helper->FlashMessenger('Parece que você já efeutou o login =)');
                return $this->_redirect('user/index');
            }
        }



        if ($this->_request->isPost()) {

            $email = $this->_request->getPost('email');
            $password = $this->_request->getPost('password');
            if (empty($email) || empty($password)) {
                if ($ajax) {
                    $return = Zend_Json::encode(array('error' => '1', 'msg' => 'Preencha seu email e senha'));
                    echo $return;
                    exit;
                }
            }
            $this->view->email = $email;
            try {
                $result = Model_Users::login($email, md5($password));
              
                if ($result) {
                    if ($ajax) {
                        $return = Zend_Json::encode(array('success' => '1'));
                        echo $return;
                        exit;
                    }
                    $this->_helper->redirector('index', 'dates-pane', 'user');
                } else {
                    if ($ajax) {
                        $return = Zend_Json::encode(array('error' => '1', "msg" => 'Usuário ou senha incorreto'));
                        echo $return;
                        exit;
                    }
                    $this->view->messages = array("error" => '1', "msg" => "Usuário ou senha incorreto");
                }
            } catch (Exception $e) {
                $this->view->messages = array("error" => '1', "msg" => "Erro ao tentar fazer login");
            }
        }
    }

    public function logoutAction() {

        Zend_Auth::getInstance()->clearIdentity();
        Zend_Session::destroy(true);

        // config and whatnot
        $config = APPLICATION_PATH . "/../hybridauth/config.php";

        require_once( APPLICATION_PATH . "/../hybridauth/Hybrid/Auth.php" );


        try {
            $hybridauth = new Hybrid_Auth($config);

            // logout the user from $provider
            $hybridauth->logoutAllProviders();
        } catch (Exception $e) {
            echo "<br /><br /><b>Oh well, we got an error :</b> " . $e->getMessage();

            echo "<hr /><h3>Trace</h3> <pre>" . $e->getTraceAsString() . "</pre>";
            exit;
        }
        // return to login page
        return $this->_redirect('index');
    }

    // Get STATE from Google GeoData
    function getAddressByName($address) {
        $address = str_replace(" ", "+", "$address");
        $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";

        $result = file_get_contents("$url");
        $json = json_decode($result);
        $numero = $cep = $logradouro = $bairro = $city = $state = $country = "";
        foreach ($json->results as $result) {
            foreach ($result->address_components as $addressPart) {
                if (in_array('street_number', $addressPart->types))
                    $numero = $addressPart->long_name;
                else if (in_array('route', $addressPart->types))
                    $logradouro = $addressPart->long_name;
                else if ((in_array('sublocality', $addressPart->types)) && (in_array('political', $addressPart->types)))
                    $bairro = $addressPart->long_name;
                else if ((in_array('locality', $addressPart->types)) && (in_array('political', $addressPart->types)))
                    $city = $addressPart->long_name;
                else if ((in_array('administrative_area_level_1', $addressPart->types)) && (in_array('political', $addressPart->types)))
                    $state = $addressPart->long_name;
                else if ((in_array('country', $addressPart->types)) && (in_array('political', $addressPart->types)))
                    $country = $addressPart->short_name;
                else if (in_array('postal_code', $addressPart->types))
                    $cep = $addressPart->long_name;
            }


            $lat = $result->geometry->location->lat;

            $lng = $result->geometry->location->lng;
        }
        $endereco = array();

        $endereco['numero'] = $numero != '' ? $numero : "";
        $endereco['logradouro'] = $logradouro != '' ? $logradouro : "";
        $endereco['bairro'] = $bairro != '' ? $bairro : "";
        $endereco['cidade'] = $city != '' ? $city : "";
        $endereco['estado'] = $state != '' ? $state : "";
        $endereco['pais'] = $country != '' ? $country : "";
        $endereco['cep'] = $cep != '' ? $cep : "";
        $endereco['lat'] = $lat != '' ? $lat : "";
        $endereco['lng'] = $lng != '' ? $lng : "";


        return $endereco;
    }

}
