<?php

class Admin_PessoaController extends App_Controller_Admin {

    public function initialize() {
        $this->model = new Model_Pessoa();
        $this->view->pageTitle = 'Cadastros ';
        $this->view->pageTitleList = 'Listagem';

        $modelCategoria = new Model_Categoria();
		// $selectCategoria = $modelCategoria->select()->from(array('p' => 'pessoa'), array('c.id','c.nome'))
        //        ->joinLeft(array('c' => 'categoria'), 'p.categoria_id = c.id', array('qtd_pessoas' => 'COUNT(*)'))
        //        ->where('c.status = ?', 'ativo')
        //        ->group('c.nome')
        //        ->order(array('c.nome ASC'));

       

        //$_categorias = $modelCategoria->fetchAll($selectCategoria);
        $_categorias = $modelCategoria->fetchAll("status = 'ativo'");
		$stmt = $this->db->query("SELECT `c`.`id` as id , `c`.`nome`  as nome, COUNT( `p`.`id` ) AS `qtd_pessoas`
FROM `categoria` AS `c`
LEFT JOIN `pessoa` AS `p` ON p.categoria_id = c.id
WHERE c.status = 'ativo' AND p.status = 'ativo'
GROUP BY `c`.`id`
ORDER BY `c`.`nome` ASC");

        $stmt->setFetchMode(Zend_Db::FETCH_OBJ);

        $_categoriasComPessoas = $stmt->fetchAll();

        $tipo = $this->_getParam('tipo', null);
        $this->view->tipo = $tipo;

        $this->view->categorias = $_categorias;
		$this->view->categoriasComPessoas = $_categoriasComPessoas;

        $this->view->estados = array("AC" => "Acre", "AL" => "Alagoas", "AM" => "Amazonas", "AP" => "Amapá", "BA" => "Bahia", "CE" => "Ceará", "DF" => "Distrito Federal", "ES" => "Espírito Santo", "GO" => "Goiás", "MA" => "Maranhão", "MT" => "Mato Grosso", "MS" => "Mato Grosso do Sul", "MG" => "Minas Gerais", "PA" => "Pará", "PB" => "Paraíba", "PR" => "Paraná", "PE" => "Pernambuco", "PI" => "Piauí", "RJ" => "Rio de Janeiro", "RN" => "Rio Grande do Norte", "RO" => "Rondônia", "RS" => "Rio Grande do Sul", "RR" => "Roraima", "SC" => "Santa Catarina", "SE" => "Sergipe", "SP" => "São Paulo", "TO" => "Tocantins");
    }

    protected function indexAction() {
        $ajax = $this->isXmlHttpRequest();

        if ($ajax) {
            $this->_helper->layout->disableLayout();
        }

        $page = $this->_getParam('page', 1);
        $per_page = $this->_getParam('per_page', '20');
        $by = $this->_getParam('by', null);
        $order = $this->_getParam('order', null);
        $tipo = $this->_getParam('tipo', null);
        $categoria = $this->_getParam('categoria', null);
        $nome = $this->_getParam('nome', null);
        $params = array();

        $select = $this->select;

        if ($by && $order) {
            $select->order(array($by . ' ' . $order));
            $this->view->by = $by;
            $this->view->order = $order;
            $params["by"] = $by;
            $params["order"] = $order;
        }
        if ($categoria) {
            $select->where('categoria_id= ?', $categoria);
            $this->view->categoria = $categoria;
//            $this->params = array("categoria" => $categoria);
            $params["categoria"] = $categoria;
        }
        if ($tipo) {
            $select->where('tipo= ?', $tipo);
            $this->view->tipo = $tipo;
//            $this->params = array("tipo" => $tipo);
            $params['tipo'] = $tipo;
        }

        if (!empty($nome) && isset($nome)) {
            $select->where('nome LIKE ?', $nome . '%');
        }

        $this->view->search_params = $params;


        $this->view->paginator = $this->paginator($this->model->fetchAll($select), $per_page, $page);

        $this->view->messages = $this->_helper->flashMessenger->getMessages();
        if ($ajax) {
            $this->render('partial/pessoal-index', null, true);
        }
    }

    protected function visualizarAction() {
        $this->_helper->layout->disableLayout();

        $id = $this->_request->getParam('id', 0);

        $rowSet = $this->model->find($id);
        $row = $rowSet->current();

        $this->view->row = $row;
    }
	


    protected function beforeSave(&$row, $action) {
        $filters = array(
            '*' => 'StringTrim'
        );
        $validators = array(
            'nome' => array('allowEmpty' => false),
            'categoria_id' => array('allowEmpty' => false)            
        );
        $options = array(
            'notEmptyMessage' => "O campo '%%field%%' deve ser preenchido adequadamente"
        );

        $params = $this->getRequest()->getParams();
        $input = new Zend_Filter_Input($filters, $validators, $params, $options);
        if (!$input->isValid()) {
            foreach ($input->getMessages() as $campo => $messages) {
//                 $campo = str_replace('data_inicio', "Data de Início", $campo);
//                    $campo = str_replace('titulo', "Título", $campo);
//                    $campo = str_replace('com', "Parceiro(a)", $campo);
//                    $campo = str_replace('intervalo', "Intervalo", $campo);
                foreach ($messages as $key => $message) {
                    $message = str_replace('%nome%', "Nome", $message);                    
                    $message = str_replace('%categoria_id%', "Catgoria", $message);
//                    $message = str_replace('%com%', "Parceiro(a)", $message);
//                    $message = str_replace('%intervalo%', "Intervalo", $message);
//                    $retorno .= array("tipo" => "error", "tipo_msg" => "Ocorreu um erro no campo $campo!!", "msg_info" => $message);
                     $this->addFlash("error", $message);
                }
            }
//            $this->addFlash("error", $retorno);
            $this->_helper->redirector('index', 'pessoa', 'admin');
            exit;
        }


        $auth = Zend_Auth::getInstance();

        $info = $auth->getStorage()->read();
        if ($action == 'new') {
            $row->data_cadastro = date('Y-m-d H:i:s');
            $row->cadastrado_por = $info->id;
        }
        if ($action == 'edit') {
            $row->data_edicao = date('Y-m-d H:i:s');
            $row->editado_por = $info->id;
        }
    }

    public function validateAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $dados = $this->getPost();

        if ($dados) {

            $pessoaModel = new Model_Pessoa();

            switch ($dados['acao']) {

                case 'cpf_cnpj':

                    $select = $pessoaModel->select()->where('cpf_cnpj = ?', $dados['dado'])
                            ->where('status != ?', 'deletado');

                    $result = $pessoaModel->fetchAll($select);

                    $rowCount = count($result);

                    if ($rowCount > 0) {
                        echo 'invalid';
                    } else {
                        echo 'valid';
                    }

                    break;
                default : echo 'invalid';
            }
        }
    }
	
	protected function importarCsvAction() {


        $confirma = $this->_request->getParam('confirma', 0);
        $arquivo = APPLICATION_PATH . "/../deputado.csv";

        $row = 1;
        if (($handle = fopen($arquivo, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                echo "<p> $num fields in line $row: <br /></p>\n";
                for ($c = 0; $c < $num; $c++) {
                    echo $c . "-" . $data[$c] . " - ";
                }

                if ($row > 1) {
                    $pessoaModel = new Model_Pessoa();
                    $pessoaModel = $pessoaModel->createRow();
                    $pessoaModel->tipo = "pf";
                    $pessoaModel->nome = $data[9];
                    $pessoaModel->tratamento = $data[7];
                    $pessoaModel->profissao = 'deputado federal';
                    $pessoaModel->cargo = 'deputado federal';
                    $pessoaModel->endereco = $data[2] . ' ' . $data[3] . " " . $data[4].' '.$data[5];
					$pessoaModel->bairro = 'NULL';
                    list($cidade, $uf, $cep) = explode(" - ", $data[6]);
                    $pessoaModel->cidade = $cidade;
                    $pessoaModel->uf = $uf;
                    $pessoaModel->cep = substr($cep, -9);
                    $pessoaModel->categoria_id = 13;
					$pessoaModel->data_cadastro = date('Y-m-d H:i:s');
                    $pessoaModel->cadastrado_por = 1;
                    $pessoaModel->status = 'ativo';
                    if ($new_id = $pessoaModel->save()) {
                        echo "adicionado: " . $new_id;
                    }
                }
                echo "<br />\n";
                $row++;
            }
            fclose($handle);
        }
       

    }

    public function exportXlsAction() {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);



        $request = $this->getRequest();
        if ($request->isPost()) {
            $por = $this->_getParam('by', null);
            $order = $this->_getParam('order', null);
            $tipo = $this->_getParam('tipo', null);
            $categoria = $this->_getParam('categoria', null);
			$nome = $this->_getParam('nome', null);

            $select = $this->select;

            if ($por && $order) {
                $select->order(array($por . ' ' . $order));
                $this->view->por = $por;
                $this->view->order = $order;
            }

            if ($categoria) {
                $select->where('categoria_id= ?', $categoria);
                $this->view->categoria = $categoria;
            }
            if ($tipo) {
                $select->where('tipo= ?', $tipo);
                $this->view->tipo = $tipo;
            }
			
			if (!empty($nome) && isset($nome)) {
                $select->where('nome LIKE ?', $nome . '%');
            }

            $campos = $this->_getParam('campos', null);


            // Create new PHPExcel object
            $objPHPExcel = new PHPExcel();

            // Set properties
            $objPHPExcel->getProperties()->setCreator("Marcio Paulo");
            $objPHPExcel->getProperties()->setLastModifiedBy("Marcio Paulo");
            $objPHPExcel->getProperties()->setTitle("Lista de Pessoas para exportação");
            $objPHPExcel->getProperties()->setSubject("Lista de Pessoas Fundação Oswaldo Cruz");
//        $objPHPExcel->getProperties()->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.");
            // Add some data
            $data = $this->model->fetchAll($this->select);

            $objPHPExcel->setActiveSheetIndex(0);
            $column = 'A';
            $line = 1;

            //imprime o cabecalho
            foreach ($data AS $row) {
                if ($line == 1) {
                    foreach ($campos as $campo) {
                        //pega o nome das colunas
                        $objPHPExcel->getActiveSheet()->SetCellValue($column . $line, $this->model->getLabel($campo));
                        $column++;
                    }
                }
                $line++;
                break;
            }
            foreach ($data AS $row) {
                $column = 'A';
                $valor = '';
                foreach ($campos as $campo) {
                    if ($campo == 'cadastrado_por' || $campo == 'editado_por') {
                        if ($campo) {
                            $valor = Model_Pessoa::getUsuarioNomeById($row->$campo);
                        }
                    } elseif ($campo == 'categoria_id') {
                        $valor = Model_Pessoa::getCategoriaNomeById($row->$campo);
                    } else {
                        $valor = $row->$campo;
                    }
                    $objPHPExcel->getActiveSheet()->SetCellValue($column . $line, $this->mb_strtoupper_new($valor));
                    $column++;
                }
                $line++;
            }

            // Rename sheet
            $objPHPExcel->getActiveSheet()->setTitle('Pessoas');


            // Save Excel 2007 file
            $filename = "lista-" . date("m-d-Y") . ".xls";

            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $this->getResponse()->setRawHeader("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8")
                    ->setRawHeader("Content-Disposition: attachment; filename=$filename")
                    ->setRawHeader("Content-Transfer-Encoding: binary")
                    ->setRawHeader("Expires: 0")
                    ->setRawHeader("Cache-Control: must-revalidate, post-check=0, pre-check=0")
                    ->setRawHeader("Pragma: public")
                    ->sendResponse();
            $objWriter->save('php://output');
        }
    }
	
	    public function imprimeAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        if ($request->isGet()) {

            $por = $this->_getParam('by', null);
            $order = $this->_getParam('order', null);
            $tipo = $this->_getParam('tipo', null);
            $categoria = $this->_getParam('categoria', null);
			$nome = $this->_getParam('nome', null);

            $select = $this->select;

            if ($por && $order) {
                $select->order(array($por . ' ' . $order));
                $this->view->por = $por;
                $this->view->order = $order;
            }

            if ($categoria) {
                $select->where('categoria_id= ?', $categoria);
                $this->view->categoria = $categoria;
            }
            if ($tipo) {
                $select->where('tipo= ?', $tipo);
                $this->view->tipo = $tipo;
            }
			if (!empty($nome) && isset($nome)) {
				$select->where('nome LIKE ?', $nome . '%');
			}



//        require_once( APPLICATION_PATH . "/../tcpdf/tcpdf.php" );
// Include the main TCPDF library (search for installation path).
//        require_once('tcpdf_include.php');
// always load alternative config file for examples
// set some language-dependent strings (optional)

            define('FPDF_FONTPATH', 'fpdf_font/');
            require_once( APPLICATION_PATH . "/../tcpdf/PDF_Label.php");

            /* ------------------------------------------------
              To create the object, 2 possibilities:
              either pass a custom format via an array
              or use a built-in AVERY name
              ------------------------------------------------ */

// Example of custom format
            /*
              $pdf = new PDF_Label(array('paper-size'=>'A4', 'metric'=>'mm', 'marginLeft'=>1, 'marginTop'=>1, 'NX'=>2, 'NY'=>7, 'SpaceX'=>0, 'SpaceY'=>0, 'width'=>99, 'height'=>38, 'font-size'=>14));

             */
// Standard format
            $pdf = new PDF_Label('3180');

            $pdf->AddPage();
            $pdf->SetAuthor('Qualitare');
            $pdf->SetTitle('Pessoas');

            $pdf->setFontSubsetting(false);
            $pdf->setPrintHeader(true);
            $pdf->setPrintFooter(false);
            $pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));


            // Add some data
            $data = $this->model->fetchAll($this->select);
//            print_r($data);exit;
            // Print labels
			//$j=1;
			//for($i=0; $i<10; $i++){
				foreach ($data as $pessoa) {
					$text = sprintf("%s, %s\n%s - CEP:%s", $pessoa->endereco, $pessoa->bairro, $pessoa->cidade . '/' . $pessoa->uf, $pessoa->cep);
					$pdf->Add_Label($pessoa->nome, $text);
					//$j++;
				}
			//}
            $filename = "lista-" . date("m-d-Y") . ".pdf";
            $pdf->Output($filename);
        }
    }

    function mb_strtoupper_new($str, $e = 'utf-8') {
        if (function_exists('mb_strtoupper')) {
            return mb_strtoupper($str, $e = 'utf-8');
        } else {
            foreach ($str as &$char) {
                $char = utf8_decode($char);
                $char = strtr(char, "abcdefghýijklmnopqrstuvwxyz" .
                        "\x9C\x9A\xE0\xE1\xE2\xE3" .
                        "\xE4\xE5\xE6\xE7\xE8\xE9" .
                        "\xEA\xEB\xEC\xED\xEE\xEF" .
                        "\xF0\xF1\xF2\xF3\xF4\xF5" .
                        "\xF6\xF8\xF9\xFA\xFB\xFC" .
                        "\xFE\xFF", "ABCDEFGHÝIJKLMNOPQRSTUVWXYZ" .
                        "\x8C\x8A\xC0\xC1\xC2\xC3\xC4" .
                        "\xC5\xC6\xC7\xC8\xC9\xCA\xCB" .
                        "\xCC\xCD\xCE\xCF\xD0\xD1\xD2" .
                        "\xD3\xD4\xD5\xD6\xD8\xD9\xDA" .
                        "\xDB\xDC\xDE\x9F");
                $char = utf8_encode($char);
            }
            return $str;
        }
    }

}

