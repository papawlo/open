<?php

class Model_Row_Contact extends Zend_Db_Table_Row_Abstract
{
    private $tags = null;
    private $user = null;
 
    /**
     * @return Model_Row_User
     */
    public function getUser()
    {
        if (!$this->user) {
            $this->user = $this->findParentRow('Model_DbTable_Users');
        }
 
        return $this->user;
    }
 
    /**
     * @return Model_Rowset_Tags
     */
    public function getTags()
    {
        if (!$this->tags) {
            $this->tags = $this->findManyToManyRowset(
                'Model_DbTable_Tags',   // match table
                'Model_DbTable_VideoTag');  // join table
        }
 
        return $this->tags;
    }
}