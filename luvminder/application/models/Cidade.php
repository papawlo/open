<?php

class Model_Cidade extends App_Db_Table_Abstract {

    protected $_name = 'cidades';
    protected $_primary = 'id_cidade';

    /**
     * Dependent tables
     */
    protected $_dependentTables = array('bairro');

    /**
     * Refence Map
     */
    protected $_referenceMap = array(
        'Estados' => array(
            'columns' => 'estado_cod',
            'refTableClass' => 'Model_Estado',
            'refColumns' => 'id_estado'
        )
    );

    public function getByEstado($estado_cod, $order = 'nome ASC', $limit = 0) {
        $select = $this->select()
                ->where('estado_cod = ?', $estado_cod)
                ->orWhere('uf = ?', $estado_cod)
                ->order($order);

        if ($limit) {
            $select->limit($limit);
        }

        return $this->fetchAll($select);
    }

    public static function getCidadesCadastradas() {
        $modelEstabelecimento = new Model_Estabelecimento();
        $select = $modelEstabelecimento->select()
                ->setIntegrityCheck(false)
                ->from('estabelecimento')
                ->join('cidades', 'cidades.id_cidade = estabelecimento.cidade', array('cidades.id_cidade', 'cidades.cidade'))
                ->group('cidades.id_cidade')
                ->order('cidades.cidade ASC');

        $results = $modelEstabelecimento->fetchAll($select);

        return $results;
    }

}