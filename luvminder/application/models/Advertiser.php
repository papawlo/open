<?php

class Model_Advertiser extends App_Db_Table_Abstract {

    protected $_name = 'advertiser';
    protected $_primary = 'id';

    /**
     * Dependent tables
     */
    protected $_dependentTables = array('Model_Campaign');

}