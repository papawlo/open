<?php

class Model_DadosUsuario extends App_Db_Table_Abstract {

    protected $_name = 'dados_usuarios';
    protected $_primary = 'id_dados_usuario';

    /**
     * The default row class
     */
    protected $_rowClass = 'RowDadosUsuario';

    /**
     * Refence Map
     */
    protected $_referenceMap = array(
        'Enderecos' => array(
            'columns' => 'endereco',
            'refTableClass' => 'Model_Enderecos',
            'refColumns' => 'id_endereco'
        ),
        'Medias' => array(
            'columns' => 'foto',
            'refTableClass' => 'Model_Media',
            'refColumns' => 'id_media'
        ),
        'Usuarios' => array(
            'columns' => 'id_usuario',
            'refTableClass' => 'Model_Usuario',
            'refColumns' => 'id_usuario'
        ),
    );

}

class RowDadosUsuario extends Zend_Db_Table_Row_Abstract {

    private $usuario = null;

    /**
     * getUser
     *
     * @return <Usuario Row> $usuario
     */
    public function getUsuario() {
        if (!$this->usuario) {
            $this->usuario = $this->findParentRow('Model_Usuario');
        }

        return $this->usuario;
    }

}