<?php

class Model_Authentications extends App_Db_Table_Abstract {

    protected $_name = 'authentications';
    protected $_primary = 'id';

    /**
     * Refence Map
     */
    protected $_referenceMap = array(
        'Users' => array(
            'columns' => 'users_id',
            'refTableClass' => 'Model_Users',
            'refColumns' => 'id'
        )
    );

    function find_by_provider_uid($provider, $provider_uid) {
        $select = $this->select()->where('provider = ?', $provider)->where('provider_uid = ?', $provider_uid)->limit(1);
        return $result = $this->fetchRow($select);
    }

    function create($user_id, $provider, $provider_uid, $email, $display_name, $first_name, $last_name, $profile_url, $website_url, $photo_url) {

        $data = array(
            'user_id' => $user_id,
            'provider' => $provider,
            'provider_uid' => $provider_uid,
            'email' => $email,
            'display_name' => $display_name,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'profile_url' => $profile_url,
            'website_url' => $website_url,
            'photo_url' => $photo_url,
            'created_at' => date('Y-m-d H:i:s')
        );
        $newRow = $this->createRow();
        // Set all the column values at once
        $newRow->setFromArray($data);
        $newRow->save();
    }

    function find_by_user_id($user_id) {

        $select = $this->select()->where('user_id = ?', $user_id)->limit(1);
        return $result = $this->fetchRow($select);
    }
  

}