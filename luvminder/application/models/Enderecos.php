<?php

class Model_Enderecos extends App_Db_Table_Abstract {

    protected $_name = 'enderecos';
    protected $_primary = 'id_endereco';

    /**
     * Dependent tables
     */
    protected $_dependentTables = array('estabeleciento', 'dados_usuario', 'estabelecimento_bairro_valor');

    /**
     * Refence Map
     */
    protected $_referenceMap = array(
        'Estados' => array(
            'columns' => 'uf_cod',
            'refTableClass' => 'Model_Estado',
            'refColumns' => 'id_estado'
        ),
        'Cidades' => array(
            'columns' => 'cidade_id',
            'refTableClass' => 'Model_Cidade',
            'refColumns' => 'id_cidade'
        ),
        'Bairros' => array(
            'columns' => 'bairro_id',
            'refTableClass' => 'Model_Bairro',
            'refColumns' => 'id_bairro'
        )
    );

    public function getByCidade($cidade, $order = 'nome ASC', $limit = 0) {
        $select = $this->select()
                ->where('cidade = ?', $cidade)
                ->order($order);

        if ($limit) {
            $select->limit($limit);
        }

        return $this->fetchAll($select);
    }

    public function findCep($cep) {

        $cepFim = substr($cep, -3);
        if ($cepFim === '000') {
            $modelCidade = new Model_Cidade();
            $select = $modelCidade->select()->where('cep = ?', $cep);
            $result = $modelCidade->fetchRow($select);
            $enderecoRow = array();
            if (count($result)) {
                //cep de cidade
                $enderecoRow = $result->toArray();
                $enderecoRow['resultado'] = 2;
            } else {
                $enderecoRow['resultado'] = 3;
            }
            return $enderecoRow;
        } else {
            $select = $this->select()->where('cep = ?', $cep);
            $result = $this->fetchRow($select);
            $enderecoRow = array();
            if (count($result)) {
                //cep com bairro
                $enderecoRow = $result->toArray();
                $enderecoRow['resultado'] = 1;
            } else {
                $enderecoRow['resultado'] = 3;
            }

            return $enderecoRow;
        }
        return false;
    }

    public static function getLogradouroCompleto($id) {

        $enderecoModel = new Model_Enderecos();

        $enderecoRow = $enderecoModel->find($id)->current();
        return $enderecoRow->nomeclog;
    }

    public static function getEnderecoCompleto($id, $numero = null, $complemento = null) {

        $enderecoModel = new Model_Enderecos();

        $enderecoRow = $enderecoModel->findWithJoins($id);
        $enderecoCompleto = "";
        $enderecoCompleto .= $enderecoRow->logracompl;
        $enderecoCompleto .= $numero ? " " . $numero : "";
        $enderecoCompleto .= $complemento ? " " . $complemento : " ";
        $enderecoCompleto .= $enderecoRow->Bairros_nome . " - ";
        $enderecoCompleto .= $enderecoRow->Cidades_nome . "/";
        $enderecoCompleto .= $enderecoRow->uf;

        return $enderecoCompleto;
    }

}