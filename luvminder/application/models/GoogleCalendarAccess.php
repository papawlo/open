<?php

class Model_GoogleCalendarAccess extends App_Db_Table_Abstract {

    protected $_name = 'google_calendar_access';
    protected $_primary = 'id';

    /**
     * Refence Map
     */
    protected $_referenceMap = array(
        'Users' => array(
            'columns' => 'users_id',
            'refTableClass' => 'Model_Users',
            'refColumns' => 'id'
        )
    );

   
}