<?php

class Model_UserConnections extends App_Db_Table_Abstract {

    protected $_name = 'users_connections';
    protected $_primary = 'id';

    /**
     * Refence Map
     */
    protected $_referenceMap = array(
        'Users' => array(
            'columns' => 'users_id',
            'refTableClass' => 'Model_Users',
            'refColumns' => 'id'
        )
    );

    // define a function to sotre it on whatever storage you want to use 
    function sotre_hybridauth_session($user_id, $data, $provider) {

        $data = array(
            'users_id' => $user_id,
            'hybridauth_session' => $provider,
            'updated_at' => date('Y-m-d H:i:s')
        );
        $newRow = $this->createRow();
        // Set all the column values at once
        $newRow->setFromArray($data);
        $newRow->save();
    }

    // define a function to get the stored hybridauth data back from your storage system
    function get_sotred_hybridauth_session($user_id, $provider) {
        $select = $this->select()->where('users_id = ?', $user_id)->limit(1);
        return $result = $this->fetchRow($select);
    }

}