<?php

class Model_Relationship extends App_Db_Table_Abstract {

    protected $_name = 'relationship';
    protected $_primary = 'id';

    /**
     * Dependent tables
     */
    protected $_dependentTables = array('Model_Reminder');

    /**
     * Row class
     */
    protected $_rowClass = 'Model_Row_Relationship';

    /**
     * Refence Map
     */
    protected $_referenceMap = array(
        'Users' => array(
            'columns' => 'users_id',
            'refTableClass' => 'Model_Users',
            'refColumns' => 'id'
        )
    );

}