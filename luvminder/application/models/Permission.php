<?php

class Model_Permission extends App_Db_Table_Abstract {

    protected $_name = 'permission';
    protected $_primary = 'id_permission';

    protected $_referenceMap = array(
        'Role' => array(
            'columns' => 'id_role',
            'refTableClass' => 'Model_Role',
            'refColumns' => 'id_role'
        ),
        'Resource' => array(
            'columns' => 'id_resource',
            'refTableClass' => 'Model_Resource',
            'refColumns' => 'id_resource'
        ),
	);	
}