<?php

class Model_Media extends Zend_Db_Table_Abstract {

    protected $_name = 'media';
    protected $_primary = 'id';

    /**
     * Dependent tables
     */
    protected $_dependentTables = array('user', 'usuario');

    public static function getThumbnail($id, $controller, $thumbnail) {

        $model = new Model_Media();

        $media = $model->find($id)->current();
        if ($media) {

            $media_helper = new App_Helpers_Media();

            return $media_helper->getThumbnail($media, $controller, $thumbnail);
        }
        return false;
    }

    public static function getDimensions($id, $thumbnail = null) {

        $model = new Model_Media();

        $media = $model->find($id)->current();

        $media_helper = new App_Helpers_Media();

        return $media_helper->getDimensions($media, $thumbnail);
    }

    public static function removeImagem($id) {

        $model = new Model_Media();

        $media = $model->find($id)->current();

        $media_helper = new App_Helpers_Media();

        if ($media) {
            if ($media_helper->removeImagem($media)) {
                $media->delete();
            }
        }
    }

    public static function customCrop($media, $controller, $slug, $top, $left, $crop_area_width, $crop_area_height) {
        $media_helper = new App_Helpers_Media();
        return $media_helper->customCrop($media, $controller, $slug, $top, $left, $crop_area_width, $crop_area_height);
    }

}