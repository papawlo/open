<?php

class Model_Notification extends App_Db_Table_Abstract {

    protected $_name = 'notification';
    protected $_primary = 'id';
    protected $_dependentTables = array('Model_Reminder');

    /**
     * Refence Map
     */
    protected $_referenceMap = array(
        'Relationship' => array(
            'columns' => 'relationship_id',
            'refTableClass' => 'Model_Relationship',
            'refColumns' => 'id'
        )
    );


}