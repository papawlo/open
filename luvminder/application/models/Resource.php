<?php

class Model_Resource extends App_Db_Table_Abstract {

    protected $_name = 'resource';
    protected $_primary = 'id_resource';
    // protected $_dependentTables = array('grupo','produto');
    
    protected $_referenceMap = array(
        'ResourcePai' => array(
            'columns' => 'id_parent',
            'refTableClass' => 'Model_Resource',
            'refColumns' => 'id_parent'
        ),
    );
}