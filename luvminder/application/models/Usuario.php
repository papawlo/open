<?php

class Model_Usuario extends App_Db_Table_Abstract {

    protected $_name = 'usuario';
    protected $_primary = 'id_usuario';

    /**
     * Dependent tables
     */
    protected $_dependentTables = array('role');
    
    /**
     * Refence Map
     */
    protected $_referenceMap = array(
        'Foto' => array(
            'columns' => 'media_id',
            'refTableClass' => 'Model_Media',
            'refColumns' => 'id'
        ),
    );

    public function getDados($id_usuario) {
        $_model = new Model_DadosUsuario();
        $select = $_model->select()->where('id_usuario = ?', $id_usuario);

        $rowDadosUsuarios = $_model->fetchRow($select);

        return $rowDadosUsuarios;
    }

   
}