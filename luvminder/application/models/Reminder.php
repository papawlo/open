<?php

class Model_Reminder extends App_Db_Table_Abstract {

    protected $_name = 'reminder';
    protected $_primary = 'id';
    
        /**
     * Row class
     */
//    protected $_rowClass = 'Model_Row_Reminder';

    /**
     * Refence Map
     */
    protected $_referenceMap = array(
        'Relationship' => array(
            'columns' => 'relationship_id',
            'refTableClass' => 'Model_Relationship',
            'refColumns' => 'id'
        )
    );

}