<?php

class Model_Auth {

    public static function login($login, $senha) {
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();

        $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
        $authAdapter->setTableName('usuario')
                ->setIdentityColumn('login')
                ->setCredentialColumn('senha')
                ->setCredentialTreatment('MD5(?)');

        $authAdapter->setIdentity($login)
                ->setCredential($senha);

        $select = $authAdapter->getDbSelect();
        $select->where('status = "ativo"');

        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($authAdapter);

        if ($result->isValid()) {

            $info = $authAdapter->getResultRowObject(null, 'senha');

            $usuarioTable = new Model_Usuario();
            // $usuarioRowset = $usuarioTable->find($info->id_usuario);
            // $usuario = $usuarioRowset->current();

            $select = $usuarioTable->select()->from(array('u' => 'usuario'))
                    ->setIntegrityCheck(false)
                    ->join(array('r' => 'role'), 'u.id_role = r.id_role', array('role_name' => 'r.name'))
                    ->where('u.id_usuario=?', $info->id_usuario);


            $usuarioRowset = $usuarioTable->fetchRow($select);
            $usuario = $usuarioRowset;
            $storage = $auth->getStorage();
            $storage->write($usuario);
            return $result->getCode();
        } else {
            return $result->getCode();
        }
    }

}
