<?php $_pagename = 'pacotes'; include "header.php"; ?>
<?php

if (have_posts()) :
while ( have_posts() ) : the_post();
global $post, $wpdb; 
$imagem_interna = get_post_meta( $post->ID,'imagem_interna', true );
$descricao = get_post_meta( $post->ID,'descricao', true );
$selecionador = get_post_meta( $post->ID,'selecionador', true );
$link_reserva = get_post_meta( $post->ID,'link_reserva', true );
$pacote_open[] = $post->ID;

?>

	<section class="pacotes read">
		<header style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/resort-top.jpg)"	></header>
		<div class="title col-xs-12 col-sm-4 col-md-3">
			<h1>Pacotes & Promoções</h1>
		</div>

		<div class="main">
			<aside class="col-xs-12 col-sm-3 col-md-2">
				<ul>
					<li class="-active">
						<a href="#">Pacotes</a>
						<ul class="submenu">
							<?php
								$args = array('post_type' => 'pacotes_promocoes','order' => 'asc','meta_key' => 'selecionador', 'meta_value' => 'pacotes');
								query_posts($args);
									if (have_posts()) :
									while (have_posts()) : the_post();
									global $post, $wpdb; 
									$imagem_capa = get_post_meta( $post->ID,'imagem_capa', true ); 
							?>		
							<li>
								<a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a>
							</li>	
							<?php endwhile; ?>
							<?php
								else : 
									echo ' ';
								endif;
								wp_reset_query();
							?>
						</ul>
					</li>
					<li>
						<a href="#">Promoções</a>
						<ul class="submenu">
							<?php
								$args = array('post_type' => 'pacotes_promocoes','order' => 'asc','meta_key' => 'selecionador', 'meta_value' => 'promocoes');
								query_posts($args);
									if (have_posts()) :
									while (have_posts()) : the_post();
									global $post, $wpdb; 
									$imagem_capa = get_post_meta( $post->ID,'imagem_capa', true ); 
							?>		
							<li>
								<a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a>
							</li>	
							<?php endwhile; ?>
							<?php
								else : 
									echo ' ';
								endif;
								wp_reset_query();
							?>
						</ul>
					</li>
				</ul>
			</aside>

			<div class="col-xs-12 col-sm-9 col-md-10">
				<div class="row">
					


		
		<div class="col-xs-12 col-sm-12">
			<header class="thumb">
				<h1><?php echo the_title(); ?></h1>
				<?php $logo = wp_get_attachment_image_src( $imagem_interna , '1024x285' ); ?>
 				<img src="<?php echo $logo[0]; ?>" alt="<?php the_title(); ?>">
			</header>
			<div class="post"><?php echo $descricao; ?></div>
			<div class="center">
				<a href="<?php echo $link_reserva; ?>" class="button-default">Reservar</a>
			</div>
							<?php endwhile; ?>
							<?php
								else : 
									echo ' ';
								endif;
								wp_reset_query();
							?>

			<div class="-list row">
				<h2><?php if( $selecionador == 'pacotes'){ ?>Outros pacotes <?php }else{ ?> Outras promoções<?php } ?></h2>

							<?php
								$args = array('post_type' => 'pacotes_promocoes','order' => 'asc','post__not_in' => $pacote_open, 'meta_key' => 'selecionador',
'meta_value' => $selecionador);
								query_posts($args);
									if (have_posts()) :
									while (have_posts()) : the_post();
									global $post, $wpdb; 
									$imagem_capa = get_post_meta( $post->ID,'imagem_capa', true );
							?>	

				<article class="col-xs-12 col-sm-4">
					<div class="thumb">
						<?php $imageUrl = wp_get_attachment_image_src($imagem_capa, full); ?>
						<?php $resizer = Odin_Thumbnail_Resizer::get_instance();
								$image1 = $resizer->process($imageUrl[0], '500', '340', true, true ); ?>
						<?php echo '<img class="wp-image-thumb img-responsive ' . sanitize_html_class( $class ) . '" src="' .$image1. '" width="' . esc_attr( $width ) . '" height="' . esc_attr( $height ) . '" alt="' . esc_attr( $alt ) . '" />'; ?>

						<div class="holder">
							<a href="<?php echo the_permalink(); ?>" class="button-default"><?php if( $selecionador == 'pacotes'){ ?>Ver Pacote <?php }else{ ?> Ver Promoção<?php } ?></a>
						</div>
						<a href="<?php echo the_permalink(); ?>" class="overlay"></a>
					</div>
					<a href="<?php echo the_permalink(); ?>" class="permalink"><?php echo the_title(); ?></a>
				</article>

							<?php endwhile; ?>
							<?php
								else : 
									echo ' ';
								endif;
								wp_reset_query();
							?>

			</div>
			<div class="center">
				<a href="../../pacotes/" class="button-default"><?php if( $selecionador == 'pacotes'){ ?>Veja mais pacotes <?php }else{ ?> Veja mais promoções<?php } ?></a>
			</div>
		</div>
</div>
			</div>
		</div>
	</section>

<?php get_footer() ?>