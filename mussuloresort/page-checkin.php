<?php $_pagename = 'checkin'; include "header.php"; ?>
<?php
if(isset($_POST['checkin_nome'])) {
   /* ------------------------------------------------ */
   
        $email= $_POST['checkin_email'];
     
    $nome = $_POST['checkin_nome'];
    $telefone = $_POST['checkin_telefone'];
    
    $nascimento = $_POST['checkin_nascimento'];
    $sexo = $_POST['checkin_sexo'];
    $nacionalidade = $_POST['checkin_nacionalidade'];
    $pais = $_POST['checkin_pais'];
    $estado = $_POST['checkin_estado'];
    $cidade = $_POST['checkin_cidade'];
    $endereco = $_POST['checkin_endereco'];
    $numero = $_POST['checkin_numero'];
    $complemento = $_POST['checkin_complemento'];
    $documento = $_POST['checkin_documento'];
    $cpf = $_POST['checkin_cpf'];
    $rg = $_POST['checkin_rg'];
    $passaporte = $_POST['checkin_passaporte'];
    /* ------------------------------------------------ */
    if(!isset($hasError)) {
        if (!isset($emailTo) || ($emailTo == '') ){
            $emailTo = "bruno@modernizr.com.br";
        }
        $subject = 'Mala Direta - Site Mussulo';
        $body = "Nome: $nome[0] \n\n Email: $email[0] \n\n Telefone: $telefone[0] \n\n Nascimento: $nascimento[0] \n\n sexo: $sexo[0] \n\n Nacionalidade: $nacionalidade[0] \n\n País: $pais[0] \n\n Estado: $estado[0] \n\n Cidade: $cidade[0] \n\n Endereço: $endereco[0] \n\n Número: $numero[0] \n\n Complemento: $complemento[0] \n\n Documento: $documento[0] \n\n CPF: $cpf[0] \n\n RG: $rg[0] \n\n Passaporte: $passaporte[0] \n\n Enviado Através do WebCheckin";
        $headers = 'From: bruno@modernizr.com.br <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email[0];

        wp_mail($emailTo, $subject, $body, $headers);
        $emailSent = true;
        echo '<div class="alert alert-success" role="alert" style="padding:10px 0 5px 0;margin:0;width: 100%;float: left; text-align:center; position:absolute; top:135px;"><p class="container">Contato realizado com sucesso!<p></div>';
    }
}
?>
	


	<section class="checkin inner">
		<header style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/resort-top.jpg)"	></header>
		<div class="title col-xs-12 col-sm-4 col-md-3">
			<h1>Checkin</h1>
		</div>
		<div class="info col-xs-12 col-sm-8 col-md-9">
			<h2>Faça seu web check-in e ganhe tempo</h2>
			<p>As informações que seguem abaixo correspondem a Ficha Nacional de Registros de Hóspedes.
É exatamente a mesma ficha que você está acostumado a preencher nos guichês de check-in nos hotéis brasileiros.</p>
		</div>
		<form action="#" method="post">
			<ul>
			</ul>
			<fieldset class="col-xs-12 col-sm-8 col-sm-offset-4 col-md-offset-3">
				<ul>
					<li class="col-xs-12">
						<input type="submit" name="send" value="Enviar" class="button-default">
						<button class="more-people button-default">Adicionar<br /> Acompanhante</button>
					</li>
				</ul>
			</fieldset>
		</form>

		<script type="text/template" id="checkin-template">
			<fieldset class="col-xs-12 col-sm-8 col-sm-offset-4 col-md-offset-3">
				<h2>Dados Pessoais</h2>
				<ul class="row">
					<li class="col-xs-12 col-sm-6">
						<label for="checkin_nome">Nome:</label>
						<input type="text" id="checkin_nome" name="checkin_nome[]">
					</li>
					<li class="col-xs-12 col-sm-6">
						<label for="checkin_email">Email:</label>
						<input type="email" id="checkin_email" name="checkin_email[]">
					</li>
					<li class="col-xs-12 col-sm-6">
						<label for="checkin_telefone">Telefone:</label>
						<input type="text" id="checkin_telefone" name="checkin_telefone[]">
					</li>
					<li class="col-xs-12 col-sm-3">
						<label for="checkin_nascimento">Nascimento:</label>
						<input type="text" id="checkin_nascimento" name="checkin_nascimento[]">
					</li>
					<li class="col-xs-12 col-sm-3">
						<label for="checkin_sexo">Sexo:</label>
						<select name="checkin_sexo[]" id="checkin_sexo">
							<option value=""></option>
							<option value="M">Masculino</option>
							<option value="F">Feminino</option>
							<option value="O">Outro</option>
						</select>
					</li>
					<li class="col-xs-12 col-sm-5">
						<label for="checkin_nacionalidade">Nacionalidade:</label>
						<input type="text" id="checkin_nacionalidade" name="checkin_nacionalidade[]">
					</li>
					<li class="col-xs-12 col-sm-5">
						<label for="checkin_pais">País:</label>
						<select name="checkin_pais[]" id="checkin_pais">
							<option value=""></option>
							<option value="M">Masculino</option>
							<option value="F">Feminino</option>
							<option value="O">Outro</option>
						</select>
					</li>
					<li class="col-xs-12 col-sm-2">
						<label for="checkin_estado">Estado:</label>
						<select name="checkin_estado[]" id="checkin_estado">
							<option value=""></option>
							<option value="M">Masculino</option>
							<option value="F">Feminino</option>
							<option value="O">Outro</option>
						</select>
					</li>
					<li class="col-xs-12 col-sm-4">
						<label for="checkin_cidade">Cidade:</label>
						<select name="checkin_cidade[]" id="checkin_cidade">
							<option value=""></option>
							<option value="M">Masculino</option>
							<option value="F">Feminino</option>
							<option value="O">Outro</option>
						</select>
					</li>
					<li class="col-xs-12 col-sm-6">
						<label for="checkin_endereco">Endereço:</label>
						<input type="text" id="checkin_endereco" name="checkin_endereco[]">
					</li>
					<li class="col-xs-12 col-sm-2">
						<label for="checkin_numero">Numero:</label>
						<input type="number" id="checkin_numero" name="checkin_numero[]">
					</li>
					<li class="col-xs-12">
						<label for="checkin_complemento">Complemento:</label>
						<input type="text" id="checkin_complemento" name="checkin_complemento[]">
					</li>
				</ul>
			</fieldset>

			<fieldset class="col-xs-12 col-sm-8 col-sm-offset-4 col-md-offset-3">
				<h2>Documento</h2>
				<ul class="row">
					<li class="col-xs-12 col-sm-6 checkbox">
						<label for="checkin_documento_br"><input type="radio" id="checkin_documento_br" class="tab-radio" data-target="#brasileiro" name="checkin_documento[]" value="Brasileiro" checked> Sou brasileiro</label>
					</li>
					<li class="col-xs-12 col-sm-6 checkbox">
						<label for="checkin_documento_en"><input type="radio" id="checkin_documento_en" class="tab-radio" data-target="#estrangeiro" name="checkin_documento[]" value="Estrangeiro"> Sou estrangeiro</label>
					</li>
				</ul>
				<div class="tabs">
					<ul id="brasileiro" class="tab -active col-xs-12 col-sm-4">
						<li>
							<label for="checkin_cpf">CPF</label>
							<input type="text" name="checkin_cpf[]" id="checkin_cpf">
						</li>
						<li>
							<label for="checkin_cpf">RG</label>
							<input type="text" name="checkin_rg[]" id="checkin_rg">
						</li>
					</ul>
					<ul id="estrangeiro" class="tab col-xs-12 col-sm-6">
						<li>
							<label for="checkin_passaporte">Passaporte</label>
							<input type="text" name="checkin_passaporte[]" id="checkin_passaporte">
						</li>
					</ul>
				</div>
			</fieldset>
		</script>
	</section>

<?php get_footer() ?>
