/*!
 * jQuery JavaScript Library v3.0.0-alpha1
 * http://jquery.com/
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2015-07-13T19:25Z
 */

 (function( global, factory ) {

 	if ( typeof module === "object" && typeof module.exports === "object" ) {
		// For CommonJS and CommonJS-like environments where a proper `window`
		// is present, execute the factory and get jQuery.
		// For environments that do not have a `window` with a `document`
		// (such as Node.js), expose a factory as module.exports.
		// This accentuates the need for the creation of a real `window`.
		// e.g. var jQuery = require("jquery")(window);
		// See ticket #14549 for more info.
		module.exports = global.document ?
		factory( global, true ) :
		function( w ) {
			if ( !w.document ) {
				throw new Error( "jQuery requires a window with a document" );
			}
			return factory( w );
		};
	} else {
		factory( global );
	}

// Pass this if window is not defined yet
}(typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

// Support: Firefox 18+
// Can't be in strict mode, several libs including ASP.NET trace
// the stack via arguments.caller.callee and Firefox dies if
// you try to trace through "use strict" call chains. (#13335)
//"use strict";
var arr = [];

var document = window.document;

var slice = arr.slice;

var concat = arr.concat;

var push = arr.push;

var indexOf = arr.indexOf;

var class2type = {};

var toString = class2type.toString;

var hasOwn = class2type.hasOwnProperty;

var support = {};



var
version = "3.0.0-alpha1",

	// Define a local copy of jQuery
	jQuery = function( selector, context ) {
		// The jQuery object is actually just the init constructor 'enhanced'
		// Need init if jQuery is called (just allow error to be thrown if not included)
		return new jQuery.fn.init( selector, context );
	},

	// Support: Android<4.1
	// Make sure we trim BOM and NBSP
	rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,

	// Matches dashed string for camelizing
	rmsPrefix = /^-ms-/,
	rdashAlpha = /-([a-z])/g,

	// Used by jQuery.camelCase as callback to replace()
	fcamelCase = function( all, letter ) {
		return letter.toUpperCase();
	};

	jQuery.fn = jQuery.prototype = {
	// The current version of jQuery being used
	jquery: version,

	constructor: jQuery,

	// The default length of a jQuery object is 0
	length: 0,

	toArray: function() {
		return slice.call( this );
	},

	// Get the Nth element in the matched element set OR
	// Get the whole matched element set as a clean array
	get: function( num ) {
		return num != null ?

			// Return just the one element from the set
			( num < 0 ? this[ num + this.length ] : this[ num ] ) :

			// Return all the elements in a clean array
			slice.call( this );
		},

	// Take an array of elements and push it onto the stack
	// (returning the new matched element set)
	pushStack: function( elems ) {

		// Build a new jQuery matched element set
		var ret = jQuery.merge( this.constructor(), elems );

		// Add the old object onto the stack (as a reference)
		ret.prevObject = this;

		// Return the newly-formed element set
		return ret;
	},

	// Execute a callback for every element in the matched set.
	each: function( callback ) {
		return jQuery.each( this, callback );
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map(this, function( elem, i ) {
			return callback.call( elem, i, elem );
		}));
	},

	slice: function() {
		return this.pushStack( slice.apply( this, arguments ) );
	},

	first: function() {
		return this.eq( 0 );
	},

	last: function() {
		return this.eq( -1 );
	},

	eq: function( i ) {
		var len = this.length,
		j = +i + ( i < 0 ? len : 0 );
		return this.pushStack( j >= 0 && j < len ? [ this[j] ] : [] );
	},

	end: function() {
		return this.prevObject || this.constructor(null);
	},

	// For internal use only.
	// Behaves like an Array's method, not like a jQuery method.
	push: push,
	sort: arr.sort,
	splice: arr.splice
};

jQuery.extend = jQuery.fn.extend = function() {
	var options, name, src, copy, copyIsArray, clone,
	target = arguments[0] || {},
	i = 1,
	length = arguments.length,
	deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;

		// Skip the boolean and the target
		target = arguments[ i ] || {};
		i++;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !jQuery.isFunction(target) ) {
		target = {};
	}

	// Extend jQuery itself if only one argument is passed
	if ( i === length ) {
		target = this;
		i--;
	}

	for ( ; i < length; i++ ) {
		// Only deal with non-null/undefined values
		if ( (options = arguments[ i ]) != null ) {
			// Extend the base object
			for ( name in options ) {
				src = target[ name ];
				copy = options[ name ];

				// Prevent never-ending loop
				if ( target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( jQuery.isPlainObject(copy) ||
					(copyIsArray = jQuery.isArray(copy)) ) ) {

					if ( copyIsArray ) {
						copyIsArray = false;
						clone = src && jQuery.isArray(src) ? src : [];

					} else {
						clone = src && jQuery.isPlainObject(src) ? src : {};
					}

					// Never move original objects, clone them
					target[ name ] = jQuery.extend( deep, clone, copy );

				// Don't bring in undefined values
			} else if ( copy !== undefined ) {
				target[ name ] = copy;
			}
		}
	}
}

	// Return the modified object
	return target;
};

jQuery.extend({
	// Unique for each copy of jQuery on the page
	expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

	// Assume jQuery is ready without the ready module
	isReady: true,

	error: function( msg ) {
		throw new Error( msg );
	},

	noop: function() {},

	isFunction: function( obj ) {
		return jQuery.type(obj) === "function";
	},

	isArray: Array.isArray,

	isWindow: function( obj ) {
		return obj != null && obj === obj.window;
	},

	isNumeric: function( obj ) {
		// parseFloat NaNs numeric-cast false positives (null|true|false|"")
		// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
		// subtraction forces infinities to NaN
		// adding 1 corrects loss of precision from parseFloat (#15100)
		return !jQuery.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
	},

	isPlainObject: function( obj ) {
		// Not plain objects:
		// - Any object or value whose internal [[Class]] property is not "[object Object]"
		// - DOM nodes
		// - window
		if ( jQuery.type( obj ) !== "object" || obj.nodeType || jQuery.isWindow( obj ) ) {
			return false;
		}

		if ( obj.constructor &&
			!hasOwn.call( obj.constructor.prototype, "isPrototypeOf" ) ) {
			return false;
	}

		// If the function hasn't returned already, we're confident that
		// |obj| is a plain object, created by {} or constructed with new Object
		return true;
	},

	isEmptyObject: function( obj ) {
		var name;
		for ( name in obj ) {
			return false;
		}
		return true;
	},

	type: function( obj ) {
		if ( obj == null ) {
			return obj + "";
		}
		// Support: Android<4.0 (functionish RegExp)
		return typeof obj === "object" || typeof obj === "function" ?
		class2type[ toString.call(obj) ] || "object" :
		typeof obj;
	},

	// Evaluates a script in a global context
	globalEval: function( code ) {
		var script = document.createElement( "script" );

		script.text = code;
		document.head.appendChild( script ).parentNode.removeChild( script );
	},

	// Convert dashed to camelCase; used by the css and data modules
	// Support: IE9-11+
	// Microsoft forgot to hump their vendor prefix (#9572)
	camelCase: function( string ) {
		return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
	},

	nodeName: function( elem, name ) {
		return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
	},

	each: function( obj, callback ) {
		var i = 0,
		length = obj.length,
		isArray = isArraylike( obj );

		if ( isArray ) {
			for ( ; i < length; i++ ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		} else {
			for ( i in obj ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		}

		return obj;
	},

	// Support: Android<4.1
	trim: function( text ) {
		return text == null ?
		"" :
		( text + "" ).replace( rtrim, "" );
	},

	// results is for internal usage only
	makeArray: function( arr, results ) {
		var ret = results || [];

		if ( arr != null ) {
			if ( isArraylike( Object(arr) ) ) {
				jQuery.merge( ret,
					typeof arr === "string" ?
					[ arr ] : arr
					);
			} else {
				push.call( ret, arr );
			}
		}

		return ret;
	},

	inArray: function( elem, arr, i ) {
		return arr == null ? -1 : indexOf.call( arr, elem, i );
	},

	// Support: Android<4.1, PhantomJS<2
	// push.apply(_, arraylike) throws on ancient WebKit
	merge: function( first, second ) {
		var len = +second.length,
		j = 0,
		i = first.length;

		for ( ; j < len; j++ ) {
			first[ i++ ] = second[ j ];
		}

		first.length = i;

		return first;
	},

	grep: function( elems, callback, invert ) {
		var callbackInverse,
		matches = [],
		i = 0,
		length = elems.length,
		callbackExpect = !invert;

		// Go through the array, only saving the items
		// that pass the validator function
		for ( ; i < length; i++ ) {
			callbackInverse = !callback( elems[ i ], i );
			if ( callbackInverse !== callbackExpect ) {
				matches.push( elems[ i ] );
			}
		}

		return matches;
	},

	// arg is for internal usage only
	map: function( elems, callback, arg ) {
		var value,
		i = 0,
		length = elems.length,
		isArray = isArraylike( elems ),
		ret = [];

		// Go through the array, translating each of the items to their new values
		if ( isArray ) {
			for ( ; i < length; i++ ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}

		// Go through every key on the object,
	} else {
		for ( i in elems ) {
			value = callback( elems[ i ], i, arg );

			if ( value != null ) {
				ret.push( value );
			}
		}
	}

		// Flatten any nested arrays
		return concat.apply( [], ret );
	},

	// A global GUID counter for objects
	guid: 1,

	// Bind a function to a context, optionally partially applying any
	// arguments.
	proxy: function( fn, context ) {
		var tmp, args, proxy;

		if ( typeof context === "string" ) {
			tmp = fn[ context ];
			context = fn;
			fn = tmp;
		}

		// Quick check to determine if target is callable, in the spec
		// this throws a TypeError, but we will just return undefined.
		if ( !jQuery.isFunction( fn ) ) {
			return undefined;
		}

		// Simulated bind
		args = slice.call( arguments, 2 );
		proxy = function() {
			return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
		};

		// Set the guid of unique handler to the same of original handler, so it can be removed
		proxy.guid = fn.guid = fn.guid || jQuery.guid++;

		return proxy;
	},

	now: Date.now,

	// jQuery.support is not used in Core but other projects attach their
	// properties to it so it needs to exist.
	support: support
});

// JSHint would error on this code due to the Symbol not being defined in ES5.
// Defining this global in .jshintrc would create a danger of using the global
// unguarded in another place, it seems safer to just disable JSHint for these
// three lines.
/* jshint ignore: start */
if ( typeof Symbol === "function" ) {
	jQuery.fn[ Symbol.iterator ] = arr[ Symbol.iterator ];
}
/* jshint ignore: end */

// Populate the class2type map
jQuery.each("Boolean Number String Function Array Date RegExp Object Error".split(" "),
	function(i, name) {
		class2type[ "[object " + name + "]" ] = name.toLowerCase();
	});

function isArraylike( obj ) {

	// Support: iOS 8.2 (not reproducible in simulator)
	// `in` check used to prevent JIT error (gh-2145)
	// hasOwn isn't used here due to false negatives
	// regarding Nodelist length in IE
	var length = "length" in obj && obj.length,
	type = jQuery.type( obj );

	if ( type === "function" || jQuery.isWindow( obj ) ) {
		return false;
	}

	return type === "array" || length === 0 ||
	typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}
var Sizzle =
/*!
 * Sizzle CSS Selector Engine v2.2.0
 * http://sizzlejs.com/
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2015-04-10
 */
 (function( window ) {

 	var i,
 	support,
 	Expr,
 	getText,
 	isXML,
 	tokenize,
 	compile,
 	select,
 	outermostContext,
 	sortInput,
 	hasDuplicate,

	// Local document vars
	setDocument,
	document,
	docElem,
	documentIsHTML,
	rbuggyQSA,
	rbuggyMatches,
	matches,
	contains,

	// Instance-specific data
	expando = "sizzle" + 1 * new Date(),
	preferredDoc = window.document,
	dirruns = 0,
	done = 0,
	classCache = createCache(),
	tokenCache = createCache(),
	compilerCache = createCache(),
	sortOrder = function( a, b ) {
		if ( a === b ) {
			hasDuplicate = true;
		}
		return 0;
	},

	// General-purpose constants
	MAX_NEGATIVE = 1 << 31,

	// Instance methods
	hasOwn = ({}).hasOwnProperty,
	arr = [],
	pop = arr.pop,
	push_native = arr.push,
	push = arr.push,
	slice = arr.slice,
	// Use a stripped-down indexOf as it's faster than native
	// http://jsperf.com/thor-indexof-vs-for/5
	indexOf = function( list, elem ) {
		var i = 0,
		len = list.length;
		for ( ; i < len; i++ ) {
			if ( list[i] === elem ) {
				return i;
			}
		}
		return -1;
	},

	booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",

	// Regular expressions

	// http://www.w3.org/TR/css3-selectors/#whitespace
	whitespace = "[\\x20\\t\\r\\n\\f]",

	// http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
	identifier = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",

	// Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
	attributes = "\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace +
		// Operator (capture 2)
		"*([*^$|!~]?=)" + whitespace +
		// "Attribute values must be CSS identifiers [capture 5] or strings [capture 3 or capture 4]"
		"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace +
"*\\]",

pseudos = ":(" + identifier + ")(?:\\((" +
		// To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
		// 1. quoted (capture 3; capture 4 or capture 5)
		"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +
		// 2. simple (capture 6)
		"((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +
		// 3. anything else (capture 2)
		".*" +
		")\\)|)",

	// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
	rwhitespace = new RegExp( whitespace + "+", "g" ),
	rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),

	rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
	rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*" ),

	rattributeQuotes = new RegExp( "=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g" ),

	rpseudo = new RegExp( pseudos ),
	ridentifier = new RegExp( "^" + identifier + "$" ),

	matchExpr = {
		"ID": new RegExp( "^#(" + identifier + ")" ),
		"CLASS": new RegExp( "^\\.(" + identifier + ")" ),
		"TAG": new RegExp( "^(" + identifier + "|[*])" ),
		"ATTR": new RegExp( "^" + attributes ),
		"PSEUDO": new RegExp( "^" + pseudos ),
		"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
			"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
				"*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
		"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),
		// For use in libraries implementing .is()
		// We use this for POS matching in `select`
		"needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
			whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
	},

	rinputs = /^(?:input|select|textarea|button)$/i,
	rheader = /^h\d$/i,

	rnative = /^[^{]+\{\s*\[native \w/,

	// Easily-parseable/retrievable ID or TAG or CLASS selectors
	rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

	rsibling = /[+~]/,
	rescape = /'|\\/g,

	// CSS escapes http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
	runescape = new RegExp( "\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig" ),
	funescape = function( _, escaped, escapedWhitespace ) {
		var high = "0x" + escaped - 0x10000;
		// NaN means non-codepoint
		// Support: Firefox<24
		// Workaround erroneous numeric interpretation of +"0x"
		return high !== high || escapedWhitespace ?
		escaped :
		high < 0 ?
				// BMP codepoint
				String.fromCharCode( high + 0x10000 ) :
				// Supplemental Plane codepoint (surrogate pair)
				String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
			},

	// Used for iframes
	// See setDocument()
	// Removing the function wrapper causes a "Permission Denied"
	// error in IE
	unloadHandler = function() {
		setDocument();
	};

// Optimize for push.apply( _, NodeList )
try {
	push.apply(
		(arr = slice.call( preferredDoc.childNodes )),
		preferredDoc.childNodes
		);
	// Support: Android<4.0
	// Detect silently failing push.apply
	arr[ preferredDoc.childNodes.length ].nodeType;
} catch ( e ) {
	push = { apply: arr.length ?

		// Leverage slice if possible
		function( target, els ) {
			push_native.apply( target, slice.call(els) );
		} :

		// Support: IE<9
		// Otherwise append directly
		function( target, els ) {
			var j = target.length,
			i = 0;
			// Can't trust NodeList.length
			while ( (target[j++] = els[i++]) ) {}
				target.length = j - 1;
		}
	};
}

function Sizzle( selector, context, results, seed ) {
	var m, i, elem, nid, match, groups, newSelector,
	newContext = context && context.ownerDocument,

		// nodeType defaults to 9, since context defaults to document
		nodeType = context ? context.nodeType : 9;

		results = results || [];

	// Return early from calls with invalid selector or context
	if ( typeof selector !== "string" || !selector ||
		nodeType !== 1 && nodeType !== 9 && nodeType !== 11 ) {

		return results;
}

	// Try to shortcut find operations (as opposed to filters) in HTML documents
	if ( !seed ) {

		if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
			setDocument( context );
		}
		context = context || document;

		if ( documentIsHTML ) {

			// If the selector is sufficiently simple, try using a "get*By*" DOM method
			// (excepting DocumentFragment context, where the methods don't exist)
			if ( nodeType !== 11 && (match = rquickExpr.exec( selector )) ) {

				// ID selector
				if ( (m = match[1]) ) {

					// Document context
					if ( nodeType === 9 ) {
						if ( (elem = context.getElementById( m )) ) {

							// Support: IE, Opera, Webkit
							// TODO: identify versions
							// getElementById can match elements by name instead of ID
							if ( elem.id === m ) {
								results.push( elem );
								return results;
							}
						} else {
							return results;
						}

					// Element context
				} else {

						// Support: IE, Opera, Webkit
						// TODO: identify versions
						// getElementById can match elements by name instead of ID
						if ( newContext && (elem = newContext.getElementById( m )) &&
							contains( context, elem ) &&
							elem.id === m ) {

							results.push( elem );
						return results;
					}
				}

				// Type selector
			} else if ( match[2] ) {
				push.apply( results, context.getElementsByTagName( selector ) );
				return results;

				// Class selector
			} else if ( (m = match[3]) && support.getElementsByClassName &&
				context.getElementsByClassName ) {

				push.apply( results, context.getElementsByClassName( m ) );
				return results;
			}
		}

			// Take advantage of querySelectorAll
			if ( support.qsa &&
				!compilerCache[ selector + " " ] &&
				(!rbuggyQSA || !rbuggyQSA.test( selector )) ) {

				if ( nodeType !== 1 ) {
					newContext = context;
					newSelector = selector;

				// qSA looks outside Element context, which is not what we want
				// Thanks to Andrew Dupont for this workaround technique
				// Support: IE <=8
				// Exclude object elements
			} else if ( context.nodeName.toLowerCase() !== "object" ) {

					// Capture the context ID, setting it first if necessary
					if ( (nid = context.getAttribute( "id" )) ) {
						nid = nid.replace( rescape, "\\$&" );
					} else {
						context.setAttribute( "id", (nid = expando) );
					}

					// Prefix every selector in the list
					groups = tokenize( selector );
					i = groups.length;
					while ( i-- ) {
						groups[i] = "[id='" + nid + "'] " + toSelector( groups[i] );
					}
					newSelector = groups.join( "," );

					// Expand context for sibling selectors
					newContext = rsibling.test( selector ) && testContext( context.parentNode ) ||
					context;
				}

				if ( newSelector ) {
					try {
						push.apply( results,
							newContext.querySelectorAll( newSelector )
							);
						return results;
					} catch ( qsaError ) {
					} finally {
						if ( nid === expando ) {
							context.removeAttribute( "id" );
						}
					}
				}
			}
		}
	}

	// All others
	return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
 * Create key-value caches of limited size
 * @returns {function(string, object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
 function createCache() {
 	var keys = [];

 	function cache( key, value ) {
		// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
		if ( keys.push( key + " " ) > Expr.cacheLength ) {
			// Only keep the most recent entries
			delete cache[ keys.shift() ];
		}
		return (cache[ key + " " ] = value);
	}
	return cache;
}

/**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
 function markFunction( fn ) {
 	fn[ expando ] = true;
 	return fn;
 }

/**
 * Support testing using an element
 * @param {Function} fn Passed the created div and expects a boolean result
 */
 function assert( fn ) {
 	var div = document.createElement("div");

 	try {
 		return !!fn( div );
 	} catch (e) {
 		return false;
 	} finally {
		// Remove from its parent by default
		if ( div.parentNode ) {
			div.parentNode.removeChild( div );
		}
		// release memory in IE
		div = null;
	}
}

/**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
 function addHandle( attrs, handler ) {
 	var arr = attrs.split("|"),
 	i = attrs.length;

 	while ( i-- ) {
 		Expr.attrHandle[ arr[i] ] = handler;
 	}
 }

/**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
 function siblingCheck( a, b ) {
 	var cur = b && a,
 	diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
 	( ~b.sourceIndex || MAX_NEGATIVE ) -
 	( ~a.sourceIndex || MAX_NEGATIVE );

	// Use IE sourceIndex if available on both nodes
	if ( diff ) {
		return diff;
	}

	// Check if b follows a
	if ( cur ) {
		while ( (cur = cur.nextSibling) ) {
			if ( cur === b ) {
				return -1;
			}
		}
	}

	return a ? 1 : -1;
}

/**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
 function createInputPseudo( type ) {
 	return function( elem ) {
 		var name = elem.nodeName.toLowerCase();
 		return name === "input" && elem.type === type;
 	};
 }

/**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
 function createButtonPseudo( type ) {
 	return function( elem ) {
 		var name = elem.nodeName.toLowerCase();
 		return (name === "input" || name === "button") && elem.type === type;
 	};
 }

/**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
 function createPositionalPseudo( fn ) {
 	return markFunction(function( argument ) {
 		argument = +argument;
 		return markFunction(function( seed, matches ) {
 			var j,
 			matchIndexes = fn( [], seed.length, argument ),
 			i = matchIndexes.length;

			// Match elements found at the specified indexes
			while ( i-- ) {
				if ( seed[ (j = matchIndexes[i]) ] ) {
					seed[j] = !(matches[j] = seed[j]);
				}
			}
		});
 	});
 }

/**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
 function testContext( context ) {
 	return context && typeof context.getElementsByTagName !== "undefined" && context;
 }

// Expose support vars for convenience
support = Sizzle.support = {};

/**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
 isXML = Sizzle.isXML = function( elem ) {
	// documentElement is verified for cases where it doesn't yet exist
	// (such as loading iframes in IE - #4833)
	var documentElement = elem && (elem.ownerDocument || elem).documentElement;
	return documentElement ? documentElement.nodeName !== "HTML" : false;
};

/**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
 setDocument = Sizzle.setDocument = function( node ) {
 	var hasCompare, parent,
 	doc = node ? node.ownerDocument || node : preferredDoc;

	// Return early if doc is invalid or already selected
	if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
		return document;
	}

	// Update global variables
	document = doc;
	docElem = document.documentElement;
	documentIsHTML = !isXML( document );

	// Support: IE 9 - 11
	// Accessing iframe documents after unload throws "permission denied" errors (jQuery #13936)
	// Limit the fix to IE with document.documentMode and IE >=9 with document.defaultView
	if ( document.documentMode && (parent = document.defaultView) && parent.top !== parent ) {
		// Support: IE 11
		if ( parent.addEventListener ) {
			parent.addEventListener( "unload", unloadHandler, false );

		// Support: IE 9 - 10 only
	} else if ( parent.attachEvent ) {
		parent.attachEvent( "onunload", unloadHandler );
	}
}

	/* Attributes
	---------------------------------------------------------------------- */

	// Support: IE<8
	// Verify that getAttribute really returns attributes and not properties
	// (excepting IE8 booleans)
	support.attributes = assert(function( div ) {
		div.className = "i";
		return !div.getAttribute("className");
	});

	/* getElement(s)By*
	---------------------------------------------------------------------- */

	// Check if getElementsByTagName("*") returns only elements
	support.getElementsByTagName = assert(function( div ) {
		div.appendChild( document.createComment("") );
		return !div.getElementsByTagName("*").length;
	});

	// Support: IE<9
	support.getElementsByClassName = rnative.test( document.getElementsByClassName );

	// Support: IE<10
	// Check if getElementById returns elements by name
	// The broken getElementById methods don't pick up programatically-set names,
	// so use a roundabout getElementsByName test
	support.getById = assert(function( div ) {
		docElem.appendChild( div ).id = expando;
		return !document.getElementsByName || !document.getElementsByName( expando ).length;
	});

	// ID find and filter
	if ( support.getById ) {
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var m = context.getElementById( id );
				return m ? [ m ] : [];
			}
		};
		Expr.filter["ID"] = function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				return elem.getAttribute("id") === attrId;
			};
		};
	} else {
		// Support: IE6/7
		// getElementById is not reliable as a find shortcut
		delete Expr.find["ID"];

		Expr.filter["ID"] =  function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				var node = typeof elem.getAttributeNode !== "undefined" &&
				elem.getAttributeNode("id");
				return node && node.value === attrId;
			};
		};
	}

	// Tag
	Expr.find["TAG"] = support.getElementsByTagName ?
	function( tag, context ) {
		if ( typeof context.getElementsByTagName !== "undefined" ) {
			return context.getElementsByTagName( tag );

			// DocumentFragment nodes don't have gEBTN
		} else if ( support.qsa ) {
			return context.querySelectorAll( tag );
		}
	} :

	function( tag, context ) {
		var elem,
		tmp = [],
		i = 0,
				// By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
				results = context.getElementsByTagName( tag );

			// Filter out possible comments
			if ( tag === "*" ) {
				while ( (elem = results[i++]) ) {
					if ( elem.nodeType === 1 ) {
						tmp.push( elem );
					}
				}

				return tmp;
			}
			return results;
		};

	// Class
	Expr.find["CLASS"] = support.getElementsByClassName && function( className, context ) {
		if ( typeof context.getElementsByClassName !== "undefined" && documentIsHTML ) {
			return context.getElementsByClassName( className );
		}
	};

	/* QSA/matchesSelector
	---------------------------------------------------------------------- */

	// QSA and matchesSelector support

	// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
	rbuggyMatches = [];

	// qSa(:focus) reports false when true (Chrome 21)
	// We allow this because of a bug in IE8/9 that throws an error
	// whenever `document.activeElement` is accessed on an iframe
	// So, we allow :focus to pass through QSA all the time to avoid the IE error
	// See http://bugs.jquery.com/ticket/13378
	rbuggyQSA = [];

	if ( (support.qsa = rnative.test( document.querySelectorAll )) ) {
		// Build QSA regex
		// Regex strategy adopted from Diego Perini
		assert(function( div ) {
			// Select is set to empty string on purpose
			// This is to test IE's treatment of not explicitly
			// setting a boolean content attribute,
			// since its presence should be enough
			// http://bugs.jquery.com/ticket/12359
			docElem.appendChild( div ).innerHTML = "<a id='" + expando + "'></a>" +
			"<select id='" + expando + "-\r\\' msallowcapture=''>" +
			"<option selected=''></option></select>";

			// Support: IE8, Opera 11-12.16
			// Nothing should be selected when empty strings follow ^= or $= or *=
			// The test attribute must be unknown in Opera but "safe" for WinRT
			// http://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
			if ( div.querySelectorAll("[msallowcapture^='']").length ) {
				rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
			}

			// Support: IE8
			// Boolean attributes and "value" are not treated correctly
			if ( !div.querySelectorAll("[selected]").length ) {
				rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
			}

			// Support: Chrome<29, Android<4.4, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.8+
			if ( !div.querySelectorAll( "[id~=" + expando + "-]" ).length ) {
				rbuggyQSA.push("~=");
			}

			// Webkit/Opera - :checked should return selected option elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			// IE8 throws error here and will not see later tests
			if ( !div.querySelectorAll(":checked").length ) {
				rbuggyQSA.push(":checked");
			}

			// Support: Safari 8+, iOS 8+
			// https://bugs.webkit.org/show_bug.cgi?id=136851
			// In-page `selector#id sibing-combinator selector` fails
			if ( !div.querySelectorAll( "a#" + expando + "+*" ).length ) {
				rbuggyQSA.push(".#.+[+~]");
			}
		});

assert(function( div ) {
			// Support: Windows 8 Native Apps
			// The type and name attributes are restricted during .innerHTML assignment
			var input = document.createElement("input");
			input.setAttribute( "type", "hidden" );
			div.appendChild( input ).setAttribute( "name", "D" );

			// Support: IE8
			// Enforce case-sensitivity of name attribute
			if ( div.querySelectorAll("[name=d]").length ) {
				rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
			}

			// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
			// IE8 throws error here and will not see later tests
			if ( !div.querySelectorAll(":enabled").length ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Opera 10-11 does not throw on post-comma invalid pseudos
			div.querySelectorAll("*,:x");
			rbuggyQSA.push(",.*:");
		});
}

if ( (support.matchesSelector = rnative.test( (matches = docElem.matches ||
	docElem.webkitMatchesSelector ||
	docElem.mozMatchesSelector ||
	docElem.oMatchesSelector ||
	docElem.msMatchesSelector) )) ) {

	assert(function( div ) {
			// Check to see if it's possible to do matchesSelector
			// on a disconnected node (IE 9)
			support.disconnectedMatch = matches.call( div, "div" );

			// This should fail with an exception
			// Gecko does not error, returns false instead
			matches.call( div, "[s!='']:x" );
			rbuggyMatches.push( "!=", pseudos );
		});
}

rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join("|") );
rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join("|") );

	/* Contains
	---------------------------------------------------------------------- */
	hasCompare = rnative.test( docElem.compareDocumentPosition );

	// Element contains another
	// Purposefully self-exclusive
	// As in, an element does not contain itself
	contains = hasCompare || rnative.test( docElem.contains ) ?
	function( a, b ) {
		var adown = a.nodeType === 9 ? a.documentElement : a,
		bup = b && b.parentNode;
		return a === bup || !!( bup && bup.nodeType === 1 && (
			adown.contains ?
			adown.contains( bup ) :
			a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
			));
	} :
	function( a, b ) {
		if ( b ) {
			while ( (b = b.parentNode) ) {
				if ( b === a ) {
					return true;
				}
			}
		}
		return false;
	};

	/* Sorting
	---------------------------------------------------------------------- */

	// Document order sorting
	sortOrder = hasCompare ?
	function( a, b ) {

		// Flag for duplicate removal
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		// Sort on method existence if only one input has compareDocumentPosition
		var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
		if ( compare ) {
			return compare;
		}

		// Calculate position if both inputs belong to the same document
		compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?
		a.compareDocumentPosition( b ) :

			// Otherwise we know they are disconnected
			1;

		// Disconnected nodes
		if ( compare & 1 ||
			(!support.sortDetached && b.compareDocumentPosition( a ) === compare) ) {

			// Choose the first element that is related to our preferred document
		if ( a === document || a.ownerDocument === preferredDoc && contains(preferredDoc, a) ) {
			return -1;
		}
		if ( b === document || b.ownerDocument === preferredDoc && contains(preferredDoc, b) ) {
			return 1;
		}

			// Maintain original order
			return sortInput ?
			( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
			0;
		}

		return compare & 4 ? -1 : 1;
	} :
	function( a, b ) {
		// Exit early if the nodes are identical
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		var cur,
		i = 0,
		aup = a.parentNode,
		bup = b.parentNode,
		ap = [ a ],
		bp = [ b ];

		// Parentless nodes are either documents or disconnected
		if ( !aup || !bup ) {
			return a === document ? -1 :
			b === document ? 1 :
			aup ? -1 :
			bup ? 1 :
			sortInput ?
			( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
			0;

		// If the nodes are siblings, we can do a quick check
	} else if ( aup === bup ) {
		return siblingCheck( a, b );
	}

		// Otherwise we need full lists of their ancestors for comparison
		cur = a;
		while ( (cur = cur.parentNode) ) {
			ap.unshift( cur );
		}
		cur = b;
		while ( (cur = cur.parentNode) ) {
			bp.unshift( cur );
		}

		// Walk down the tree looking for a discrepancy
		while ( ap[i] === bp[i] ) {
			i++;
		}

		return i ?
			// Do a sibling check if the nodes have a common ancestor
			siblingCheck( ap[i], bp[i] ) :

			// Otherwise nodes in our document sort first
			ap[i] === preferredDoc ? -1 :
			bp[i] === preferredDoc ? 1 :
			0;
		};

		return document;
	};

	Sizzle.matches = function( expr, elements ) {
		return Sizzle( expr, null, null, elements );
	};

	Sizzle.matchesSelector = function( elem, expr ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	// Make sure that attribute selectors are quoted
	expr = expr.replace( rattributeQuotes, "='$1']" );

	if ( support.matchesSelector && documentIsHTML &&
		!compilerCache[ expr + " " ] &&
		( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
		( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

		try {
			var ret = matches.call( elem, expr );

			// IE 9's matchesSelector returns false on disconnected nodes
			if ( ret || support.disconnectedMatch ||
					// As well, disconnected nodes are said to be in a document
					// fragment in IE 9
					elem.document && elem.document.nodeType !== 11 ) {
				return ret;
		}
	} catch (e) {}
}

return Sizzle( expr, document, null, [ elem ] ).length > 0;
};

Sizzle.contains = function( context, elem ) {
	// Set document vars if needed
	if ( ( context.ownerDocument || context ) !== document ) {
		setDocument( context );
	}
	return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	var fn = Expr.attrHandle[ name.toLowerCase() ],
		// Don't get fooled by Object.prototype properties (jQuery #13807)
		val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
		fn( elem, name, !documentIsHTML ) :
		undefined;

		return val !== undefined ?
		val :
		support.attributes || !documentIsHTML ?
		elem.getAttribute( name ) :
		(val = elem.getAttributeNode(name)) && val.specified ?
		val.value :
		null;
	};

	Sizzle.error = function( msg ) {
		throw new Error( "Syntax error, unrecognized expression: " + msg );
	};

/**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
 Sizzle.uniqueSort = function( results ) {
 	var elem,
 	duplicates = [],
 	j = 0,
 	i = 0;

	// Unless we *know* we can detect duplicates, assume their presence
	hasDuplicate = !support.detectDuplicates;
	sortInput = !support.sortStable && results.slice( 0 );
	results.sort( sortOrder );

	if ( hasDuplicate ) {
		while ( (elem = results[i++]) ) {
			if ( elem === results[ i ] ) {
				j = duplicates.push( i );
			}
		}
		while ( j-- ) {
			results.splice( duplicates[ j ], 1 );
		}
	}

	// Clear input after sorting to release objects
	// See https://github.com/jquery/sizzle/pull/225
	sortInput = null;

	return results;
};

/**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
 getText = Sizzle.getText = function( elem ) {
 	var node,
 	ret = "",
 	i = 0,
 	nodeType = elem.nodeType;

 	if ( !nodeType ) {
		// If no nodeType, this is expected to be an array
		while ( (node = elem[i++]) ) {
			// Do not traverse comment nodes
			ret += getText( node );
		}
	} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
		// Use textContent for elements
		// innerText usage removed for consistency of new lines (jQuery #11153)
		if ( typeof elem.textContent === "string" ) {
			return elem.textContent;
		} else {
			// Traverse its children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				ret += getText( elem );
			}
		}
	} else if ( nodeType === 3 || nodeType === 4 ) {
		return elem.nodeValue;
	}
	// Do not include comment or processing instruction nodes

	return ret;
};

Expr = Sizzle.selectors = {

	// Can be adjusted by the user
	cacheLength: 50,

	createPseudo: markFunction,

	match: matchExpr,

	attrHandle: {},

	find: {},

	relative: {
		">": { dir: "parentNode", first: true },
		" ": { dir: "parentNode" },
		"+": { dir: "previousSibling", first: true },
		"~": { dir: "previousSibling" }
	},

	preFilter: {
		"ATTR": function( match ) {
			match[1] = match[1].replace( runescape, funescape );

			// Move the given value to match[3] whether quoted or unquoted
			match[3] = ( match[3] || match[4] || match[5] || "" ).replace( runescape, funescape );

			if ( match[2] === "~=" ) {
				match[3] = " " + match[3] + " ";
			}

			return match.slice( 0, 4 );
		},

		"CHILD": function( match ) {
			/* matches from matchExpr["CHILD"]
				1 type (only|nth|...)
				2 what (child|of-type)
				3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
				4 xn-component of xn+y argument ([+-]?\d*n|)
				5 sign of xn-component
				6 x of xn-component
				7 sign of y-component
				8 y of y-component
				*/
				match[1] = match[1].toLowerCase();

				if ( match[1].slice( 0, 3 ) === "nth" ) {
				// nth-* requires argument
				if ( !match[3] ) {
					Sizzle.error( match[0] );
				}

				// numeric x and y parameters for Expr.filter.CHILD
				// remember that false/true cast respectively to 0/1
				match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
				match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );

			// other types prohibit arguments
		} else if ( match[3] ) {
			Sizzle.error( match[0] );
		}

		return match;
	},

	"PSEUDO": function( match ) {
		var excess,
		unquoted = !match[6] && match[2];

		if ( matchExpr["CHILD"].test( match[0] ) ) {
			return null;
		}

			// Accept quoted arguments as-is
			if ( match[3] ) {
				match[2] = match[4] || match[5] || "";

			// Strip excess characters from unquoted arguments
		} else if ( unquoted && rpseudo.test( unquoted ) &&
				// Get excess from tokenize (recursively)
				(excess = tokenize( unquoted, true )) &&
				// advance to the next closing parenthesis
				(excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {

				// excess is a negative index
				match[0] = match[0].slice( 0, excess );
				match[2] = unquoted.slice( 0, excess );
			}

			// Return only captures needed by the pseudo filter method (type and argument)
			return match.slice( 0, 3 );
		}
	},

	filter: {

		"TAG": function( nodeNameSelector ) {
			var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
			return nodeNameSelector === "*" ?
			function() { return true; } :
			function( elem ) {
				return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
			};
		},

		"CLASS": function( className ) {
			var pattern = classCache[ className + " " ];

			return pattern ||
			(pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
			classCache( className, function( elem ) {
				return pattern.test( typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== "undefined" && elem.getAttribute("class") || "" );
			});
		},

		"ATTR": function( name, operator, check ) {
			return function( elem ) {
				var result = Sizzle.attr( elem, name );

				if ( result == null ) {
					return operator === "!=";
				}
				if ( !operator ) {
					return true;
				}

				result += "";

				return operator === "=" ? result === check :
				operator === "!=" ? result !== check :
				operator === "^=" ? check && result.indexOf( check ) === 0 :
				operator === "*=" ? check && result.indexOf( check ) > -1 :
				operator === "$=" ? check && result.slice( -check.length ) === check :
				operator === "~=" ? ( " " + result.replace( rwhitespace, " " ) + " " ).indexOf( check ) > -1 :
				operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
				false;
			};
		},

		"CHILD": function( type, what, argument, first, last ) {
			var simple = type.slice( 0, 3 ) !== "nth",
			forward = type.slice( -4 ) !== "last",
			ofType = what === "of-type";

			return first === 1 && last === 0 ?

				// Shortcut for :nth-*(n)
				function( elem ) {
					return !!elem.parentNode;
				} :

				function( elem, context, xml ) {
					var cache, uniqueCache, outerCache, node, nodeIndex, start,
					dir = simple !== forward ? "nextSibling" : "previousSibling",
					parent = elem.parentNode,
					name = ofType && elem.nodeName.toLowerCase(),
					useCache = !xml && !ofType,
					diff = false;

					if ( parent ) {

						// :(first|last|only)-(child|of-type)
						if ( simple ) {
							while ( dir ) {
								node = elem;
								while ( (node = node[ dir ]) ) {
									if ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) {

										return false;
								}
							}
								// Reverse direction for :only-* (if we haven't yet done so)
								start = dir = type === "only" && !start && "nextSibling";
							}
							return true;
						}

						start = [ forward ? parent.firstChild : parent.lastChild ];

						// non-xml :nth-child(...) stores cache data on `parent`
						if ( forward && useCache ) {

							// Seek `elem` from a previously-cached index

							// ...in a gzip-friendly way
							node = parent;
							outerCache = node[ expando ] || (node[ expando ] = {});

							// Support: IE <9 only
							// Defend against cloned attroperties (jQuery gh-1709)
							uniqueCache = outerCache[ node.uniqueID ] ||
							(outerCache[ node.uniqueID ] = {});

							cache = uniqueCache[ type ] || [];
							nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
							diff = nodeIndex && cache[ 2 ];
							node = nodeIndex && parent.childNodes[ nodeIndex ];

							while ( (node = ++nodeIndex && node && node[ dir ] ||

								// Fallback to seeking `elem` from the start
								(diff = nodeIndex = 0) || start.pop()) ) {

								// When found, cache indexes on `parent` and break
							if ( node.nodeType === 1 && ++diff && node === elem ) {
								uniqueCache[ type ] = [ dirruns, nodeIndex, diff ];
								break;
							}
						}

					} else {
							// Use previously-cached element index if available
							if ( useCache ) {
								// ...in a gzip-friendly way
								node = elem;
								outerCache = node[ expando ] || (node[ expando ] = {});

								// Support: IE <9 only
								// Defend against cloned attroperties (jQuery gh-1709)
								uniqueCache = outerCache[ node.uniqueID ] ||
								(outerCache[ node.uniqueID ] = {});

								cache = uniqueCache[ type ] || [];
								nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
								diff = nodeIndex;
							}

							// xml :nth-child(...)
							// or :nth-last-child(...) or :nth(-last)?-of-type(...)
							if ( diff === false ) {
								// Use the same loop as above to seek `elem` from the start
								while ( (node = ++nodeIndex && node && node[ dir ] ||
									(diff = nodeIndex = 0) || start.pop()) ) {

									if ( ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) &&
										++diff ) {

										// Cache the index of each encountered element
									if ( useCache ) {
										outerCache = node[ expando ] || (node[ expando ] = {});

											// Support: IE <9 only
											// Defend against cloned attroperties (jQuery gh-1709)
											uniqueCache = outerCache[ node.uniqueID ] ||
											(outerCache[ node.uniqueID ] = {});

											uniqueCache[ type ] = [ dirruns, diff ];
										}

										if ( node === elem ) {
											break;
										}
									}
								}
							}
						}

						// Incorporate the offset, then check against cycle size
						diff -= last;
						return diff === first || ( diff % first === 0 && diff / first >= 0 );
					}
				};
			},

			"PSEUDO": function( pseudo, argument ) {
			// pseudo-class names are case-insensitive
			// http://www.w3.org/TR/selectors/#pseudo-classes
			// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
			// Remember that setFilters inherits from pseudos
			var args,
			fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
			Sizzle.error( "unsupported pseudo: " + pseudo );

			// The user may use createPseudo to indicate that
			// arguments are needed to create the filter function
			// just as Sizzle does
			if ( fn[ expando ] ) {
				return fn( argument );
			}

			// But maintain support for old signatures
			if ( fn.length > 1 ) {
				args = [ pseudo, pseudo, "", argument ];
				return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
				markFunction(function( seed, matches ) {
					var idx,
					matched = fn( seed, argument ),
					i = matched.length;
					while ( i-- ) {
						idx = indexOf( seed, matched[i] );
						seed[ idx ] = !( matches[ idx ] = matched[i] );
					}
				}) :
				function( elem ) {
					return fn( elem, 0, args );
				};
			}

			return fn;
		}
	},

	pseudos: {
		// Potentially complex pseudos
		"not": markFunction(function( selector ) {
			// Trim the selector passed to compile
			// to avoid treating leading and trailing
			// spaces as combinators
			var input = [],
			results = [],
			matcher = compile( selector.replace( rtrim, "$1" ) );

			return matcher[ expando ] ?
			markFunction(function( seed, matches, context, xml ) {
				var elem,
				unmatched = matcher( seed, null, xml, [] ),
				i = seed.length;

					// Match elements unmatched by `matcher`
					while ( i-- ) {
						if ( (elem = unmatched[i]) ) {
							seed[i] = !(matches[i] = elem);
						}
					}
				}) :
			function( elem, context, xml ) {
				input[0] = elem;
				matcher( input, null, xml, results );
					// Don't keep the element (issue #299)
					input[0] = null;
					return !results.pop();
				};
			}),

		"has": markFunction(function( selector ) {
			return function( elem ) {
				return Sizzle( selector, elem ).length > 0;
			};
		}),

		"contains": markFunction(function( text ) {
			text = text.replace( runescape, funescape );
			return function( elem ) {
				return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) > -1;
			};
		}),

		// "Whether an element is represented by a :lang() selector
		// is based solely on the element's language value
		// being equal to the identifier C,
		// or beginning with the identifier C immediately followed by "-".
		// The matching of C against the element's language value is performed case-insensitively.
		// The identifier C does not have to be a valid language name."
		// http://www.w3.org/TR/selectors/#lang-pseudo
		"lang": markFunction( function( lang ) {
			// lang value must be a valid identifier
			if ( !ridentifier.test(lang || "") ) {
				Sizzle.error( "unsupported lang: " + lang );
			}
			lang = lang.replace( runescape, funescape ).toLowerCase();
			return function( elem ) {
				var elemLang;
				do {
					if ( (elemLang = documentIsHTML ?
						elem.lang :
						elem.getAttribute("xml:lang") || elem.getAttribute("lang")) ) {

						elemLang = elemLang.toLowerCase();
					return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
				}
			} while ( (elem = elem.parentNode) && elem.nodeType === 1 );
			return false;
		};
	}),

		// Miscellaneous
		"target": function( elem ) {
			var hash = window.location && window.location.hash;
			return hash && hash.slice( 1 ) === elem.id;
		},

		"root": function( elem ) {
			return elem === docElem;
		},

		"focus": function( elem ) {
			return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
		},

		// Boolean properties
		"enabled": function( elem ) {
			return elem.disabled === false;
		},

		"disabled": function( elem ) {
			return elem.disabled === true;
		},

		"checked": function( elem ) {
			// In CSS3, :checked should return both checked and selected elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			var nodeName = elem.nodeName.toLowerCase();
			return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
		},

		"selected": function( elem ) {
			// Accessing this property makes selected-by-default
			// options in Safari work properly
			if ( elem.parentNode ) {
				elem.parentNode.selectedIndex;
			}

			return elem.selected === true;
		},

		// Contents
		"empty": function( elem ) {
			// http://www.w3.org/TR/selectors/#empty-pseudo
			// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
			//   but not by others (comment: 8; processing instruction: 7; etc.)
			// nodeType < 6 works because attributes (2) do not appear as children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				if ( elem.nodeType < 6 ) {
					return false;
				}
			}
			return true;
		},

		"parent": function( elem ) {
			return !Expr.pseudos["empty"]( elem );
		},

		// Element/input types
		"header": function( elem ) {
			return rheader.test( elem.nodeName );
		},

		"input": function( elem ) {
			return rinputs.test( elem.nodeName );
		},

		"button": function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === "button" || name === "button";
		},

		"text": function( elem ) {
			var attr;
			return elem.nodeName.toLowerCase() === "input" &&
			elem.type === "text" &&

				// Support: IE<8
				// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
				( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text" );
			},

		// Position-in-collection
		"first": createPositionalPseudo(function() {
			return [ 0 ];
		}),

		"last": createPositionalPseudo(function( matchIndexes, length ) {
			return [ length - 1 ];
		}),

		"eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
			return [ argument < 0 ? argument + length : argument ];
		}),

		"even": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 0;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"odd": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 1;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; --i >= 0; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; ++i < length; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		})
	}
};

Expr.pseudos["nth"] = Expr.pseudos["eq"];

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
	Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
	Expr.pseudos[ i ] = createButtonPseudo( i );
}

// Easy API for creating new setFilters
function setFilters() {}
setFilters.prototype = Expr.filters = Expr.pseudos;
Expr.setFilters = new setFilters();

tokenize = Sizzle.tokenize = function( selector, parseOnly ) {
	var matched, match, tokens, type,
	soFar, groups, preFilters,
	cached = tokenCache[ selector + " " ];

	if ( cached ) {
		return parseOnly ? 0 : cached.slice( 0 );
	}

	soFar = selector;
	groups = [];
	preFilters = Expr.preFilter;

	while ( soFar ) {

		// Comma and first run
		if ( !matched || (match = rcomma.exec( soFar )) ) {
			if ( match ) {
				// Don't consume trailing commas as valid
				soFar = soFar.slice( match[0].length ) || soFar;
			}
			groups.push( (tokens = []) );
		}

		matched = false;

		// Combinators
		if ( (match = rcombinators.exec( soFar )) ) {
			matched = match.shift();
			tokens.push({
				value: matched,
				// Cast descendant combinators to space
				type: match[0].replace( rtrim, " " )
			});
			soFar = soFar.slice( matched.length );
		}

		// Filters
		for ( type in Expr.filter ) {
			if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
				(match = preFilters[ type ]( match ))) ) {
				matched = match.shift();
			tokens.push({
				value: matched,
				type: type,
				matches: match
			});
			soFar = soFar.slice( matched.length );
		}
	}

	if ( !matched ) {
		break;
	}
}

	// Return the length of the invalid excess
	// if we're just parsing
	// Otherwise, throw an error or return tokens
	return parseOnly ?
	soFar.length :
	soFar ?
	Sizzle.error( selector ) :
			// Cache the tokens
			tokenCache( selector, groups ).slice( 0 );
		};

		function toSelector( tokens ) {
			var i = 0,
			len = tokens.length,
			selector = "";
			for ( ; i < len; i++ ) {
				selector += tokens[i].value;
			}
			return selector;
		}

		function addCombinator( matcher, combinator, base ) {
			var dir = combinator.dir,
			checkNonElements = base && dir === "parentNode",
			doneName = done++;

			return combinator.first ?
		// Check against closest ancestor/preceding element
		function( elem, context, xml ) {
			while ( (elem = elem[ dir ]) ) {
				if ( elem.nodeType === 1 || checkNonElements ) {
					return matcher( elem, context, xml );
				}
			}
		} :

		// Check against all ancestor/preceding elements
		function( elem, context, xml ) {
			var oldCache, uniqueCache, outerCache,
			newCache = [ dirruns, doneName ];

			// We can't set arbitrary data on XML nodes, so they don't benefit from combinator caching
			if ( xml ) {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						if ( matcher( elem, context, xml ) ) {
							return true;
						}
					}
				}
			} else {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						outerCache = elem[ expando ] || (elem[ expando ] = {});

						// Support: IE <9 only
						// Defend against cloned attroperties (jQuery gh-1709)
						uniqueCache = outerCache[ elem.uniqueID ] || (outerCache[ elem.uniqueID ] = {});

						if ( (oldCache = uniqueCache[ dir ]) &&
							oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

							// Assign to newCache so results back-propagate to previous elements
						return (newCache[ 2 ] = oldCache[ 2 ]);
					} else {
							// Reuse newcache so results back-propagate to previous elements
							uniqueCache[ dir ] = newCache;

							// A match means we're done; a fail means we have to keep checking
							if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {
								return true;
							}
						}
					}
				}
			}
		};
	}

	function elementMatcher( matchers ) {
		return matchers.length > 1 ?
		function( elem, context, xml ) {
			var i = matchers.length;
			while ( i-- ) {
				if ( !matchers[i]( elem, context, xml ) ) {
					return false;
				}
			}
			return true;
		} :
		matchers[0];
	}

	function multipleContexts( selector, contexts, results ) {
		var i = 0,
		len = contexts.length;
		for ( ; i < len; i++ ) {
			Sizzle( selector, contexts[i], results );
		}
		return results;
	}

	function condense( unmatched, map, filter, context, xml ) {
		var elem,
		newUnmatched = [],
		i = 0,
		len = unmatched.length,
		mapped = map != null;

		for ( ; i < len; i++ ) {
			if ( (elem = unmatched[i]) ) {
				if ( !filter || filter( elem, context, xml ) ) {
					newUnmatched.push( elem );
					if ( mapped ) {
						map.push( i );
					}
				}
			}
		}

		return newUnmatched;
	}

	function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
		if ( postFilter && !postFilter[ expando ] ) {
			postFilter = setMatcher( postFilter );
		}
		if ( postFinder && !postFinder[ expando ] ) {
			postFinder = setMatcher( postFinder, postSelector );
		}
		return markFunction(function( seed, results, context, xml ) {
			var temp, i, elem,
			preMap = [],
			postMap = [],
			preexisting = results.length,

			// Get initial elements from seed or context
			elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),

			// Prefilter to get matcher input, preserving a map for seed-results synchronization
			matcherIn = preFilter && ( seed || !selector ) ?
			condense( elems, preMap, preFilter, context, xml ) :
			elems,

			matcherOut = matcher ?
				// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
				postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

					// ...intermediate processing is necessary
					[] :

					// ...otherwise use results directly
					results :
					matcherIn;

		// Find primary matches
		if ( matcher ) {
			matcher( matcherIn, matcherOut, context, xml );
		}

		// Apply postFilter
		if ( postFilter ) {
			temp = condense( matcherOut, postMap );
			postFilter( temp, [], context, xml );

			// Un-match failing elements by moving them back to matcherIn
			i = temp.length;
			while ( i-- ) {
				if ( (elem = temp[i]) ) {
					matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
				}
			}
		}

		if ( seed ) {
			if ( postFinder || preFilter ) {
				if ( postFinder ) {
					// Get the final matcherOut by condensing this intermediate into postFinder contexts
					temp = [];
					i = matcherOut.length;
					while ( i-- ) {
						if ( (elem = matcherOut[i]) ) {
							// Restore matcherIn since elem is not yet a final match
							temp.push( (matcherIn[i] = elem) );
						}
					}
					postFinder( null, (matcherOut = []), temp, xml );
				}

				// Move matched elements from seed to results to keep them synchronized
				i = matcherOut.length;
				while ( i-- ) {
					if ( (elem = matcherOut[i]) &&
						(temp = postFinder ? indexOf( seed, elem ) : preMap[i]) > -1 ) {

						seed[temp] = !(results[temp] = elem);
				}
			}
		}

		// Add elements to results, through postFinder if defined
	} else {
		matcherOut = condense(
			matcherOut === results ?
			matcherOut.splice( preexisting, matcherOut.length ) :
			matcherOut
			);
		if ( postFinder ) {
			postFinder( null, results, matcherOut, xml );
		} else {
			push.apply( results, matcherOut );
		}
	}
});
}

function matcherFromTokens( tokens ) {
	var checkContext, matcher, j,
	len = tokens.length,
	leadingRelative = Expr.relative[ tokens[0].type ],
	implicitRelative = leadingRelative || Expr.relative[" "],
	i = leadingRelative ? 1 : 0,

		// The foundational matcher ensures that elements are reachable from top-level context(s)
		matchContext = addCombinator( function( elem ) {
			return elem === checkContext;
		}, implicitRelative, true ),
		matchAnyContext = addCombinator( function( elem ) {
			return indexOf( checkContext, elem ) > -1;
		}, implicitRelative, true ),
		matchers = [ function( elem, context, xml ) {
			var ret = ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
				(checkContext = context).nodeType ?
				matchContext( elem, context, xml ) :
				matchAnyContext( elem, context, xml ) );
			// Avoid hanging onto element (issue #299)
			checkContext = null;
			return ret;
		} ];

		for ( ; i < len; i++ ) {
			if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
				matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
			} else {
				matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );

			// Return special upon seeing a positional matcher
			if ( matcher[ expando ] ) {
				// Find the next relative operator (if any) for proper handling
				j = ++i;
				for ( ; j < len; j++ ) {
					if ( Expr.relative[ tokens[j].type ] ) {
						break;
					}
				}
				return setMatcher(
					i > 1 && elementMatcher( matchers ),
					i > 1 && toSelector(
						// If the preceding token was a descendant combinator, insert an implicit any-element `*`
						tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === " " ? "*" : "" })
						).replace( rtrim, "$1" ),
					matcher,
					i < j && matcherFromTokens( tokens.slice( i, j ) ),
					j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
					j < len && toSelector( tokens )
					);
			}
			matchers.push( matcher );
		}
	}

	return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
	var bySet = setMatchers.length > 0,
	byElement = elementMatchers.length > 0,
	superMatcher = function( seed, context, xml, results, outermost ) {
		var elem, j, matcher,
		matchedCount = 0,
		i = "0",
		unmatched = seed && [],
		setMatched = [],
		contextBackup = outermostContext,
				// We must always have either seed elements or outermost context
				elems = seed || byElement && Expr.find["TAG"]( "*", outermost ),
				// Use integer dirruns iff this is the outermost matcher
				dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
				len = elems.length;

				if ( outermost ) {
					outermostContext = context === document || context || outermost;
				}

			// Add elements passing elementMatchers directly to results
			// Support: IE<9, Safari
			// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
			for ( ; i !== len && (elem = elems[i]) != null; i++ ) {
				if ( byElement && elem ) {
					j = 0;
					if ( !context && elem.ownerDocument !== document ) {
						setDocument( elem );
						xml = !documentIsHTML;
					}
					while ( (matcher = elementMatchers[j++]) ) {
						if ( matcher( elem, context || document, xml) ) {
							results.push( elem );
							break;
						}
					}
					if ( outermost ) {
						dirruns = dirrunsUnique;
					}
				}

				// Track unmatched elements for set filters
				if ( bySet ) {
					// They will have gone through all possible matchers
					if ( (elem = !matcher && elem) ) {
						matchedCount--;
					}

					// Lengthen the array for every element, matched or not
					if ( seed ) {
						unmatched.push( elem );
					}
				}
			}

			// `i` is now the count of elements visited above, and adding it to `matchedCount`
			// makes the latter nonnegative.
			matchedCount += i;

			// Apply set filters to unmatched elements
			// NOTE: This can be skipped if there are no unmatched elements (i.e., `matchedCount`
			// equals `i`), unless we didn't visit _any_ elements in the above loop because we have
			// no element matchers and no seed.
			// Incrementing an initially-string "0" `i` allows `i` to remain a string only in that
			// case, which will result in a "00" `matchedCount` that differs from `i` but is also
			// numerically zero.
			if ( bySet && i !== matchedCount ) {
				j = 0;
				while ( (matcher = setMatchers[j++]) ) {
					matcher( unmatched, setMatched, context, xml );
				}

				if ( seed ) {
					// Reintegrate element matches to eliminate the need for sorting
					if ( matchedCount > 0 ) {
						while ( i-- ) {
							if ( !(unmatched[i] || setMatched[i]) ) {
								setMatched[i] = pop.call( results );
							}
						}
					}

					// Discard index placeholder values to get only actual matches
					setMatched = condense( setMatched );
				}

				// Add matches to results
				push.apply( results, setMatched );

				// Seedless set matches succeeding multiple successful matchers stipulate sorting
				if ( outermost && !seed && setMatched.length > 0 &&
					( matchedCount + setMatchers.length ) > 1 ) {

					Sizzle.uniqueSort( results );
			}
		}

			// Override manipulation of globals by nested matchers
			if ( outermost ) {
				dirruns = dirrunsUnique;
				outermostContext = contextBackup;
			}

			return unmatched;
		};

		return bySet ?
		markFunction( superMatcher ) :
		superMatcher;
	}

	compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {
		var i,
		setMatchers = [],
		elementMatchers = [],
		cached = compilerCache[ selector + " " ];

		if ( !cached ) {
		// Generate a function of recursive functions that can be used to check each element
		if ( !match ) {
			match = tokenize( selector );
		}
		i = match.length;
		while ( i-- ) {
			cached = matcherFromTokens( match[i] );
			if ( cached[ expando ] ) {
				setMatchers.push( cached );
			} else {
				elementMatchers.push( cached );
			}
		}

		// Cache the compiled function
		cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );

		// Save selector and tokenization
		cached.selector = selector;
	}
	return cached;
};

/**
 * A low-level selection function that works with Sizzle's compiled
 *  selector functions
 * @param {String|Function} selector A selector or a pre-compiled
 *  selector function built with Sizzle.compile
 * @param {Element} context
 * @param {Array} [results]
 * @param {Array} [seed] A set of elements to match against
 */
 select = Sizzle.select = function( selector, context, results, seed ) {
 	var i, tokens, token, type, find,
 	compiled = typeof selector === "function" && selector,
 	match = !seed && tokenize( (selector = compiled.selector || selector) );

 	results = results || [];

	// Try to minimize operations if there is only one selector in the list and no seed
	// (the latter of which guarantees us context)
	if ( match.length === 1 ) {

		// Reduce context if the leading compound selector is an ID
		tokens = match[0] = match[0].slice( 0 );
		if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
			support.getById && context.nodeType === 9 && documentIsHTML &&
			Expr.relative[ tokens[1].type ] ) {

			context = ( Expr.find["ID"]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];
		if ( !context ) {
			return results;

			// Precompiled matchers will still verify ancestry, so step up a level
		} else if ( compiled ) {
			context = context.parentNode;
		}

		selector = selector.slice( tokens.shift().value.length );
	}

		// Fetch a seed set for right-to-left matching
		i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
		while ( i-- ) {
			token = tokens[i];

			// Abort if we hit a combinator
			if ( Expr.relative[ (type = token.type) ] ) {
				break;
			}
			if ( (find = Expr.find[ type ]) ) {
				// Search, expanding context for leading sibling combinators
				if ( (seed = find(
					token.matches[0].replace( runescape, funescape ),
					rsibling.test( tokens[0].type ) && testContext( context.parentNode ) || context
					)) ) {

					// If seed is empty or no tokens remain, we can return early
				tokens.splice( i, 1 );
				selector = seed.length && toSelector( tokens );
				if ( !selector ) {
					push.apply( results, seed );
					return results;
				}

				break;
			}
		}
	}
}

	// Compile and execute a filtering function if one is not provided
	// Provide `match` to avoid retokenization if we modified the selector above
	( compiled || compile( selector, match ) )(
		seed,
		context,
		!documentIsHTML,
		results,
		!context || rsibling.test( selector ) && testContext( context.parentNode ) || context
		);
	return results;
};

// One-time assignments

// Sort stability
support.sortStable = expando.split("").sort( sortOrder ).join("") === expando;

// Support: Chrome 14-35+
// Always assume duplicates if they aren't passed to the comparison function
support.detectDuplicates = !!hasDuplicate;

// Initialize against the default document
setDocument();

// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
// Detached nodes confoundingly follow *each other*
support.sortDetached = assert(function( div1 ) {
	// Should return 1, but returns 4 (following)
	return div1.compareDocumentPosition( document.createElement("div") ) & 1;
});

// Support: IE<8
// Prevent attribute/property "interpolation"
// http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !assert(function( div ) {
	div.innerHTML = "<a href='#'></a>";
	return div.firstChild.getAttribute("href") === "#" ;
}) ) {
	addHandle( "type|href|height|width", function( elem, name, isXML ) {
		if ( !isXML ) {
			return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
		}
	});
}

// Support: IE<9
// Use defaultValue in place of getAttribute("value")
if ( !support.attributes || !assert(function( div ) {
	div.innerHTML = "<input/>";
	div.firstChild.setAttribute( "value", "" );
	return div.firstChild.getAttribute( "value" ) === "";
}) ) {
	addHandle( "value", function( elem, name, isXML ) {
		if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
			return elem.defaultValue;
		}
	});
}

// Support: IE<9
// Use getAttributeNode to fetch booleans when getAttribute lies
if ( !assert(function( div ) {
	return div.getAttribute("disabled") == null;
}) ) {
	addHandle( booleans, function( elem, name, isXML ) {
		var val;
		if ( !isXML ) {
			return elem[ name ] === true ? name.toLowerCase() :
			(val = elem.getAttributeNode( name )) && val.specified ?
			val.value :
			null;
		}
	});
}

return Sizzle;

})( window );



jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;
jQuery.expr[":"] = jQuery.expr.pseudos;
jQuery.uniqueSort = jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;



var rneedsContext = jQuery.expr.match.needsContext;

var rsingleTag = (/^<([\w-]+)\s*\/?>(?:<\/\1>|)$/);



var risSimple = /^.[^:#\[\.,]*$/;

// Implement the identical functionality for filter and not
function winnow( elements, qualifier, not ) {
	if ( jQuery.isFunction( qualifier ) ) {
		return jQuery.grep( elements, function( elem, i ) {
			/* jshint -W018 */
			return !!qualifier.call( elem, i, elem ) !== not;
		});

	}

	if ( qualifier.nodeType ) {
		return jQuery.grep( elements, function( elem ) {
			return ( elem === qualifier ) !== not;
		});

	}

	if ( typeof qualifier === "string" ) {
		if ( risSimple.test( qualifier ) ) {
			return jQuery.filter( qualifier, elements, not );
		}

		qualifier = jQuery.filter( qualifier, elements );
	}

	return jQuery.grep( elements, function( elem ) {
		return ( indexOf.call( qualifier, elem ) > -1 ) !== not;
	});
}

jQuery.filter = function( expr, elems, not ) {
	var elem = elems[ 0 ];

	if ( not ) {
		expr = ":not(" + expr + ")";
	}

	return elems.length === 1 && elem.nodeType === 1 ?
	jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [] :
	jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
		return elem.nodeType === 1;
	}));
};

jQuery.fn.extend({
	find: function( selector ) {
		var i,
		len = this.length,
		ret = [],
		self = this;

		if ( typeof selector !== "string" ) {
			return this.pushStack( jQuery( selector ).filter(function() {
				for ( i = 0; i < len; i++ ) {
					if ( jQuery.contains( self[ i ], this ) ) {
						return true;
					}
				}
			}) );
		}

		for ( i = 0; i < len; i++ ) {
			jQuery.find( selector, self[ i ], ret );
		}

		return this.pushStack( len > 1 ? jQuery.uniqueSort( ret ) : ret );
	},
	filter: function( selector ) {
		return this.pushStack( winnow(this, selector || [], false) );
	},
	not: function( selector ) {
		return this.pushStack( winnow(this, selector || [], true) );
	},
	is: function( selector ) {
		return !!winnow(
			this,

			// If this is a positional/relative selector, check membership in the returned set
			// so $("p:first").is("p:last") won't return true for a doc with two "p".
			typeof selector === "string" && rneedsContext.test( selector ) ?
			jQuery( selector ) :
			selector || [],
			false
			).length;
	}
});


// Initialize a jQuery object


// A central reference to the root jQuery(document)
var rootjQuery,

	// A simple way to check for HTML strings
	// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
	// Strict HTML recognition (#11290: must start with <)
	// Shortcut simple #id case for speed
	rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,

	init = jQuery.fn.init = function( selector, context, root ) {
		var match, elem;

		// HANDLE: $(""), $(null), $(undefined), $(false)
		if ( !selector ) {
			return this;
		}

		// init accepts an alternate rootjQuery
		// so migrate can support jQuery.sub (gh-2101)
		root = root || rootjQuery;

		// Handle HTML strings
		if ( typeof selector === "string" ) {
			if ( selector[0] === "<" &&
				selector[ selector.length - 1 ] === ">" &&
				selector.length >= 3 ) {

				// Assume that strings that start and end with <> are HTML and skip the regex check
			match = [ null, selector, null ];

		} else {
			match = rquickExpr.exec( selector );
		}

			// Match html or make sure no context is specified for #id
			if ( match && (match[1] || !context) ) {

				// HANDLE: $(html) -> $(array)
				if ( match[1] ) {
					context = context instanceof jQuery ? context[0] : context;

					// Option to run scripts is true for back-compat
					// Intentionally let the error be thrown if parseHTML is not present
					jQuery.merge( this, jQuery.parseHTML(
						match[1],
						context && context.nodeType ? context.ownerDocument || context : document,
						true
						) );

					// HANDLE: $(html, props)
					if ( rsingleTag.test( match[1] ) && jQuery.isPlainObject( context ) ) {
						for ( match in context ) {
							// Properties of context are called as methods if possible
							if ( jQuery.isFunction( this[ match ] ) ) {
								this[ match ]( context[ match ] );

							// ...and otherwise set as attributes
						} else {
							this.attr( match, context[ match ] );
						}
					}
				}

				return this;

				// HANDLE: $(#id)
			} else {
				elem = document.getElementById( match[2] );

				if ( elem ) {
						// Inject the element directly into the jQuery object
						this[0] = elem;
						this.length = 1;
					}
					return this;
				}

			// HANDLE: $(expr, $(...))
		} else if ( !context || context.jquery ) {
			return ( context || root ).find( selector );

			// HANDLE: $(expr, context)
			// (which is just equivalent to: $(context).find(expr)
		} else {
			return this.constructor( context ).find( selector );
		}

		// HANDLE: $(DOMElement)
	} else if ( selector.nodeType ) {
		this[0] = selector;
		this.length = 1;
		return this;

		// HANDLE: $(function)
		// Shortcut for document ready
	} else if ( jQuery.isFunction( selector ) ) {
		return root.ready !== undefined ?
		root.ready( selector ) :
				// Execute immediately if ready is not present
				selector( jQuery );
			}

			return jQuery.makeArray( selector, this );
		};

// Give the init function the jQuery prototype for later instantiation
init.prototype = jQuery.fn;

// Initialize central reference
rootjQuery = jQuery( document );


var rparentsprev = /^(?:parents|prev(?:Until|All))/,
	// Methods guaranteed to produce a unique set when starting from a unique set
	guaranteedUnique = {
		children: true,
		contents: true,
		next: true,
		prev: true
	};

	jQuery.extend({
		dir: function( elem, dir, until ) {
			var matched = [],
			truncate = until !== undefined;

			while ( (elem = elem[ dir ]) && elem.nodeType !== 9 ) {
				if ( elem.nodeType === 1 ) {
					if ( truncate && jQuery( elem ).is( until ) ) {
						break;
					}
					matched.push( elem );
				}
			}
			return matched;
		},

		sibling: function( n, elem ) {
			var matched = [];

			for ( ; n; n = n.nextSibling ) {
				if ( n.nodeType === 1 && n !== elem ) {
					matched.push( n );
				}
			}

			return matched;
		}
	});

	jQuery.fn.extend({
		has: function( target ) {
			var targets = jQuery( target, this ),
			l = targets.length;

			return this.filter(function() {
				var i = 0;
				for ( ; i < l; i++ ) {
					if ( jQuery.contains( this, targets[i] ) ) {
						return true;
					}
				}
			});
		},

		closest: function( selectors, context ) {
			var cur,
			i = 0,
			l = this.length,
			matched = [],
			pos = rneedsContext.test( selectors ) || typeof selectors !== "string" ?
			jQuery( selectors, context || this.context ) :
			0;

			for ( ; i < l; i++ ) {
				for ( cur = this[i]; cur && cur !== context; cur = cur.parentNode ) {
				// Always skip document fragments
				if ( cur.nodeType < 11 && (pos ?
					pos.index(cur) > -1 :

					// Don't pass non-elements to Sizzle
					cur.nodeType === 1 &&
					jQuery.find.matchesSelector(cur, selectors)) ) {

					matched.push( cur );
				break;
			}
		}
	}

	return this.pushStack( matched.length > 1 ? jQuery.uniqueSort( matched ) : matched );
},

	// Determine the position of an element within the set
	index: function( elem ) {

		// No argument, return index in parent
		if ( !elem ) {
			return ( this[ 0 ] && this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;
		}

		// Index in selector
		if ( typeof elem === "string" ) {
			return indexOf.call( jQuery( elem ), this[ 0 ] );
		}

		// Locate the position of the desired element
		return indexOf.call( this,

			// If it receives a jQuery object, the first element is used
			elem.jquery ? elem[ 0 ] : elem
			);
	},

	add: function( selector, context ) {
		return this.pushStack(
			jQuery.uniqueSort(
				jQuery.merge( this.get(), jQuery( selector, context ) )
				)
			);
	},

	addBack: function( selector ) {
		return this.add( selector == null ?
			this.prevObject : this.prevObject.filter(selector)
			);
	}
});

function sibling( cur, dir ) {
	while ( (cur = cur[dir]) && cur.nodeType !== 1 ) {}
		return cur;
}

jQuery.each({
	parent: function( elem ) {
		var parent = elem.parentNode;
		return parent && parent.nodeType !== 11 ? parent : null;
	},
	parents: function( elem ) {
		return jQuery.dir( elem, "parentNode" );
	},
	parentsUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "parentNode", until );
	},
	next: function( elem ) {
		return sibling( elem, "nextSibling" );
	},
	prev: function( elem ) {
		return sibling( elem, "previousSibling" );
	},
	nextAll: function( elem ) {
		return jQuery.dir( elem, "nextSibling" );
	},
	prevAll: function( elem ) {
		return jQuery.dir( elem, "previousSibling" );
	},
	nextUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "nextSibling", until );
	},
	prevUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "previousSibling", until );
	},
	siblings: function( elem ) {
		return jQuery.sibling( ( elem.parentNode || {} ).firstChild, elem );
	},
	children: function( elem ) {
		return jQuery.sibling( elem.firstChild );
	},
	contents: function( elem ) {
		return elem.contentDocument || jQuery.merge( [], elem.childNodes );
	}
}, function( name, fn ) {
	jQuery.fn[ name ] = function( until, selector ) {
		var matched = jQuery.map( this, fn, until );

		if ( name.slice( -5 ) !== "Until" ) {
			selector = until;
		}

		if ( selector && typeof selector === "string" ) {
			matched = jQuery.filter( selector, matched );
		}

		if ( this.length > 1 ) {
			// Remove duplicates
			if ( !guaranteedUnique[ name ] ) {
				jQuery.uniqueSort( matched );
			}

			// Reverse order for parents* and prev-derivatives
			if ( rparentsprev.test( name ) ) {
				matched.reverse();
			}
		}

		return this.pushStack( matched );
	};
});
var rnotwhite = (/\S+/g);



// Convert String-formatted options into Object-formatted ones
function createOptions( options ) {
	var object = {};
	jQuery.each( options.match( rnotwhite ) || [], function( _, flag ) {
		object[ flag ] = true;
	});
	return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
 jQuery.Callbacks = function( options ) {

	// Convert options from String-formatted to Object-formatted if needed
	// (we check in cache first)
	options = typeof options === "string" ?
	createOptions( options ) :
	jQuery.extend( {}, options );

	var // Flag to know if list is currently firing
	firing,
		// Last fire value for non-forgettable lists
		memory,
		// Flag to know if list was already fired
		fired,
		// Flag to prevent firing
		locked,
		// Actual callback list
		list = [],
		// Queue of execution data for repeatable lists
		queue = [],
		// Index of currently firing callback (modified by add/remove as needed)
		firingIndex = -1,
		// Fire callbacks
		fire = function() {

			// Enforce single-firing
			locked = options.once;

			// Execute callbacks for all pending executions,
			// respecting firingIndex overrides and runtime changes
			fired = firing = true;
			for ( ; queue.length; firingIndex = -1 ) {
				memory = queue.shift();
				while ( ++firingIndex < list.length ) {

					// Run callback and check for early termination
					if ( list[ firingIndex ].apply( memory[ 0 ], memory[ 1 ] ) === false &&
						options.stopOnFalse ) {

						// Jump to end and forget the data so .add doesn't re-fire
					firingIndex = list.length;
					memory = false;
				}
			}
		}

			// Forget the data if we're done with it
			if ( !options.memory ) {
				memory = false;
			}

			firing = false;

			// Clean up if we're done firing for good
			if ( locked ) {

				// Keep an empty list if we have data for future add calls
				if ( memory ) {
					list = [];

				// Otherwise, this object is spent
			} else {
				list = "";
			}
		}
	},

		// Actual Callbacks object
		self = {

			// Add a callback or a collection of callbacks to the list
			add: function() {
				if ( list ) {

					// If we have memory from a past run, we should fire after adding
					if ( memory && !firing ) {
						firingIndex = list.length - 1;
						queue.push( memory );
					}

					(function add( args ) {
						jQuery.each( args, function( _, arg ) {
							if ( jQuery.isFunction( arg ) ) {
								if ( !options.unique || !self.has( arg ) ) {
									list.push( arg );
								}
							} else if ( arg && arg.length && jQuery.type( arg ) !== "string" ) {
								// Inspect recursively
								add( arg );
							}
						});
					})( arguments );

					if ( memory && !firing ) {
						fire();
					}
				}
				return this;
			},

			// Remove a callback from the list
			remove: function() {
				jQuery.each( arguments, function( _, arg ) {
					var index;
					while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
						list.splice( index, 1 );

						// Handle firing indexes
						if ( index <= firingIndex ) {
							firingIndex--;
						}
					}
				});
				return this;
			},

			// Check if a given callback is in the list.
			// If no argument is given, return whether or not list has callbacks attached.
			has: function( fn ) {
				return fn ?
				jQuery.inArray( fn, list ) > -1 :
				list.length > 0;
			},

			// Remove all callbacks from the list
			empty: function() {
				if ( list ) {
					list = [];
				}
				return this;
			},

			// Disable .fire and .add
			// Abort any current/pending executions
			// Clear all callbacks and values
			disable: function() {
				locked = queue = [];
				list = memory = "";
				return this;
			},
			disabled: function() {
				return !list;
			},

			// Disable .fire
			// Also disable .add unless we have memory (since it would have no effect)
			// Abort any pending executions
			lock: function() {
				locked = queue = [];
				if ( !memory && !firing ) {
					list = memory = "";
				}
				return this;
			},
			locked: function() {
				return !!locked;
			},

			// Call all callbacks with the given context and arguments
			fireWith: function( context, args ) {
				if ( !locked ) {
					args = args || [];
					args = [ context, args.slice ? args.slice() : args ];
					queue.push( args );
					if ( !firing ) {
						fire();
					}
				}
				return this;
			},

			// Call all the callbacks with the given arguments
			fire: function() {
				self.fireWith( this, arguments );
				return this;
			},

			// To know if the callbacks have already been called at least once
			fired: function() {
				return !!fired;
			}
		};

		return self;
	};


	function Identity( v ) {
		return v;
	}
	function Thrower( ex ) {
		throw ex;
	}

	jQuery.extend({

		Deferred: function( func ) {
			var tuples = [
				// action, add listener, callbacks,
				// ... .then handlers, argument index, [final state]
				[ "notify", "progress", jQuery.Callbacks("memory"),
				jQuery.Callbacks("memory"), 2 ],
				[ "resolve", "done", jQuery.Callbacks("once memory"),
				jQuery.Callbacks("once memory"), 0, "resolved" ],
				[ "reject", "fail", jQuery.Callbacks("once memory"),
				jQuery.Callbacks("once memory"), 1, "rejected" ]
				],
				state = "pending",
				promise = {
					state: function() {
						return state;
					},
					always: function() {
						deferred.done( arguments ).fail( arguments );
						return this;
					},
					"catch": function( fn ) {
						return promise.then( null, fn );
					},
				// Keep pipe for back-compat
				pipe: function( /* fnDone, fnFail, fnProgress */ ) {
					var fns = arguments;

					return jQuery.Deferred(function( newDefer ) {
						jQuery.each( tuples, function( i, tuple ) {
							// Map tuples (progress, done, fail) to arguments (done, fail, progress)
							var fn = jQuery.isFunction( fns[ tuple[ 4 ] ] ) && fns[ tuple[ 4 ] ];

							// deferred.progress(function() { bind to newDefer or newDefer.notify })
							// deferred.done(function() { bind to newDefer or newDefer.resolve })
							// deferred.fail(function() { bind to newDefer or newDefer.reject })
							deferred[ tuple[1] ](function() {
								var returned = fn && fn.apply( this, arguments );
								if ( returned && jQuery.isFunction( returned.promise ) ) {
									returned.promise()
									.progress( newDefer.notify )
									.done( newDefer.resolve )
									.fail( newDefer.reject );
								} else {
									newDefer[ tuple[ 0 ] + "With" ](
										this === promise ? newDefer.promise() : this,
										fn ? [ returned ] : arguments
										);
								}
							});
						});
						fns = null;
					}).promise();
				},
				then: function( onFulfilled, onRejected, onProgress ) {
					var maxDepth = 0;
					function resolve( depth, deferred, handler, special ) {
						return function() {
							var that = this === promise ? undefined : this,
							args = arguments,
							mightThrow = function() {
								var returned, then;

									// Support: Promises/A+ section 2.3.3.3.3
									// https://promisesaplus.com/#point-59
									// Ignore double-resolution attempts
									if ( depth < maxDepth ) {
										return;
									}

									returned = handler.apply( that, args );

									// Support: Promises/A+ section 2.3.1
									// https://promisesaplus.com/#point-48
									if ( returned === deferred.promise() ) {
										throw new TypeError( "Thenable self-resolution" );
									}

									// Support: Promises/A+ sections 2.3.3.1, 3.5
									// https://promisesaplus.com/#point-54
									// https://promisesaplus.com/#point-75
									// Retrieve `then` only once
									then = returned &&

										// Support: Promises/A+ section 2.3.4
										// https://promisesaplus.com/#point-64
										// Only check objects and functions for thenability
										( typeof returned === "object" ||
											typeof returned === "function" ) &&
										returned.then;

									// Handle a returned thenable
									if ( jQuery.isFunction( then ) ) {
										// Special processors (notify) just wait for resolution
										if ( special ) {
											then.call(
												returned,
												resolve( maxDepth, deferred, Identity, special ),
												resolve( maxDepth, deferred, Thrower, special )
												);

										// Normal processors (resolve) also hook into progress
									} else {

											// ...and disregard older resolution values
											maxDepth++;

											then.call(
												returned,
												resolve( maxDepth, deferred, Identity, special ),
												resolve( maxDepth, deferred, Thrower, special ),
												resolve( maxDepth, deferred, Identity,
													deferred.notify )
												);
										}

									// Handle all other returned values
								} else {
										// Only substitue handlers pass on context
										// and multiple values (non-spec behavior)
										if ( handler !== Identity ) {
											that = undefined;
											args = [ returned ];
										}

										// Process the value(s)
										// Default process is resolve
										( special || deferred.resolveWith )(
											that || deferred.promise(), args );
									}
								},

								// Only normal processors (resolve) catch and reject exceptions
								process = special ?
								mightThrow :
								function() {
									try {
										mightThrow();
									} catch ( e ) {

											// Support: Promises/A+ section 2.3.3.3.4.1
											// https://promisesaplus.com/#point-61
											// Ignore post-resolution exceptions
											if ( depth + 1 >= maxDepth ) {
												// Only substitue handlers pass on context
												// and multiple values (non-spec behavior)
												if ( handler !== Thrower ) {
													that = undefined;
													args = [ e ];
												}

												deferred.rejectWith( that || deferred.promise(),
													args );
											}
										}
									};

							// Support: Promises/A+ section 2.3.3.3.1
							// https://promisesaplus.com/#point-57
							// Re-resolve promises immediately to dodge false rejection from
							// subsequent errors
							if ( depth ) {
								process();
							} else {
								window.setTimeout( process );
							}
						};
					}

					return jQuery.Deferred(function( newDefer ) {
						// progress_handlers.add( ... )
						tuples[ 0 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								jQuery.isFunction( onProgress ) ?
								onProgress :
								Identity,
								newDefer.notifyWith
								)
							);

						// fulfilled_handlers.add( ... )
						tuples[ 1 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								jQuery.isFunction( onFulfilled ) ?
								onFulfilled :
								Identity
								)
							);

						// rejected_handlers.add( ... )
						tuples[ 2 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								jQuery.isFunction( onRejected ) ?
								onRejected :
								Thrower
								)
							);
					}).promise();
				},
				// Get a promise for this deferred
				// If obj is provided, the promise aspect is added to the object
				promise: function( obj ) {
					return obj != null ? jQuery.extend( obj, promise ) : promise;
				}
			},
			deferred = {};

		// Add list-specific methods
		jQuery.each( tuples, function( i, tuple ) {
			var list = tuple[ 2 ],
			stateString = tuple[ 5 ];

			// promise.progress = list.add
			// promise.done = list.add
			// promise.fail = list.add
			promise[ tuple[1] ] = list.add;

			// Handle state
			if ( stateString ) {
				list.add(
					function() {
						// state = "resolved" (i.e., fulfilled)
						// state = "rejected"
						state = stateString;
					},

					// rejected_callbacks.disable
					// fulfilled_callbacks.disable
					tuples[ 3 - i ][ 2 ].disable,

					// progress_callbacks.lock
					tuples[ 0 ][ 2 ].lock
					);
			}

			// progress_handlers.fire
			// fulfilled_handlers.fire
			// rejected_handlers.fire
			list.add( tuple[ 3 ].fire );

			// deferred.notify = function() { deferred.notifyWith(...) }
			// deferred.resolve = function() { deferred.resolveWith(...) }
			// deferred.reject = function() { deferred.rejectWith(...) }
			deferred[ tuple[0] ] = function() {
				deferred[ tuple[0] + "With" ]( this === deferred ? promise : this, arguments );
				return this;
			};

			// deferred.notifyWith = list.fireWith
			// deferred.resolveWith = list.fireWith
			// deferred.rejectWith = list.fireWith
			deferred[ tuple[0] + "With" ] = list.fireWith;
		});

		// Make the deferred a promise
		promise.promise( deferred );

		// Call given func if any
		if ( func ) {
			func.call( deferred, deferred );
		}

		// All done!
		return deferred;
	},

	// Deferred helper
	when: function( subordinate /* , ..., subordinateN */ ) {
		var method,
		i = 0,
		resolveValues = slice.call( arguments ),
		length = resolveValues.length,

			// the count of uncompleted subordinates
			remaining = length !== 1 ||
			( subordinate && jQuery.isFunction( subordinate.promise ) ) ? length : 0,

			// the master Deferred.
			// If resolveValues consist of only a single Deferred, just use that.
			master = remaining === 1 ? subordinate : jQuery.Deferred(),

			// Update function for both resolve and progress values
			updateFunc = function( i, contexts, values ) {
				return function( value ) {
					contexts[ i ] = this;
					values[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
					if ( values === progressValues ) {
						master.notifyWith( contexts, values );
					} else if ( !( --remaining ) ) {
						master.resolveWith( contexts, values );
					}
				};
			},
			progressValues, progressContexts, resolveContexts;

		// Add listeners to Deferred subordinates; treat others as resolved
		if ( length > 1 ) {
			progressValues = new Array( length );
			progressContexts = new Array( length );
			resolveContexts = new Array( length );
			for ( ; i < length; i++ ) {
				if ( resolveValues[ i ] &&
					jQuery.isFunction( (method = resolveValues[ i ].promise) ) ) {

					method.call( resolveValues[ i ] )
				.progress( updateFunc( i, progressContexts, progressValues ) )
				.done( updateFunc( i, resolveContexts, resolveValues ) )
				.fail( master.reject );
			} else if ( resolveValues[ i ] &&
				jQuery.isFunction( (method = resolveValues[ i ].then) ) ) {

				method.call(
					resolveValues[ i ],
					updateFunc( i, resolveContexts, resolveValues ),
					master.reject,
					updateFunc( i, progressContexts, progressValues )
					);
			} else {
				--remaining;
			}
		}
	}

		// If we're not waiting on anything, resolve the master
		if ( !remaining ) {
			master.resolveWith( resolveContexts, resolveValues );
		}

		return master.promise();
	}
});


// The deferred used on DOM ready
var readyList;

jQuery.fn.ready = function( fn ) {
	// Add the callback
	jQuery.ready.promise().done( fn );

	return this;
};

jQuery.extend({
	// Is the DOM ready to be used? Set to true once it occurs.
	isReady: false,

	// A counter to track how many items to wait for before
	// the ready event fires. See #6781
	readyWait: 1,

	// Hold (or release) the ready event
	holdReady: function( hold ) {
		if ( hold ) {
			jQuery.readyWait++;
		} else {
			jQuery.ready( true );
		}
	},

	// Handle when the DOM is ready
	ready: function( wait ) {

		// Abort if there are pending holds or we're already ready
		if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
			return;
		}

		// Remember that the DOM is ready
		jQuery.isReady = true;

		// If a normal DOM Ready event fired, decrement, and wait if need be
		if ( wait !== true && --jQuery.readyWait > 0 ) {
			return;
		}

		// If there are functions bound, to execute
		readyList.resolveWith( document, [ jQuery ] );
	}
});

/**
 * The ready event handler and self cleanup method
 */
 function completed() {
 	document.removeEventListener( "DOMContentLoaded", completed );
 	window.removeEventListener( "load", completed );
 	jQuery.ready();
 }

 jQuery.ready.promise = function( obj ) {
 	if ( !readyList ) {

 		readyList = jQuery.Deferred();

		// Catch cases where $(document).ready() is called
		// after the browser event has already occurred.
		// We once tried to use readyState "interactive" here,
		// but it caused issues like the one
		// discovered by ChrisS here: http://bugs.jquery.com/ticket/12282#comment:15
		if ( document.readyState === "complete" ) {
			// Handle it asynchronously to allow scripts the opportunity to delay ready
			window.setTimeout( jQuery.ready );

		} else {

			// Use the handy event callback
			document.addEventListener( "DOMContentLoaded", completed );

			// A fallback to window.onload, that will always work
			window.addEventListener( "load", completed );
		}
	}
	return readyList.promise( obj );
};

// Kick off the DOM ready check even if the user does not
jQuery.ready.promise();




// Multifunctional method to get and set values of a collection
// The value/s can optionally be executed if it's a function
var access = jQuery.access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
	var i = 0,
	len = elems.length,
	bulk = key == null;

	// Sets many values
	if ( jQuery.type( key ) === "object" ) {
		chainable = true;
		for ( i in key ) {
			access( elems, fn, i, key[i], true, emptyGet, raw );
		}

	// Sets one value
} else if ( value !== undefined ) {
	chainable = true;

	if ( !jQuery.isFunction( value ) ) {
		raw = true;
	}

	if ( bulk ) {
			// Bulk operations run against the entire set
			if ( raw ) {
				fn.call( elems, value );
				fn = null;

			// ...except when executing function values
		} else {
			bulk = fn;
			fn = function( elem, key, value ) {
				return bulk.call( jQuery( elem ), value );
			};
		}
	}

	if ( fn ) {
		for ( ; i < len; i++ ) {
			fn( elems[i], key, raw ? value : value.call( elems[i], i, fn( elems[i], key ) ) );
		}
	}
}

return chainable ?
elems :

		// Gets
		bulk ?
		fn.call( elems ) :
		len ? fn( elems[0], key ) : emptyGet;
	};


/**
 * Determines whether an object can have data
 */
 jQuery.acceptData = function( owner ) {
	// Accepts only:
	//  - Node
	//    - Node.ELEMENT_NODE
	//    - Node.DOCUMENT_NODE
	//  - Object
	//    - Any
	/* jshint -W018 */
	return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
};


function Data() {
	this.expando = jQuery.expando + Data.uid++;
}

Data.uid = 1;
Data.accepts = jQuery.acceptData;

Data.prototype = {

	register: function( owner ) {
		var value = {};

		// If it is a node unlikely to be stringify-ed or looped over
		// use plain assignment
		if ( owner.nodeType ) {
			owner[ this.expando ] = value;

		// Otherwise secure it in a non-enumerable, non-writable property
		// configurability must be true to allow the property to be
		// deleted with the delete operator
	} else {
		Object.defineProperty( owner, this.expando, {
			value: value,
			writable: true,
			configurable: true
		});
	}
	return owner[ this.expando ];
},
cache: function( owner ) {

		// We can accept data for non-element nodes in modern browsers,
		// but we should not, see #8335.
		// Always return an empty object.
		if ( !Data.accepts( owner ) ) {
			return {};
		}

		// Check if the owner object already has a cache
		var cache = owner[ this.expando ];

		// If so, return it
		if ( cache ) {
			return cache;
		}

		// If not, register one
		return this.register( owner );
	},
	set: function( owner, data, value ) {
		var prop,
		cache = this.cache( owner );

		// Handle: [ owner, key, value ] args
		// Always use camelCase key (gh-2257)
		if ( typeof data === "string" ) {
			cache[ jQuery.camelCase( data ) ] = value;

		// Handle: [ owner, { properties } ] args
	} else {

			// Copy the properties one-by-one to the cache object
			for ( prop in data ) {
				cache[ jQuery.camelCase( prop ) ] = data[ prop ];
			}
		}
		return cache;
	},
	get: function( owner, key ) {
		var cache = this.cache( owner );

		return key === undefined ?
		cache :

			// Always use camelCase key (gh-2257)
			cache[ jQuery.camelCase( key ) ];
		},
		access: function( owner, key, value ) {

		// In cases where either:
		//
		//   1. No key was specified
		//   2. A string key was specified, but no value provided
		//
		// Take the "read" path and allow the get method to determine
		// which value to return, respectively either:
		//
		//   1. The entire cache object
		//   2. The data stored at the key
		//
		if ( key === undefined ||
			( ( key && typeof key === "string" ) && value === undefined ) ) {

			return this.get( owner, key );
	}

		// [*]When the key is not a string, or both a key and value
		// are specified, set or extend (existing objects) with either:
		//
		//   1. An object of properties
		//   2. A key and value
		//
		this.set( owner, key, value );

		// Since the "set" path can have two possible entry points
		// return the expected data based on which path was taken[*]
		return value !== undefined ? value : key;
	},
	remove: function( owner, key ) {
		var i,
		cache = owner[ this.expando ];

		if ( cache === undefined ) {
			return;
		}

		if ( key !== undefined ) {

			// Support array or space separated string of keys
			if ( jQuery.isArray( key ) ) {

				// If key is an array of keys...
				// We always set camelCase keys, so remove that.
				key = key.map( jQuery.camelCase );
			} else {
				key = jQuery.camelCase( key );

				// If a key with the spaces exists, use it.
				// Otherwise, create an array by matching non-whitespace
				key = key in cache ?
				[ key ] :
				( key.match( rnotwhite ) || [] );
			}

			i = key.length;

			while ( i-- ) {
				delete cache[ key[ i ] ];
			}
		}

		// Remove the expando if there's no more data
		if ( key === undefined || jQuery.isEmptyObject( cache ) ) {
			delete owner[ this.expando ];
		}
	},
	hasData: function( owner ) {
		var cache = owner[ this.expando ];
		return cache !== undefined && !jQuery.isEmptyObject( cache );
	}
};
var dataPriv = new Data();

var dataUser = new Data();



//	Implementation Summary
//
//	1. Enforce API surface and semantic compatibility with 1.9.x branch
//	2. Improve the module's maintainability by reducing the storage
//		paths to a single mechanism.
//	3. Use the same single mechanism to support "private" and "user" data.
//	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
//	5. Avoid exposing implementation details on user objects (eg. expando properties)
//	6. Provide a clear path for implementation upgrade to WeakMap in 2014

var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
rmultiDash = /[A-Z]/g;

function dataAttr( elem, key, data ) {
	var name;

	// If nothing was found internally, try to fetch any
	// data from the HTML5 data-* attribute
	if ( data === undefined && elem.nodeType === 1 ) {
		name = "data-" + key.replace( rmultiDash, "-$&" ).toLowerCase();
		data = elem.getAttribute( name );

		if ( typeof data === "string" ) {
			try {
				data = data === "true" ? true :
				data === "false" ? false :
				data === "null" ? null :
					// Only convert to a number if it doesn't change the string
					+data + "" === data ? +data :
					rbrace.test( data ) ? jQuery.parseJSON( data ) :
					data;
				} catch ( e ) {}

			// Make sure we set the data so it isn't changed later
			dataUser.set( elem, key, data );
		} else {
			data = undefined;
		}
	}
	return data;
}

jQuery.extend({
	hasData: function( elem ) {
		return dataUser.hasData( elem ) || dataPriv.hasData( elem );
	},

	data: function( elem, name, data ) {
		return dataUser.access( elem, name, data );
	},

	removeData: function( elem, name ) {
		dataUser.remove( elem, name );
	},

	// TODO: Now that all calls to _data and _removeData have been replaced
	// with direct calls to dataPriv methods, these can be deprecated.
	_data: function( elem, name, data ) {
		return dataPriv.access( elem, name, data );
	},

	_removeData: function( elem, name ) {
		dataPriv.remove( elem, name );
	}
});

jQuery.fn.extend({
	data: function( key, value ) {
		var i, name, data,
		elem = this[ 0 ],
		attrs = elem && elem.attributes;

		// Gets all values
		if ( key === undefined ) {
			if ( this.length ) {
				data = dataUser.get( elem );

				if ( elem.nodeType === 1 && !dataPriv.get( elem, "hasDataAttrs" ) ) {
					i = attrs.length;
					while ( i-- ) {

						// Support: IE11+
						// The attrs elements can be null (#14894)
						if ( attrs[ i ] ) {
							name = attrs[ i ].name;
							if ( name.indexOf( "data-" ) === 0 ) {
								name = jQuery.camelCase( name.slice(5) );
								dataAttr( elem, name, data[ name ] );
							}
						}
					}
					dataPriv.set( elem, "hasDataAttrs", true );
				}
			}

			return data;
		}

		// Sets multiple values
		if ( typeof key === "object" ) {
			return this.each(function() {
				dataUser.set( this, key );
			});
		}

		return access( this, function( value ) {
			var data;

			// The calling jQuery object (element matches) is not empty
			// (and therefore has an element appears at this[ 0 ]) and the
			// `value` parameter was not undefined. An empty jQuery object
			// will result in `undefined` for elem = this[ 0 ] which will
			// throw an exception if an attempt to read a data cache is made.
			if ( elem && value === undefined ) {

				// Attempt to get data from the cache
				// The key will always be camelCased in Data
				data = dataUser.get( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// Attempt to "discover" the data in
				// HTML5 custom data-* attrs
				data = dataAttr( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// We tried really hard, but the data doesn't exist.
				return;
			}

			// Set the data...
			this.each(function() {

				// We always store the camelCased key
				dataUser.set( this, key, value );
			});
		}, null, value, arguments.length > 1, null, true );
},

removeData: function( key ) {
	return this.each(function() {
		dataUser.remove( this, key );
	});
}
});


jQuery.extend({
	queue: function( elem, type, data ) {
		var queue;

		if ( elem ) {
			type = ( type || "fx" ) + "queue";
			queue = dataPriv.get( elem, type );

			// Speed up dequeue by getting out quickly if this is just a lookup
			if ( data ) {
				if ( !queue || jQuery.isArray( data ) ) {
					queue = dataPriv.access( elem, type, jQuery.makeArray(data) );
				} else {
					queue.push( data );
				}
			}
			return queue || [];
		}
	},

	dequeue: function( elem, type ) {
		type = type || "fx";

		var queue = jQuery.queue( elem, type ),
		startLength = queue.length,
		fn = queue.shift(),
		hooks = jQuery._queueHooks( elem, type ),
		next = function() {
			jQuery.dequeue( elem, type );
		};

		// If the fx queue is dequeued, always remove the progress sentinel
		if ( fn === "inprogress" ) {
			fn = queue.shift();
			startLength--;
		}

		if ( fn ) {

			// Add a progress sentinel to prevent the fx queue from being
			// automatically dequeued
			if ( type === "fx" ) {
				queue.unshift( "inprogress" );
			}

			// Clear up the last queue stop function
			delete hooks.stop;
			fn.call( elem, next, hooks );
		}

		if ( !startLength && hooks ) {
			hooks.empty.fire();
		}
	},

	// Not public - generate a queueHooks object, or return the current one
	_queueHooks: function( elem, type ) {
		var key = type + "queueHooks";
		return dataPriv.get( elem, key ) || dataPriv.access( elem, key, {
			empty: jQuery.Callbacks("once memory").add(function() {
				dataPriv.remove( elem, [ type + "queue", key ] );
			})
		});
	}
});

jQuery.fn.extend({
	queue: function( type, data ) {
		var setter = 2;

		if ( typeof type !== "string" ) {
			data = type;
			type = "fx";
			setter--;
		}

		if ( arguments.length < setter ) {
			return jQuery.queue( this[0], type );
		}

		return data === undefined ?
		this :
		this.each(function() {
			var queue = jQuery.queue( this, type, data );

				// Ensure a hooks for this queue
				jQuery._queueHooks( this, type );

				if ( type === "fx" && queue[0] !== "inprogress" ) {
					jQuery.dequeue( this, type );
				}
			});
	},
	dequeue: function( type ) {
		return this.each(function() {
			jQuery.dequeue( this, type );
		});
	},
	clearQueue: function( type ) {
		return this.queue( type || "fx", [] );
	},
	// Get a promise resolved when queues of a certain type
	// are emptied (fx is the type by default)
	promise: function( type, obj ) {
		var tmp,
		count = 1,
		defer = jQuery.Deferred(),
		elements = this,
		i = this.length,
		resolve = function() {
			if ( !( --count ) ) {
				defer.resolveWith( elements, [ elements ] );
			}
		};

		if ( typeof type !== "string" ) {
			obj = type;
			type = undefined;
		}
		type = type || "fx";

		while ( i-- ) {
			tmp = dataPriv.get( elements[ i ], type + "queueHooks" );
			if ( tmp && tmp.empty ) {
				count++;
				tmp.empty.add( resolve );
			}
		}
		resolve();
		return defer.promise( obj );
	}
});
var pnum = (/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/).source;

var rcssNum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" );


var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

var isHidden = function( elem, el ) {
		// isHidden might be called from jQuery#filter function;
		// in that case, element will be second argument
		elem = el || elem;
		return jQuery.css( elem, "display" ) === "none" ||
		!jQuery.contains( elem.ownerDocument, elem );
	};

	var swap = function( elem, options, callback, args ) {
		var ret, name,
		old = {};

	// Remember the old values, and insert the new ones
	for ( name in options ) {
		old[ name ] = elem.style[ name ];
		elem.style[ name ] = options[ name ];
	}

	ret = callback.apply( elem, args || [] );

	// Revert the old values
	for ( name in options ) {
		elem.style[ name ] = old[ name ];
	}

	return ret;
};




function adjustCSS( elem, prop, valueParts, tween ) {
	var adjusted,
	scale = 1,
	maxIterations = 20,
	currentValue = tween ?
	function() { return tween.cur(); } :
	function() { return jQuery.css( elem, prop, "" ); },
	initial = currentValue(),
	unit = valueParts && valueParts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),
		// Starting value computation is required for potential unit mismatches
		initialInUnit = ( jQuery.cssNumber[ prop ] || unit !== "px" && +initial ) &&
		rcssNum.exec( jQuery.css( elem, prop ) );

		if ( initialInUnit && initialInUnit[ 3 ] !== unit ) {
		// Trust units reported by jQuery.css
		unit = unit || initialInUnit[ 3 ];

		// Make sure we update the tween properties later on
		valueParts = valueParts || [];

		// Iteratively approximate from a nonzero starting point
		initialInUnit = +initial || 1;

		do {
			// If previous iteration zeroed out, double until we get *something*.
			// Use string for doubling so we don't accidentally see scale as unchanged below
			scale = scale || ".5";

			// Adjust and apply
			initialInUnit = initialInUnit / scale;
			jQuery.style( elem, prop, initialInUnit + unit );

		// Update scale, tolerating zero or NaN from tween.cur()
		// Break the loop if scale is unchanged or perfect, or if we've just had enough.
	} while (
		scale !== (scale = currentValue() / initial) && scale !== 1 && --maxIterations
		);
}

if ( valueParts ) {
	initialInUnit = +initialInUnit || +initial || 0;
		// Apply relative offset (+=/-=) if specified
		adjusted = valueParts[ 1 ] ?
		initialInUnit + ( valueParts[ 1 ] + 1 ) * valueParts[ 2 ] :
		+valueParts[ 2 ];
		if ( tween ) {
			tween.unit = unit;
			tween.start = initialInUnit;
			tween.end = adjusted;
		}
	}
	return adjusted;
}


function showHide( elements, show ) {
	var display, elem,
	values = [],
	index = 0,
	length = elements.length;

	// Determine new display value for elements that need to change
	for ( ; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}

		display = elem.style.display;
		if ( show ) {
			if ( display === "none" ) {
				// Restore a pre-hide() value if we have one
				values[ index ] = dataPriv.get( elem, "display" ) || "";
			}
		} else {
			if ( display !== "none" ) {
				values[ index ] = "none";

				// Remember the value we're replacing
				dataPriv.set( elem, "display", display );
			}
		}
	}

	// Set the display of the elements in a second loop
	// to avoid the constant reflow
	for ( index = 0; index < length; index++ ) {
		if ( values[ index ] != null ) {
			elements[ index ].style.display = values[ index ];
		}
	}

	return elements;
}
var rcheckableType = (/^(?:checkbox|radio)$/i);

var rtagName = ( /<([\w:-]+)/ );

var rscriptType = ( /^$|\/(?:java|ecma)script/i );



// We have to close these tags to support XHTML (#13200)
var wrapMap = {

	// Support: IE9
	option: [ 1, "<select multiple='multiple'>", "</select>" ],

	thead: [ 1, "<table>", "</table>" ],

	// Some of the following wrappers are not fully defined, because
	// their parent elements (except for "table" element) could be omitted
	// since browser parsers are smart enough to auto-insert them

	// Support: Android 2.3
	// Android browser doesn't auto-insert colgroup
	col: [ 2, "<table><colgroup>", "</colgroup></table>" ],

	// Auto-insert "tbody" element
	tr: [ 2, "<table>", "</table>" ],

	// Auto-insert "tbody" and "tr" elements
	td: [ 3, "<table>", "</table>" ],

	_default: [ 0, "", "" ]
};

// Support: IE9
wrapMap.optgroup = wrapMap.option;

wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;


function getAll( context, tag ) {
	// Support: IE9-11+
	// Use typeof to avoid zero-argument method invocation on host objects (#15151)
	var ret = typeof context.getElementsByTagName !== "undefined" ?
	context.getElementsByTagName( tag || "*" ) :
	typeof context.querySelectorAll !== "undefined" ?
	context.querySelectorAll( tag || "*" ) :
	[];

	return tag === undefined || tag && jQuery.nodeName( context, tag ) ?
	jQuery.merge( [ context ], ret ) :
	ret;
}


// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
	var i = 0,
	l = elems.length;

	for ( ; i < l; i++ ) {
		dataPriv.set(
			elems[ i ],
			"globalEval",
			!refElements || dataPriv.get( refElements[ i ], "globalEval" )
			);
	}
}


var rhtml = /<|&#?\w+;/;

function buildFragment( elems, context, scripts, selection, ignored ) {
	var elem, tmp, tag, wrap, contains, j,
	fragment = context.createDocumentFragment(),
	nodes = [],
	i = 0,
	l = elems.length;

	for ( ; i < l; i++ ) {
		elem = elems[ i ];

		if ( elem || elem === 0 ) {

			// Add nodes directly
			if ( jQuery.type( elem ) === "object" ) {
				// Support: Android<4.1, PhantomJS<2
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

			// Convert non-html into a text node
		} else if ( !rhtml.test( elem ) ) {
			nodes.push( context.createTextNode( elem ) );

			// Convert html into DOM nodes
		} else {
			tmp = tmp || fragment.appendChild( context.createElement( "div" ) );

				// Deserialize a standard representation
				tag = ( rtagName.exec( elem ) || [ "", "" ] )[ 1 ].toLowerCase();
				wrap = wrapMap[ tag ] || wrapMap._default;
				tmp.innerHTML = wrap[ 1 ] + jQuery.htmlPrefilter( elem ) + wrap[ 2 ];

				// Descend through wrappers to the right content
				j = wrap[ 0 ];
				while ( j-- ) {
					tmp = tmp.lastChild;
				}

				// Support: Android<4.1, PhantomJS<2
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, tmp.childNodes );

				// Remember the top-level container
				tmp = fragment.firstChild;

				// Ensure the created nodes are orphaned (#12392)
				tmp.textContent = "";
			}
		}
	}

	// Remove wrapper from fragment
	fragment.textContent = "";

	i = 0;
	while ( ( elem = nodes[ i++ ] ) ) {

		// Skip elements already in the context collection (trac-4087)
		if ( selection && jQuery.inArray( elem, selection ) > -1 ) {
			if ( ignored ) {
				ignored.push( elem );
			}
			continue;
		}

		contains = jQuery.contains( elem.ownerDocument, elem );

		// Append to fragment
		tmp = getAll( fragment.appendChild( elem ), "script" );

		// Preserve script evaluation history
		if ( contains ) {
			setGlobalEval( tmp );
		}

		// Capture executables
		if ( scripts ) {
			j = 0;
			while ( ( elem = tmp[ j++ ] ) ) {
				if ( rscriptType.test( elem.type || "" ) ) {
					scripts.push( elem );
				}
			}
		}
	}

	return fragment;
}


(function() {
	var fragment = document.createDocumentFragment(),
	div = fragment.appendChild( document.createElement( "div" ) ),
	input = document.createElement( "input" );

	// Support: Android 4.0-4.3
	// Check state lost if the name is set (#11217)
	// Support: Windows Web Apps (WWA)
	// `name` and `type` must use .setAttribute for WWA (#14901)
	input.setAttribute( "type", "radio" );
	input.setAttribute( "checked", "checked" );
	input.setAttribute( "name", "t" );

	div.appendChild( input );

	// Support: Android<4.2
	// Older WebKit doesn't clone checked state correctly in fragments
	support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

	// Support: IE<=11+
	// Make sure textarea (and checkbox) defaultValue is properly cloned
	div.innerHTML = "<textarea>x</textarea>";
	support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;
})();


support.focusin = "onfocusin" in window;


var
rkeyEvent = /^key/,
rmouseEvent = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
rtypenamespace = /^([^.]*)(?:\.(.+)|)/;

function returnTrue() {
	return true;
}

function returnFalse() {
	return false;
}

// Support: IE9
// See #13393 for more info
function safeActiveElement() {
	try {
		return document.activeElement;
	} catch ( err ) { }
}

function on( elem, types, selector, data, fn, one ) {
	var origFn, type;

	// Types can be a map of types/handlers
	if ( typeof types === "object" ) {
		// ( types-Object, selector, data )
		if ( typeof selector !== "string" ) {
			// ( types-Object, data )
			data = data || selector;
			selector = undefined;
		}
		for ( type in types ) {
			on( elem, type, selector, data, types[ type ], one );
		}
		return elem;
	}

	if ( data == null && fn == null ) {
		// ( types, fn )
		fn = selector;
		data = selector = undefined;
	} else if ( fn == null ) {
		if ( typeof selector === "string" ) {
			// ( types, selector, fn )
			fn = data;
			data = undefined;
		} else {
			// ( types, data, fn )
			fn = data;
			data = selector;
			selector = undefined;
		}
	}
	if ( fn === false ) {
		fn = returnFalse;
	}

	if ( one === 1 ) {
		origFn = fn;
		fn = function( event ) {
			// Can use an empty set, since event contains the info
			jQuery().off( event );
			return origFn.apply( this, arguments );
		};
		// Use same guid so caller can remove using origFn
		fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
	}
	return elem.each( function() {
		jQuery.event.add( this, types, fn, data, selector );
	});
}

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
 jQuery.event = {

 	global: {},

 	add: function( elem, types, handler, data, selector ) {

 		var handleObjIn, eventHandle, tmp,
 		events, t, handleObj,
 		special, handlers, type, namespaces, origType,
 		elemData = dataPriv.get( elem );

		// Don't attach events to noData or text/comment nodes (but allow plain objects)
		if ( !elemData ) {
			return;
		}

		// Caller can pass in an object of custom data in lieu of the handler
		if ( handler.handler ) {
			handleObjIn = handler;
			handler = handleObjIn.handler;
			selector = handleObjIn.selector;
		}

		// Make sure that the handler has a unique ID, used to find/remove it later
		if ( !handler.guid ) {
			handler.guid = jQuery.guid++;
		}

		// Init the element's event structure and main handler, if this is the first
		if ( !(events = elemData.events) ) {
			events = elemData.events = {};
		}
		if ( !(eventHandle = elemData.handle) ) {
			eventHandle = elemData.handle = function( e ) {
				// Discard the second event of a jQuery.event.trigger() and
				// when an event is called after a page has unloaded
				return typeof jQuery !== "undefined" && jQuery.event.triggered !== e.type ?
				jQuery.event.dispatch.apply( elem, arguments ) : undefined;
			};
		}

		// Handle multiple events separated by a space
		types = ( types || "" ).match( rnotwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[t] ) || [];
			type = origType = tmp[1];
			namespaces = ( tmp[2] || "" ).split( "." ).sort();

			// There *must* be a type, no attaching namespace-only handlers
			if ( !type ) {
				continue;
			}

			// If event changes its type, use the special event handlers for the changed type
			special = jQuery.event.special[ type ] || {};

			// If selector defined, determine special event api type, otherwise given type
			type = ( selector ? special.delegateType : special.bindType ) || type;

			// Update special based on newly reset type
			special = jQuery.event.special[ type ] || {};

			// handleObj is passed to all event handlers
			handleObj = jQuery.extend({
				type: type,
				origType: origType,
				data: data,
				handler: handler,
				guid: handler.guid,
				selector: selector,
				needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
				namespace: namespaces.join(".")
			}, handleObjIn );

			// Init the event handler queue if we're the first
			if ( !(handlers = events[ type ]) ) {
				handlers = events[ type ] = [];
				handlers.delegateCount = 0;

				// Only use addEventListener if the special events handler returns false
				if ( !special.setup ||
					special.setup.call( elem, data, namespaces, eventHandle ) === false ) {

					if ( elem.addEventListener ) {
						elem.addEventListener( type, eventHandle );
					}
				}
			}

			if ( special.add ) {
				special.add.call( elem, handleObj );

				if ( !handleObj.handler.guid ) {
					handleObj.handler.guid = handler.guid;
				}
			}

			// Add to the element's handler list, delegates in front
			if ( selector ) {
				handlers.splice( handlers.delegateCount++, 0, handleObj );
			} else {
				handlers.push( handleObj );
			}

			// Keep track of which events have ever been used, for event optimization
			jQuery.event.global[ type ] = true;
		}

	},

	// Detach an event or set of events from an element
	remove: function( elem, types, handler, selector, mappedTypes ) {

		var j, origCount, tmp,
		events, t, handleObj,
		special, handlers, type, namespaces, origType,
		elemData = dataPriv.hasData( elem ) && dataPriv.get( elem );

		if ( !elemData || !(events = elemData.events) ) {
			return;
		}

		// Once for each type.namespace in types; type may be omitted
		types = ( types || "" ).match( rnotwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[t] ) || [];
			type = origType = tmp[1];
			namespaces = ( tmp[2] || "" ).split( "." ).sort();

			// Unbind all events (on this namespace, if provided) for the element
			if ( !type ) {
				for ( type in events ) {
					jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
				}
				continue;
			}

			special = jQuery.event.special[ type ] || {};
			type = ( selector ? special.delegateType : special.bindType ) || type;
			handlers = events[ type ] || [];
			tmp = tmp[2] && new RegExp( "(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)" );

			// Remove matching events
			origCount = j = handlers.length;
			while ( j-- ) {
				handleObj = handlers[ j ];

				if ( ( mappedTypes || origType === handleObj.origType ) &&
					( !handler || handler.guid === handleObj.guid ) &&
					( !tmp || tmp.test( handleObj.namespace ) ) &&
					( !selector || selector === handleObj.selector ||
						selector === "**" && handleObj.selector ) ) {
					handlers.splice( j, 1 );

				if ( handleObj.selector ) {
					handlers.delegateCount--;
				}
				if ( special.remove ) {
					special.remove.call( elem, handleObj );
				}
			}
		}

			// Remove generic event handler if we removed something and no more handlers exist
			// (avoids potential for endless recursion during removal of special event handlers)
			if ( origCount && !handlers.length ) {
				if ( !special.teardown ||
					special.teardown.call( elem, namespaces, elemData.handle ) === false ) {

					jQuery.removeEvent( elem, type, elemData.handle );
			}

			delete events[ type ];
		}
	}

		// Remove data and the expando if it's no longer used
		if ( jQuery.isEmptyObject( events ) ) {
			dataPriv.remove( elem, "handle events" );
		}
	},

	trigger: function( event, data, elem, onlyHandlers ) {

		var i, cur, tmp, bubbleType, ontype, handle, special,
		eventPath = [ elem || document ],
		type = hasOwn.call( event, "type" ) ? event.type : event,
		namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split(".") : [];

		cur = tmp = elem = elem || document;

		// Don't do events on text and comment nodes
		if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
			return;
		}

		// focus/blur morphs to focusin/out; ensure we're not firing them right now
		if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
			return;
		}

		if ( type.indexOf(".") > -1 ) {
			// Namespaced trigger; create a regexp to match event type in handle()
			namespaces = type.split(".");
			type = namespaces.shift();
			namespaces.sort();
		}
		ontype = type.indexOf(":") < 0 && "on" + type;

		// Caller can pass in a jQuery.Event object, Object, or just an event type string
		event = event[ jQuery.expando ] ?
		event :
		new jQuery.Event( type, typeof event === "object" && event );

		// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
		event.isTrigger = onlyHandlers ? 2 : 3;
		event.namespace = namespaces.join(".");
		event.rnamespace = event.namespace ?
		new RegExp( "(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)" ) :
		null;

		// Clean up the event in case it is being reused
		event.result = undefined;
		if ( !event.target ) {
			event.target = elem;
		}

		// Clone any incoming data and prepend the event, creating the handler arg list
		data = data == null ?
		[ event ] :
		jQuery.makeArray( data, [ event ] );

		// Allow special events to draw outside the lines
		special = jQuery.event.special[ type ] || {};
		if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
			return;
		}

		// Determine event propagation path in advance, per W3C events spec (#9951)
		// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
		if ( !onlyHandlers && !special.noBubble && !jQuery.isWindow( elem ) ) {

			bubbleType = special.delegateType || type;
			if ( !rfocusMorph.test( bubbleType + type ) ) {
				cur = cur.parentNode;
			}
			for ( ; cur; cur = cur.parentNode ) {
				eventPath.push( cur );
				tmp = cur;
			}

			// Only add window if we got to document (e.g., not plain obj or detached DOM)
			if ( tmp === (elem.ownerDocument || document) ) {
				eventPath.push( tmp.defaultView || tmp.parentWindow || window );
			}
		}

		// Fire handlers on the event path
		i = 0;
		while ( (cur = eventPath[i++]) && !event.isPropagationStopped() ) {

			event.type = i > 1 ?
			bubbleType :
			special.bindType || type;

			// jQuery handler
			handle = ( dataPriv.get( cur, "events" ) || {} )[ event.type ] &&
			dataPriv.get( cur, "handle" );
			if ( handle ) {
				handle.apply( cur, data );
			}

			// Native handler
			handle = ontype && cur[ ontype ];
			if ( handle && handle.apply && jQuery.acceptData( cur ) ) {
				event.result = handle.apply( cur, data );
				if ( event.result === false ) {
					event.preventDefault();
				}
			}
		}
		event.type = type;

		// If nobody prevented the default action, do it now
		if ( !onlyHandlers && !event.isDefaultPrevented() ) {

			if ( (!special._default || special._default.apply( eventPath.pop(), data ) === false) &&
				jQuery.acceptData( elem ) ) {

				// Call a native DOM method on the target with the same name name as the event.
				// Don't do default actions on window, that's where global variables be (#6170)
				if ( ontype && jQuery.isFunction( elem[ type ] ) && !jQuery.isWindow( elem ) ) {

					// Don't re-trigger an onFOO event when we call its FOO() method
					tmp = elem[ ontype ];

					if ( tmp ) {
						elem[ ontype ] = null;
					}

					// Prevent re-triggering of the same event, since we already bubbled it above
					jQuery.event.triggered = type;
					elem[ type ]();
					jQuery.event.triggered = undefined;

					if ( tmp ) {
						elem[ ontype ] = tmp;
					}
				}
			}
		}

		return event.result;
	},

	dispatch: function( event ) {

		// Make a writable jQuery.Event from the native event object
		event = jQuery.event.fix( event );

		var i, j, ret, matched, handleObj,
		handlerQueue = [],
		args = slice.call( arguments ),
		handlers = ( dataPriv.get( this, "events" ) || {} )[ event.type ] || [],
		special = jQuery.event.special[ event.type ] || {};

		// Use the fix-ed jQuery.Event rather than the (read-only) native event
		args[0] = event;
		event.delegateTarget = this;

		// Call the preDispatch hook for the mapped type, and let it bail if desired
		if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
			return;
		}

		// Determine handlers
		handlerQueue = jQuery.event.handlers.call( this, event, handlers );

		// Run delegates first; they may want to stop propagation beneath us
		i = 0;
		while ( (matched = handlerQueue[ i++ ]) && !event.isPropagationStopped() ) {
			event.currentTarget = matched.elem;

			j = 0;
			while ( (handleObj = matched.handlers[ j++ ]) &&
				!event.isImmediatePropagationStopped() ) {

				// Triggered event must either 1) have no namespace, or 2) have namespace(s)
				// a subset or equal to those in the bound event (both can have no namespace).
				if ( !event.rnamespace || event.rnamespace.test( handleObj.namespace ) ) {

					event.handleObj = handleObj;
					event.data = handleObj.data;

					ret = ( (jQuery.event.special[ handleObj.origType ] || {}).handle ||
						handleObj.handler ).apply( matched.elem, args );

					if ( ret !== undefined ) {
						if ( (event.result = ret) === false ) {
							event.preventDefault();
							event.stopPropagation();
						}
					}
				}
			}
		}

		// Call the postDispatch hook for the mapped type
		if ( special.postDispatch ) {
			special.postDispatch.call( this, event );
		}

		return event.result;
	},

	handlers: function( event, handlers ) {
		var i, matches, sel, handleObj,
		handlerQueue = [],
		delegateCount = handlers.delegateCount,
		cur = event.target;

		// Support (at least): Chrome, IE9
		// Find delegate handlers
		// Black-hole SVG <use> instance trees (#13180)
		//
		// Support: Firefox
		// Avoid non-left-click bubbling in Firefox (#3861)
		if ( delegateCount && cur.nodeType && (!event.button || event.type !== "click") ) {

			for ( ; cur !== this; cur = cur.parentNode || this ) {

				// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
				if ( cur.disabled !== true || event.type !== "click" ) {
					matches = [];
					for ( i = 0; i < delegateCount; i++ ) {
						handleObj = handlers[ i ];

						// Don't conflict with Object.prototype properties (#13203)
						sel = handleObj.selector + " ";

						if ( matches[ sel ] === undefined ) {
							matches[ sel ] = handleObj.needsContext ?
							jQuery( sel, this ).index( cur ) > -1 :
							jQuery.find( sel, this, null, [ cur ] ).length;
						}
						if ( matches[ sel ] ) {
							matches.push( handleObj );
						}
					}
					if ( matches.length ) {
						handlerQueue.push({ elem: cur, handlers: matches });
					}
				}
			}
		}

		// Add the remaining (directly-bound) handlers
		if ( delegateCount < handlers.length ) {
			handlerQueue.push({ elem: this, handlers: handlers.slice( delegateCount ) });
		}

		return handlerQueue;
	},

	// Includes some event props shared by KeyEvent and MouseEvent
	props: ( "altKey bubbles cancelable ctrlKey currentTarget detail eventPhase " +
		"metaKey relatedTarget shiftKey target timeStamp view which" ).split(" "),

	fixHooks: {},

	keyHooks: {
		props: "char charCode key keyCode".split(" "),
		filter: function( event, original ) {

			// Add which for key events
			if ( event.which == null ) {
				event.which = original.charCode != null ? original.charCode : original.keyCode;
			}

			return event;
		}
	},

	mouseHooks: {
		props: ( "button buttons clientX clientY offsetX offsetY pageX pageY " +
			"screenX screenY toElement" ).split(" "),
		filter: function( event, original ) {
			var eventDoc, doc, body,
			button = original.button;

			// Calculate pageX/Y if missing and clientX/Y available
			if ( event.pageX == null && original.clientX != null ) {
				eventDoc = event.target.ownerDocument || document;
				doc = eventDoc.documentElement;
				body = eventDoc.body;

				event.pageX = original.clientX +
				( doc && doc.scrollLeft || body && body.scrollLeft || 0 ) -
				( doc && doc.clientLeft || body && body.clientLeft || 0 );
				event.pageY = original.clientY +
				( doc && doc.scrollTop  || body && body.scrollTop  || 0 ) -
				( doc && doc.clientTop  || body && body.clientTop  || 0 );
			}

			// Add which for click: 1 === left; 2 === middle; 3 === right
			// Note: button is not normalized, so don't use it
			if ( !event.which && button !== undefined ) {
				event.which = ( button & 1 ? 1 : ( button & 2 ? 3 : ( button & 4 ? 2 : 0 ) ) );
			}

			return event;
		}
	},

	fix: function( event ) {
		if ( event[ jQuery.expando ] ) {
			return event;
		}

		// Create a writable copy of the event object and normalize some properties
		var i, prop, copy,
		type = event.type,
		originalEvent = event,
		fixHook = this.fixHooks[ type ];

		if ( !fixHook ) {
			this.fixHooks[ type ] = fixHook =
			rmouseEvent.test( type ) ? this.mouseHooks :
			rkeyEvent.test( type ) ? this.keyHooks :
			{};
		}
		copy = fixHook.props ? this.props.concat( fixHook.props ) : this.props;

		event = new jQuery.Event( originalEvent );

		i = copy.length;
		while ( i-- ) {
			prop = copy[ i ];
			event[ prop ] = originalEvent[ prop ];
		}

		// Support: Safari 6.0+
		// Target should not be a text node (#504, #13143)
		if ( event.target.nodeType === 3 ) {
			event.target = event.target.parentNode;
		}

		return fixHook.filter ? fixHook.filter( event, originalEvent ) : event;
	},

	special: {
		load: {
			// Prevent triggered image.load events from bubbling to window.load
			noBubble: true
		},
		focus: {
			// Fire native event if possible so blur/focus sequence is correct
			trigger: function() {
				if ( this !== safeActiveElement() && this.focus ) {
					this.focus();
					return false;
				}
			},
			delegateType: "focusin"
		},
		blur: {
			trigger: function() {
				if ( this === safeActiveElement() && this.blur ) {
					this.blur();
					return false;
				}
			},
			delegateType: "focusout"
		},
		click: {
			// For checkbox, fire native event so checked state will be right
			trigger: function() {
				if ( this.type === "checkbox" && this.click && jQuery.nodeName( this, "input" ) ) {
					this.click();
					return false;
				}
			},

			// For cross-browser consistency, don't fire native .click() on links
			_default: function( event ) {
				return jQuery.nodeName( event.target, "a" );
			}
		},

		beforeunload: {
			postDispatch: function( event ) {

				// Support: Firefox 20+
				// Firefox doesn't alert if the returnValue field is not set.
				if ( event.result !== undefined && event.originalEvent ) {
					event.originalEvent.returnValue = event.result;
				}
			}
		}
	},

	// Piggyback on a donor event to simulate a different one
	simulate: function( type, elem, event ) {
		var e = jQuery.extend(
			new jQuery.Event(),
			event,
			{
				type: type,
				isSimulated: true
				// Previously, `originalEvent: {}` was set here, so stopPropagation call
				// would not be triggered on donor event, since in our own
				// jQuery.event.stopPropagation function we had a check for existence of
				// originalEvent.stopPropagation method, so, consequently it would be a noop.
				//
				// But now, this "simulate" function is used only for events
				// for which stopPropagation() is noop, so there is no need for that anymore.
				//
				// For the compat branch though, guard for "click" and "submit"
				// events is still used, but was moved to jQuery.event.stopPropagation function
				// because `originalEvent` should point to the original event for the constancy
				// with other events and for more focused logic
			}
			);

		jQuery.event.trigger( e, null, elem );

		if ( e.isDefaultPrevented() ) {
			event.preventDefault();
		}
	}
};

jQuery.removeEvent = function( elem, type, handle ) {

	// This "if" is needed for plain objects
	if ( elem.removeEventListener ) {
		elem.removeEventListener( type, handle );
	}
};

jQuery.Event = function( src, props ) {
	// Allow instantiation without the 'new' keyword
	if ( !(this instanceof jQuery.Event) ) {
		return new jQuery.Event( src, props );
	}

	// Event object
	if ( src && src.type ) {
		this.originalEvent = src;
		this.type = src.type;

		// Events bubbling up the document may have been marked as prevented
		// by a handler lower down the tree; reflect the correct value.
		this.isDefaultPrevented = src.defaultPrevented ||
		src.defaultPrevented === undefined &&
				// Support: Android<4.0
				src.returnValue === false ?
				returnTrue :
				returnFalse;

	// Event type
} else {
	this.type = src;
}

	// Put explicitly provided properties onto the event object
	if ( props ) {
		jQuery.extend( this, props );
	}

	// Create a timestamp if incoming event doesn't have one
	this.timeStamp = src && src.timeStamp || jQuery.now();

	// Mark it as fixed
	this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
	constructor: jQuery.Event,
	isDefaultPrevented: returnFalse,
	isPropagationStopped: returnFalse,
	isImmediatePropagationStopped: returnFalse,

	preventDefault: function() {
		var e = this.originalEvent;

		this.isDefaultPrevented = returnTrue;

		if ( e ) {
			e.preventDefault();
		}
	},
	stopPropagation: function() {
		var e = this.originalEvent;

		this.isPropagationStopped = returnTrue;

		if ( e ) {
			e.stopPropagation();
		}
	},
	stopImmediatePropagation: function() {
		var e = this.originalEvent;

		this.isImmediatePropagationStopped = returnTrue;

		if ( e ) {
			e.stopImmediatePropagation();
		}

		this.stopPropagation();
	}
};

// Create mouseenter/leave events using mouseover/out and event-time checks
// so that event delegation works in jQuery.
// Do the same for pointerenter/pointerleave and pointerover/pointerout
//
// Support: Safari<7.0
// Safari doesn't support mouseenter/mouseleave at all.
//
// Support: Chrome 34+
// Mouseenter doesn't perform while left mouse button is pressed
// (and initiated outside the observed element)
// https://code.google.com/p/chromium/issues/detail?id=333868
jQuery.each({
	mouseenter: "mouseover",
	mouseleave: "mouseout",
	pointerenter: "pointerover",
	pointerleave: "pointerout"
}, function( orig, fix ) {
	jQuery.event.special[ orig ] = {
		delegateType: fix,
		bindType: fix,

		handle: function( event ) {
			var ret,
			target = this,
			related = event.relatedTarget,
			handleObj = event.handleObj;

			// For mousenter/leave call the handler if related is outside the target.
			// NB: No relatedTarget if the mouse left/entered the browser window
			if ( !related || (related !== target && !jQuery.contains( target, related )) ) {
				event.type = handleObj.origType;
				ret = handleObj.handler.apply( this, arguments );
				event.type = fix;
			}
			return ret;
		}
	};
});

// Support: Firefox
// Firefox doesn't have focus(in | out) events
// Related ticket - https://bugzilla.mozilla.org/show_bug.cgi?id=687787
//
// Support: Chrome, Safari
// focus(in | out) events fire after focus & blur events,
// which is spec violation - http://www.w3.org/TR/DOM-Level-3-Events/#events-focusevent-event-order
// Related ticket - https://code.google.com/p/chromium/issues/detail?id=449857
if ( !support.focusin ) {
	jQuery.each({ focus: "focusin", blur: "focusout" }, function( orig, fix ) {

		// Attach a single capturing handler on the document while someone wants focusin/focusout
		var handler = function( event ) {
			jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ) );
		};

		jQuery.event.special[ fix ] = {
			setup: function() {
				var doc = this.ownerDocument || this,
				attaches = dataPriv.access( doc, fix );

				if ( !attaches ) {
					doc.addEventListener( orig, handler, true );
				}
				dataPriv.access( doc, fix, ( attaches || 0 ) + 1 );
			},
			teardown: function() {
				var doc = this.ownerDocument || this,
				attaches = dataPriv.access( doc, fix ) - 1;

				if ( !attaches ) {
					doc.removeEventListener( orig, handler, true );
					dataPriv.remove( doc, fix );

				} else {
					dataPriv.access( doc, fix, attaches );
				}
			}
		};
	});
}

jQuery.fn.extend({

	on: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn );
	},
	one: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn, 1 );
	},
	off: function( types, selector, fn ) {
		var handleObj, type;
		if ( types && types.preventDefault && types.handleObj ) {
			// ( event )  dispatched jQuery.Event
			handleObj = types.handleObj;
			jQuery( types.delegateTarget ).off(
				handleObj.namespace ?
				handleObj.origType + "." + handleObj.namespace :
				handleObj.origType,
				handleObj.selector,
				handleObj.handler
				);
			return this;
		}
		if ( typeof types === "object" ) {
			// ( types-object [, selector] )
			for ( type in types ) {
				this.off( type, selector, types[ type ] );
			}
			return this;
		}
		if ( selector === false || typeof selector === "function" ) {
			// ( types [, fn] )
			fn = selector;
			selector = undefined;
		}
		if ( fn === false ) {
			fn = returnFalse;
		}
		return this.each(function() {
			jQuery.event.remove( this, types, fn, selector );
		});
	},

	trigger: function( type, data ) {
		return this.each(function() {
			jQuery.event.trigger( type, data, this );
		});
	},
	triggerHandler: function( type, data ) {
		var elem = this[0];
		if ( elem ) {
			return jQuery.event.trigger( type, data, elem, true );
		}
	}
});


var
rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,
rnoInnerhtml = /<(?:script|style|link)/i,
	// checked="checked" or checked
	rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
	rscriptTypeMasked = /^true\/(.*)/,
	rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

	function manipulationTarget( elem, content ) {
		if ( jQuery.nodeName( elem, "table" ) &&
			jQuery.nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ) {

			return elem.getElementsByTagName( "tbody" )[ 0 ] || elem;
	}

	return elem;
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
	elem.type = (elem.getAttribute("type") !== null) + "/" + elem.type;
	return elem;
}
function restoreScript( elem ) {
	var match = rscriptTypeMasked.exec( elem.type );

	if ( match ) {
		elem.type = match[ 1 ];
	} else {
		elem.removeAttribute("type");
	}

	return elem;
}

function cloneCopyEvent( src, dest ) {
	var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;

	if ( dest.nodeType !== 1 ) {
		return;
	}

	// 1. Copy private data: events, handlers, etc.
	if ( dataPriv.hasData( src ) ) {
		pdataOld = dataPriv.access( src );
		pdataCur = dataPriv.set( dest, pdataOld );
		events = pdataOld.events;

		if ( events ) {
			delete pdataCur.handle;
			pdataCur.events = {};

			for ( type in events ) {
				for ( i = 0, l = events[ type ].length; i < l; i++ ) {
					jQuery.event.add( dest, type, events[ type ][ i ] );
				}
			}
		}
	}

	// 2. Copy user data
	if ( dataUser.hasData( src ) ) {
		udataOld = dataUser.access( src );
		udataCur = jQuery.extend( {}, udataOld );

		dataUser.set( dest, udataCur );
	}
}

// Fix IE bugs, see support tests
function fixInput( src, dest ) {
	var nodeName = dest.nodeName.toLowerCase();

	// Fails to persist the checked state of a cloned checkbox or radio button.
	if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
		dest.checked = src.checked;

	// Fails to return the selected option to the default selected state when cloning options
} else if ( nodeName === "input" || nodeName === "textarea" ) {
	dest.defaultValue = src.defaultValue;
}
}

function domManip( collection, args, callback, ignored ) {

	// Flatten any nested arrays
	args = concat.apply( [], args );

	var fragment, first, scripts, hasScripts, node, doc,
	i = 0,
	l = collection.length,
	iNoClone = l - 1,
	value = args[ 0 ],
	isFunction = jQuery.isFunction( value );

	// We can't cloneNode fragments that contain checked, in WebKit
	if ( isFunction ||
		( l > 1 && typeof value === "string" &&
			!support.checkClone && rchecked.test( value ) ) ) {
		return collection.each(function( index ) {
			var self = collection.eq( index );
			if ( isFunction ) {
				args[ 0 ] = value.call( this, index, self.html() );
			}
			domManip( self, args, callback, ignored );
		});
}

if ( l ) {
	fragment = buildFragment( args, collection[ 0 ].ownerDocument, false, collection, ignored );
	first = fragment.firstChild;

	if ( fragment.childNodes.length === 1 ) {
		fragment = first;
	}

		// Require either new content or an interest in ignored elements to invoke the callback
		if ( first || ignored ) {
			scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
			hasScripts = scripts.length;

			// Use the original fragment for the last item
			// instead of the first because it can end up
			// being emptied incorrectly in certain situations (#8070).
			for ( ; i < l; i++ ) {
				node = fragment;

				if ( i !== iNoClone ) {
					node = jQuery.clone( node, true, true );

					// Keep references to cloned scripts for later restoration
					if ( hasScripts ) {
						// Support: Android<4.1, PhantomJS<2
						// push.apply(_, arraylike) throws on ancient WebKit
						jQuery.merge( scripts, getAll( node, "script" ) );
					}
				}

				callback.call( collection[ i ], node, i );
			}

			if ( hasScripts ) {
				doc = scripts[ scripts.length - 1 ].ownerDocument;

				// Reenable scripts
				jQuery.map( scripts, restoreScript );

				// Evaluate executable scripts on first document insertion
				for ( i = 0; i < hasScripts; i++ ) {
					node = scripts[ i ];
					if ( rscriptType.test( node.type || "" ) &&
						!dataPriv.access( node, "globalEval" ) &&
						jQuery.contains( doc, node ) ) {

						if ( node.src ) {
							// Optional AJAX dependency, but won't run scripts if not present
							if ( jQuery._evalUrl ) {
								jQuery._evalUrl( node.src );
							}
						} else {
							jQuery.globalEval( node.textContent.replace( rcleanScript, "" ) );
						}
					}
				}
			}
		}
	}

	return collection;
}

function remove( elem, selector, keepData ) {
	var node,
	nodes = selector ? jQuery.filter( selector, elem ) : elem,
	i = 0;

	for ( ; (node = nodes[i]) != null; i++ ) {
		if ( !keepData && node.nodeType === 1 ) {
			jQuery.cleanData( getAll( node ) );
		}

		if ( node.parentNode ) {
			if ( keepData && jQuery.contains( node.ownerDocument, node ) ) {
				setGlobalEval( getAll( node, "script" ) );
			}
			node.parentNode.removeChild( node );
		}
	}

	return elem;
}

jQuery.extend({
	htmlPrefilter: function( html ) {
		return html.replace( rxhtmlTag, "<$1></$2>" );
	},

	clone: function( elem, dataAndEvents, deepDataAndEvents ) {
		var i, l, srcElements, destElements,
		clone = elem.cloneNode( true ),
		inPage = jQuery.contains( elem.ownerDocument, elem );

		// Fix IE cloning issues
		if ( !support.noCloneChecked && ( elem.nodeType === 1 || elem.nodeType === 11 ) &&
			!jQuery.isXMLDoc( elem ) ) {

			// We eschew Sizzle here for performance reasons: http://jsperf.com/getall-vs-sizzle/2
		destElements = getAll( clone );
		srcElements = getAll( elem );

		for ( i = 0, l = srcElements.length; i < l; i++ ) {
			fixInput( srcElements[ i ], destElements[ i ] );
		}
	}

		// Copy the events from the original to the clone
		if ( dataAndEvents ) {
			if ( deepDataAndEvents ) {
				srcElements = srcElements || getAll( elem );
				destElements = destElements || getAll( clone );

				for ( i = 0, l = srcElements.length; i < l; i++ ) {
					cloneCopyEvent( srcElements[ i ], destElements[ i ] );
				}
			} else {
				cloneCopyEvent( elem, clone );
			}
		}

		// Preserve script evaluation history
		destElements = getAll( clone, "script" );
		if ( destElements.length > 0 ) {
			setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
		}

		// Return the cloned set
		return clone;
	},

	cleanData: function( elems ) {
		var data, elem, type,
		special = jQuery.event.special,
		i = 0;

		for ( ; (elem = elems[ i ]) !== undefined; i++ ) {
			if ( jQuery.acceptData( elem ) && (data = elem[ dataPriv.expando ])) {
				if ( data.events ) {
					for ( type in data.events ) {
						if ( special[ type ] ) {
							jQuery.event.remove( elem, type );

						// This is a shortcut to avoid jQuery.event.remove's overhead
					} else {
						jQuery.removeEvent( elem, type, data.handle );
					}
				}
			}
			delete elem[ dataPriv.expando ];
		}
	}
}
});

jQuery.fn.extend({
	detach: function( selector ) {
		return remove( this, selector, true );
	},

	remove: function( selector ) {
		return remove( this, selector );
	},

	text: function( value ) {
		return access( this, function( value ) {
			return value === undefined ?
			jQuery.text( this ) :
			this.empty().each(function() {
				if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
					this.textContent = value;
				}
			});
		}, null, value, arguments.length );
	},

	append: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.appendChild( elem );
			}
		});
	},

	prepend: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.insertBefore( elem, target.firstChild );
			}
		});
	},

	before: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this );
			}
		});
	},

	after: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this.nextSibling );
			}
		});
	},

	empty: function() {
		var elem,
		i = 0;

		for ( ; (elem = this[i]) != null; i++ ) {
			if ( elem.nodeType === 1 ) {

				// Prevent memory leaks
				jQuery.cleanData( getAll( elem, false ) );

				// Remove any remaining nodes
				elem.textContent = "";
			}
		}

		return this;
	},

	clone: function( dataAndEvents, deepDataAndEvents ) {
		dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
		deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

		return this.map(function() {
			return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
		});
	},

	html: function( value ) {
		return access( this, function( value ) {
			var elem = this[ 0 ] || {},
			i = 0,
			l = this.length;

			if ( value === undefined && elem.nodeType === 1 ) {
				return elem.innerHTML;
			}

			// See if we can take a shortcut and just use innerHTML
			if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
				!wrapMap[ ( rtagName.exec( value ) || [ "", "" ] )[ 1 ].toLowerCase() ] ) {

				value = jQuery.htmlPrefilter( value );

			try {
				for ( ; i < l; i++ ) {
					elem = this[ i ] || {};

						// Remove element nodes and prevent memory leaks
						if ( elem.nodeType === 1 ) {
							jQuery.cleanData( getAll( elem, false ) );
							elem.innerHTML = value;
						}
					}

					elem = 0;

				// If using innerHTML throws an exception, use the fallback method
			} catch ( e ) {}
		}

		if ( elem ) {
			this.empty().append( value );
		}
	}, null, value, arguments.length );
	},

	replaceWith: function() {
		var ignored = [];

		// Make the changes, replacing each non-ignored context element with the new content
		return domManip( this, arguments, function( elem ) {
			var parent = this.parentNode;

			if ( jQuery.inArray( this, ignored ) < 0 ) {
				jQuery.cleanData( getAll( this ) );
				if ( parent ) {
					parent.replaceChild( elem, this );
				}
			}

		// Force callback invocation
	}, ignored );
	}
});

jQuery.each({
	appendTo: "append",
	prependTo: "prepend",
	insertBefore: "before",
	insertAfter: "after",
	replaceAll: "replaceWith"
}, function( name, original ) {
	jQuery.fn[ name ] = function( selector ) {
		var elems,
		ret = [],
		insert = jQuery( selector ),
		last = insert.length - 1,
		i = 0;

		for ( ; i <= last; i++ ) {
			elems = i === last ? this : this.clone( true );
			jQuery( insert[ i ] )[ original ]( elems );

			// Support: Android<4.1, PhantomJS<2
			// .get() because push.apply(_, arraylike) throws on ancient WebKit
			push.apply( ret, elems.get() );
		}

		return this.pushStack( ret );
	};
});
var rmargin = (/^margin/);

var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );

var getStyles = function( elem ) {

		// Support: IE<=11+, Firefox<=30+ (#15098, #14150)
		// IE throws on elements created in popups
		// FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
		var view = elem.ownerDocument.defaultView;

		if ( !view.opener ) {
			view = window;
		}

		return view.getComputedStyle( elem );
	};

	var documentElement = document.documentElement;



	(function() {
		var pixelPositionVal, boxSizingReliableVal, pixelMarginRightVal,
		container = document.createElement( "div" ),
		div = document.createElement( "div" );

	// Finish early in limited (non-browser) environments
	if ( !div.style ) {
		return;
	}

	// Support: IE9-11+
	// Style of cloned element affects source element cloned (#8908)
	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	container.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;" +
	"padding:0;margin-top:1px;position:absolute";
	container.appendChild( div );

	// Executing both pixelPosition & boxSizingReliable tests require only one layout
	// so they're executed at the same time to save the second computation.
	function computeStyleTests() {
		div.style.cssText =
			// Support: Android 2.3
			// Vendor-prefix box-sizing
			"-webkit-box-sizing:border-box;box-sizing:border-box;" +
			"display:block;position:absolute;" +
			"margin:0;margin-top:1%;margin-right:50%;" +
			"border:1px;padding:1px;" +
			"top:1%;width:50%;height:4px";
			div.innerHTML = "";
			documentElement.appendChild( container );

			var divStyle = window.getComputedStyle( div );
			pixelPositionVal = divStyle.top !== "1%";
			boxSizingReliableVal = divStyle.height === "4px";
			pixelMarginRightVal = divStyle.marginRight === "4px";

			documentElement.removeChild( container );
		}

		jQuery.extend( support, {
			pixelPosition: function() {
			// This test is executed only once but we still do memoizing
			// since we can use the boxSizingReliable pre-computing.
			// No need to check if the test was already performed, though.
			computeStyleTests();
			return pixelPositionVal;
		},
		boxSizingReliable: function() {
			if ( boxSizingReliableVal == null ) {
				computeStyleTests();
			}
			return boxSizingReliableVal;
		},
		pixelMarginRight: function() {
			// Support: Android 4.0-4.3
			// We're checking for boxSizingReliableVal here instead of pixelMarginRightVal
			// since that compresses better and they're computed together anyway.
			if ( boxSizingReliableVal == null ) {
				computeStyleTests();
			}
			return pixelMarginRightVal;
		},
		reliableMarginRight: function() {

			// Support: Android 2.3
			// Check if div with explicit width and no margin-right incorrectly
			// gets computed margin-right based on width of container. (#3333)
			// WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
			// This support function is only executed once so no memoizing is needed.
			var ret,
			marginDiv = div.appendChild( document.createElement( "div" ) );

			// Reset CSS: box-sizing; display; margin; border; padding
			marginDiv.style.cssText = div.style.cssText =
				// Support: Android 2.3
				// Vendor-prefix box-sizing
				"-webkit-box-sizing:content-box;box-sizing:content-box;" +
				"display:block;margin:0;border:0;padding:0";
				marginDiv.style.marginRight = marginDiv.style.width = "0";
				div.style.width = "1px";
				documentElement.appendChild( container );

				ret = !parseFloat( window.getComputedStyle( marginDiv ).marginRight );

				documentElement.removeChild( container );
				div.removeChild( marginDiv );

				return ret;
			}
		});
})();


function curCSS( elem, name, computed ) {
	var width, minWidth, maxWidth, ret,
	style = elem.style;

	computed = computed || getStyles( elem );

	// Support: IE9
	// getPropertyValue is only needed for .css('filter') (#12537)
	if ( computed ) {
		ret = computed.getPropertyValue( name ) || computed[ name ];

		if ( ret === "" && !jQuery.contains( elem.ownerDocument, elem ) ) {
			ret = jQuery.style( elem, name );
		}

		// A tribute to the "awesome hack by Dean Edwards"
		// Android Browser returns percentage for some values,
		// but width seems to be reliably pixels.
		// This is against the CSSOM draft spec:
		// http://dev.w3.org/csswg/cssom/#resolved-values
		if ( !support.pixelMarginRight() && rnumnonpx.test( ret ) && rmargin.test( name ) ) {

			// Remember the original values
			width = style.width;
			minWidth = style.minWidth;
			maxWidth = style.maxWidth;

			// Put in the new values to get a computed value out
			style.minWidth = style.maxWidth = style.width = ret;
			ret = computed.width;

			// Revert the changed values
			style.width = width;
			style.minWidth = minWidth;
			style.maxWidth = maxWidth;
		}
	}

	return ret !== undefined ?
		// Support: IE9-11+
		// IE returns zIndex value as an integer.
		ret + "" :
		ret;
	}


	function addGetHookIf( conditionFn, hookFn ) {
	// Define the hook, we'll check on the first run if it's really needed.
	return {
		get: function() {
			if ( conditionFn() ) {
				// Hook not needed (or it's not possible to use it due
				// to missing dependency), remove it.
delete this.get;
return;
}

			// Hook needed; redefine it so that the support test is not executed again.
			return (this.get = hookFn).apply( this, arguments );
		}
	};
}


var
	// Swappable if display is none or starts with table
	// except "table", "table-cell", or "table-caption"
	// See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
	rdisplayswap = /^(none|table(?!-c[ea]).+)/,
	rnumsplit = new RegExp( "^(" + pnum + ")(.*)$", "i" ),

	cssShow = { position: "absolute", visibility: "hidden", display: "block" },
	cssNormalTransform = {
		letterSpacing: "0",
		fontWeight: "400"
	},

	cssPrefixes = [ "Webkit", "Moz", "ms" ],
	emptyStyle = document.createElement( "div" ).style;

// Return a css property mapped to a potentially vendor prefixed property
function vendorPropName( name ) {

	// Shortcut for names that are not vendor prefixed
	if ( name in emptyStyle ) {
		return name;
	}

	// Check for vendor prefixed names
	var capName = name[0].toUpperCase() + name.slice(1),
	i = cssPrefixes.length;

	while ( i-- ) {
		name = cssPrefixes[ i ] + capName;
		if ( name in emptyStyle ) {
			return name;
		}
	}
}

function setPositiveNumber( elem, value, subtract ) {
	var matches = rnumsplit.exec( value );
	return matches ?
		// Guard against undefined "subtract", e.g., when used as in cssHooks
		Math.max( 0, matches[ 1 ] - ( subtract || 0 ) ) + ( matches[ 2 ] || "px" ) :
		value;
	}

	function augmentWidthOrHeight( elem, name, extra, isBorderBox, styles ) {
		var i = extra === ( isBorderBox ? "border" : "content" ) ?
		// If we already have the right measurement, avoid augmentation
		4 :
		// Otherwise initialize for horizontal or vertical properties
		name === "width" ? 1 : 0,

		val = 0;

		for ( ; i < 4; i += 2 ) {
		// Both box models exclude margin, so add it if we want it
		if ( extra === "margin" ) {
			val += jQuery.css( elem, extra + cssExpand[ i ], true, styles );
		}

		if ( isBorderBox ) {
			// border-box includes padding, so remove it if we want content
			if ( extra === "content" ) {
				val -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
			}

			// At this point, extra isn't border nor margin, so remove border
			if ( extra !== "margin" ) {
				val -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		} else {
			// At this point, extra isn't content, so add padding
			val += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

			// At this point, extra isn't content nor padding, so add border
			if ( extra !== "padding" ) {
				val += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		}
	}

	return val;
}

function getWidthOrHeight( elem, name, extra ) {

	// Start with offset property, which is equivalent to the border-box value
	var val,
	valueIsBorderBox = true,
	styles = getStyles( elem ),
	isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box";

	// Support: IE <= 11 only
	// Running getBoundingClientRect on a disconnected node
	// in IE throws an error.
	if ( elem.getClientRects().length ) {
		val = elem.getBoundingClientRect()[ name ];
	}

	// Support: IE11 only
	// In IE 11 fullscreen elements inside of an iframe have
	// 100x too small dimensions (gh-1764).
	if ( document.msFullscreenElement && window.top !== window ) {
		val *= 100;
	}

	// Some non-html elements return undefined for offsetWidth, so check for null/undefined
	// svg - https://bugzilla.mozilla.org/show_bug.cgi?id=649285
	// MathML - https://bugzilla.mozilla.org/show_bug.cgi?id=491668
	if ( val <= 0 || val == null ) {
		// Fall back to computed then uncomputed css if necessary
		val = curCSS( elem, name, styles );
		if ( val < 0 || val == null ) {
			val = elem.style[ name ];
		}

		// Computed unit is not pixels. Stop here and return.
		if ( rnumnonpx.test(val) ) {
			return val;
		}

		// Check for style in case a browser which returns unreliable values
		// for getComputedStyle silently falls back to the reliable elem.style
		valueIsBorderBox = isBorderBox &&
		( support.boxSizingReliable() || val === elem.style[ name ] );

		// Normalize "", auto, and prepare for extra
		val = parseFloat( val ) || 0;
	}

	// Use the active box-sizing model to add/subtract irrelevant styles
	return ( val +
		augmentWidthOrHeight(
			elem,
			name,
			extra || ( isBorderBox ? "border" : "content" ),
			valueIsBorderBox,
			styles
			)
		) + "px";
}

jQuery.extend({

	// Add in style property hooks for overriding the default
	// behavior of getting and setting a style property
	cssHooks: {
		opacity: {
			get: function( elem, computed ) {
				if ( computed ) {

					// We should always get a number back from opacity
					var ret = curCSS( elem, "opacity" );
					return ret === "" ? "1" : ret;
				}
			}
		}
	},

	// Don't automatically add "px" to these possibly-unitless properties
	cssNumber: {
		"columnCount": true,
		"fillOpacity": true,
		"flexGrow": true,
		"flexShrink": true,
		"fontWeight": true,
		"lineHeight": true,
		"opacity": true,
		"order": true,
		"orphans": true,
		"widows": true,
		"zIndex": true,
		"zoom": true
	},

	// Add in properties whose names you wish to fix before
	// setting or getting the value
	cssProps: {
		"float": "cssFloat"
	},

	// Get and set the style property on a DOM Node
	style: function( elem, name, value, extra ) {

		// Don't set styles on text and comment nodes
		if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
			return;
		}

		// Make sure that we're working with the right name
		var ret, type, hooks,
		origName = jQuery.camelCase( name ),
		style = elem.style;

		name = jQuery.cssProps[ origName ] ||
		( jQuery.cssProps[ origName ] = vendorPropName( origName ) || origName );

		// Gets hook for the prefixed version, then unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// Check if we're setting a value
		if ( value !== undefined ) {
			type = typeof value;

			// Convert "+=" or "-=" to relative numbers (#7345)
			if ( type === "string" && (ret = rcssNum.exec( value )) && ret[ 1 ] ) {
				value = adjustCSS( elem, name, ret );
				// Fixes bug #9237
				type = "number";
			}

			// Make sure that null and NaN values aren't set (#7116)
			if ( value == null || value !== value ) {
				return;
			}

			// If a number was passed in, add the unit (except for certain CSS properties)
			if ( type === "number" ) {
				value += ret && ret[ 3 ] || ( jQuery.cssNumber[ origName ] ? "" : "px" );
			}

			// Support: IE9-11+
			// background-* props affect original clone's values
			if ( !support.clearCloneStyle && value === "" && name.indexOf( "background" ) === 0 ) {
				style[ name ] = "inherit";
			}

			// If a hook was provided, use that value, otherwise just set the specified value
			if ( !hooks || !("set" in hooks) ||
				(value = hooks.set( elem, value, extra )) !== undefined ) {

				style[ name ] = value;
		}

	} else {
			// If a hook was provided get the non-computed value from there
			if ( hooks && "get" in hooks &&
				(ret = hooks.get( elem, false, extra )) !== undefined ) {

				return ret;
		}

			// Otherwise just get the value from the style object
			return style[ name ];
		}
	},

	css: function( elem, name, extra, styles ) {
		var val, num, hooks,
		origName = jQuery.camelCase( name );

		// Make sure that we're working with the right name
		name = jQuery.cssProps[ origName ] ||
		( jQuery.cssProps[ origName ] = vendorPropName( origName ) || origName );

		// Try prefixed name followed by the unprefixed name
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// If a hook was provided get the computed value from there
		if ( hooks && "get" in hooks ) {
			val = hooks.get( elem, true, extra );
		}

		// Otherwise, if a way to get the computed value exists, use that
		if ( val === undefined ) {
			val = curCSS( elem, name, styles );
		}

		// Convert "normal" to computed value
		if ( val === "normal" && name in cssNormalTransform ) {
			val = cssNormalTransform[ name ];
		}

		// Make numeric if forced or a qualifier was provided and val looks numeric
		if ( extra === "" || extra ) {
			num = parseFloat( val );
			return extra === true || jQuery.isNumeric( num ) ? num || 0 : val;
		}
		return val;
	}
});

jQuery.each([ "height", "width" ], function( i, name ) {
	jQuery.cssHooks[ name ] = {
		get: function( elem, computed, extra ) {
			if ( computed ) {

				// Certain elements can have dimension info if we invisibly show them
				// but it must have a current display style that would benefit
				return rdisplayswap.test( jQuery.css( elem, "display" ) ) &&
					// Support: Safari 8+
					// Table columns in Safari have non-zero offsetWidth & zero
					// getBoundingClientRect().width unless display is changed.
					// Support: IE <= 11 only
					// Running getBoundingClientRect on a disconnected node
					// in IE throws an error.
					( !elem.getClientRects().length || !elem.getBoundingClientRect().width ) ?
					swap( elem, cssShow, function() {
						return getWidthOrHeight( elem, name, extra );
					}) :
					getWidthOrHeight( elem, name, extra );
				}
			},

			set: function( elem, value, extra ) {
				var styles = extra && getStyles( elem );
				return setPositiveNumber( elem, value, extra ?
					augmentWidthOrHeight(
						elem,
						name,
						extra,
						jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
						styles
						) : 0
					);
			}
		};
	});

// Support: Android 2.3
jQuery.cssHooks.marginRight = addGetHookIf( support.reliableMarginRight,
	function( elem, computed ) {
		if ( computed ) {
			return swap( elem, { "display": "inline-block" },
				curCSS, [ elem, "marginRight" ] );
		}
	}
	);

// These hooks are used by animate to expand properties
jQuery.each({
	margin: "",
	padding: "",
	border: "Width"
}, function( prefix, suffix ) {
	jQuery.cssHooks[ prefix + suffix ] = {
		expand: function( value ) {
			var i = 0,
			expanded = {},

				// Assumes a single number if not a string
				parts = typeof value === "string" ? value.split(" ") : [ value ];

				for ( ; i < 4; i++ ) {
					expanded[ prefix + cssExpand[ i ] + suffix ] =
					parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
				}

				return expanded;
			}
		};

		if ( !rmargin.test( prefix ) ) {
			jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
		}
	});

jQuery.fn.extend({
	css: function( name, value ) {
		return access( this, function( elem, name, value ) {
			var styles, len,
			map = {},
			i = 0;

			if ( jQuery.isArray( name ) ) {
				styles = getStyles( elem );
				len = name.length;

				for ( ; i < len; i++ ) {
					map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
				}

				return map;
			}

			return value !== undefined ?
			jQuery.style( elem, name, value ) :
			jQuery.css( elem, name );
		}, name, value, arguments.length > 1 );
	},
	show: function() {
		return showHide( this, true );
	},
	hide: function() {
		return showHide( this );
	},
	toggle: function( state ) {
		if ( typeof state === "boolean" ) {
			return state ? this.show() : this.hide();
		}

		return this.each(function() {
			if ( isHidden( this ) ) {
				jQuery( this ).show();
			} else {
				jQuery( this ).hide();
			}
		});
	}
});


function Tween( elem, options, prop, end, easing ) {
	return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
	constructor: Tween,
	init: function( elem, options, prop, end, easing, unit ) {
		this.elem = elem;
		this.prop = prop;
		this.easing = easing || jQuery.easing._default;
		this.options = options;
		this.start = this.now = this.cur();
		this.end = end;
		this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
	},
	cur: function() {
		var hooks = Tween.propHooks[ this.prop ];

		return hooks && hooks.get ?
		hooks.get( this ) :
		Tween.propHooks._default.get( this );
	},
	run: function( percent ) {
		var eased,
		hooks = Tween.propHooks[ this.prop ];

		if ( this.options.duration ) {
			this.pos = eased = jQuery.easing[ this.easing ](
				percent, this.options.duration * percent, 0, 1, this.options.duration
				);
		} else {
			this.pos = eased = percent;
		}
		this.now = ( this.end - this.start ) * eased + this.start;

		if ( this.options.step ) {
			this.options.step.call( this.elem, this.now, this );
		}

		if ( hooks && hooks.set ) {
			hooks.set( this );
		} else {
			Tween.propHooks._default.set( this );
		}
		return this;
	}
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
	_default: {
		get: function( tween ) {
			var result;

			// Use a property on the element directly when it is not a DOM element,
			// or when there is no matching style property that exists.
			if ( tween.elem.nodeType !== 1 ||
				tween.elem[ tween.prop ] != null && tween.elem.style[ tween.prop ] == null ) {
				return tween.elem[ tween.prop ];
		}

			// Passing an empty string as a 3rd parameter to .css will automatically
			// attempt a parseFloat and fallback to a string if the parse fails.
			// Simple values such as "10px" are parsed to Float;
			// complex values such as "rotate(1rad)" are returned as-is.
			result = jQuery.css( tween.elem, tween.prop, "" );
			// Empty strings, null, undefined and "auto" are converted to 0.
			return !result || result === "auto" ? 0 : result;
		},
		set: function( tween ) {
			// Use step hook for back compat.
			// Use cssHook if its there.
			// Use .style if available and use plain properties where available.
			if ( jQuery.fx.step[ tween.prop ] ) {
				jQuery.fx.step[ tween.prop ]( tween );
			} else if ( tween.elem.nodeType === 1 &&
				( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null ||
					jQuery.cssHooks[ tween.prop ] ) ) {
				jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
			} else {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	}
};

// Support: IE9
// Panic based approach to setting things on disconnected nodes
Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
	set: function( tween ) {
		if ( tween.elem.nodeType && tween.elem.parentNode ) {
			tween.elem[ tween.prop ] = tween.now;
		}
	}
};

jQuery.easing = {
	linear: function( p ) {
		return p;
	},
	swing: function( p ) {
		return 0.5 - Math.cos( p * Math.PI ) / 2;
	},
	_default: "swing"
};

jQuery.fx = Tween.prototype.init;

// Back Compat <1.8 extension point
jQuery.fx.step = {};




var
fxNow, timerId,
rfxtypes = /^(?:toggle|show|hide)$/,
rrun = /queueHooks$/;

function raf() {
	if ( timerId ) {
		window.requestAnimationFrame( raf );
		jQuery.fx.tick();
	}
}

// Animations created synchronously will run synchronously
function createFxNow() {
	window.setTimeout(function() {
		fxNow = undefined;
	});
	return ( fxNow = jQuery.now() );
}

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
	var which,
	i = 0,
	attrs = { height: type };

	// If we include width, step value is 1 to do all cssExpand values,
	// otherwise step value is 2 to skip over Left and Right
	includeWidth = includeWidth ? 1 : 0;
	for ( ; i < 4 ; i += 2 - includeWidth ) {
		which = cssExpand[ i ];
		attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
	}

	if ( includeWidth ) {
		attrs.opacity = attrs.width = type;
	}

	return attrs;
}

function createTween( value, prop, animation ) {
	var tween,
	collection = ( Animation.tweeners[ prop ] || [] ).concat( Animation.tweeners[ "*" ] ),
	index = 0,
	length = collection.length;
	for ( ; index < length; index++ ) {
		if ( (tween = collection[ index ].call( animation, prop, value )) ) {

			// We're done with this property
			return tween;
		}
	}
}

function defaultPrefilter( elem, props, opts ) {
	/* jshint validthis: true */
	var prop, value, toggle, hooks, oldfire, propTween, restoreDisplay, display,
	isBox = "width" in props || "height" in props,
	anim = this,
	orig = {},
	style = elem.style,
	hidden = elem.nodeType && isHidden( elem ),
	dataShow = dataPriv.get( elem, "fxshow" );

	// Queue-skipping animations hijack the fx hooks
	if ( !opts.queue ) {
		hooks = jQuery._queueHooks( elem, "fx" );
		if ( hooks.unqueued == null ) {
			hooks.unqueued = 0;
			oldfire = hooks.empty.fire;
			hooks.empty.fire = function() {
				if ( !hooks.unqueued ) {
					oldfire();
				}
			};
		}
		hooks.unqueued++;

		anim.always(function() {
			// Ensure the complete handler is called before this completes
			anim.always(function() {
				hooks.unqueued--;
				if ( !jQuery.queue( elem, "fx" ).length ) {
					hooks.empty.fire();
				}
			});
		});
	}

	// Detect show/hide animations
	for ( prop in props ) {
		value = props[ prop ];
		if ( rfxtypes.test( value ) ) {
			delete props[ prop ];
			toggle = toggle || value === "toggle";
			if ( value === ( hidden ? "hide" : "show" ) ) {

				// Pretend to be hidden if this is a "show" and
				// there is still data from a stopped show/hide
				if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
					hidden = true;

				// Ignore all other no-op show/hide data
			} else {
				continue;
			}
		}
		orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );
	}
}

	// Bail out if this is a no-op like .hide().hide()
	propTween = !jQuery.isEmptyObject( props );
	if ( !propTween && jQuery.isEmptyObject( orig ) ) {
		return;
	}

	// Restrict "overflow" and "display" styles during box animations
	if ( isBox && elem.nodeType === 1 ) {
		// Support: IE 9 - 11
		// Record all 3 overflow attributes because IE does not infer the shorthand
		// from identically-valued overflowX and overflowY
		opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

		// Identify a display type, preferring old show/hide data over the CSS cascade
		restoreDisplay = dataShow && dataShow.display;
		if ( restoreDisplay == null ) {
			restoreDisplay = dataPriv.get( elem, "display" );
		}
		display = jQuery.css( elem, "display" );
		if ( display === "none" ) {
			display = restoreDisplay || swap( elem, { "display": "" }, function() {
				return jQuery.css( elem, "display" );
			} );
		}

		// Animate inline elements as inline-block
		if ( display === "inline" || display === "inline-block" && restoreDisplay != null ) {
			if ( jQuery.css( elem, "float" ) === "none" ) {

				// Restore the original display value at the end of pure show/hide animations
				if ( !propTween ) {
					anim.done(function() {
						style.display = restoreDisplay;
					});
					if ( restoreDisplay == null ) {
						display = style.display;
						restoreDisplay = display === "none" ? "" : display;
					}
				}
				style.display = "inline-block";
			}
		}
	}

	if ( opts.overflow ) {
		style.overflow = "hidden";
		anim.always(function() {
			style.overflow = opts.overflow[ 0 ];
			style.overflowX = opts.overflow[ 1 ];
			style.overflowY = opts.overflow[ 2 ];
		});
	}

	// Implement show/hide animations
	propTween = false;
	for ( prop in orig ) {

		// General show/hide setup for this element animation
		if ( !propTween ) {
			if ( dataShow ) {
				if ( "hidden" in dataShow ) {
					hidden = dataShow.hidden;
				}
			} else {
				dataShow = dataPriv.access( elem, "fxshow", { display: restoreDisplay } );
			}

			// Store hidden/visible for toggle so `.stop().toggle()` "reverses"
			if ( toggle ) {
				dataShow.hidden = !hidden;
			}

			// Show elements before animating them
			if ( hidden ) {
				showHide( [ elem ], true );
			}

			/* jshint -W083 */
			anim.done(function() {
				// The final step of a "hide" animation is actually hiding the element
				if ( !hidden ) {
					showHide( [ elem ] );
				}
				dataPriv.remove( elem, "fxshow" );
				for ( prop in orig ) {
					jQuery.style( elem, prop, orig[ prop ] );
				}
			});
		}

		// Per-property setup
		propTween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );
		if ( !( prop in dataShow ) ) {
			dataShow[ prop ] = propTween.start;
			if ( hidden ) {
				propTween.end = propTween.start;
				propTween.start = prop === "width" || prop === "height" ? 1 : 0;
			}
		}
	}
}

function propFilter( props, specialEasing ) {
	var index, name, easing, value, hooks;

	// camelCase, specialEasing and expand cssHook pass
	for ( index in props ) {
		name = jQuery.camelCase( index );
		easing = specialEasing[ name ];
		value = props[ index ];
		if ( jQuery.isArray( value ) ) {
			easing = value[ 1 ];
			value = props[ index ] = value[ 0 ];
		}

		if ( index !== name ) {
			props[ name ] = value;
			delete props[ index ];
		}

		hooks = jQuery.cssHooks[ name ];
		if ( hooks && "expand" in hooks ) {
			value = hooks.expand( value );
			delete props[ name ];

			// Not quite $.extend, this won't overwrite existing keys.
			// Reusing 'index' because we have the correct "name"
			for ( index in value ) {
				if ( !( index in props ) ) {
					props[ index ] = value[ index ];
					specialEasing[ index ] = easing;
				}
			}
		} else {
			specialEasing[ name ] = easing;
		}
	}
}

function Animation( elem, properties, options ) {
	var result,
	stopped,
	index = 0,
	length = Animation.prefilters.length,
	deferred = jQuery.Deferred().always( function() {
			// Don't match elem in the :animated selector
			delete tick.elem;
		}),
	tick = function() {
		if ( stopped ) {
			return false;
		}
		var currentTime = fxNow || createFxNow(),
		remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),
				// Support: Android 2.3
				// Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
				temp = remaining / animation.duration || 0,
				percent = 1 - temp,
				index = 0,
				length = animation.tweens.length;

				for ( ; index < length ; index++ ) {
					animation.tweens[ index ].run( percent );
				}

				deferred.notifyWith( elem, [ animation, percent, remaining ]);

				if ( percent < 1 && length ) {
					return remaining;
				} else {
					deferred.resolveWith( elem, [ animation ] );
					return false;
				}
			},
			animation = deferred.promise({
				elem: elem,
				props: jQuery.extend( {}, properties ),
				opts: jQuery.extend( true, {
					specialEasing: {},
					easing: jQuery.easing._default
				}, options ),
				originalProperties: properties,
				originalOptions: options,
				startTime: fxNow || createFxNow(),
				duration: options.duration,
				tweens: [],
				createTween: function( prop, end ) {
					var tween = jQuery.Tween( elem, animation.opts, prop, end,
						animation.opts.specialEasing[ prop ] || animation.opts.easing );
					animation.tweens.push( tween );
					return tween;
				},
				stop: function( gotoEnd ) {
					var index = 0,
					// If we are going to the end, we want to run all the tweens
					// otherwise we skip this part
					length = gotoEnd ? animation.tweens.length : 0;
					if ( stopped ) {
						return this;
					}
					stopped = true;
					for ( ; index < length ; index++ ) {
						animation.tweens[ index ].run( 1 );
					}

				// Resolve when we played the last frame; otherwise, reject
				if ( gotoEnd ) {
					deferred.resolveWith( elem, [ animation, gotoEnd ] );
				} else {
					deferred.rejectWith( elem, [ animation, gotoEnd ] );
				}
				return this;
			}
		}),
props = animation.props;

propFilter( props, animation.opts.specialEasing );

for ( ; index < length ; index++ ) {
	result = Animation.prefilters[ index ].call( animation, elem, props, animation.opts );
	if ( result ) {
		if ( jQuery.isFunction( result.stop ) ) {
			jQuery._queueHooks( animation.elem, animation.opts.queue ).stop =
			jQuery.proxy( result.stop, result );
		}
		return result;
	}
}

jQuery.map( props, createTween, animation );

if ( jQuery.isFunction( animation.opts.start ) ) {
	animation.opts.start.call( elem, animation );
}

jQuery.fx.timer(
	jQuery.extend( tick, {
		elem: elem,
		anim: animation,
		queue: animation.opts.queue
	})
	);

	// attach callbacks from options
	return animation.progress( animation.opts.progress )
	.done( animation.opts.done, animation.opts.complete )
	.fail( animation.opts.fail )
	.always( animation.opts.always );
}

jQuery.Animation = jQuery.extend( Animation, {

	tweeners: {
		"*": [ function( prop, value ) {
			var tween = this.createTween( prop, value );
			adjustCSS( tween.elem, prop, rcssNum.exec( value ), tween );
			return tween;
		} ]
	},

	tweener: function( props, callback ) {
		if ( jQuery.isFunction( props ) ) {
			callback = props;
			props = [ "*" ];
		} else {
			props = props.match( rnotwhite );
		}

		var prop,
		index = 0,
		length = props.length;

		for ( ; index < length ; index++ ) {
			prop = props[ index ];
			Animation.tweeners[ prop ] = Animation.tweeners[ prop ] || [];
			Animation.tweeners[ prop ].unshift( callback );
		}
	},

	prefilters: [ defaultPrefilter ],

	prefilter: function( callback, prepend ) {
		if ( prepend ) {
			Animation.prefilters.unshift( callback );
		} else {
			Animation.prefilters.push( callback );
		}
	}
});

jQuery.speed = function( speed, easing, fn ) {
	var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
		complete: fn || !fn && easing ||
		jQuery.isFunction( speed ) && speed,
		duration: speed,
		easing: fn && easing || easing && !jQuery.isFunction( easing ) && easing
	};

	// Go to the end state if fx are off or if document is hidden
	if ( jQuery.fx.off || document.hidden ) {
		opt.duration = 0;

	} else {
		opt.duration = typeof opt.duration === "number" ?
		opt.duration : opt.duration in jQuery.fx.speeds ?
		jQuery.fx.speeds[ opt.duration ] : jQuery.fx.speeds._default;
	}

	// Normalize opt.queue - true/undefined/null -> "fx"
	if ( opt.queue == null || opt.queue === true ) {
		opt.queue = "fx";
	}

	// Queueing
	opt.old = opt.complete;

	opt.complete = function() {
		if ( jQuery.isFunction( opt.old ) ) {
			opt.old.call( this );
		}

		if ( opt.queue ) {
			jQuery.dequeue( this, opt.queue );
		}
	};

	return opt;
};

jQuery.fn.extend({
	fadeTo: function( speed, to, easing, callback ) {

		// Show any hidden elements after setting opacity to 0
		return this.filter( isHidden ).css( "opacity", 0 ).show()

			// Animate to the value specified
			.end().animate({ opacity: to }, speed, easing, callback );
		},
		animate: function( prop, speed, easing, callback ) {
			var empty = jQuery.isEmptyObject( prop ),
			optall = jQuery.speed( speed, easing, callback ),
			doAnimation = function() {
				// Operate on a copy of prop so per-property easing won't be lost
				var anim = Animation( this, jQuery.extend( {}, prop ), optall );

				// Empty animations, or finishing resolves immediately
				if ( empty || dataPriv.get( this, "finish" ) ) {
					anim.stop( true );
				}
			};
			doAnimation.finish = doAnimation;

			return empty || optall.queue === false ?
			this.each( doAnimation ) :
			this.queue( optall.queue, doAnimation );
		},
		stop: function( type, clearQueue, gotoEnd ) {
			var stopQueue = function( hooks ) {
				var stop = hooks.stop;
				delete hooks.stop;
				stop( gotoEnd );
			};

			if ( typeof type !== "string" ) {
				gotoEnd = clearQueue;
				clearQueue = type;
				type = undefined;
			}
			if ( clearQueue && type !== false ) {
				this.queue( type || "fx", [] );
			}

			return this.each(function() {
				var dequeue = true,
				index = type != null && type + "queueHooks",
				timers = jQuery.timers,
				data = dataPriv.get( this );

				if ( index ) {
					if ( data[ index ] && data[ index ].stop ) {
						stopQueue( data[ index ] );
					}
				} else {
					for ( index in data ) {
						if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
							stopQueue( data[ index ] );
						}
					}
				}

				for ( index = timers.length; index--; ) {
					if ( timers[ index ].elem === this &&
						(type == null || timers[ index ].queue === type) ) {

						timers[ index ].anim.stop( gotoEnd );
					dequeue = false;
					timers.splice( index, 1 );
				}
			}

			// Start the next in the queue if the last step wasn't forced.
			// Timers currently will call their complete callbacks, which
			// will dequeue but only if they were gotoEnd.
			if ( dequeue || !gotoEnd ) {
				jQuery.dequeue( this, type );
			}
		});
		},
		finish: function( type ) {
			if ( type !== false ) {
				type = type || "fx";
			}
			return this.each(function() {
				var index,
				data = dataPriv.get( this ),
				queue = data[ type + "queue" ],
				hooks = data[ type + "queueHooks" ],
				timers = jQuery.timers,
				length = queue ? queue.length : 0;

			// Enable finishing flag on private data
			data.finish = true;

			// Empty the queue first
			jQuery.queue( this, type, [] );

			if ( hooks && hooks.stop ) {
				hooks.stop.call( this, true );
			}

			// Look for any active animations, and finish them
			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
					timers[ index ].anim.stop( true );
					timers.splice( index, 1 );
				}
			}

			// Look for any animations in the old queue and finish them
			for ( index = 0; index < length; index++ ) {
				if ( queue[ index ] && queue[ index ].finish ) {
					queue[ index ].finish.call( this );
				}
			}

			// Turn off finishing flag
			delete data.finish;
		});
		}
	});

jQuery.each([ "toggle", "show", "hide" ], function( i, name ) {
	var cssFn = jQuery.fn[ name ];
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return speed == null || typeof speed === "boolean" ?
		cssFn.apply( this, arguments ) :
		this.animate( genFx( name, true ), speed, easing, callback );
	};
});

// Generate shortcuts for custom animations
jQuery.each({
	slideDown: genFx("show"),
	slideUp: genFx("hide"),
	slideToggle: genFx("toggle"),
	fadeIn: { opacity: "show" },
	fadeOut: { opacity: "hide" },
	fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return this.animate( props, speed, easing, callback );
	};
});

jQuery.timers = [];
jQuery.fx.tick = function() {
	var timer,
	i = 0,
	timers = jQuery.timers;

	fxNow = jQuery.now();

	for ( ; i < timers.length; i++ ) {
		timer = timers[ i ];
		// Checks the timer has not already been removed
		if ( !timer() && timers[ i ] === timer ) {
			timers.splice( i--, 1 );
		}
	}

	if ( !timers.length ) {
		jQuery.fx.stop();
	}
	fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
	jQuery.timers.push( timer );
	if ( timer() ) {
		jQuery.fx.start();
	} else {
		jQuery.timers.pop();
	}
};

jQuery.fx.interval = 13;
jQuery.fx.start = function() {
	if ( !timerId ) {
		timerId = window.requestAnimationFrame ?
		window.requestAnimationFrame( raf ) :
		window.setInterval( jQuery.fx.tick, jQuery.fx.interval );
	}
};

jQuery.fx.stop = function() {
	if ( window.cancelAnimationFrame ) {
		window.cancelAnimationFrame( timerId );
	} else {
		window.clearInterval( timerId );
	}

	timerId = null;
};

jQuery.fx.speeds = {
	slow: 600,
	fast: 200,
	// Default speed
	_default: 400
};


// Based off of the plugin by Clint Helfers, with permission.
// http://web.archive.org/web/20100324014747/http://blindsignals.com/index.php/2009/07/jquery-delay/
jQuery.fn.delay = function( time, type ) {
	time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
	type = type || "fx";

	return this.queue( type, function( next, hooks ) {
		var timeout = window.setTimeout( next, time );
		hooks.stop = function() {
			window.clearTimeout( timeout );
		};
	});
};


(function() {
	var input = document.createElement( "input" ),
	select = document.createElement( "select" ),
	opt = select.appendChild( document.createElement( "option" ) );

	input.type = "checkbox";

	// Support: Android<4.4
	// Default value for a checkbox should be "on"
	support.checkOn = input.value !== "";

	// Support: IE<=11+
	// Must access selectedIndex to make default options select
	support.optSelected = opt.selected;

	// Support: Android<=2.3
	// Options inside disabled selects are incorrectly marked as disabled
	select.disabled = true;
	support.optDisabled = !opt.disabled;

	// Support: IE<=11+
	// An input loses its value after becoming a radio
	input = document.createElement( "input" );
	input.value = "t";
	input.type = "radio";
	support.radioValue = input.value === "t";
})();


var boolHook,
attrHandle = jQuery.expr.attrHandle;

jQuery.fn.extend({
	attr: function( name, value ) {
		return access( this, jQuery.attr, name, value, arguments.length > 1 );
	},

	removeAttr: function( name ) {
		return this.each(function() {
			jQuery.removeAttr( this, name );
		});
	}
});

jQuery.extend({
	attr: function( elem, name, value ) {
		var ret, hooks,
		nType = elem.nodeType;

		// Don't get/set attributes on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		// Fallback to prop when attributes are not supported
		if ( typeof elem.getAttribute === "undefined" ) {
			return jQuery.prop( elem, name, value );
		}

		// All attributes are lowercase
		// Grab necessary hook if one is defined
		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
			name = name.toLowerCase();
			hooks = jQuery.attrHooks[ name ] ||
			( jQuery.expr.match.bool.test( name ) ? boolHook : undefined );
		}

		if ( value !== undefined ) {
			if ( value === null ) {
				jQuery.removeAttr( elem, name );
				return;
			}

			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
		}

		elem.setAttribute( name, value + "" );
		return value;
	}

	if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
		return ret;
	}

	ret = jQuery.find.attr( elem, name );

		// Non-existent attributes return null, we normalize to undefined
		return ret == null ? undefined : ret;
	},

	attrHooks: {
		type: {
			set: function( elem, value ) {
				if ( !support.radioValue && value === "radio" &&
					jQuery.nodeName( elem, "input" ) ) {
					var val = elem.value;
				elem.setAttribute( "type", value );
				if ( val ) {
					elem.value = val;
				}
				return value;
			}
		}
	}
},

removeAttr: function( elem, value ) {
	var name, propName,
	i = 0,
	attrNames = value && value.match( rnotwhite );

	if ( attrNames && elem.nodeType === 1 ) {
		while ( ( name = attrNames[i++] ) ) {
			propName = jQuery.propFix[ name ] || name;

				// Boolean attributes get special treatment (#10870)
				if ( jQuery.expr.match.bool.test( name ) ) {

					// Set corresponding property to false
					elem[ propName ] = false;
				}

				elem.removeAttribute( name );
			}
		}
	}
});

// Hooks for boolean attributes
boolHook = {
	set: function( elem, value, name ) {
		if ( value === false ) {
			// Remove boolean attributes when set to false
			jQuery.removeAttr( elem, name );
		} else {
			elem.setAttribute( name, name );
		}
		return name;
	}
};
jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {
	var getter = attrHandle[ name ] || jQuery.find.attr;

	attrHandle[ name ] = function( elem, name, isXML ) {
		var ret, handle;
		if ( !isXML ) {
			// Avoid an infinite loop by temporarily removing this function from the getter
			handle = attrHandle[ name ];
			attrHandle[ name ] = ret;
			ret = getter( elem, name, isXML ) != null ?
			name.toLowerCase() :
			null;
			attrHandle[ name ] = handle;
		}
		return ret;
	};
});




var rfocusable = /^(?:input|select|textarea|button)$/i;

jQuery.fn.extend({
	prop: function( name, value ) {
		return access( this, jQuery.prop, name, value, arguments.length > 1 );
	},

	removeProp: function( name ) {
		return this.each(function() {
			delete this[ jQuery.propFix[ name ] || name ];
		});
	}
});

jQuery.extend({
	prop: function( elem, name, value ) {
		var ret, hooks,
		nType = elem.nodeType;

		// Don't get/set properties on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {

			// Fix name and attach hooks
			name = jQuery.propFix[ name ] || name;
			hooks = jQuery.propHooks[ name ];
		}

		if ( value !== undefined ) {
			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
		}

		return ( elem[ name ] = value );
	}

	if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
		return ret;
	}

	return elem[ name ];
},

propHooks: {
	tabIndex: {
		get: function( elem ) {
			return elem.hasAttribute( "tabindex" ) ||
			rfocusable.test( elem.nodeName ) || elem.href ?
			elem.tabIndex :
			-1;
		}
	}
},

propFix: {
	"for": "htmlFor",
	"class": "className"
}
});

if ( !support.optSelected ) {
	jQuery.propHooks.selected = {
		get: function( elem ) {
			var parent = elem.parentNode;
			if ( parent && parent.parentNode ) {
				parent.parentNode.selectedIndex;
			}
			return null;
		}
	};
}

jQuery.each([
	"tabIndex",
	"readOnly",
	"maxLength",
	"cellSpacing",
	"cellPadding",
	"rowSpan",
	"colSpan",
	"useMap",
	"frameBorder",
	"contentEditable"
	], function() {
		jQuery.propFix[ this.toLowerCase() ] = this;
	});




var rclass = /[\t\r\n\f]/g;

function getClass( elem ) {
	return elem.getAttribute && elem.getAttribute( "class" ) || "";
}

jQuery.fn.extend({
	addClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
		proceed = typeof value === "string" && value,
		i = 0,
		len = this.length;

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( j ) {
				jQuery( this ).addClass( value.call( this, j, getClass( this ) ) );
			});
		}

		if ( proceed ) {
			// The disjunction here is for better compressibility (see removeClass)
			classes = ( value || "" ).match( rnotwhite ) || [];

			for ( ; i < len; i++ ) {
				elem = this[ i ];
				curValue = getClass( elem );
				cur = elem.nodeType === 1 &&
				( " " + curValue + " " ).replace( rclass, " " );

				if ( cur ) {
					j = 0;
					while ( (clazz = classes[j++]) ) {
						if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
							cur += clazz + " ";
						}
					}

					// only assign if different to avoid unneeded rendering.
					finalValue = jQuery.trim( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	removeClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
		proceed = arguments.length === 0 || typeof value === "string" && value,
		i = 0,
		len = this.length;

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( j ) {
				jQuery( this ).removeClass( value.call( this, j, getClass( this ) ) );
			});
		}
		if ( proceed ) {
			classes = ( value || "" ).match( rnotwhite ) || [];

			for ( ; i < len; i++ ) {
				elem = this[ i ];
				curValue = getClass( elem );

				// This expression is here for better compressibility (see addClass)
				cur = elem.nodeType === 1 &&
				( " " + curValue + " " ).replace( rclass, " " );

				if ( cur ) {
					j = 0;
					while ( (clazz = classes[j++]) ) {
						// Remove *all* instances
						while ( cur.indexOf( " " + clazz + " " ) > -1 ) {
							cur = cur.replace( " " + clazz + " ", " " );
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = value ? jQuery.trim( cur ) : "";
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	toggleClass: function( value, stateVal ) {
		var type = typeof value;

		if ( typeof stateVal === "boolean" && type === "string" ) {
			return stateVal ? this.addClass( value ) : this.removeClass( value );
		}

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( i ) {
				jQuery( this ).toggleClass(
					value.call( this, i, getClass( this ), stateVal ),
					stateVal
					);
			});
		}

		return this.each(function() {
			var className, i, self, classNames;

			if ( type === "string" ) {

				// Toggle individual class names
				i = 0;
				self = jQuery( this );
				classNames = value.match( rnotwhite ) || [];

				while ( ( className = classNames[ i++ ] ) ) {

					// Check each className given, space separated list
					if ( self.hasClass( className ) ) {
						self.removeClass( className );
					} else {
						self.addClass( className );
					}
				}

			// Toggle whole class name
		} else if ( value === undefined || type === "boolean" ) {
			className = getClass( this );
			if ( className ) {

					// store className if set
					dataPriv.set( this, "__className__", className );
				}

				// If the element has a class name or if we're passed `false`,
				// then remove the whole classname (if there was one, the above saved it).
				// Otherwise bring back whatever was previously saved (if anything),
				// falling back to the empty string if nothing was stored.
				if ( this.setAttribute ) {
					this.setAttribute( "class",
						className || value === false ?
						"" :
						dataPriv.get( this, "__className__" ) || ""
						);
				}
			}
		});
},

hasClass: function( selector ) {
	var className = " " + selector + " ",
	i = 0,
	l = this.length;
	for ( ; i < l; i++ ) {
		if ( this[i].nodeType === 1 &&
			( " " + getClass( this[i] ) + " " ).replace( rclass, " " )
			.indexOf( className ) > -1
			) {
			return true;
	}
}

return false;
}
});




var rreturn = /\r/g;

jQuery.fn.extend({
	val: function( value ) {
		var hooks, ret, isFunction,
		elem = this[0];

		if ( !arguments.length ) {
			if ( elem ) {
				hooks = jQuery.valHooks[ elem.type ] ||
				jQuery.valHooks[ elem.nodeName.toLowerCase() ];

				if ( hooks && "get" in hooks && (ret = hooks.get( elem, "value" )) !== undefined ) {
					return ret;
				}

				ret = elem.value;

				return typeof ret === "string" ?
					// Handle most common string cases
					ret.replace(rreturn, "") :
					// Handle cases where value is null/undef or number
					ret == null ? "" : ret;
				}

				return;
			}

			isFunction = jQuery.isFunction( value );

			return this.each(function( i ) {
				var val;

				if ( this.nodeType !== 1 ) {
					return;
				}

				if ( isFunction ) {
					val = value.call( this, i, jQuery( this ).val() );
				} else {
					val = value;
				}

			// Treat null/undefined as ""; convert numbers to string
			if ( val == null ) {
				val = "";

			} else if ( typeof val === "number" ) {
				val += "";

			} else if ( jQuery.isArray( val ) ) {
				val = jQuery.map( val, function( value ) {
					return value == null ? "" : value + "";
				});
			}

			hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

			// If set returns undefined, fall back to normal setting
			if ( !hooks || !("set" in hooks) || hooks.set( this, val, "value" ) === undefined ) {
				this.value = val;
			}
		});
		}
	});

jQuery.extend({
	valHooks: {
		option: {
			get: function( elem ) {
				// Support: IE<11
				// option.value not trimmed (#14858)
				return jQuery.trim( elem.value );
			}
		},
		select: {
			get: function( elem ) {
				var value, option,
				options = elem.options,
				index = elem.selectedIndex,
				one = elem.type === "select-one" || index < 0,
				values = one ? null : [],
				max = one ? index + 1 : options.length,
				i = index < 0 ?
				max :
				one ? index : 0;

				// Loop through all the selected options
				for ( ; i < max; i++ ) {
					option = options[ i ];

					// IE8-9 doesn't update selected after form reset (#2551)
					if ( ( option.selected || i === index ) &&
							// Don't return options that are disabled or in a disabled optgroup
							( support.optDisabled ?
								!option.disabled : option.getAttribute( "disabled" ) === null ) &&
							( !option.parentNode.disabled ||
								!jQuery.nodeName( option.parentNode, "optgroup" ) ) ) {

						// Get the specific value for the option
					value = jQuery( option ).val();

						// We don't need an array for one selects
						if ( one ) {
							return value;
						}

						// Multi-Selects return an array
						values.push( value );
					}
				}

				return values;
			},

			set: function( elem, value ) {
				var optionSet, option,
				options = elem.options,
				values = jQuery.makeArray( value ),
				i = options.length;

				while ( i-- ) {
					option = options[ i ];
					if ( (option.selected =
						jQuery.inArray( jQuery.valHooks.option.get( option ), values ) > -1) ) {
						optionSet = true;
				}
			}

				// Force browsers to behave consistently when non-matching value is set
				if ( !optionSet ) {
					elem.selectedIndex = -1;
				}
				return values;
			}
		}
	}
});

// Radios and checkboxes getter/setter
jQuery.each([ "radio", "checkbox" ], function() {
	jQuery.valHooks[ this ] = {
		set: function( elem, value ) {
			if ( jQuery.isArray( value ) ) {
				return ( elem.checked = jQuery.inArray( jQuery(elem).val(), value ) > -1 );
			}
		}
	};
	if ( !support.checkOn ) {
		jQuery.valHooks[ this ].get = function( elem ) {
			return elem.getAttribute("value") === null ? "on" : elem.value;
		};
	}
});




// Return jQuery for attributes-only inclusion


jQuery.each( ("blur focus focusin focusout resize scroll click dblclick " +
	"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
	"change select submit keydown keypress keyup contextmenu").split(" "),
function( i, name ) {

	// Handle event binding
	jQuery.fn[ name ] = function( data, fn ) {
		return arguments.length > 0 ?
		this.on( name, null, data, fn ) :
		this.trigger( name );
	};
});

jQuery.fn.extend({
	hover: function( fnOver, fnOut ) {
		return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
	},

	bind: function( types, data, fn ) {
		return this.on( types, null, data, fn );
	},
	unbind: function( types, fn ) {
		return this.off( types, null, fn );
	},

	delegate: function( selector, types, data, fn ) {
		return this.on( types, selector, data, fn );
	},
	undelegate: function( selector, types, fn ) {
		// ( namespace ) or ( selector, types [, fn] )
		return arguments.length === 1 ?
		this.off( selector, "**" ) :
		this.off( types, selector || "**", fn );
	}
});


var location = window.location;

var nonce = jQuery.now();

var rquery = (/\?/);



// Support: Android 2.3
// Workaround failure to string-cast null input
jQuery.parseJSON = function( data ) {
	return JSON.parse( data + "" );
};


// Cross-browser xml parsing
jQuery.parseXML = function( data ) {
	var xml;
	if ( !data || typeof data !== "string" ) {
		return null;
	}

	// Support: IE9
	try {
		xml = ( new window.DOMParser() ).parseFromString( data, "text/xml" );
	} catch ( e ) {
		xml = undefined;
	}

	if ( !xml || xml.getElementsByTagName( "parsererror" ).length ) {
		jQuery.error( "Invalid XML: " + data );
	}
	return xml;
};


var
rhash = /#.*$/,
rts = /([?&])_=[^&]*/,
rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,
	// #7653, #8125, #8152: local protocol detection
	rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
	rnoContent = /^(?:GET|HEAD)$/,
	rprotocol = /^\/\//,

	/* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
*/
prefilters = {},

	/* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
*/
transports = {},

	// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
	allTypes = "*/".concat( "*" ),

	// Anchor tag for parsing the document origin
	originAnchor = document.createElement( "a" );
	originAnchor.href = location.href;

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

	// dataTypeExpression is optional and defaults to "*"
	return function( dataTypeExpression, func ) {

		if ( typeof dataTypeExpression !== "string" ) {
			func = dataTypeExpression;
			dataTypeExpression = "*";
		}

		var dataType,
		i = 0,
		dataTypes = dataTypeExpression.toLowerCase().match( rnotwhite ) || [];

		if ( jQuery.isFunction( func ) ) {
			// For each dataType in the dataTypeExpression
			while ( (dataType = dataTypes[i++]) ) {
				// Prepend if requested
				if ( dataType[0] === "+" ) {
					dataType = dataType.slice( 1 ) || "*";
					(structure[ dataType ] = structure[ dataType ] || []).unshift( func );

				// Otherwise append
			} else {
				(structure[ dataType ] = structure[ dataType ] || []).push( func );
			}
		}
	}
};
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

	var inspected = {},
	seekingTransport = ( structure === transports );

	function inspect( dataType ) {
		var selected;
		inspected[ dataType ] = true;
		jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
			var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
			if ( typeof dataTypeOrTransport === "string" &&
				!seekingTransport && !inspected[ dataTypeOrTransport ] ) {

				options.dataTypes.unshift( dataTypeOrTransport );
			inspect( dataTypeOrTransport );
			return false;
		} else if ( seekingTransport ) {
			return !( selected = dataTypeOrTransport );
		}
	});
		return selected;
	}

	return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
	var key, deep,
	flatOptions = jQuery.ajaxSettings.flatOptions || {};

	for ( key in src ) {
		if ( src[ key ] !== undefined ) {
			( flatOptions[ key ] ? target : ( deep || (deep = {}) ) )[ key ] = src[ key ];
		}
	}
	if ( deep ) {
		jQuery.extend( true, target, deep );
	}

	return target;
}

/* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
 function ajaxHandleResponses( s, jqXHR, responses ) {

 	var ct, type, finalDataType, firstDataType,
 	contents = s.contents,
 	dataTypes = s.dataTypes;

	// Remove auto dataType and get content-type in the process
	while ( dataTypes[ 0 ] === "*" ) {
		dataTypes.shift();
		if ( ct === undefined ) {
			ct = s.mimeType || jqXHR.getResponseHeader("Content-Type");
		}
	}

	// Check if we're dealing with a known content-type
	if ( ct ) {
		for ( type in contents ) {
			if ( contents[ type ] && contents[ type ].test( ct ) ) {
				dataTypes.unshift( type );
				break;
			}
		}
	}

	// Check to see if we have a response for the expected dataType
	if ( dataTypes[ 0 ] in responses ) {
		finalDataType = dataTypes[ 0 ];
	} else {
		// Try convertible dataTypes
		for ( type in responses ) {
			if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[0] ] ) {
				finalDataType = type;
				break;
			}
			if ( !firstDataType ) {
				firstDataType = type;
			}
		}
		// Or just use first one
		finalDataType = finalDataType || firstDataType;
	}

	// If we found a dataType
	// We add the dataType to the list if needed
	// and return the corresponding response
	if ( finalDataType ) {
		if ( finalDataType !== dataTypes[ 0 ] ) {
			dataTypes.unshift( finalDataType );
		}
		return responses[ finalDataType ];
	}
}

/* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
 function ajaxConvert( s, response, jqXHR, isSuccess ) {
 	var conv2, current, conv, tmp, prev,
 	converters = {},
		// Work with a copy of dataTypes in case we need to modify it for conversion
		dataTypes = s.dataTypes.slice();

	// Create converters map with lowercased keys
	if ( dataTypes[ 1 ] ) {
		for ( conv in s.converters ) {
			converters[ conv.toLowerCase() ] = s.converters[ conv ];
		}
	}

	current = dataTypes.shift();

	// Convert to each sequential dataType
	while ( current ) {

		if ( s.responseFields[ current ] ) {
			jqXHR[ s.responseFields[ current ] ] = response;
		}

		// Apply the dataFilter if provided
		if ( !prev && isSuccess && s.dataFilter ) {
			response = s.dataFilter( response, s.dataType );
		}

		prev = current;
		current = dataTypes.shift();

		if ( current ) {

		// There's only work to do if current dataType is non-auto
		if ( current === "*" ) {

			current = prev;

			// Convert response if prev dataType is non-auto and differs from current
		} else if ( prev !== "*" && prev !== current ) {

				// Seek a direct converter
				conv = converters[ prev + " " + current ] || converters[ "* " + current ];

				// If none found, seek a pair
				if ( !conv ) {
					for ( conv2 in converters ) {

						// If conv2 outputs current
						tmp = conv2.split( " " );
						if ( tmp[ 1 ] === current ) {

							// If prev can be converted to accepted input
							conv = converters[ prev + " " + tmp[ 0 ] ] ||
							converters[ "* " + tmp[ 0 ] ];
							if ( conv ) {
								// Condense equivalence converters
								if ( conv === true ) {
									conv = converters[ conv2 ];

								// Otherwise, insert the intermediate dataType
							} else if ( converters[ conv2 ] !== true ) {
								current = tmp[ 0 ];
								dataTypes.unshift( tmp[ 1 ] );
							}
							break;
						}
					}
				}
			}

				// Apply converter (if not an equivalence)
				if ( conv !== true ) {

					// Unless errors are allowed to bubble, catch and return them
					if ( conv && s[ "throws" ] ) {
						response = conv( response );
					} else {
						try {
							response = conv( response );
						} catch ( e ) {
							return {
								state: "parsererror",
								error: conv ? e : "No conversion from " + prev + " to " + current
							};
						}
					}
				}
			}
		}
	}

	return { state: "success", data: response };
}

jQuery.extend({

	// Counter for holding the number of active queries
	active: 0,

	// Last-Modified header cache for next request
	lastModified: {},
	etag: {},

	ajaxSettings: {
		url: location.href,
		type: "GET",
		isLocal: rlocalProtocol.test( location.protocol ),
		global: true,
		processData: true,
		async: true,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		/*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		throws: false,
		traditional: false,
		headers: {},
		*/

		accepts: {
			"*": allTypes,
			text: "text/plain",
			html: "text/html",
			xml: "application/xml, text/xml",
			json: "application/json, text/javascript"
		},

		contents: {
			xml: /xml/,
			html: /html/,
			json: /json/
		},

		responseFields: {
			xml: "responseXML",
			text: "responseText",
			json: "responseJSON"
		},

		// Data converters
		// Keys separate source (or catchall "*") and destination types with a single space
		converters: {

			// Convert anything to text
			"* text": String,

			// Text to html (true = no transformation)
			"text html": true,

			// Evaluate text as a json expression
			"text json": jQuery.parseJSON,

			// Parse text as xml
			"text xml": jQuery.parseXML
		},

		// For options that shouldn't be deep extended:
		// you can add your own custom options here if
		// and when you create one that shouldn't be
		// deep extended (see ajaxExtend)
		flatOptions: {
			url: true,
			context: true
		}
	},

	// Creates a full fledged settings object into target
	// with both ajaxSettings and settings fields.
	// If target is omitted, writes into ajaxSettings.
	ajaxSetup: function( target, settings ) {
		return settings ?

			// Building a settings object
			ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

			// Extending ajaxSettings
			ajaxExtend( jQuery.ajaxSettings, target );
		},

		ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
		ajaxTransport: addToPrefiltersOrTransports( transports ),

	// Main method
	ajax: function( url, options ) {

		// If url is an object, simulate pre-1.5 signature
		if ( typeof url === "object" ) {
			options = url;
			url = undefined;
		}

		// Force options to be an object
		options = options || {};

		var transport,
			// URL without anti-cache param
			cacheURL,
			// Response headers
			responseHeadersString,
			responseHeaders,
			// timeout handle
			timeoutTimer,
			// Url cleanup var
			urlAnchor,
			// To know if global events are to be dispatched
			fireGlobals,
			// Loop variable
			i,
			// Create the final options object
			s = jQuery.ajaxSetup( {}, options ),
			// Callbacks context
			callbackContext = s.context || s,
			// Context for global events is callbackContext if it is a DOM node or jQuery collection
			globalEventContext = s.context &&
			( callbackContext.nodeType || callbackContext.jquery ) ?
			jQuery( callbackContext ) :
			jQuery.event,
			// Deferreds
			deferred = jQuery.Deferred(),
			completeDeferred = jQuery.Callbacks("once memory"),
			// Status-dependent callbacks
			statusCode = s.statusCode || {},
			// Headers (they are sent all at once)
			requestHeaders = {},
			requestHeadersNames = {},
			// The jqXHR state
			state = 0,
			// Default abort message
			strAbort = "canceled",
			// Fake xhr
			jqXHR = {
				readyState: 0,

				// Builds headers hashtable if needed
				getResponseHeader: function( key ) {
					var match;
					if ( state === 2 ) {
						if ( !responseHeaders ) {
							responseHeaders = {};
							while ( (match = rheaders.exec( responseHeadersString )) ) {
								responseHeaders[ match[1].toLowerCase() ] = match[ 2 ];
							}
						}
						match = responseHeaders[ key.toLowerCase() ];
					}
					return match == null ? null : match;
				},

				// Raw string
				getAllResponseHeaders: function() {
					return state === 2 ? responseHeadersString : null;
				},

				// Caches the header
				setRequestHeader: function( name, value ) {
					var lname = name.toLowerCase();
					if ( !state ) {
						name = requestHeadersNames[ lname ] = requestHeadersNames[ lname ] || name;
						requestHeaders[ name ] = value;
					}
					return this;
				},

				// Overrides response content-type header
				overrideMimeType: function( type ) {
					if ( !state ) {
						s.mimeType = type;
					}
					return this;
				},

				// Status-dependent callbacks
				statusCode: function( map ) {
					var code;
					if ( map ) {
						if ( state < 2 ) {
							for ( code in map ) {
								// Lazy-add the new callback in a way that preserves old ones
								statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
							}
						} else {
							// Execute the appropriate callbacks
							jqXHR.always( map[ jqXHR.status ] );
						}
					}
					return this;
				},

				// Cancel the request
				abort: function( statusText ) {
					var finalText = statusText || strAbort;
					if ( transport ) {
						transport.abort( finalText );
					}
					done( 0, finalText );
					return this;
				}
			};

		// Attach deferreds
		deferred.promise( jqXHR );

		// Remove hash character (#7531: and string promotion)
		// Add protocol if not provided (prefilters might expect it)
		// Handle falsy url in the settings object (#10093: consistency with old signature)
		// We also use the url parameter if available
		s.url = ( ( url || s.url || location.href ) + "" ).replace( rhash, "" )
		.replace( rprotocol, location.protocol + "//" );

		// Alias method option to type as per ticket #12004
		s.type = options.method || options.type || s.method || s.type;

		// Extract dataTypes list
		s.dataTypes = jQuery.trim( s.dataType || "*" ).toLowerCase().match( rnotwhite ) || [ "" ];

		// A cross-domain request is in order when the origin doesn't match the current origin.
		if ( s.crossDomain == null ) {
			urlAnchor = document.createElement( "a" );

			// Support: IE8-11+
			// IE throws exception if url is malformed, e.g. http://example.com:80x/
			try {
				urlAnchor.href = s.url;
				// Support: IE8-11+
				// Anchor's host property isn't correctly set when s.url is relative
				urlAnchor.href = urlAnchor.href;
				s.crossDomain = originAnchor.protocol + "//" + originAnchor.host !==
				urlAnchor.protocol + "//" + urlAnchor.host;
			} catch ( e ) {
				// If there is an error parsing the URL, assume it is crossDomain,
				// it can be rejected by the transport if it is invalid
				s.crossDomain = true;
			}
		}

		// Convert data if not already a string
		if ( s.data && s.processData && typeof s.data !== "string" ) {
			s.data = jQuery.param( s.data, s.traditional );
		}

		// Apply prefilters
		inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

		// If request was aborted inside a prefilter, stop there
		if ( state === 2 ) {
			return jqXHR;
		}

		// We can fire global events as of now if asked to
		// Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)
		fireGlobals = jQuery.event && s.global;

		// Watch for a new set of requests
		if ( fireGlobals && jQuery.active++ === 0 ) {
			jQuery.event.trigger("ajaxStart");
		}

		// Uppercase the type
		s.type = s.type.toUpperCase();

		// Determine if request has content
		s.hasContent = !rnoContent.test( s.type );

		// Save the URL in case we're toying with the If-Modified-Since
		// and/or If-None-Match header later on
		cacheURL = s.url;

		// More options handling for requests with no content
		if ( !s.hasContent ) {

			// If data is available, append data to url
			if ( s.data ) {
				cacheURL = ( s.url += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data );
				// #9682: remove data so that it's not used in an eventual retry
				delete s.data;
			}

			// Add anti-cache in url if needed
			if ( s.cache === false ) {
				s.url = rts.test( cacheURL ) ?

					// If there is already a '_' parameter, set its value
					cacheURL.replace( rts, "$1_=" + nonce++ ) :

					// Otherwise add one to the end
					cacheURL + ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + nonce++;
				}
			}

		// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
		if ( s.ifModified ) {
			if ( jQuery.lastModified[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
			}
			if ( jQuery.etag[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
			}
		}

		// Set the correct header, if data is being sent
		if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
			jqXHR.setRequestHeader( "Content-Type", s.contentType );
		}

		// Set the Accepts header for the server, depending on the dataType
		jqXHR.setRequestHeader(
			"Accept",
			s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[0] ] ?
			s.accepts[ s.dataTypes[0] ] +
			( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
			s.accepts[ "*" ]
			);

		// Check for headers option
		for ( i in s.headers ) {
			jqXHR.setRequestHeader( i, s.headers[ i ] );
		}

		// Allow custom headers/mimetypes and early abort
		if ( s.beforeSend &&
			( s.beforeSend.call( callbackContext, jqXHR, s ) === false || state === 2 ) ) {

			// Abort if not done already and return
		return jqXHR.abort();
	}

		// Aborting is no longer a cancellation
		strAbort = "abort";

		// Install callbacks on deferreds
		completeDeferred.add( s.complete );
		jqXHR.done( s.success );
		jqXHR.fail( s.error );

		// Get transport
		transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

		// If no transport, we auto-abort
		if ( !transport ) {
			done( -1, "No Transport" );
		} else {
			jqXHR.readyState = 1;

			// Send global event
			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
			}

			// If request was aborted inside ajaxSend, stop there
			if ( state === 2 ) {
				return jqXHR;
			}

			// Timeout
			if ( s.async && s.timeout > 0 ) {
				timeoutTimer = window.setTimeout(function() {
					jqXHR.abort("timeout");
				}, s.timeout );
			}

			try {
				state = 1;
				transport.send( requestHeaders, done );
			} catch ( e ) {
				// Propagate exception as error if not done
				if ( state < 2 ) {
					done( -1, e );
				// Simply rethrow otherwise
			} else {
				throw e;
			}
		}
	}

		// Callback for when everything is done
		function done( status, nativeStatusText, responses, headers ) {
			var isSuccess, success, error, response, modified,
			statusText = nativeStatusText;

			// Called once
			if ( state === 2 ) {
				return;
			}

			// State is "done" now
			state = 2;

			// Clear timeout if it exists
			if ( timeoutTimer ) {
				window.clearTimeout( timeoutTimer );
			}

			// Dereference transport for early garbage collection
			// (no matter how long the jqXHR object will be used)
			transport = undefined;

			// Cache response headers
			responseHeadersString = headers || "";

			// Set readyState
			jqXHR.readyState = status > 0 ? 4 : 0;

			// Determine if successful
			isSuccess = status >= 200 && status < 300 || status === 304;

			// Get response data
			if ( responses ) {
				response = ajaxHandleResponses( s, jqXHR, responses );
			}

			// Convert no matter what (that way responseXXX fields are always set)
			response = ajaxConvert( s, response, jqXHR, isSuccess );

			// If successful, handle type chaining
			if ( isSuccess ) {

				// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
				if ( s.ifModified ) {
					modified = jqXHR.getResponseHeader("Last-Modified");
					if ( modified ) {
						jQuery.lastModified[ cacheURL ] = modified;
					}
					modified = jqXHR.getResponseHeader("etag");
					if ( modified ) {
						jQuery.etag[ cacheURL ] = modified;
					}
				}

				// if no content
				if ( status === 204 || s.type === "HEAD" ) {
					statusText = "nocontent";

				// if not modified
			} else if ( status === 304 ) {
				statusText = "notmodified";

				// If we have data, let's convert it
			} else {
				statusText = response.state;
				success = response.data;
				error = response.error;
				isSuccess = !error;
			}
		} else {
				// Extract error from statusText and normalize for non-aborts
				error = statusText;
				if ( status || !statusText ) {
					statusText = "error";
					if ( status < 0 ) {
						status = 0;
					}
				}
			}

			// Set data for the fake xhr object
			jqXHR.status = status;
			jqXHR.statusText = ( nativeStatusText || statusText ) + "";

			// Success/Error
			if ( isSuccess ) {
				deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
			} else {
				deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
			}

			// Status-dependent callbacks
			jqXHR.statusCode( statusCode );
			statusCode = undefined;

			if ( fireGlobals ) {
				globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
					[ jqXHR, s, isSuccess ? success : error ] );
			}

			// Complete
			completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );
				// Handle the global AJAX counter
				if ( !( --jQuery.active ) ) {
					jQuery.event.trigger("ajaxStop");
				}
			}
		}

		return jqXHR;
	},

	getJSON: function( url, data, callback ) {
		return jQuery.get( url, data, callback, "json" );
	},

	getScript: function( url, callback ) {
		return jQuery.get( url, undefined, callback, "script" );
	}
});

jQuery.each( [ "get", "post" ], function( i, method ) {
	jQuery[ method ] = function( url, data, callback, type ) {
		// Shift arguments if data argument was omitted
		if ( jQuery.isFunction( data ) ) {
			type = type || callback;
			callback = data;
			data = undefined;
		}

		// The url can be an options object (which then must have .url)
		return jQuery.ajax( jQuery.extend({
			url: url,
			type: method,
			dataType: type,
			data: data,
			success: callback
		}, jQuery.isPlainObject( url ) && url ) );
	};
});


jQuery._evalUrl = function( url ) {
	return jQuery.ajax({
		url: url,

		// Make this explicit, since user can override this through ajaxSetup (#11264)
		type: "GET",
		dataType: "script",
		cache: true,
		async: false,
		global: false,
		"throws": true
	});
};


jQuery.fn.extend({
	wrapAll: function( html ) {
		var wrap;

		if ( this[ 0 ] ) {
			if ( jQuery.isFunction( html ) ) {
				html = html.call( this[ 0 ] );
			}

			// The elements to wrap the target around
			wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );

			if ( this[ 0 ].parentNode ) {
				wrap.insertBefore( this[ 0 ] );
			}

			wrap.map(function() {
				var elem = this;

				while ( elem.firstElementChild ) {
					elem = elem.firstElementChild;
				}

				return elem;
			}).append( this );
		}

		return this;
	},

	wrapInner: function( html ) {
		if ( jQuery.isFunction( html ) ) {
			return this.each(function( i ) {
				jQuery( this ).wrapInner( html.call(this, i) );
			});
		}

		return this.each(function() {
			var self = jQuery( this ),
			contents = self.contents();

			if ( contents.length ) {
				contents.wrapAll( html );

			} else {
				self.append( html );
			}
		});
	},

	wrap: function( html ) {
		var isFunction = jQuery.isFunction( html );

		return this.each(function( i ) {
			jQuery( this ).wrapAll( isFunction ? html.call(this, i) : html );
		});
	},

	unwrap: function( selector ) {
		this.parent( selector ).not( "body" ).each(function() {
			jQuery( this ).replaceWith( this.childNodes );
		});
		return this;
	}
});


jQuery.expr.filters.hidden = function( elem ) {
	return !jQuery.expr.filters.visible( elem );
};
jQuery.expr.filters.visible = function( elem ) {
	return !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length );
};




var r20 = /%20/g,
rbracket = /\[\]$/,
rCRLF = /\r?\n/g,
rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
rsubmittable = /^(?:input|select|textarea|keygen)/i;

function buildParams( prefix, obj, traditional, add ) {
	var name;

	if ( jQuery.isArray( obj ) ) {
		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if ( traditional || rbracket.test( prefix ) ) {
				// Treat each array item as a scalar.
				add( prefix, v );

			} else {
				// Item is non-scalar (array or object), encode its numeric index.
				buildParams(
					prefix + "[" + ( typeof v === "object" ? i : "" ) + "]",
					v,
					traditional,
					add
					);
			}
		});

	} else if ( !traditional && jQuery.type( obj ) === "object" ) {
		// Serialize object item.
		for ( name in obj ) {
			buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
		}

	} else {
		// Serialize scalar item.
		add( prefix, obj );
	}
}

// Serialize an array of form elements or a set of
// key/values into a query string
jQuery.param = function( a, traditional ) {
	var prefix,
	s = [],
	add = function( key, value ) {
			// If value is a function, invoke it and return its value
			value = jQuery.isFunction( value ) ? value() : ( value == null ? "" : value );
			s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
		};

	// Set traditional to true for jQuery <= 1.3.2 behavior.
	if ( traditional === undefined ) {
		traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
	}

	// If an array was passed in, assume that it is an array of form elements.
	if ( jQuery.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {
		// Serialize the form elements
		jQuery.each( a, function() {
			add( this.name, this.value );
		});

	} else {
		// If traditional, encode the "old" way (the way 1.3.2 or older
		// did it), otherwise encode params recursively.
for ( prefix in a ) {
	buildParams( prefix, a[ prefix ], traditional, add );
}
}

	// Return the resulting serialization
	return s.join( "&" ).replace( r20, "+" );
};

jQuery.fn.extend({
	serialize: function() {
		return jQuery.param( this.serializeArray() );
	},
	serializeArray: function() {
		return this.map(function() {
			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		})
		.filter(function() {
			var type = this.type;

			// Use .is( ":disabled" ) so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
			rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
			( this.checked || !rcheckableType.test( type ) );
		})
		.map(function( i, elem ) {
			var val = jQuery( this ).val();

			return val == null ?
			null :
			jQuery.isArray( val ) ?
			jQuery.map( val, function( val ) {
				return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
			}) :
			{ name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
		}).get();
	}
});


jQuery.ajaxSettings.xhr = function() {
	try {
		return new window.XMLHttpRequest();
	} catch ( e ) {}
};

var xhrSuccessStatus = {
		// file protocol always yields status code 0, assume 200
		0: 200,
		// Support: IE9
		// #1450: sometimes IE returns 1223 when it should be 204
		1223: 204
	},
	xhrSupported = jQuery.ajaxSettings.xhr();

	support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
	support.ajax = xhrSupported = !!xhrSupported;

	jQuery.ajaxTransport(function( options ) {
		var callback;

	// Cross domain only allowed if supported through XMLHttpRequest
	if ( support.cors || xhrSupported && !options.crossDomain ) {
		return {
			send: function( headers, complete ) {
				var i,
				xhr = options.xhr();

				xhr.open(
					options.type,
					options.url,
					options.async,
					options.username,
					options.password
					);

				// Apply custom fields if provided
				if ( options.xhrFields ) {
					for ( i in options.xhrFields ) {
						xhr[ i ] = options.xhrFields[ i ];
					}
				}

				// Override mime type if needed
				if ( options.mimeType && xhr.overrideMimeType ) {
					xhr.overrideMimeType( options.mimeType );
				}

				// X-Requested-With header
				// For cross-domain requests, seeing as conditions for a preflight are
				// akin to a jigsaw puzzle, we simply never set it to be sure.
				// (it can always be set on a per-request basis or even using ajaxSetup)
				// For same-domain requests, won't change header if already provided.
				if ( !options.crossDomain && !headers["X-Requested-With"] ) {
					headers["X-Requested-With"] = "XMLHttpRequest";
				}

				// Set headers
				for ( i in headers ) {
					xhr.setRequestHeader( i, headers[ i ] );
				}

				// Callback
				callback = function( type ) {
					return function() {
						if ( callback ) {
							callback = xhr.onload = xhr.onerror = null;

							if ( type === "abort" ) {
								xhr.abort();
							} else if ( type === "error" ) {
								complete(
									// file: protocol always yields status 0; see #8605, #14207
									xhr.status,
									xhr.statusText
									);
							} else {
								complete(
									xhrSuccessStatus[ xhr.status ] || xhr.status,
									xhr.statusText,
									// Support: IE9
									// Accessing binary-data responseText throws an exception
									// (#11426)
									typeof xhr.responseText === "string" ? {
										text: xhr.responseText
									} : undefined,
									xhr.getAllResponseHeaders()
									);
							}
						}
					};
				};

				// Listen to events
				xhr.onload = callback();
				xhr.onerror = callback("error");

				// Create the abort callback
				callback = callback("abort");

				try {
					// Do send the request (this may raise an exception)
					xhr.send( options.hasContent && options.data || null );
				} catch ( e ) {
					// #14683: Only rethrow if this hasn't been notified as an error yet
					if ( callback ) {
						throw e;
					}
				}
			},

			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
});




// Install script dataType
jQuery.ajaxSetup({
	accepts: {
		script: "text/javascript, application/javascript, " +
		"application/ecmascript, application/x-ecmascript"
	},
	contents: {
		script: /(?:java|ecma)script/
	},
	converters: {
		"text script": function( text ) {
			jQuery.globalEval( text );
			return text;
		}
	}
});

// Handle cache's special case and crossDomain
jQuery.ajaxPrefilter( "script", function( s ) {
	if ( s.cache === undefined ) {
		s.cache = false;
	}
	if ( s.crossDomain ) {
		s.type = "GET";
	}
});

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function( s ) {
	// This transport only deals with cross domain requests
	if ( s.crossDomain ) {
		var script, callback;
		return {
			send: function( _, complete ) {
				script = jQuery("<script>").prop({
					charset: s.scriptCharset,
					src: s.url
				}).on(
				"load error",
				callback = function( evt ) {
					script.remove();
					callback = null;
					if ( evt ) {
						complete( evt.type === "error" ? 404 : 200, evt.type );
					}
				}
				);

				// Use native DOM manipulation to avoid our domManip AJAX trickery
				document.head.appendChild( script[ 0 ] );
			},
			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
});




var oldCallbacks = [],
rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup({
	jsonp: "callback",
	jsonpCallback: function() {
		var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce++ ) );
		this[ callback ] = true;
		return callback;
	}
});

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

	var callbackName, overwritten, responseContainer,
	jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
		"url" :
		typeof s.data === "string" &&
		( s.contentType || "" )
		.indexOf("application/x-www-form-urlencoded") === 0 &&
		rjsonp.test( s.data ) && "data"
		);

	// Handle iff the expected data type is "jsonp" or we have a parameter to set
	if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

		// Get callback name, remembering preexisting value associated with it
		callbackName = s.jsonpCallback = jQuery.isFunction( s.jsonpCallback ) ?
		s.jsonpCallback() :
		s.jsonpCallback;

		// Insert callback into url or form data
		if ( jsonProp ) {
			s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
		} else if ( s.jsonp !== false ) {
			s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
		}

		// Use data converter to retrieve json after script execution
		s.converters["script json"] = function() {
			if ( !responseContainer ) {
				jQuery.error( callbackName + " was not called" );
			}
			return responseContainer[ 0 ];
		};

		// force json dataType
		s.dataTypes[ 0 ] = "json";

		// Install callback
		overwritten = window[ callbackName ];
		window[ callbackName ] = function() {
			responseContainer = arguments;
		};

		// Clean-up function (fires after converters)
		jqXHR.always(function() {
			// If previous value didn't exist - remove it
			if ( overwritten === undefined ) {
				jQuery( window ).removeProp( callbackName );

			// Otherwise restore preexisting value
		} else {
			window[ callbackName ] = overwritten;
		}

			// Save back as free
			if ( s[ callbackName ] ) {
				// make sure that re-using the options doesn't screw things around
				s.jsonpCallback = originalSettings.jsonpCallback;

				// save the callback name for future use
				oldCallbacks.push( callbackName );
			}

			// Call if it was a function and we have a response
			if ( responseContainer && jQuery.isFunction( overwritten ) ) {
				overwritten( responseContainer[ 0 ] );
			}

			responseContainer = overwritten = undefined;
		});

		// Delegate to script
		return "script";
	}
});




support.createHTMLDocument = (function() {
	var body = document.implementation.createHTMLDocument( "" ).body;
	body.innerHTML = "<form></form><form></form>";
	return body.childNodes.length === 2;
})();


// data: string of html
// context (optional): If specified, the fragment will be created in this context,
// defaults to document
// keepScripts (optional): If true, will include scripts passed in the html string
jQuery.parseHTML = function( data, context, keepScripts ) {
	if ( typeof data !== "string" ) {
		return [];
	}
	if ( typeof context === "boolean" ) {
		keepScripts = context;
		context = false;
	}
	// document.implementation stops scripts or inline event handlers from
	// being executed immediately
	context = context || ( support.createHTMLDocument ?
		document.implementation.createHTMLDocument( "" ) :
		document );

	var parsed = rsingleTag.exec( data ),
	scripts = !keepScripts && [];

	// Single tag
	if ( parsed ) {
		return [ context.createElement( parsed[1] ) ];
	}

	parsed = buildFragment( [ data ], context, scripts );

	if ( scripts && scripts.length ) {
		jQuery( scripts ).remove();
	}

	return jQuery.merge( [], parsed.childNodes );
};


/**
 * Load a url into a page
 */
 jQuery.fn.load = function( url, params, callback ) {
 	var selector, type, response,
 	self = this,
 	off = url.indexOf(" ");

 	if ( off > -1 ) {
 		selector = jQuery.trim( url.slice( off ) );
 		url = url.slice( 0, off );
 	}

	// If it's a function
	if ( jQuery.isFunction( params ) ) {

		// We assume that it's the callback
		callback = params;
		params = undefined;

	// Otherwise, build a param string
} else if ( params && typeof params === "object" ) {
	type = "POST";
}

	// If we have elements to modify, make the request
	if ( self.length > 0 ) {
		jQuery.ajax({
			url: url,

			// If "type" variable is undefined, then "GET" method will be used.
			// Make value of this field explicit since
			// user can override it through ajaxSetup method
			type: type || "GET",
			dataType: "html",
			data: params
		}).done(function( responseText ) {

			// Save response for use in complete callback
			response = arguments;

			self.html( selector ?

				// If a selector was specified, locate the right elements in a dummy div
				// Exclude scripts to avoid IE 'Permission Denied' errors
				jQuery("<div>").append( jQuery.parseHTML( responseText ) ).find( selector ) :

				// Otherwise use the full result
				responseText );

		// If the request succeeds, this function gets "data", "status", "jqXHR"
		// but they are ignored because response was set above.
		// If it fails, this function gets "jqXHR", "status", "error"
	}).always( callback && function( jqXHR, status ) {
		self.each( function() {
			callback.apply( self, response || [ jqXHR.responseText, status, jqXHR ] );
		});
	});
}

return this;
};




// Attach a bunch of functions for handling common AJAX events
jQuery.each([
	"ajaxStart",
	"ajaxStop",
	"ajaxComplete",
	"ajaxError",
	"ajaxSuccess",
	"ajaxSend"
	], function( i, type ) {
		jQuery.fn[ type ] = function( fn ) {
			return this.on( type, fn );
		};
	});




jQuery.expr.filters.animated = function( elem ) {
	return jQuery.grep(jQuery.timers, function( fn ) {
		return elem === fn.elem;
	}).length;
};




/**
 * Gets a window from an element
 */
 function getWindow( elem ) {
 	return jQuery.isWindow( elem ) ? elem : elem.nodeType === 9 && elem.defaultView;
 }

 jQuery.offset = {
 	setOffset: function( elem, options, i ) {
 		var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
 		position = jQuery.css( elem, "position" ),
 		curElem = jQuery( elem ),
 		props = {};

		// Set position first, in-case top/left are set even on static elem
		if ( position === "static" ) {
			elem.style.position = "relative";
		}

		curOffset = curElem.offset();
		curCSSTop = jQuery.css( elem, "top" );
		curCSSLeft = jQuery.css( elem, "left" );
		calculatePosition = ( position === "absolute" || position === "fixed" ) &&
		( curCSSTop + curCSSLeft ).indexOf("auto") > -1;

		// Need to be able to calculate position if either
		// top or left is auto and position is either absolute or fixed
		if ( calculatePosition ) {
			curPosition = curElem.position();
			curTop = curPosition.top;
			curLeft = curPosition.left;

		} else {
			curTop = parseFloat( curCSSTop ) || 0;
			curLeft = parseFloat( curCSSLeft ) || 0;
		}

		if ( jQuery.isFunction( options ) ) {

			// Use jQuery.extend here to allow modification of coordinates argument (gh-1848)
			options = options.call( elem, i, jQuery.extend( {}, curOffset ) );
		}

		if ( options.top != null ) {
			props.top = ( options.top - curOffset.top ) + curTop;
		}
		if ( options.left != null ) {
			props.left = ( options.left - curOffset.left ) + curLeft;
		}

		if ( "using" in options ) {
			options.using.call( elem, props );

		} else {
			curElem.css( props );
		}
	}
};

jQuery.fn.extend({
	offset: function( options ) {
		// Preserve chaining for setter
		if ( arguments.length ) {
			return options === undefined ?
			this :
			this.each(function( i ) {
				jQuery.offset.setOffset( this, options, i );
			});
		}

		var docElem, win, rect, doc,
		elem = this[ 0 ];

		if ( !elem ) {
			return;
		}

		// Support: IE<=11+
		// Running getBoundingClientRect on a
		// disconnected node in IE throws an error
		if ( !elem.getClientRects().length ) {
			return { top: 0, left: 0 };
		}

		rect = elem.getBoundingClientRect();

		// Make sure element is not hidden (display: none)
		if ( rect.width || rect.height ) {
			doc = elem.ownerDocument;
			win = getWindow( doc );
			docElem = doc.documentElement;

			return {
				top: rect.top + win.pageYOffset - docElem.clientTop,
				left: rect.left + win.pageXOffset - docElem.clientLeft
			};
		}

		// Return zeros for disconnected and hidden elements (gh-2310)
		return rect;
	},

	position: function() {
		if ( !this[ 0 ] ) {
			return;
		}

		var offsetParent, offset,
		elem = this[ 0 ],
		parentOffset = { top: 0, left: 0 };

		// Fixed elements are offset from window (parentOffset = {top:0, left: 0},
		// because it is its only offset parent
		if ( jQuery.css( elem, "position" ) === "fixed" ) {
			// Assume getBoundingClientRect is there when computed position is fixed
			offset = elem.getBoundingClientRect();

		} else {
			// Get *real* offsetParent
			offsetParent = this.offsetParent();

			// Get correct offsets
			offset = this.offset();
			if ( !jQuery.nodeName( offsetParent[ 0 ], "html" ) ) {
				parentOffset = offsetParent.offset();
			}

			// Add offsetParent borders
			// Subtract offsetParent scroll positions
			parentOffset.top += jQuery.css( offsetParent[ 0 ], "borderTopWidth", true ) -
			offsetParent.scrollTop();
			parentOffset.left += jQuery.css( offsetParent[ 0 ], "borderLeftWidth", true ) -
			offsetParent.scrollLeft();
		}

		// Subtract parent offsets and element margins
		return {
			top: offset.top - parentOffset.top - jQuery.css( elem, "marginTop", true ),
			left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true )
		};
	},

	// This method will return documentElement in the following cases:
	// 1) For the element inside the iframe without offsetParent, this method will return
	//    documentElement of the parent window
	// 2) For the hidden or detached element
	// 3) For body or html element, i.e. in case of the html node - it will return itself
	//
	// but those exceptions were never presented as a real life use-cases
	// and might be considered as more preferable results.
	//
	// This logic, however, is not guaranteed and can change at any point in the future
	offsetParent: function() {
		return this.map(function() {
			var offsetParent = this.offsetParent;

			while ( offsetParent && jQuery.css( offsetParent, "position" ) === "static" ) {
				offsetParent = offsetParent.offsetParent;
			}

			return offsetParent || documentElement;
		});
	}
});

// Create scrollLeft and scrollTop methods
jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
	var top = "pageYOffset" === prop;

	jQuery.fn[ method ] = function( val ) {
		return access( this, function( elem, method, val ) {
			var win = getWindow( elem );

			if ( val === undefined ) {
				return win ? win[ prop ] : elem[ method ];
			}

			if ( win ) {
				win.scrollTo(
					!top ? val : win.pageXOffset,
					top ? val : win.pageYOffset
					);

			} else {
				elem[ method ] = val;
			}
		}, method, val, arguments.length, null );
	};
});

// Support: Safari<7+, Chrome<37+
// Add the top/left cssHooks using jQuery.fn.position
// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
// Blink bug: https://code.google.com/p/chromium/issues/detail?id=229280
// getComputedStyle returns percent when specified for top/left/bottom/right;
// rather than make the css module depend on the offset module, just check for it here
jQuery.each( [ "top", "left" ], function( i, prop ) {
	jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
		function( elem, computed ) {
			if ( computed ) {
				computed = curCSS( elem, prop );
				// If curCSS returns percentage, fallback to offset
				return rnumnonpx.test( computed ) ?
				jQuery( elem ).position()[ prop ] + "px" :
				computed;
			}
		}
		);
});


// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
	jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name },
		function( defaultExtra, funcName ) {

		// Margin is only for outerHeight, outerWidth
		jQuery.fn[ funcName ] = function( margin, value ) {
			var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
			extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

			return access( this, function( elem, type, value ) {
				var doc;

				if ( jQuery.isWindow( elem ) ) {
					// As of 5/8/2012 this will yield incorrect results for Mobile Safari, but there
					// isn't a whole lot we can do. See pull request at this URL for discussion:
					// https://github.com/jquery/jquery/pull/764
					return elem.document.documentElement[ "client" + name ];
				}

				// Get document width or height
				if ( elem.nodeType === 9 ) {
					doc = elem.documentElement;

					// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
					// whichever is greatest
					return Math.max(
						elem.body[ "scroll" + name ], doc[ "scroll" + name ],
						elem.body[ "offset" + name ], doc[ "offset" + name ],
						doc[ "client" + name ]
						);
				}

				return value === undefined ?
					// Get width or height on the element, requesting but not forcing parseFloat
					jQuery.css( elem, type, extra ) :

					// Set width or height on the element
					jQuery.style( elem, type, value, extra );
				}, type, chainable ? margin : undefined, chainable, null );
};
});
});




// Register as a named AMD module, since jQuery can be concatenated with other
// files that may use define, but not via a proper concatenation script that
// understands anonymous AMD modules. A named AMD is safest and most robust
// way to register. Lowercase jquery is used because AMD module names are
// derived from file names, and jQuery is normally delivered in a lowercase
// file name. Do this after creating the global so that if an AMD module wants
// to call noConflict to hide this version of jQuery, it will work.

// Note that for maximum portability, libraries that are not jQuery should
// declare themselves as anonymous modules, and avoid setting a global if an
// AMD loader is present. jQuery is a special case. For more information, see
// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon

if ( typeof define === "function" && define.amd ) {
	define( "jquery", [], function() {
		return jQuery;
	});
}



var
	// Map over jQuery in case of overwrite
	_jQuery = window.jQuery,

	// Map over the $ in case of overwrite
	_$ = window.$;

	jQuery.noConflict = function( deep ) {
		if ( window.$ === jQuery ) {
			window.$ = _$;
		}

		if ( deep && window.jQuery === jQuery ) {
			window.jQuery = _jQuery;
		}

		return jQuery;
	};

// Expose jQuery and $ identifiers, even in AMD
// (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
// and CommonJS for browser emulators (#13566)
if ( !noGlobal ) {
	window.jQuery = window.$ = jQuery;
}

return jQuery;
}));

/*!
 * Modernizr v2.8.3
 * www.modernizr.com
 *
 * Copyright (c) Faruk Ates, Paul Irish, Alex Sexton
 * Available under the BSD and MIT licenses: www.modernizr.com/license/
 */

/*
 * Modernizr tests which native CSS3 and HTML5 features are available in
 * the current UA and makes the results available to you in two ways:
 * as properties on a global Modernizr object, and as classes on the
 * <html> element. This information allows you to progressively enhance
 * your pages with a granular level of control over the experience.
 *
 * Modernizr has an optional (not included) conditional resource loader
 * called Modernizr.load(), based on Yepnope.js (yepnopejs.com).
 * To get a build that includes Modernizr.load(), as well as choosing
 * which tests to include, go to www.modernizr.com/download/
 *
 * Authors        Faruk Ates, Paul Irish, Alex Sexton
 * Contributors   Ryan Seddon, Ben Alman
 */

 window.Modernizr = (function( window, document, undefined ) {

 	var version = '2.8.3',

 	Modernizr = {},

 	/*>>cssclasses*/
    // option for enabling the HTML classes to be added
    enableClasses = true,
    /*>>cssclasses*/

    docElement = document.documentElement,

    /**
     * Create our "modernizr" element that we do most feature tests on.
     */
     mod = 'modernizr',
     modElem = document.createElement(mod),
     mStyle = modElem.style,

    /**
     * Create the input element for various Web Forms feature tests.
     */
     inputElem /*>>inputelem*/ = document.createElement('input') /*>>inputelem*/ ,

     /*>>smile*/
     smile = ':)',
/*>>smile*/

toString = {}.toString,

    // TODO :: make the prefixes more granular
    /*>>prefixes*/
    // List of property values to set for css tests. See ticket #21
    prefixes = ' -webkit- -moz- -o- -ms- '.split(' '),
    /*>>prefixes*/

    /*>>domprefixes*/
    // Following spec is to expose vendor-specific style properties as:
    //   elem.style.WebkitBorderRadius
    // and the following would be incorrect:
    //   elem.style.webkitBorderRadius

    // Webkit ghosts their properties in lowercase but Opera & Moz do not.
    // Microsoft uses a lowercase `ms` instead of the correct `Ms` in IE8+
    //   erik.eae.net/archives/2008/03/10/21.48.10/

    // More here: github.com/Modernizr/Modernizr/issues/issue/21
    omPrefixes = 'Webkit Moz O ms',

    cssomPrefixes = omPrefixes.split(' '),

    domPrefixes = omPrefixes.toLowerCase().split(' '),
    /*>>domprefixes*/

    /*>>ns*/
    ns = {'svg': 'http://www.w3.org/2000/svg'},
    /*>>ns*/

    tests = {},
    inputs = {},
    attrs = {},

    classes = [],

    slice = classes.slice,

    featureName, // used in testing loop


    /*>>teststyles*/
    // Inject element with style element and some CSS rules
    injectElementWithStyles = function( rule, callback, nodes, testnames ) {

    	var style, ret, node, docOverflow,
    	div = document.createElement('div'),
          // After page load injecting a fake body doesn't work so check if body exists
          body = document.body,
          // IE6 and 7 won't return offsetWidth or offsetHeight unless it's in the body element, so we fake it.
          fakeBody = body || document.createElement('body');

          if ( parseInt(nodes, 10) ) {
          // In order not to give false positives we create a node for each test
          // This also allows the method to scale for unspecified uses
          while ( nodes-- ) {
          	node = document.createElement('div');
          	node.id = testnames ? testnames[nodes] : mod + (nodes + 1);
          	div.appendChild(node);
          }
      }

      // <style> elements in IE6-9 are considered 'NoScope' elements and therefore will be removed
      // when injected with innerHTML. To get around this you need to prepend the 'NoScope' element
      // with a 'scoped' element, in our case the soft-hyphen entity as it won't mess with our measurements.
      // msdn.microsoft.com/en-us/library/ms533897%28VS.85%29.aspx
      // Documents served as xml will throw if using &shy; so use xml friendly encoded version. See issue #277
      style = ['&#173;','<style id="s', mod, '">', rule, '</style>'].join('');
      div.id = mod;
      // IE6 will false positive on some tests due to the style element inside the test div somehow interfering offsetHeight, so insert it into body or fakebody.
      // Opera will act all quirky when injecting elements in documentElement when page is served as xml, needs fakebody too. #270
      (body ? div : fakeBody).innerHTML += style;
      fakeBody.appendChild(div);
      if ( !body ) {
          //avoid crashing IE8, if background image is used
          fakeBody.style.background = '';
          //Safari 5.13/5.1.4 OSX stops loading if ::-webkit-scrollbar is used and scrollbars are visible
          fakeBody.style.overflow = 'hidden';
          docOverflow = docElement.style.overflow;
          docElement.style.overflow = 'hidden';
          docElement.appendChild(fakeBody);
      }

      ret = callback(div, rule);
      // If this is done after page load we don't want to remove the body so check if body exists
      if ( !body ) {
      	fakeBody.parentNode.removeChild(fakeBody);
      	docElement.style.overflow = docOverflow;
      } else {
      	div.parentNode.removeChild(div);
      }

      return !!ret;

  },
  /*>>teststyles*/

  /*>>mq*/
    // adapted from matchMedia polyfill
    // by Scott Jehl and Paul Irish
    // gist.github.com/786768
    testMediaQuery = function( mq ) {

    	var matchMedia = window.matchMedia || window.msMatchMedia;
    	if ( matchMedia ) {
    		return matchMedia(mq) && matchMedia(mq).matches || false;
    	}

    	var bool;

    	injectElementWithStyles('@media ' + mq + ' { #' + mod + ' { position: absolute; } }', function( node ) {
    		bool = (window.getComputedStyle ?
    			getComputedStyle(node, null) :
    			node.currentStyle)['position'] == 'absolute';
    	});

    	return bool;

    },
    /*>>mq*/


    /*>>hasevent*/
    //
    // isEventSupported determines if a given element supports the given event
    // kangax.github.com/iseventsupported/
    //
    // The following results are known incorrects:
    //   Modernizr.hasEvent("webkitTransitionEnd", elem) // false negative
    //   Modernizr.hasEvent("textInput") // in Webkit. github.com/Modernizr/Modernizr/issues/333
    //   ...
    isEventSupported = (function() {

    	var TAGNAMES = {
    		'select': 'input', 'change': 'input',
    		'submit': 'form', 'reset': 'form',
    		'error': 'img', 'load': 'img', 'abort': 'img'
    	};

    	function isEventSupported( eventName, element ) {

    		element = element || document.createElement(TAGNAMES[eventName] || 'div');
    		eventName = 'on' + eventName;

        // When using `setAttribute`, IE skips "unload", WebKit skips "unload" and "resize", whereas `in` "catches" those
        var isSupported = eventName in element;

        if ( !isSupported ) {
          // If it has no `setAttribute` (i.e. doesn't implement Node interface), try generic element
          if ( !element.setAttribute ) {
          	element = document.createElement('div');
          }
          if ( element.setAttribute && element.removeAttribute ) {
          	element.setAttribute(eventName, '');
          	isSupported = is(element[eventName], 'function');

            // If property was created, "remove it" (by setting value to `undefined`)
            if ( !is(element[eventName], 'undefined') ) {
            	element[eventName] = undefined;
            }
            element.removeAttribute(eventName);
        }
    }

    element = null;
    return isSupported;
}
return isEventSupported;
})(),
/*>>hasevent*/

    // TODO :: Add flag for hasownprop ? didn't last time

    // hasOwnProperty shim by kangax needed for Safari 2.0 support
    _hasOwnProperty = ({}).hasOwnProperty, hasOwnProp;

    if ( !is(_hasOwnProperty, 'undefined') && !is(_hasOwnProperty.call, 'undefined') ) {
    	hasOwnProp = function (object, property) {
    		return _hasOwnProperty.call(object, property);
    	};
    }
    else {
    	hasOwnProp = function (object, property) { /* yes, this can give false positives/negatives, but most of the time we don't care about those */
    	return ((property in object) && is(object.constructor.prototype[property], 'undefined'));
    };
}

    // Adapted from ES5-shim https://github.com/kriskowal/es5-shim/blob/master/es5-shim.js
    // es5.github.com/#x15.3.4.5

    if (!Function.prototype.bind) {
    	Function.prototype.bind = function bind(that) {

    		var target = this;

    		if (typeof target != "function") {
    			throw new TypeError();
    		}

    		var args = slice.call(arguments, 1),
    		bound = function () {

    			if (this instanceof bound) {

    				var F = function(){};
    				F.prototype = target.prototype;
    				var self = new F();

    				var result = target.apply(
    					self,
    					args.concat(slice.call(arguments))
    					);
    				if (Object(result) === result) {
    					return result;
    				}
    				return self;

    			} else {

    				return target.apply(
    					that,
    					args.concat(slice.call(arguments))
    					);

    			}

    		};

    		return bound;
    	};
    }

    /**
     * setCss applies given styles to the Modernizr DOM node.
     */
     function setCss( str ) {
     	mStyle.cssText = str;
     }

    /**
     * setCssAll extrapolates all vendor-specific css strings.
     */
     function setCssAll( str1, str2 ) {
     	return setCss(prefixes.join(str1 + ';') + ( str2 || '' ));
     }

    /**
     * is returns a boolean for if typeof obj is exactly type.
     */
     function is( obj, type ) {
     	return typeof obj === type;
     }

    /**
     * contains returns a boolean for if substr is found within str.
     */
     function contains( str, substr ) {
     	return !!~('' + str).indexOf(substr);
     }

     /*>>testprop*/

    // testProps is a generic CSS / DOM property test.

    // In testing support for a given CSS property, it's legit to test:
    //    `elem.style[styleName] !== undefined`
    // If the property is supported it will return an empty string,
    // if unsupported it will return undefined.

    // We'll take advantage of this quick test and skip setting a style
    // on our modernizr element, but instead just testing undefined vs
    // empty string.

    // Because the testing of the CSS property names (with "-", as
    // opposed to the camelCase DOM properties) is non-portable and
    // non-standard but works in WebKit and IE (but not Gecko or Opera),
    // we explicitly reject properties with dashes so that authors
    // developing in WebKit or IE first don't end up with
    // browser-specific content by accident.

    function testProps( props, prefixed ) {
    	for ( var i in props ) {
    		var prop = props[i];
    		if ( !contains(prop, "-") && mStyle[prop] !== undefined ) {
    			return prefixed == 'pfx' ? prop : true;
    		}
    	}
    	return false;
    }
    /*>>testprop*/

    // TODO :: add testDOMProps
    /**
     * testDOMProps is a generic DOM property test; if a browser supports
     *   a certain property, it won't return undefined for it.
     */
     function testDOMProps( props, obj, elem ) {
     	for ( var i in props ) {
     		var item = obj[props[i]];
     		if ( item !== undefined) {

                // return the property name as a string
                if (elem === false) return props[i];

                // let's bind a function
                if (is(item, 'function')){
                  // default to autobind unless override
                  return item.bind(elem || obj);
              }

                // return the unbound function or obj or value
                return item;
            }
        }
        return false;
    }

    /*>>testallprops*/
    /**
     * testPropsAll tests a list of DOM properties we want to check against.
     *   We specify literally ALL possible (known and/or likely) properties on
     *   the element including the non-vendor prefixed one, for forward-
     *   compatibility.
     */
     function testPropsAll( prop, prefixed, elem ) {

     	var ucProp  = prop.charAt(0).toUpperCase() + prop.slice(1),
     	props   = (prop + ' ' + cssomPrefixes.join(ucProp + ' ') + ucProp).split(' ');

        // did they call .prefixed('boxSizing') or are we just testing a prop?
        if(is(prefixed, "string") || is(prefixed, "undefined")) {
        	return testProps(props, prefixed);

        // otherwise, they called .prefixed('requestAnimationFrame', window[, elem])
    } else {
    	props = (prop + ' ' + (domPrefixes).join(ucProp + ' ') + ucProp).split(' ');
    	return testDOMProps(props, prefixed, elem);
    }
}
/*>>testallprops*/


    /**
     * Tests
     * -----
     */

    // The *new* flexbox
    // dev.w3.org/csswg/css3-flexbox

    tests['flexbox'] = function() {
    	return testPropsAll('flexWrap');
    };

    // The *old* flexbox
    // www.w3.org/TR/2009/WD-css3-flexbox-20090723/

    tests['flexboxlegacy'] = function() {
    	return testPropsAll('boxDirection');
    };

    // On the S60 and BB Storm, getContext exists, but always returns undefined
    // so we actually have to call getContext() to verify
    // github.com/Modernizr/Modernizr/issues/issue/97/

    tests['canvas'] = function() {
    	var elem = document.createElement('canvas');
    	return !!(elem.getContext && elem.getContext('2d'));
    };

    tests['canvastext'] = function() {
    	return !!(Modernizr['canvas'] && is(document.createElement('canvas').getContext('2d').fillText, 'function'));
    };

    // webk.it/70117 is tracking a legit WebGL feature detect proposal

    // We do a soft detect which may false positive in order to avoid
    // an expensive context creation: bugzil.la/732441

    tests['webgl'] = function() {
    	return !!window.WebGLRenderingContext;
    };

    /*
     * The Modernizr.touch test only indicates if the browser supports
     *    touch events, which does not necessarily reflect a touchscreen
     *    device, as evidenced by tablets running Windows 7 or, alas,
     *    the Palm Pre / WebOS (touch) phones.
     *
     * Additionally, Chrome (desktop) used to lie about its support on this,
     *    but that has since been rectified: crbug.com/36415
     *
     * We also test for Firefox 4 Multitouch Support.
     *
     * For more info, see: modernizr.github.com/Modernizr/touch.html
     */

     tests['touch'] = function() {
     	var bool;

     	if(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
     		bool = true;
     	} else {
     		injectElementWithStyles(['@media (',prefixes.join('touch-enabled),('),mod,')','{#modernizr{top:9px;position:absolute}}'].join(''), function( node ) {
     			bool = node.offsetTop === 9;
     		});
     	}

     	return bool;
     };


    // geolocation is often considered a trivial feature detect...
    // Turns out, it's quite tricky to get right:
    //
    // Using !!navigator.geolocation does two things we don't want. It:
    //   1. Leaks memory in IE9: github.com/Modernizr/Modernizr/issues/513
    //   2. Disables page caching in WebKit: webk.it/43956
    //
    // Meanwhile, in Firefox < 8, an about:config setting could expose
    // a false positive that would throw an exception: bugzil.la/688158

    tests['geolocation'] = function() {
    	return 'geolocation' in navigator;
    };


    tests['postmessage'] = function() {
    	return !!window.postMessage;
    };


    // Chrome incognito mode used to throw an exception when using openDatabase
    // It doesn't anymore.
    tests['websqldatabase'] = function() {
    	return !!window.openDatabase;
    };

    // Vendors had inconsistent prefixing with the experimental Indexed DB:
    // - Webkit's implementation is accessible through webkitIndexedDB
    // - Firefox shipped moz_indexedDB before FF4b9, but since then has been mozIndexedDB
    // For speed, we don't test the legacy (and beta-only) indexedDB
    tests['indexedDB'] = function() {
    	return !!testPropsAll("indexedDB", window);
    };

    // documentMode logic from YUI to filter out IE8 Compat Mode
    //   which false positives.
    tests['hashchange'] = function() {
    	return isEventSupported('hashchange', window) && (document.documentMode === undefined || document.documentMode > 7);
    };

    // Per 1.6:
    // This used to be Modernizr.historymanagement but the longer
    // name has been deprecated in favor of a shorter and property-matching one.
    // The old API is still available in 1.6, but as of 2.0 will throw a warning,
    // and in the first release thereafter disappear entirely.
    tests['history'] = function() {
    	return !!(window.history && history.pushState);
    };

    tests['draganddrop'] = function() {
    	var div = document.createElement('div');
    	return ('draggable' in div) || ('ondragstart' in div && 'ondrop' in div);
    };

    // FF3.6 was EOL'ed on 4/24/12, but the ESR version of FF10
    // will be supported until FF19 (2/12/13), at which time, ESR becomes FF17.
    // FF10 still uses prefixes, so check for it until then.
    // for more ESR info, see: mozilla.org/en-US/firefox/organizations/faq/
    tests['websockets'] = function() {
    	return 'WebSocket' in window || 'MozWebSocket' in window;
    };


    // css-tricks.com/rgba-browser-support/
    tests['rgba'] = function() {
        // Set an rgba() color and check the returned value

        setCss('background-color:rgba(150,255,150,.5)');

        return contains(mStyle.backgroundColor, 'rgba');
    };

    tests['hsla'] = function() {
        // Same as rgba(), in fact, browsers re-map hsla() to rgba() internally,
        //   except IE9 who retains it as hsla

        setCss('background-color:hsla(120,40%,100%,.5)');

        return contains(mStyle.backgroundColor, 'rgba') || contains(mStyle.backgroundColor, 'hsla');
    };

    tests['multiplebgs'] = function() {
        // Setting multiple images AND a color on the background shorthand property
        //  and then querying the style.background property value for the number of
        //  occurrences of "url(" is a reliable method for detecting ACTUAL support for this!

        	setCss('background:url(https://),url(https://),red url(https://)');

        // If the UA supports multiple backgrounds, there should be three occurrences
        //   of the string "url(" in the return value for elemStyle.background

        	return (/(url\s*\(.*?){3}/).test(mStyle.background);
        };



    // this will false positive in Opera Mini
    //   github.com/Modernizr/Modernizr/issues/396

    tests['backgroundsize'] = function() {
    	return testPropsAll('backgroundSize');
    };

    tests['borderimage'] = function() {
    	return testPropsAll('borderImage');
    };


    // Super comprehensive table about all the unique implementations of
    // border-radius: muddledramblings.com/table-of-css3-border-radius-compliance

    tests['borderradius'] = function() {
    	return testPropsAll('borderRadius');
    };

    // WebOS unfortunately false positives on this test.
    tests['boxshadow'] = function() {
    	return testPropsAll('boxShadow');
    };

    // FF3.0 will false positive on this test
    tests['textshadow'] = function() {
    	return document.createElement('div').style.textShadow === '';
    };


    tests['opacity'] = function() {
        // Browsers that actually have CSS Opacity implemented have done so
        //  according to spec, which means their return values are within the
        //  range of [0.0,1.0] - including the leading zero.

        setCssAll('opacity:.55');

        // The non-literal . in this regex is intentional:
        //   German Chrome returns this value as 0,55
        // github.com/Modernizr/Modernizr/issues/#issue/59/comment/516632
        return (/^0.55$/).test(mStyle.opacity);
    };


    // Note, Android < 4 will pass this test, but can only animate
    //   a single property at a time
    //   goo.gl/v3V4Gp
    tests['cssanimations'] = function() {
    	return testPropsAll('animationName');
    };


    tests['csscolumns'] = function() {
    	return testPropsAll('columnCount');
    };


    tests['cssgradients'] = function() {
        /**
         * For CSS Gradients syntax, please see:
         * webkit.org/blog/175/introducing-css-gradients/
         * developer.mozilla.org/en/CSS/-moz-linear-gradient
         * developer.mozilla.org/en/CSS/-moz-radial-gradient
         * dev.w3.org/csswg/css3-images/#gradients-
         */

         var str1 = 'background-image:',
         str2 = 'gradient(linear,left top,right bottom,from(#9f9),to(white));',
         str3 = 'linear-gradient(left top,#9f9, white);';

         setCss(
             // legacy webkit syntax (FIXME: remove when syntax not in use anymore)
             (str1 + '-webkit- '.split(' ').join(str2 + str1) +
             // standard syntax             // trailing 'background-image:'
             prefixes.join(str3 + str1)).slice(0, -str1.length)
             );

         return contains(mStyle.backgroundImage, 'gradient');
     };


     tests['cssreflections'] = function() {
     	return testPropsAll('boxReflect');
     };


     tests['csstransforms'] = function() {
     	return !!testPropsAll('transform');
     };


     tests['csstransforms3d'] = function() {

     	var ret = !!testPropsAll('perspective');

        // Webkit's 3D transforms are passed off to the browser's own graphics renderer.
        //   It works fine in Safari on Leopard and Snow Leopard, but not in Chrome in
        //   some conditions. As a result, Webkit typically recognizes the syntax but
        //   will sometimes throw a false positive, thus we must do a more thorough check:
        if ( ret && 'webkitPerspective' in docElement.style ) {

          // Webkit allows this media query to succeed only if the feature is enabled.
          // `@media (transform-3d),(-webkit-transform-3d){ ... }`
          injectElementWithStyles('@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}', function( node, rule ) {
          	ret = node.offsetLeft === 9 && node.offsetHeight === 3;
          });
      }
      return ret;
  };


  tests['csstransitions'] = function() {
  	return testPropsAll('transition');
  };


  /*>>fontface*/
    // @font-face detection routine by Diego Perini
    // javascript.nwbox.com/CSSSupport/

    // false positives:
    //   WebOS github.com/Modernizr/Modernizr/issues/342
    //   WP7   github.com/Modernizr/Modernizr/issues/538
    tests['fontface'] = function() {
    	var bool;

    	injectElementWithStyles('@font-face {font-family:"font";src:url("https://")}', function( node, rule ) {
    		var style = document.getElementById('smodernizr'),
    		sheet = style.sheet || style.styleSheet,
    		cssText = sheet ? (sheet.cssRules && sheet.cssRules[0] ? sheet.cssRules[0].cssText : sheet.cssText || '') : '';

    		bool = /src/i.test(cssText) && cssText.indexOf(rule.split(' ')[0]) === 0;
    	});

    	return bool;
    };
    /*>>fontface*/

    // CSS generated content detection
    tests['generatedcontent'] = function() {
    	var bool;

    	injectElementWithStyles(['#',mod,'{font:0/0 a}#',mod,':after{content:"',smile,'";visibility:hidden;font:3px/1 a}'].join(''), function( node ) {
    		bool = node.offsetHeight >= 3;
    	});

    	return bool;
    };



    // These tests evaluate support of the video/audio elements, as well as
    // testing what types of content they support.
    //
    // We're using the Boolean constructor here, so that we can extend the value
    // e.g.  Modernizr.video     // true
    //       Modernizr.video.ogg // 'probably'
    //
    // Codec values from : github.com/NielsLeenheer/html5test/blob/9106a8/index.html#L845
    //                     thx to NielsLeenheer and zcorpan

    // Note: in some older browsers, "no" was a return value instead of empty string.
    //   It was live in FF3.5.0 and 3.5.1, but fixed in 3.5.2
    //   It was also live in Safari 4.0.0 - 4.0.4, but fixed in 4.0.5

    tests['video'] = function() {
    	var elem = document.createElement('video'),
    	bool = false;

        // IE9 Running on Windows Server SKU can cause an exception to be thrown, bug #224
        try {
        	if ( bool = !!elem.canPlayType ) {
        		bool      = new Boolean(bool);
        		bool.ogg  = elem.canPlayType('video/ogg; codecs="theora"')      .replace(/^no$/,'');

                // Without QuickTime, this value will be `undefined`. github.com/Modernizr/Modernizr/issues/546
                bool.h264 = elem.canPlayType('video/mp4; codecs="avc1.42E01E"') .replace(/^no$/,'');

                bool.webm = elem.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,'');
            }

        } catch(e) { }

        return bool;
    };

    tests['audio'] = function() {
    	var elem = document.createElement('audio'),
    	bool = false;

    	try {
    		if ( bool = !!elem.canPlayType ) {
    			bool      = new Boolean(bool);
    			bool.ogg  = elem.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/,'');
    			bool.mp3  = elem.canPlayType('audio/mpeg;')               .replace(/^no$/,'');

                // Mimetypes accepted:
                //   developer.mozilla.org/En/Media_formats_supported_by_the_audio_and_video_elements
                //   bit.ly/iphoneoscodecs
                bool.wav  = elem.canPlayType('audio/wav; codecs="1"')     .replace(/^no$/,'');
                bool.m4a  = ( elem.canPlayType('audio/x-m4a;')            ||
                	elem.canPlayType('audio/aac;'))             .replace(/^no$/,'');
            }
        } catch(e) { }

        return bool;
    };


    // In FF4, if disabled, window.localStorage should === null.

    // Normally, we could not test that directly and need to do a
    //   `('localStorage' in window) && ` test first because otherwise Firefox will
    //   throw bugzil.la/365772 if cookies are disabled

    // Also in iOS5 Private Browsing mode, attempting to use localStorage.setItem
    // will throw the exception:
    //   QUOTA_EXCEEDED_ERRROR DOM Exception 22.
    // Peculiarly, getItem and removeItem calls do not throw.

    // Because we are forced to try/catch this, we'll go aggressive.

    // Just FWIW: IE8 Compat mode supports these features completely:
    //   www.quirksmode.org/dom/html5.html
    // But IE8 doesn't support either with local files

    tests['localstorage'] = function() {
    	try {
    		localStorage.setItem(mod, mod);
    		localStorage.removeItem(mod);
    		return true;
    	} catch(e) {
    		return false;
    	}
    };

    tests['sessionstorage'] = function() {
    	try {
    		sessionStorage.setItem(mod, mod);
    		sessionStorage.removeItem(mod);
    		return true;
    	} catch(e) {
    		return false;
    	}
    };


    tests['webworkers'] = function() {
    	return !!window.Worker;
    };


    tests['applicationcache'] = function() {
    	return !!window.applicationCache;
    };


    // Thanks to Erik Dahlstrom
    tests['svg'] = function() {
    	return !!document.createElementNS && !!document.createElementNS(ns.svg, 'svg').createSVGRect;
    };

    // specifically for SVG inline in HTML, not within XHTML
    // test page: paulirish.com/demo/inline-svg
    tests['inlinesvg'] = function() {
    	var div = document.createElement('div');
    	div.innerHTML = '<svg/>';
    	return (div.firstChild && div.firstChild.namespaceURI) == ns.svg;
    };

    // SVG SMIL animation
    tests['smil'] = function() {
    	return !!document.createElementNS && /SVGAnimate/.test(toString.call(document.createElementNS(ns.svg, 'animate')));
    };

    // This test is only for clip paths in SVG proper, not clip paths on HTML content
    // demo: srufaculty.sru.edu/david.dailey/svg/newstuff/clipPath4.svg

    // However read the comments to dig into applying SVG clippaths to HTML content here:
    //   github.com/Modernizr/Modernizr/issues/213#issuecomment-1149491
    tests['svgclippaths'] = function() {
    	return !!document.createElementNS && /SVGClipPath/.test(toString.call(document.createElementNS(ns.svg, 'clipPath')));
    };

    /*>>webforms*/
    // input features and input types go directly onto the ret object, bypassing the tests loop.
    // Hold this guy to execute in a moment.
    function webforms() {
    	/*>>input*/
        // Run through HTML5's new input attributes to see if the UA understands any.
        // We're using f which is the <input> element created early on
        // Mike Taylr has created a comprehensive resource for testing these attributes
        //   when applied to all input types:
        //   miketaylr.com/code/input-type-attr.html
        // spec: www.whatwg.org/specs/web-apps/current-work/multipage/the-input-element.html#input-type-attr-summary

        // Only input placeholder is tested while textarea's placeholder is not.
        // Currently Safari 4 and Opera 11 have support only for the input placeholder
        // Both tests are available in feature-detects/forms-placeholder.js
        Modernizr['input'] = (function( props ) {
        	for ( var i = 0, len = props.length; i < len; i++ ) {
        		attrs[ props[i] ] = !!(props[i] in inputElem);
        	}
        	if (attrs.list){
              // safari false positive's on datalist: webk.it/74252
              // see also github.com/Modernizr/Modernizr/issues/146
              attrs.list = !!(document.createElement('datalist') && window.HTMLDataListElement);
          }
          return attrs;
      })('autocomplete autofocus list placeholder max min multiple pattern required step'.split(' '));
      /*>>input*/

      /*>>inputtypes*/
        // Run through HTML5's new input types to see if the UA understands any.
        //   This is put behind the tests runloop because it doesn't return a
        //   true/false like all the other tests; instead, it returns an object
        //   containing each input type with its corresponding true/false value

        // Big thanks to @miketaylr for the html5 forms expertise. miketaylr.com/
        Modernizr['inputtypes'] = (function(props) {

        	for ( var i = 0, bool, inputElemType, defaultView, len = props.length; i < len; i++ ) {

        		inputElem.setAttribute('type', inputElemType = props[i]);
        		bool = inputElem.type !== 'text';

                // We first check to see if the type we give it sticks..
                // If the type does, we feed it a textual value, which shouldn't be valid.
                // If the value doesn't stick, we know there's input sanitization which infers a custom UI
                if ( bool ) {

                	inputElem.value         = smile;
                	inputElem.style.cssText = 'position:absolute;visibility:hidden;';

                	if ( /^range$/.test(inputElemType) && inputElem.style.WebkitAppearance !== undefined ) {

                		docElement.appendChild(inputElem);
                		defaultView = document.defaultView;

                      // Safari 2-4 allows the smiley as a value, despite making a slider
                      bool =  defaultView.getComputedStyle &&
                      defaultView.getComputedStyle(inputElem, null).WebkitAppearance !== 'textfield' &&
                              // Mobile android web browser has false positive, so must
                              // check the height to see if the widget is actually there.
                              (inputElem.offsetHeight !== 0);

                              docElement.removeChild(inputElem);

                          } else if ( /^(search|tel)$/.test(inputElemType) ){
                      // Spec doesn't define any special parsing or detectable UI
                      //   behaviors so we pass these through as true

                      // Interestingly, opera fails the earlier test, so it doesn't
                      //  even make it here.

                  } else if ( /^(url|email)$/.test(inputElemType) ) {
                      // Real url and email support comes with prebaked validation.
                      bool = inputElem.checkValidity && inputElem.checkValidity() === false;

                  } else {
                      // If the upgraded input compontent rejects the :) text, we got a winner
bool = inputElem.value != smile;
}
}

inputs[ props[i] ] = !!bool;
}
return inputs;
})('search tel url email datetime date month week time datetime-local number range color'.split(' '));
/*>>inputtypes*/
}
/*>>webforms*/


    // End of test definitions
    // -----------------------



    // Run through all tests and detect their support in the current UA.
    // todo: hypothetically we could be doing an array of tests and use a basic loop here.
    for ( var feature in tests ) {
    	if ( hasOwnProp(tests, feature) ) {
            // run the test, throw the return value into the Modernizr,
            //   then based on that boolean, define an appropriate className
            //   and push it into an array of classes we'll join later.
            featureName  = feature.toLowerCase();
            Modernizr[featureName] = tests[feature]();

            classes.push((Modernizr[featureName] ? '' : 'no-') + featureName);
        }
    }

    /*>>webforms*/
    // input tests need to run.
    Modernizr.input || webforms();
    /*>>webforms*/


    /**
     * addTest allows the user to define their own feature tests
     * the result will be added onto the Modernizr object,
     * as well as an appropriate className set on the html element
     *
     * @param feature - String naming the feature
     * @param test - Function returning true if feature is supported, false if not
     */
     Modernizr.addTest = function ( feature, test ) {
     	if ( typeof feature == 'object' ) {
     		for ( var key in feature ) {
     			if ( hasOwnProp( feature, key ) ) {
     				Modernizr.addTest( key, feature[ key ] );
     			}
     		}
     	} else {

     		feature = feature.toLowerCase();

     		if ( Modernizr[feature] !== undefined ) {
           // we're going to quit if you're trying to overwrite an existing test
           // if we were to allow it, we'd do this:
           //   var re = new RegExp("\\b(no-)?" + feature + "\\b");
           //   docElement.className = docElement.className.replace( re, '' );
           // but, no rly, stuff 'em.
           return Modernizr;
       }

       test = typeof test == 'function' ? test() : test;

       if (typeof enableClasses !== "undefined" && enableClasses) {
       	docElement.className += ' ' + (test ? '' : 'no-') + feature;
       }
       Modernizr[feature] = test;

   }

       return Modernizr; // allow chaining.
   };


    // Reset modElem.cssText to nothing to reduce memory footprint.
    setCss('');
    modElem = inputElem = null;

    /*>>shiv*/
    /**
     * @preserve HTML5 Shiv prev3.7.1 | @afarkas @jdalton @jon_neal @rem | MIT/GPL2 Licensed
     */
     ;(function(window, document) {
     	/*jshint evil:true */
     	/** version */
     	var version = '3.7.0';

     	/** Preset options */
     	var options = window.html5 || {};

     	/** Used to skip problem elements */
     	var reSkip = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i;

     	/** Not all elements can be cloned in IE **/
     	var saveClones = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i;

     	/** Detect whether the browser supports default html5 styles */
     	var supportsHtml5Styles;

     	/** Name of the expando, to work with multiple documents or to re-shiv one document */
     	var expando = '_html5shiv';

     	/** The id for the the documents expando */
     	var expanID = 0;

     	/** Cached data for each document */
     	var expandoData = {};

     	/** Detect whether the browser supports unknown elements */
     	var supportsUnknownElements;

     	(function() {
     		try {
     			var a = document.createElement('a');
     			a.innerHTML = '<xyz></xyz>';
            //if the hidden property is implemented we can assume, that the browser supports basic HTML5 Styles
            supportsHtml5Styles = ('hidden' in a);

            supportsUnknownElements = a.childNodes.length == 1 || (function() {
              // assign a false positive if unable to shiv
              (document.createElement)('a');
              var frag = document.createDocumentFragment();
              return (
              	typeof frag.cloneNode == 'undefined' ||
              	typeof frag.createDocumentFragment == 'undefined' ||
              	typeof frag.createElement == 'undefined'
              	);
          }());
        } catch(e) {
            // assign a false positive if detection fails => unable to shiv
            supportsHtml5Styles = true;
            supportsUnknownElements = true;
        }

    }());

     	/*--------------------------------------------------------------------------*/

        /**
         * Creates a style sheet with the given CSS text and adds it to the document.
         * @private
         * @param {Document} ownerDocument The document.
         * @param {String} cssText The CSS text.
         * @returns {StyleSheet} The style element.
         */
         function addStyleSheet(ownerDocument, cssText) {
         	var p = ownerDocument.createElement('p'),
         	parent = ownerDocument.getElementsByTagName('head')[0] || ownerDocument.documentElement;

         	p.innerHTML = 'x<style>' + cssText + '</style>';
         	return parent.insertBefore(p.lastChild, parent.firstChild);
         }

        /**
         * Returns the value of `html5.elements` as an array.
         * @private
         * @returns {Array} An array of shived element node names.
         */
         function getElements() {
         	var elements = html5.elements;
         	return typeof elements == 'string' ? elements.split(' ') : elements;
         }

        /**
         * Returns the data associated to the given document
         * @private
         * @param {Document} ownerDocument The document.
         * @returns {Object} An object of data.
         */
         function getExpandoData(ownerDocument) {
         	var data = expandoData[ownerDocument[expando]];
         	if (!data) {
         		data = {};
         		expanID++;
         		ownerDocument[expando] = expanID;
         		expandoData[expanID] = data;
         	}
         	return data;
         }

        /**
         * returns a shived element for the given nodeName and document
         * @memberOf html5
         * @param {String} nodeName name of the element
         * @param {Document} ownerDocument The context document.
         * @returns {Object} The shived element.
         */
         function createElement(nodeName, ownerDocument, data){
         	if (!ownerDocument) {
         		ownerDocument = document;
         	}
         	if(supportsUnknownElements){
         		return ownerDocument.createElement(nodeName);
         	}
         	if (!data) {
         		data = getExpandoData(ownerDocument);
         	}
         	var node;

         	if (data.cache[nodeName]) {
         		node = data.cache[nodeName].cloneNode();
         	} else if (saveClones.test(nodeName)) {
         		node = (data.cache[nodeName] = data.createElem(nodeName)).cloneNode();
         	} else {
         		node = data.createElem(nodeName);
         	}

          // Avoid adding some elements to fragments in IE < 9 because
          // * Attributes like `name` or `type` cannot be set/changed once an element
          //   is inserted into a document/fragment
          // * Link elements with `src` attributes that are inaccessible, as with
          //   a 403 response, will cause the tab/window to crash
          // * Script elements appended to fragments will execute when their `src`
          //   or `text` property is set
          return node.canHaveChildren && !reSkip.test(nodeName) && !node.tagUrn ? data.frag.appendChild(node) : node;
      }

        /**
         * returns a shived DocumentFragment for the given document
         * @memberOf html5
         * @param {Document} ownerDocument The context document.
         * @returns {Object} The shived DocumentFragment.
         */
         function createDocumentFragment(ownerDocument, data){
         	if (!ownerDocument) {
         		ownerDocument = document;
         	}
         	if(supportsUnknownElements){
         		return ownerDocument.createDocumentFragment();
         	}
         	data = data || getExpandoData(ownerDocument);
         	var clone = data.frag.cloneNode(),
         	i = 0,
         	elems = getElements(),
         	l = elems.length;
         	for(;i<l;i++){
         		clone.createElement(elems[i]);
         	}
         	return clone;
         }

        /**
         * Shivs the `createElement` and `createDocumentFragment` methods of the document.
         * @private
         * @param {Document|DocumentFragment} ownerDocument The document.
         * @param {Object} data of the document.
         */
         function shivMethods(ownerDocument, data) {
         	if (!data.cache) {
         		data.cache = {};
         		data.createElem = ownerDocument.createElement;
         		data.createFrag = ownerDocument.createDocumentFragment;
         		data.frag = data.createFrag();
         	}


         	ownerDocument.createElement = function(nodeName) {
            //abort shiv
            if (!html5.shivMethods) {
            	return data.createElem(nodeName);
            }
            return createElement(nodeName, ownerDocument, data);
        };

        ownerDocument.createDocumentFragment = Function('h,f', 'return function(){' +
        	'var n=f.cloneNode(),c=n.createElement;' +
        	'h.shivMethods&&(' +
                                                          // unroll the `createElement` calls
                                                          getElements().join().replace(/[\w\-]+/g, function(nodeName) {
                                                          	data.createElem(nodeName);
                                                          	data.frag.createElement(nodeName);
                                                          	return 'c("' + nodeName + '")';
                                                          }) +
                                                          ');return n}'
        )(html5, data.frag);
    }

    /*--------------------------------------------------------------------------*/

        /**
         * Shivs the given document.
         * @memberOf html5
         * @param {Document} ownerDocument The document to shiv.
         * @returns {Document} The shived document.
         */
         function shivDocument(ownerDocument) {
         	if (!ownerDocument) {
         		ownerDocument = document;
         	}
         	var data = getExpandoData(ownerDocument);

         	if (html5.shivCSS && !supportsHtml5Styles && !data.hasCSS) {
         		data.hasCSS = !!addStyleSheet(ownerDocument,
                                          // corrects block display not defined in IE6/7/8/9
                                          'article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}' +
                                            // adds styling not present in IE6/7/8/9
                                            'mark{background:#FF0;color:#000}' +
                                            // hides non-rendered elements
                                            'template{display:none}'
                                            );
         	}
         	if (!supportsUnknownElements) {
         		shivMethods(ownerDocument, data);
         	}
         	return ownerDocument;
         }

         /*--------------------------------------------------------------------------*/

        /**
         * The `html5` object is exposed so that more elements can be shived and
         * existing shiving can be detected on iframes.
         * @type Object
         * @example
         *
         * // options can be changed before the script is included
         * html5 = { 'elements': 'mark section', 'shivCSS': false, 'shivMethods': false };
         */
         var html5 = {

          /**
           * An array or space separated string of node names of the elements to shiv.
           * @memberOf html5
           * @type Array|String
           */
           'elements': options.elements || 'abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video',

          /**
           * current version of html5shiv
           */
           'version': version,

          /**
           * A flag to indicate that the HTML5 style sheet should be inserted.
           * @memberOf html5
           * @type Boolean
           */
           'shivCSS': (options.shivCSS !== false),

          /**
           * Is equal to true if a browser supports creating unknown/HTML5 elements
           * @memberOf html5
           * @type boolean
           */
           'supportsUnknownElements': supportsUnknownElements,

          /**
           * A flag to indicate that the document's `createElement` and `createDocumentFragment`
           * methods should be overwritten.
           * @memberOf html5
           * @type Boolean
           */
           'shivMethods': (options.shivMethods !== false),

          /**
           * A string to describe the type of `html5` object ("default" or "default print").
           * @memberOf html5
           * @type String
           */
           'type': 'default',

          // shivs the document according to the specified `html5` object options
          'shivDocument': shivDocument,

          //creates a shived element
          createElement: createElement,

          //creates a shived documentFragment
          createDocumentFragment: createDocumentFragment
      };

      /*--------------------------------------------------------------------------*/

        // expose html5
        window.html5 = html5;

        // shiv the document
        shivDocument(document);

    }(this, document));
/*>>shiv*/

    // Assign private properties to the return object with prefix
    Modernizr._version      = version;

    // expose these for the plugin API. Look in the source for how to join() them against your input
    /*>>prefixes*/
    Modernizr._prefixes     = prefixes;
    /*>>prefixes*/
    /*>>domprefixes*/
    Modernizr._domPrefixes  = domPrefixes;
    Modernizr._cssomPrefixes  = cssomPrefixes;
    /*>>domprefixes*/

    /*>>mq*/
    // Modernizr.mq tests a given media query, live against the current state of the window
    // A few important notes:
    //   * If a browser does not support media queries at all (eg. oldIE) the mq() will always return false
    //   * A max-width or orientation query will be evaluated against the current state, which may change later.
    //   * You must specify values. Eg. If you are testing support for the min-width media query use:
    //       Modernizr.mq('(min-width:0)')
    // usage:
    // Modernizr.mq('only screen and (max-width:768)')
    Modernizr.mq            = testMediaQuery;
    /*>>mq*/

    /*>>hasevent*/
    // Modernizr.hasEvent() detects support for a given event, with an optional element to test on
    // Modernizr.hasEvent('gesturestart', elem)
    Modernizr.hasEvent      = isEventSupported;
    /*>>hasevent*/

    /*>>testprop*/
    // Modernizr.testProp() investigates whether a given style property is recognized
    // Note that the property names must be provided in the camelCase variant.
    // Modernizr.testProp('pointerEvents')
    Modernizr.testProp      = function(prop){
    	return testProps([prop]);
    };
    /*>>testprop*/

    /*>>testallprops*/
    // Modernizr.testAllProps() investigates whether a given style property,
    //   or any of its vendor-prefixed variants, is recognized
    // Note that the property names must be provided in the camelCase variant.
    // Modernizr.testAllProps('boxSizing')
    Modernizr.testAllProps  = testPropsAll;
    /*>>testallprops*/


    /*>>teststyles*/
    // Modernizr.testStyles() allows you to add custom styles to the document and test an element afterwards
    // Modernizr.testStyles('#modernizr { position:absolute }', function(elem, rule){ ... })
    Modernizr.testStyles    = injectElementWithStyles;
    /*>>teststyles*/


    /*>>prefixed*/
    // Modernizr.prefixed() returns the prefixed or nonprefixed property name variant of your input
    // Modernizr.prefixed('boxSizing') // 'MozBoxSizing'

    // Properties must be passed as dom-style camelcase, rather than `box-sizing` hypentated style.
    // Return values will also be the camelCase variant, if you need to translate that to hypenated style use:
    //
    //     str.replace(/([A-Z])/g, function(str,m1){ return '-' + m1.toLowerCase(); }).replace(/^ms-/,'-ms-');

    // If you're trying to ascertain which transition end event to bind to, you might do something like...
    //
    //     var transEndEventNames = {
    //       'WebkitTransition' : 'webkitTransitionEnd',
    //       'MozTransition'    : 'transitionend',
    //       'OTransition'      : 'oTransitionEnd',
    //       'msTransition'     : 'MSTransitionEnd',
    //       'transition'       : 'transitionend'
    //     },
    //     transEndEventName = transEndEventNames[ Modernizr.prefixed('transition') ];

    Modernizr.prefixed      = function(prop, obj, elem){
    	if(!obj) {
    		return testPropsAll(prop, 'pfx');
    	} else {
        // Testing DOM property e.g. Modernizr.prefixed('requestAnimationFrame', window) // 'mozRequestAnimationFrame'
        return testPropsAll(prop, obj, elem);
    }
};
/*>>prefixed*/


/*>>cssclasses*/
    // Remove "no-js" class from <html> element, if it exists:
    docElement.className = docElement.className.replace(/(^|\s)no-js(\s|$)/, '$1$2') +

                            // Add the new classes to the <html> element.
                            (enableClasses ? ' js ' + classes.join(' ') : '');
                            /*>>cssclasses*/

                            return Modernizr;

                        })(this, this.document);

/*!
* jQuery Cycle2; version: 2.1.6 build: 20141007
* http://jquery.malsup.com/cycle2/
* Copyright (c) 2014 M. Alsup; Dual licensed: MIT/GPL
*/

/* Cycle2 core engine */
;(function($) {
	"use strict";

	var version = '2.1.6';

	$.fn.cycle = function( options ) {
    // fix mistakes with the ready state
    var o;
    if ( this.length === 0 && !$.isReady ) {
    	o = { s: this.selector, c: this.context };
    	$.fn.cycle.log('requeuing slideshow (dom not ready)');
    	$(function() {
    		$( o.s, o.c ).cycle(options);
    	});
    	return this;
    }

    return this.each(function() {
    	var data, opts, shortName, val;
    	var container = $(this);
    	var log = $.fn.cycle.log;

    	if ( container.data('cycle.opts') )
            return; // already initialized

        if ( container.data('cycle-log') === false || 
        	( options && options.log === false ) ||
        	( opts && opts.log === false) ) {
        	log = $.noop;
    }

    log('--c2 init--');
    data = container.data();
    for (var p in data) {
            // allow props to be accessed sans 'cycle' prefix and log the overrides
            if (data.hasOwnProperty(p) && /^cycle[A-Z]+/.test(p) ) {
            	val = data[p];
            	shortName = p.match(/^cycle(.*)/)[1].replace(/^[A-Z]/, lowerCase);
            	log(shortName+':', val, '('+typeof val +')');
            	data[shortName] = val;
            }
        }

        opts = $.extend( {}, $.fn.cycle.defaults, data, options || {});

        opts.timeoutId = 0;
        opts.paused = opts.paused || false; // #57
        opts.container = container;
        opts._maxZ = opts.maxZ;

        opts.API = $.extend ( { _container: container }, $.fn.cycle.API );
        opts.API.log = log;
        opts.API.trigger = function( eventName, args ) {
        	opts.container.trigger( eventName, args );
        	return opts.API;
        };

        container.data( 'cycle.opts', opts );
        container.data( 'cycle.API', opts.API );

        // opportunity for plugins to modify opts and API
        opts.API.trigger('cycle-bootstrap', [ opts, opts.API ]);

        opts.API.addInitialSlides();
        opts.API.preInitSlideshow();

        if ( opts.slides.length )
        	opts.API.initSlideshow();
    });
};

$.fn.cycle.API = {
	opts: function() {
		return this._container.data( 'cycle.opts' );
	},
	addInitialSlides: function() {
		var opts = this.opts();
		var slides = opts.slides;
		opts.slideCount = 0;
        opts.slides = $(); // empty set
        
        // add slides that already exist
        slides = slides.jquery ? slides : opts.container.find( slides );

        if ( opts.random ) {
        	slides.sort(function() {return Math.random() - 0.5;});
        }

        opts.API.add( slides );
    },

    preInitSlideshow: function() {
    	var opts = this.opts();
    	opts.API.trigger('cycle-pre-initialize', [ opts ]);
    	var tx = $.fn.cycle.transitions[opts.fx];
    	if (tx && $.isFunction(tx.preInit))
    		tx.preInit( opts );
    	opts._preInitialized = true;
    },

    postInitSlideshow: function() {
    	var opts = this.opts();
    	opts.API.trigger('cycle-post-initialize', [ opts ]);
    	var tx = $.fn.cycle.transitions[opts.fx];
    	if (tx && $.isFunction(tx.postInit))
    		tx.postInit( opts );
    },

    initSlideshow: function() {
    	var opts = this.opts();
    	var pauseObj = opts.container;
    	var slideOpts;
    	opts.API.calcFirstSlide();

    	if ( opts.container.css('position') == 'static' )
    		opts.container.css('position', 'relative');

    	$(opts.slides[opts.currSlide]).css({
    		opacity: 1,
    		display: 'block',
    		visibility: 'visible'
    	});
    	opts.API.stackSlides( opts.slides[opts.currSlide], opts.slides[opts.nextSlide], !opts.reverse );

    	if ( opts.pauseOnHover ) {
            // allow pauseOnHover to specify an element
            if ( opts.pauseOnHover !== true )
            	pauseObj = $( opts.pauseOnHover );

            pauseObj.hover(
            	function(){ opts.API.pause( true ); }, 
            	function(){ opts.API.resume( true ); }
            	);
        }

        // stage initial transition
        if ( opts.timeout ) {
        	slideOpts = opts.API.getSlideOpts( opts.currSlide );
        	opts.API.queueTransition( slideOpts, slideOpts.timeout + opts.delay );
        }

        opts._initialized = true;
        opts.API.updateView( true );
        opts.API.trigger('cycle-initialized', [ opts ]);
        opts.API.postInitSlideshow();
    },

    pause: function( hover ) {
    	var opts = this.opts(),
    	slideOpts = opts.API.getSlideOpts(),
    	alreadyPaused = opts.hoverPaused || opts.paused;

    	if ( hover )
    		opts.hoverPaused = true; 
    	else
    		opts.paused = true;

    	if ( ! alreadyPaused ) {
    		opts.container.addClass('cycle-paused');
    		opts.API.trigger('cycle-paused', [ opts ]).log('cycle-paused');

    		if ( slideOpts.timeout ) {
    			clearTimeout( opts.timeoutId );
    			opts.timeoutId = 0;

                // determine how much time is left for the current slide
                opts._remainingTimeout -= ( $.now() - opts._lastQueue );
                if ( opts._remainingTimeout < 0 || isNaN(opts._remainingTimeout) )
                	opts._remainingTimeout = undefined;
            }
        }
    },

    resume: function( hover ) {
    	var opts = this.opts(),
    	alreadyResumed = !opts.hoverPaused && !opts.paused,
    	remaining;

    	if ( hover )
    		opts.hoverPaused = false; 
    	else
    		opts.paused = false;


    	if ( ! alreadyResumed ) {
    		opts.container.removeClass('cycle-paused');
            // #gh-230; if an animation is in progress then don't queue a new transition; it will
            // happen naturally
            if ( opts.slides.filter(':animated').length === 0 )
            	opts.API.queueTransition( opts.API.getSlideOpts(), opts._remainingTimeout );
            opts.API.trigger('cycle-resumed', [ opts, opts._remainingTimeout ] ).log('cycle-resumed');
        }
    },

    add: function( slides, prepend ) {
    	var opts = this.opts();
    	var oldSlideCount = opts.slideCount;
    	var startSlideshow = false;
    	var len;

    	if ( $.type(slides) == 'string')
    		slides = $.trim( slides );

    	$( slides ).each(function(i) {
    		var slideOpts;
    		var slide = $(this);

    		if ( prepend )
    			opts.container.prepend( slide );
    		else
    			opts.container.append( slide );

    		opts.slideCount++;
    		slideOpts = opts.API.buildSlideOpts( slide );

    		if ( prepend )
    			opts.slides = $( slide ).add( opts.slides );
    		else
    			opts.slides = opts.slides.add( slide );

    		opts.API.initSlide( slideOpts, slide, --opts._maxZ );

    		slide.data('cycle.opts', slideOpts);
    		opts.API.trigger('cycle-slide-added', [ opts, slideOpts, slide ]);
    	});

    	opts.API.updateView( true );

    	startSlideshow = opts._preInitialized && (oldSlideCount < 2 && opts.slideCount >= 1);
    	if ( startSlideshow ) {
    		if ( !opts._initialized )
    			opts.API.initSlideshow();
    		else if ( opts.timeout ) {
    			len = opts.slides.length;
    			opts.nextSlide = opts.reverse ? len - 1 : 1;
    			if ( !opts.timeoutId ) {
    				opts.API.queueTransition( opts );
    			}
    		}
    	}
    },

    calcFirstSlide: function() {
    	var opts = this.opts();
    	var firstSlideIndex;
    	firstSlideIndex = parseInt( opts.startingSlide || 0, 10 );
    	if (firstSlideIndex >= opts.slides.length || firstSlideIndex < 0)
    		firstSlideIndex = 0;

    	opts.currSlide = firstSlideIndex;
    	if ( opts.reverse ) {
    		opts.nextSlide = firstSlideIndex - 1;
    		if (opts.nextSlide < 0)
    			opts.nextSlide = opts.slides.length - 1;
    	}
    	else {
    		opts.nextSlide = firstSlideIndex + 1;
    		if (opts.nextSlide == opts.slides.length)
    			opts.nextSlide = 0;
    	}
    },

    calcNextSlide: function() {
    	var opts = this.opts();
    	var roll;
    	if ( opts.reverse ) {
    		roll = (opts.nextSlide - 1) < 0;
    		opts.nextSlide = roll ? opts.slideCount - 1 : opts.nextSlide-1;
    		opts.currSlide = roll ? 0 : opts.nextSlide+1;
    	}
    	else {
    		roll = (opts.nextSlide + 1) == opts.slides.length;
    		opts.nextSlide = roll ? 0 : opts.nextSlide+1;
    		opts.currSlide = roll ? opts.slides.length-1 : opts.nextSlide-1;
    	}
    },

    calcTx: function( slideOpts, manual ) {
    	var opts = slideOpts;
    	var tx;

    	if ( opts._tempFx )
    		tx = $.fn.cycle.transitions[opts._tempFx];
    	else if ( manual && opts.manualFx )
    		tx = $.fn.cycle.transitions[opts.manualFx];

    	if ( !tx )
    		tx = $.fn.cycle.transitions[opts.fx];

    	opts._tempFx = null;
    	this.opts()._tempFx = null;

    	if (!tx) {
    		tx = $.fn.cycle.transitions.fade;
    		opts.API.log('Transition "' + opts.fx + '" not found.  Using fade.');
    	}
    	return tx;
    },

    prepareTx: function( manual, fwd ) {
    	var opts = this.opts();
    	var after, curr, next, slideOpts, tx;

    	if ( opts.slideCount < 2 ) {
    		opts.timeoutId = 0;
    		return;
    	}
    	if ( manual && ( !opts.busy || opts.manualTrump ) ) {
    		opts.API.stopTransition();
    		opts.busy = false;
    		clearTimeout(opts.timeoutId);
    		opts.timeoutId = 0;
    	}
    	if ( opts.busy )
    		return;
    	if ( opts.timeoutId === 0 && !manual )
    		return;

    	curr = opts.slides[opts.currSlide];
    	next = opts.slides[opts.nextSlide];
    	slideOpts = opts.API.getSlideOpts( opts.nextSlide );
    	tx = opts.API.calcTx( slideOpts, manual );

    	opts._tx = tx;

    	if ( manual && slideOpts.manualSpeed !== undefined )
    		slideOpts.speed = slideOpts.manualSpeed;

        // if ( opts.nextSlide === opts.currSlide )
        //     opts.API.calcNextSlide();

        // ensure that:
        //      1. advancing to a different slide
        //      2. this is either a manual event (prev/next, pager, cmd) or 
        //              a timer event and slideshow is not paused
        if ( opts.nextSlide != opts.currSlide && 
            (manual || (!opts.paused && !opts.hoverPaused && opts.timeout) )) { // #62

        	opts.API.trigger('cycle-before', [ slideOpts, curr, next, fwd ]);
        if ( tx.before )
        	tx.before( slideOpts, curr, next, fwd );

        after = function() {
        	opts.busy = false;
                // #76; bail if slideshow has been destroyed
                if (! opts.container.data( 'cycle.opts' ) )
                	return;

                if (tx.after)
                	tx.after( slideOpts, curr, next, fwd );
                opts.API.trigger('cycle-after', [ slideOpts, curr, next, fwd ]);
                opts.API.queueTransition( slideOpts);
                opts.API.updateView( true );
            };

            opts.busy = true;
            if (tx.transition)
            	tx.transition(slideOpts, curr, next, fwd, after);
            else
            	opts.API.doTransition( slideOpts, curr, next, fwd, after);

            opts.API.calcNextSlide();
            opts.API.updateView();
        } else {
        	opts.API.queueTransition( slideOpts );
        }
    },

    // perform the actual animation
    doTransition: function( slideOpts, currEl, nextEl, fwd, callback) {
    	var opts = slideOpts;
    	var curr = $(currEl), next = $(nextEl);
    	var fn = function() {
            // make sure animIn has something so that callback doesn't trigger immediately
            next.animate(opts.animIn || { opacity: 1}, opts.speed, opts.easeIn || opts.easing, callback);
        };

        next.css(opts.cssBefore || {});
        curr.animate(opts.animOut || {}, opts.speed, opts.easeOut || opts.easing, function() {
        	curr.css(opts.cssAfter || {});
        	if (!opts.sync) {
        		fn();
        	}
        });
        if (opts.sync) {
        	fn();
        }
    },

    queueTransition: function( slideOpts, specificTimeout ) {
    	var opts = this.opts();
    	var timeout = specificTimeout !== undefined ? specificTimeout : slideOpts.timeout;
    	if (opts.nextSlide === 0 && --opts.loop === 0) {
    		opts.API.log('terminating; loop=0');
    		opts.timeout = 0;
    		if ( timeout ) {
    			setTimeout(function() {
    				opts.API.trigger('cycle-finished', [ opts ]);
    			}, timeout);
    		}
    		else {
    			opts.API.trigger('cycle-finished', [ opts ]);
    		}
            // reset nextSlide
            opts.nextSlide = opts.currSlide;
            return;
        }
        if ( opts.continueAuto !== undefined ) {
        	if ( opts.continueAuto === false || 
        		($.isFunction(opts.continueAuto) && opts.continueAuto() === false )) {
        		opts.API.log('terminating automatic transitions');
        	opts.timeout = 0;
        	if ( opts.timeoutId )
        		clearTimeout(opts.timeoutId);
        	return;
        }
    }
    if ( timeout ) {
    	opts._lastQueue = $.now();
    	if ( specificTimeout === undefined )
    		opts._remainingTimeout = slideOpts.timeout;

    	if ( !opts.paused && ! opts.hoverPaused ) {
    		opts.timeoutId = setTimeout(function() { 
    			opts.API.prepareTx( false, !opts.reverse ); 
    		}, timeout );
    	}
    }
},

stopTransition: function() {
	var opts = this.opts();
	if ( opts.slides.filter(':animated').length ) {
		opts.slides.stop(false, true);
		opts.API.trigger('cycle-transition-stopped', [ opts ]);
	}

	if ( opts._tx && opts._tx.stopTransition )
		opts._tx.stopTransition( opts );
},

    // advance slide forward or back
    advanceSlide: function( val ) {
    	var opts = this.opts();
    	clearTimeout(opts.timeoutId);
    	opts.timeoutId = 0;
    	opts.nextSlide = opts.currSlide + val;

    	if (opts.nextSlide < 0)
    		opts.nextSlide = opts.slides.length - 1;
    	else if (opts.nextSlide >= opts.slides.length)
    		opts.nextSlide = 0;

    	opts.API.prepareTx( true,  val >= 0 );
    	return false;
    },

    buildSlideOpts: function( slide ) {
    	var opts = this.opts();
    	var val, shortName;
    	var slideOpts = slide.data() || {};
    	for (var p in slideOpts) {
            // allow props to be accessed sans 'cycle' prefix and log the overrides
            if (slideOpts.hasOwnProperty(p) && /^cycle[A-Z]+/.test(p) ) {
            	val = slideOpts[p];
            	shortName = p.match(/^cycle(.*)/)[1].replace(/^[A-Z]/, lowerCase);
            	opts.API.log('['+(opts.slideCount-1)+']', shortName+':', val, '('+typeof val +')');
            	slideOpts[shortName] = val;
            }
        }

        slideOpts = $.extend( {}, $.fn.cycle.defaults, opts, slideOpts );
        slideOpts.slideNum = opts.slideCount;

        try {
            // these props should always be read from the master state object
            delete slideOpts.API;
            delete slideOpts.slideCount;
            delete slideOpts.currSlide;
            delete slideOpts.nextSlide;
            delete slideOpts.slides;
        } catch(e) {
            // no op
        }
        return slideOpts;
    },

    getSlideOpts: function( index ) {
    	var opts = this.opts();
    	if ( index === undefined )
    		index = opts.currSlide;

    	var slide = opts.slides[index];
    	var slideOpts = $(slide).data('cycle.opts');
    	return $.extend( {}, opts, slideOpts );
    },
    
    initSlide: function( slideOpts, slide, suggestedZindex ) {
    	var opts = this.opts();
    	slide.css( slideOpts.slideCss || {} );
    	if ( suggestedZindex > 0 )
    		slide.css( 'zIndex', suggestedZindex );

        // ensure that speed settings are sane
        if ( isNaN( slideOpts.speed ) )
        	slideOpts.speed = $.fx.speeds[slideOpts.speed] || $.fx.speeds._default;
        if ( !slideOpts.sync )
        	slideOpts.speed = slideOpts.speed / 2;

        slide.addClass( opts.slideClass );
    },

    updateView: function( isAfter, isDuring, forceEvent ) {
    	var opts = this.opts();
    	if ( !opts._initialized )
    		return;
    	var slideOpts = opts.API.getSlideOpts();
    	var currSlide = opts.slides[ opts.currSlide ];

    	if ( ! isAfter && isDuring !== true ) {
    		opts.API.trigger('cycle-update-view-before', [ opts, slideOpts, currSlide ]);
    		if ( opts.updateView < 0 )
    			return;
    	}

    	if ( opts.slideActiveClass ) {
    		opts.slides.removeClass( opts.slideActiveClass )
    		.eq( opts.currSlide ).addClass( opts.slideActiveClass );
    	}

    	if ( isAfter && opts.hideNonActive )
    		opts.slides.filter( ':not(.' + opts.slideActiveClass + ')' ).css('visibility', 'hidden');

    	if ( opts.updateView === 0 ) {
    		setTimeout(function() {
    			opts.API.trigger('cycle-update-view', [ opts, slideOpts, currSlide, isAfter ]);
    		}, slideOpts.speed / (opts.sync ? 2 : 1) );
    	}

    	if ( opts.updateView !== 0 )
    		opts.API.trigger('cycle-update-view', [ opts, slideOpts, currSlide, isAfter ]);

    	if ( isAfter )
    		opts.API.trigger('cycle-update-view-after', [ opts, slideOpts, currSlide ]);
    },

    getComponent: function( name ) {
    	var opts = this.opts();
    	var selector = opts[name];
    	if (typeof selector === 'string') {
            // if selector is a child, sibling combinator, adjancent selector then use find, otherwise query full dom
            return (/^\s*[\>|\+|~]/).test( selector ) ? opts.container.find( selector ) : $( selector );
        }
        if (selector.jquery)
        	return selector;
        
        return $(selector);
    },

    stackSlides: function( curr, next, fwd ) {
    	var opts = this.opts();
    	if ( !curr ) {
    		curr = opts.slides[opts.currSlide];
    		next = opts.slides[opts.nextSlide];
    		fwd = !opts.reverse;
    	}

        // reset the zIndex for the common case:
        // curr slide on top,  next slide beneath, and the rest in order to be shown
        $(curr).css('zIndex', opts.maxZ);

        var i;
        var z = opts.maxZ - 2;
        var len = opts.slideCount;
        if (fwd) {
        	for ( i = opts.currSlide + 1; i < len; i++ )
        		$( opts.slides[i] ).css( 'zIndex', z-- );
        	for ( i = 0; i < opts.currSlide; i++ )
        		$( opts.slides[i] ).css( 'zIndex', z-- );
        }
        else {
        	for ( i = opts.currSlide - 1; i >= 0; i-- )
        		$( opts.slides[i] ).css( 'zIndex', z-- );
        	for ( i = len - 1; i > opts.currSlide; i-- )
        		$( opts.slides[i] ).css( 'zIndex', z-- );
        }

        $(next).css('zIndex', opts.maxZ - 1);
    },

    getSlideIndex: function( el ) {
    	return this.opts().slides.index( el );
    }

}; // API

// default logger
$.fn.cycle.log = function log() {
	/*global console:true */
	if (window.console && console.log)
		console.log('[cycle2] ' + Array.prototype.join.call(arguments, ' ') );
};

$.fn.cycle.version = function() { return 'Cycle2: ' + version; };

// helper functions

function lowerCase(s) {
	return (s || '').toLowerCase();
}

// expose transition object
$.fn.cycle.transitions = {
	custom: {
	},
	none: {
		before: function( opts, curr, next, fwd ) {
			opts.API.stackSlides( next, curr, fwd );
			opts.cssBefore = { opacity: 1, visibility: 'visible', display: 'block' };
		}
	},
	fade: {
		before: function( opts, curr, next, fwd ) {
			var css = opts.API.getSlideOpts( opts.nextSlide ).slideCss || {};
			opts.API.stackSlides( curr, next, fwd );
			opts.cssBefore = $.extend(css, { opacity: 0, visibility: 'visible', display: 'block' });
			opts.animIn = { opacity: 1 };
			opts.animOut = { opacity: 0 };
		}
	},
	fadeout: {
		before: function( opts , curr, next, fwd ) {
			var css = opts.API.getSlideOpts( opts.nextSlide ).slideCss || {};
			opts.API.stackSlides( curr, next, fwd );
			opts.cssBefore = $.extend(css, { opacity: 1, visibility: 'visible', display: 'block' });
			opts.animOut = { opacity: 0 };
		}
	},
	scrollHorz: {
		before: function( opts, curr, next, fwd ) {
			opts.API.stackSlides( curr, next, fwd );
			var w = opts.container.css('overflow','hidden').width();
			opts.cssBefore = { left: fwd ? w : - w, top: 0, opacity: 1, visibility: 'visible', display: 'block' };
			opts.cssAfter = { zIndex: opts._maxZ - 2, left: 0 };
			opts.animIn = { left: 0 };
			opts.animOut = { left: fwd ? -w : w };
		}
	}
};

// @see: http://jquery.malsup.com/cycle2/api
$.fn.cycle.defaults = {
	allowWrap:        true,
	autoSelector:     '.cycle-slideshow[data-cycle-auto-init!=false]',
	delay:            0,
	easing:           null,
	fx:              'fade',
	hideNonActive:    true,
	loop:             0,
	manualFx:         undefined,
	manualSpeed:      undefined,
	manualTrump:      true,
	maxZ:             100,
	pauseOnHover:     false,
	reverse:          false,
	slideActiveClass: 'cycle-slide-active',
	slideClass:       'cycle-slide',
	slideCss:         { position: 'absolute', top: 0, left: 0 },
	slides:          '> img',
	speed:            500,
	startingSlide:    0,
	sync:             true,
	timeout:          4000,
	updateView:       0
};

// automatically find and run slideshows
$(document).ready(function() {
	$( $.fn.cycle.defaults.autoSelector ).cycle();
});

})(jQuery);

/*! Cycle2 autoheight plugin; Copyright (c) M.Alsup, 2012; version: 20130913 */
(function($) {
	"use strict";

	$.extend($.fn.cycle.defaults, {
    autoHeight: 0, // setting this option to false disables autoHeight logic
    autoHeightSpeed: 250,
    autoHeightEasing: null
});    

	$(document).on( 'cycle-initialized', function( e, opts ) {
		var autoHeight = opts.autoHeight;
		var t = $.type( autoHeight );
		var resizeThrottle = null;
		var ratio;

		if ( t !== 'string' && t !== 'number' )
			return;

    // bind events
    opts.container.on( 'cycle-slide-added cycle-slide-removed', initAutoHeight );
    opts.container.on( 'cycle-destroyed', onDestroy );

    if ( autoHeight == 'container' ) {
    	opts.container.on( 'cycle-before', onBefore );
    }
    else if ( t === 'string' && /\d+\:\d+/.test( autoHeight ) ) { 
        // use ratio
        ratio = autoHeight.match(/(\d+)\:(\d+)/);
        ratio = ratio[1] / ratio[2];
        opts._autoHeightRatio = ratio;
    }

    // if autoHeight is a number then we don't need to recalculate the sentinel
    // index on resize
    if ( t !== 'number' ) {
        // bind unique resize handler per slideshow (so it can be 'off-ed' in onDestroy)
        opts._autoHeightOnResize = function () {
        	clearTimeout( resizeThrottle );
        	resizeThrottle = setTimeout( onResize, 50 );
        };

        $(window).on( 'resize orientationchange', opts._autoHeightOnResize );
    }

    setTimeout( onResize, 30 );

    function onResize() {
    	initAutoHeight( e, opts );
    }
});

function initAutoHeight( e, opts ) {
	var clone, height, sentinelIndex;
	var autoHeight = opts.autoHeight;

	if ( autoHeight == 'container' ) {
		height = $( opts.slides[ opts.currSlide ] ).outerHeight();
		opts.container.height( height );
	}
	else if ( opts._autoHeightRatio ) { 
		opts.container.height( opts.container.width() / opts._autoHeightRatio );
	}
	else if ( autoHeight === 'calc' || ( $.type( autoHeight ) == 'number' && autoHeight >= 0 ) ) {
		if ( autoHeight === 'calc' )
			sentinelIndex = calcSentinelIndex( e, opts );
		else if ( autoHeight >= opts.slides.length )
			sentinelIndex = 0;
		else 
			sentinelIndex = autoHeight;

        // only recreate sentinel if index is different
        if ( sentinelIndex == opts._sentinelIndex )
        	return;

        opts._sentinelIndex = sentinelIndex;
        if ( opts._sentinel )
        	opts._sentinel.remove();

        // clone existing slide as sentinel
        clone = $( opts.slides[ sentinelIndex ].cloneNode(true) );
        
        // #50; remove special attributes from cloned content
        clone.removeAttr( 'id name rel' ).find( '[id],[name],[rel]' ).removeAttr( 'id name rel' );

        clone.css({
        	position: 'static',
        	visibility: 'hidden',
        	display: 'block'
        }).prependTo( opts.container ).addClass('cycle-sentinel cycle-slide').removeClass('cycle-slide-active');
        clone.find( '*' ).css( 'visibility', 'hidden' );

        opts._sentinel = clone;
    }
}    

function calcSentinelIndex( e, opts ) {
	var index = 0, max = -1;

    // calculate tallest slide index
    opts.slides.each(function(i) {
    	var h = $(this).height();
    	if ( h > max ) {
    		max = h;
    		index = i;
    	}
    });
    return index;
}

function onBefore( e, opts, outgoing, incoming, forward ) {
	var h = $(incoming).outerHeight();
	opts.container.animate( { height: h }, opts.autoHeightSpeed, opts.autoHeightEasing );
}

function onDestroy( e, opts ) {
	if ( opts._autoHeightOnResize ) {
		$(window).off( 'resize orientationchange', opts._autoHeightOnResize );
		opts._autoHeightOnResize = null;
	}
	opts.container.off( 'cycle-slide-added cycle-slide-removed', initAutoHeight );
	opts.container.off( 'cycle-destroyed', onDestroy );
	opts.container.off( 'cycle-before', onBefore );

	if ( opts._sentinel ) {
		opts._sentinel.remove();
		opts._sentinel = null;
	}
}

})(jQuery);

/*! caption plugin for Cycle2;  version: 20130306 */
(function($) {
	"use strict";

	$.extend($.fn.cycle.defaults, {
		caption:          '> .cycle-caption',
		captionTemplate:  '{{slideNum}} / {{slideCount}}',
		overlay:          '> .cycle-overlay',
		overlayTemplate:  '<div>{{title}}</div><div>{{desc}}</div>',
		captionModule:    'caption'
	});    

	$(document).on( 'cycle-update-view', function( e, opts, slideOpts, currSlide ) {
		if ( opts.captionModule !== 'caption' )
			return;
		var el;
		$.each(['caption','overlay'], function() {
			var name = this; 
			var template = slideOpts[name+'Template'];
			var el = opts.API.getComponent( name );
			if( el.length && template ) {
				el.html( opts.API.tmpl( template, slideOpts, opts, currSlide ) );
				el.show();
			}
			else {
				el.hide();
			}
		});
	});

	$(document).on( 'cycle-destroyed', function( e, opts ) {
		var el;
		$.each(['caption','overlay'], function() {
			var name = this, template = opts[name+'Template'];
			if ( opts[name] && template ) {
				el = opts.API.getComponent( 'caption' );
				el.empty();
			}
		});
	});

})(jQuery);

/*! command plugin for Cycle2;  version: 20140415 */
(function($) {
	"use strict";

	var c2 = $.fn.cycle;

	$.fn.cycle = function( options ) {
		var cmd, cmdFn, opts;
		var args = $.makeArray( arguments );

		if ( $.type( options ) == 'number' ) {
			return this.cycle( 'goto', options );
		}

		if ( $.type( options ) == 'string' ) {
			return this.each(function() {
				var cmdArgs;
				cmd = options;
				opts = $(this).data('cycle.opts');

				if ( opts === undefined ) {
					c2.log('slideshow must be initialized before sending commands; "' + cmd + '" ignored');
					return;
				}
				else {
                cmd = cmd == 'goto' ? 'jump' : cmd; // issue #3; change 'goto' to 'jump' internally
                cmdFn = opts.API[ cmd ];
                if ( $.isFunction( cmdFn )) {
                	cmdArgs = $.makeArray( args );
                	cmdArgs.shift();
                	return cmdFn.apply( opts.API, cmdArgs );
                }
                else {
                	c2.log( 'unknown command: ', cmd );
                }
            }
        });
		}
		else {
			return c2.apply( this, arguments );
		}
	};

// copy props
$.extend( $.fn.cycle, c2 );

$.extend( c2.API, {
	next: function() {
		var opts = this.opts();
		if ( opts.busy && ! opts.manualTrump )
			return;

		var count = opts.reverse ? -1 : 1;
		if ( opts.allowWrap === false && ( opts.currSlide + count ) >= opts.slideCount )
			return;

		opts.API.advanceSlide( count );
		opts.API.trigger('cycle-next', [ opts ]).log('cycle-next');
	},

	prev: function() {
		var opts = this.opts();
		if ( opts.busy && ! opts.manualTrump )
			return;
		var count = opts.reverse ? 1 : -1;
		if ( opts.allowWrap === false && ( opts.currSlide + count ) < 0 )
			return;

		opts.API.advanceSlide( count );
		opts.API.trigger('cycle-prev', [ opts ]).log('cycle-prev');
	},

	destroy: function() {
        this.stop(); //#204

        var opts = this.opts();
        var clean = $.isFunction( $._data ) ? $._data : $.noop;  // hack for #184 and #201
        clearTimeout(opts.timeoutId);
        opts.timeoutId = 0;
        opts.API.stop();
        opts.API.trigger( 'cycle-destroyed', [ opts ] ).log('cycle-destroyed');
        opts.container.removeData();
        clean( opts.container[0], 'parsedAttrs', false );

        // #75; remove inline styles
        if ( ! opts.retainStylesOnDestroy ) {
        	opts.container.removeAttr( 'style' );
        	opts.slides.removeAttr( 'style' );
        	opts.slides.removeClass( opts.slideActiveClass );
        }
        opts.slides.each(function() {
        	var slide = $(this);
        	slide.removeData();
        	slide.removeClass( opts.slideClass );
        	clean( this, 'parsedAttrs', false );
        });
    },

    jump: function( index, fx ) {
        // go to the requested slide
        var fwd;
        var opts = this.opts();
        if ( opts.busy && ! opts.manualTrump )
        	return;
        var num = parseInt( index, 10 );
        if (isNaN(num) || num < 0 || num >= opts.slides.length) {
        	opts.API.log('goto: invalid slide index: ' + num);
        	return;
        }
        if (num == opts.currSlide) {
        	opts.API.log('goto: skipping, already on slide', num);
        	return;
        }
        opts.nextSlide = num;
        clearTimeout(opts.timeoutId);
        opts.timeoutId = 0;
        opts.API.log('goto: ', num, ' (zero-index)');
        fwd = opts.currSlide < opts.nextSlide;
        opts._tempFx = fx;
        opts.API.prepareTx( true, fwd );
    },

    stop: function() {
    	var opts = this.opts();
    	var pauseObj = opts.container;
    	clearTimeout(opts.timeoutId);
    	opts.timeoutId = 0;
    	opts.API.stopTransition();
    	if ( opts.pauseOnHover ) {
    		if ( opts.pauseOnHover !== true )
    			pauseObj = $( opts.pauseOnHover );
    		pauseObj.off('mouseenter mouseleave');
    	}
    	opts.API.trigger('cycle-stopped', [ opts ]).log('cycle-stopped');
    },

    reinit: function() {
    	var opts = this.opts();
    	opts.API.destroy();
    	opts.container.cycle();
    },

    remove: function( index ) {
    	var opts = this.opts();
    	var slide, slideToRemove, slides = [], slideNum = 1;
    	for ( var i=0; i < opts.slides.length; i++ ) {
    		slide = opts.slides[i];
    		if ( i == index ) {
    			slideToRemove = slide;
    		}
    		else {
    			slides.push( slide );
    			$( slide ).data('cycle.opts').slideNum = slideNum;
    			slideNum++;
    		}
    	}
    	if ( slideToRemove ) {
    		opts.slides = $( slides );
    		opts.slideCount--;
    		$( slideToRemove ).remove();
    		if (index == opts.currSlide)
    			opts.API.advanceSlide( 1 );
    		else if ( index < opts.currSlide )
    			opts.currSlide--;
    		else
    			opts.currSlide++;

    		opts.API.trigger('cycle-slide-removed', [ opts, index, slideToRemove ]).log('cycle-slide-removed');
    		opts.API.updateView();
    	}
    }

});

// listen for clicks on elements with data-cycle-cmd attribute
$(document).on('click.cycle', '[data-cycle-cmd]', function(e) {
    // issue cycle command
    e.preventDefault();
    var el = $(this);
    var command = el.data('cycle-cmd');
    var context = el.data('cycle-context') || '.cycle-slideshow';
    $(context).cycle(command, el.data('cycle-arg'));
});


})(jQuery);

/*! hash plugin for Cycle2;  version: 20130905 */
(function($) {
	"use strict";

	$(document).on( 'cycle-pre-initialize', function( e, opts ) {
		onHashChange( opts, true );

		opts._onHashChange = function() {
			onHashChange( opts, false );
		};

		$( window ).on( 'hashchange', opts._onHashChange);
	});

	$(document).on( 'cycle-update-view', function( e, opts, slideOpts ) {
		if ( slideOpts.hash && ( '#' + slideOpts.hash ) != window.location.hash ) {
			opts._hashFence = true;
			window.location.hash = slideOpts.hash;
		}
	});

	$(document).on( 'cycle-destroyed', function( e, opts) {
		if ( opts._onHashChange ) {
			$( window ).off( 'hashchange', opts._onHashChange );
		}
	});

	function onHashChange( opts, setStartingSlide ) {
		var hash;
		if ( opts._hashFence ) {
			opts._hashFence = false;
			return;
		}

		hash = window.location.hash.substring(1);

		opts.slides.each(function(i) {
			if ( $(this).data( 'cycle-hash' ) == hash ) {
				if ( setStartingSlide === true ) {
					opts.startingSlide = i;
				}
				else {
					var fwd = opts.currSlide < i;
					opts.nextSlide = i;
					opts.API.prepareTx( true, fwd );
				}
				return false;
			}
		});
	}

})(jQuery);

/*! loader plugin for Cycle2;  version: 20131121 */
(function($) {
	"use strict";

	$.extend($.fn.cycle.defaults, {
		loader: false
	});

	$(document).on( 'cycle-bootstrap', function( e, opts ) {
		var addFn;

		if ( !opts.loader )
			return;

    // override API.add for this slideshow
    addFn = opts.API.add;
    opts.API.add = add;

    function add( slides, prepend ) {
    	var slideArr = [];
    	if ( $.type( slides ) == 'string' )
    		slides = $.trim( slides );
    	else if ( $.type( slides) === 'array' ) {
    		for (var i=0; i < slides.length; i++ )
    			slides[i] = $(slides[i])[0];
    	}

    	slides = $( slides );
    	var slideCount = slides.length;

    	if ( ! slideCount )
    		return;

        slides.css('visibility','hidden').appendTo('body').each(function(i) { // appendTo fixes #56
        	var count = 0;
        	var slide = $(this);
        	var images = slide.is('img') ? slide : slide.find('img');
        	slide.data('index', i);
            // allow some images to be marked as unimportant (and filter out images w/o src value)
            images = images.filter(':not(.cycle-loader-ignore)').filter(':not([src=""])');
            if ( ! images.length ) {
            	--slideCount;
            	slideArr.push( slide );
            	return;
            }

            count = images.length;
            images.each(function() {
                // add images that are already loaded
                if ( this.complete ) {
                	imageLoaded();
                }
                else {
                	$(this).load(function() {
                		imageLoaded();
                	}).on("error", function() {
                		if ( --count === 0 ) {
                            // ignore this slide
                            opts.API.log('slide skipped; img not loaded:', this.src);
                            if ( --slideCount === 0 && opts.loader == 'wait') {
                            	addFn.apply( opts.API, [ slideArr, prepend ] );
                            }
                        }
                    });
                }
            });

            function imageLoaded() {
            	if ( --count === 0 ) {
            		--slideCount;
            		addSlide( slide );
            	}
            }
        });

if ( slideCount )
	opts.container.addClass('cycle-loading');


function addSlide( slide ) {
	var curr;
	if ( opts.loader == 'wait' ) {
		slideArr.push( slide );
		if ( slideCount === 0 ) {
                    // #59; sort slides into original markup order
                    slideArr.sort( sorter );
                    addFn.apply( opts.API, [ slideArr, prepend ] );
                    opts.container.removeClass('cycle-loading');
                }
            }
            else {
            	curr = $(opts.slides[opts.currSlide]);
            	addFn.apply( opts.API, [ slide, prepend ] );
            	curr.show();
            	opts.container.removeClass('cycle-loading');
            }
        }

        function sorter(a, b) {
        	return a.data('index') - b.data('index');
        }
    }
});

})(jQuery);

/*! pager plugin for Cycle2;  version: 20140415 */
(function($) {
	"use strict";

	$.extend($.fn.cycle.defaults, {
		pager:            '> .cycle-pager',
		pagerActiveClass: 'cycle-pager-active',
		pagerEvent:       'click.cycle',
		pagerEventBubble: undefined,
		pagerTemplate:    '<span>&bull;</span>'
	});

	$(document).on( 'cycle-bootstrap', function( e, opts, API ) {
    // add method to API
    API.buildPagerLink = buildPagerLink;
});

	$(document).on( 'cycle-slide-added', function( e, opts, slideOpts, slideAdded ) {
		if ( opts.pager ) {
			opts.API.buildPagerLink ( opts, slideOpts, slideAdded );
			opts.API.page = page;
		}
	});

	$(document).on( 'cycle-slide-removed', function( e, opts, index, slideRemoved ) {
		if ( opts.pager ) {
			var pagers = opts.API.getComponent( 'pager' );
			pagers.each(function() {
				var pager = $(this);
				$( pager.children()[index] ).remove();
			});
		}
	});

	$(document).on( 'cycle-update-view', function( e, opts, slideOpts ) {
		var pagers;

		if ( opts.pager ) {
			pagers = opts.API.getComponent( 'pager' );
			pagers.each(function() {
				$(this).children().removeClass( opts.pagerActiveClass )
				.eq( opts.currSlide ).addClass( opts.pagerActiveClass );
			});
		}
	});

	$(document).on( 'cycle-destroyed', function( e, opts ) {
		var pager = opts.API.getComponent( 'pager' );

		if ( pager ) {
        pager.children().off( opts.pagerEvent ); // #202
        if ( opts.pagerTemplate )
        	pager.empty();
    }
});

	function buildPagerLink( opts, slideOpts, slide ) {
		var pagerLink;
		var pagers = opts.API.getComponent( 'pager' );
		pagers.each(function() {
			var pager = $(this);
			if ( slideOpts.pagerTemplate ) {
				var markup = opts.API.tmpl( slideOpts.pagerTemplate, slideOpts, opts, slide[0] );
				pagerLink = $( markup ).appendTo( pager );
			}
			else {
				pagerLink = pager.children().eq( opts.slideCount - 1 );
			}
			pagerLink.on( opts.pagerEvent, function(e) {
				if ( ! opts.pagerEventBubble )
					e.preventDefault();
				opts.API.page( pager, e.currentTarget);
			});
		});
	}

	function page( pager, target ) {
		/*jshint validthis:true */
		var opts = this.opts();
		if ( opts.busy && ! opts.manualTrump )
			return;

		var index = pager.children().index( target );
		var nextSlide = index;
		var fwd = opts.currSlide < nextSlide;
		if (opts.currSlide == nextSlide) {
        return; // no op, clicked pager for the currently displayed slide
    }
    opts.nextSlide = nextSlide;
    opts._tempFx = opts.pagerFx;
    opts.API.prepareTx( true, fwd );
    opts.API.trigger('cycle-pager-activated', [opts, pager, target ]);
}

})(jQuery);

/*! prevnext plugin for Cycle2;  version: 20140408 */
(function($) {
	"use strict";

	$.extend($.fn.cycle.defaults, {
		next:           '> .cycle-next',
		nextEvent:      'click.cycle',
		disabledClass:  'disabled',
		prev:           '> .cycle-prev',
		prevEvent:      'click.cycle',
		swipe:          false
	});

	$(document).on( 'cycle-initialized', function( e, opts ) {
		opts.API.getComponent( 'next' ).on( opts.nextEvent, function(e) {
			e.preventDefault();
			opts.API.next();
		});

		opts.API.getComponent( 'prev' ).on( opts.prevEvent, function(e) {
			e.preventDefault();
			opts.API.prev();
		});

		if ( opts.swipe ) {
			var nextEvent = opts.swipeVert ? 'swipeUp.cycle' : 'swipeLeft.cycle swipeleft.cycle';
			var prevEvent = opts.swipeVert ? 'swipeDown.cycle' : 'swipeRight.cycle swiperight.cycle';
			opts.container.on( nextEvent, function(e) {
				opts._tempFx = opts.swipeFx;
				opts.API.next();
			});
			opts.container.on( prevEvent, function() {
				opts._tempFx = opts.swipeFx;
				opts.API.prev();
			});
		}
	});

	$(document).on( 'cycle-update-view', function( e, opts, slideOpts, currSlide ) {
		if ( opts.allowWrap )
			return;

		var cls = opts.disabledClass;
		var next = opts.API.getComponent( 'next' );
		var prev = opts.API.getComponent( 'prev' );
		var prevBoundry = opts._prevBoundry || 0;
		var nextBoundry = (opts._nextBoundry !== undefined)?opts._nextBoundry:opts.slideCount - 1;

		if ( opts.currSlide == nextBoundry )
			next.addClass( cls ).prop( 'disabled', true );
		else
			next.removeClass( cls ).prop( 'disabled', false );

		if ( opts.currSlide === prevBoundry )
			prev.addClass( cls ).prop( 'disabled', true );
		else
			prev.removeClass( cls ).prop( 'disabled', false );
	});


	$(document).on( 'cycle-destroyed', function( e, opts ) {
		opts.API.getComponent( 'prev' ).off( opts.nextEvent );
		opts.API.getComponent( 'next' ).off( opts.prevEvent );
		opts.container.off( 'swipeleft.cycle swiperight.cycle swipeLeft.cycle swipeRight.cycle swipeUp.cycle swipeDown.cycle' );
	});

})(jQuery);

/*! progressive loader plugin for Cycle2;  version: 20130315 */
(function($) {
	"use strict";

	$.extend($.fn.cycle.defaults, {
		progressive: false
	});

	$(document).on( 'cycle-pre-initialize', function( e, opts ) {
		if ( !opts.progressive )
			return;

		var API = opts.API;
		var nextFn = API.next;
		var prevFn = API.prev;
		var prepareTxFn = API.prepareTx;
		var type = $.type( opts.progressive );
		var slides, scriptEl;

		if ( type == 'array' ) {
			slides = opts.progressive;
		}
		else if ($.isFunction( opts.progressive ) ) {
			slides = opts.progressive( opts );
		}
		else if ( type == 'string' ) {
			scriptEl = $( opts.progressive );
			slides = $.trim( scriptEl.html() );
			if ( !slides )
				return;
        // is it json array?
        if ( /^(\[)/.test( slides ) ) {
        	try {
        		slides = $.parseJSON( slides );
        	}
        	catch(err) {
        		API.log( 'error parsing progressive slides', err );
        		return;
        	}
        }
        else {
            // plain text, split on delimeter
            slides = slides.split( new RegExp( scriptEl.data('cycle-split') || '\n') );
            
            // #95; look for empty slide
            if ( ! slides[ slides.length - 1 ] )
            	slides.pop();
        }
    }



    if ( prepareTxFn ) {
    	API.prepareTx = function( manual, fwd ) {
    		var index, slide;

    		if ( manual || slides.length === 0 ) {
    			prepareTxFn.apply( opts.API, [ manual, fwd ] );
    			return;
    		}

    		if ( fwd && opts.currSlide == ( opts.slideCount-1) ) {
    			slide = slides[ 0 ];
    			slides = slides.slice( 1 );
    			opts.container.one('cycle-slide-added', function(e, opts ) {
    				setTimeout(function() {
    					opts.API.advanceSlide( 1 );
    				},50);
    			});
    			opts.API.add( slide );
    		}
    		else if ( !fwd && opts.currSlide === 0 ) {
    			index = slides.length-1;
    			slide = slides[ index ];
    			slides = slides.slice( 0, index );
    			opts.container.one('cycle-slide-added', function(e, opts ) {
    				setTimeout(function() {
    					opts.currSlide = 1;
    					opts.API.advanceSlide( -1 );
    				},50);
    			});
    			opts.API.add( slide, true );
    		}
    		else {
    			prepareTxFn.apply( opts.API, [ manual, fwd ] );
    		}
    	};
    }

    if ( nextFn ) {
    	API.next = function() {
    		var opts = this.opts();
    		if ( slides.length && opts.currSlide == ( opts.slideCount - 1 ) ) {
    			var slide = slides[ 0 ];
    			slides = slides.slice( 1 );
    			opts.container.one('cycle-slide-added', function(e, opts ) {
    				nextFn.apply( opts.API );
    				opts.container.removeClass('cycle-loading');
    			});
    			opts.container.addClass('cycle-loading');
    			opts.API.add( slide );
    		}
    		else {
    			nextFn.apply( opts.API );    
    		}
    	};
    }
    
    if ( prevFn ) {
    	API.prev = function() {
    		var opts = this.opts();
    		if ( slides.length && opts.currSlide === 0 ) {
    			var index = slides.length-1;
    			var slide = slides[ index ];
    			slides = slides.slice( 0, index );
    			opts.container.one('cycle-slide-added', function(e, opts ) {
    				opts.currSlide = 1;
    				opts.API.advanceSlide( -1 );
    				opts.container.removeClass('cycle-loading');
    			});
    			opts.container.addClass('cycle-loading');
    			opts.API.add( slide, true );
    		}
    		else {
    			prevFn.apply( opts.API );
    		}
    	};
    }
});

})(jQuery);

/*! tmpl plugin for Cycle2;  version: 20121227 */
(function($) {
	"use strict";

	$.extend($.fn.cycle.defaults, {
		tmplRegex: '{{((.)?.*?)}}'
	});

	$.extend($.fn.cycle.API, {
		tmpl: function( str, opts /*, ... */) {
			var regex = new RegExp( opts.tmplRegex || $.fn.cycle.defaults.tmplRegex, 'g' );
			var args = $.makeArray( arguments );
			args.shift();
			return str.replace(regex, function(_, str) {
				var i, j, obj, prop, names = str.split('.');
				for (i=0; i < args.length; i++) {
					obj = args[i];
					if ( ! obj )
						continue;
					if (names.length > 1) {
						prop = obj;
						for (j=0; j < names.length; j++) {
							obj = prop;
							prop = prop[ names[j] ] || str;
						}
					} else {
						prop = obj[str];
					}

					if ($.isFunction(prop))
						return prop.apply(obj, args);
					if (prop !== undefined && prop !== null && prop != str)
						return prop;
				}
				return str;
			});
		}
	});    

})(jQuery);

/*!
 * jQuery Validation Plugin v1.14.0
 *
 * http://jqueryvalidation.org/
 *
 * Copyright (c) 2015 Jörn Zaefferer
 * Released under the MIT license
 */
 (function( factory ) {
 	if ( typeof define === "function" && define.amd ) {
 		define( ["jquery"], factory );
 	} else {
 		factory( jQuery );
 	}
 }(function( $ ) {

 	$.extend($.fn, {
	// http://jqueryvalidation.org/validate/
	validate: function( options ) {

		// if nothing is selected, return nothing; can't chain anyway
		if ( !this.length ) {
			if ( options && options.debug && window.console ) {
				console.warn( "Nothing selected, can't validate, returning nothing." );
			}
			return;
		}

		// check if a validator for this form was already created
		var validator = $.data( this[ 0 ], "validator" );
		if ( validator ) {
			return validator;
		}

		// Add novalidate tag if HTML5.
		this.attr( "novalidate", "novalidate" );

		validator = new $.validator( options, this[ 0 ] );
		$.data( this[ 0 ], "validator", validator );

		if ( validator.settings.onsubmit ) {

			this.on( "click.validate", ":submit", function( event ) {
				if ( validator.settings.submitHandler ) {
					validator.submitButton = event.target;
				}

				// allow suppressing validation by adding a cancel class to the submit button
				if ( $( this ).hasClass( "cancel" ) ) {
					validator.cancelSubmit = true;
				}

				// allow suppressing validation by adding the html5 formnovalidate attribute to the submit button
				if ( $( this ).attr( "formnovalidate" ) !== undefined ) {
					validator.cancelSubmit = true;
				}
			});

			// validate the form on submit
			this.on( "submit.validate", function( event ) {
				if ( validator.settings.debug ) {
					// prevent form submit to be able to see console output
					event.preventDefault();
				}
				function handle() {
					var hidden, result;
					if ( validator.settings.submitHandler ) {
						if ( validator.submitButton ) {
							// insert a hidden input as a replacement for the missing submit button
							hidden = $( "<input type='hidden'/>" )
							.attr( "name", validator.submitButton.name )
							.val( $( validator.submitButton ).val() )
							.appendTo( validator.currentForm );
						}
						result = validator.settings.submitHandler.call( validator, validator.currentForm, event );
						if ( validator.submitButton ) {
							// and clean up afterwards; thanks to no-block-scope, hidden can be referenced
							hidden.remove();
						}
						if ( result !== undefined ) {
							return result;
						}
						return false;
					}
					return true;
				}

				// prevent submit for invalid forms or custom submit handlers
				if ( validator.cancelSubmit ) {
					validator.cancelSubmit = false;
					return handle();
				}
				if ( validator.form() ) {
					if ( validator.pendingRequest ) {
						validator.formSubmitted = true;
						return false;
					}
					return handle();
				} else {
					validator.focusInvalid();
					return false;
				}
			});
}

return validator;
},
	// http://jqueryvalidation.org/valid/
	valid: function() {
		var valid, validator, errorList;

		if ( $( this[ 0 ] ).is( "form" ) ) {
			valid = this.validate().form();
		} else {
			errorList = [];
			valid = true;
			validator = $( this[ 0 ].form ).validate();
			this.each( function() {
				valid = validator.element( this ) && valid;
				errorList = errorList.concat( validator.errorList );
			});
			validator.errorList = errorList;
		}
		return valid;
	},

	// http://jqueryvalidation.org/rules/
	rules: function( command, argument ) {
		var element = this[ 0 ],
		settings, staticRules, existingRules, data, param, filtered;

		if ( command ) {
			settings = $.data( element.form, "validator" ).settings;
			staticRules = settings.rules;
			existingRules = $.validator.staticRules( element );
			switch ( command ) {
				case "add":
				$.extend( existingRules, $.validator.normalizeRule( argument ) );
				// remove messages from rules, but allow them to be set separately
				delete existingRules.messages;
				staticRules[ element.name ] = existingRules;
				if ( argument.messages ) {
					settings.messages[ element.name ] = $.extend( settings.messages[ element.name ], argument.messages );
				}
				break;
				case "remove":
				if ( !argument ) {
					delete staticRules[ element.name ];
					return existingRules;
				}
				filtered = {};
				$.each( argument.split( /\s/ ), function( index, method ) {
					filtered[ method ] = existingRules[ method ];
					delete existingRules[ method ];
					if ( method === "required" ) {
						$( element ).removeAttr( "aria-required" );
					}
				});
				return filtered;
			}
		}

		data = $.validator.normalizeRules(
			$.extend(
				{},
				$.validator.classRules( element ),
				$.validator.attributeRules( element ),
				$.validator.dataRules( element ),
				$.validator.staticRules( element )
				), element );

		// make sure required is at front
		if ( data.required ) {
			param = data.required;
			delete data.required;
			data = $.extend( { required: param }, data );
			$( element ).attr( "aria-required", "true" );
		}

		// make sure remote is at back
		if ( data.remote ) {
			param = data.remote;
			delete data.remote;
			data = $.extend( data, { remote: param });
		}

		return data;
	}
});

// Custom selectors
$.extend( $.expr[ ":" ], {
	// http://jqueryvalidation.org/blank-selector/
	blank: function( a ) {
		return !$.trim( "" + $( a ).val() );
	},
	// http://jqueryvalidation.org/filled-selector/
	filled: function( a ) {
		return !!$.trim( "" + $( a ).val() );
	},
	// http://jqueryvalidation.org/unchecked-selector/
	unchecked: function( a ) {
		return !$( a ).prop( "checked" );
	}
});

// constructor for validator
$.validator = function( options, form ) {
	this.settings = $.extend( true, {}, $.validator.defaults, options );
	this.currentForm = form;
	this.init();
};

// http://jqueryvalidation.org/jQuery.validator.format/
$.validator.format = function( source, params ) {
	if ( arguments.length === 1 ) {
		return function() {
			var args = $.makeArray( arguments );
			args.unshift( source );
			return $.validator.format.apply( this, args );
		};
	}
	if ( arguments.length > 2 && params.constructor !== Array  ) {
		params = $.makeArray( arguments ).slice( 1 );
	}
	if ( params.constructor !== Array ) {
		params = [ params ];
	}
	$.each( params, function( i, n ) {
		source = source.replace( new RegExp( "\\{" + i + "\\}", "g" ), function() {
			return n;
		});
	});
	return source;
};

$.extend( $.validator, {

	defaults: {
		messages: {},
		groups: {},
		rules: {},
		errorClass: "error",
		validClass: "valid",
		errorElement: "label",
		focusCleanup: false,
		focusInvalid: true,
		errorContainer: $( [] ),
		errorLabelContainer: $( [] ),
		onsubmit: true,
		ignore: ":hidden",
		ignoreTitle: false,
		onfocusin: function( element ) {
			this.lastActive = element;

			// Hide error label and remove error class on focus if enabled
			if ( this.settings.focusCleanup ) {
				if ( this.settings.unhighlight ) {
					this.settings.unhighlight.call( this, element, this.settings.errorClass, this.settings.validClass );
				}
				this.hideThese( this.errorsFor( element ) );
			}
		},
		onfocusout: function( element ) {
			if ( !this.checkable( element ) && ( element.name in this.submitted || !this.optional( element ) ) ) {
				this.element( element );
			}
		},
		onkeyup: function( element, event ) {
			// Avoid revalidate the field when pressing one of the following keys
			// Shift       => 16
			// Ctrl        => 17
			// Alt         => 18
			// Caps lock   => 20
			// End         => 35
			// Home        => 36
			// Left arrow  => 37
			// Up arrow    => 38
			// Right arrow => 39
			// Down arrow  => 40
			// Insert      => 45
			// Num lock    => 144
			// AltGr key   => 225
			var excludedKeys = [
			16, 17, 18, 20, 35, 36, 37,
			38, 39, 40, 45, 144, 225
			];

			if ( event.which === 9 && this.elementValue( element ) === "" || $.inArray( event.keyCode, excludedKeys ) !== -1 ) {
				return;
			} else if ( element.name in this.submitted || element === this.lastElement ) {
				this.element( element );
			}
		},
		onclick: function( element ) {
			// click on selects, radiobuttons and checkboxes
			if ( element.name in this.submitted ) {
				this.element( element );

			// or option elements, check parent select in that case
		} else if ( element.parentNode.name in this.submitted ) {
			this.element( element.parentNode );
		}
	},
	highlight: function( element, errorClass, validClass ) {
		if ( element.type === "radio" ) {
			this.findByName( element.name ).addClass( errorClass ).removeClass( validClass );
		} else {
			$( element ).addClass( errorClass ).removeClass( validClass );
		}
	},
	unhighlight: function( element, errorClass, validClass ) {
		if ( element.type === "radio" ) {
			this.findByName( element.name ).removeClass( errorClass ).addClass( validClass );
		} else {
			$( element ).removeClass( errorClass ).addClass( validClass );
		}
	}
},

	// http://jqueryvalidation.org/jQuery.validator.setDefaults/
	setDefaults: function( settings ) {
		$.extend( $.validator.defaults, settings );
	},

	messages: {
		required: "This field is required.",
		remote: "Please fix this field.",
		email: "Please enter a valid email address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date ( ISO ).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		maxlength: $.validator.format( "Please enter no more than {0} characters." ),
		minlength: $.validator.format( "Please enter at least {0} characters." ),
		rangelength: $.validator.format( "Please enter a value between {0} and {1} characters long." ),
		range: $.validator.format( "Please enter a value between {0} and {1}." ),
		max: $.validator.format( "Please enter a value less than or equal to {0}." ),
		min: $.validator.format( "Please enter a value greater than or equal to {0}." )
	},

	autoCreateRanges: false,

	prototype: {

		init: function() {
			this.labelContainer = $( this.settings.errorLabelContainer );
			this.errorContext = this.labelContainer.length && this.labelContainer || $( this.currentForm );
			this.containers = $( this.settings.errorContainer ).add( this.settings.errorLabelContainer );
			this.submitted = {};
			this.valueCache = {};
			this.pendingRequest = 0;
			this.pending = {};
			this.invalid = {};
			this.reset();

			var groups = ( this.groups = {} ),
			rules;
			$.each( this.settings.groups, function( key, value ) {
				if ( typeof value === "string" ) {
					value = value.split( /\s/ );
				}
				$.each( value, function( index, name ) {
					groups[ name ] = key;
				});
			});
			rules = this.settings.rules;
			$.each( rules, function( key, value ) {
				rules[ key ] = $.validator.normalizeRule( value );
			});

			function delegate( event ) {
				var validator = $.data( this.form, "validator" ),
				eventType = "on" + event.type.replace( /^validate/, "" ),
				settings = validator.settings;
				if ( settings[ eventType ] && !$( this ).is( settings.ignore ) ) {
					settings[ eventType ].call( validator, this, event );
				}
			}

			$( this.currentForm )
			.on( "focusin.validate focusout.validate keyup.validate",
				":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'], " +
				"[type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], " +
				"[type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], " +
				"[type='radio'], [type='checkbox']", delegate)
				// Support: Chrome, oldIE
				// "select" is provided as event.target when clicking a option
				.on("click.validate", "select, option, [type='radio'], [type='checkbox']", delegate);

				if ( this.settings.invalidHandler ) {
					$( this.currentForm ).on( "invalid-form.validate", this.settings.invalidHandler );
				}

			// Add aria-required to any Static/Data/Class required fields before first validation
			// Screen readers require this attribute to be present before the initial submission http://www.w3.org/TR/WCAG-TECHS/ARIA2.html
			$( this.currentForm ).find( "[required], [data-rule-required], .required" ).attr( "aria-required", "true" );
		},

		// http://jqueryvalidation.org/Validator.form/
		form: function() {
			this.checkForm();
			$.extend( this.submitted, this.errorMap );
			this.invalid = $.extend({}, this.errorMap );
			if ( !this.valid() ) {
				$( this.currentForm ).triggerHandler( "invalid-form", [ this ]);
			}
			this.showErrors();
			return this.valid();
		},

		checkForm: function() {
			this.prepareForm();
			for ( var i = 0, elements = ( this.currentElements = this.elements() ); elements[ i ]; i++ ) {
				this.check( elements[ i ] );
			}
			return this.valid();
		},

		// http://jqueryvalidation.org/Validator.element/
		element: function( element ) {
			var cleanElement = this.clean( element ),
			checkElement = this.validationTargetFor( cleanElement ),
			result = true;

			this.lastElement = checkElement;

			if ( checkElement === undefined ) {
				delete this.invalid[ cleanElement.name ];
			} else {
				this.prepareElement( checkElement );
				this.currentElements = $( checkElement );

				result = this.check( checkElement ) !== false;
				if ( result ) {
					delete this.invalid[ checkElement.name ];
				} else {
					this.invalid[ checkElement.name ] = true;
				}
			}
			// Add aria-invalid status for screen readers
			$( element ).attr( "aria-invalid", !result );

			if ( !this.numberOfInvalids() ) {
				// Hide error containers on last error
				this.toHide = this.toHide.add( this.containers );
			}
			this.showErrors();
			return result;
		},

		// http://jqueryvalidation.org/Validator.showErrors/
		showErrors: function( errors ) {
			if ( errors ) {
				// add items to error list and map
				$.extend( this.errorMap, errors );
				this.errorList = [];
				for ( var name in errors ) {
					this.errorList.push({
						message: errors[ name ],
						element: this.findByName( name )[ 0 ]
					});
				}
				// remove items from success list
				this.successList = $.grep( this.successList, function( element ) {
					return !( element.name in errors );
				});
			}
			if ( this.settings.showErrors ) {
				this.settings.showErrors.call( this, this.errorMap, this.errorList );
			} else {
				this.defaultShowErrors();
			}
		},

		// http://jqueryvalidation.org/Validator.resetForm/
		resetForm: function() {
			if ( $.fn.resetForm ) {
				$( this.currentForm ).resetForm();
			}
			this.submitted = {};
			this.lastElement = null;
			this.prepareForm();
			this.hideErrors();
			var i, elements = this.elements()
			.removeData( "previousValue" )
			.removeAttr( "aria-invalid" );

			if ( this.settings.unhighlight ) {
				for ( i = 0; elements[ i ]; i++ ) {
					this.settings.unhighlight.call( this, elements[ i ],
						this.settings.errorClass, "" );
				}
			} else {
				elements.removeClass( this.settings.errorClass );
			}
		},

		numberOfInvalids: function() {
			return this.objectLength( this.invalid );
		},

		objectLength: function( obj ) {
			/* jshint unused: false */
			var count = 0,
			i;
			for ( i in obj ) {
				count++;
			}
			return count;
		},

		hideErrors: function() {
			this.hideThese( this.toHide );
		},

		hideThese: function( errors ) {
			errors.not( this.containers ).text( "" );
			this.addWrapper( errors ).hide();
		},

		valid: function() {
			return this.size() === 0;
		},

		size: function() {
			return this.errorList.length;
		},

		focusInvalid: function() {
			if ( this.settings.focusInvalid ) {
				try {
					$( this.findLastActive() || this.errorList.length && this.errorList[ 0 ].element || [])
					.filter( ":visible" )
					.focus()
					// manually trigger focusin event; without it, focusin handler isn't called, findLastActive won't have anything to find
					.trigger( "focusin" );
				} catch ( e ) {
					// ignore IE throwing errors when focusing hidden elements
				}
			}
		},

		findLastActive: function() {
			var lastActive = this.lastActive;
			return lastActive && $.grep( this.errorList, function( n ) {
				return n.element.name === lastActive.name;
			}).length === 1 && lastActive;
		},

		elements: function() {
			var validator = this,
			rulesCache = {};

			// select all valid inputs inside the form (no submit or reset buttons)
			return $( this.currentForm )
			.find( "input, select, textarea" )
			.not( ":submit, :reset, :image, :disabled" )
			.not( this.settings.ignore )
			.filter( function() {
				if ( !this.name && validator.settings.debug && window.console ) {
					console.error( "%o has no name assigned", this );
				}

				// select only the first element for each name, and only those with rules specified
				if ( this.name in rulesCache || !validator.objectLength( $( this ).rules() ) ) {
					return false;
				}

				rulesCache[ this.name ] = true;
				return true;
			});
		},

		clean: function( selector ) {
			return $( selector )[ 0 ];
		},

		errors: function() {
			var errorClass = this.settings.errorClass.split( " " ).join( "." );
			return $( this.settings.errorElement + "." + errorClass, this.errorContext );
		},

		reset: function() {
			this.successList = [];
			this.errorList = [];
			this.errorMap = {};
			this.toShow = $( [] );
			this.toHide = $( [] );
			this.currentElements = $( [] );
		},

		prepareForm: function() {
			this.reset();
			this.toHide = this.errors().add( this.containers );
		},

		prepareElement: function( element ) {
			this.reset();
			this.toHide = this.errorsFor( element );
		},

		elementValue: function( element ) {
			var val,
			$element = $( element ),
			type = element.type;

			if ( type === "radio" || type === "checkbox" ) {
				return this.findByName( element.name ).filter(":checked").val();
			} else if ( type === "number" && typeof element.validity !== "undefined" ) {
				return element.validity.badInput ? false : $element.val();
			}

			val = $element.val();
			if ( typeof val === "string" ) {
				return val.replace(/\r/g, "" );
			}
			return val;
		},

		check: function( element ) {
			element = this.validationTargetFor( this.clean( element ) );

			var rules = $( element ).rules(),
			rulesCount = $.map( rules, function( n, i ) {
				return i;
			}).length,
			dependencyMismatch = false,
			val = this.elementValue( element ),
			result, method, rule;

			for ( method in rules ) {
				rule = { method: method, parameters: rules[ method ] };
				try {

					result = $.validator.methods[ method ].call( this, val, element, rule.parameters );

					// if a method indicates that the field is optional and therefore valid,
					// don't mark it as valid when there are no other rules
					if ( result === "dependency-mismatch" && rulesCount === 1 ) {
						dependencyMismatch = true;
						continue;
					}
					dependencyMismatch = false;

					if ( result === "pending" ) {
						this.toHide = this.toHide.not( this.errorsFor( element ) );
						return;
					}

					if ( !result ) {
						this.formatAndAdd( element, rule );
						return false;
					}
				} catch ( e ) {
					if ( this.settings.debug && window.console ) {
						console.log( "Exception occurred when checking element " + element.id + ", check the '" + rule.method + "' method.", e );
					}
					if ( e instanceof TypeError ) {
						e.message += ".  Exception occurred when checking element " + element.id + ", check the '" + rule.method + "' method.";
					}

					throw e;
				}
			}
			if ( dependencyMismatch ) {
				return;
			}
			if ( this.objectLength( rules ) ) {
				this.successList.push( element );
			}
			return true;
		},

		// return the custom message for the given element and validation method
		// specified in the element's HTML5 data attribute
		// return the generic message if present and no method specific message is present
		customDataMessage: function( element, method ) {
			return $( element ).data( "msg" + method.charAt( 0 ).toUpperCase() +
				method.substring( 1 ).toLowerCase() ) || $( element ).data( "msg" );
		},

		// return the custom message for the given element name and validation method
		customMessage: function( name, method ) {
			var m = this.settings.messages[ name ];
			return m && ( m.constructor === String ? m : m[ method ]);
		},

		// return the first defined argument, allowing empty strings
		findDefined: function() {
			for ( var i = 0; i < arguments.length; i++) {
				if ( arguments[ i ] !== undefined ) {
					return arguments[ i ];
				}
			}
			return undefined;
		},

		defaultMessage: function( element, method ) {
			return this.findDefined(
				this.customMessage( element.name, method ),
				this.customDataMessage( element, method ),
				// title is never undefined, so handle empty string as undefined
				!this.settings.ignoreTitle && element.title || undefined,
				$.validator.messages[ method ],
				"<strong>Warning: No message defined for " + element.name + "</strong>"
				);
		},

		formatAndAdd: function( element, rule ) {
			var message = this.defaultMessage( element, rule.method ),
			theregex = /\$?\{(\d+)\}/g;
			if ( typeof message === "function" ) {
				message = message.call( this, rule.parameters, element );
			} else if ( theregex.test( message ) ) {
				message = $.validator.format( message.replace( theregex, "{$1}" ), rule.parameters );
			}
			this.errorList.push({
				message: message,
				element: element,
				method: rule.method
			});

			this.errorMap[ element.name ] = message;
			this.submitted[ element.name ] = message;
		},

		addWrapper: function( toToggle ) {
			if ( this.settings.wrapper ) {
				toToggle = toToggle.add( toToggle.parent( this.settings.wrapper ) );
			}
			return toToggle;
		},

		defaultShowErrors: function() {
			var i, elements, error;
			for ( i = 0; this.errorList[ i ]; i++ ) {
				error = this.errorList[ i ];
				if ( this.settings.highlight ) {
					this.settings.highlight.call( this, error.element, this.settings.errorClass, this.settings.validClass );
				}
				this.showLabel( error.element, error.message );
			}
			if ( this.errorList.length ) {
				this.toShow = this.toShow.add( this.containers );
			}
			if ( this.settings.success ) {
				for ( i = 0; this.successList[ i ]; i++ ) {
					this.showLabel( this.successList[ i ] );
				}
			}
			if ( this.settings.unhighlight ) {
				for ( i = 0, elements = this.validElements(); elements[ i ]; i++ ) {
					this.settings.unhighlight.call( this, elements[ i ], this.settings.errorClass, this.settings.validClass );
				}
			}
			this.toHide = this.toHide.not( this.toShow );
			this.hideErrors();
			this.addWrapper( this.toShow ).show();
		},

		validElements: function() {
			return this.currentElements.not( this.invalidElements() );
		},

		invalidElements: function() {
			return $( this.errorList ).map(function() {
				return this.element;
			});
		},

		showLabel: function( element, message ) {
			var place, group, errorID,
			error = this.errorsFor( element ),
			elementID = this.idOrName( element ),
			describedBy = $( element ).attr( "aria-describedby" );
			if ( error.length ) {
				// refresh error/success class
				error.removeClass( this.settings.validClass ).addClass( this.settings.errorClass );
				// replace message on existing label
				error.html( message );
			} else {
				// create error element
				error = $( "<" + this.settings.errorElement + ">" )
				.attr( "id", elementID + "-error" )
				.addClass( this.settings.errorClass )
				.html( message || "" );

				// Maintain reference to the element to be placed into the DOM
				place = error;
				if ( this.settings.wrapper ) {
					// make sure the element is visible, even in IE
					// actually showing the wrapped element is handled elsewhere
					place = error.hide().show().wrap( "<" + this.settings.wrapper + "/>" ).parent();
				}
				if ( this.labelContainer.length ) {
					this.labelContainer.append( place );
				} else if ( this.settings.errorPlacement ) {
					this.settings.errorPlacement( place, $( element ) );
				} else {
					place.insertAfter( element );
				}

				// Link error back to the element
				if ( error.is( "label" ) ) {
					// If the error is a label, then associate using 'for'
					error.attr( "for", elementID );
				} else if ( error.parents( "label[for='" + elementID + "']" ).length === 0 ) {
					// If the element is not a child of an associated label, then it's necessary
					// to explicitly apply aria-describedby

					errorID = error.attr( "id" ).replace( /(:|\.|\[|\]|\$)/g, "\\$1");
					// Respect existing non-error aria-describedby
					if ( !describedBy ) {
						describedBy = errorID;
					} else if ( !describedBy.match( new RegExp( "\\b" + errorID + "\\b" ) ) ) {
						// Add to end of list if not already present
						describedBy += " " + errorID;
					}
					$( element ).attr( "aria-describedby", describedBy );

					// If this element is grouped, then assign to all elements in the same group
					group = this.groups[ element.name ];
					if ( group ) {
						$.each( this.groups, function( name, testgroup ) {
							if ( testgroup === group ) {
								$( "[name='" + name + "']", this.currentForm )
								.attr( "aria-describedby", error.attr( "id" ) );
							}
						});
					}
				}
			}
			if ( !message && this.settings.success ) {
				error.text( "" );
				if ( typeof this.settings.success === "string" ) {
					error.addClass( this.settings.success );
				} else {
					this.settings.success( error, element );
				}
			}
			this.toShow = this.toShow.add( error );
		},

		errorsFor: function( element ) {
			var name = this.idOrName( element ),
			describer = $( element ).attr( "aria-describedby" ),
			selector = "label[for='" + name + "'], label[for='" + name + "'] *";

			// aria-describedby should directly reference the error element
			if ( describer ) {
				selector = selector + ", #" + describer.replace( /\s+/g, ", #" );
			}
			return this
			.errors()
			.filter( selector );
		},

		idOrName: function( element ) {
			return this.groups[ element.name ] || ( this.checkable( element ) ? element.name : element.id || element.name );
		},

		validationTargetFor: function( element ) {

			// If radio/checkbox, validate first element in group instead
			if ( this.checkable( element ) ) {
				element = this.findByName( element.name );
			}

			// Always apply ignore filter
			return $( element ).not( this.settings.ignore )[ 0 ];
		},

		checkable: function( element ) {
			return ( /radio|checkbox/i ).test( element.type );
		},

		findByName: function( name ) {
			return $( this.currentForm ).find( "[name='" + name + "']" );
		},

		getLength: function( value, element ) {
			switch ( element.nodeName.toLowerCase() ) {
				case "select":
				return $( "option:selected", element ).length;
				case "input":
				if ( this.checkable( element ) ) {
					return this.findByName( element.name ).filter( ":checked" ).length;
				}
			}
			return value.length;
		},

		depend: function( param, element ) {
			return this.dependTypes[typeof param] ? this.dependTypes[typeof param]( param, element ) : true;
		},

		dependTypes: {
			"boolean": function( param ) {
				return param;
			},
			"string": function( param, element ) {
				return !!$( param, element.form ).length;
			},
			"function": function( param, element ) {
				return param( element );
			}
		},

		optional: function( element ) {
			var val = this.elementValue( element );
			return !$.validator.methods.required.call( this, val, element ) && "dependency-mismatch";
		},

		startRequest: function( element ) {
			if ( !this.pending[ element.name ] ) {
				this.pendingRequest++;
				this.pending[ element.name ] = true;
			}
		},

		stopRequest: function( element, valid ) {
			this.pendingRequest--;
			// sometimes synchronization fails, make sure pendingRequest is never < 0
			if ( this.pendingRequest < 0 ) {
				this.pendingRequest = 0;
			}
			delete this.pending[ element.name ];
			if ( valid && this.pendingRequest === 0 && this.formSubmitted && this.form() ) {
				$( this.currentForm ).submit();
				this.formSubmitted = false;
			} else if (!valid && this.pendingRequest === 0 && this.formSubmitted ) {
				$( this.currentForm ).triggerHandler( "invalid-form", [ this ]);
				this.formSubmitted = false;
			}
		},

		previousValue: function( element ) {
			return $.data( element, "previousValue" ) || $.data( element, "previousValue", {
				old: null,
				valid: true,
				message: this.defaultMessage( element, "remote" )
			});
		},

		// cleans up all forms and elements, removes validator-specific events
		destroy: function() {
			this.resetForm();

			$( this.currentForm )
			.off( ".validate" )
			.removeData( "validator" );
		}

	},

	classRuleSettings: {
		required: { required: true },
		email: { email: true },
		url: { url: true },
		date: { date: true },
		dateISO: { dateISO: true },
		number: { number: true },
		digits: { digits: true },
		creditcard: { creditcard: true }
	},

	addClassRules: function( className, rules ) {
		if ( className.constructor === String ) {
			this.classRuleSettings[ className ] = rules;
		} else {
			$.extend( this.classRuleSettings, className );
		}
	},

	classRules: function( element ) {
		var rules = {},
		classes = $( element ).attr( "class" );

		if ( classes ) {
			$.each( classes.split( " " ), function() {
				if ( this in $.validator.classRuleSettings ) {
					$.extend( rules, $.validator.classRuleSettings[ this ]);
				}
			});
		}
		return rules;
	},

	normalizeAttributeRule: function( rules, type, method, value ) {

		// convert the value to a number for number inputs, and for text for backwards compability
		// allows type="date" and others to be compared as strings
		if ( /min|max/.test( method ) && ( type === null || /number|range|text/.test( type ) ) ) {
			value = Number( value );

			// Support Opera Mini, which returns NaN for undefined minlength
			if ( isNaN( value ) ) {
				value = undefined;
			}
		}

		if ( value || value === 0 ) {
			rules[ method ] = value;
		} else if ( type === method && type !== "range" ) {

			// exception: the jquery validate 'range' method
			// does not test for the html5 'range' type
			rules[ method ] = true;
		}
	},

	attributeRules: function( element ) {
		var rules = {},
		$element = $( element ),
		type = element.getAttribute( "type" ),
		method, value;

		for ( method in $.validator.methods ) {

			// support for <input required> in both html5 and older browsers
			if ( method === "required" ) {
				value = element.getAttribute( method );

				// Some browsers return an empty string for the required attribute
				// and non-HTML5 browsers might have required="" markup
				if ( value === "" ) {
					value = true;
				}

				// force non-HTML5 browsers to return bool
				value = !!value;
			} else {
				value = $element.attr( method );
			}

			this.normalizeAttributeRule( rules, type, method, value );
		}

		// maxlength may be returned as -1, 2147483647 ( IE ) and 524288 ( safari ) for text inputs
		if ( rules.maxlength && /-1|2147483647|524288/.test( rules.maxlength ) ) {
			delete rules.maxlength;
		}

		return rules;
	},

	dataRules: function( element ) {
		var rules = {},
		$element = $( element ),
		type = element.getAttribute( "type" ),
		method, value;

		for ( method in $.validator.methods ) {
			value = $element.data( "rule" + method.charAt( 0 ).toUpperCase() + method.substring( 1 ).toLowerCase() );
			this.normalizeAttributeRule( rules, type, method, value );
		}
		return rules;
	},

	staticRules: function( element ) {
		var rules = {},
		validator = $.data( element.form, "validator" );

		if ( validator.settings.rules ) {
			rules = $.validator.normalizeRule( validator.settings.rules[ element.name ] ) || {};
		}
		return rules;
	},

	normalizeRules: function( rules, element ) {
		// handle dependency check
		$.each( rules, function( prop, val ) {
			// ignore rule when param is explicitly false, eg. required:false
			if ( val === false ) {
				delete rules[ prop ];
				return;
			}
			if ( val.param || val.depends ) {
				var keepRule = true;
				switch ( typeof val.depends ) {
					case "string":
					keepRule = !!$( val.depends, element.form ).length;
					break;
					case "function":
					keepRule = val.depends.call( element, element );
					break;
				}
				if ( keepRule ) {
					rules[ prop ] = val.param !== undefined ? val.param : true;
				} else {
					delete rules[ prop ];
				}
			}
		});

		// evaluate parameters
		$.each( rules, function( rule, parameter ) {
			rules[ rule ] = $.isFunction( parameter ) ? parameter( element ) : parameter;
		});

		// clean number parameters
		$.each([ "minlength", "maxlength" ], function() {
			if ( rules[ this ] ) {
				rules[ this ] = Number( rules[ this ] );
			}
		});
		$.each([ "rangelength", "range" ], function() {
			var parts;
			if ( rules[ this ] ) {
				if ( $.isArray( rules[ this ] ) ) {
					rules[ this ] = [ Number( rules[ this ][ 0 ]), Number( rules[ this ][ 1 ] ) ];
				} else if ( typeof rules[ this ] === "string" ) {
					parts = rules[ this ].replace(/[\[\]]/g, "" ).split( /[\s,]+/ );
					rules[ this ] = [ Number( parts[ 0 ]), Number( parts[ 1 ] ) ];
				}
			}
		});

		if ( $.validator.autoCreateRanges ) {
			// auto-create ranges
			if ( rules.min != null && rules.max != null ) {
				rules.range = [ rules.min, rules.max ];
				delete rules.min;
				delete rules.max;
			}
			if ( rules.minlength != null && rules.maxlength != null ) {
				rules.rangelength = [ rules.minlength, rules.maxlength ];
				delete rules.minlength;
				delete rules.maxlength;
			}
		}

		return rules;
	},

	// Converts a simple string to a {string: true} rule, e.g., "required" to {required:true}
	normalizeRule: function( data ) {
		if ( typeof data === "string" ) {
			var transformed = {};
			$.each( data.split( /\s/ ), function() {
				transformed[ this ] = true;
			});
			data = transformed;
		}
		return data;
	},

	// http://jqueryvalidation.org/jQuery.validator.addMethod/
	addMethod: function( name, method, message ) {
		$.validator.methods[ name ] = method;
		$.validator.messages[ name ] = message !== undefined ? message : $.validator.messages[ name ];
		if ( method.length < 3 ) {
			$.validator.addClassRules( name, $.validator.normalizeRule( name ) );
		}
	},

	methods: {

		// http://jqueryvalidation.org/required-method/
		required: function( value, element, param ) {
			// check if dependency is met
			if ( !this.depend( param, element ) ) {
				return "dependency-mismatch";
			}
			if ( element.nodeName.toLowerCase() === "select" ) {
				// could be an array for select-multiple or a string, both are fine this way
				var val = $( element ).val();
				return val && val.length > 0;
			}
			if ( this.checkable( element ) ) {
				return this.getLength( value, element ) > 0;
			}
			return value.length > 0;
		},

		// http://jqueryvalidation.org/email-method/
		email: function( value, element ) {
			// From https://html.spec.whatwg.org/multipage/forms.html#valid-e-mail-address
			// Retrieved 2014-01-14
			// If you have a problem with this implementation, report a bug against the above spec
			// Or use custom methods to implement your own email validation
			return this.optional( element ) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test( value );
		},

		// http://jqueryvalidation.org/url-method/
		url: function( value, element ) {

			// Copyright (c) 2010-2013 Diego Perini, MIT licensed
			// https://gist.github.com/dperini/729294
			// see also https://mathiasbynens.be/demo/url-regex
			// modified to allow protocol-relative URLs
			return this.optional( element ) || /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test( value );
		},

		// http://jqueryvalidation.org/date-method/
		date: function( value, element ) {
			return this.optional( element ) || !/Invalid|NaN/.test( new Date( value ).toString() );
		},

		// http://jqueryvalidation.org/dateISO-method/
		dateISO: function( value, element ) {
			return this.optional( element ) || /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test( value );
		},

		// http://jqueryvalidation.org/number-method/
		number: function( value, element ) {
			return this.optional( element ) || /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test( value );
		},

		// http://jqueryvalidation.org/digits-method/
		digits: function( value, element ) {
			return this.optional( element ) || /^\d+$/.test( value );
		},

		// http://jqueryvalidation.org/creditcard-method/
		// based on http://en.wikipedia.org/wiki/Luhn_algorithm
		creditcard: function( value, element ) {
			if ( this.optional( element ) ) {
				return "dependency-mismatch";
			}
			// accept only spaces, digits and dashes
			if ( /[^0-9 \-]+/.test( value ) ) {
				return false;
			}
			var nCheck = 0,
			nDigit = 0,
			bEven = false,
			n, cDigit;

			value = value.replace( /\D/g, "" );

			// Basing min and max length on
			// http://developer.ean.com/general_info/Valid_Credit_Card_Types
			if ( value.length < 13 || value.length > 19 ) {
				return false;
			}

			for ( n = value.length - 1; n >= 0; n--) {
				cDigit = value.charAt( n );
				nDigit = parseInt( cDigit, 10 );
				if ( bEven ) {
					if ( ( nDigit *= 2 ) > 9 ) {
						nDigit -= 9;
					}
				}
				nCheck += nDigit;
				bEven = !bEven;
			}

			return ( nCheck % 10 ) === 0;
		},

		// http://jqueryvalidation.org/minlength-method/
		minlength: function( value, element, param ) {
			var length = $.isArray( value ) ? value.length : this.getLength( value, element );
			return this.optional( element ) || length >= param;
		},

		// http://jqueryvalidation.org/maxlength-method/
		maxlength: function( value, element, param ) {
			var length = $.isArray( value ) ? value.length : this.getLength( value, element );
			return this.optional( element ) || length <= param;
		},

		// http://jqueryvalidation.org/rangelength-method/
		rangelength: function( value, element, param ) {
			var length = $.isArray( value ) ? value.length : this.getLength( value, element );
			return this.optional( element ) || ( length >= param[ 0 ] && length <= param[ 1 ] );
		},

		// http://jqueryvalidation.org/min-method/
		min: function( value, element, param ) {
			return this.optional( element ) || value >= param;
		},

		// http://jqueryvalidation.org/max-method/
		max: function( value, element, param ) {
			return this.optional( element ) || value <= param;
		},

		// http://jqueryvalidation.org/range-method/
		range: function( value, element, param ) {
			return this.optional( element ) || ( value >= param[ 0 ] && value <= param[ 1 ] );
		},

		// http://jqueryvalidation.org/equalTo-method/
		equalTo: function( value, element, param ) {
			// bind to the blur event of the target in order to revalidate whenever the target field is updated
			// TODO find a way to bind the event just once, avoiding the unbind-rebind overhead
			var target = $( param );
			if ( this.settings.onfocusout ) {
				target.off( ".validate-equalTo" ).on( "blur.validate-equalTo", function() {
					$( element ).valid();
				});
			}
			return value === target.val();
		},

		// http://jqueryvalidation.org/remote-method/
		remote: function( value, element, param ) {
			if ( this.optional( element ) ) {
				return "dependency-mismatch";
			}

			var previous = this.previousValue( element ),
			validator, data;

			if (!this.settings.messages[ element.name ] ) {
				this.settings.messages[ element.name ] = {};
			}
			previous.originalMessage = this.settings.messages[ element.name ].remote;
			this.settings.messages[ element.name ].remote = previous.message;

			param = typeof param === "string" && { url: param } || param;

			if ( previous.old === value ) {
				return previous.valid;
			}

			previous.old = value;
			validator = this;
			this.startRequest( element );
			data = {};
			data[ element.name ] = value;
			$.ajax( $.extend( true, {
				mode: "abort",
				port: "validate" + element.name,
				dataType: "json",
				data: data,
				context: validator.currentForm,
				success: function( response ) {
					var valid = response === true || response === "true",
					errors, message, submitted;

					validator.settings.messages[ element.name ].remote = previous.originalMessage;
					if ( valid ) {
						submitted = validator.formSubmitted;
						validator.prepareElement( element );
						validator.formSubmitted = submitted;
						validator.successList.push( element );
						delete validator.invalid[ element.name ];
						validator.showErrors();
					} else {
						errors = {};
						message = response || validator.defaultMessage( element, "remote" );
						errors[ element.name ] = previous.message = $.isFunction( message ) ? message( value ) : message;
						validator.invalid[ element.name ] = true;
						validator.showErrors( errors );
					}
					previous.valid = valid;
					validator.stopRequest( element, valid );
				}
			}, param ) );
return "pending";
}
}

});

// ajax mode: abort
// usage: $.ajax({ mode: "abort"[, port: "uniqueport"]});
// if mode:"abort" is used, the previous request on that port (port can be undefined) is aborted via XMLHttpRequest.abort()

var pendingRequests = {},
ajax;
// Use a prefilter if available (1.5+)
if ( $.ajaxPrefilter ) {
	$.ajaxPrefilter(function( settings, _, xhr ) {
		var port = settings.port;
		if ( settings.mode === "abort" ) {
			if ( pendingRequests[port] ) {
				pendingRequests[port].abort();
			}
			pendingRequests[port] = xhr;
		}
	});
} else {
	// Proxy ajax
	ajax = $.ajax;
	$.ajax = function( settings ) {
		var mode = ( "mode" in settings ? settings : $.ajaxSettings ).mode,
		port = ( "port" in settings ? settings : $.ajaxSettings ).port;
		if ( mode === "abort" ) {
			if ( pendingRequests[port] ) {
				pendingRequests[port].abort();
			}
			pendingRequests[port] = ajax.apply(this, arguments);
			return pendingRequests[port];
		}
		return ajax.apply(this, arguments);
	};
}

}));
/*!
 * fancyBox - jQuery Plugin
 * version: 2.1.5 (Fri, 14 Jun 2013)
 * @requires jQuery v1.6 or later
 *
 * Examples at http://fancyapps.com/fancybox/
 * License: www.fancyapps.com/fancybox/#license
 *
 * Copyright 2012 Janis Skarnelis - janis@fancyapps.com
 *
 */

 (function (window, document, $, undefined) {
 	"use strict";

 	var H = $("html"),
 	W = $(window),
 	D = $(document),
 	F = $.fancybox = function () {
 		F.open.apply( this, arguments );
 	},
 	IE =  navigator.userAgent.match(/msie/i),
 	didUpdate	= null,
 	isTouch		= document.createTouch !== undefined,

 	isQuery	= function(obj) {
 		return obj && obj.hasOwnProperty && obj instanceof $;
 	},
 	isString = function(str) {
 		return str && $.type(str) === "string";
 	},
 	isPercentage = function(str) {
 		return isString(str) && str.indexOf('%') > 0;
 	},
 	isScrollable = function(el) {
 		return (el && !(el.style.overflow && el.style.overflow === 'hidden') && ((el.clientWidth && el.scrollWidth > el.clientWidth) || (el.clientHeight && el.scrollHeight > el.clientHeight)));
 	},
 	getScalar = function(orig, dim) {
 		var value = parseInt(orig, 10) || 0;

 		if (dim && isPercentage(orig)) {
 			value = F.getViewport()[ dim ] / 100 * value;
 		}

 		return Math.ceil(value);
 	},
 	getValue = function(value, dim) {
 		return getScalar(value, dim) + 'px';
 	};

 	$.extend(F, {
		// The current version of fancyBox
		version: '2.1.5',

		defaults: {
			padding : 15,
			margin  : 20,

			width     : 800,
			height    : 600,
			minWidth  : 100,
			minHeight : 100,
			maxWidth  : 9999,
			maxHeight : 9999,
			pixelRatio: 1, // Set to 2 for retina display support

			autoSize   : true,
			autoHeight : false,
			autoWidth  : false,

			autoResize  : true,
			autoCenter  : !isTouch,
			fitToView   : true,
			aspectRatio : false,
			topRatio    : 0.5,
			leftRatio   : 0.5,

			scrolling : 'auto', // 'auto', 'yes' or 'no'
			wrapCSS   : '',

			arrows     : true,
			closeBtn   : true,
			closeClick : false,
			nextClick  : false,
			mouseWheel : true,
			autoPlay   : false,
			playSpeed  : 3000,
			preload    : 3,
			modal      : false,
			loop       : true,

			ajax  : {
				dataType : 'html',
				headers  : { 'X-fancyBox': true }
			},
			iframe : {
				scrolling : 'auto',
				preload   : true
			},
			swf : {
				wmode: 'transparent',
				allowfullscreen   : 'true',
				allowscriptaccess : 'always'
			},

			keys  : {
				next : {
					13 : 'left', // enter
					34 : 'up',   // page down
					39 : 'left', // right arrow
					40 : 'up'    // down arrow
				},
				prev : {
					8  : 'right',  // backspace
					33 : 'down',   // page up
					37 : 'right',  // left arrow
					38 : 'down'    // up arrow
				},
				close  : [27], // escape key
				play   : [32], // space - start/stop slideshow
				toggle : [70]  // letter "f" - toggle fullscreen
			},

			direction : {
				next : 'left',
				prev : 'right'
			},

			scrollOutside  : true,

			// Override some properties
			index   : 0,
			type    : null,
			href    : null,
			content : null,
			title   : null,

			// HTML templates
			tpl: {
				wrap     : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
				image    : '<img class="fancybox-image" src="{href}" alt="" />',
				iframe   : '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen' + (IE ? ' allowtransparency="true"' : '') + '></iframe>',
				error    : '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
				closeBtn : '<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"></a>',
				next     : '<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
				prev     : '<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
			},

			// Properties for each animation type
			// Opening fancyBox
			openEffect  : 'fade', // 'elastic', 'fade' or 'none'
			openSpeed   : 250,
			openEasing  : 'swing',
			openOpacity : true,
			openMethod  : 'zoomIn',

			// Closing fancyBox
			closeEffect  : 'fade', // 'elastic', 'fade' or 'none'
			closeSpeed   : 250,
			closeEasing  : 'swing',
			closeOpacity : true,
			closeMethod  : 'zoomOut',

			// Changing next gallery item
			nextEffect : 'elastic', // 'elastic', 'fade' or 'none'
			nextSpeed  : 250,
			nextEasing : 'swing',
			nextMethod : 'changeIn',

			// Changing previous gallery item
			prevEffect : 'elastic', // 'elastic', 'fade' or 'none'
			prevSpeed  : 250,
			prevEasing : 'swing',
			prevMethod : 'changeOut',

			// Enable default helpers
			helpers : {
				overlay : true,
				title   : true
			},

			// Callbacks
			onCancel     : $.noop, // If canceling
			beforeLoad   : $.noop, // Before loading
			afterLoad    : $.noop, // After loading
			beforeShow   : $.noop, // Before changing in current item
			afterShow    : $.noop, // After opening
			beforeChange : $.noop, // Before changing gallery item
			beforeClose  : $.noop, // Before closing
			afterClose   : $.noop  // After closing
		},

		//Current state
		group    : {}, // Selected group
		opts     : {}, // Group options
		previous : null,  // Previous element
		coming   : null,  // Element being loaded
		current  : null,  // Currently loaded element
		isActive : false, // Is activated
		isOpen   : false, // Is currently open
		isOpened : false, // Have been fully opened at least once

		wrap  : null,
		skin  : null,
		outer : null,
		inner : null,

		player : {
			timer    : null,
			isActive : false
		},

		// Loaders
		ajaxLoad   : null,
		imgPreload : null,

		// Some collections
		transitions : {},
		helpers     : {},

		/*
		 *	Static methods
		 */

		 open: function (group, opts) {
		 	if (!group) {
		 		return;
		 	}

		 	if (!$.isPlainObject(opts)) {
		 		opts = {};
		 	}

			// Close if already active
			if (false === F.close(true)) {
				return;
			}

			// Normalize group
			if (!$.isArray(group)) {
				group = isQuery(group) ? $(group).get() : [group];
			}

			// Recheck if the type of each element is `object` and set content type (image, ajax, etc)
			$.each(group, function(i, element) {
				var obj = {},
				href,
				title,
				content,
				type,
				rez,
				hrefParts,
				selector;

				if ($.type(element) === "object") {
					// Check if is DOM element
					if (element.nodeType) {
						element = $(element);
					}

					if (isQuery(element)) {
						obj = {
							href    : element.data('fancybox-href') || element.attr('href'),
							title   : element.data('fancybox-title') || element.attr('title'),
							isDom   : true,
							element : element
						};

						if ($.metadata) {
							$.extend(true, obj, element.metadata());
						}

					} else {
						obj = element;
					}
				}

				href  = opts.href  || obj.href || (isString(element) ? element : null);
				title = opts.title !== undefined ? opts.title : obj.title || '';

				content = opts.content || obj.content;
				type    = content ? 'html' : (opts.type  || obj.type);

				if (!type && obj.isDom) {
					type = element.data('fancybox-type');

					if (!type) {
						rez  = element.prop('class').match(/fancybox\.(\w+)/);
						type = rez ? rez[1] : null;
					}
				}

				if (isString(href)) {
					// Try to guess the content type
					if (!type) {
						if (F.isImage(href)) {
							type = 'image';

						} else if (F.isSWF(href)) {
							type = 'swf';

						} else if (href.charAt(0) === '#') {
							type = 'inline';

						} else if (isString(element)) {
							type    = 'html';
							content = element;
						}
					}

					// Split url into two pieces with source url and content selector, e.g,
					// "/mypage.html #my_id" will load "/mypage.html" and display element having id "my_id"
					if (type === 'ajax') {
						hrefParts = href.split(/\s+/, 2);
						href      = hrefParts.shift();
						selector  = hrefParts.shift();
					}
				}

				if (!content) {
					if (type === 'inline') {
						if (href) {
							content = $( isString(href) ? href.replace(/.*(?=#[^\s]+$)/, '') : href ); //strip for ie7

						} else if (obj.isDom) {
							content = element;
						}

					} else if (type === 'html') {
						content = href;

					} else if (!type && !href && obj.isDom) {
						type    = 'inline';
						content = element;
					}
				}

				$.extend(obj, {
					href     : href,
					type     : type,
					content  : content,
					title    : title,
					selector : selector
				});

				group[ i ] = obj;
			});

			// Extend the defaults
			F.opts = $.extend(true, {}, F.defaults, opts);

			// All options are merged recursive except keys
			if (opts.keys !== undefined) {
				F.opts.keys = opts.keys ? $.extend({}, F.defaults.keys, opts.keys) : false;
			}

			F.group = group;

			return F._start(F.opts.index);
		},

		// Cancel image loading or abort ajax request
		cancel: function () {
			var coming = F.coming;

			if (!coming || false === F.trigger('onCancel')) {
				return;
			}

			F.hideLoading();

			if (F.ajaxLoad) {
				F.ajaxLoad.abort();
			}

			F.ajaxLoad = null;

			if (F.imgPreload) {
				F.imgPreload.onload = F.imgPreload.onerror = null;
			}

			if (coming.wrap) {
				coming.wrap.stop(true, true).trigger('onReset').remove();
			}

			F.coming = null;

			// If the first item has been canceled, then clear everything
			if (!F.current) {
				F._afterZoomOut( coming );
			}
		},

		// Start closing animation if is open; remove immediately if opening/closing
		close: function (event) {
			F.cancel();

			if (false === F.trigger('beforeClose')) {
				return;
			}

			F.unbindEvents();

			if (!F.isActive) {
				return;
			}

			if (!F.isOpen || event === true) {
				$('.fancybox-wrap').stop(true).trigger('onReset').remove();

				F._afterZoomOut();

			} else {
				F.isOpen = F.isOpened = false;
				F.isClosing = true;

				$('.fancybox-item, .fancybox-nav').remove();

				F.wrap.stop(true, true).removeClass('fancybox-opened');

				F.transitions[ F.current.closeMethod ]();
			}
		},

		// Manage slideshow:
		//   $.fancybox.play(); - toggle slideshow
		//   $.fancybox.play( true ); - start
		//   $.fancybox.play( false ); - stop
		play: function ( action ) {
			var clear = function () {
				clearTimeout(F.player.timer);
			},
			set = function () {
				clear();

				if (F.current && F.player.isActive) {
					F.player.timer = setTimeout(F.next, F.current.playSpeed);
				}
			},
			stop = function () {
				clear();

				D.unbind('.player');

				F.player.isActive = false;

				F.trigger('onPlayEnd');
			},
			start = function () {
				if (F.current && (F.current.loop || F.current.index < F.group.length - 1)) {
					F.player.isActive = true;

					D.bind({
						'onCancel.player beforeClose.player' : stop,
						'onUpdate.player'   : set,
						'beforeLoad.player' : clear
					});

					set();

					F.trigger('onPlayStart');
				}
			};

			if (action === true || (!F.player.isActive && action !== false)) {
				start();
			} else {
				stop();
			}
		},

		// Navigate to next gallery item
		next: function ( direction ) {
			var current = F.current;

			if (current) {
				if (!isString(direction)) {
					direction = current.direction.next;
				}

				F.jumpto(current.index + 1, direction, 'next');
			}
		},

		// Navigate to previous gallery item
		prev: function ( direction ) {
			var current = F.current;

			if (current) {
				if (!isString(direction)) {
					direction = current.direction.prev;
				}

				F.jumpto(current.index - 1, direction, 'prev');
			}
		},

		// Navigate to gallery item by index
		jumpto: function ( index, direction, router ) {
			var current = F.current;

			if (!current) {
				return;
			}

			index = getScalar(index);

			F.direction = direction || current.direction[ (index >= current.index ? 'next' : 'prev') ];
			F.router    = router || 'jumpto';

			if (current.loop) {
				if (index < 0) {
					index = current.group.length + (index % current.group.length);
				}

				index = index % current.group.length;
			}

			if (current.group[ index ] !== undefined) {
				F.cancel();

				F._start(index);
			}
		},

		// Center inside viewport and toggle position type to fixed or absolute if needed
		reposition: function (e, onlyAbsolute) {
			var current = F.current,
			wrap    = current ? current.wrap : null,
			pos;

			if (wrap) {
				pos = F._getPosition(onlyAbsolute);

				if (e && e.type === 'scroll') {
					delete pos.position;

					wrap.stop(true, true).animate(pos, 200);

				} else {
					wrap.css(pos);

					current.pos = $.extend({}, current.dim, pos);
				}
			}
		},

		update: function (e) {
			var type = (e && e.type),
			anyway = !type || type === 'orientationchange';

			if (anyway) {
				clearTimeout(didUpdate);

				didUpdate = null;
			}

			if (!F.isOpen || didUpdate) {
				return;
			}

			didUpdate = setTimeout(function() {
				var current = F.current;

				if (!current || F.isClosing) {
					return;
				}

				F.wrap.removeClass('fancybox-tmp');

				if (anyway || type === 'load' || (type === 'resize' && current.autoResize)) {
					F._setDimension();
				}

				if (!(type === 'scroll' && current.canShrink)) {
					F.reposition(e);
				}

				F.trigger('onUpdate');

				didUpdate = null;

			}, (anyway && !isTouch ? 0 : 300));
		},

		// Shrink content to fit inside viewport or restore if resized
		toggle: function ( action ) {
			if (F.isOpen) {
				F.current.fitToView = $.type(action) === "boolean" ? action : !F.current.fitToView;

				// Help browser to restore document dimensions
				if (isTouch) {
					F.wrap.removeAttr('style').addClass('fancybox-tmp');

					F.trigger('onUpdate');
				}

				F.update();
			}
		},

		hideLoading: function () {
			D.unbind('.loading');

			$('#fancybox-loading').remove();
		},

		showLoading: function () {
			var el, viewport;

			F.hideLoading();

			el = $('<div id="fancybox-loading"><div></div></div>').click(F.cancel).appendTo('body');

			// If user will press the escape-button, the request will be canceled
			D.bind('keydown.loading', function(e) {
				if ((e.which || e.keyCode) === 27) {
					e.preventDefault();

					F.cancel();
				}
			});

			if (!F.defaults.fixed) {
				viewport = F.getViewport();

				el.css({
					position : 'absolute',
					top  : (viewport.h * 0.5) + viewport.y,
					left : (viewport.w * 0.5) + viewport.x
				});
			}
		},

		getViewport: function () {
			var locked = (F.current && F.current.locked) || false,
			rez    = {
				x: W.scrollLeft(),
				y: W.scrollTop()
			};

			if (locked) {
				rez.w = locked[0].clientWidth;
				rez.h = locked[0].clientHeight;

			} else {
				// See http://bugs.jquery.com/ticket/6724
				rez.w = isTouch && window.innerWidth  ? window.innerWidth  : W.width();
				rez.h = isTouch && window.innerHeight ? window.innerHeight : W.height();
			}

			return rez;
		},

		// Unbind the keyboard / clicking actions
		unbindEvents: function () {
			if (F.wrap && isQuery(F.wrap)) {
				F.wrap.unbind('.fb');
			}

			D.unbind('.fb');
			W.unbind('.fb');
		},

		bindEvents: function () {
			var current = F.current,
			keys;

			if (!current) {
				return;
			}

			// Changing document height on iOS devices triggers a 'resize' event,
			// that can change document height... repeating infinitely
			W.bind('orientationchange.fb' + (isTouch ? '' : ' resize.fb') + (current.autoCenter && !current.locked ? ' scroll.fb' : ''), F.update);

			keys = current.keys;

			if (keys) {
				D.bind('keydown.fb', function (e) {
					var code   = e.which || e.keyCode,
					target = e.target || e.srcElement;

					// Skip esc key if loading, because showLoading will cancel preloading
					if (code === 27 && F.coming) {
						return false;
					}

					// Ignore key combinations and key events within form elements
					if (!e.ctrlKey && !e.altKey && !e.shiftKey && !e.metaKey && !(target && (target.type || $(target).is('[contenteditable]')))) {
						$.each(keys, function(i, val) {
							if (current.group.length > 1 && val[ code ] !== undefined) {
								F[ i ]( val[ code ] );

								e.preventDefault();
								return false;
							}

							if ($.inArray(code, val) > -1) {
								F[ i ] ();

								e.preventDefault();
								return false;
							}
						});
					}
				});
			}

			if ($.fn.mousewheel && current.mouseWheel) {
				F.wrap.bind('mousewheel.fb', function (e, delta, deltaX, deltaY) {
					var target = e.target || null,
					parent = $(target),
					canScroll = false;

					while (parent.length) {
						if (canScroll || parent.is('.fancybox-skin') || parent.is('.fancybox-wrap')) {
							break;
						}

						canScroll = isScrollable( parent[0] );
						parent    = $(parent).parent();
					}

					if (delta !== 0 && !canScroll) {
						if (F.group.length > 1 && !current.canShrink) {
							if (deltaY > 0 || deltaX > 0) {
								F.prev( deltaY > 0 ? 'down' : 'left' );

							} else if (deltaY < 0 || deltaX < 0) {
								F.next( deltaY < 0 ? 'up' : 'right' );
							}

							e.preventDefault();
						}
					}
				});
			}
		},

		trigger: function (event, o) {
			var ret, obj = o || F.coming || F.current;

			if (!obj) {
				return;
			}

			if ($.isFunction( obj[event] )) {
				ret = obj[event].apply(obj, Array.prototype.slice.call(arguments, 1));
			}

			if (ret === false) {
				return false;
			}

			if (obj.helpers) {
				$.each(obj.helpers, function (helper, opts) {
					if (opts && F.helpers[helper] && $.isFunction(F.helpers[helper][event])) {
						F.helpers[helper][event]($.extend(true, {}, F.helpers[helper].defaults, opts), obj);
					}
				});
			}

			D.trigger(event);
		},

		isImage: function (str) {
			return isString(str) && str.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg)((\?|#).*)?$)/i);
		},

		isSWF: function (str) {
			return isString(str) && str.match(/\.(swf)((\?|#).*)?$/i);
		},

		_start: function (index) {
			var coming = {},
			obj,
			href,
			type,
			margin,
			padding;

			index = getScalar( index );
			obj   = F.group[ index ] || null;

			if (!obj) {
				return false;
			}

			coming = $.extend(true, {}, F.opts, obj);

			// Convert margin and padding properties to array - top, right, bottom, left
			margin  = coming.margin;
			padding = coming.padding;

			if ($.type(margin) === 'number') {
				coming.margin = [margin, margin, margin, margin];
			}

			if ($.type(padding) === 'number') {
				coming.padding = [padding, padding, padding, padding];
			}

			// 'modal' propery is just a shortcut
			if (coming.modal) {
				$.extend(true, coming, {
					closeBtn   : false,
					closeClick : false,
					nextClick  : false,
					arrows     : false,
					mouseWheel : false,
					keys       : null,
					helpers: {
						overlay : {
							closeClick : false
						}
					}
				});
			}

			// 'autoSize' property is a shortcut, too
			if (coming.autoSize) {
				coming.autoWidth = coming.autoHeight = true;
			}

			if (coming.width === 'auto') {
				coming.autoWidth = true;
			}

			if (coming.height === 'auto') {
				coming.autoHeight = true;
			}

			/*
			 * Add reference to the group, so it`s possible to access from callbacks, example:
			 * afterLoad : function() {
			 *     this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
			 * }
			 */

			 coming.group  = F.group;
			 coming.index  = index;

			// Give a chance for callback or helpers to update coming item (type, title, etc)
			F.coming = coming;

			if (false === F.trigger('beforeLoad')) {
				F.coming = null;

				return;
			}

			type = coming.type;
			href = coming.href;

			if (!type) {
				F.coming = null;

				//If we can not determine content type then drop silently or display next/prev item if looping through gallery
				if (F.current && F.router && F.router !== 'jumpto') {
					F.current.index = index;

					return F[ F.router ]( F.direction );
				}

				return false;
			}

			F.isActive = true;

			if (type === 'image' || type === 'swf') {
				coming.autoHeight = coming.autoWidth = false;
				coming.scrolling  = 'visible';
			}

			if (type === 'image') {
				coming.aspectRatio = true;
			}

			if (type === 'iframe' && isTouch) {
				coming.scrolling = 'scroll';
			}

			// Build the neccessary markup
			coming.wrap = $(coming.tpl.wrap).addClass('fancybox-' + (isTouch ? 'mobile' : 'desktop') + ' fancybox-type-' + type + ' fancybox-tmp ' + coming.wrapCSS).appendTo( coming.parent || 'body' );

			$.extend(coming, {
				skin  : $('.fancybox-skin',  coming.wrap),
				outer : $('.fancybox-outer', coming.wrap),
				inner : $('.fancybox-inner', coming.wrap)
			});

			$.each(["Top", "Right", "Bottom", "Left"], function(i, v) {
				coming.skin.css('padding' + v, getValue(coming.padding[ i ]));
			});

			F.trigger('onReady');

			// Check before try to load; 'inline' and 'html' types need content, others - href
			if (type === 'inline' || type === 'html') {
				if (!coming.content || !coming.content.length) {
					return F._error( 'content' );
				}

			} else if (!href) {
				return F._error( 'href' );
			}

			if (type === 'image') {
				F._loadImage();

			} else if (type === 'ajax') {
				F._loadAjax();

			} else if (type === 'iframe') {
				F._loadIframe();

			} else {
				F._afterLoad();
			}
		},

		_error: function ( type ) {
			$.extend(F.coming, {
				type       : 'html',
				autoWidth  : true,
				autoHeight : true,
				minWidth   : 0,
				minHeight  : 0,
				scrolling  : 'no',
				hasError   : type,
				content    : F.coming.tpl.error
			});

			F._afterLoad();
		},

		_loadImage: function () {
			// Reset preload image so it is later possible to check "complete" property
			var img = F.imgPreload = new Image();

			img.onload = function () {
				this.onload = this.onerror = null;

				F.coming.width  = this.width / F.opts.pixelRatio;
				F.coming.height = this.height / F.opts.pixelRatio;

				F._afterLoad();
			};

			img.onerror = function () {
				this.onload = this.onerror = null;

				F._error( 'image' );
			};

			img.src = F.coming.href;

			if (img.complete !== true) {
				F.showLoading();
			}
		},

		_loadAjax: function () {
			var coming = F.coming;

			F.showLoading();

			F.ajaxLoad = $.ajax($.extend({}, coming.ajax, {
				url: coming.href,
				error: function (jqXHR, textStatus) {
					if (F.coming && textStatus !== 'abort') {
						F._error( 'ajax', jqXHR );

					} else {
						F.hideLoading();
					}
				},
				success: function (data, textStatus) {
					if (textStatus === 'success') {
						coming.content = data;

						F._afterLoad();
					}
				}
			}));
		},

		_loadIframe: function() {
			var coming = F.coming,
			iframe = $(coming.tpl.iframe.replace(/\{rnd\}/g, new Date().getTime()))
			.attr('scrolling', isTouch ? 'auto' : coming.iframe.scrolling)
			.attr('src', coming.href);

			// This helps IE
			$(coming.wrap).bind('onReset', function () {
				try {
					$(this).find('iframe').hide().attr('src', '//about:blank').end().empty();
				} catch (e) {}
			});

			if (coming.iframe.preload) {
				F.showLoading();

				iframe.one('load', function() {
					$(this).data('ready', 1);

					// iOS will lose scrolling if we resize
					if (!isTouch) {
						$(this).bind('load.fb', F.update);
					}

					// Without this trick:
					//   - iframe won't scroll on iOS devices
					//   - IE7 sometimes displays empty iframe
					$(this).parents('.fancybox-wrap').width('100%').removeClass('fancybox-tmp').show();

					F._afterLoad();
				});
			}

			coming.content = iframe.appendTo( coming.inner );

			if (!coming.iframe.preload) {
				F._afterLoad();
			}
		},

		_preloadImages: function() {
			var group   = F.group,
			current = F.current,
			len     = group.length,
			cnt     = current.preload ? Math.min(current.preload, len - 1) : 0,
			item,
			i;

			for (i = 1; i <= cnt; i += 1) {
				item = group[ (current.index + i ) % len ];

				if (item.type === 'image' && item.href) {
					new Image().src = item.href;
				}
			}
		},

		_afterLoad: function () {
			var coming   = F.coming,
			previous = F.current,
			placeholder = 'fancybox-placeholder',
			current,
			content,
			type,
			scrolling,
			href,
			embed;

			F.hideLoading();

			if (!coming || F.isActive === false) {
				return;
			}

			if (false === F.trigger('afterLoad', coming, previous)) {
				coming.wrap.stop(true).trigger('onReset').remove();

				F.coming = null;

				return;
			}

			if (previous) {
				F.trigger('beforeChange', previous);

				previous.wrap.stop(true).removeClass('fancybox-opened')
				.find('.fancybox-item, .fancybox-nav')
				.remove();
			}

			F.unbindEvents();

			current   = coming;
			content   = coming.content;
			type      = coming.type;
			scrolling = coming.scrolling;

			$.extend(F, {
				wrap  : current.wrap,
				skin  : current.skin,
				outer : current.outer,
				inner : current.inner,
				current  : current,
				previous : previous
			});

			href = current.href;

			switch (type) {
				case 'inline':
				case 'ajax':
				case 'html':
				if (current.selector) {
					content = $('<div>').html(content).find(current.selector);

				} else if (isQuery(content)) {
					if (!content.data(placeholder)) {
						content.data(placeholder, $('<div class="' + placeholder + '"></div>').insertAfter( content ).hide() );
					}

					content = content.show().detach();

					current.wrap.bind('onReset', function () {
						if ($(this).find(content).length) {
							content.hide().replaceAll( content.data(placeholder) ).data(placeholder, false);
						}
					});
				}
				break;

				case 'image':
				content = current.tpl.image.replace('{href}', href);
				break;

				case 'swf':
				content = '<object id="fancybox-swf" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="movie" value="' + href + '"></param>';
				embed   = '';

				$.each(current.swf, function(name, val) {
					content += '<param name="' + name + '" value="' + val + '"></param>';
					embed   += ' ' + name + '="' + val + '"';
				});

				content += '<embed src="' + href + '" type="application/x-shockwave-flash" width="100%" height="100%"' + embed + '></embed></object>';
				break;
			}

			if (!(isQuery(content) && content.parent().is(current.inner))) {
				current.inner.append( content );
			}

			// Give a chance for helpers or callbacks to update elements
			F.trigger('beforeShow');

			// Set scrolling before calculating dimensions
			current.inner.css('overflow', scrolling === 'yes' ? 'scroll' : (scrolling === 'no' ? 'hidden' : scrolling));

			// Set initial dimensions and start position
			F._setDimension();

			F.reposition();

			F.isOpen = false;
			F.coming = null;

			F.bindEvents();

			if (!F.isOpened) {
				$('.fancybox-wrap').not( current.wrap ).stop(true).trigger('onReset').remove();

			} else if (previous.prevMethod) {
				F.transitions[ previous.prevMethod ]();
			}

			F.transitions[ F.isOpened ? current.nextMethod : current.openMethod ]();

			F._preloadImages();
		},

		_setDimension: function () {
			var viewport   = F.getViewport(),
			steps      = 0,
			canShrink  = false,
			canExpand  = false,
			wrap       = F.wrap,
			skin       = F.skin,
			inner      = F.inner,
			current    = F.current,
			width      = current.width,
			height     = current.height,
			minWidth   = current.minWidth,
			minHeight  = current.minHeight,
			maxWidth   = current.maxWidth,
			maxHeight  = current.maxHeight,
			scrolling  = current.scrolling,
			scrollOut  = current.scrollOutside ? current.scrollbarWidth : 0,
			margin     = current.margin,
			wMargin    = getScalar(margin[1] + margin[3]),
			hMargin    = getScalar(margin[0] + margin[2]),
			wPadding,
			hPadding,
			wSpace,
			hSpace,
			origWidth,
			origHeight,
			origMaxWidth,
			origMaxHeight,
			ratio,
			width_,
			height_,
			maxWidth_,
			maxHeight_,
			iframe,
			body;

			// Reset dimensions so we could re-check actual size
			wrap.add(skin).add(inner).width('auto').height('auto').removeClass('fancybox-tmp');

			wPadding = getScalar(skin.outerWidth(true)  - skin.width());
			hPadding = getScalar(skin.outerHeight(true) - skin.height());

			// Any space between content and viewport (margin, padding, border, title)
			wSpace = wMargin + wPadding;
			hSpace = hMargin + hPadding;

			origWidth  = isPercentage(width)  ? (viewport.w - wSpace) * getScalar(width)  / 100 : width;
			origHeight = isPercentage(height) ? (viewport.h - hSpace) * getScalar(height) / 100 : height;

			if (current.type === 'iframe') {
				iframe = current.content;

				if (current.autoHeight && iframe.data('ready') === 1) {
					try {
						if (iframe[0].contentWindow.document.location) {
							inner.width( origWidth ).height(9999);

							body = iframe.contents().find('body');

							if (scrollOut) {
								body.css('overflow-x', 'hidden');
							}

							origHeight = body.outerHeight(true);
						}

					} catch (e) {}
				}

			} else if (current.autoWidth || current.autoHeight) {
				inner.addClass( 'fancybox-tmp' );

				// Set width or height in case we need to calculate only one dimension
				if (!current.autoWidth) {
					inner.width( origWidth );
				}

				if (!current.autoHeight) {
					inner.height( origHeight );
				}

				if (current.autoWidth) {
					origWidth = inner.width();
				}

				if (current.autoHeight) {
					origHeight = inner.height();
				}

				inner.removeClass( 'fancybox-tmp' );
			}

			width  = getScalar( origWidth );
			height = getScalar( origHeight );

			ratio  = origWidth / origHeight;

			// Calculations for the content
			minWidth  = getScalar(isPercentage(minWidth) ? getScalar(minWidth, 'w') - wSpace : minWidth);
			maxWidth  = getScalar(isPercentage(maxWidth) ? getScalar(maxWidth, 'w') - wSpace : maxWidth);

			minHeight = getScalar(isPercentage(minHeight) ? getScalar(minHeight, 'h') - hSpace : minHeight);
			maxHeight = getScalar(isPercentage(maxHeight) ? getScalar(maxHeight, 'h') - hSpace : maxHeight);

			// These will be used to determine if wrap can fit in the viewport
			origMaxWidth  = maxWidth;
			origMaxHeight = maxHeight;

			if (current.fitToView) {
				maxWidth  = Math.min(viewport.w - wSpace, maxWidth);
				maxHeight = Math.min(viewport.h - hSpace, maxHeight);
			}

			maxWidth_  = viewport.w - wMargin;
			maxHeight_ = viewport.h - hMargin;

			if (current.aspectRatio) {
				if (width > maxWidth) {
					width  = maxWidth;
					height = getScalar(width / ratio);
				}

				if (height > maxHeight) {
					height = maxHeight;
					width  = getScalar(height * ratio);
				}

				if (width < minWidth) {
					width  = minWidth;
					height = getScalar(width / ratio);
				}

				if (height < minHeight) {
					height = minHeight;
					width  = getScalar(height * ratio);
				}

			} else {
				width = Math.max(minWidth, Math.min(width, maxWidth));

				if (current.autoHeight && current.type !== 'iframe') {
					inner.width( width );

					height = inner.height();
				}

				height = Math.max(minHeight, Math.min(height, maxHeight));
			}

			// Try to fit inside viewport (including the title)
			if (current.fitToView) {
				inner.width( width ).height( height );

				wrap.width( width + wPadding );

				// Real wrap dimensions
				width_  = wrap.width();
				height_ = wrap.height();

				if (current.aspectRatio) {
					while ((width_ > maxWidth_ || height_ > maxHeight_) && width > minWidth && height > minHeight) {
						if (steps++ > 19) {
							break;
						}

						height = Math.max(minHeight, Math.min(maxHeight, height - 10));
						width  = getScalar(height * ratio);

						if (width < minWidth) {
							width  = minWidth;
							height = getScalar(width / ratio);
						}

						if (width > maxWidth) {
							width  = maxWidth;
							height = getScalar(width / ratio);
						}

						inner.width( width ).height( height );

						wrap.width( width + wPadding );

						width_  = wrap.width();
						height_ = wrap.height();
					}

				} else {
					width  = Math.max(minWidth,  Math.min(width,  width  - (width_  - maxWidth_)));
					height = Math.max(minHeight, Math.min(height, height - (height_ - maxHeight_)));
				}
			}

			if (scrollOut && scrolling === 'auto' && height < origHeight && (width + wPadding + scrollOut) < maxWidth_) {
				width += scrollOut;
			}

			inner.width( width ).height( height );

			wrap.width( width + wPadding );

			width_  = wrap.width();
			height_ = wrap.height();

			canShrink = (width_ > maxWidth_ || height_ > maxHeight_) && width > minWidth && height > minHeight;
			canExpand = current.aspectRatio ? (width < origMaxWidth && height < origMaxHeight && width < origWidth && height < origHeight) : ((width < origMaxWidth || height < origMaxHeight) && (width < origWidth || height < origHeight));

			$.extend(current, {
				dim : {
					width	: getValue( width_ ),
					height	: getValue( height_ )
				},
				origWidth  : origWidth,
				origHeight : origHeight,
				canShrink  : canShrink,
				canExpand  : canExpand,
				wPadding   : wPadding,
				hPadding   : hPadding,
				wrapSpace  : height_ - skin.outerHeight(true),
				skinSpace  : skin.height() - height
			});

			if (!iframe && current.autoHeight && height > minHeight && height < maxHeight && !canExpand) {
				inner.height('auto');
			}
		},

		_getPosition: function (onlyAbsolute) {
			var current  = F.current,
			viewport = F.getViewport(),
			margin   = current.margin,
			width    = F.wrap.width()  + margin[1] + margin[3],
			height   = F.wrap.height() + margin[0] + margin[2],
			rez      = {
				position: 'absolute',
				top  : margin[0],
				left : margin[3]
			};

			if (current.autoCenter && current.fixed && !onlyAbsolute && height <= viewport.h && width <= viewport.w) {
				rez.position = 'fixed';

			} else if (!current.locked) {
				rez.top  += viewport.y;
				rez.left += viewport.x;
			}

			rez.top  = getValue(Math.max(rez.top,  rez.top  + ((viewport.h - height) * current.topRatio)));
			rez.left = getValue(Math.max(rez.left, rez.left + ((viewport.w - width)  * current.leftRatio)));

			return rez;
		},

		_afterZoomIn: function () {
			var current = F.current;

			if (!current) {
				return;
			}

			F.isOpen = F.isOpened = true;

			F.wrap.css('overflow', 'visible').addClass('fancybox-opened');

			F.update();

			// Assign a click event
			if ( current.closeClick || (current.nextClick && F.group.length > 1) ) {
				F.inner.css('cursor', 'pointer').bind('click.fb', function(e) {
					if (!$(e.target).is('a') && !$(e.target).parent().is('a')) {
						e.preventDefault();

						F[ current.closeClick ? 'close' : 'next' ]();
					}
				});
			}

			// Create a close button
			if (current.closeBtn) {
				$(current.tpl.closeBtn).appendTo(F.skin).bind('click.fb', function(e) {
					e.preventDefault();

					F.close();
				});
			}

			// Create navigation arrows
			if (current.arrows && F.group.length > 1) {
				if (current.loop || current.index > 0) {
					$(current.tpl.prev).appendTo(F.outer).bind('click.fb', F.prev);
				}

				if (current.loop || current.index < F.group.length - 1) {
					$(current.tpl.next).appendTo(F.outer).bind('click.fb', F.next);
				}
			}

			F.trigger('afterShow');

			// Stop the slideshow if this is the last item
			if (!current.loop && current.index === current.group.length - 1) {
				F.play( false );

			} else if (F.opts.autoPlay && !F.player.isActive) {
				F.opts.autoPlay = false;

				F.play();
			}
		},

		_afterZoomOut: function ( obj ) {
			obj = obj || F.current;

			$('.fancybox-wrap').trigger('onReset').remove();

			$.extend(F, {
				group  : {},
				opts   : {},
				router : false,
				current   : null,
				isActive  : false,
				isOpened  : false,
				isOpen    : false,
				isClosing : false,
				wrap   : null,
				skin   : null,
				outer  : null,
				inner  : null
			});

			F.trigger('afterClose', obj);
		}
	});

	/*
	 *	Default transitions
	 */

	 F.transitions = {
	 	getOrigPosition: function () {
	 		var current  = F.current,
	 		element  = current.element,
	 		orig     = current.orig,
	 		pos      = {},
	 		width    = 50,
	 		height   = 50,
	 		hPadding = current.hPadding,
	 		wPadding = current.wPadding,
	 		viewport = F.getViewport();

	 		if (!orig && current.isDom && element.is(':visible')) {
	 			orig = element.find('img:first');

	 			if (!orig.length) {
	 				orig = element;
	 			}
	 		}

	 		if (isQuery(orig)) {
	 			pos = orig.offset();

	 			if (orig.is('img')) {
	 				width  = orig.outerWidth();
	 				height = orig.outerHeight();
	 			}

	 		} else {
	 			pos.top  = viewport.y + (viewport.h - height) * current.topRatio;
	 			pos.left = viewport.x + (viewport.w - width)  * current.leftRatio;
	 		}

	 		if (F.wrap.css('position') === 'fixed' || current.locked) {
	 			pos.top  -= viewport.y;
	 			pos.left -= viewport.x;
	 		}

	 		pos = {
	 			top     : getValue(pos.top  - hPadding * current.topRatio),
	 			left    : getValue(pos.left - wPadding * current.leftRatio),
	 			width   : getValue(width  + wPadding),
	 			height  : getValue(height + hPadding)
	 		};

	 		return pos;
	 	},

	 	step: function (now, fx) {
	 		var ratio,
	 		padding,
	 		value,
	 		prop       = fx.prop,
	 		current    = F.current,
	 		wrapSpace  = current.wrapSpace,
	 		skinSpace  = current.skinSpace;

	 		if (prop === 'width' || prop === 'height') {
	 			ratio = fx.end === fx.start ? 1 : (now - fx.start) / (fx.end - fx.start);

	 			if (F.isClosing) {
	 				ratio = 1 - ratio;
	 			}

	 			padding = prop === 'width' ? current.wPadding : current.hPadding;
	 			value   = now - padding;

	 			F.skin[ prop ](  getScalar( prop === 'width' ?  value : value - (wrapSpace * ratio) ) );
	 			F.inner[ prop ]( getScalar( prop === 'width' ?  value : value - (wrapSpace * ratio) - (skinSpace * ratio) ) );
	 		}
	 	},

	 	zoomIn: function () {
	 		var current  = F.current,
	 		startPos = current.pos,
	 		effect   = current.openEffect,
	 		elastic  = effect === 'elastic',
	 		endPos   = $.extend({opacity : 1}, startPos);

			// Remove "position" property that breaks older IE
			delete endPos.position;

			if (elastic) {
				startPos = this.getOrigPosition();

				if (current.openOpacity) {
					startPos.opacity = 0.1;
				}

			} else if (effect === 'fade') {
				startPos.opacity = 0.1;
			}

			F.wrap.css(startPos).animate(endPos, {
				duration : effect === 'none' ? 0 : current.openSpeed,
				easing   : current.openEasing,
				step     : elastic ? this.step : null,
				complete : F._afterZoomIn
			});
		},

		zoomOut: function () {
			var current  = F.current,
			effect   = current.closeEffect,
			elastic  = effect === 'elastic',
			endPos   = {opacity : 0.1};

			if (elastic) {
				endPos = this.getOrigPosition();

				if (current.closeOpacity) {
					endPos.opacity = 0.1;
				}
			}

			F.wrap.animate(endPos, {
				duration : effect === 'none' ? 0 : current.closeSpeed,
				easing   : current.closeEasing,
				step     : elastic ? this.step : null,
				complete : F._afterZoomOut
			});
		},

		changeIn: function () {
			var current   = F.current,
			effect    = current.nextEffect,
			startPos  = current.pos,
			endPos    = { opacity : 1 },
			direction = F.direction,
			distance  = 200,
			field;

			startPos.opacity = 0.1;

			if (effect === 'elastic') {
				field = direction === 'down' || direction === 'up' ? 'top' : 'left';

				if (direction === 'down' || direction === 'right') {
					startPos[ field ] = getValue(getScalar(startPos[ field ]) - distance);
					endPos[ field ]   = '+=' + distance + 'px';

				} else {
					startPos[ field ] = getValue(getScalar(startPos[ field ]) + distance);
					endPos[ field ]   = '-=' + distance + 'px';
				}
			}

			// Workaround for http://bugs.jquery.com/ticket/12273
			if (effect === 'none') {
				F._afterZoomIn();

			} else {
				F.wrap.css(startPos).animate(endPos, {
					duration : current.nextSpeed,
					easing   : current.nextEasing,
					complete : F._afterZoomIn
				});
			}
		},

		changeOut: function () {
			var previous  = F.previous,
			effect    = previous.prevEffect,
			endPos    = { opacity : 0.1 },
			direction = F.direction,
			distance  = 200;

			if (effect === 'elastic') {
				endPos[ direction === 'down' || direction === 'up' ? 'top' : 'left' ] = ( direction === 'up' || direction === 'left' ? '-' : '+' ) + '=' + distance + 'px';
			}

			previous.wrap.animate(endPos, {
				duration : effect === 'none' ? 0 : previous.prevSpeed,
				easing   : previous.prevEasing,
				complete : function () {
					$(this).trigger('onReset').remove();
				}
			});
		}
	};

	/*
	 *	Overlay helper
	 */

	 F.helpers.overlay = {
	 	defaults : {
			closeClick : true,      // if true, fancyBox will be closed when user clicks on the overlay
			speedOut   : 200,       // duration of fadeOut animation
			showEarly  : true,      // indicates if should be opened immediately or wait until the content is ready
			css        : {},        // custom CSS properties
			locked     : !isTouch,  // if true, the content will be locked into overlay
			fixed      : true       // if false, the overlay CSS position property will not be set to "fixed"
		},

		overlay : null,      // current handle
		fixed   : false,     // indicates if the overlay has position "fixed"
		el      : $('html'), // element that contains "the lock"

		// Public methods
		create : function(opts) {
			opts = $.extend({}, this.defaults, opts);

			if (this.overlay) {
				this.close();
			}

			this.overlay = $('<div class="fancybox-overlay"></div>').appendTo( F.coming ? F.coming.parent : opts.parent );
			this.fixed   = false;

			if (opts.fixed && F.defaults.fixed) {
				this.overlay.addClass('fancybox-overlay-fixed');

				this.fixed = true;
			}
		},

		open : function(opts) {
			var that = this;

			opts = $.extend({}, this.defaults, opts);

			if (this.overlay) {
				this.overlay.unbind('.overlay').width('auto').height('auto');

			} else {
				this.create(opts);
			}

			if (!this.fixed) {
				W.bind('resize.overlay', $.proxy( this.update, this) );

				this.update();
			}

			if (opts.closeClick) {
				this.overlay.bind('click.overlay', function(e) {
					if ($(e.target).hasClass('fancybox-overlay')) {
						if (F.isActive) {
							F.close();
						} else {
							that.close();
						}

						return false;
					}
				});
			}

			this.overlay.css( opts.css ).show();
		},

		close : function() {
			var scrollV, scrollH;

			W.unbind('resize.overlay');

			if (this.el.hasClass('fancybox-lock')) {
				$('.fancybox-margin').removeClass('fancybox-margin');

				scrollV = W.scrollTop();
				scrollH = W.scrollLeft();

				this.el.removeClass('fancybox-lock');

				W.scrollTop( scrollV ).scrollLeft( scrollH );
			}

			$('.fancybox-overlay').remove().hide();

			$.extend(this, {
				overlay : null,
				fixed   : false
			});
		},

		// Private, callbacks

		update : function () {
			var width = '100%', offsetWidth;

			// Reset width/height so it will not mess
			this.overlay.width(width).height('100%');

			// jQuery does not return reliable result for IE
			if (IE) {
				offsetWidth = Math.max(document.documentElement.offsetWidth, document.body.offsetWidth);

				if (D.width() > offsetWidth) {
					width = D.width();
				}

			} else if (D.width() > W.width()) {
				width = D.width();
			}

			this.overlay.width(width).height(D.height());
		},

		// This is where we can manipulate DOM, because later it would cause iframes to reload
		onReady : function (opts, obj) {
			var overlay = this.overlay;

			$('.fancybox-overlay').stop(true, true);

			if (!overlay) {
				this.create(opts);
			}

			if (opts.locked && this.fixed && obj.fixed) {
				if (!overlay) {
					this.margin = D.height() > W.height() ? $('html').css('margin-right').replace("px", "") : false;
				}

				obj.locked = this.overlay.append( obj.wrap );
				obj.fixed  = false;
			}

			if (opts.showEarly === true) {
				this.beforeShow.apply(this, arguments);
			}
		},

		beforeShow : function(opts, obj) {
			var scrollV, scrollH;

			if (obj.locked) {
				if (this.margin !== false) {
					$('*').filter(function(){
						return ($(this).css('position') === 'fixed' && !$(this).hasClass("fancybox-overlay") && !$(this).hasClass("fancybox-wrap") );
					}).addClass('fancybox-margin');

					this.el.addClass('fancybox-margin');
				}

				scrollV = W.scrollTop();
				scrollH = W.scrollLeft();

				this.el.addClass('fancybox-lock');

				W.scrollTop( scrollV ).scrollLeft( scrollH );
			}

			this.open(opts);
		},

		onUpdate : function() {
			if (!this.fixed) {
				this.update();
			}
		},

		afterClose: function (opts) {
			// Remove overlay if exists and fancyBox is not opening
			// (e.g., it is not being open using afterClose callback)
			//if (this.overlay && !F.isActive) {
				if (this.overlay && !F.coming) {
					this.overlay.fadeOut(opts.speedOut, $.proxy( this.close, this ));
				}
			}
		};

	/*
	 *	Title helper
	 */

	 F.helpers.title = {
	 	defaults : {
			type     : 'float', // 'float', 'inside', 'outside' or 'over',
			position : 'bottom' // 'top' or 'bottom'
		},

		beforeShow: function (opts) {
			var current = F.current,
			text    = current.title,
			type    = opts.type,
			title,
			target;

			if ($.isFunction(text)) {
				text = text.call(current.element, current);
			}

			if (!isString(text) || $.trim(text) === '') {
				return;
			}

			title = $('<div class="fancybox-title fancybox-title-' + type + '-wrap">' + text + '</div>');

			switch (type) {
				case 'inside':
				target = F.skin;
				break;

				case 'outside':
				target = F.wrap;
				break;

				case 'over':
				target = F.inner;
				break;

				default: // 'float'
				target = F.skin;

				title.appendTo('body');

				if (IE) {
					title.width( title.width() );
				}

				title.wrapInner('<span class="child"></span>');

					//Increase bottom margin so this title will also fit into viewport
					F.current.margin[2] += Math.abs( getScalar(title.css('margin-bottom')) );
					break;
				}

				title[ (opts.position === 'top' ? 'prependTo'  : 'appendTo') ](target);
			}
		};

	// jQuery plugin initialization
	$.fn.fancybox = function (options) {
		var index,
		that     = $(this),
		selector = this.selector || '',
		run      = function(e) {
			var what = $(this).blur(), idx = index, relType, relVal;

			if (!(e.ctrlKey || e.altKey || e.shiftKey || e.metaKey) && !what.is('.fancybox-wrap')) {
				relType = options.groupAttr || 'data-fancybox-group';
				relVal  = what.attr(relType);

				if (!relVal) {
					relType = 'rel';
					relVal  = what.get(0)[ relType ];
				}

				if (relVal && relVal !== '' && relVal !== 'nofollow') {
					what = selector.length ? $(selector) : that;
					what = what.filter('[' + relType + '="' + relVal + '"]');
					idx  = what.index(this);
				}

				options.index = idx;

					// Stop an event from bubbling if everything is fine
					if (F.open(what, options) !== false) {
						e.preventDefault();
					}
				}
			};

			options = options || {};
			index   = options.index || 0;

			if (!selector || options.live === false) {
				that.unbind('click.fb-start').bind('click.fb-start', run);

			} else {
				D.undelegate(selector, 'click.fb-start').delegate(selector + ":not('.fancybox-item, .fancybox-nav')", 'click.fb-start', run);
			}

			this.filter('[data-fancybox-start=1]').trigger('click');

			return this;
		};

	// Tests that need a body at doc ready
	D.ready(function() {
		var w1, w2;

		if ( $.scrollbarWidth === undefined ) {
			// http://benalman.com/projects/jquery-misc-plugins/#scrollbarwidth
			$.scrollbarWidth = function() {
				var parent = $('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo('body'),
				child  = parent.children(),
				width  = child.innerWidth() - child.height( 99 ).innerWidth();

				parent.remove();

				return width;
			};
		}

		if ( $.support.fixedPosition === undefined ) {
			$.support.fixedPosition = (function() {
				var elem  = $('<div style="position:fixed;top:20px;"></div>').appendTo('body'),
				fixed = ( elem[0].offsetTop === 20 || elem[0].offsetTop === 15 );

				elem.remove();

				return fixed;
			}());
		}

		$.extend(F.defaults, {
			scrollbarWidth : $.scrollbarWidth(),
			fixed  : $.support.fixedPosition,
			parent : $('body')
		});

		//Get real width of page scroll-bar
		w1 = $(window).width();

		H.addClass('fancybox-lock-test');

		w2 = $(window).width();

		H.removeClass('fancybox-lock-test');

		$("<style type='text/css'>.fancybox-margin{margin-right:" + (w2 - w1) + "px;}</style>").appendTo("head");
	});

}(window, document, jQuery));
/**
 * Swiper 3.1.2
 * Most modern mobile touch slider and framework with hardware accelerated transitions
 * 
 * http://www.idangero.us/swiper/
 * 
 * Copyright 2015, Vladimir Kharlampidi
 * The iDangero.us
 * http://www.idangero.us/
 * 
 * Licensed under MIT
 * 
 * Released on: August 22, 2015
 */
 (function () {
 	'use strict';
 	var $;
    /*===========================
    Swiper
    ===========================*/
    var Swiper = function (container, params) {
    	if (!(this instanceof Swiper)) return new Swiper(container, params);

    	var defaults = {
    		direction: 'horizontal',
    		touchEventsTarget: 'container',
    		initialSlide: 0,
    		speed: 300,
            // autoplay
            autoplay: false,
            autoplayDisableOnInteraction: true,
            // To support iOS's swipe-to-go-back gesture (when being used in-app, with UIWebView).
            iOSEdgeSwipeDetection: false,
            iOSEdgeSwipeThreshold: 20,
            // Free mode
            freeMode: false,
            freeModeMomentum: true,
            freeModeMomentumRatio: 1,
            freeModeMomentumBounce: true,
            freeModeMomentumBounceRatio: 1,
            freeModeSticky: false,
            // Set wrapper width
            setWrapperSize: false,
            // Virtual Translate
            virtualTranslate: false,
            // Effects
            effect: 'slide', // 'slide' or 'fade' or 'cube' or 'coverflow'
            coverflow: {
            	rotate: 50,
            	stretch: 0,
            	depth: 100,
            	modifier: 1,
            	slideShadows : true
            },
            cube: {
            	slideShadows: true,
            	shadow: true,
            	shadowOffset: 20,
            	shadowScale: 0.94
            },
            fade: {
            	crossFade: false
            },
            // Parallax
            parallax: false,
            // Scrollbar
            scrollbar: null,
            scrollbarHide: true,
            // Keyboard Mousewheel
            keyboardControl: false,
            mousewheelControl: false,
            mousewheelReleaseOnEdges: false,
            mousewheelInvert: false,
            mousewheelForceToAxis: false,
            mousewheelSensitivity: 1,
            // Hash Navigation
            hashnav: false,
            // Slides grid
            spaceBetween: 0,
            slidesPerView: 1,
            slidesPerColumn: 1,
            slidesPerColumnFill: 'column',
            slidesPerGroup: 1,
            centeredSlides: false,
            slidesOffsetBefore: 0, // in px
            slidesOffsetAfter: 0, // in px
            // Round length
            roundLengths: false,
            // Touches
            touchRatio: 1,
            touchAngle: 45,
            simulateTouch: true,
            shortSwipes: true,
            longSwipes: true,
            longSwipesRatio: 0.5,
            longSwipesMs: 300,
            followFinger: true,
            onlyExternal: false,
            threshold: 0,
            touchMoveStopPropagation: true,
            // Pagination
            pagination: null,
            paginationElement: 'span',
            paginationClickable: false,
            paginationHide: false,
            paginationBulletRender: null,
            // Resistance
            resistance: true,
            resistanceRatio: 0.85,
            // Next/prev buttons
            nextButton: null,
            prevButton: null,
            // Progress
            watchSlidesProgress: false,
            watchSlidesVisibility: false,
            // Cursor
            grabCursor: false,
            // Clicks
            preventClicks: true,
            preventClicksPropagation: true,
            slideToClickedSlide: false,
            // Lazy Loading
            lazyLoading: false,
            lazyLoadingInPrevNext: false,
            lazyLoadingOnTransitionStart: false,
            // Images
            preloadImages: true,
            updateOnImagesReady: true,
            // loop
            loop: false,
            loopAdditionalSlides: 0,
            loopedSlides: null,
            // Control
            control: undefined,
            controlInverse: false,
            controlBy: 'slide', //or 'container'
            // Swiping/no swiping
            allowSwipeToPrev: true,
            allowSwipeToNext: true,
            swipeHandler: null, //'.swipe-handler',
            noSwiping: true,
            noSwipingClass: 'swiper-no-swiping',
            // NS
            slideClass: 'swiper-slide',
            slideActiveClass: 'swiper-slide-active',
            slideVisibleClass: 'swiper-slide-visible',
            slideDuplicateClass: 'swiper-slide-duplicate',
            slideNextClass: 'swiper-slide-next',
            slidePrevClass: 'swiper-slide-prev',
            wrapperClass: 'swiper-wrapper',
            bulletClass: 'swiper-pagination-bullet',
            bulletActiveClass: 'swiper-pagination-bullet-active',
            buttonDisabledClass: 'swiper-button-disabled',
            paginationHiddenClass: 'swiper-pagination-hidden',
            // Observer
            observer: false,
            observeParents: false,
            // Accessibility
            a11y: false,
            prevSlideMessage: 'Previous slide',
            nextSlideMessage: 'Next slide',
            firstSlideMessage: 'This is the first slide',
            lastSlideMessage: 'This is the last slide',
            paginationBulletMessage: 'Go to slide {{index}}',
            // Callbacks
            runCallbacksOnInit: true
            /*
            Callbacks:
            onInit: function (swiper)
            onDestroy: function (swiper)
            onClick: function (swiper, e)
            onTap: function (swiper, e)
            onDoubleTap: function (swiper, e)
            onSliderMove: function (swiper, e)
            onSlideChangeStart: function (swiper)
            onSlideChangeEnd: function (swiper)
            onTransitionStart: function (swiper)
            onTransitionEnd: function (swiper)
            onImagesReady: function (swiper)
            onProgress: function (swiper, progress)
            onTouchStart: function (swiper, e)
            onTouchMove: function (swiper, e)
            onTouchMoveOpposite: function (swiper, e)
            onTouchEnd: function (swiper, e)
            onReachBeginning: function (swiper)
            onReachEnd: function (swiper)
            onSetTransition: function (swiper, duration)
            onSetTranslate: function (swiper, translate)
            onAutoplayStart: function (swiper)
            onAutoplayStop: function (swiper),
            onLazyImageLoad: function (swiper, slide, image)
            onLazyImageReady: function (swiper, slide, image)
            */

        };
        var initialVirtualTranslate = params && params.virtualTranslate;
        
        params = params || {};
        for (var def in defaults) {
        	if (typeof params[def] === 'undefined') {
        		params[def] = defaults[def];
        	}
        	else if (typeof params[def] === 'object') {
        		for (var deepDef in defaults[def]) {
        			if (typeof params[def][deepDef] === 'undefined') {
        				params[def][deepDef] = defaults[def][deepDef];
        			}
        		}
        	}
        }
        
        // Swiper
        var s = this;
        
        // Version
        s.version = '3.1.0';
        
        // Params
        s.params = params;
        
        // Classname
        s.classNames = [];
        /*=========================
          Dom Library and plugins
          ===========================*/
          if (typeof $ !== 'undefined' && typeof Dom7 !== 'undefined'){
          	$ = Dom7;
          }  
          if (typeof $ === 'undefined') {
          	if (typeof Dom7 === 'undefined') {
          		$ = window.Dom7 || window.Zepto || window.jQuery;
          	}
          	else {
          		$ = Dom7;
          	}
          	if (!$) return;
          }
        // Export it to Swiper instance
        s.$ = $;
        
        /*=========================
          Preparation - Define Container, Wrapper and Pagination
          ===========================*/
          s.container = $(container);
          if (s.container.length === 0) return;
          if (s.container.length > 1) {
          	s.container.each(function () {
          		new Swiper(this, params);
          	});
          	return;
          }

        // Save instance in container HTML Element and in data
        s.container[0].swiper = s;
        s.container.data('swiper', s);
        
        s.classNames.push('swiper-container-' + s.params.direction);
        
        if (s.params.freeMode) {
        	s.classNames.push('swiper-container-free-mode');
        }
        if (!s.support.flexbox) {
        	s.classNames.push('swiper-container-no-flexbox');
        	s.params.slidesPerColumn = 1;
        }
        // Enable slides progress when required
        if (s.params.parallax || s.params.watchSlidesVisibility) {
        	s.params.watchSlidesProgress = true;
        }
        // Coverflow / 3D
        if (['cube', 'coverflow'].indexOf(s.params.effect) >= 0) {
        	if (s.support.transforms3d) {
        		s.params.watchSlidesProgress = true;
        		s.classNames.push('swiper-container-3d');
        	}
        	else {
        		s.params.effect = 'slide';
        	}
        }
        if (s.params.effect !== 'slide') {
        	s.classNames.push('swiper-container-' + s.params.effect);
        }
        if (s.params.effect === 'cube') {
        	s.params.resistanceRatio = 0;
        	s.params.slidesPerView = 1;
        	s.params.slidesPerColumn = 1;
        	s.params.slidesPerGroup = 1;
        	s.params.centeredSlides = false;
        	s.params.spaceBetween = 0;
        	s.params.virtualTranslate = true;
        	s.params.setWrapperSize = false;
        }
        if (s.params.effect === 'fade') {
        	s.params.slidesPerView = 1;
        	s.params.slidesPerColumn = 1;
        	s.params.slidesPerGroup = 1;
        	s.params.watchSlidesProgress = true;
        	s.params.spaceBetween = 0;
        	if (typeof initialVirtualTranslate === 'undefined') {
        		s.params.virtualTranslate = true;
        	}
        }
        
        // Grab Cursor
        if (s.params.grabCursor && s.support.touch) {
        	s.params.grabCursor = false;
        }
        
        // Wrapper
        s.wrapper = s.container.children('.' + s.params.wrapperClass);
        
        // Pagination
        if (s.params.pagination) {
        	s.paginationContainer = $(s.params.pagination);
        	if (s.params.paginationClickable) {
        		s.paginationContainer.addClass('swiper-pagination-clickable');
        	}
        }
        
        // Is Horizontal
        function isH() {
        	return s.params.direction === 'horizontal';
        }
        
        // RTL
        s.rtl = isH() && (s.container[0].dir.toLowerCase() === 'rtl' || s.container.css('direction') === 'rtl');
        if (s.rtl) {
        	s.classNames.push('swiper-container-rtl');
        }
        
        // Wrong RTL support
        if (s.rtl) {
        	s.wrongRTL = s.wrapper.css('display') === '-webkit-box';
        }
        
        // Columns
        if (s.params.slidesPerColumn > 1) {
        	s.classNames.push('swiper-container-multirow');
        }
        
        // Check for Android
        if (s.device.android) {
        	s.classNames.push('swiper-container-android');
        }
        
        // Add classes
        s.container.addClass(s.classNames.join(' '));
        
        // Translate
        s.translate = 0;
        
        // Progress
        s.progress = 0;
        
        // Velocity
        s.velocity = 0;
        
        /*=========================
          Locks, unlocks
          ===========================*/
          s.lockSwipeToNext = function () {
          	s.params.allowSwipeToNext = false;
          };
          s.lockSwipeToPrev = function () {
          	s.params.allowSwipeToPrev = false;
          };
          s.lockSwipes = function () {
          	s.params.allowSwipeToNext = s.params.allowSwipeToPrev = false;
          };
          s.unlockSwipeToNext = function () {
          	s.params.allowSwipeToNext = true;
          };
          s.unlockSwipeToPrev = function () {
          	s.params.allowSwipeToPrev = true;
          };
          s.unlockSwipes = function () {
          	s.params.allowSwipeToNext = s.params.allowSwipeToPrev = true;
          };

        /*=========================
          Round helper
          ===========================*/
          function round(a) {
          	return Math.floor(a);
          }  
        /*=========================
          Set grab cursor
          ===========================*/
          if (s.params.grabCursor) {
          	s.container[0].style.cursor = 'move';
          	s.container[0].style.cursor = '-webkit-grab';
          	s.container[0].style.cursor = '-moz-grab';
          	s.container[0].style.cursor = 'grab';
          }
        /*=========================
          Update on Images Ready
          ===========================*/
          s.imagesToLoad = [];
          s.imagesLoaded = 0;

          s.loadImage = function (imgElement, src, checkForComplete, callback) {
          	var image;
          	function onReady () {
          		if (callback) callback();
          	}
          	if (!imgElement.complete || !checkForComplete) {
          		if (src) {
          			image = new window.Image();
          			image.onload = onReady;
          			image.onerror = onReady;
          			image.src = src;
          		} else {
          			onReady();
          		}

            } else {//image already loaded...
            	onReady();
            }
        };
        s.preloadImages = function () {
        	s.imagesToLoad = s.container.find('img');
        	function _onReady() {
        		if (typeof s === 'undefined' || s === null) return;
        		if (s.imagesLoaded !== undefined) s.imagesLoaded++;
        		if (s.imagesLoaded === s.imagesToLoad.length) {
        			if (s.params.updateOnImagesReady) s.update();
        			s.emit('onImagesReady', s);
        		}
        	}
        	for (var i = 0; i < s.imagesToLoad.length; i++) {
        		s.loadImage(s.imagesToLoad[i], (s.imagesToLoad[i].currentSrc || s.imagesToLoad[i].getAttribute('src')), true, _onReady);
        	}
        };
        
        /*=========================
          Autoplay
          ===========================*/
          s.autoplayTimeoutId = undefined;
          s.autoplaying = false;
          s.autoplayPaused = false;
          function autoplay() {
          	s.autoplayTimeoutId = setTimeout(function () {
          		if (s.params.loop) {
          			s.fixLoop();
          			s._slideNext();
          		}
          		else {
          			if (!s.isEnd) {
          				s._slideNext();
          			}
          			else {
          				if (!params.autoplayStopOnLast) {
          					s._slideTo(0);
          				}
          				else {
          					s.stopAutoplay();
          				}
          			}
          		}
          	}, s.params.autoplay);
          }
          s.startAutoplay = function () {
          	if (typeof s.autoplayTimeoutId !== 'undefined') return false;
          	if (!s.params.autoplay) return false;
          	if (s.autoplaying) return false;
          	s.autoplaying = true;
          	s.emit('onAutoplayStart', s);
          	autoplay();
          };
          s.stopAutoplay = function (internal) {
          	if (!s.autoplayTimeoutId) return;
          	if (s.autoplayTimeoutId) clearTimeout(s.autoplayTimeoutId);
          	s.autoplaying = false;
          	s.autoplayTimeoutId = undefined;
          	s.emit('onAutoplayStop', s);
          };
          s.pauseAutoplay = function (speed) {
          	if (s.autoplayPaused) return;
          	if (s.autoplayTimeoutId) clearTimeout(s.autoplayTimeoutId);
          	s.autoplayPaused = true;
          	if (speed === 0) {
          		s.autoplayPaused = false;
          		autoplay();
          	}
          	else {
          		s.wrapper.transitionEnd(function () {
          			if (!s) return;
          			s.autoplayPaused = false;
          			if (!s.autoplaying) {
          				s.stopAutoplay();
          			}
          			else {
          				autoplay();
          			}
          		});
          	}
          };
        /*=========================
          Min/Max Translate
          ===========================*/
          s.minTranslate = function () {
          	return (-s.snapGrid[0]);
          };
          s.maxTranslate = function () {
          	return (-s.snapGrid[s.snapGrid.length - 1]);
          };
        /*=========================
          Slider/slides sizes
          ===========================*/
          s.updateContainerSize = function () {
          	var width, height;
          	if (typeof s.params.width !== 'undefined') {
          		width = s.params.width;
          	}
          	else {
          		width = s.container[0].clientWidth;
          	}
          	if (typeof s.params.height !== 'undefined') {
          		height = s.params.height;
          	}
          	else {
          		height = s.container[0].clientHeight;
          	}
          	if (width === 0 && isH() || height === 0 && !isH()) {
          		return;
          	}

            //Subtract paddings
            width = width - parseInt(s.container.css('padding-left'), 10) - parseInt(s.container.css('padding-right'), 10);
            height = height - parseInt(s.container.css('padding-top'), 10) - parseInt(s.container.css('padding-bottom'), 10);
            
            // Store values
            s.width = width;
            s.height = height;
            s.size = isH() ? s.width : s.height;
        };
        
        s.updateSlidesSize = function () {
        	s.slides = s.wrapper.children('.' + s.params.slideClass);
        	s.snapGrid = [];
        	s.slidesGrid = [];
        	s.slidesSizesGrid = [];

        	var spaceBetween = s.params.spaceBetween,
        	slidePosition = -s.params.slidesOffsetBefore,
        	i,
        	prevSlideSize = 0,
        	index = 0;
        	if (typeof spaceBetween === 'string' && spaceBetween.indexOf('%') >= 0) {
        		spaceBetween = parseFloat(spaceBetween.replace('%', '')) / 100 * s.size;
        	}

        	s.virtualSize = -spaceBetween;
            // reset margins
            if (s.rtl) s.slides.css({marginLeft: '', marginTop: ''});
            else s.slides.css({marginRight: '', marginBottom: ''});

            var slidesNumberEvenToRows;
            if (s.params.slidesPerColumn > 1) {
            	if (Math.floor(s.slides.length / s.params.slidesPerColumn) === s.slides.length / s.params.slidesPerColumn) {
            		slidesNumberEvenToRows = s.slides.length;
            	}
            	else {
            		slidesNumberEvenToRows = Math.ceil(s.slides.length / s.params.slidesPerColumn) * s.params.slidesPerColumn;
            	}
            }

            // Calc slides
            var slideSize;
            var slidesPerColumn = s.params.slidesPerColumn;
            var slidesPerRow = slidesNumberEvenToRows / slidesPerColumn;
            var numFullColumns = slidesPerRow - (s.params.slidesPerColumn * slidesPerRow - s.slides.length);
            for (i = 0; i < s.slides.length; i++) {
            	slideSize = 0;
            	var slide = s.slides.eq(i);
            	if (s.params.slidesPerColumn > 1) {
                    // Set slides order
                    var newSlideOrderIndex;
                    var column, row;
                    if (s.params.slidesPerColumnFill === 'column') {
                    	column = Math.floor(i / slidesPerColumn);
                    	row = i - column * slidesPerColumn;
                    	if (column > numFullColumns || (column === numFullColumns && row === slidesPerColumn-1)) {
                    		if (++row >= slidesPerColumn) {
                    			row = 0;
                    			column++;
                    		}
                    	}
                    	newSlideOrderIndex = column + row * slidesNumberEvenToRows / slidesPerColumn;
                    	slide
                    	.css({
                    		'-webkit-box-ordinal-group': newSlideOrderIndex,
                    		'-moz-box-ordinal-group': newSlideOrderIndex,
                    		'-ms-flex-order': newSlideOrderIndex,
                    		'-webkit-order': newSlideOrderIndex,
                    		'order': newSlideOrderIndex
                    	});
                    }
                    else {
                    	row = Math.floor(i / slidesPerRow);
                    	column = i - row * slidesPerRow;
                    }
                    slide
                    .css({
                    	'margin-top': (row !== 0 && s.params.spaceBetween) && (s.params.spaceBetween + 'px')
                    })
                    .attr('data-swiper-column', column)
                    .attr('data-swiper-row', row);

                }
                if (slide.css('display') === 'none') continue;
                if (s.params.slidesPerView === 'auto') {
                	slideSize = isH() ? slide.outerWidth(true) : slide.outerHeight(true);
                	if (s.params.roundLengths) slideSize = round(slideSize);
                }
                else {
                	slideSize = (s.size - (s.params.slidesPerView - 1) * spaceBetween) / s.params.slidesPerView;
                	if (s.params.roundLengths) slideSize = round(slideSize);

                	if (isH()) {
                		s.slides[i].style.width = slideSize + 'px';
                	}
                	else {
                		s.slides[i].style.height = slideSize + 'px';
                	}
                }
                s.slides[i].swiperSlideSize = slideSize;
                s.slidesSizesGrid.push(slideSize);


                if (s.params.centeredSlides) {
                	slidePosition = slidePosition + slideSize / 2 + prevSlideSize / 2 + spaceBetween;
                	if (i === 0) slidePosition = slidePosition - s.size / 2 - spaceBetween;
                	if (Math.abs(slidePosition) < 1 / 1000) slidePosition = 0;
                	if ((index) % s.params.slidesPerGroup === 0) s.snapGrid.push(slidePosition);
                	s.slidesGrid.push(slidePosition);
                }
                else {
                	if ((index) % s.params.slidesPerGroup === 0) s.snapGrid.push(slidePosition);
                	s.slidesGrid.push(slidePosition);
                	slidePosition = slidePosition + slideSize + spaceBetween;
                }

                s.virtualSize += slideSize + spaceBetween;

                prevSlideSize = slideSize;

                index ++;
            }
            s.virtualSize = Math.max(s.virtualSize, s.size) + s.params.slidesOffsetAfter;

            var newSlidesGrid;

            if (
            	s.rtl && s.wrongRTL && (s.params.effect === 'slide' || s.params.effect === 'coverflow')) {
            	s.wrapper.css({width: s.virtualSize + s.params.spaceBetween + 'px'});
        }
        if (!s.support.flexbox || s.params.setWrapperSize) {
        	if (isH()) s.wrapper.css({width: s.virtualSize + s.params.spaceBetween + 'px'});
        	else s.wrapper.css({height: s.virtualSize + s.params.spaceBetween + 'px'});
        }
        
        if (s.params.slidesPerColumn > 1) {
        	s.virtualSize = (slideSize + s.params.spaceBetween) * slidesNumberEvenToRows;
        	s.virtualSize = Math.ceil(s.virtualSize / s.params.slidesPerColumn) - s.params.spaceBetween;
        	s.wrapper.css({width: s.virtualSize + s.params.spaceBetween + 'px'});
        	if (s.params.centeredSlides) {
        		newSlidesGrid = [];
        		for (i = 0; i < s.snapGrid.length; i++) {
        			if (s.snapGrid[i] < s.virtualSize + s.snapGrid[0]) newSlidesGrid.push(s.snapGrid[i]);
        		}
        		s.snapGrid = newSlidesGrid;
        	}
        }
        
            // Remove last grid elements depending on width
            if (!s.params.centeredSlides) {
            	newSlidesGrid = [];
            	for (i = 0; i < s.snapGrid.length; i++) {
            		if (s.snapGrid[i] <= s.virtualSize - s.size) {
            			newSlidesGrid.push(s.snapGrid[i]);
            		}
            	}
            	s.snapGrid = newSlidesGrid;
            	if (Math.floor(s.virtualSize - s.size) > Math.floor(s.snapGrid[s.snapGrid.length - 1])) {
            		s.snapGrid.push(s.virtualSize - s.size);
            	}
            }
            if (s.snapGrid.length === 0) s.snapGrid = [0];

            if (s.params.spaceBetween !== 0) {
            	if (isH()) {
            		if (s.rtl) s.slides.css({marginLeft: spaceBetween + 'px'});
            		else s.slides.css({marginRight: spaceBetween + 'px'});
            	}
            	else s.slides.css({marginBottom: spaceBetween + 'px'});
            }
            if (s.params.watchSlidesProgress) {
            	s.updateSlidesOffset();
            }
        };
        s.updateSlidesOffset = function () {
        	for (var i = 0; i < s.slides.length; i++) {
        		s.slides[i].swiperSlideOffset = isH() ? s.slides[i].offsetLeft : s.slides[i].offsetTop;
        	}
        };
        
        /*=========================
          Slider/slides progress
          ===========================*/
          s.updateSlidesProgress = function (translate) {
          	if (typeof translate === 'undefined') {
          		translate = s.translate || 0;
          	}
          	if (s.slides.length === 0) return;
          	if (typeof s.slides[0].swiperSlideOffset === 'undefined') s.updateSlidesOffset();

          	var offsetCenter = -translate;
          	if (s.rtl) offsetCenter = translate;

            // Visible Slides
            var containerBox = s.container[0].getBoundingClientRect();
            var sideBefore = isH() ? 'left' : 'top';
            var sideAfter = isH() ? 'right' : 'bottom';
            s.slides.removeClass(s.params.slideVisibleClass);
            for (var i = 0; i < s.slides.length; i++) {
            	var slide = s.slides[i];
            	var slideProgress = (offsetCenter - slide.swiperSlideOffset) / (slide.swiperSlideSize + s.params.spaceBetween);
            	if (s.params.watchSlidesVisibility) {
            		var slideBefore = -(offsetCenter - slide.swiperSlideOffset);
            		var slideAfter = slideBefore + s.slidesSizesGrid[i];
            		var isVisible =
            		(slideBefore >= 0 && slideBefore < s.size) ||
            		(slideAfter > 0 && slideAfter <= s.size) ||
            		(slideBefore <= 0 && slideAfter >= s.size);
            		if (isVisible) {
            			s.slides.eq(i).addClass(s.params.slideVisibleClass);
            		}
            	}
            	slide.progress = s.rtl ? -slideProgress : slideProgress;
            }
        };
        s.updateProgress = function (translate) {
        	if (typeof translate === 'undefined') {
        		translate = s.translate || 0;
        	}
        	var translatesDiff = s.maxTranslate() - s.minTranslate();
        	if (translatesDiff === 0) {
        		s.progress = 0;
        		s.isBeginning = s.isEnd = true;
        	}
        	else {
        		s.progress = (translate - s.minTranslate()) / (translatesDiff);
        		s.isBeginning = s.progress <= 0;
        		s.isEnd = s.progress >= 1;
        	}
        	if (s.isBeginning) s.emit('onReachBeginning', s);
        	if (s.isEnd) s.emit('onReachEnd', s);

        	if (s.params.watchSlidesProgress) s.updateSlidesProgress(translate);
        	s.emit('onProgress', s, s.progress);
        };
        s.updateActiveIndex = function () {
        	var translate = s.rtl ? s.translate : -s.translate;
        	var newActiveIndex, i, snapIndex;
        	for (i = 0; i < s.slidesGrid.length; i ++) {
        		if (typeof s.slidesGrid[i + 1] !== 'undefined') {
        			if (translate >= s.slidesGrid[i] && translate < s.slidesGrid[i + 1] - (s.slidesGrid[i + 1] - s.slidesGrid[i]) / 2) {
        				newActiveIndex = i;
        			}
        			else if (translate >= s.slidesGrid[i] && translate < s.slidesGrid[i + 1]) {
        				newActiveIndex = i + 1;
        			}
        		}
        		else {
        			if (translate >= s.slidesGrid[i]) {
        				newActiveIndex = i;
        			}
        		}
        	}
            // Normalize slideIndex
            if (newActiveIndex < 0 || typeof newActiveIndex === 'undefined') newActiveIndex = 0;
            // for (i = 0; i < s.slidesGrid.length; i++) {
                // if (- translate >= s.slidesGrid[i]) {
                    // newActiveIndex = i;
                // }
            // }
            snapIndex = Math.floor(newActiveIndex / s.params.slidesPerGroup);
            if (snapIndex >= s.snapGrid.length) snapIndex = s.snapGrid.length - 1;

            if (newActiveIndex === s.activeIndex) {
            	return;
            }
            s.snapIndex = snapIndex;
            s.previousIndex = s.activeIndex;
            s.activeIndex = newActiveIndex;
            s.updateClasses();
        };
        
        /*=========================
          Classes
          ===========================*/
          s.updateClasses = function () {
          	s.slides.removeClass(s.params.slideActiveClass + ' ' + s.params.slideNextClass + ' ' + s.params.slidePrevClass);
          	var activeSlide = s.slides.eq(s.activeIndex);
            // Active classes
            activeSlide.addClass(s.params.slideActiveClass);
            activeSlide.next('.' + s.params.slideClass).addClass(s.params.slideNextClass);
            activeSlide.prev('.' + s.params.slideClass).addClass(s.params.slidePrevClass);

            // Pagination
            if (s.bullets && s.bullets.length > 0) {
            	s.bullets.removeClass(s.params.bulletActiveClass);
            	var bulletIndex;
            	if (s.params.loop) {
            		bulletIndex = Math.ceil(s.activeIndex - s.loopedSlides)/s.params.slidesPerGroup;
            		if (bulletIndex > s.slides.length - 1 - s.loopedSlides * 2) {
            			bulletIndex = bulletIndex - (s.slides.length - s.loopedSlides * 2);
            		}
            		if (bulletIndex > s.bullets.length - 1) bulletIndex = bulletIndex - s.bullets.length;
            	}
            	else {
            		if (typeof s.snapIndex !== 'undefined') {
            			bulletIndex = s.snapIndex;
            		}
            		else {
            			bulletIndex = s.activeIndex || 0;
            		}
            	}
            	if (s.paginationContainer.length > 1) {
            		s.bullets.each(function () {
            			if ($(this).index() === bulletIndex) $(this).addClass(s.params.bulletActiveClass);
            		});
            	}
            	else {
            		s.bullets.eq(bulletIndex).addClass(s.params.bulletActiveClass);
            	}
            }

            // Next/active buttons
            if (!s.params.loop) {
            	if (s.params.prevButton) {
            		if (s.isBeginning) {
            			$(s.params.prevButton).addClass(s.params.buttonDisabledClass);
            			if (s.params.a11y && s.a11y) s.a11y.disable($(s.params.prevButton));
            		}
            		else {
            			$(s.params.prevButton).removeClass(s.params.buttonDisabledClass);
            			if (s.params.a11y && s.a11y) s.a11y.enable($(s.params.prevButton));
            		}
            	}
            	if (s.params.nextButton) {
            		if (s.isEnd) {
            			$(s.params.nextButton).addClass(s.params.buttonDisabledClass);
            			if (s.params.a11y && s.a11y) s.a11y.disable($(s.params.nextButton));
            		}
            		else {
            			$(s.params.nextButton).removeClass(s.params.buttonDisabledClass);
            			if (s.params.a11y && s.a11y) s.a11y.enable($(s.params.nextButton));
            		}
            	}
            }
        };
        
        /*=========================
          Pagination
          ===========================*/
          s.updatePagination = function () {
          	if (!s.params.pagination) return;
          	if (s.paginationContainer && s.paginationContainer.length > 0) {
          		var bulletsHTML = '';
          		var numberOfBullets = s.params.loop ? Math.ceil((s.slides.length - s.loopedSlides * 2) / s.params.slidesPerGroup) : s.snapGrid.length;
          		for (var i = 0; i < numberOfBullets; i++) {
          			if (s.params.paginationBulletRender) {
          				bulletsHTML += s.params.paginationBulletRender(i, s.params.bulletClass);
          			}
          			else {
          				bulletsHTML += '<' + s.params.paginationElement+' class="' + s.params.bulletClass + '"></' + s.params.paginationElement + '>';
          			}
          		}
          		s.paginationContainer.html(bulletsHTML);
          		s.bullets = s.paginationContainer.find('.' + s.params.bulletClass);
          		if (s.params.paginationClickable && s.params.a11y && s.a11y) {
          			s.a11y.initPagination();
          		}
          	}
          };
        /*=========================
          Common update method
          ===========================*/
          s.update = function (updateTranslate) {
          	s.updateContainerSize();
          	s.updateSlidesSize();
          	s.updateProgress();
          	s.updatePagination();
          	s.updateClasses();
          	if (s.params.scrollbar && s.scrollbar) {
          		s.scrollbar.set();
          	}
          	function forceSetTranslate() {
          		newTranslate = Math.min(Math.max(s.translate, s.maxTranslate()), s.minTranslate());
          		s.setWrapperTranslate(newTranslate);
          		s.updateActiveIndex();
          		s.updateClasses();
          	}
          	if (updateTranslate) {
          		var translated, newTranslate;
          		if (s.controller && s.controller.spline) {
          			s.controller.spline = undefined;
          		}
          		if (s.params.freeMode) {
          			forceSetTranslate();
          		}
          		else {
          			if ((s.params.slidesPerView === 'auto' || s.params.slidesPerView > 1) && s.isEnd && !s.params.centeredSlides) {
          				translated = s.slideTo(s.slides.length - 1, 0, false, true);
          			}
          			else {
          				translated = s.slideTo(s.activeIndex, 0, false, true);
          			}
          			if (!translated) {
          				forceSetTranslate();
          			}
          		}

          	}
          };

        /*=========================
          Resize Handler
          ===========================*/
          s.onResize = function (forceUpdatePagination) {
            // Disable locks on resize
            var allowSwipeToPrev = s.params.allowSwipeToPrev;
            var allowSwipeToNext = s.params.allowSwipeToNext;
            s.params.allowSwipeToPrev = s.params.allowSwipeToNext = true;

            s.updateContainerSize();
            s.updateSlidesSize();
            if (s.params.slidesPerView === 'auto' || s.params.freeMode || forceUpdatePagination) s.updatePagination();
            if (s.params.scrollbar && s.scrollbar) {
            	s.scrollbar.set();
            }
            if (s.controller && s.controller.spline) {
            	s.controller.spline = undefined;
            }
            if (s.params.freeMode) {
            	var newTranslate = Math.min(Math.max(s.translate, s.maxTranslate()), s.minTranslate());
            	s.setWrapperTranslate(newTranslate);
            	s.updateActiveIndex();
            	s.updateClasses();
            }
            else {
            	s.updateClasses();
            	if ((s.params.slidesPerView === 'auto' || s.params.slidesPerView > 1) && s.isEnd && !s.params.centeredSlides) {
            		s.slideTo(s.slides.length - 1, 0, false, true);
            	}
            	else {
            		s.slideTo(s.activeIndex, 0, false, true);
            	}
            }
            // Return locks after resize
            s.params.allowSwipeToPrev = allowSwipeToPrev;
            s.params.allowSwipeToNext = allowSwipeToNext;
        };
        
        /*=========================
          Events
          ===========================*/

        //Define Touch Events
        var desktopEvents = ['mousedown', 'mousemove', 'mouseup'];
        if (window.navigator.pointerEnabled) desktopEvents = ['pointerdown', 'pointermove', 'pointerup'];
        else if (window.navigator.msPointerEnabled) desktopEvents = ['MSPointerDown', 'MSPointerMove', 'MSPointerUp'];
        s.touchEvents = {
        	start : s.support.touch || !s.params.simulateTouch  ? 'touchstart' : desktopEvents[0],
        	move : s.support.touch || !s.params.simulateTouch ? 'touchmove' : desktopEvents[1],
        	end : s.support.touch || !s.params.simulateTouch ? 'touchend' : desktopEvents[2]
        };
        
        
        // WP8 Touch Events Fix
        if (window.navigator.pointerEnabled || window.navigator.msPointerEnabled) {
        	(s.params.touchEventsTarget === 'container' ? s.container : s.wrapper).addClass('swiper-wp8-' + s.params.direction);
        }
        
        // Attach/detach events
        s.initEvents = function (detach) {
        	var actionDom = detach ? 'off' : 'on';
        	var action = detach ? 'removeEventListener' : 'addEventListener';
        	var touchEventsTarget = s.params.touchEventsTarget === 'container' ? s.container[0] : s.wrapper[0];
        	var target = s.support.touch ? touchEventsTarget : document;

        	var moveCapture = s.params.nested ? true : false;

            //Touch Events
            if (s.browser.ie) {
            	touchEventsTarget[action](s.touchEvents.start, s.onTouchStart, false);
            	target[action](s.touchEvents.move, s.onTouchMove, moveCapture);
            	target[action](s.touchEvents.end, s.onTouchEnd, false);
            }
            else {
            	if (s.support.touch) {
            		touchEventsTarget[action](s.touchEvents.start, s.onTouchStart, false);
            		touchEventsTarget[action](s.touchEvents.move, s.onTouchMove, moveCapture);
            		touchEventsTarget[action](s.touchEvents.end, s.onTouchEnd, false);
            	}
            	if (params.simulateTouch && !s.device.ios && !s.device.android) {
            		touchEventsTarget[action]('mousedown', s.onTouchStart, false);
            		document[action]('mousemove', s.onTouchMove, moveCapture);
            		document[action]('mouseup', s.onTouchEnd, false);
            	}
            }
            window[action]('resize', s.onResize);

            // Next, Prev, Index
            if (s.params.nextButton) {
            	$(s.params.nextButton)[actionDom]('click', s.onClickNext);
            	if (s.params.a11y && s.a11y) $(s.params.nextButton)[actionDom]('keydown', s.a11y.onEnterKey);
            }
            if (s.params.prevButton) {
            	$(s.params.prevButton)[actionDom]('click', s.onClickPrev);
            	if (s.params.a11y && s.a11y) $(s.params.prevButton)[actionDom]('keydown', s.a11y.onEnterKey);
            }
            if (s.params.pagination && s.params.paginationClickable) {
            	$(s.paginationContainer)[actionDom]('click', '.' + s.params.bulletClass, s.onClickIndex);
            	if (s.params.a11y && s.a11y) $(s.paginationContainer)[actionDom]('keydown', '.' + s.params.bulletClass, s.a11y.onEnterKey);
            }

            // Prevent Links Clicks
            if (s.params.preventClicks || s.params.preventClicksPropagation) touchEventsTarget[action]('click', s.preventClicks, true);
        };
        s.attachEvents = function (detach) {
        	s.initEvents();
        };
        s.detachEvents = function () {
        	s.initEvents(true);
        };
        
        /*=========================
          Handle Clicks
          ===========================*/
        // Prevent Clicks
        s.allowClick = true;
        s.preventClicks = function (e) {
        	if (!s.allowClick) {
        		if (s.params.preventClicks) e.preventDefault();
        		if (s.params.preventClicksPropagation && s.animating) {
        			e.stopPropagation();
        			e.stopImmediatePropagation();
        		}
        	}
        };
        // Clicks
        s.onClickNext = function (e) {
        	e.preventDefault();
        	if (s.isEnd && !s.params.loop) return;
        	s.slideNext();
        };
        s.onClickPrev = function (e) {
        	e.preventDefault();
        	if (s.isBeginning && !s.params.loop) return;
        	s.slidePrev();
        };
        s.onClickIndex = function (e) {
        	e.preventDefault();
        	var index = $(this).index() * s.params.slidesPerGroup;
        	if (s.params.loop) index = index + s.loopedSlides;
        	s.slideTo(index);
        };
        
        /*=========================
          Handle Touches
          ===========================*/
          function findElementInEvent(e, selector) {
          	var el = $(e.target);
          	if (!el.is(selector)) {
          		if (typeof selector === 'string') {
          			el = el.parents(selector);
          		}
          		else if (selector.nodeType) {
          			var found;
          			el.parents().each(function (index, _el) {
          				if (_el === selector) found = selector;
          			});
          			if (!found) return undefined;
          			else return selector;
          		}
          	}
          	if (el.length === 0) {
          		return undefined;
          	}
          	return el[0];
          }
          s.updateClickedSlide = function (e) {
          	var slide = findElementInEvent(e, '.' + s.params.slideClass);
          	var slideFound = false;
          	if (slide) {
          		for (var i = 0; i < s.slides.length; i++) {
          			if (s.slides[i] === slide) slideFound = true;
          		}
          	}

          	if (slide && slideFound) {
          		s.clickedSlide = slide;
          		s.clickedIndex = $(slide).index();
          	}
          	else {
          		s.clickedSlide = undefined;
          		s.clickedIndex = undefined;
          		return;
          	}
          	if (s.params.slideToClickedSlide && s.clickedIndex !== undefined && s.clickedIndex !== s.activeIndex) {
          		var slideToIndex = s.clickedIndex,
          		realIndex;
          		if (s.params.loop) {
          			realIndex = $(s.clickedSlide).attr('data-swiper-slide-index');
          			if (slideToIndex > s.slides.length - s.params.slidesPerView) {
          				s.fixLoop();
          				slideToIndex = s.wrapper.children('.' + s.params.slideClass + '[data-swiper-slide-index="' + realIndex + '"]').eq(0).index();
          				setTimeout(function () {
          					s.slideTo(slideToIndex);
          				}, 0);
          			}
          			else if (slideToIndex < s.params.slidesPerView - 1) {
          				s.fixLoop();
          				var duplicatedSlides = s.wrapper.children('.' + s.params.slideClass + '[data-swiper-slide-index="' + realIndex + '"]');
          				slideToIndex = duplicatedSlides.eq(duplicatedSlides.length - 1).index();
          				setTimeout(function () {
          					s.slideTo(slideToIndex);
          				}, 0);
          			}
          			else {
          				s.slideTo(slideToIndex);
          			}
          		}
          		else {
          			s.slideTo(slideToIndex);
          		}
          	}
          };

          var isTouched,
          isMoved,
          touchStartTime,
          isScrolling,
          currentTranslate,
          startTranslate,
          allowThresholdMove,
            // Form elements to match
            formElements = 'input, select, textarea, button',
            // Last click time
            lastClickTime = Date.now(), clickTimeout,
            //Velocities
            velocities = [],
            allowMomentumBounce;

        // Animating Flag
        s.animating = false;
        
        // Touches information
        s.touches = {
        	startX: 0,
        	startY: 0,
        	currentX: 0,
        	currentY: 0,
        	diff: 0
        };
        
        // Touch handlers
        var isTouchEvent, startMoving;
        s.onTouchStart = function (e) {
        	if (e.originalEvent) e = e.originalEvent;
        	isTouchEvent = e.type === 'touchstart';
        	if (!isTouchEvent && 'which' in e && e.which === 3) return;
        	if (s.params.noSwiping && findElementInEvent(e, '.' + s.params.noSwipingClass)) {
        		s.allowClick = true;
        		return;
        	}
        	if (s.params.swipeHandler) {
        		if (!findElementInEvent(e, s.params.swipeHandler)) return;
        	}

        	var startX = s.touches.currentX = e.type === 'touchstart' ? e.targetTouches[0].pageX : e.pageX;
        	var startY = s.touches.currentY = e.type === 'touchstart' ? e.targetTouches[0].pageY : e.pageY;

            // Do NOT start if iOS edge swipe is detected. Otherwise iOS app (UIWebView) cannot swipe-to-go-back anymore
            if(s.device.ios && s.params.iOSEdgeSwipeDetection && startX <= s.params.iOSEdgeSwipeThreshold) {
            	return;
            }

            isTouched = true;
            isMoved = false;
            isScrolling = undefined;
            startMoving = undefined;
            s.touches.startX = startX;
            s.touches.startY = startY;
            touchStartTime = Date.now();
            s.allowClick = true;
            s.updateContainerSize();
            s.swipeDirection = undefined;
            if (s.params.threshold > 0) allowThresholdMove = false;
            if (e.type !== 'touchstart') {
            	var preventDefault = true;
            	if ($(e.target).is(formElements)) preventDefault = false;
            	if (document.activeElement && $(document.activeElement).is(formElements)) {
            		document.activeElement.blur();
            	}
            	if (preventDefault) {
            		e.preventDefault();
            	}
            }
            s.emit('onTouchStart', s, e);
        };
        
        s.onTouchMove = function (e) {
        	if (e.originalEvent) e = e.originalEvent;
        	if (isTouchEvent && e.type === 'mousemove') return;
        	if (e.preventedByNestedSwiper) return;
        	if (s.params.onlyExternal) {
                // isMoved = true;
                s.allowClick = false;
                if (isTouched) {
                	s.touches.startX = s.touches.currentX = e.type === 'touchmove' ? e.targetTouches[0].pageX : e.pageX;
                	s.touches.startY = s.touches.currentY = e.type === 'touchmove' ? e.targetTouches[0].pageY : e.pageY;
                	touchStartTime = Date.now();
                }
                return;
            }
            if (isTouchEvent && document.activeElement) {
            	if (e.target === document.activeElement && $(e.target).is(formElements)) {
            		isMoved = true;
            		s.allowClick = false;
            		return;
            	}
            }

            s.emit('onTouchMove', s, e);

            if (e.targetTouches && e.targetTouches.length > 1) return;

            s.touches.currentX = e.type === 'touchmove' ? e.targetTouches[0].pageX : e.pageX;
            s.touches.currentY = e.type === 'touchmove' ? e.targetTouches[0].pageY : e.pageY;

            if (typeof isScrolling === 'undefined') {
            	var touchAngle = Math.atan2(Math.abs(s.touches.currentY - s.touches.startY), Math.abs(s.touches.currentX - s.touches.startX)) * 180 / Math.PI;
            	isScrolling = isH() ? touchAngle > s.params.touchAngle : (90 - touchAngle > s.params.touchAngle);
            }
            if (isScrolling) {
            	s.emit('onTouchMoveOpposite', s, e);
            }
            if (typeof startMoving === 'undefined' && s.browser.ieTouch) {
            	if (s.touches.currentX !== s.touches.startX || s.touches.currentY !== s.touches.startY) {
            		startMoving = true;
            	}
            }
            if (!isTouched) return;
            if (isScrolling)  {
            	isTouched = false;
            	return;
            }
            if (!startMoving && s.browser.ieTouch) {
            	return;
            }
            s.allowClick = false;
            s.emit('onSliderMove', s, e);
            e.preventDefault();
            if (s.params.touchMoveStopPropagation && !s.params.nested) {
            	e.stopPropagation();
            }

            if (!isMoved) {
            	if (params.loop) {
            		s.fixLoop();
            	}
            	startTranslate = s.getWrapperTranslate();
            	s.setWrapperTransition(0);
            	if (s.animating) {
            		s.wrapper.trigger('webkitTransitionEnd transitionend oTransitionEnd MSTransitionEnd msTransitionEnd');
            	}
            	if (s.params.autoplay && s.autoplaying) {
            		if (s.params.autoplayDisableOnInteraction) {
            			s.stopAutoplay();
            		}
            		else {
            			s.pauseAutoplay();
            		}
            	}
            	allowMomentumBounce = false;
                //Grab Cursor
                if (s.params.grabCursor) {
                	s.container[0].style.cursor = 'move';
                	s.container[0].style.cursor = '-webkit-grabbing';
                	s.container[0].style.cursor = '-moz-grabbin';
                	s.container[0].style.cursor = 'grabbing';
                }
            }
            isMoved = true;

            var diff = s.touches.diff = isH() ? s.touches.currentX - s.touches.startX : s.touches.currentY - s.touches.startY;
            
            diff = diff * s.params.touchRatio;
            if (s.rtl) diff = -diff;

            s.swipeDirection = diff > 0 ? 'prev' : 'next';
            currentTranslate = diff + startTranslate;

            var disableParentSwiper = true;
            if ((diff > 0 && currentTranslate > s.minTranslate())) {
            	disableParentSwiper = false;
            	if (s.params.resistance) currentTranslate = s.minTranslate() - 1 + Math.pow(-s.minTranslate() + startTranslate + diff, s.params.resistanceRatio);
            }
            else if (diff < 0 && currentTranslate < s.maxTranslate()) {
            	disableParentSwiper = false;
            	if (s.params.resistance) currentTranslate = s.maxTranslate() + 1 - Math.pow(s.maxTranslate() - startTranslate - diff, s.params.resistanceRatio);
            }

            if (disableParentSwiper) {
            	e.preventedByNestedSwiper = true;
            }

            // Directions locks
            if (!s.params.allowSwipeToNext && s.swipeDirection === 'next' && currentTranslate < startTranslate) {
            	currentTranslate = startTranslate;
            }
            if (!s.params.allowSwipeToPrev && s.swipeDirection === 'prev' && currentTranslate > startTranslate) {
            	currentTranslate = startTranslate;
            }

            if (!s.params.followFinger) return;

            // Threshold
            if (s.params.threshold > 0) {
            	if (Math.abs(diff) > s.params.threshold || allowThresholdMove) {
            		if (!allowThresholdMove) {
            			allowThresholdMove = true;
            			s.touches.startX = s.touches.currentX;
            			s.touches.startY = s.touches.currentY;
            			currentTranslate = startTranslate;
            			s.touches.diff = isH() ? s.touches.currentX - s.touches.startX : s.touches.currentY - s.touches.startY;
            			return;
            		}
            	}
            	else {
            		currentTranslate = startTranslate;
            		return;
            	}
            }
            // Update active index in free mode
            if (s.params.freeMode || s.params.watchSlidesProgress) {
            	s.updateActiveIndex();
            }
            if (s.params.freeMode) {
                //Velocity
                if (velocities.length === 0) {
                	velocities.push({
                		position: s.touches[isH() ? 'startX' : 'startY'],
                		time: touchStartTime
                	});
                }
                velocities.push({
                	position: s.touches[isH() ? 'currentX' : 'currentY'],
                	time: (new window.Date()).getTime()
                });
            }
            // Update progress
            s.updateProgress(currentTranslate);
            // Update translate
            s.setWrapperTranslate(currentTranslate);
        };
        s.onTouchEnd = function (e) {
        	if (e.originalEvent) e = e.originalEvent;
        	s.emit('onTouchEnd', s, e);
        	if (!isTouched) return;
            //Return Grab Cursor
            if (s.params.grabCursor && isMoved && isTouched) {
            	s.container[0].style.cursor = 'move';
            	s.container[0].style.cursor = '-webkit-grab';
            	s.container[0].style.cursor = '-moz-grab';
            	s.container[0].style.cursor = 'grab';
            }

            // Time diff
            var touchEndTime = Date.now();
            var timeDiff = touchEndTime - touchStartTime;

            // Tap, doubleTap, Click
            if (s.allowClick) {
            	s.updateClickedSlide(e);
            	s.emit('onTap', s, e);
            	if (timeDiff < 300 && (touchEndTime - lastClickTime) > 300) {
            		if (clickTimeout) clearTimeout(clickTimeout);
            		clickTimeout = setTimeout(function () {
            			if (!s) return;
            			if (s.params.paginationHide && s.paginationContainer.length > 0 && !$(e.target).hasClass(s.params.bulletClass)) {
            				s.paginationContainer.toggleClass(s.params.paginationHiddenClass);
            			}
            			s.emit('onClick', s, e);
            		}, 300);

            	}
            	if (timeDiff < 300 && (touchEndTime - lastClickTime) < 300) {
            		if (clickTimeout) clearTimeout(clickTimeout);
            		s.emit('onDoubleTap', s, e);
            	}
            }

            lastClickTime = Date.now();
            setTimeout(function () {
            	if (s) s.allowClick = true;
            }, 0);

            if (!isTouched || !isMoved || !s.swipeDirection || s.touches.diff === 0 || currentTranslate === startTranslate) {
            	isTouched = isMoved = false;
            	return;
            }
            isTouched = isMoved = false;

            var currentPos;
            if (s.params.followFinger) {
            	currentPos = s.rtl ? s.translate : -s.translate;
            }
            else {
            	currentPos = -currentTranslate;
            }
            if (s.params.freeMode) {
            	if (currentPos < -s.minTranslate()) {
            		s.slideTo(s.activeIndex);
            		return;
            	}
            	else if (currentPos > -s.maxTranslate()) {
            		if (s.slides.length < s.snapGrid.length) {
            			s.slideTo(s.snapGrid.length - 1);
            		}
            		else {
            			s.slideTo(s.slides.length - 1);
            		}
            		return;
            	}

            	if (s.params.freeModeMomentum) {
            		if (velocities.length > 1) {
            			var lastMoveEvent = velocities.pop(), velocityEvent = velocities.pop();

            			var distance = lastMoveEvent.position - velocityEvent.position;
            			var time = lastMoveEvent.time - velocityEvent.time;
            			s.velocity = distance / time;
            			s.velocity = s.velocity / 2;
            			if (Math.abs(s.velocity) < 0.02) {
            				s.velocity = 0;
            			}
                        // this implies that the user stopped moving a finger then released.
                        // There would be no events with distance zero, so the last event is stale.
                        if (time > 150 || (new window.Date().getTime() - lastMoveEvent.time) > 300) {
                        	s.velocity = 0;
                        }
                    } else {
                    	s.velocity = 0;
                    }

                    velocities.length = 0;
                    var momentumDuration = 1000 * s.params.freeModeMomentumRatio;
                    var momentumDistance = s.velocity * momentumDuration;

                    var newPosition = s.translate + momentumDistance;
                    if (s.rtl) newPosition = - newPosition;
                    var doBounce = false;
                    var afterBouncePosition;
                    var bounceAmount = Math.abs(s.velocity) * 20 * s.params.freeModeMomentumBounceRatio;
                    if (newPosition < s.maxTranslate()) {
                    	if (s.params.freeModeMomentumBounce) {
                    		if (newPosition + s.maxTranslate() < -bounceAmount) {
                    			newPosition = s.maxTranslate() - bounceAmount;
                    		}
                    		afterBouncePosition = s.maxTranslate();
                    		doBounce = true;
                    		allowMomentumBounce = true;
                    	}
                    	else {
                    		newPosition = s.maxTranslate();
                    	}
                    }
                    else if (newPosition > s.minTranslate()) {
                    	if (s.params.freeModeMomentumBounce) {
                    		if (newPosition - s.minTranslate() > bounceAmount) {
                    			newPosition = s.minTranslate() + bounceAmount;
                    		}
                    		afterBouncePosition = s.minTranslate();
                    		doBounce = true;
                    		allowMomentumBounce = true;
                    	}
                    	else {
                    		newPosition = s.minTranslate();
                    	}
                    }
                    else if (s.params.freeModeSticky) {
                    	var j = 0,
                    	nextSlide;
                    	for (j = 0; j < s.snapGrid.length; j += 1) {
                    		if (s.snapGrid[j] > -newPosition) {
                    			nextSlide = j;
                    			break;
                    		}

                    	}
                    	if (Math.abs(s.snapGrid[nextSlide] - newPosition) < Math.abs(s.snapGrid[nextSlide - 1] - newPosition) || s.swipeDirection === 'next') {
                    		newPosition = s.snapGrid[nextSlide];
                    	} else {
                    		newPosition = s.snapGrid[nextSlide - 1];
                    	}
                    	if (!s.rtl) newPosition = - newPosition;
                    }
                    //Fix duration
                    if (s.velocity !== 0) {
                    	if (s.rtl) {
                    		momentumDuration = Math.abs((-newPosition - s.translate) / s.velocity);
                    	}
                    	else {
                    		momentumDuration = Math.abs((newPosition - s.translate) / s.velocity);
                    	}
                    }
                    else if (s.params.freeModeSticky) {
                    	s.slideReset();
                    	return;
                    }

                    if (s.params.freeModeMomentumBounce && doBounce) {
                    	s.updateProgress(afterBouncePosition);
                    	s.setWrapperTransition(momentumDuration);
                    	s.setWrapperTranslate(newPosition);
                    	s.onTransitionStart();
                    	s.animating = true;
                    	s.wrapper.transitionEnd(function () {
                    		if (!s || !allowMomentumBounce) return;
                    		s.emit('onMomentumBounce', s);

                    		s.setWrapperTransition(s.params.speed);
                    		s.setWrapperTranslate(afterBouncePosition);
                    		s.wrapper.transitionEnd(function () {
                    			if (!s) return;
                    			s.onTransitionEnd();
                    		});
                    	});
                    } else if (s.velocity) {
                    	s.updateProgress(newPosition);
                    	s.setWrapperTransition(momentumDuration);
                    	s.setWrapperTranslate(newPosition);
                    	s.onTransitionStart();
                    	if (!s.animating) {
                    		s.animating = true;
                    		s.wrapper.transitionEnd(function () {
                    			if (!s) return;
                    			s.onTransitionEnd();
                    		});
                    	}

                    } else {
                    	s.updateProgress(newPosition);
                    }

                    s.updateActiveIndex();
                }
                if (!s.params.freeModeMomentum || timeDiff >= s.params.longSwipesMs) {
                	s.updateProgress();
                	s.updateActiveIndex();
                }
                return;
            }

            // Find current slide
            var i, stopIndex = 0, groupSize = s.slidesSizesGrid[0];
            for (i = 0; i < s.slidesGrid.length; i += s.params.slidesPerGroup) {
            	if (typeof s.slidesGrid[i + s.params.slidesPerGroup] !== 'undefined') {
            		if (currentPos >= s.slidesGrid[i] && currentPos < s.slidesGrid[i + s.params.slidesPerGroup]) {
            			stopIndex = i;
            			groupSize = s.slidesGrid[i + s.params.slidesPerGroup] - s.slidesGrid[i];
            		}
            	}
            	else {
            		if (currentPos >= s.slidesGrid[i]) {
            			stopIndex = i;
            			groupSize = s.slidesGrid[s.slidesGrid.length - 1] - s.slidesGrid[s.slidesGrid.length - 2];
            		}
            	}
            }

            // Find current slide size
            var ratio = (currentPos - s.slidesGrid[stopIndex]) / groupSize;

            if (timeDiff > s.params.longSwipesMs) {
                // Long touches
                if (!s.params.longSwipes) {
                	s.slideTo(s.activeIndex);
                	return;
                }
                if (s.swipeDirection === 'next') {
                	if (ratio >= s.params.longSwipesRatio) s.slideTo(stopIndex + s.params.slidesPerGroup);
                	else s.slideTo(stopIndex);

                }
                if (s.swipeDirection === 'prev') {
                	if (ratio > (1 - s.params.longSwipesRatio)) s.slideTo(stopIndex + s.params.slidesPerGroup);
                	else s.slideTo(stopIndex);
                }
            }
            else {
                // Short swipes
                if (!s.params.shortSwipes) {
                	s.slideTo(s.activeIndex);
                	return;
                }
                if (s.swipeDirection === 'next') {
                	s.slideTo(stopIndex + s.params.slidesPerGroup);

                }
                if (s.swipeDirection === 'prev') {
                	s.slideTo(stopIndex);
                }
            }
        };
        /*=========================
          Transitions
          ===========================*/
          s._slideTo = function (slideIndex, speed) {
          	return s.slideTo(slideIndex, speed, true, true);
          };
          s.slideTo = function (slideIndex, speed, runCallbacks, internal) {
          	if (typeof runCallbacks === 'undefined') runCallbacks = true;
          	if (typeof slideIndex === 'undefined') slideIndex = 0;
          	if (slideIndex < 0) slideIndex = 0;
          	s.snapIndex = Math.floor(slideIndex / s.params.slidesPerGroup);
          	if (s.snapIndex >= s.snapGrid.length) s.snapIndex = s.snapGrid.length - 1;

          	var translate = - s.snapGrid[s.snapIndex];

            // Stop autoplay
            if (s.params.autoplay && s.autoplaying) {
            	if (internal || !s.params.autoplayDisableOnInteraction) {
            		s.pauseAutoplay(speed);
            	}
            	else {
            		s.stopAutoplay();
            	}
            }
            // Update progress
            s.updateProgress(translate);

            // Normalize slideIndex
            for (var i = 0; i < s.slidesGrid.length; i++) {
            	if (- Math.floor(translate * 100) >= Math.floor(s.slidesGrid[i] * 100)) {
            		slideIndex = i;
            	}
            }

            // Directions locks
            if (!s.params.allowSwipeToNext && translate < s.translate && translate < s.minTranslate()) {
            	return false;
            }
            if (!s.params.allowSwipeToPrev && translate > s.translate && translate > s.maxTranslate()) {
            	if ((s.activeIndex || 0) !== slideIndex ) return false;
            }

            // Update Index
            if (typeof speed === 'undefined') speed = s.params.speed;
            s.previousIndex = s.activeIndex || 0;
            s.activeIndex = slideIndex;

            if (translate === s.translate) {
            	s.updateClasses();
            	return false;
            }
            s.updateClasses();
            s.onTransitionStart(runCallbacks);
            var translateX = isH() ? translate : 0, translateY = isH() ? 0 : translate;
            if (speed === 0) {
            	s.setWrapperTransition(0);
            	s.setWrapperTranslate(translate);
            	s.onTransitionEnd(runCallbacks);
            }
            else {
            	s.setWrapperTransition(speed);
            	s.setWrapperTranslate(translate);
            	if (!s.animating) {
            		s.animating = true;
            		s.wrapper.transitionEnd(function () {
            			if (!s) return;
            			s.onTransitionEnd(runCallbacks);
            		});
            	}

            }

            return true;
        };
        
        s.onTransitionStart = function (runCallbacks) {
        	if (typeof runCallbacks === 'undefined') runCallbacks = true;
        	if (s.lazy) s.lazy.onTransitionStart();
        	if (runCallbacks) {
        		s.emit('onTransitionStart', s);
        		if (s.activeIndex !== s.previousIndex) {
        			s.emit('onSlideChangeStart', s);
        		}
        	}
        };
        s.onTransitionEnd = function (runCallbacks) {
        	s.animating = false;
        	s.setWrapperTransition(0);
        	if (typeof runCallbacks === 'undefined') runCallbacks = true;
        	if (s.lazy) s.lazy.onTransitionEnd();
        	if (runCallbacks) {
        		s.emit('onTransitionEnd', s);
        		if (s.activeIndex !== s.previousIndex) {
        			s.emit('onSlideChangeEnd', s);
        		}
        	}
        	if (s.params.hashnav && s.hashnav) {
        		s.hashnav.setHash();
        	}

        };
        s.slideNext = function (runCallbacks, speed, internal) {
        	if (s.params.loop) {
        		if (s.animating) return false;
        		s.fixLoop();
        		var clientLeft = s.container[0].clientLeft;
        		return s.slideTo(s.activeIndex + s.params.slidesPerGroup, speed, runCallbacks, internal);
        	}
        	else return s.slideTo(s.activeIndex + s.params.slidesPerGroup, speed, runCallbacks, internal);
        };
        s._slideNext = function (speed) {
        	return s.slideNext(true, speed, true);
        };
        s.slidePrev = function (runCallbacks, speed, internal) {
        	if (s.params.loop) {
        		if (s.animating) return false;
        		s.fixLoop();
        		var clientLeft = s.container[0].clientLeft;
        		return s.slideTo(s.activeIndex - 1, speed, runCallbacks, internal);
        	}
        	else return s.slideTo(s.activeIndex - 1, speed, runCallbacks, internal);
        };
        s._slidePrev = function (speed) {
        	return s.slidePrev(true, speed, true);
        };
        s.slideReset = function (runCallbacks, speed, internal) {
        	return s.slideTo(s.activeIndex, speed, runCallbacks);
        };
        
        /*=========================
          Translate/transition helpers
          ===========================*/
          s.setWrapperTransition = function (duration, byController) {
          	s.wrapper.transition(duration);
          	if (s.params.effect !== 'slide' && s.effects[s.params.effect]) {
          		s.effects[s.params.effect].setTransition(duration);
          	}
          	if (s.params.parallax && s.parallax) {
          		s.parallax.setTransition(duration);
          	}
          	if (s.params.scrollbar && s.scrollbar) {
          		s.scrollbar.setTransition(duration);
          	}
          	if (s.params.control && s.controller) {
          		s.controller.setTransition(duration, byController);
          	}
          	s.emit('onSetTransition', s, duration);
          };
          s.setWrapperTranslate = function (translate, updateActiveIndex, byController) {
          	var x = 0, y = 0, z = 0;
          	if (isH()) {
          		x = s.rtl ? -translate : translate;
          	}
          	else {
          		y = translate;
          	}
          	if (!s.params.virtualTranslate) {
          		if (s.support.transforms3d) s.wrapper.transform('translate3d(' + x + 'px, ' + y + 'px, ' + z + 'px)');
          		else s.wrapper.transform('translate(' + x + 'px, ' + y + 'px)');
          	}

          	s.translate = isH() ? x : y;

          	if (updateActiveIndex) s.updateActiveIndex();
          	if (s.params.effect !== 'slide' && s.effects[s.params.effect]) {
          		s.effects[s.params.effect].setTranslate(s.translate);
          	}
          	if (s.params.parallax && s.parallax) {
          		s.parallax.setTranslate(s.translate);
          	}
          	if (s.params.scrollbar && s.scrollbar) {
          		s.scrollbar.setTranslate(s.translate);
          	}
          	if (s.params.control && s.controller) {
          		s.controller.setTranslate(s.translate, byController);
          	}
          	s.emit('onSetTranslate', s, s.translate);
          };

          s.getTranslate = function (el, axis) {
          	var matrix, curTransform, curStyle, transformMatrix;

            // automatic axis detection
            if (typeof axis === 'undefined') {
            	axis = 'x';
            }

            if (s.params.virtualTranslate) {
            	return s.rtl ? -s.translate : s.translate;
            }

            curStyle = window.getComputedStyle(el, null);
            if (window.WebKitCSSMatrix) {
                // Some old versions of Webkit choke when 'none' is passed; pass
                // empty string instead in this case
                transformMatrix = new window.WebKitCSSMatrix(curStyle.webkitTransform === 'none' ? '' : curStyle.webkitTransform);
            }
            else {
            	transformMatrix = curStyle.MozTransform || curStyle.OTransform || curStyle.MsTransform || curStyle.msTransform  || curStyle.transform || curStyle.getPropertyValue('transform').replace('translate(', 'matrix(1, 0, 0, 1,');
            		matrix = transformMatrix.toString().split(',');
            	}

            	if (axis === 'x') {
                //Latest Chrome and webkits Fix
                if (window.WebKitCSSMatrix)
                	curTransform = transformMatrix.m41;
                //Crazy IE10 Matrix
                else if (matrix.length === 16)
                	curTransform = parseFloat(matrix[12]);
                //Normal Browsers
                else
                	curTransform = parseFloat(matrix[4]);
            }
            if (axis === 'y') {
                //Latest Chrome and webkits Fix
                if (window.WebKitCSSMatrix)
                	curTransform = transformMatrix.m42;
                //Crazy IE10 Matrix
                else if (matrix.length === 16)
                	curTransform = parseFloat(matrix[13]);
                //Normal Browsers
                else
                	curTransform = parseFloat(matrix[5]);
            }
            if (s.rtl && curTransform) curTransform = -curTransform;
            return curTransform || 0;
        };
        s.getWrapperTranslate = function (axis) {
        	if (typeof axis === 'undefined') {
        		axis = isH() ? 'x' : 'y';
        	}
        	return s.getTranslate(s.wrapper[0], axis);
        };
        
        /*=========================
          Observer
          ===========================*/
          s.observers = [];
          function initObserver(target, options) {
          	options = options || {};
            // create an observer instance
            var ObserverFunc = window.MutationObserver || window.WebkitMutationObserver;
            var observer = new ObserverFunc(function (mutations) {
            	mutations.forEach(function (mutation) {
            		s.onResize(true);
            		s.emit('onObserverUpdate', s, mutation);
            	});
            });

            observer.observe(target, {
            	attributes: typeof options.attributes === 'undefined' ? true : options.attributes,
            	childList: typeof options.childList === 'undefined' ? true : options.childList,
            	characterData: typeof options.characterData === 'undefined' ? true : options.characterData
            });

            s.observers.push(observer);
        }
        s.initObservers = function () {
        	if (s.params.observeParents) {
        		var containerParents = s.container.parents();
        		for (var i = 0; i < containerParents.length; i++) {
        			initObserver(containerParents[i]);
        		}
        	}

            // Observe container
            initObserver(s.container[0], {childList: false});

            // Observe wrapper
            initObserver(s.wrapper[0], {attributes: false});
        };
        s.disconnectObservers = function () {
        	for (var i = 0; i < s.observers.length; i++) {
        		s.observers[i].disconnect();
        	}
        	s.observers = [];
        };
        /*=========================
          Loop
          ===========================*/
        // Create looped slides
        s.createLoop = function () {
            // Remove duplicated slides
            s.wrapper.children('.' + s.params.slideClass + '.' + s.params.slideDuplicateClass).remove();

            var slides = s.wrapper.children('.' + s.params.slideClass);

            if(s.params.slidesPerView === 'auto' && !s.params.loopedSlides) s.params.loopedSlides = slides.length;

            s.loopedSlides = parseInt(s.params.loopedSlides || s.params.slidesPerView, 10);
            s.loopedSlides = s.loopedSlides + s.params.loopAdditionalSlides;
            if (s.loopedSlides > slides.length) {
            	s.loopedSlides = slides.length;
            }

            var prependSlides = [], appendSlides = [], i;
            slides.each(function (index, el) {
            	var slide = $(this);
            	if (index < s.loopedSlides) appendSlides.push(el);
            	if (index < slides.length && index >= slides.length - s.loopedSlides) prependSlides.push(el);
            	slide.attr('data-swiper-slide-index', index);
            });
            for (i = 0; i < appendSlides.length; i++) {
            	s.wrapper.append($(appendSlides[i].cloneNode(true)).addClass(s.params.slideDuplicateClass));
            }
            for (i = prependSlides.length - 1; i >= 0; i--) {
            	s.wrapper.prepend($(prependSlides[i].cloneNode(true)).addClass(s.params.slideDuplicateClass));
            }
        };
        s.destroyLoop = function () {
        	s.wrapper.children('.' + s.params.slideClass + '.' + s.params.slideDuplicateClass).remove();
        	s.slides.removeAttr('data-swiper-slide-index');
        };
        s.fixLoop = function () {
        	var newIndex;
            //Fix For Negative Oversliding
            if (s.activeIndex < s.loopedSlides) {
            	newIndex = s.slides.length - s.loopedSlides * 3 + s.activeIndex;
            	newIndex = newIndex + s.loopedSlides;
            	s.slideTo(newIndex, 0, false, true);
            }
            //Fix For Positive Oversliding
            else if ((s.params.slidesPerView === 'auto' && s.activeIndex >= s.loopedSlides * 2) || (s.activeIndex > s.slides.length - s.params.slidesPerView * 2)) {
            	newIndex = -s.slides.length + s.activeIndex + s.loopedSlides;
            	newIndex = newIndex + s.loopedSlides;
            	s.slideTo(newIndex, 0, false, true);
            }
        };
        /*=========================
          Append/Prepend/Remove Slides
          ===========================*/
          s.appendSlide = function (slides) {
          	if (s.params.loop) {
          		s.destroyLoop();
          	}
          	if (typeof slides === 'object' && slides.length) {
          		for (var i = 0; i < slides.length; i++) {
          			if (slides[i]) s.wrapper.append(slides[i]);
          		}
          	}
          	else {
          		s.wrapper.append(slides);
          	}
          	if (s.params.loop) {
          		s.createLoop();
          	}
          	if (!(s.params.observer && s.support.observer)) {
          		s.update(true);
          	}
          };
          s.prependSlide = function (slides) {
          	if (s.params.loop) {
          		s.destroyLoop();
          	}
          	var newActiveIndex = s.activeIndex + 1;
          	if (typeof slides === 'object' && slides.length) {
          		for (var i = 0; i < slides.length; i++) {
          			if (slides[i]) s.wrapper.prepend(slides[i]);
          		}
          		newActiveIndex = s.activeIndex + slides.length;
          	}
          	else {
          		s.wrapper.prepend(slides);
          	}
          	if (s.params.loop) {
          		s.createLoop();
          	}
          	if (!(s.params.observer && s.support.observer)) {
          		s.update(true);
          	}
          	s.slideTo(newActiveIndex, 0, false);
          };
          s.removeSlide = function (slidesIndexes) {
          	if (s.params.loop) {
          		s.destroyLoop();
          		s.slides = s.wrapper.children('.' + s.params.slideClass);
          	}
          	var newActiveIndex = s.activeIndex,
          	indexToRemove;
          	if (typeof slidesIndexes === 'object' && slidesIndexes.length) {
          		for (var i = 0; i < slidesIndexes.length; i++) {
          			indexToRemove = slidesIndexes[i];
          			if (s.slides[indexToRemove]) s.slides.eq(indexToRemove).remove();
          			if (indexToRemove < newActiveIndex) newActiveIndex--;
          		}
          		newActiveIndex = Math.max(newActiveIndex, 0);
          	}
          	else {
          		indexToRemove = slidesIndexes;
          		if (s.slides[indexToRemove]) s.slides.eq(indexToRemove).remove();
          		if (indexToRemove < newActiveIndex) newActiveIndex--;
          		newActiveIndex = Math.max(newActiveIndex, 0);
          	}

          	if (s.params.loop) {
          		s.createLoop();
          	}

          	if (!(s.params.observer && s.support.observer)) {
          		s.update(true);
          	}
          	if (s.params.loop) {
          		s.slideTo(newActiveIndex + s.loopedSlides, 0, false);
          	}
          	else {
          		s.slideTo(newActiveIndex, 0, false);
          	}

          };
          s.removeAllSlides = function () {
          	var slidesIndexes = [];
          	for (var i = 0; i < s.slides.length; i++) {
          		slidesIndexes.push(i);
          	}
          	s.removeSlide(slidesIndexes);
          };


        /*=========================
          Effects
          ===========================*/
          s.effects = {
          	fade: {
          		setTranslate: function () {
          			for (var i = 0; i < s.slides.length; i++) {
          				var slide = s.slides.eq(i);
          				var offset = slide[0].swiperSlideOffset;
          				var tx = -offset;
          				if (!s.params.virtualTranslate) tx = tx - s.translate;
          				var ty = 0;
          				if (!isH()) {
          					ty = tx;
          					tx = 0;
          				}
          				var slideOpacity = s.params.fade.crossFade ?
          				Math.max(1 - Math.abs(slide[0].progress), 0) :
          				1 + Math.min(Math.max(slide[0].progress, -1), 0);
          				slide
          				.css({
          					opacity: slideOpacity
          				})
          				.transform('translate3d(' + tx + 'px, ' + ty + 'px, 0px)');

          			}

          		},
          		setTransition: function (duration) {
          			s.slides.transition(duration);
          			if (s.params.virtualTranslate && duration !== 0) {
          				var eventTriggered = false;
          				s.slides.transitionEnd(function () {
          					if (eventTriggered) return;
          					if (!s) return;
          					eventTriggered = true;
          					s.animating = false;
          					var triggerEvents = ['webkitTransitionEnd', 'transitionend', 'oTransitionEnd', 'MSTransitionEnd', 'msTransitionEnd'];
          					for (var i = 0; i < triggerEvents.length; i++) {
          						s.wrapper.trigger(triggerEvents[i]);
          					}
          				});
          			}
          		}
          	},
          	cube: {
          		setTranslate: function () {
          			var wrapperRotate = 0, cubeShadow;
          			if (s.params.cube.shadow) {
          				if (isH()) {
          					cubeShadow = s.wrapper.find('.swiper-cube-shadow');
          					if (cubeShadow.length === 0) {
          						cubeShadow = $('<div class="swiper-cube-shadow"></div>');
          						s.wrapper.append(cubeShadow);
          					}
          					cubeShadow.css({height: s.width + 'px'});
          				}
          				else {
          					cubeShadow = s.container.find('.swiper-cube-shadow');
          					if (cubeShadow.length === 0) {
          						cubeShadow = $('<div class="swiper-cube-shadow"></div>');
          						s.container.append(cubeShadow);
          					}
          				}
          			}
          			for (var i = 0; i < s.slides.length; i++) {
          				var slide = s.slides.eq(i);
          				var slideAngle = i * 90;
          				var round = Math.floor(slideAngle / 360);
          				if (s.rtl) {
          					slideAngle = -slideAngle;
          					round = Math.floor(-slideAngle / 360);
          				}
          				var progress = Math.max(Math.min(slide[0].progress, 1), -1);
          				var tx = 0, ty = 0, tz = 0;
          				if (i % 4 === 0) {
          					tx = - round * 4 * s.size;
          					tz = 0;
          				}
          				else if ((i - 1) % 4 === 0) {
          					tx = 0;
          					tz = - round * 4 * s.size;
          				}
          				else if ((i - 2) % 4 === 0) {
          					tx = s.size + round * 4 * s.size;
          					tz = s.size;
          				}
          				else if ((i - 3) % 4 === 0) {
          					tx = - s.size;
          					tz = 3 * s.size + s.size * 4 * round;
          				}
          				if (s.rtl) {
          					tx = -tx;
          				}

          				if (!isH()) {
          					ty = tx;
          					tx = 0;
          				}

          				var transform = 'rotateX(' + (isH() ? 0 : -slideAngle) + 'deg) rotateY(' + (isH() ? slideAngle : 0) + 'deg) translate3d(' + tx + 'px, ' + ty + 'px, ' + tz + 'px)';
          				if (progress <= 1 && progress > -1) {
          					wrapperRotate = i * 90 + progress * 90;
          					if (s.rtl) wrapperRotate = -i * 90 - progress * 90;
          				}
          				slide.transform(transform);
          				if (s.params.cube.slideShadows) {
                            //Set shadows
                            var shadowBefore = isH() ? slide.find('.swiper-slide-shadow-left') : slide.find('.swiper-slide-shadow-top');
                            var shadowAfter = isH() ? slide.find('.swiper-slide-shadow-right') : slide.find('.swiper-slide-shadow-bottom');
                            if (shadowBefore.length === 0) {
                            	shadowBefore = $('<div class="swiper-slide-shadow-' + (isH() ? 'left' : 'top') + '"></div>');
                            	slide.append(shadowBefore);
                            }
                            if (shadowAfter.length === 0) {
                            	shadowAfter = $('<div class="swiper-slide-shadow-' + (isH() ? 'right' : 'bottom') + '"></div>');
                            	slide.append(shadowAfter);
                            }
                            var shadowOpacity = slide[0].progress;
                            if (shadowBefore.length) shadowBefore[0].style.opacity = -slide[0].progress;
                            if (shadowAfter.length) shadowAfter[0].style.opacity = slide[0].progress;
                        }
                    }
                    s.wrapper.css({
                    	'-webkit-transform-origin': '50% 50% -' + (s.size / 2) + 'px',
                    	'-moz-transform-origin': '50% 50% -' + (s.size / 2) + 'px',
                    	'-ms-transform-origin': '50% 50% -' + (s.size / 2) + 'px',
                    	'transform-origin': '50% 50% -' + (s.size / 2) + 'px'
                    });

                    if (s.params.cube.shadow) {
                    	if (isH()) {
                    		cubeShadow.transform('translate3d(0px, ' + (s.width / 2 + s.params.cube.shadowOffset) + 'px, ' + (-s.width / 2) + 'px) rotateX(90deg) rotateZ(0deg) scale(' + (s.params.cube.shadowScale) + ')');
                    	}
                    	else {
                    		var shadowAngle = Math.abs(wrapperRotate) - Math.floor(Math.abs(wrapperRotate) / 90) * 90;
                    		var multiplier = 1.5 - (Math.sin(shadowAngle * 2 * Math.PI / 360) / 2 + Math.cos(shadowAngle * 2 * Math.PI / 360) / 2);
                    		var scale1 = s.params.cube.shadowScale,
                    		scale2 = s.params.cube.shadowScale / multiplier,
                    		offset = s.params.cube.shadowOffset;
                    		cubeShadow.transform('scale3d(' + scale1 + ', 1, ' + scale2 + ') translate3d(0px, ' + (s.height / 2 + offset) + 'px, ' + (-s.height / 2 / scale2) + 'px) rotateX(-90deg)');
                    	}
                    }
                    var zFactor = (s.isSafari || s.isUiWebView) ? (-s.size / 2) : 0;
                    s.wrapper.transform('translate3d(0px,0,' + zFactor + 'px) rotateX(' + (isH() ? 0 : wrapperRotate) + 'deg) rotateY(' + (isH() ? -wrapperRotate : 0) + 'deg)');
                },
                setTransition: function (duration) {
                	s.slides.transition(duration).find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left').transition(duration);
                	if (s.params.cube.shadow && !isH()) {
                		s.container.find('.swiper-cube-shadow').transition(duration);
                	}
                }
            },
            coverflow: {
            	setTranslate: function () {
            		var transform = s.translate;
            		var center = isH() ? -transform + s.width / 2 : -transform + s.height / 2;
            		var rotate = isH() ? s.params.coverflow.rotate: -s.params.coverflow.rotate;
            		var translate = s.params.coverflow.depth;
                    //Each slide offset from center
                    for (var i = 0, length = s.slides.length; i < length; i++) {
                    	var slide = s.slides.eq(i);
                    	var slideSize = s.slidesSizesGrid[i];
                    	var slideOffset = slide[0].swiperSlideOffset;
                    	var offsetMultiplier = (center - slideOffset - slideSize / 2) / slideSize * s.params.coverflow.modifier;

                    	var rotateY = isH() ? rotate * offsetMultiplier : 0;
                    	var rotateX = isH() ? 0 : rotate * offsetMultiplier;
                        // var rotateZ = 0
                        var translateZ = -translate * Math.abs(offsetMultiplier);

                        var translateY = isH() ? 0 : s.params.coverflow.stretch * (offsetMultiplier);
                        var translateX = isH() ? s.params.coverflow.stretch * (offsetMultiplier) : 0;

                        //Fix for ultra small values
                        if (Math.abs(translateX) < 0.001) translateX = 0;
                        if (Math.abs(translateY) < 0.001) translateY = 0;
                        if (Math.abs(translateZ) < 0.001) translateZ = 0;
                        if (Math.abs(rotateY) < 0.001) rotateY = 0;
                        if (Math.abs(rotateX) < 0.001) rotateX = 0;

                        var slideTransform = 'translate3d(' + translateX + 'px,' + translateY + 'px,' + translateZ + 'px)  rotateX(' + rotateX + 'deg) rotateY(' + rotateY + 'deg)';

                        slide.transform(slideTransform);
                        slide[0].style.zIndex = -Math.abs(Math.round(offsetMultiplier)) + 1;
                        if (s.params.coverflow.slideShadows) {
                            //Set shadows
                            var shadowBefore = isH() ? slide.find('.swiper-slide-shadow-left') : slide.find('.swiper-slide-shadow-top');
                            var shadowAfter = isH() ? slide.find('.swiper-slide-shadow-right') : slide.find('.swiper-slide-shadow-bottom');
                            if (shadowBefore.length === 0) {
                            	shadowBefore = $('<div class="swiper-slide-shadow-' + (isH() ? 'left' : 'top') + '"></div>');
                            	slide.append(shadowBefore);
                            }
                            if (shadowAfter.length === 0) {
                            	shadowAfter = $('<div class="swiper-slide-shadow-' + (isH() ? 'right' : 'bottom') + '"></div>');
                            	slide.append(shadowAfter);
                            }
                            if (shadowBefore.length) shadowBefore[0].style.opacity = offsetMultiplier > 0 ? offsetMultiplier : 0;
                            if (shadowAfter.length) shadowAfter[0].style.opacity = (-offsetMultiplier) > 0 ? -offsetMultiplier : 0;
                        }
                    }

                    //Set correct perspective for IE10
                    if (s.browser.ie) {
                    	var ws = s.wrapper[0].style;
                    	ws.perspectiveOrigin = center + 'px 50%';
                    }
                },
                setTransition: function (duration) {
                	s.slides.transition(duration).find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left').transition(duration);
                }
            }
        };

        /*=========================
          Images Lazy Loading
          ===========================*/
          s.lazy = {
          	initialImageLoaded: false,
          	loadImageInSlide: function (index, loadInDuplicate) {
          		if (typeof index === 'undefined') return;
          		if (typeof loadInDuplicate === 'undefined') loadInDuplicate = true;
          		if (s.slides.length === 0) return;

          		var slide = s.slides.eq(index);
          		var img = slide.find('.swiper-lazy:not(.swiper-lazy-loaded):not(.swiper-lazy-loading)');
          		if (slide.hasClass('swiper-lazy') && !slide.hasClass('swiper-lazy-loaded') && !slide.hasClass('swiper-lazy-loading')) {
          			img.add(slide[0]);
          		}
          		if (img.length === 0) return;

          		img.each(function () {
          			var _img = $(this);
          			_img.addClass('swiper-lazy-loading');
          			var background = _img.attr('data-background');
          			var src = _img.attr('data-src');
          			s.loadImage(_img[0], (src || background), false, function () {
          				if (background) {
          					_img.css('background-image', 'url(' + background + ')');
          					_img.removeAttr('data-background');
          				}
          				else {
          					_img.attr('src', src);
          					_img.removeAttr('data-src');
          				}

          				_img.addClass('swiper-lazy-loaded').removeClass('swiper-lazy-loading');
          				slide.find('.swiper-lazy-preloader, .preloader').remove();
          				if (s.params.loop && loadInDuplicate) {
          					var slideOriginalIndex = slide.attr('data-swiper-slide-index');
          					if (slide.hasClass(s.params.slideDuplicateClass)) {
          						var originalSlide = s.wrapper.children('[data-swiper-slide-index="' + slideOriginalIndex + '"]:not(.' + s.params.slideDuplicateClass + ')');
          						s.lazy.loadImageInSlide(originalSlide.index(), false);
          					}
          					else {
          						var duplicatedSlide = s.wrapper.children('.' + s.params.slideDuplicateClass + '[data-swiper-slide-index="' + slideOriginalIndex + '"]');
          						s.lazy.loadImageInSlide(duplicatedSlide.index(), false);
          					}
          				}
          				s.emit('onLazyImageReady', s, slide[0], _img[0]);
          			});

s.emit('onLazyImageLoad', s, slide[0], _img[0]);
});

},
load: function () {
	var i;
	if (s.params.watchSlidesVisibility) {
		s.wrapper.children('.' + s.params.slideVisibleClass).each(function () {
			s.lazy.loadImageInSlide($(this).index());
		});
	}
	else {
		if (s.params.slidesPerView > 1) {
			for (i = s.activeIndex; i < s.activeIndex + s.params.slidesPerView ; i++) {
				if (s.slides[i]) s.lazy.loadImageInSlide(i);
			}
		}
		else {
			s.lazy.loadImageInSlide(s.activeIndex);    
		}
	}
	if (s.params.lazyLoadingInPrevNext) {
		if (s.params.slidesPerView > 1) {
                        // Next Slides
                        for (i = s.activeIndex + s.params.slidesPerView; i < s.activeIndex + s.params.slidesPerView + s.params.slidesPerView; i++) {
                        	if (s.slides[i]) s.lazy.loadImageInSlide(i);
                        }
                        // Prev Slides
                        for (i = s.activeIndex - s.params.slidesPerView; i < s.activeIndex ; i++) {
                        	if (s.slides[i]) s.lazy.loadImageInSlide(i);
                        }
                    }
                    else {
                    	var nextSlide = s.wrapper.children('.' + s.params.slideNextClass);
                    	if (nextSlide.length > 0) s.lazy.loadImageInSlide(nextSlide.index());

                    	var prevSlide = s.wrapper.children('.' + s.params.slidePrevClass);
                    	if (prevSlide.length > 0) s.lazy.loadImageInSlide(prevSlide.index());
                    }
                }
            },
            onTransitionStart: function () {
            	if (s.params.lazyLoading) {
            		if (s.params.lazyLoadingOnTransitionStart || (!s.params.lazyLoadingOnTransitionStart && !s.lazy.initialImageLoaded)) {
            			s.lazy.load();
            		}
            	}
            },
            onTransitionEnd: function () {
            	if (s.params.lazyLoading && !s.params.lazyLoadingOnTransitionStart) {
            		s.lazy.load();
            	}
            }
        };
        

        /*=========================
          Scrollbar
          ===========================*/
          s.scrollbar = {
          	set: function () {
          		if (!s.params.scrollbar) return;
          		var sb = s.scrollbar;
          		sb.track = $(s.params.scrollbar);
          		sb.drag = sb.track.find('.swiper-scrollbar-drag');
          		if (sb.drag.length === 0) {
          			sb.drag = $('<div class="swiper-scrollbar-drag"></div>');
          			sb.track.append(sb.drag);
          		}
          		sb.drag[0].style.width = '';
          		sb.drag[0].style.height = '';
          		sb.trackSize = isH() ? sb.track[0].offsetWidth : sb.track[0].offsetHeight;

          		sb.divider = s.size / s.virtualSize;
          		sb.moveDivider = sb.divider * (sb.trackSize / s.size);
          		sb.dragSize = sb.trackSize * sb.divider;

          		if (isH()) {
          			sb.drag[0].style.width = sb.dragSize + 'px';
          		}
          		else {
          			sb.drag[0].style.height = sb.dragSize + 'px';
          		}

          		if (sb.divider >= 1) {
          			sb.track[0].style.display = 'none';
          		}
          		else {
          			sb.track[0].style.display = '';
          		}
          		if (s.params.scrollbarHide) {
          			sb.track[0].style.opacity = 0;
          		}
          	},
          	setTranslate: function () {
          		if (!s.params.scrollbar) return;
          		var diff;
          		var sb = s.scrollbar;
          		var translate = s.translate || 0;
          		var newPos;

          		var newSize = sb.dragSize;
          		newPos = (sb.trackSize - sb.dragSize) * s.progress;
          		if (s.rtl && isH()) {
          			newPos = -newPos;
          			if (newPos > 0) {
          				newSize = sb.dragSize - newPos;
          				newPos = 0;
          			}
          			else if (-newPos + sb.dragSize > sb.trackSize) {
          				newSize = sb.trackSize + newPos;
          			}
          		}
          		else {
          			if (newPos < 0) {
          				newSize = sb.dragSize + newPos;
          				newPos = 0;
          			}
          			else if (newPos + sb.dragSize > sb.trackSize) {
          				newSize = sb.trackSize - newPos;
          			}
          		}
          		if (isH()) {
          			if (s.support.transforms3d) {
          				sb.drag.transform('translate3d(' + (newPos) + 'px, 0, 0)');
          			}
          			else {
          				sb.drag.transform('translateX(' + (newPos) + 'px)');   
          			}
          			sb.drag[0].style.width = newSize + 'px';
          		}
          		else {
          			if (s.support.transforms3d) {
          				sb.drag.transform('translate3d(0px, ' + (newPos) + 'px, 0)');
          			}
          			else {
          				sb.drag.transform('translateY(' + (newPos) + 'px)');   
          			}
          			sb.drag[0].style.height = newSize + 'px';
          		}
          		if (s.params.scrollbarHide) {
          			clearTimeout(sb.timeout);
          			sb.track[0].style.opacity = 1;
          			sb.timeout = setTimeout(function () {
          				sb.track[0].style.opacity = 0;
          				sb.track.transition(400);
          			}, 1000);
          		}
          	},
          	setTransition: function (duration) {
          		if (!s.params.scrollbar) return;
          		s.scrollbar.drag.transition(duration);
          	}
          };

        /*=========================
          Controller
          ===========================*/
          s.controller = {
          	LinearSpline: function (x, y) {
          		this.x = x;
          		this.y = y;
          		this.lastIndex = x.length - 1;
                // Given an x value (x2), return the expected y2 value:
                // (x1,y1) is the known point before given value,
                // (x3,y3) is the known point after given value.
                var i1, i3;
                var l = this.x.length;

                this.interpolate = function (x2) {
                	if (!x2) return 0;

                    // Get the indexes of x1 and x3 (the array indexes before and after given x2):
                    i3 = binarySearch(this.x, x2);
                    i1 = i3 - 1;

                    // We have our indexes i1 & i3, so we can calculate already:
                    // y2 := ((x2−x1) × (y3−y1)) ÷ (x3−x1) + y1
                    return ((x2 - this.x[i1]) * (this.y[i3] - this.y[i1])) / (this.x[i3] - this.x[i1]) + this.y[i1];
                };

                var binarySearch = (function() {
                	var maxIndex, minIndex, guess;
                	return function(array, val) {
                		minIndex = -1;
                		maxIndex = array.length;
                		while (maxIndex - minIndex > 1)
                			if (array[guess = maxIndex + minIndex >> 1] <= val) {
                				minIndex = guess;
                			} else {
                				maxIndex = guess;
                			}
                			return maxIndex;
                		};
                	})();
                },
            //xxx: for now i will just save one spline function to to 
            getInterpolateFunction: function(c){
            	if(!s.controller.spline) s.controller.spline = s.params.loop ? 
            		new s.controller.LinearSpline(s.slidesGrid, c.slidesGrid) :
            	new s.controller.LinearSpline(s.snapGrid, c.snapGrid);
            },
            setTranslate: function (translate, byController) {
            	var controlled = s.params.control;
            	var multiplier, controlledTranslate;
            	function setControlledTranslate(c) {
                    // this will create an Interpolate function based on the snapGrids
                    // x is the Grid of the scrolled scroller and y will be the controlled scroller
                    // it makes sense to create this only once and recall it for the interpolation
                    // the function does a lot of value caching for performance 
                    translate = c.rtl && c.params.direction === 'horizontal' ? -s.translate : s.translate;
                    if (s.params.controlBy === 'slide') {
                    	s.controller.getInterpolateFunction(c);
                        // i am not sure why the values have to be multiplicated this way, tried to invert the snapGrid
                        // but it did not work out
                        controlledTranslate = -s.controller.spline.interpolate(-translate);
                    }

                    if(!controlledTranslate || s.params.controlBy === 'container'){
                    	multiplier = (c.maxTranslate() - c.minTranslate()) / (s.maxTranslate() - s.minTranslate());
                    	controlledTranslate = (translate - s.minTranslate()) * multiplier + c.minTranslate();
                    }

                    if (s.params.controlInverse) {
                    	controlledTranslate = c.maxTranslate() - controlledTranslate;
                    }
                    c.updateProgress(controlledTranslate);
                    c.setWrapperTranslate(controlledTranslate, false, s);
                    c.updateActiveIndex();
                }
                if (s.isArray(controlled)) {
                	for (var i = 0; i < controlled.length; i++) {
                		if (controlled[i] !== byController && controlled[i] instanceof Swiper) {
                			setControlledTranslate(controlled[i]);
                		}
                	}
                }
                else if (controlled instanceof Swiper && byController !== controlled) {

                	setControlledTranslate(controlled);
                }
            },
            setTransition: function (duration, byController) {
            	var controlled = s.params.control;
            	var i;
            	function setControlledTransition(c) {
            		c.setWrapperTransition(duration, s);
            		if (duration !== 0) {
            			c.onTransitionStart();
            			c.wrapper.transitionEnd(function(){
            				if (!controlled) return;
            				if (c.params.loop && s.params.controlBy === 'slide') {
            					c.fixLoop();
            				}
            				c.onTransitionEnd();

            			});
            		}
            	}
            	if (s.isArray(controlled)) {
            		for (i = 0; i < controlled.length; i++) {
            			if (controlled[i] !== byController && controlled[i] instanceof Swiper) {
            				setControlledTransition(controlled[i]);
            			}
            		}
            	}
            	else if (controlled instanceof Swiper && byController !== controlled) {
            		setControlledTransition(controlled);
            	}
            }
        };

        /*=========================
          Hash Navigation
          ===========================*/
          s.hashnav = {
          	init: function () {
          		if (!s.params.hashnav) return;
          		s.hashnav.initialized = true;
          		var hash = document.location.hash.replace('#', '');
          		if (!hash) return;
          		var speed = 0;
          		for (var i = 0, length = s.slides.length; i < length; i++) {
          			var slide = s.slides.eq(i);
          			var slideHash = slide.attr('data-hash');
          			if (slideHash === hash && !slide.hasClass(s.params.slideDuplicateClass)) {
          				var index = slide.index();
          				s.slideTo(index, speed, s.params.runCallbacksOnInit, true);
          			}
          		}
          	},
          	setHash: function () {
          		if (!s.hashnav.initialized || !s.params.hashnav) return;
          		document.location.hash = s.slides.eq(s.activeIndex).attr('data-hash') || '';
          	}
          };

        /*=========================
          Keyboard Control
          ===========================*/
          function handleKeyboard(e) {
            if (e.originalEvent) e = e.originalEvent; //jquery fix
            var kc = e.keyCode || e.charCode;
            // Directions locks
            if (!s.params.allowSwipeToNext && (isH() && kc === 39 || !isH() && kc === 40)) {
            	return false;
            }
            if (!s.params.allowSwipeToPrev && (isH() && kc === 37 || !isH() && kc === 38)) {
            	return false;
            }
            if (e.shiftKey || e.altKey || e.ctrlKey || e.metaKey) {
            	return;
            }
            if (document.activeElement && document.activeElement.nodeName && (document.activeElement.nodeName.toLowerCase() === 'input' || document.activeElement.nodeName.toLowerCase() === 'textarea')) {
            	return;
            }
            if (kc === 37 || kc === 39 || kc === 38 || kc === 40) {
            	var inView = false;
                //Check that swiper should be inside of visible area of window
                if (s.container.parents('.swiper-slide').length > 0 && s.container.parents('.swiper-slide-active').length === 0) {
                	return;
                }
                var windowScroll = {
                	left: window.pageXOffset,
                	top: window.pageYOffset
                };
                var windowWidth = window.innerWidth;
                var windowHeight = window.innerHeight;
                var swiperOffset = s.container.offset();
                if (s.rtl) swiperOffset.left = swiperOffset.left - s.container[0].scrollLeft;
                var swiperCoord = [
                [swiperOffset.left, swiperOffset.top],
                [swiperOffset.left + s.width, swiperOffset.top],
                [swiperOffset.left, swiperOffset.top + s.height],
                [swiperOffset.left + s.width, swiperOffset.top + s.height]
                ];
                for (var i = 0; i < swiperCoord.length; i++) {
                	var point = swiperCoord[i];
                	if (
                		point[0] >= windowScroll.left && point[0] <= windowScroll.left + windowWidth &&
                		point[1] >= windowScroll.top && point[1] <= windowScroll.top + windowHeight
                		) {
                		inView = true;
                }

            }
            if (!inView) return;
        }
        if (isH()) {
        	if (kc === 37 || kc === 39) {
        		if (e.preventDefault) e.preventDefault();
        		else e.returnValue = false;
        	}
        	if ((kc === 39 && !s.rtl) || (kc === 37 && s.rtl)) s.slideNext();
        	if ((kc === 37 && !s.rtl) || (kc === 39 && s.rtl)) s.slidePrev();
        }
        else {
        	if (kc === 38 || kc === 40) {
        		if (e.preventDefault) e.preventDefault();
        		else e.returnValue = false;
        	}
        	if (kc === 40) s.slideNext();
        	if (kc === 38) s.slidePrev();
        }
    }
    s.disableKeyboardControl = function () {
    	$(document).off('keydown', handleKeyboard);
    };
    s.enableKeyboardControl = function () {
    	$(document).on('keydown', handleKeyboard);
    };


        /*=========================
          Mousewheel Control
          ===========================*/
          s.mousewheel = {
          	event: false,
          	lastScrollTime: (new window.Date()).getTime()
          };
          if (s.params.mousewheelControl) {
          	try {
          		new window.WheelEvent('wheel');
          		s.mousewheel.event = 'wheel';
          	} catch (e) {}

          	if (!s.mousewheel.event && document.onmousewheel !== undefined) {
          		s.mousewheel.event = 'mousewheel';
          	}
          	if (!s.mousewheel.event) {
          		s.mousewheel.event = 'DOMMouseScroll';
          	}
          }
          function handleMousewheel(e) {
            if (e.originalEvent) e = e.originalEvent; //jquery fix
            var we = s.mousewheel.event;
            var delta = 0;
            //Opera & IE
            if (e.detail) delta = -e.detail;
            //WebKits
            else if (we === 'mousewheel') {
            	if (s.params.mousewheelForceToAxis) {
            		if (isH()) {
            			if (Math.abs(e.wheelDeltaX) > Math.abs(e.wheelDeltaY)) delta = e.wheelDeltaX;
            			else return;
            		}
            		else {
            			if (Math.abs(e.wheelDeltaY) > Math.abs(e.wheelDeltaX)) delta = e.wheelDeltaY;
            			else return;
            		}
            	}
            	else {
            		delta = e.wheelDelta;
            	}
            }
            //Old FireFox
            else if (we === 'DOMMouseScroll') delta = -e.detail;
            //New FireFox
            else if (we === 'wheel') {
            	if (s.params.mousewheelForceToAxis) {
            		if (isH()) {
            			if (Math.abs(e.deltaX) > Math.abs(e.deltaY)) delta = -e.deltaX;
            			else return;
            		}
            		else {
            			if (Math.abs(e.deltaY) > Math.abs(e.deltaX)) delta = -e.deltaY;
            			else return;
            		}
            	}
            	else {
            		delta = Math.abs(e.deltaX) > Math.abs(e.deltaY) ? - e.deltaX : - e.deltaY;
            	}
            }

            if (s.params.mousewheelInvert) delta = -delta;

            if (!s.params.freeMode) {
            	if ((new window.Date()).getTime() - s.mousewheel.lastScrollTime > 60) {
            		if (delta < 0) {
            			if ((!s.isEnd || s.params.loop) && !s.animating) s.slideNext();
            			else if (s.params.mousewheelReleaseOnEdges) return true;
            		}
            		else {
            			if ((!s.isBeginning || s.params.loop) && !s.animating) s.slidePrev();
            			else if (s.params.mousewheelReleaseOnEdges) return true;
            		}
            	}
            	s.mousewheel.lastScrollTime = (new window.Date()).getTime();

            }
            else {
                //Freemode or scrollContainer:

                var position = s.getWrapperTranslate() + delta * s.params.mousewheelSensitivity;

                if (position > 0) position = 0;
                if (position < s.maxTranslate()) position = s.maxTranslate();

                s.setWrapperTransition(0);
                s.setWrapperTranslate(position);
                s.updateProgress();
                s.updateActiveIndex();

                if (s.params.freeModeSticky) {
                	clearTimeout(s.mousewheel.timeout);
                	s.mousewheel.timeout = setTimeout(function () {
                		s.slideReset();
                	}, 300);
                }

                // Return page scroll on edge positions
                if (position === 0 || position === s.maxTranslate()) return;
            }
            if (s.params.autoplay) s.stopAutoplay();

            if (e.preventDefault) e.preventDefault();
            else e.returnValue = false;
            return false;
        }
        s.disableMousewheelControl = function () {
        	if (!s.mousewheel.event) return false;
        	s.container.off(s.mousewheel.event, handleMousewheel);
        	return true;
        };
        
        s.enableMousewheelControl = function () {
        	if (!s.mousewheel.event) return false;
        	s.container.on(s.mousewheel.event, handleMousewheel);
        	return true;
        };

        /*=========================
          Parallax
          ===========================*/
          function setParallaxTransform(el, progress) {
          	el = $(el);
          	var p, pX, pY;

          	p = el.attr('data-swiper-parallax') || '0';
          	pX = el.attr('data-swiper-parallax-x');
          	pY = el.attr('data-swiper-parallax-y');
          	if (pX || pY) {
          		pX = pX || '0';
          		pY = pY || '0';
          	}
          	else {
          		if (isH()) {
          			pX = p;
          			pY = '0';
          		}
          		else {
          			pY = p;
          			pX = '0';
          		}
          	}
          	if ((pX).indexOf('%') >= 0) {
          		pX = parseInt(pX, 10) * progress + '%';
          	}
          	else {
          		pX = pX * progress + 'px' ;
          	}
          	if ((pY).indexOf('%') >= 0) {
          		pY = parseInt(pY, 10) * progress + '%';
          	}
          	else {
          		pY = pY * progress + 'px' ;
          	}
          	el.transform('translate3d(' + pX + ', ' + pY + ',0px)');
          }   
          s.parallax = {
          	setTranslate: function () {
          		s.container.children('[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]').each(function(){
          			setParallaxTransform(this, s.progress);

          		});
          		s.slides.each(function () {
          			var slide = $(this);
          			slide.find('[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]').each(function () {
          				var progress = Math.min(Math.max(slide[0].progress, -1), 1);
          				setParallaxTransform(this, progress);
          			});
          		});
          	},
          	setTransition: function (duration) {
          		if (typeof duration === 'undefined') duration = s.params.speed;
          		s.container.find('[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]').each(function(){
          			var el = $(this);
          			var parallaxDuration = parseInt(el.attr('data-swiper-parallax-duration'), 10) || duration;
          			if (duration === 0) parallaxDuration = 0;
          			el.transition(parallaxDuration);
          		});
          	}
          };


        /*=========================
          Plugins API. Collect all and init all plugins
          ===========================*/
          s._plugins = [];
          for (var plugin in s.plugins) {
          	var p = s.plugins[plugin](s, s.params[plugin]);
          	if (p) s._plugins.push(p);
          }
        // Method to call all plugins event/method
        s.callPlugins = function (eventName) {
        	for (var i = 0; i < s._plugins.length; i++) {
        		if (eventName in s._plugins[i]) {
        			s._plugins[i][eventName](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
        		}
        	}
        };

        /*=========================
          Events/Callbacks/Plugins Emitter
          ===========================*/
          function normalizeEventName (eventName) {
          	if (eventName.indexOf('on') !== 0) {
          		if (eventName[0] !== eventName[0].toUpperCase()) {
          			eventName = 'on' + eventName[0].toUpperCase() + eventName.substring(1);
          		}
          		else {
          			eventName = 'on' + eventName;
          		}
          	}
          	return eventName;
          }
          s.emitterEventListeners = {

          };
          s.emit = function (eventName) {
            // Trigger callbacks
            if (s.params[eventName]) {
            	s.params[eventName](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
            }
            var i;
            // Trigger events
            if (s.emitterEventListeners[eventName]) {
            	for (i = 0; i < s.emitterEventListeners[eventName].length; i++) {
            		s.emitterEventListeners[eventName][i](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
            	}
            }
            // Trigger plugins
            if (s.callPlugins) s.callPlugins(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
        };
        s.on = function (eventName, handler) {
        	eventName = normalizeEventName(eventName);
        	if (!s.emitterEventListeners[eventName]) s.emitterEventListeners[eventName] = [];
        	s.emitterEventListeners[eventName].push(handler);
        	return s;
        };
        s.off = function (eventName, handler) {
        	var i;
        	eventName = normalizeEventName(eventName);
        	if (typeof handler === 'undefined') {
                // Remove all handlers for such event
                s.emitterEventListeners[eventName] = [];
                return s;
            }
            if (!s.emitterEventListeners[eventName] || s.emitterEventListeners[eventName].length === 0) return;
            for (i = 0; i < s.emitterEventListeners[eventName].length; i++) {
            	if(s.emitterEventListeners[eventName][i] === handler) s.emitterEventListeners[eventName].splice(i, 1);
            }
            return s;
        };
        s.once = function (eventName, handler) {
        	eventName = normalizeEventName(eventName);
        	var _handler = function () {
        		handler(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
        		s.off(eventName, _handler);
        	};
        	s.on(eventName, _handler);
        	return s;
        };

        // Accessibility tools
        s.a11y = {
        	makeFocusable: function ($el) {
        		$el.attr('tabIndex', '0');
        		return $el;
        	},
        	addRole: function ($el, role) {
        		$el.attr('role', role);
        		return $el;
        	},

        	addLabel: function ($el, label) {
        		$el.attr('aria-label', label);
        		return $el;
        	},

        	disable: function ($el) {
        		$el.attr('aria-disabled', true);
        		return $el;
        	},

        	enable: function ($el) {
        		$el.attr('aria-disabled', false);
        		return $el;
        	},

        	onEnterKey: function (event) {
        		if (event.keyCode !== 13) return;
        		if ($(event.target).is(s.params.nextButton)) {
        			s.onClickNext(event);
        			if (s.isEnd) {
        				s.a11y.notify(s.params.lastSlideMessage);
        			}
        			else {
        				s.a11y.notify(s.params.nextSlideMessage);
        			}
        		}
        		else if ($(event.target).is(s.params.prevButton)) {
        			s.onClickPrev(event);
        			if (s.isBeginning) {
        				s.a11y.notify(s.params.firstSlideMessage);
        			}
        			else {
        				s.a11y.notify(s.params.prevSlideMessage);
        			}
        		}
        		if ($(event.target).is('.' + s.params.bulletClass)) {
        			$(event.target)[0].click();
        		}
        	},

        	liveRegion: $('<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>'),

        	notify: function (message) {
        		var notification = s.a11y.liveRegion;
        		if (notification.length === 0) return;
        		notification.html('');
        		notification.html(message);
        	},
        	init: function () {
                // Setup accessibility
                if (s.params.nextButton) {
                	var nextButton = $(s.params.nextButton);
                	s.a11y.makeFocusable(nextButton);
                	s.a11y.addRole(nextButton, 'button');
                	s.a11y.addLabel(nextButton, s.params.nextSlideMessage);
                }
                if (s.params.prevButton) {
                	var prevButton = $(s.params.prevButton);
                	s.a11y.makeFocusable(prevButton);
                	s.a11y.addRole(prevButton, 'button');
                	s.a11y.addLabel(prevButton, s.params.prevSlideMessage);
                }

                $(s.container).append(s.a11y.liveRegion);
            },
            initPagination: function () {
            	if (s.params.pagination && s.params.paginationClickable && s.bullets && s.bullets.length) {
            		s.bullets.each(function () {
            			var bullet = $(this);
            			s.a11y.makeFocusable(bullet);
            			s.a11y.addRole(bullet, 'button');
            			s.a11y.addLabel(bullet, s.params.paginationBulletMessage.replace(/{{index}}/, bullet.index() + 1));
            		});
            	}
            },
            destroy: function () {
            	if (s.a11y.liveRegion && s.a11y.liveRegion.length > 0) s.a11y.liveRegion.remove();
            }
        };
        

        /*=========================
          Init/Destroy
          ===========================*/
          s.init = function () {
          	if (s.params.loop) s.createLoop();
          	s.updateContainerSize();
          	s.updateSlidesSize();
          	s.updatePagination();
          	if (s.params.scrollbar && s.scrollbar) {
          		s.scrollbar.set();
          	}
          	if (s.params.effect !== 'slide' && s.effects[s.params.effect]) {
          		if (!s.params.loop) s.updateProgress();
          		s.effects[s.params.effect].setTranslate();
          	}
          	if (s.params.loop) {
          		s.slideTo(s.params.initialSlide + s.loopedSlides, 0, s.params.runCallbacksOnInit);
          	}
          	else {
          		s.slideTo(s.params.initialSlide, 0, s.params.runCallbacksOnInit);
          		if (s.params.initialSlide === 0) {
          			if (s.parallax && s.params.parallax) s.parallax.setTranslate();
          			if (s.lazy && s.params.lazyLoading) {
          				s.lazy.load();
          				s.lazy.initialImageLoaded = true;
          			}
          		}
          	}
          	s.attachEvents();
          	if (s.params.observer && s.support.observer) {
          		s.initObservers();
          	}
          	if (s.params.preloadImages && !s.params.lazyLoading) {
          		s.preloadImages();
          	}
          	if (s.params.autoplay) {
          		s.startAutoplay();
          	}
          	if (s.params.keyboardControl) {
          		if (s.enableKeyboardControl) s.enableKeyboardControl();
          	}
          	if (s.params.mousewheelControl) {
          		if (s.enableMousewheelControl) s.enableMousewheelControl();
          	}
          	if (s.params.hashnav) {
          		if (s.hashnav) s.hashnav.init();
          	}
          	if (s.params.a11y && s.a11y) s.a11y.init();
          	s.emit('onInit', s);
          };

        // Cleanup dynamic styles
        s.cleanupStyles = function () {
            // Container
            s.container.removeClass(s.classNames.join(' ')).removeAttr('style');

            // Wrapper
            s.wrapper.removeAttr('style');

            // Slides
            if (s.slides && s.slides.length) {
            	s.slides
            	.removeClass([
            		s.params.slideVisibleClass,
            		s.params.slideActiveClass,
            		s.params.slideNextClass,
            		s.params.slidePrevClass
            		].join(' '))
            	.removeAttr('style')
            	.removeAttr('data-swiper-column')
            	.removeAttr('data-swiper-row');
            }

            // Pagination/Bullets
            if (s.paginationContainer && s.paginationContainer.length) {
            	s.paginationContainer.removeClass(s.params.paginationHiddenClass);
            }
            if (s.bullets && s.bullets.length) {
            	s.bullets.removeClass(s.params.bulletActiveClass);
            }

            // Buttons
            if (s.params.prevButton) $(s.params.prevButton).removeClass(s.params.buttonDisabledClass);
            if (s.params.nextButton) $(s.params.nextButton).removeClass(s.params.buttonDisabledClass);

            // Scrollbar
            if (s.params.scrollbar && s.scrollbar) {
            	if (s.scrollbar.track && s.scrollbar.track.length) s.scrollbar.track.removeAttr('style');
            	if (s.scrollbar.drag && s.scrollbar.drag.length) s.scrollbar.drag.removeAttr('style');
            }
        };
        
        // Destroy
        s.destroy = function (deleteInstance, cleanupStyles) {
            // Detach evebts
            s.detachEvents();
            // Stop autoplay
            s.stopAutoplay();
            // Destroy loop
            if (s.params.loop) {
            	s.destroyLoop();
            }
            // Cleanup styles
            if (cleanupStyles) {
            	s.cleanupStyles();
            }
            // Disconnect observer
            s.disconnectObservers();
            // Disable keyboard/mousewheel
            if (s.params.keyboardControl) {
            	if (s.disableKeyboardControl) s.disableKeyboardControl();
            }
            if (s.params.mousewheelControl) {
            	if (s.disableMousewheelControl) s.disableMousewheelControl();
            }
            // Disable a11y
            if (s.params.a11y && s.a11y) s.a11y.destroy();
            // Destroy callback
            s.emit('onDestroy');
            // Delete instance
            if (deleteInstance !== false) s = null;
        };
        
        s.init();
        


        // Return swiper instance
        return s;
    };
    

    /*==================================================
        Prototype
        ====================================================*/
        Swiper.prototype = {
        	isSafari: (function () {
        		var ua = navigator.userAgent.toLowerCase();
        		return (ua.indexOf('safari') >= 0 && ua.indexOf('chrome') < 0 && ua.indexOf('android') < 0);
        	})(),
        	isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(navigator.userAgent),
        	isArray: function (arr) {
        		return Object.prototype.toString.apply(arr) === '[object Array]';
        	},
        /*==================================================
        Browser
        ====================================================*/
        browser: {
        	ie: window.navigator.pointerEnabled || window.navigator.msPointerEnabled,
        	ieTouch: (window.navigator.msPointerEnabled && window.navigator.msMaxTouchPoints > 1) || (window.navigator.pointerEnabled && window.navigator.maxTouchPoints > 1),
        },
        /*==================================================
        Devices
        ====================================================*/
        device: (function () {
        	var ua = navigator.userAgent;
        	var android = ua.match(/(Android);?[\s\/]+([\d.]+)?/);
        	var ipad = ua.match(/(iPad).*OS\s([\d_]+)/);
        	var ipod = ua.match(/(iPod)(.*OS\s([\d_]+))?/);
        	var iphone = !ipad && ua.match(/(iPhone\sOS)\s([\d_]+)/);
        	return {
        		ios: ipad || iphone || ipod,
        		android: android
        	};
        })(),
        /*==================================================
        Feature Detection
        ====================================================*/
        support: {
        	touch : (window.Modernizr && Modernizr.touch === true) || (function () {
        		return !!(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch);
        	})(),

        	transforms3d : (window.Modernizr && Modernizr.csstransforms3d === true) || (function () {
        		var div = document.createElement('div').style;
        		return ('webkitPerspective' in div || 'MozPerspective' in div || 'OPerspective' in div || 'MsPerspective' in div || 'perspective' in div);
        	})(),

        	flexbox: (function () {
        		var div = document.createElement('div').style;
        		var styles = ('alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient').split(' ');
        		for (var i = 0; i < styles.length; i++) {
        			if (styles[i] in div) return true;
        		}
        	})(),

        	observer: (function () {
        		return ('MutationObserver' in window || 'WebkitMutationObserver' in window);
        	})()
        },
        /*==================================================
        Plugins
        ====================================================*/
        plugins: {}
    };
    

    /*===========================
    Dom7 Library
    ===========================*/
    var Dom7 = (function () {
    	var Dom7 = function (arr) {
    		var _this = this, i = 0;
            // Create array-like object
            for (i = 0; i < arr.length; i++) {
            	_this[i] = arr[i];
            }
            _this.length = arr.length;
            // Return collection with methods
            return this;
        };
        var $ = function (selector, context) {
        	var arr = [], i = 0;
        	if (selector && !context) {
        		if (selector instanceof Dom7) {
        			return selector;
        		}
        	}
        	if (selector) {
                // String
                if (typeof selector === 'string') {
                	var els, tempParent, html = selector.trim();
                	if (html.indexOf('<') >= 0 && html.indexOf('>') >= 0) {
                		var toCreate = 'div';
                		if (html.indexOf('<li') === 0) toCreate = 'ul';
                		if (html.indexOf('<tr') === 0) toCreate = 'tbody';
                		if (html.indexOf('<td') === 0 || html.indexOf('<th') === 0) toCreate = 'tr';
                		if (html.indexOf('<tbody') === 0) toCreate = 'table';
                		if (html.indexOf('<option') === 0) toCreate = 'select';
                		tempParent = document.createElement(toCreate);
                		tempParent.innerHTML = selector;
                		for (i = 0; i < tempParent.childNodes.length; i++) {
                			arr.push(tempParent.childNodes[i]);
                		}
                	}
                	else {
                		if (!context && selector[0] === '#' && !selector.match(/[ .<>:~]/)) {
                            // Pure ID selector
                            els = [document.getElementById(selector.split('#')[1])];
                        }
                        else {
                            // Other selectors
                            els = (context || document).querySelectorAll(selector);
                        }
                        for (i = 0; i < els.length; i++) {
                        	if (els[i]) arr.push(els[i]);
                        }
                    }
                }
                // Node/element
                else if (selector.nodeType || selector === window || selector === document) {
                	arr.push(selector);
                }
                //Array of elements or instance of Dom
                else if (selector.length > 0 && selector[0].nodeType) {
                	for (i = 0; i < selector.length; i++) {
                		arr.push(selector[i]);
                	}
                }
            }
            return new Dom7(arr);
        };
        Dom7.prototype = {
            // Classes and attriutes
            addClass: function (className) {
            	if (typeof className === 'undefined') {
            		return this;
            	}
            	var classes = className.split(' ');
            	for (var i = 0; i < classes.length; i++) {
            		for (var j = 0; j < this.length; j++) {
            			this[j].classList.add(classes[i]);
            		}
            	}
            	return this;
            },
            removeClass: function (className) {
            	var classes = className.split(' ');
            	for (var i = 0; i < classes.length; i++) {
            		for (var j = 0; j < this.length; j++) {
            			this[j].classList.remove(classes[i]);
            		}
            	}
            	return this;
            },
            hasClass: function (className) {
            	if (!this[0]) return false;
            	else return this[0].classList.contains(className);
            },
            toggleClass: function (className) {
            	var classes = className.split(' ');
            	for (var i = 0; i < classes.length; i++) {
            		for (var j = 0; j < this.length; j++) {
            			this[j].classList.toggle(classes[i]);
            		}
            	}
            	return this;
            },
            attr: function (attrs, value) {
            	if (arguments.length === 1 && typeof attrs === 'string') {
                    // Get attr
                    if (this[0]) return this[0].getAttribute(attrs);
                    else return undefined;
                }
                else {
                    // Set attrs
                    for (var i = 0; i < this.length; i++) {
                    	if (arguments.length === 2) {
                            // String
                            this[i].setAttribute(attrs, value);
                        }
                        else {
                            // Object
                            for (var attrName in attrs) {
                            	this[i][attrName] = attrs[attrName];
                            	this[i].setAttribute(attrName, attrs[attrName]);
                            }
                        }
                    }
                    return this;
                }
            },
            removeAttr: function (attr) {
            	for (var i = 0; i < this.length; i++) {
            		this[i].removeAttribute(attr);
            	}
            	return this;
            },
            data: function (key, value) {
            	if (typeof value === 'undefined') {
                    // Get value
                    if (this[0]) {
                    	var dataKey = this[0].getAttribute('data-' + key);
                    	if (dataKey) return dataKey;
                    	else if (this[0].dom7ElementDataStorage && (key in this[0].dom7ElementDataStorage)) return this[0].dom7ElementDataStorage[key];
                    	else return undefined;
                    }
                    else return undefined;
                }
                else {
                    // Set value
                    for (var i = 0; i < this.length; i++) {
                    	var el = this[i];
                    	if (!el.dom7ElementDataStorage) el.dom7ElementDataStorage = {};
                    	el.dom7ElementDataStorage[key] = value;
                    }
                    return this;
                }
            },
            // Transforms
            transform : function (transform) {
            	for (var i = 0; i < this.length; i++) {
            		var elStyle = this[i].style;
            		elStyle.webkitTransform = elStyle.MsTransform = elStyle.msTransform = elStyle.MozTransform = elStyle.OTransform = elStyle.transform = transform;
            	}
            	return this;
            },
            transition: function (duration) {
            	if (typeof duration !== 'string') {
            		duration = duration + 'ms';
            	}
            	for (var i = 0; i < this.length; i++) {
            		var elStyle = this[i].style;
            		elStyle.webkitTransitionDuration = elStyle.MsTransitionDuration = elStyle.msTransitionDuration = elStyle.MozTransitionDuration = elStyle.OTransitionDuration = elStyle.transitionDuration = duration;
            	}
            	return this;
            },
            //Events
            on: function (eventName, targetSelector, listener, capture) {
            	function handleLiveEvent(e) {
            		var target = e.target;
            		if ($(target).is(targetSelector)) listener.call(target, e);
            		else {
            			var parents = $(target).parents();
            			for (var k = 0; k < parents.length; k++) {
            				if ($(parents[k]).is(targetSelector)) listener.call(parents[k], e);
            			}
            		}
            	}
            	var events = eventName.split(' ');
            	var i, j;
            	for (i = 0; i < this.length; i++) {
            		if (typeof targetSelector === 'function' || targetSelector === false) {
                        // Usual events
                        if (typeof targetSelector === 'function') {
                        	listener = arguments[1];
                        	capture = arguments[2] || false;
                        }
                        for (j = 0; j < events.length; j++) {
                        	this[i].addEventListener(events[j], listener, capture);
                        }
                    }
                    else {
                        //Live events
                        for (j = 0; j < events.length; j++) {
                        	if (!this[i].dom7LiveListeners) this[i].dom7LiveListeners = [];
                        	this[i].dom7LiveListeners.push({listener: listener, liveListener: handleLiveEvent});
                        	this[i].addEventListener(events[j], handleLiveEvent, capture);
                        }
                    }
                }

                return this;
            },
            off: function (eventName, targetSelector, listener, capture) {
            	var events = eventName.split(' ');
            	for (var i = 0; i < events.length; i++) {
            		for (var j = 0; j < this.length; j++) {
            			if (typeof targetSelector === 'function' || targetSelector === false) {
                            // Usual events
                            if (typeof targetSelector === 'function') {
                            	listener = arguments[1];
                            	capture = arguments[2] || false;
                            }
                            this[j].removeEventListener(events[i], listener, capture);
                        }
                        else {
                            // Live event
                            if (this[j].dom7LiveListeners) {
                            	for (var k = 0; k < this[j].dom7LiveListeners.length; k++) {
                            		if (this[j].dom7LiveListeners[k].listener === listener) {
                            			this[j].removeEventListener(events[i], this[j].dom7LiveListeners[k].liveListener, capture);
                            		}
                            	}
                            }
                        }
                    }
                }
                return this;
            },
            once: function (eventName, targetSelector, listener, capture) {
            	var dom = this;
            	if (typeof targetSelector === 'function') {
            		targetSelector = false;
            		listener = arguments[1];
            		capture = arguments[2];
            	}
            	function proxy(e) {
            		listener(e);
            		dom.off(eventName, targetSelector, proxy, capture);
            	}
            	dom.on(eventName, targetSelector, proxy, capture);
            },
            trigger: function (eventName, eventData) {
            	for (var i = 0; i < this.length; i++) {
            		var evt;
            		try {
            			evt = new window.CustomEvent(eventName, {detail: eventData, bubbles: true, cancelable: true});
            		}
            		catch (e) {
            			evt = document.createEvent('Event');
            			evt.initEvent(eventName, true, true);
            			evt.detail = eventData;
            		}
            		this[i].dispatchEvent(evt);
            	}
            	return this;
            },
            transitionEnd: function (callback) {
            	var events = ['webkitTransitionEnd', 'transitionend', 'oTransitionEnd', 'MSTransitionEnd', 'msTransitionEnd'],
            	i, j, dom = this;
            	function fireCallBack(e) {
            		/*jshint validthis:true */
            		if (e.target !== this) return;
            		callback.call(this, e);
            		for (i = 0; i < events.length; i++) {
            			dom.off(events[i], fireCallBack);
            		}
            	}
            	if (callback) {
            		for (i = 0; i < events.length; i++) {
            			dom.on(events[i], fireCallBack);
            		}
            	}
            	return this;
            },
            // Sizing/Styles
            width: function () {
            	if (this[0] === window) {
            		return window.innerWidth;
            	}
            	else {
            		if (this.length > 0) {
            			return parseFloat(this.css('width'));
            		}
            		else {
            			return null;
            		}
            	}
            },
            outerWidth: function (includeMargins) {
            	if (this.length > 0) {
            		if (includeMargins)
            			return this[0].offsetWidth + parseFloat(this.css('margin-right')) + parseFloat(this.css('margin-left'));
            		else
            			return this[0].offsetWidth;
            	}
            	else return null;
            },
            height: function () {
            	if (this[0] === window) {
            		return window.innerHeight;
            	}
            	else {
            		if (this.length > 0) {
            			return parseFloat(this.css('height'));
            		}
            		else {
            			return null;
            		}
            	}
            },
            outerHeight: function (includeMargins) {
            	if (this.length > 0) {
            		if (includeMargins)
            			return this[0].offsetHeight + parseFloat(this.css('margin-top')) + parseFloat(this.css('margin-bottom'));
            		else
            			return this[0].offsetHeight;
            	}
            	else return null;
            },
            offset: function () {
            	if (this.length > 0) {
            		var el = this[0];
            		var box = el.getBoundingClientRect();
            		var body = document.body;
            		var clientTop  = el.clientTop  || body.clientTop  || 0;
            		var clientLeft = el.clientLeft || body.clientLeft || 0;
            		var scrollTop  = window.pageYOffset || el.scrollTop;
            		var scrollLeft = window.pageXOffset || el.scrollLeft;
            		return {
            			top: box.top  + scrollTop  - clientTop,
            			left: box.left + scrollLeft - clientLeft
            		};
            	}
            	else {
            		return null;
            	}
            },
            css: function (props, value) {
            	var i;
            	if (arguments.length === 1) {
            		if (typeof props === 'string') {
            			if (this[0]) return window.getComputedStyle(this[0], null).getPropertyValue(props);
            		}
            		else {
            			for (i = 0; i < this.length; i++) {
            				for (var prop in props) {
            					this[i].style[prop] = props[prop];
            				}
            			}
            			return this;
            		}
            	}
            	if (arguments.length === 2 && typeof props === 'string') {
            		for (i = 0; i < this.length; i++) {
            			this[i].style[props] = value;
            		}
            		return this;
            	}
            	return this;
            },

            //Dom manipulation
            each: function (callback) {
            	for (var i = 0; i < this.length; i++) {
            		callback.call(this[i], i, this[i]);
            	}
            	return this;
            },
            html: function (html) {
            	if (typeof html === 'undefined') {
            		return this[0] ? this[0].innerHTML : undefined;
            	}
            	else {
            		for (var i = 0; i < this.length; i++) {
            			this[i].innerHTML = html;
            		}
            		return this;
            	}
            },
            is: function (selector) {
            	if (!this[0]) return false;
            	var compareWith, i;
            	if (typeof selector === 'string') {
            		var el = this[0];
            		if (el === document) return selector === document;
            		if (el === window) return selector === window;

            		if (el.matches) return el.matches(selector);
            		else if (el.webkitMatchesSelector) return el.webkitMatchesSelector(selector);
            		else if (el.mozMatchesSelector) return el.mozMatchesSelector(selector);
            		else if (el.msMatchesSelector) return el.msMatchesSelector(selector);
            		else {
            			compareWith = $(selector);
            			for (i = 0; i < compareWith.length; i++) {
            				if (compareWith[i] === this[0]) return true;
            			}
            			return false;
            		}
            	}
            	else if (selector === document) return this[0] === document;
            	else if (selector === window) return this[0] === window;
            	else {
            		if (selector.nodeType || selector instanceof Dom7) {
            			compareWith = selector.nodeType ? [selector] : selector;
            			for (i = 0; i < compareWith.length; i++) {
            				if (compareWith[i] === this[0]) return true;
            			}
            			return false;
            		}
            		return false;
            	}

            },
            index: function () {
            	if (this[0]) {
            		var child = this[0];
            		var i = 0;
            		while ((child = child.previousSibling) !== null) {
            			if (child.nodeType === 1) i++;
            		}
            		return i;
            	}
            	else return undefined;
            },
            eq: function (index) {
            	if (typeof index === 'undefined') return this;
            	var length = this.length;
            	var returnIndex;
            	if (index > length - 1) {
            		return new Dom7([]);
            	}
            	if (index < 0) {
            		returnIndex = length + index;
            		if (returnIndex < 0) return new Dom7([]);
            		else return new Dom7([this[returnIndex]]);
            	}
            	return new Dom7([this[index]]);
            },
            append: function (newChild) {
            	var i, j;
            	for (i = 0; i < this.length; i++) {
            		if (typeof newChild === 'string') {
            			var tempDiv = document.createElement('div');
            			tempDiv.innerHTML = newChild;
            			while (tempDiv.firstChild) {
            				this[i].appendChild(tempDiv.firstChild);
            			}
            		}
            		else if (newChild instanceof Dom7) {
            			for (j = 0; j < newChild.length; j++) {
            				this[i].appendChild(newChild[j]);
            			}
            		}
            		else {
            			this[i].appendChild(newChild);
            		}
            	}
            	return this;
            },
            prepend: function (newChild) {
            	var i, j;
            	for (i = 0; i < this.length; i++) {
            		if (typeof newChild === 'string') {
            			var tempDiv = document.createElement('div');
            			tempDiv.innerHTML = newChild;
            			for (j = tempDiv.childNodes.length - 1; j >= 0; j--) {
            				this[i].insertBefore(tempDiv.childNodes[j], this[i].childNodes[0]);
            			}
                        // this[i].insertAdjacentHTML('afterbegin', newChild);
                    }
                    else if (newChild instanceof Dom7) {
                    	for (j = 0; j < newChild.length; j++) {
                    		this[i].insertBefore(newChild[j], this[i].childNodes[0]);
                    	}
                    }
                    else {
                    	this[i].insertBefore(newChild, this[i].childNodes[0]);
                    }
                }
                return this;
            },
            insertBefore: function (selector) {
            	var before = $(selector);
            	for (var i = 0; i < this.length; i++) {
            		if (before.length === 1) {
            			before[0].parentNode.insertBefore(this[i], before[0]);
            		}
            		else if (before.length > 1) {
            			for (var j = 0; j < before.length; j++) {
            				before[j].parentNode.insertBefore(this[i].cloneNode(true), before[j]);
            			}
            		}
            	}
            },
            insertAfter: function (selector) {
            	var after = $(selector);
            	for (var i = 0; i < this.length; i++) {
            		if (after.length === 1) {
            			after[0].parentNode.insertBefore(this[i], after[0].nextSibling);
            		}
            		else if (after.length > 1) {
            			for (var j = 0; j < after.length; j++) {
            				after[j].parentNode.insertBefore(this[i].cloneNode(true), after[j].nextSibling);
            			}
            		}
            	}
            },
            next: function (selector) {
            	if (this.length > 0) {
            		if (selector) {
            			if (this[0].nextElementSibling && $(this[0].nextElementSibling).is(selector)) return new Dom7([this[0].nextElementSibling]);
            			else return new Dom7([]);
            		}
            		else {
            			if (this[0].nextElementSibling) return new Dom7([this[0].nextElementSibling]);
            			else return new Dom7([]);
            		}
            	}
            	else return new Dom7([]);
            },
            nextAll: function (selector) {
            	var nextEls = [];
            	var el = this[0];
            	if (!el) return new Dom7([]);
            	while (el.nextElementSibling) {
            		var next = el.nextElementSibling;
            		if (selector) {
            			if($(next).is(selector)) nextEls.push(next);
            		}
            		else nextEls.push(next);
            		el = next;
            	}
            	return new Dom7(nextEls);
            },
            prev: function (selector) {
            	if (this.length > 0) {
            		if (selector) {
            			if (this[0].previousElementSibling && $(this[0].previousElementSibling).is(selector)) return new Dom7([this[0].previousElementSibling]);
            			else return new Dom7([]);
            		}
            		else {
            			if (this[0].previousElementSibling) return new Dom7([this[0].previousElementSibling]);
            			else return new Dom7([]);
            		}
            	}
            	else return new Dom7([]);
            },
            prevAll: function (selector) {
            	var prevEls = [];
            	var el = this[0];
            	if (!el) return new Dom7([]);
            	while (el.previousElementSibling) {
            		var prev = el.previousElementSibling;
            		if (selector) {
            			if($(prev).is(selector)) prevEls.push(prev);
            		}
            		else prevEls.push(prev);
            		el = prev;
            	}
            	return new Dom7(prevEls);
            },
            parent: function (selector) {
            	var parents = [];
            	for (var i = 0; i < this.length; i++) {
            		if (selector) {
            			if ($(this[i].parentNode).is(selector)) parents.push(this[i].parentNode);
            		}
            		else {
            			parents.push(this[i].parentNode);
            		}
            	}
            	return $($.unique(parents));
            },
            parents: function (selector) {
            	var parents = [];
            	for (var i = 0; i < this.length; i++) {
            		var parent = this[i].parentNode;
            		while (parent) {
            			if (selector) {
            				if ($(parent).is(selector)) parents.push(parent);
            			}
            			else {
            				parents.push(parent);
            			}
            			parent = parent.parentNode;
            		}
            	}
            	return $($.unique(parents));
            },
            find : function (selector) {
            	var foundElements = [];
            	for (var i = 0; i < this.length; i++) {
            		var found = this[i].querySelectorAll(selector);
            		for (var j = 0; j < found.length; j++) {
            			foundElements.push(found[j]);
            		}
            	}
            	return new Dom7(foundElements);
            },
            children: function (selector) {
            	var children = [];
            	for (var i = 0; i < this.length; i++) {
            		var childNodes = this[i].childNodes;

            		for (var j = 0; j < childNodes.length; j++) {
            			if (!selector) {
            				if (childNodes[j].nodeType === 1) children.push(childNodes[j]);
            			}
            			else {
            				if (childNodes[j].nodeType === 1 && $(childNodes[j]).is(selector)) children.push(childNodes[j]);
            			}
            		}
            	}
            	return new Dom7($.unique(children));
            },
            remove: function () {
            	for (var i = 0; i < this.length; i++) {
            		if (this[i].parentNode) this[i].parentNode.removeChild(this[i]);
            	}
            	return this;
            },
            add: function () {
            	var dom = this;
            	var i, j;
            	for (i = 0; i < arguments.length; i++) {
            		var toAdd = $(arguments[i]);
            		for (j = 0; j < toAdd.length; j++) {
            			dom[dom.length] = toAdd[j];
            			dom.length++;
            		}
            	}
            	return dom;
            }
        };
        $.fn = Dom7.prototype;
        $.unique = function (arr) {
        	var unique = [];
        	for (var i = 0; i < arr.length; i++) {
        		if (unique.indexOf(arr[i]) === -1) unique.push(arr[i]);
        	}
        	return unique;
        };

        return $;
    })();
    

    /*===========================
     Get Dom libraries
     ===========================*/
     var swiperDomPlugins = ['jQuery', 'Zepto', 'Dom7'];
     for (var i = 0; i < swiperDomPlugins.length; i++) {
     	if (window[swiperDomPlugins[i]]) {
     		addLibraryPlugin(window[swiperDomPlugins[i]]);
     	}
     }
    // Required DOM Plugins
    var domLib;
    if (typeof Dom7 === 'undefined') {
    	domLib = window.Dom7 || window.Zepto || window.jQuery;
    }
    else {
    	domLib = Dom7;
    }

    /*===========================
    Add .swiper plugin from Dom libraries
    ===========================*/
    function addLibraryPlugin(lib) {
    	lib.fn.swiper = function (params) {
    		var firstInstance;
    		lib(this).each(function () {
    			var s = new Swiper(this, params);
    			if (!firstInstance) firstInstance = s;
    		});
    		return firstInstance;
    	};
    }
    
    if (domLib) {
    	if (!('transitionEnd' in domLib.fn)) {
    		domLib.fn.transitionEnd = function (callback) {
    			var events = ['webkitTransitionEnd', 'transitionend', 'oTransitionEnd', 'MSTransitionEnd', 'msTransitionEnd'],
    			i, j, dom = this;
    			function fireCallBack(e) {
    				/*jshint validthis:true */
    				if (e.target !== this) return;
    				callback.call(this, e);
    				for (i = 0; i < events.length; i++) {
    					dom.off(events[i], fireCallBack);
    				}
    			}
    			if (callback) {
    				for (i = 0; i < events.length; i++) {
    					dom.on(events[i], fireCallBack);
    				}
    			}
    			return this;
    		};
    	}
    	if (!('transform' in domLib.fn)) {
    		domLib.fn.transform = function (transform) {
    			for (var i = 0; i < this.length; i++) {
    				var elStyle = this[i].style;
    				elStyle.webkitTransform = elStyle.MsTransform = elStyle.msTransform = elStyle.MozTransform = elStyle.OTransform = elStyle.transform = transform;
    			}
    			return this;
    		};
    	}
    	if (!('transition' in domLib.fn)) {
    		domLib.fn.transition = function (duration) {
    			if (typeof duration !== 'string') {
    				duration = duration + 'ms';
    			}
    			for (var i = 0; i < this.length; i++) {
    				var elStyle = this[i].style;
    				elStyle.webkitTransitionDuration = elStyle.MsTransitionDuration = elStyle.msTransitionDuration = elStyle.MozTransitionDuration = elStyle.OTransitionDuration = elStyle.transitionDuration = duration;
    			}
    			return this;
    		};
    	}
    }

    window.Swiper = Swiper;
})();
/*===========================
Swiper AMD Export
===========================*/
if (typeof(module) !== 'undefined')
{
	module.exports = window.Swiper;
}
else if (typeof define === 'function' && define.amd) {
	define([], function () {
		'use strict';
		return window.Swiper;
	});
}
//     Underscore.js 1.8.3
//     http://underscorejs.org
//     (c) 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.

(function() {

  // Baseline setup
  // --------------

  // Establish the root object, `window` in the browser, or `exports` on the server.
  var root = this;

  // Save the previous value of the `_` variable.
  var previousUnderscore = root._;

  // Save bytes in the minified (but not gzipped) version:
  var ArrayProto = Array.prototype, ObjProto = Object.prototype, FuncProto = Function.prototype;

  // Create quick reference variables for speed access to core prototypes.
  var
  push             = ArrayProto.push,
  slice            = ArrayProto.slice,
  toString         = ObjProto.toString,
  hasOwnProperty   = ObjProto.hasOwnProperty;

  // All **ECMAScript 5** native function implementations that we hope to use
  // are declared here.
  var
  nativeIsArray      = Array.isArray,
  nativeKeys         = Object.keys,
  nativeBind         = FuncProto.bind,
  nativeCreate       = Object.create;

  // Naked function reference for surrogate-prototype-swapping.
  var Ctor = function(){};

  // Create a safe reference to the Underscore object for use below.
  var _ = function(obj) {
  	if (obj instanceof _) return obj;
  	if (!(this instanceof _)) return new _(obj);
  	this._wrapped = obj;
  };

  // Export the Underscore object for **Node.js**, with
  // backwards-compatibility for the old `require()` API. If we're in
  // the browser, add `_` as a global object.
  if (typeof exports !== 'undefined') {
  	if (typeof module !== 'undefined' && module.exports) {
  		exports = module.exports = _;
  	}
  	exports._ = _;
  } else {
  	root._ = _;
  }

  // Current version.
  _.VERSION = '1.8.3';

  // Internal function that returns an efficient (for current engines) version
  // of the passed-in callback, to be repeatedly applied in other Underscore
  // functions.
  var optimizeCb = function(func, context, argCount) {
  	if (context === void 0) return func;
  	switch (argCount == null ? 3 : argCount) {
  		case 1: return function(value) {
  			return func.call(context, value);
  		};
  		case 2: return function(value, other) {
  			return func.call(context, value, other);
  		};
  		case 3: return function(value, index, collection) {
  			return func.call(context, value, index, collection);
  		};
  		case 4: return function(accumulator, value, index, collection) {
  			return func.call(context, accumulator, value, index, collection);
  		};
  	}
  	return function() {
  		return func.apply(context, arguments);
  	};
  };

  // A mostly-internal function to generate callbacks that can be applied
  // to each element in a collection, returning the desired result — either
  // identity, an arbitrary callback, a property matcher, or a property accessor.
  var cb = function(value, context, argCount) {
  	if (value == null) return _.identity;
  	if (_.isFunction(value)) return optimizeCb(value, context, argCount);
  	if (_.isObject(value)) return _.matcher(value);
  	return _.property(value);
  };
  _.iteratee = function(value, context) {
  	return cb(value, context, Infinity);
  };

  // An internal function for creating assigner functions.
  var createAssigner = function(keysFunc, undefinedOnly) {
  	return function(obj) {
  		var length = arguments.length;
  		if (length < 2 || obj == null) return obj;
  		for (var index = 1; index < length; index++) {
  			var source = arguments[index],
  			keys = keysFunc(source),
  			l = keys.length;
  			for (var i = 0; i < l; i++) {
  				var key = keys[i];
  				if (!undefinedOnly || obj[key] === void 0) obj[key] = source[key];
  			}
  		}
  		return obj;
  	};
  };

  // An internal function for creating a new object that inherits from another.
  var baseCreate = function(prototype) {
  	if (!_.isObject(prototype)) return {};
  	if (nativeCreate) return nativeCreate(prototype);
  	Ctor.prototype = prototype;
  	var result = new Ctor;
  	Ctor.prototype = null;
  	return result;
  };

  var property = function(key) {
  	return function(obj) {
  		return obj == null ? void 0 : obj[key];
  	};
  };

  // Helper for collection methods to determine whether a collection
  // should be iterated as an array or as an object
  // Related: http://people.mozilla.org/~jorendorff/es6-draft.html#sec-tolength
  // Avoids a very nasty iOS 8 JIT bug on ARM-64. #2094
  var MAX_ARRAY_INDEX = Math.pow(2, 53) - 1;
  var getLength = property('length');
  var isArrayLike = function(collection) {
  	var length = getLength(collection);
  	return typeof length == 'number' && length >= 0 && length <= MAX_ARRAY_INDEX;
  };

  // Collection Functions
  // --------------------

  // The cornerstone, an `each` implementation, aka `forEach`.
  // Handles raw objects in addition to array-likes. Treats all
  // sparse array-likes as if they were dense.
  _.each = _.forEach = function(obj, iteratee, context) {
  	iteratee = optimizeCb(iteratee, context);
  	var i, length;
  	if (isArrayLike(obj)) {
  		for (i = 0, length = obj.length; i < length; i++) {
  			iteratee(obj[i], i, obj);
  		}
  	} else {
  		var keys = _.keys(obj);
  		for (i = 0, length = keys.length; i < length; i++) {
  			iteratee(obj[keys[i]], keys[i], obj);
  		}
  	}
  	return obj;
  };

  // Return the results of applying the iteratee to each element.
  _.map = _.collect = function(obj, iteratee, context) {
  	iteratee = cb(iteratee, context);
  	var keys = !isArrayLike(obj) && _.keys(obj),
  	length = (keys || obj).length,
  	results = Array(length);
  	for (var index = 0; index < length; index++) {
  		var currentKey = keys ? keys[index] : index;
  		results[index] = iteratee(obj[currentKey], currentKey, obj);
  	}
  	return results;
  };

  // Create a reducing function iterating left or right.
  function createReduce(dir) {
    // Optimized iterator function as using arguments.length
    // in the main function will deoptimize the, see #1991.
    function iterator(obj, iteratee, memo, keys, index, length) {
    	for (; index >= 0 && index < length; index += dir) {
    		var currentKey = keys ? keys[index] : index;
    		memo = iteratee(memo, obj[currentKey], currentKey, obj);
    	}
    	return memo;
    }

    return function(obj, iteratee, memo, context) {
    	iteratee = optimizeCb(iteratee, context, 4);
    	var keys = !isArrayLike(obj) && _.keys(obj),
    	length = (keys || obj).length,
    	index = dir > 0 ? 0 : length - 1;
      // Determine the initial value if none is provided.
      if (arguments.length < 3) {
      	memo = obj[keys ? keys[index] : index];
      	index += dir;
      }
      return iterator(obj, iteratee, memo, keys, index, length);
  };
}

  // **Reduce** builds up a single result from a list of values, aka `inject`,
  // or `foldl`.
  _.reduce = _.foldl = _.inject = createReduce(1);

  // The right-associative version of reduce, also known as `foldr`.
  _.reduceRight = _.foldr = createReduce(-1);

  // Return the first value which passes a truth test. Aliased as `detect`.
  _.find = _.detect = function(obj, predicate, context) {
  	var key;
  	if (isArrayLike(obj)) {
  		key = _.findIndex(obj, predicate, context);
  	} else {
  		key = _.findKey(obj, predicate, context);
  	}
  	if (key !== void 0 && key !== -1) return obj[key];
  };

  // Return all the elements that pass a truth test.
  // Aliased as `select`.
  _.filter = _.select = function(obj, predicate, context) {
  	var results = [];
  	predicate = cb(predicate, context);
  	_.each(obj, function(value, index, list) {
  		if (predicate(value, index, list)) results.push(value);
  	});
  	return results;
  };

  // Return all the elements for which a truth test fails.
  _.reject = function(obj, predicate, context) {
  	return _.filter(obj, _.negate(cb(predicate)), context);
  };

  // Determine whether all of the elements match a truth test.
  // Aliased as `all`.
  _.every = _.all = function(obj, predicate, context) {
  	predicate = cb(predicate, context);
  	var keys = !isArrayLike(obj) && _.keys(obj),
  	length = (keys || obj).length;
  	for (var index = 0; index < length; index++) {
  		var currentKey = keys ? keys[index] : index;
  		if (!predicate(obj[currentKey], currentKey, obj)) return false;
  	}
  	return true;
  };

  // Determine if at least one element in the object matches a truth test.
  // Aliased as `any`.
  _.some = _.any = function(obj, predicate, context) {
  	predicate = cb(predicate, context);
  	var keys = !isArrayLike(obj) && _.keys(obj),
  	length = (keys || obj).length;
  	for (var index = 0; index < length; index++) {
  		var currentKey = keys ? keys[index] : index;
  		if (predicate(obj[currentKey], currentKey, obj)) return true;
  	}
  	return false;
  };

  // Determine if the array or object contains a given item (using `===`).
  // Aliased as `includes` and `include`.
  _.contains = _.includes = _.include = function(obj, item, fromIndex, guard) {
  	if (!isArrayLike(obj)) obj = _.values(obj);
  	if (typeof fromIndex != 'number' || guard) fromIndex = 0;
  	return _.indexOf(obj, item, fromIndex) >= 0;
  };

  // Invoke a method (with arguments) on every item in a collection.
  _.invoke = function(obj, method) {
  	var args = slice.call(arguments, 2);
  	var isFunc = _.isFunction(method);
  	return _.map(obj, function(value) {
  		var func = isFunc ? method : value[method];
  		return func == null ? func : func.apply(value, args);
  	});
  };

  // Convenience version of a common use case of `map`: fetching a property.
  _.pluck = function(obj, key) {
  	return _.map(obj, _.property(key));
  };

  // Convenience version of a common use case of `filter`: selecting only objects
  // containing specific `key:value` pairs.
  _.where = function(obj, attrs) {
  	return _.filter(obj, _.matcher(attrs));
  };

  // Convenience version of a common use case of `find`: getting the first object
  // containing specific `key:value` pairs.
  _.findWhere = function(obj, attrs) {
  	return _.find(obj, _.matcher(attrs));
  };

  // Return the maximum element (or element-based computation).
  _.max = function(obj, iteratee, context) {
  	var result = -Infinity, lastComputed = -Infinity,
  	value, computed;
  	if (iteratee == null && obj != null) {
  		obj = isArrayLike(obj) ? obj : _.values(obj);
  		for (var i = 0, length = obj.length; i < length; i++) {
  			value = obj[i];
  			if (value > result) {
  				result = value;
  			}
  		}
  	} else {
  		iteratee = cb(iteratee, context);
  		_.each(obj, function(value, index, list) {
  			computed = iteratee(value, index, list);
  			if (computed > lastComputed || computed === -Infinity && result === -Infinity) {
  				result = value;
  				lastComputed = computed;
  			}
  		});
  	}
  	return result;
  };

  // Return the minimum element (or element-based computation).
  _.min = function(obj, iteratee, context) {
  	var result = Infinity, lastComputed = Infinity,
  	value, computed;
  	if (iteratee == null && obj != null) {
  		obj = isArrayLike(obj) ? obj : _.values(obj);
  		for (var i = 0, length = obj.length; i < length; i++) {
  			value = obj[i];
  			if (value < result) {
  				result = value;
  			}
  		}
  	} else {
  		iteratee = cb(iteratee, context);
  		_.each(obj, function(value, index, list) {
  			computed = iteratee(value, index, list);
  			if (computed < lastComputed || computed === Infinity && result === Infinity) {
  				result = value;
  				lastComputed = computed;
  			}
  		});
  	}
  	return result;
  };

  // Shuffle a collection, using the modern version of the
  // [Fisher-Yates shuffle](http://en.wikipedia.org/wiki/Fisher–Yates_shuffle).
  _.shuffle = function(obj) {
  	var set = isArrayLike(obj) ? obj : _.values(obj);
  	var length = set.length;
  	var shuffled = Array(length);
  	for (var index = 0, rand; index < length; index++) {
  		rand = _.random(0, index);
  		if (rand !== index) shuffled[index] = shuffled[rand];
  		shuffled[rand] = set[index];
  	}
  	return shuffled;
  };

  // Sample **n** random values from a collection.
  // If **n** is not specified, returns a single random element.
  // The internal `guard` argument allows it to work with `map`.
  _.sample = function(obj, n, guard) {
  	if (n == null || guard) {
  		if (!isArrayLike(obj)) obj = _.values(obj);
  		return obj[_.random(obj.length - 1)];
  	}
  	return _.shuffle(obj).slice(0, Math.max(0, n));
  };

  // Sort the object's values by a criterion produced by an iteratee.
  _.sortBy = function(obj, iteratee, context) {
  	iteratee = cb(iteratee, context);
  	return _.pluck(_.map(obj, function(value, index, list) {
  		return {
  			value: value,
  			index: index,
  			criteria: iteratee(value, index, list)
  		};
  	}).sort(function(left, right) {
  		var a = left.criteria;
  		var b = right.criteria;
  		if (a !== b) {
  			if (a > b || a === void 0) return 1;
  			if (a < b || b === void 0) return -1;
  		}
  		return left.index - right.index;
  	}), 'value');
  };

  // An internal function used for aggregate "group by" operations.
  var group = function(behavior) {
  	return function(obj, iteratee, context) {
  		var result = {};
  		iteratee = cb(iteratee, context);
  		_.each(obj, function(value, index) {
  			var key = iteratee(value, index, obj);
  			behavior(result, value, key);
  		});
  		return result;
  	};
  };

  // Groups the object's values by a criterion. Pass either a string attribute
  // to group by, or a function that returns the criterion.
  _.groupBy = group(function(result, value, key) {
  	if (_.has(result, key)) result[key].push(value); else result[key] = [value];
  });

  // Indexes the object's values by a criterion, similar to `groupBy`, but for
  // when you know that your index values will be unique.
  _.indexBy = group(function(result, value, key) {
  	result[key] = value;
  });

  // Counts instances of an object that group by a certain criterion. Pass
  // either a string attribute to count by, or a function that returns the
  // criterion.
  _.countBy = group(function(result, value, key) {
  	if (_.has(result, key)) result[key]++; else result[key] = 1;
  });

  // Safely create a real, live array from anything iterable.
  _.toArray = function(obj) {
  	if (!obj) return [];
  	if (_.isArray(obj)) return slice.call(obj);
  	if (isArrayLike(obj)) return _.map(obj, _.identity);
  	return _.values(obj);
  };

  // Return the number of elements in an object.
  _.size = function(obj) {
  	if (obj == null) return 0;
  	return isArrayLike(obj) ? obj.length : _.keys(obj).length;
  };

  // Split a collection into two arrays: one whose elements all satisfy the given
  // predicate, and one whose elements all do not satisfy the predicate.
  _.partition = function(obj, predicate, context) {
  	predicate = cb(predicate, context);
  	var pass = [], fail = [];
  	_.each(obj, function(value, key, obj) {
  		(predicate(value, key, obj) ? pass : fail).push(value);
  	});
  	return [pass, fail];
  };

  // Array Functions
  // ---------------

  // Get the first element of an array. Passing **n** will return the first N
  // values in the array. Aliased as `head` and `take`. The **guard** check
  // allows it to work with `_.map`.
  _.first = _.head = _.take = function(array, n, guard) {
  	if (array == null) return void 0;
  	if (n == null || guard) return array[0];
  	return _.initial(array, array.length - n);
  };

  // Returns everything but the last entry of the array. Especially useful on
  // the arguments object. Passing **n** will return all the values in
  // the array, excluding the last N.
  _.initial = function(array, n, guard) {
  	return slice.call(array, 0, Math.max(0, array.length - (n == null || guard ? 1 : n)));
  };

  // Get the last element of an array. Passing **n** will return the last N
  // values in the array.
  _.last = function(array, n, guard) {
  	if (array == null) return void 0;
  	if (n == null || guard) return array[array.length - 1];
  	return _.rest(array, Math.max(0, array.length - n));
  };

  // Returns everything but the first entry of the array. Aliased as `tail` and `drop`.
  // Especially useful on the arguments object. Passing an **n** will return
  // the rest N values in the array.
  _.rest = _.tail = _.drop = function(array, n, guard) {
  	return slice.call(array, n == null || guard ? 1 : n);
  };

  // Trim out all falsy values from an array.
  _.compact = function(array) {
  	return _.filter(array, _.identity);
  };

  // Internal implementation of a recursive `flatten` function.
  var flatten = function(input, shallow, strict, startIndex) {
  	var output = [], idx = 0;
  	for (var i = startIndex || 0, length = getLength(input); i < length; i++) {
  		var value = input[i];
  		if (isArrayLike(value) && (_.isArray(value) || _.isArguments(value))) {
        //flatten current level of array or arguments object
        if (!shallow) value = flatten(value, shallow, strict);
        var j = 0, len = value.length;
        output.length += len;
        while (j < len) {
        	output[idx++] = value[j++];
        }
    } else if (!strict) {
    	output[idx++] = value;
    }
}
return output;
};

  // Flatten out an array, either recursively (by default), or just one level.
  _.flatten = function(array, shallow) {
  	return flatten(array, shallow, false);
  };

  // Return a version of the array that does not contain the specified value(s).
  _.without = function(array) {
  	return _.difference(array, slice.call(arguments, 1));
  };

  // Produce a duplicate-free version of the array. If the array has already
  // been sorted, you have the option of using a faster algorithm.
  // Aliased as `unique`.
  _.uniq = _.unique = function(array, isSorted, iteratee, context) {
  	if (!_.isBoolean(isSorted)) {
  		context = iteratee;
  		iteratee = isSorted;
  		isSorted = false;
  	}
  	if (iteratee != null) iteratee = cb(iteratee, context);
  	var result = [];
  	var seen = [];
  	for (var i = 0, length = getLength(array); i < length; i++) {
  		var value = array[i],
  		computed = iteratee ? iteratee(value, i, array) : value;
  		if (isSorted) {
  			if (!i || seen !== computed) result.push(value);
  			seen = computed;
  		} else if (iteratee) {
  			if (!_.contains(seen, computed)) {
  				seen.push(computed);
  				result.push(value);
  			}
  		} else if (!_.contains(result, value)) {
  			result.push(value);
  		}
  	}
  	return result;
  };

  // Produce an array that contains the union: each distinct element from all of
  // the passed-in arrays.
  _.union = function() {
  	return _.uniq(flatten(arguments, true, true));
  };

  // Produce an array that contains every item shared between all the
  // passed-in arrays.
  _.intersection = function(array) {
  	var result = [];
  	var argsLength = arguments.length;
  	for (var i = 0, length = getLength(array); i < length; i++) {
  		var item = array[i];
  		if (_.contains(result, item)) continue;
  		for (var j = 1; j < argsLength; j++) {
  			if (!_.contains(arguments[j], item)) break;
  		}
  		if (j === argsLength) result.push(item);
  	}
  	return result;
  };

  // Take the difference between one array and a number of other arrays.
  // Only the elements present in just the first array will remain.
  _.difference = function(array) {
  	var rest = flatten(arguments, true, true, 1);
  	return _.filter(array, function(value){
  		return !_.contains(rest, value);
  	});
  };

  // Zip together multiple lists into a single array -- elements that share
  // an index go together.
  _.zip = function() {
  	return _.unzip(arguments);
  };

  // Complement of _.zip. Unzip accepts an array of arrays and groups
  // each array's elements on shared indices
  _.unzip = function(array) {
  	var length = array && _.max(array, getLength).length || 0;
  	var result = Array(length);

  	for (var index = 0; index < length; index++) {
  		result[index] = _.pluck(array, index);
  	}
  	return result;
  };

  // Converts lists into objects. Pass either a single array of `[key, value]`
  // pairs, or two parallel arrays of the same length -- one of keys, and one of
  // the corresponding values.
  _.object = function(list, values) {
  	var result = {};
  	for (var i = 0, length = getLength(list); i < length; i++) {
  		if (values) {
  			result[list[i]] = values[i];
  		} else {
  			result[list[i][0]] = list[i][1];
  		}
  	}
  	return result;
  };

  // Generator function to create the findIndex and findLastIndex functions
  function createPredicateIndexFinder(dir) {
  	return function(array, predicate, context) {
  		predicate = cb(predicate, context);
  		var length = getLength(array);
  		var index = dir > 0 ? 0 : length - 1;
  		for (; index >= 0 && index < length; index += dir) {
  			if (predicate(array[index], index, array)) return index;
  		}
  		return -1;
  	};
  }

  // Returns the first index on an array-like that passes a predicate test
  _.findIndex = createPredicateIndexFinder(1);
  _.findLastIndex = createPredicateIndexFinder(-1);

  // Use a comparator function to figure out the smallest index at which
  // an object should be inserted so as to maintain order. Uses binary search.
  _.sortedIndex = function(array, obj, iteratee, context) {
  	iteratee = cb(iteratee, context, 1);
  	var value = iteratee(obj);
  	var low = 0, high = getLength(array);
  	while (low < high) {
  		var mid = Math.floor((low + high) / 2);
  		if (iteratee(array[mid]) < value) low = mid + 1; else high = mid;
  	}
  	return low;
  };

  // Generator function to create the indexOf and lastIndexOf functions
  function createIndexFinder(dir, predicateFind, sortedIndex) {
  	return function(array, item, idx) {
  		var i = 0, length = getLength(array);
  		if (typeof idx == 'number') {
  			if (dir > 0) {
  				i = idx >= 0 ? idx : Math.max(idx + length, i);
  			} else {
  				length = idx >= 0 ? Math.min(idx + 1, length) : idx + length + 1;
  			}
  		} else if (sortedIndex && idx && length) {
  			idx = sortedIndex(array, item);
  			return array[idx] === item ? idx : -1;
  		}
  		if (item !== item) {
  			idx = predicateFind(slice.call(array, i, length), _.isNaN);
  			return idx >= 0 ? idx + i : -1;
  		}
  		for (idx = dir > 0 ? i : length - 1; idx >= 0 && idx < length; idx += dir) {
  			if (array[idx] === item) return idx;
  		}
  		return -1;
  	};
  }

  // Return the position of the first occurrence of an item in an array,
  // or -1 if the item is not included in the array.
  // If the array is large and already in sort order, pass `true`
  // for **isSorted** to use binary search.
  _.indexOf = createIndexFinder(1, _.findIndex, _.sortedIndex);
  _.lastIndexOf = createIndexFinder(-1, _.findLastIndex);

  // Generate an integer Array containing an arithmetic progression. A port of
  // the native Python `range()` function. See
  // [the Python documentation](http://docs.python.org/library/functions.html#range).
  _.range = function(start, stop, step) {
  	if (stop == null) {
  		stop = start || 0;
  		start = 0;
  	}
  	step = step || 1;

  	var length = Math.max(Math.ceil((stop - start) / step), 0);
  	var range = Array(length);

  	for (var idx = 0; idx < length; idx++, start += step) {
  		range[idx] = start;
  	}

  	return range;
  };

  // Function (ahem) Functions
  // ------------------

  // Determines whether to execute a function as a constructor
  // or a normal function with the provided arguments
  var executeBound = function(sourceFunc, boundFunc, context, callingContext, args) {
  	if (!(callingContext instanceof boundFunc)) return sourceFunc.apply(context, args);
  	var self = baseCreate(sourceFunc.prototype);
  	var result = sourceFunc.apply(self, args);
  	if (_.isObject(result)) return result;
  	return self;
  };

  // Create a function bound to a given object (assigning `this`, and arguments,
  // optionally). Delegates to **ECMAScript 5**'s native `Function.bind` if
  // available.
  _.bind = function(func, context) {
  	if (nativeBind && func.bind === nativeBind) return nativeBind.apply(func, slice.call(arguments, 1));
  	if (!_.isFunction(func)) throw new TypeError('Bind must be called on a function');
  	var args = slice.call(arguments, 2);
  	var bound = function() {
  		return executeBound(func, bound, context, this, args.concat(slice.call(arguments)));
  	};
  	return bound;
  };

  // Partially apply a function by creating a version that has had some of its
  // arguments pre-filled, without changing its dynamic `this` context. _ acts
  // as a placeholder, allowing any combination of arguments to be pre-filled.
  _.partial = function(func) {
  	var boundArgs = slice.call(arguments, 1);
  	var bound = function() {
  		var position = 0, length = boundArgs.length;
  		var args = Array(length);
  		for (var i = 0; i < length; i++) {
  			args[i] = boundArgs[i] === _ ? arguments[position++] : boundArgs[i];
  		}
  		while (position < arguments.length) args.push(arguments[position++]);
  		return executeBound(func, bound, this, this, args);
  	};
  	return bound;
  };

  // Bind a number of an object's methods to that object. Remaining arguments
  // are the method names to be bound. Useful for ensuring that all callbacks
  // defined on an object belong to it.
  _.bindAll = function(obj) {
  	var i, length = arguments.length, key;
  	if (length <= 1) throw new Error('bindAll must be passed function names');
  	for (i = 1; i < length; i++) {
  		key = arguments[i];
  		obj[key] = _.bind(obj[key], obj);
  	}
  	return obj;
  };

  // Memoize an expensive function by storing its results.
  _.memoize = function(func, hasher) {
  	var memoize = function(key) {
  		var cache = memoize.cache;
  		var address = '' + (hasher ? hasher.apply(this, arguments) : key);
  		if (!_.has(cache, address)) cache[address] = func.apply(this, arguments);
  		return cache[address];
  	};
  	memoize.cache = {};
  	return memoize;
  };

  // Delays a function for the given number of milliseconds, and then calls
  // it with the arguments supplied.
  _.delay = function(func, wait) {
  	var args = slice.call(arguments, 2);
  	return setTimeout(function(){
  		return func.apply(null, args);
  	}, wait);
  };

  // Defers a function, scheduling it to run after the current call stack has
  // cleared.
  _.defer = _.partial(_.delay, _, 1);

  // Returns a function, that, when invoked, will only be triggered at most once
  // during a given window of time. Normally, the throttled function will run
  // as much as it can, without ever going more than once per `wait` duration;
  // but if you'd like to disable the execution on the leading edge, pass
  // `{leading: false}`. To disable execution on the trailing edge, ditto.
  _.throttle = function(func, wait, options) {
  	var context, args, result;
  	var timeout = null;
  	var previous = 0;
  	if (!options) options = {};
  	var later = function() {
  		previous = options.leading === false ? 0 : _.now();
  		timeout = null;
  		result = func.apply(context, args);
  		if (!timeout) context = args = null;
  	};
  	return function() {
  		var now = _.now();
  		if (!previous && options.leading === false) previous = now;
  		var remaining = wait - (now - previous);
  		context = this;
  		args = arguments;
  		if (remaining <= 0 || remaining > wait) {
  			if (timeout) {
  				clearTimeout(timeout);
  				timeout = null;
  			}
  			previous = now;
  			result = func.apply(context, args);
  			if (!timeout) context = args = null;
  		} else if (!timeout && options.trailing !== false) {
  			timeout = setTimeout(later, remaining);
  		}
  		return result;
  	};
  };

  // Returns a function, that, as long as it continues to be invoked, will not
  // be triggered. The function will be called after it stops being called for
  // N milliseconds. If `immediate` is passed, trigger the function on the
  // leading edge, instead of the trailing.
  _.debounce = function(func, wait, immediate) {
  	var timeout, args, context, timestamp, result;

  	var later = function() {
  		var last = _.now() - timestamp;

  		if (last < wait && last >= 0) {
  			timeout = setTimeout(later, wait - last);
  		} else {
  			timeout = null;
  			if (!immediate) {
  				result = func.apply(context, args);
  				if (!timeout) context = args = null;
  			}
  		}
  	};

  	return function() {
  		context = this;
  		args = arguments;
  		timestamp = _.now();
  		var callNow = immediate && !timeout;
  		if (!timeout) timeout = setTimeout(later, wait);
  		if (callNow) {
  			result = func.apply(context, args);
  			context = args = null;
  		}

  		return result;
  	};
  };

  // Returns the first function passed as an argument to the second,
  // allowing you to adjust arguments, run code before and after, and
  // conditionally execute the original function.
  _.wrap = function(func, wrapper) {
  	return _.partial(wrapper, func);
  };

  // Returns a negated version of the passed-in predicate.
  _.negate = function(predicate) {
  	return function() {
  		return !predicate.apply(this, arguments);
  	};
  };

  // Returns a function that is the composition of a list of functions, each
  // consuming the return value of the function that follows.
  _.compose = function() {
  	var args = arguments;
  	var start = args.length - 1;
  	return function() {
  		var i = start;
  		var result = args[start].apply(this, arguments);
  		while (i--) result = args[i].call(this, result);
  		return result;
  	};
  };

  // Returns a function that will only be executed on and after the Nth call.
  _.after = function(times, func) {
  	return function() {
  		if (--times < 1) {
  			return func.apply(this, arguments);
  		}
  	};
  };

  // Returns a function that will only be executed up to (but not including) the Nth call.
  _.before = function(times, func) {
  	var memo;
  	return function() {
  		if (--times > 0) {
  			memo = func.apply(this, arguments);
  		}
  		if (times <= 1) func = null;
  		return memo;
  	};
  };

  // Returns a function that will be executed at most one time, no matter how
  // often you call it. Useful for lazy initialization.
  _.once = _.partial(_.before, 2);

  // Object Functions
  // ----------------

  // Keys in IE < 9 that won't be iterated by `for key in ...` and thus missed.
  var hasEnumBug = !{toString: null}.propertyIsEnumerable('toString');
  var nonEnumerableProps = ['valueOf', 'isPrototypeOf', 'toString',
  'propertyIsEnumerable', 'hasOwnProperty', 'toLocaleString'];

  function collectNonEnumProps(obj, keys) {
  	var nonEnumIdx = nonEnumerableProps.length;
  	var constructor = obj.constructor;
  	var proto = (_.isFunction(constructor) && constructor.prototype) || ObjProto;

    // Constructor is a special case.
    var prop = 'constructor';
    if (_.has(obj, prop) && !_.contains(keys, prop)) keys.push(prop);

    while (nonEnumIdx--) {
    	prop = nonEnumerableProps[nonEnumIdx];
    	if (prop in obj && obj[prop] !== proto[prop] && !_.contains(keys, prop)) {
    		keys.push(prop);
    	}
    }
}

  // Retrieve the names of an object's own properties.
  // Delegates to **ECMAScript 5**'s native `Object.keys`
  _.keys = function(obj) {
  	if (!_.isObject(obj)) return [];
  	if (nativeKeys) return nativeKeys(obj);
  	var keys = [];
  	for (var key in obj) if (_.has(obj, key)) keys.push(key);
    // Ahem, IE < 9.
if (hasEnumBug) collectNonEnumProps(obj, keys);
return keys;
};

  // Retrieve all the property names of an object.
  _.allKeys = function(obj) {
  	if (!_.isObject(obj)) return [];
  	var keys = [];
  	for (var key in obj) keys.push(key);
    // Ahem, IE < 9.
if (hasEnumBug) collectNonEnumProps(obj, keys);
return keys;
};

  // Retrieve the values of an object's properties.
  _.values = function(obj) {
  	var keys = _.keys(obj);
  	var length = keys.length;
  	var values = Array(length);
  	for (var i = 0; i < length; i++) {
  		values[i] = obj[keys[i]];
  	}
  	return values;
  };

  // Returns the results of applying the iteratee to each element of the object
  // In contrast to _.map it returns an object
  _.mapObject = function(obj, iteratee, context) {
  	iteratee = cb(iteratee, context);
  	var keys =  _.keys(obj),
  	length = keys.length,
  	results = {},
  	currentKey;
  	for (var index = 0; index < length; index++) {
  		currentKey = keys[index];
  		results[currentKey] = iteratee(obj[currentKey], currentKey, obj);
  	}
  	return results;
  };

  // Convert an object into a list of `[key, value]` pairs.
  _.pairs = function(obj) {
  	var keys = _.keys(obj);
  	var length = keys.length;
  	var pairs = Array(length);
  	for (var i = 0; i < length; i++) {
  		pairs[i] = [keys[i], obj[keys[i]]];
  	}
  	return pairs;
  };

  // Invert the keys and values of an object. The values must be serializable.
  _.invert = function(obj) {
  	var result = {};
  	var keys = _.keys(obj);
  	for (var i = 0, length = keys.length; i < length; i++) {
  		result[obj[keys[i]]] = keys[i];
  	}
  	return result;
  };

  // Return a sorted list of the function names available on the object.
  // Aliased as `methods`
  _.functions = _.methods = function(obj) {
  	var names = [];
  	for (var key in obj) {
  		if (_.isFunction(obj[key])) names.push(key);
  	}
  	return names.sort();
  };

  // Extend a given object with all the properties in passed-in object(s).
  _.extend = createAssigner(_.allKeys);

  // Assigns a given object with all the own properties in the passed-in object(s)
  // (https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)
  _.extendOwn = _.assign = createAssigner(_.keys);

  // Returns the first key on an object that passes a predicate test
  _.findKey = function(obj, predicate, context) {
  	predicate = cb(predicate, context);
  	var keys = _.keys(obj), key;
  	for (var i = 0, length = keys.length; i < length; i++) {
  		key = keys[i];
  		if (predicate(obj[key], key, obj)) return key;
  	}
  };

  // Return a copy of the object only containing the whitelisted properties.
  _.pick = function(object, oiteratee, context) {
  	var result = {}, obj = object, iteratee, keys;
  	if (obj == null) return result;
  	if (_.isFunction(oiteratee)) {
  		keys = _.allKeys(obj);
  		iteratee = optimizeCb(oiteratee, context);
  	} else {
  		keys = flatten(arguments, false, false, 1);
  		iteratee = function(value, key, obj) { return key in obj; };
  		obj = Object(obj);
  	}
  	for (var i = 0, length = keys.length; i < length; i++) {
  		var key = keys[i];
  		var value = obj[key];
  		if (iteratee(value, key, obj)) result[key] = value;
  	}
  	return result;
  };

   // Return a copy of the object without the blacklisted properties.
   _.omit = function(obj, iteratee, context) {
   	if (_.isFunction(iteratee)) {
   		iteratee = _.negate(iteratee);
   	} else {
   		var keys = _.map(flatten(arguments, false, false, 1), String);
   		iteratee = function(value, key) {
   			return !_.contains(keys, key);
   		};
   	}
   	return _.pick(obj, iteratee, context);
   };

  // Fill in a given object with default properties.
  _.defaults = createAssigner(_.allKeys, true);

  // Creates an object that inherits from the given prototype object.
  // If additional properties are provided then they will be added to the
  // created object.
  _.create = function(prototype, props) {
  	var result = baseCreate(prototype);
  	if (props) _.extendOwn(result, props);
  	return result;
  };

  // Create a (shallow-cloned) duplicate of an object.
  _.clone = function(obj) {
  	if (!_.isObject(obj)) return obj;
  	return _.isArray(obj) ? obj.slice() : _.extend({}, obj);
  };

  // Invokes interceptor with the obj, and then returns obj.
  // The primary purpose of this method is to "tap into" a method chain, in
  // order to perform operations on intermediate results within the chain.
  _.tap = function(obj, interceptor) {
  	interceptor(obj);
  	return obj;
  };

  // Returns whether an object has a given set of `key:value` pairs.
  _.isMatch = function(object, attrs) {
  	var keys = _.keys(attrs), length = keys.length;
  	if (object == null) return !length;
  	var obj = Object(object);
  	for (var i = 0; i < length; i++) {
  		var key = keys[i];
  		if (attrs[key] !== obj[key] || !(key in obj)) return false;
  	}
  	return true;
  };


  // Internal recursive comparison function for `isEqual`.
  var eq = function(a, b, aStack, bStack) {
    // Identical objects are equal. `0 === -0`, but they aren't identical.
    // See the [Harmony `egal` proposal](http://wiki.ecmascript.org/doku.php?id=harmony:egal).
    if (a === b) return a !== 0 || 1 / a === 1 / b;
    // A strict comparison is necessary because `null == undefined`.
    if (a == null || b == null) return a === b;
    // Unwrap any wrapped objects.
    if (a instanceof _) a = a._wrapped;
    if (b instanceof _) b = b._wrapped;
    // Compare `[[Class]]` names.
    var className = toString.call(a);
    if (className !== toString.call(b)) return false;
    switch (className) {
      // Strings, numbers, regular expressions, dates, and booleans are compared by value.
      case '[object RegExp]':
      // RegExps are coerced to strings for comparison (Note: '' + /a/i === '/a/i')
      case '[object String]':
        // Primitives and their corresponding object wrappers are equivalent; thus, `"5"` is
        // equivalent to `new String("5")`.
        return '' + a === '' + b;
        case '[object Number]':
        // `NaN`s are equivalent, but non-reflexive.
        // Object(NaN) is equivalent to NaN
        if (+a !== +a) return +b !== +b;
        // An `egal` comparison is performed for other numeric values.
        return +a === 0 ? 1 / +a === 1 / b : +a === +b;
        case '[object Date]':
        case '[object Boolean]':
        // Coerce dates and booleans to numeric primitive values. Dates are compared by their
        // millisecond representations. Note that invalid dates with millisecond representations
        // of `NaN` are not equivalent.
        return +a === +b;
    }

    var areArrays = className === '[object Array]';
    if (!areArrays) {
    	if (typeof a != 'object' || typeof b != 'object') return false;

      // Objects with different constructors are not equivalent, but `Object`s or `Array`s
      // from different frames are.
      var aCtor = a.constructor, bCtor = b.constructor;
      if (aCtor !== bCtor && !(_.isFunction(aCtor) && aCtor instanceof aCtor &&
      	_.isFunction(bCtor) && bCtor instanceof bCtor)
      	&& ('constructor' in a && 'constructor' in b)) {
      	return false;
  }
}
    // Assume equality for cyclic structures. The algorithm for detecting cyclic
    // structures is adapted from ES 5.1 section 15.12.3, abstract operation `JO`.

    // Initializing stack of traversed objects.
    // It's done here since we only need them for objects and arrays comparison.
    aStack = aStack || [];
    bStack = bStack || [];
    var length = aStack.length;
    while (length--) {
      // Linear search. Performance is inversely proportional to the number of
      // unique nested structures.
      if (aStack[length] === a) return bStack[length] === b;
  }

    // Add the first object to the stack of traversed objects.
    aStack.push(a);
    bStack.push(b);

    // Recursively compare objects and arrays.
    if (areArrays) {
      // Compare array lengths to determine if a deep comparison is necessary.
      length = a.length;
      if (length !== b.length) return false;
      // Deep compare the contents, ignoring non-numeric properties.
      while (length--) {
      	if (!eq(a[length], b[length], aStack, bStack)) return false;
      }
  } else {
      // Deep compare objects.
      var keys = _.keys(a), key;
      length = keys.length;
      // Ensure that both objects contain the same number of properties before comparing deep equality.
      if (_.keys(b).length !== length) return false;
      while (length--) {
        // Deep compare each member
        key = keys[length];
        if (!(_.has(b, key) && eq(a[key], b[key], aStack, bStack))) return false;
    }
}
    // Remove the first object from the stack of traversed objects.
    aStack.pop();
    bStack.pop();
    return true;
};

  // Perform a deep comparison to check if two objects are equal.
  _.isEqual = function(a, b) {
  	return eq(a, b);
  };

  // Is a given array, string, or object empty?
  // An "empty" object has no enumerable own-properties.
  _.isEmpty = function(obj) {
  	if (obj == null) return true;
  	if (isArrayLike(obj) && (_.isArray(obj) || _.isString(obj) || _.isArguments(obj))) return obj.length === 0;
  	return _.keys(obj).length === 0;
  };

  // Is a given value a DOM element?
  _.isElement = function(obj) {
  	return !!(obj && obj.nodeType === 1);
  };

  // Is a given value an array?
  // Delegates to ECMA5's native Array.isArray
  _.isArray = nativeIsArray || function(obj) {
  	return toString.call(obj) === '[object Array]';
  };

  // Is a given variable an object?
  _.isObject = function(obj) {
  	var type = typeof obj;
  	return type === 'function' || type === 'object' && !!obj;
  };

  // Add some isType methods: isArguments, isFunction, isString, isNumber, isDate, isRegExp, isError.
  _.each(['Arguments', 'Function', 'String', 'Number', 'Date', 'RegExp', 'Error'], function(name) {
  	_['is' + name] = function(obj) {
  		return toString.call(obj) === '[object ' + name + ']';
  	};
  });

  // Define a fallback version of the method in browsers (ahem, IE < 9), where
  // there isn't any inspectable "Arguments" type.
  if (!_.isArguments(arguments)) {
  	_.isArguments = function(obj) {
  		return _.has(obj, 'callee');
  	};
  }

  // Optimize `isFunction` if appropriate. Work around some typeof bugs in old v8,
  // IE 11 (#1621), and in Safari 8 (#1929).
  if (typeof /./ != 'function' && typeof Int8Array != 'object') {
  	_.isFunction = function(obj) {
  		return typeof obj == 'function' || false;
  	};
  }

  // Is a given object a finite number?
  _.isFinite = function(obj) {
  	return isFinite(obj) && !isNaN(parseFloat(obj));
  };

  // Is the given value `NaN`? (NaN is the only number which does not equal itself).
  _.isNaN = function(obj) {
  	return _.isNumber(obj) && obj !== +obj;
  };

  // Is a given value a boolean?
  _.isBoolean = function(obj) {
  	return obj === true || obj === false || toString.call(obj) === '[object Boolean]';
  };

  // Is a given value equal to null?
  _.isNull = function(obj) {
  	return obj === null;
  };

  // Is a given variable undefined?
  _.isUndefined = function(obj) {
  	return obj === void 0;
  };

  // Shortcut function for checking if an object has a given property directly
  // on itself (in other words, not on a prototype).
  _.has = function(obj, key) {
  	return obj != null && hasOwnProperty.call(obj, key);
  };

  // Utility Functions
  // -----------------

  // Run Underscore.js in *noConflict* mode, returning the `_` variable to its
  // previous owner. Returns a reference to the Underscore object.
  _.noConflict = function() {
  	root._ = previousUnderscore;
  	return this;
  };

  // Keep the identity function around for default iteratees.
  _.identity = function(value) {
  	return value;
  };

  // Predicate-generating functions. Often useful outside of Underscore.
  _.constant = function(value) {
  	return function() {
  		return value;
  	};
  };

  _.noop = function(){};

  _.property = property;

  // Generates a function for a given object that returns a given property.
  _.propertyOf = function(obj) {
  	return obj == null ? function(){} : function(key) {
  		return obj[key];
  	};
  };

  // Returns a predicate for checking whether an object has a given set of
  // `key:value` pairs.
  _.matcher = _.matches = function(attrs) {
  	attrs = _.extendOwn({}, attrs);
  	return function(obj) {
  		return _.isMatch(obj, attrs);
  	};
  };

  // Run a function **n** times.
  _.times = function(n, iteratee, context) {
  	var accum = Array(Math.max(0, n));
  	iteratee = optimizeCb(iteratee, context, 1);
  	for (var i = 0; i < n; i++) accum[i] = iteratee(i);
  		return accum;
  };

  // Return a random integer between min and max (inclusive).
  _.random = function(min, max) {
  	if (max == null) {
  		max = min;
  		min = 0;
  	}
  	return min + Math.floor(Math.random() * (max - min + 1));
  };

  // A (possibly faster) way to get the current timestamp as an integer.
  _.now = Date.now || function() {
  	return new Date().getTime();
  };

   // List of HTML entities for escaping.
   var escapeMap = {
   	'&': '&amp;',
   	'<': '&lt;',
   	'>': '&gt;',
   	'"': '&quot;',
   	"'": '&#x27;',
   	'`': '&#x60;'
   };
   var unescapeMap = _.invert(escapeMap);

  // Functions for escaping and unescaping strings to/from HTML interpolation.
  var createEscaper = function(map) {
  	var escaper = function(match) {
  		return map[match];
  	};
    // Regexes for identifying a key that needs to be escaped
    var source = '(?:' + _.keys(map).join('|') + ')';
    var testRegexp = RegExp(source);
    var replaceRegexp = RegExp(source, 'g');
    return function(string) {
    	string = string == null ? '' : '' + string;
    	return testRegexp.test(string) ? string.replace(replaceRegexp, escaper) : string;
    };
};
_.escape = createEscaper(escapeMap);
_.unescape = createEscaper(unescapeMap);

  // If the value of the named `property` is a function then invoke it with the
  // `object` as context; otherwise, return it.
  _.result = function(object, property, fallback) {
  	var value = object == null ? void 0 : object[property];
  	if (value === void 0) {
  		value = fallback;
  	}
  	return _.isFunction(value) ? value.call(object) : value;
  };

  // Generate a unique integer id (unique within the entire client session).
  // Useful for temporary DOM ids.
  var idCounter = 0;
  _.uniqueId = function(prefix) {
  	var id = ++idCounter + '';
  	return prefix ? prefix + id : id;
  };

  // By default, Underscore uses ERB-style template delimiters, change the
  // following template settings to use alternative delimiters.
  _.templateSettings = {
  	evaluate    : /<%([\s\S]+?)%>/g,
  	interpolate : /<%=([\s\S]+?)%>/g,
  	escape      : /<%-([\s\S]+?)%>/g
  };

  // When customizing `templateSettings`, if you don't want to define an
  // interpolation, evaluation or escaping regex, we need one that is
  // guaranteed not to match.
  var noMatch = /(.)^/;

  // Certain characters need to be escaped so that they can be put into a
  // string literal.
  var escapes = {
  	"'":      "'",
  	'\\':     '\\',
  	'\r':     'r',
  	'\n':     'n',
  	'\u2028': 'u2028',
  	'\u2029': 'u2029'
  };

  var escaper = /\\|'|\r|\n|\u2028|\u2029/g;

  var escapeChar = function(match) {
  	return '\\' + escapes[match];
  };

  // JavaScript micro-templating, similar to John Resig's implementation.
  // Underscore templating handles arbitrary delimiters, preserves whitespace,
  // and correctly escapes quotes within interpolated code.
  // NB: `oldSettings` only exists for backwards compatibility.
  _.template = function(text, settings, oldSettings) {
  	if (!settings && oldSettings) settings = oldSettings;
  	settings = _.defaults({}, settings, _.templateSettings);

    // Combine delimiters into one regular expression via alternation.
    var matcher = RegExp([
    	(settings.escape || noMatch).source,
    	(settings.interpolate || noMatch).source,
    	(settings.evaluate || noMatch).source
    	].join('|') + '|$', 'g');

    // Compile the template source, escaping string literals appropriately.
    var index = 0;
    var source = "__p+='";
    text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
    	source += text.slice(index, offset).replace(escaper, escapeChar);
    	index = offset + match.length;

    	if (escape) {
    		source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
    	} else if (interpolate) {
    		source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
    	} else if (evaluate) {
    		source += "';\n" + evaluate + "\n__p+='";
    	}

      // Adobe VMs need the match returned to produce the correct offest.
      return match;
  });
    source += "';\n";

    // If a variable is not specified, place data values in local scope.
    if (!settings.variable) source = 'with(obj||{}){\n' + source + '}\n';

    source = "var __t,__p='',__j=Array.prototype.join," +
    "print=function(){__p+=__j.call(arguments,'');};\n" +
    source + 'return __p;\n';

    try {
    	var render = new Function(settings.variable || 'obj', '_', source);
    } catch (e) {
    	e.source = source;
    	throw e;
    }

    var template = function(data) {
    	return render.call(this, data, _);
    };

    // Provide the compiled source as a convenience for precompilation.
    var argument = settings.variable || 'obj';
    template.source = 'function(' + argument + '){\n' + source + '}';

    return template;
};

  // Add a "chain" function. Start chaining a wrapped Underscore object.
  _.chain = function(obj) {
  	var instance = _(obj);
  	instance._chain = true;
  	return instance;
  };

  // OOP
  // ---------------
  // If Underscore is called as a function, it returns a wrapped object that
  // can be used OO-style. This wrapper holds altered versions of all the
  // underscore functions. Wrapped objects may be chained.

  // Helper function to continue chaining intermediate results.
  var result = function(instance, obj) {
  	return instance._chain ? _(obj).chain() : obj;
  };

  // Add your own custom functions to the Underscore object.
  _.mixin = function(obj) {
  	_.each(_.functions(obj), function(name) {
  		var func = _[name] = obj[name];
  		_.prototype[name] = function() {
  			var args = [this._wrapped];
  			push.apply(args, arguments);
  			return result(this, func.apply(_, args));
  		};
  	});
  };

  // Add all of the Underscore functions to the wrapper object.
  _.mixin(_);

  // Add all mutator Array functions to the wrapper.
  _.each(['pop', 'push', 'reverse', 'shift', 'sort', 'splice', 'unshift'], function(name) {
  	var method = ArrayProto[name];
  	_.prototype[name] = function() {
  		var obj = this._wrapped;
  		method.apply(obj, arguments);
  		if ((name === 'shift' || name === 'splice') && obj.length === 0) delete obj[0];
  		return result(this, obj);
  	};
  });

  // Add all accessor Array functions to the wrapper.
  _.each(['concat', 'join', 'slice'], function(name) {
  	var method = ArrayProto[name];
  	_.prototype[name] = function() {
  		return result(this, method.apply(this._wrapped, arguments));
  	};
  });

  // Extracts the result from a wrapped and chained object.
  _.prototype.value = function() {
  	return this._wrapped;
  };

  // Provide unwrapping proxy for some methods used in engine operations
  // such as arithmetic and JSON stringification.
  _.prototype.valueOf = _.prototype.toJSON = _.prototype.value;

  _.prototype.toString = function() {
  	return '' + this._wrapped;
  };

  // AMD registration happens at the end for compatibility with AMD loaders
  // that may not enforce next-turn semantics on modules. Even though general
  // practice for AMD registration is to be anonymous, underscore registers
  // as a named module because, like jQuery, it is a base library that is
  // popular enough to be bundled in a third party lib, but not be part of
  // an AMD load request. Those cases could generate an error when an
  // anonymous define() is called outside of a loader request.
  if (typeof define === 'function' && define.amd) {
  	define('underscore', [], function() {
  		return _;
  	});
  }
}.call(this));

//! moment.js
//! version : 2.10.6
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com

(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	global.moment = factory()
}(this, function () { 'use strict';

	var hookCallback;

	function utils_hooks__hooks () {
		return hookCallback.apply(null, arguments);
	}

    // This is done to register the method called with moment()
    // without creating circular dependencies.
    function setHookCallback (callback) {
    	hookCallback = callback;
    }

    function isArray(input) {
    	return Object.prototype.toString.call(input) === '[object Array]';
    }

    function isDate(input) {
    	return input instanceof Date || Object.prototype.toString.call(input) === '[object Date]';
    }

    function map(arr, fn) {
    	var res = [], i;
    	for (i = 0; i < arr.length; ++i) {
    		res.push(fn(arr[i], i));
    	}
    	return res;
    }

    function hasOwnProp(a, b) {
    	return Object.prototype.hasOwnProperty.call(a, b);
    }

    function extend(a, b) {
    	for (var i in b) {
    		if (hasOwnProp(b, i)) {
    			a[i] = b[i];
    		}
    	}

    	if (hasOwnProp(b, 'toString')) {
    		a.toString = b.toString;
    	}

    	if (hasOwnProp(b, 'valueOf')) {
    		a.valueOf = b.valueOf;
    	}

    	return a;
    }

    function create_utc__createUTC (input, format, locale, strict) {
    	return createLocalOrUTC(input, format, locale, strict, true).utc();
    }

    function defaultParsingFlags() {
        // We need to deep clone this object.
        return {
        	empty           : false,
        	unusedTokens    : [],
        	unusedInput     : [],
        	overflow        : -2,
        	charsLeftOver   : 0,
        	nullInput       : false,
        	invalidMonth    : null,
        	invalidFormat   : false,
        	userInvalidated : false,
        	iso             : false
        };
    }

    function getParsingFlags(m) {
    	if (m._pf == null) {
    		m._pf = defaultParsingFlags();
    	}
    	return m._pf;
    }

    function valid__isValid(m) {
    	if (m._isValid == null) {
    		var flags = getParsingFlags(m);
    		m._isValid = !isNaN(m._d.getTime()) &&
    		flags.overflow < 0 &&
    		!flags.empty &&
    		!flags.invalidMonth &&
    		!flags.invalidWeekday &&
    		!flags.nullInput &&
    		!flags.invalidFormat &&
    		!flags.userInvalidated;

    		if (m._strict) {
    			m._isValid = m._isValid &&
    			flags.charsLeftOver === 0 &&
    			flags.unusedTokens.length === 0 &&
    			flags.bigHour === undefined;
    		}
    	}
    	return m._isValid;
    }

    function valid__createInvalid (flags) {
    	var m = create_utc__createUTC(NaN);
    	if (flags != null) {
    		extend(getParsingFlags(m), flags);
    	}
    	else {
    		getParsingFlags(m).userInvalidated = true;
    	}

    	return m;
    }

    var momentProperties = utils_hooks__hooks.momentProperties = [];

    function copyConfig(to, from) {
    	var i, prop, val;

    	if (typeof from._isAMomentObject !== 'undefined') {
    		to._isAMomentObject = from._isAMomentObject;
    	}
    	if (typeof from._i !== 'undefined') {
    		to._i = from._i;
    	}
    	if (typeof from._f !== 'undefined') {
    		to._f = from._f;
    	}
    	if (typeof from._l !== 'undefined') {
    		to._l = from._l;
    	}
    	if (typeof from._strict !== 'undefined') {
    		to._strict = from._strict;
    	}
    	if (typeof from._tzm !== 'undefined') {
    		to._tzm = from._tzm;
    	}
    	if (typeof from._isUTC !== 'undefined') {
    		to._isUTC = from._isUTC;
    	}
    	if (typeof from._offset !== 'undefined') {
    		to._offset = from._offset;
    	}
    	if (typeof from._pf !== 'undefined') {
    		to._pf = getParsingFlags(from);
    	}
    	if (typeof from._locale !== 'undefined') {
    		to._locale = from._locale;
    	}

    	if (momentProperties.length > 0) {
    		for (i in momentProperties) {
    			prop = momentProperties[i];
    			val = from[prop];
    			if (typeof val !== 'undefined') {
    				to[prop] = val;
    			}
    		}
    	}

    	return to;
    }

    var updateInProgress = false;

    // Moment prototype object
    function Moment(config) {
    	copyConfig(this, config);
    	this._d = new Date(config._d != null ? config._d.getTime() : NaN);
        // Prevent infinite loop in case updateOffset creates new moment
        // objects.
        if (updateInProgress === false) {
        	updateInProgress = true;
        	utils_hooks__hooks.updateOffset(this);
        	updateInProgress = false;
        }
    }

    function isMoment (obj) {
    	return obj instanceof Moment || (obj != null && obj._isAMomentObject != null);
    }

    function absFloor (number) {
    	if (number < 0) {
    		return Math.ceil(number);
    	} else {
    		return Math.floor(number);
    	}
    }

    function toInt(argumentForCoercion) {
    	var coercedNumber = +argumentForCoercion,
    	value = 0;

    	if (coercedNumber !== 0 && isFinite(coercedNumber)) {
    		value = absFloor(coercedNumber);
    	}

    	return value;
    }

    function compareArrays(array1, array2, dontConvert) {
    	var len = Math.min(array1.length, array2.length),
    	lengthDiff = Math.abs(array1.length - array2.length),
    	diffs = 0,
    	i;
    	for (i = 0; i < len; i++) {
    		if ((dontConvert && array1[i] !== array2[i]) ||
    			(!dontConvert && toInt(array1[i]) !== toInt(array2[i]))) {
    			diffs++;
    	}
    }
    return diffs + lengthDiff;
}

function Locale() {
}

var locales = {};
var globalLocale;

function normalizeLocale(key) {
	return key ? key.toLowerCase().replace('_', '-') : key;
}

    // pick the locale from the array
    // try ['en-au', 'en-gb'] as 'en-au', 'en-gb', 'en', as in move through the list trying each
    // substring from most specific to least, but move to the next array item if it's a more specific variant than the current root
    function chooseLocale(names) {
    	var i = 0, j, next, locale, split;

    	while (i < names.length) {
    		split = normalizeLocale(names[i]).split('-');
    		j = split.length;
    		next = normalizeLocale(names[i + 1]);
    		next = next ? next.split('-') : null;
    		while (j > 0) {
    			locale = loadLocale(split.slice(0, j).join('-'));
    			if (locale) {
    				return locale;
    			}
    			if (next && next.length >= j && compareArrays(split, next, true) >= j - 1) {
                    //the next array item is better than a shallower substring of this one
                    break;
                }
                j--;
            }
            i++;
        }
        return null;
    }

    function loadLocale(name) {
    	var oldLocale = null;
        // TODO: Find a better way to register and load all the locales in Node
        if (!locales[name] && typeof module !== 'undefined' &&
        	module && module.exports) {
        	try {
        		oldLocale = globalLocale._abbr;
        		require('./locale/' + name);
                // because defineLocale currently also sets the global locale, we
                // want to undo that for lazy loaded locales
                locale_locales__getSetGlobalLocale(oldLocale);
            } catch (e) { }
        }
        return locales[name];
    }

    // This function will load locale and then set the global locale.  If
    // no arguments are passed in, it will simply return the current global
    // locale key.
    function locale_locales__getSetGlobalLocale (key, values) {
    	var data;
    	if (key) {
    		if (typeof values === 'undefined') {
    			data = locale_locales__getLocale(key);
    		}
    		else {
    			data = defineLocale(key, values);
    		}

    		if (data) {
                // moment.duration._locale = moment._locale = data;
                globalLocale = data;
            }
        }

        return globalLocale._abbr;
    }

    function defineLocale (name, values) {
    	if (values !== null) {
    		values.abbr = name;
    		locales[name] = locales[name] || new Locale();
    		locales[name].set(values);

            // backwards compat for now: also set the locale
            locale_locales__getSetGlobalLocale(name);

            return locales[name];
        } else {
            // useful for testing
            delete locales[name];
            return null;
        }
    }

    // returns locale data
    function locale_locales__getLocale (key) {
    	var locale;

    	if (key && key._locale && key._locale._abbr) {
    		key = key._locale._abbr;
    	}

    	if (!key) {
    		return globalLocale;
    	}

    	if (!isArray(key)) {
            //short-circuit everything else
            locale = loadLocale(key);
            if (locale) {
            	return locale;
            }
            key = [key];
        }

        return chooseLocale(key);
    }

    var aliases = {};

    function addUnitAlias (unit, shorthand) {
    	var lowerCase = unit.toLowerCase();
    	aliases[lowerCase] = aliases[lowerCase + 's'] = aliases[shorthand] = unit;
    }

    function normalizeUnits(units) {
    	return typeof units === 'string' ? aliases[units] || aliases[units.toLowerCase()] : undefined;
    }

    function normalizeObjectUnits(inputObject) {
    	var normalizedInput = {},
    	normalizedProp,
    	prop;

    	for (prop in inputObject) {
    		if (hasOwnProp(inputObject, prop)) {
    			normalizedProp = normalizeUnits(prop);
    			if (normalizedProp) {
    				normalizedInput[normalizedProp] = inputObject[prop];
    			}
    		}
    	}

    	return normalizedInput;
    }

    function makeGetSet (unit, keepTime) {
    	return function (value) {
    		if (value != null) {
    			get_set__set(this, unit, value);
    			utils_hooks__hooks.updateOffset(this, keepTime);
    			return this;
    		} else {
    			return get_set__get(this, unit);
    		}
    	};
    }

    function get_set__get (mom, unit) {
    	return mom._d['get' + (mom._isUTC ? 'UTC' : '') + unit]();
    }

    function get_set__set (mom, unit, value) {
    	return mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value);
    }

    // MOMENTS

    function getSet (units, value) {
    	var unit;
    	if (typeof units === 'object') {
    		for (unit in units) {
    			this.set(unit, units[unit]);
    		}
    	} else {
    		units = normalizeUnits(units);
    		if (typeof this[units] === 'function') {
    			return this[units](value);
    		}
    	}
    	return this;
    }

    function zeroFill(number, targetLength, forceSign) {
    	var absNumber = '' + Math.abs(number),
    	zerosToFill = targetLength - absNumber.length,
    	sign = number >= 0;
    	return (sign ? (forceSign ? '+' : '') : '-') +
    	Math.pow(10, Math.max(0, zerosToFill)).toString().substr(1) + absNumber;
    }

    var formattingTokens = /(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Q|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g;

    var localFormattingTokens = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g;

    var formatFunctions = {};

    var formatTokenFunctions = {};

    // token:    'M'
    // padded:   ['MM', 2]
    // ordinal:  'Mo'
    // callback: function () { this.month() + 1 }
    function addFormatToken (token, padded, ordinal, callback) {
    	var func = callback;
    	if (typeof callback === 'string') {
    		func = function () {
    			return this[callback]();
    		};
    	}
    	if (token) {
    		formatTokenFunctions[token] = func;
    	}
    	if (padded) {
    		formatTokenFunctions[padded[0]] = function () {
    			return zeroFill(func.apply(this, arguments), padded[1], padded[2]);
    		};
    	}
    	if (ordinal) {
    		formatTokenFunctions[ordinal] = function () {
    			return this.localeData().ordinal(func.apply(this, arguments), token);
    		};
    	}
    }

    function removeFormattingTokens(input) {
    	if (input.match(/\[[\s\S]/)) {
    		return input.replace(/^\[|\]$/g, '');
    	}
    	return input.replace(/\\/g, '');
    }

    function makeFormatFunction(format) {
    	var array = format.match(formattingTokens), i, length;

    	for (i = 0, length = array.length; i < length; i++) {
    		if (formatTokenFunctions[array[i]]) {
    			array[i] = formatTokenFunctions[array[i]];
    		} else {
    			array[i] = removeFormattingTokens(array[i]);
    		}
    	}

    	return function (mom) {
    		var output = '';
    		for (i = 0; i < length; i++) {
    			output += array[i] instanceof Function ? array[i].call(mom, format) : array[i];
    		}
    		return output;
    	};
    }

    // format date using native date object
    function formatMoment(m, format) {
    	if (!m.isValid()) {
    		return m.localeData().invalidDate();
    	}

    	format = expandFormat(format, m.localeData());
    	formatFunctions[format] = formatFunctions[format] || makeFormatFunction(format);

    	return formatFunctions[format](m);
    }

    function expandFormat(format, locale) {
    	var i = 5;

    	function replaceLongDateFormatTokens(input) {
    		return locale.longDateFormat(input) || input;
    	}

    	localFormattingTokens.lastIndex = 0;
    	while (i >= 0 && localFormattingTokens.test(format)) {
    		format = format.replace(localFormattingTokens, replaceLongDateFormatTokens);
    		localFormattingTokens.lastIndex = 0;
    		i -= 1;
    	}

    	return format;
    }

    var match1         = /\d/;            //       0 - 9
    var match2         = /\d\d/;          //      00 - 99
    var match3         = /\d{3}/;         //     000 - 999
    var match4         = /\d{4}/;         //    0000 - 9999
    var match6         = /[+-]?\d{6}/;    // -999999 - 999999
    var match1to2      = /\d\d?/;         //       0 - 99
    var match1to3      = /\d{1,3}/;       //       0 - 999
    var match1to4      = /\d{1,4}/;       //       0 - 9999
    var match1to6      = /[+-]?\d{1,6}/;  // -999999 - 999999

    var matchUnsigned  = /\d+/;           //       0 - inf
    var matchSigned    = /[+-]?\d+/;      //    -inf - inf

    var matchOffset    = /Z|[+-]\d\d:?\d\d/gi; // +00:00 -00:00 +0000 -0000 or Z

    var matchTimestamp = /[+-]?\d+(\.\d{1,3})?/; // 123456789 123456789.123

    // any word (or two) characters or numbers including two/three word month in arabic.
    var matchWord = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i;

    var regexes = {};

    function isFunction (sth) {
        // https://github.com/moment/moment/issues/2325
        return typeof sth === 'function' &&
        Object.prototype.toString.call(sth) === '[object Function]';
    }


    function addRegexToken (token, regex, strictRegex) {
    	regexes[token] = isFunction(regex) ? regex : function (isStrict) {
    		return (isStrict && strictRegex) ? strictRegex : regex;
    	};
    }

    function getParseRegexForToken (token, config) {
    	if (!hasOwnProp(regexes, token)) {
    		return new RegExp(unescapeFormat(token));
    	}

    	return regexes[token](config._strict, config._locale);
    }

    // Code from http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript
    function unescapeFormat(s) {
    	return s.replace('\\', '').replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function (matched, p1, p2, p3, p4) {
    		return p1 || p2 || p3 || p4;
    	}).replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    }

    var tokens = {};

    function addParseToken (token, callback) {
    	var i, func = callback;
    	if (typeof token === 'string') {
    		token = [token];
    	}
    	if (typeof callback === 'number') {
    		func = function (input, array) {
    			array[callback] = toInt(input);
    		};
    	}
    	for (i = 0; i < token.length; i++) {
    		tokens[token[i]] = func;
    	}
    }

    function addWeekParseToken (token, callback) {
    	addParseToken(token, function (input, array, config, token) {
    		config._w = config._w || {};
    		callback(input, config._w, config, token);
    	});
    }

    function addTimeToArrayFromToken(token, input, config) {
    	if (input != null && hasOwnProp(tokens, token)) {
    		tokens[token](input, config._a, config, token);
    	}
    }

    var YEAR = 0;
    var MONTH = 1;
    var DATE = 2;
    var HOUR = 3;
    var MINUTE = 4;
    var SECOND = 5;
    var MILLISECOND = 6;

    function daysInMonth(year, month) {
    	return new Date(Date.UTC(year, month + 1, 0)).getUTCDate();
    }

    // FORMATTING

    addFormatToken('M', ['MM', 2], 'Mo', function () {
    	return this.month() + 1;
    });

    addFormatToken('MMM', 0, 0, function (format) {
    	return this.localeData().monthsShort(this, format);
    });

    addFormatToken('MMMM', 0, 0, function (format) {
    	return this.localeData().months(this, format);
    });

    // ALIASES

    addUnitAlias('month', 'M');

    // PARSING

    addRegexToken('M',    match1to2);
    addRegexToken('MM',   match1to2, match2);
    addRegexToken('MMM',  matchWord);
    addRegexToken('MMMM', matchWord);

    addParseToken(['M', 'MM'], function (input, array) {
    	array[MONTH] = toInt(input) - 1;
    });

    addParseToken(['MMM', 'MMMM'], function (input, array, config, token) {
    	var month = config._locale.monthsParse(input, token, config._strict);
        // if we didn't find a month name, mark the date as invalid.
        if (month != null) {
        	array[MONTH] = month;
        } else {
        	getParsingFlags(config).invalidMonth = input;
        }
    });

    // LOCALES

    var defaultLocaleMonths = 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_');
    function localeMonths (m) {
    	return this._months[m.month()];
    }

    var defaultLocaleMonthsShort = 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_');
    function localeMonthsShort (m) {
    	return this._monthsShort[m.month()];
    }

    function localeMonthsParse (monthName, format, strict) {
    	var i, mom, regex;

    	if (!this._monthsParse) {
    		this._monthsParse = [];
    		this._longMonthsParse = [];
    		this._shortMonthsParse = [];
    	}

    	for (i = 0; i < 12; i++) {
            // make the regex if we don't have it already
            mom = create_utc__createUTC([2000, i]);
            if (strict && !this._longMonthsParse[i]) {
            	this._longMonthsParse[i] = new RegExp('^' + this.months(mom, '').replace('.', '') + '$', 'i');
            	this._shortMonthsParse[i] = new RegExp('^' + this.monthsShort(mom, '').replace('.', '') + '$', 'i');
            }
            if (!strict && !this._monthsParse[i]) {
            	regex = '^' + this.months(mom, '') + '|^' + this.monthsShort(mom, '');
            	this._monthsParse[i] = new RegExp(regex.replace('.', ''), 'i');
            }
            // test the regex
            if (strict && format === 'MMMM' && this._longMonthsParse[i].test(monthName)) {
            	return i;
            } else if (strict && format === 'MMM' && this._shortMonthsParse[i].test(monthName)) {
            	return i;
            } else if (!strict && this._monthsParse[i].test(monthName)) {
            	return i;
            }
        }
    }

    // MOMENTS

    function setMonth (mom, value) {
    	var dayOfMonth;

        // TODO: Move this out of here!
        if (typeof value === 'string') {
        	value = mom.localeData().monthsParse(value);
            // TODO: Another silent failure?
            if (typeof value !== 'number') {
            	return mom;
            }
        }

        dayOfMonth = Math.min(mom.date(), daysInMonth(mom.year(), value));
        mom._d['set' + (mom._isUTC ? 'UTC' : '') + 'Month'](value, dayOfMonth);
        return mom;
    }

    function getSetMonth (value) {
    	if (value != null) {
    		setMonth(this, value);
    		utils_hooks__hooks.updateOffset(this, true);
    		return this;
    	} else {
    		return get_set__get(this, 'Month');
    	}
    }

    function getDaysInMonth () {
    	return daysInMonth(this.year(), this.month());
    }

    function checkOverflow (m) {
    	var overflow;
    	var a = m._a;

    	if (a && getParsingFlags(m).overflow === -2) {
    		overflow =
    		a[MONTH]       < 0 || a[MONTH]       > 11  ? MONTH :
    		a[DATE]        < 1 || a[DATE]        > daysInMonth(a[YEAR], a[MONTH]) ? DATE :
    		a[HOUR]        < 0 || a[HOUR]        > 24 || (a[HOUR] === 24 && (a[MINUTE] !== 0 || a[SECOND] !== 0 || a[MILLISECOND] !== 0)) ? HOUR :
    		a[MINUTE]      < 0 || a[MINUTE]      > 59  ? MINUTE :
    		a[SECOND]      < 0 || a[SECOND]      > 59  ? SECOND :
    		a[MILLISECOND] < 0 || a[MILLISECOND] > 999 ? MILLISECOND :
    		-1;

    		if (getParsingFlags(m)._overflowDayOfYear && (overflow < YEAR || overflow > DATE)) {
    			overflow = DATE;
    		}

    		getParsingFlags(m).overflow = overflow;
    	}

    	return m;
    }

    function warn(msg) {
    	if (utils_hooks__hooks.suppressDeprecationWarnings === false && typeof console !== 'undefined' && console.warn) {
    		console.warn('Deprecation warning: ' + msg);
    	}
    }

    function deprecate(msg, fn) {
    	var firstTime = true;

    	return extend(function () {
    		if (firstTime) {
    			warn(msg + '\n' + (new Error()).stack);
    			firstTime = false;
    		}
    		return fn.apply(this, arguments);
    	}, fn);
    }

    var deprecations = {};

    function deprecateSimple(name, msg) {
    	if (!deprecations[name]) {
    		warn(msg);
    		deprecations[name] = true;
    	}
    }

    utils_hooks__hooks.suppressDeprecationWarnings = false;

    var from_string__isoRegex = /^\s*(?:[+-]\d{6}|\d{4})-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;

    var isoDates = [
    ['YYYYYY-MM-DD', /[+-]\d{6}-\d{2}-\d{2}/],
    ['YYYY-MM-DD', /\d{4}-\d{2}-\d{2}/],
    ['GGGG-[W]WW-E', /\d{4}-W\d{2}-\d/],
    ['GGGG-[W]WW', /\d{4}-W\d{2}/],
    ['YYYY-DDD', /\d{4}-\d{3}/]
    ];

    // iso time formats and regexes
    var isoTimes = [
    ['HH:mm:ss.SSSS', /(T| )\d\d:\d\d:\d\d\.\d+/],
    ['HH:mm:ss', /(T| )\d\d:\d\d:\d\d/],
    ['HH:mm', /(T| )\d\d:\d\d/],
    ['HH', /(T| )\d\d/]
    ];

    var aspNetJsonRegex = /^\/?Date\((\-?\d+)/i;

    // date from iso format
    function configFromISO(config) {
    	var i, l,
    	string = config._i,
    	match = from_string__isoRegex.exec(string);

    	if (match) {
    		getParsingFlags(config).iso = true;
    		for (i = 0, l = isoDates.length; i < l; i++) {
    			if (isoDates[i][1].exec(string)) {
    				config._f = isoDates[i][0];
    				break;
    			}
    		}
    		for (i = 0, l = isoTimes.length; i < l; i++) {
    			if (isoTimes[i][1].exec(string)) {
                    // match[6] should be 'T' or space
                    config._f += (match[6] || ' ') + isoTimes[i][0];
                    break;
                }
            }
            if (string.match(matchOffset)) {
            	config._f += 'Z';
            }
            configFromStringAndFormat(config);
        } else {
        	config._isValid = false;
        }
    }

    // date from iso format or fallback
    function configFromString(config) {
    	var matched = aspNetJsonRegex.exec(config._i);

    	if (matched !== null) {
    		config._d = new Date(+matched[1]);
    		return;
    	}

    	configFromISO(config);
    	if (config._isValid === false) {
    		delete config._isValid;
    		utils_hooks__hooks.createFromInputFallback(config);
    	}
    }

    utils_hooks__hooks.createFromInputFallback = deprecate(
    	'moment construction falls back to js Date. This is ' +
    	'discouraged and will be removed in upcoming major ' +
    	'release. Please refer to ' +
    	'https://github.com/moment/moment/issues/1407 for more info.',
    	function (config) {
    		config._d = new Date(config._i + (config._useUTC ? ' UTC' : ''));
    	}
    	);

    function createDate (y, m, d, h, M, s, ms) {
        //can't just apply() to create a date:
        //http://stackoverflow.com/questions/181348/instantiating-a-javascript-object-by-calling-prototype-constructor-apply
        var date = new Date(y, m, d, h, M, s, ms);

        //the date constructor doesn't accept years < 1970
        if (y < 1970) {
        	date.setFullYear(y);
        }
        return date;
    }

    function createUTCDate (y) {
    	var date = new Date(Date.UTC.apply(null, arguments));
    	if (y < 1970) {
    		date.setUTCFullYear(y);
    	}
    	return date;
    }

    addFormatToken(0, ['YY', 2], 0, function () {
    	return this.year() % 100;
    });

    addFormatToken(0, ['YYYY',   4],       0, 'year');
    addFormatToken(0, ['YYYYY',  5],       0, 'year');
    addFormatToken(0, ['YYYYYY', 6, true], 0, 'year');

    // ALIASES

    addUnitAlias('year', 'y');

    // PARSING

    addRegexToken('Y',      matchSigned);
    addRegexToken('YY',     match1to2, match2);
    addRegexToken('YYYY',   match1to4, match4);
    addRegexToken('YYYYY',  match1to6, match6);
    addRegexToken('YYYYYY', match1to6, match6);

    addParseToken(['YYYYY', 'YYYYYY'], YEAR);
    addParseToken('YYYY', function (input, array) {
    	array[YEAR] = input.length === 2 ? utils_hooks__hooks.parseTwoDigitYear(input) : toInt(input);
    });
    addParseToken('YY', function (input, array) {
    	array[YEAR] = utils_hooks__hooks.parseTwoDigitYear(input);
    });

    // HELPERS

    function daysInYear(year) {
    	return isLeapYear(year) ? 366 : 365;
    }

    function isLeapYear(year) {
    	return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
    }

    // HOOKS

    utils_hooks__hooks.parseTwoDigitYear = function (input) {
    	return toInt(input) + (toInt(input) > 68 ? 1900 : 2000);
    };

    // MOMENTS

    var getSetYear = makeGetSet('FullYear', false);

    function getIsLeapYear () {
    	return isLeapYear(this.year());
    }

    addFormatToken('w', ['ww', 2], 'wo', 'week');
    addFormatToken('W', ['WW', 2], 'Wo', 'isoWeek');

    // ALIASES

    addUnitAlias('week', 'w');
    addUnitAlias('isoWeek', 'W');

    // PARSING

    addRegexToken('w',  match1to2);
    addRegexToken('ww', match1to2, match2);
    addRegexToken('W',  match1to2);
    addRegexToken('WW', match1to2, match2);

    addWeekParseToken(['w', 'ww', 'W', 'WW'], function (input, week, config, token) {
    	week[token.substr(0, 1)] = toInt(input);
    });

    // HELPERS

    // firstDayOfWeek       0 = sun, 6 = sat
    //                      the day of the week that starts the week
    //                      (usually sunday or monday)
    // firstDayOfWeekOfYear 0 = sun, 6 = sat
    //                      the first week is the week that contains the first
    //                      of this day of the week
    //                      (eg. ISO weeks use thursday (4))
    function weekOfYear(mom, firstDayOfWeek, firstDayOfWeekOfYear) {
    	var end = firstDayOfWeekOfYear - firstDayOfWeek,
    	daysToDayOfWeek = firstDayOfWeekOfYear - mom.day(),
    	adjustedMoment;


    	if (daysToDayOfWeek > end) {
    		daysToDayOfWeek -= 7;
    	}

    	if (daysToDayOfWeek < end - 7) {
    		daysToDayOfWeek += 7;
    	}

    	adjustedMoment = local__createLocal(mom).add(daysToDayOfWeek, 'd');
    	return {
    		week: Math.ceil(adjustedMoment.dayOfYear() / 7),
    		year: adjustedMoment.year()
    	};
    }

    // LOCALES

    function localeWeek (mom) {
    	return weekOfYear(mom, this._week.dow, this._week.doy).week;
    }

    var defaultLocaleWeek = {
        dow : 0, // Sunday is the first day of the week.
        doy : 6  // The week that contains Jan 1st is the first week of the year.
    };

    function localeFirstDayOfWeek () {
    	return this._week.dow;
    }

    function localeFirstDayOfYear () {
    	return this._week.doy;
    }

    // MOMENTS

    function getSetWeek (input) {
    	var week = this.localeData().week(this);
    	return input == null ? week : this.add((input - week) * 7, 'd');
    }

    function getSetISOWeek (input) {
    	var week = weekOfYear(this, 1, 4).week;
    	return input == null ? week : this.add((input - week) * 7, 'd');
    }

    addFormatToken('DDD', ['DDDD', 3], 'DDDo', 'dayOfYear');

    // ALIASES

    addUnitAlias('dayOfYear', 'DDD');

    // PARSING

    addRegexToken('DDD',  match1to3);
    addRegexToken('DDDD', match3);
    addParseToken(['DDD', 'DDDD'], function (input, array, config) {
    	config._dayOfYear = toInt(input);
    });

    // HELPERS

    //http://en.wikipedia.org/wiki/ISO_week_date#Calculating_a_date_given_the_year.2C_week_number_and_weekday
    function dayOfYearFromWeeks(year, week, weekday, firstDayOfWeekOfYear, firstDayOfWeek) {
    	var week1Jan = 6 + firstDayOfWeek - firstDayOfWeekOfYear, janX = createUTCDate(year, 0, 1 + week1Jan), d = janX.getUTCDay(), dayOfYear;
    	if (d < firstDayOfWeek) {
    		d += 7;
    	}

    	weekday = weekday != null ? 1 * weekday : firstDayOfWeek;

    	dayOfYear = 1 + week1Jan + 7 * (week - 1) - d + weekday;

    	return {
    		year: dayOfYear > 0 ? year : year - 1,
    		dayOfYear: dayOfYear > 0 ?  dayOfYear : daysInYear(year - 1) + dayOfYear
    	};
    }

    // MOMENTS

    function getSetDayOfYear (input) {
    	var dayOfYear = Math.round((this.clone().startOf('day') - this.clone().startOf('year')) / 864e5) + 1;
    	return input == null ? dayOfYear : this.add((input - dayOfYear), 'd');
    }

    // Pick the first defined of two or three arguments.
    function defaults(a, b, c) {
    	if (a != null) {
    		return a;
    	}
    	if (b != null) {
    		return b;
    	}
    	return c;
    }

    function currentDateArray(config) {
    	var now = new Date();
    	if (config._useUTC) {
    		return [now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate()];
    	}
    	return [now.getFullYear(), now.getMonth(), now.getDate()];
    }

    // convert an array to a date.
    // the array should mirror the parameters below
    // note: all values past the year are optional and will default to the lowest possible value.
    // [year, month, day , hour, minute, second, millisecond]
    function configFromArray (config) {
    	var i, date, input = [], currentDate, yearToUse;

    	if (config._d) {
    		return;
    	}

    	currentDate = currentDateArray(config);

        //compute day of the year from weeks and weekdays
        if (config._w && config._a[DATE] == null && config._a[MONTH] == null) {
        	dayOfYearFromWeekInfo(config);
        }

        //if the day of the year is set, figure out what it is
        if (config._dayOfYear) {
        	yearToUse = defaults(config._a[YEAR], currentDate[YEAR]);

        	if (config._dayOfYear > daysInYear(yearToUse)) {
        		getParsingFlags(config)._overflowDayOfYear = true;
        	}

        	date = createUTCDate(yearToUse, 0, config._dayOfYear);
        	config._a[MONTH] = date.getUTCMonth();
        	config._a[DATE] = date.getUTCDate();
        }

        // Default to current date.
        // * if no year, month, day of month are given, default to today
        // * if day of month is given, default month and year
        // * if month is given, default only year
        // * if year is given, don't default anything
        for (i = 0; i < 3 && config._a[i] == null; ++i) {
        	config._a[i] = input[i] = currentDate[i];
        }

        // Zero out whatever was not defaulted, including time
        for (; i < 7; i++) {
        	config._a[i] = input[i] = (config._a[i] == null) ? (i === 2 ? 1 : 0) : config._a[i];
        }

        // Check for 24:00:00.000
        if (config._a[HOUR] === 24 &&
        	config._a[MINUTE] === 0 &&
        	config._a[SECOND] === 0 &&
        	config._a[MILLISECOND] === 0) {
        	config._nextDay = true;
        config._a[HOUR] = 0;
    }

    config._d = (config._useUTC ? createUTCDate : createDate).apply(null, input);
        // Apply timezone offset from input. The actual utcOffset can be changed
        // with parseZone.
        if (config._tzm != null) {
        	config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);
        }

        if (config._nextDay) {
        	config._a[HOUR] = 24;
        }
    }

    function dayOfYearFromWeekInfo(config) {
    	var w, weekYear, week, weekday, dow, doy, temp;

    	w = config._w;
    	if (w.GG != null || w.W != null || w.E != null) {
    		dow = 1;
    		doy = 4;

            // TODO: We need to take the current isoWeekYear, but that depends on
            // how we interpret now (local, utc, fixed offset). So create
            // a now version of current config (take local/utc/offset flags, and
            // create now).
weekYear = defaults(w.GG, config._a[YEAR], weekOfYear(local__createLocal(), 1, 4).year);
week = defaults(w.W, 1);
weekday = defaults(w.E, 1);
} else {
	dow = config._locale._week.dow;
	doy = config._locale._week.doy;

	weekYear = defaults(w.gg, config._a[YEAR], weekOfYear(local__createLocal(), dow, doy).year);
	week = defaults(w.w, 1);

	if (w.d != null) {
                // weekday -- low day numbers are considered next week
                weekday = w.d;
                if (weekday < dow) {
                	++week;
                }
            } else if (w.e != null) {
                // local weekday -- counting starts from begining of week
                weekday = w.e + dow;
            } else {
                // default to begining of week
                weekday = dow;
            }
        }
        temp = dayOfYearFromWeeks(weekYear, week, weekday, doy, dow);

        config._a[YEAR] = temp.year;
        config._dayOfYear = temp.dayOfYear;
    }

    utils_hooks__hooks.ISO_8601 = function () {};

    // date from string and format string
    function configFromStringAndFormat(config) {
        // TODO: Move this to another part of the creation flow to prevent circular deps
        if (config._f === utils_hooks__hooks.ISO_8601) {
        	configFromISO(config);
        	return;
        }

        config._a = [];
        getParsingFlags(config).empty = true;

        // This array is used to make a Date, either with `new Date` or `Date.UTC`
        var string = '' + config._i,
        i, parsedInput, tokens, token, skipped,
        stringLength = string.length,
        totalParsedInputLength = 0;

        tokens = expandFormat(config._f, config._locale).match(formattingTokens) || [];

        for (i = 0; i < tokens.length; i++) {
        	token = tokens[i];
        	parsedInput = (string.match(getParseRegexForToken(token, config)) || [])[0];
        	if (parsedInput) {
        		skipped = string.substr(0, string.indexOf(parsedInput));
        		if (skipped.length > 0) {
        			getParsingFlags(config).unusedInput.push(skipped);
        		}
        		string = string.slice(string.indexOf(parsedInput) + parsedInput.length);
        		totalParsedInputLength += parsedInput.length;
        	}
            // don't parse if it's not a known token
            if (formatTokenFunctions[token]) {
            	if (parsedInput) {
            		getParsingFlags(config).empty = false;
            	}
            	else {
            		getParsingFlags(config).unusedTokens.push(token);
            	}
            	addTimeToArrayFromToken(token, parsedInput, config);
            }
            else if (config._strict && !parsedInput) {
            	getParsingFlags(config).unusedTokens.push(token);
            }
        }

        // add remaining unparsed input length to the string
        getParsingFlags(config).charsLeftOver = stringLength - totalParsedInputLength;
        if (string.length > 0) {
        	getParsingFlags(config).unusedInput.push(string);
        }

        // clear _12h flag if hour is <= 12
        if (getParsingFlags(config).bigHour === true &&
        	config._a[HOUR] <= 12 &&
        	config._a[HOUR] > 0) {
        	getParsingFlags(config).bigHour = undefined;
    }
        // handle meridiem
        config._a[HOUR] = meridiemFixWrap(config._locale, config._a[HOUR], config._meridiem);

        configFromArray(config);
        checkOverflow(config);
    }


    function meridiemFixWrap (locale, hour, meridiem) {
    	var isPm;

    	if (meridiem == null) {
            // nothing to do
            return hour;
        }
        if (locale.meridiemHour != null) {
        	return locale.meridiemHour(hour, meridiem);
        } else if (locale.isPM != null) {
            // Fallback
            isPm = locale.isPM(meridiem);
            if (isPm && hour < 12) {
            	hour += 12;
            }
            if (!isPm && hour === 12) {
            	hour = 0;
            }
            return hour;
        } else {
            // this is not supposed to happen
            return hour;
        }
    }

    function configFromStringAndArray(config) {
    	var tempConfig,
    	bestMoment,

    	scoreToBeat,
    	i,
    	currentScore;

    	if (config._f.length === 0) {
    		getParsingFlags(config).invalidFormat = true;
    		config._d = new Date(NaN);
    		return;
    	}

    	for (i = 0; i < config._f.length; i++) {
    		currentScore = 0;
    		tempConfig = copyConfig({}, config);
    		if (config._useUTC != null) {
    			tempConfig._useUTC = config._useUTC;
    		}
    		tempConfig._f = config._f[i];
    		configFromStringAndFormat(tempConfig);

    		if (!valid__isValid(tempConfig)) {
    			continue;
    		}

            // if there is any input that was not parsed add a penalty for that format
            currentScore += getParsingFlags(tempConfig).charsLeftOver;

            //or tokens
            currentScore += getParsingFlags(tempConfig).unusedTokens.length * 10;

            getParsingFlags(tempConfig).score = currentScore;

            if (scoreToBeat == null || currentScore < scoreToBeat) {
            	scoreToBeat = currentScore;
            	bestMoment = tempConfig;
            }
        }

        extend(config, bestMoment || tempConfig);
    }

    function configFromObject(config) {
    	if (config._d) {
    		return;
    	}

    	var i = normalizeObjectUnits(config._i);
    	config._a = [i.year, i.month, i.day || i.date, i.hour, i.minute, i.second, i.millisecond];

    	configFromArray(config);
    }

    function createFromConfig (config) {
    	var res = new Moment(checkOverflow(prepareConfig(config)));
    	if (res._nextDay) {
            // Adding is smart enough around DST
            res.add(1, 'd');
            res._nextDay = undefined;
        }

        return res;
    }

    function prepareConfig (config) {
    	var input = config._i,
    	format = config._f;

    	config._locale = config._locale || locale_locales__getLocale(config._l);

    	if (input === null || (format === undefined && input === '')) {
    		return valid__createInvalid({nullInput: true});
    	}

    	if (typeof input === 'string') {
    		config._i = input = config._locale.preparse(input);
    	}

    	if (isMoment(input)) {
    		return new Moment(checkOverflow(input));
    	} else if (isArray(format)) {
    		configFromStringAndArray(config);
    	} else if (format) {
    		configFromStringAndFormat(config);
    	} else if (isDate(input)) {
    		config._d = input;
    	} else {
    		configFromInput(config);
    	}

    	return config;
    }

    function configFromInput(config) {
    	var input = config._i;
    	if (input === undefined) {
    		config._d = new Date();
    	} else if (isDate(input)) {
    		config._d = new Date(+input);
    	} else if (typeof input === 'string') {
    		configFromString(config);
    	} else if (isArray(input)) {
    		config._a = map(input.slice(0), function (obj) {
    			return parseInt(obj, 10);
    		});
    		configFromArray(config);
    	} else if (typeof(input) === 'object') {
    		configFromObject(config);
    	} else if (typeof(input) === 'number') {
            // from milliseconds
            config._d = new Date(input);
        } else {
        	utils_hooks__hooks.createFromInputFallback(config);
        }
    }

    function createLocalOrUTC (input, format, locale, strict, isUTC) {
    	var c = {};

    	if (typeof(locale) === 'boolean') {
    		strict = locale;
    		locale = undefined;
    	}
        // object construction must be done this way.
        // https://github.com/moment/moment/issues/1423
        c._isAMomentObject = true;
        c._useUTC = c._isUTC = isUTC;
        c._l = locale;
        c._i = input;
        c._f = format;
        c._strict = strict;

        return createFromConfig(c);
    }

    function local__createLocal (input, format, locale, strict) {
    	return createLocalOrUTC(input, format, locale, strict, false);
    }

    var prototypeMin = deprecate(
    	'moment().min is deprecated, use moment.min instead. https://github.com/moment/moment/issues/1548',
    	function () {
    		var other = local__createLocal.apply(null, arguments);
    		return other < this ? this : other;
    	}
    	);

    var prototypeMax = deprecate(
    	'moment().max is deprecated, use moment.max instead. https://github.com/moment/moment/issues/1548',
    	function () {
    		var other = local__createLocal.apply(null, arguments);
    		return other > this ? this : other;
    	}
    	);

    // Pick a moment m from moments so that m[fn](other) is true for all
    // other. This relies on the function fn to be transitive.
    //
    // moments should either be an array of moment objects or an array, whose
    // first element is an array of moment objects.
    function pickBy(fn, moments) {
    	var res, i;
    	if (moments.length === 1 && isArray(moments[0])) {
    		moments = moments[0];
    	}
    	if (!moments.length) {
    		return local__createLocal();
    	}
    	res = moments[0];
    	for (i = 1; i < moments.length; ++i) {
    		if (!moments[i].isValid() || moments[i][fn](res)) {
    			res = moments[i];
    		}
    	}
    	return res;
    }

    // TODO: Use [].sort instead?
    function min () {
    	var args = [].slice.call(arguments, 0);

    	return pickBy('isBefore', args);
    }

    function max () {
    	var args = [].slice.call(arguments, 0);

    	return pickBy('isAfter', args);
    }

    function Duration (duration) {
    	var normalizedInput = normalizeObjectUnits(duration),
    	years = normalizedInput.year || 0,
    	quarters = normalizedInput.quarter || 0,
    	months = normalizedInput.month || 0,
    	weeks = normalizedInput.week || 0,
    	days = normalizedInput.day || 0,
    	hours = normalizedInput.hour || 0,
    	minutes = normalizedInput.minute || 0,
    	seconds = normalizedInput.second || 0,
    	milliseconds = normalizedInput.millisecond || 0;

        // representation for dateAddRemove
        this._milliseconds = +milliseconds +
            seconds * 1e3 + // 1000
            minutes * 6e4 + // 1000 * 60
            hours * 36e5; // 1000 * 60 * 60
        // Because of dateAddRemove treats 24 hours as different from a
        // day when working around DST, we need to store them separately
        this._days = +days +
        weeks * 7;
        // It is impossible translate months into days without knowing
        // which months you are are talking about, so we have to store
        // it separately.
        this._months = +months +
        quarters * 3 +
        years * 12;

        this._data = {};

        this._locale = locale_locales__getLocale();

        this._bubble();
    }

    function isDuration (obj) {
    	return obj instanceof Duration;
    }

    function offset (token, separator) {
    	addFormatToken(token, 0, 0, function () {
    		var offset = this.utcOffset();
    		var sign = '+';
    		if (offset < 0) {
    			offset = -offset;
    			sign = '-';
    		}
    		return sign + zeroFill(~~(offset / 60), 2) + separator + zeroFill(~~(offset) % 60, 2);
    	});
    }

    offset('Z', ':');
    offset('ZZ', '');

    // PARSING

    addRegexToken('Z',  matchOffset);
    addRegexToken('ZZ', matchOffset);
    addParseToken(['Z', 'ZZ'], function (input, array, config) {
    	config._useUTC = true;
    	config._tzm = offsetFromString(input);
    });

    // HELPERS

    // timezone chunker
    // '+10:00' > ['10',  '00']
    // '-1530'  > ['-15', '30']
    var chunkOffset = /([\+\-]|\d\d)/gi;

    function offsetFromString(string) {
    	var matches = ((string || '').match(matchOffset) || []);
    	var chunk   = matches[matches.length - 1] || [];
    	var parts   = (chunk + '').match(chunkOffset) || ['-', 0, 0];
    	var minutes = +(parts[1] * 60) + toInt(parts[2]);

    	return parts[0] === '+' ? minutes : -minutes;
    }

    // Return a moment from input, that is local/utc/zone equivalent to model.
    function cloneWithOffset(input, model) {
    	var res, diff;
    	if (model._isUTC) {
    		res = model.clone();
    		diff = (isMoment(input) || isDate(input) ? +input : +local__createLocal(input)) - (+res);
            // Use low-level api, because this fn is low-level api.
            res._d.setTime(+res._d + diff);
            utils_hooks__hooks.updateOffset(res, false);
            return res;
        } else {
        	return local__createLocal(input).local();
        }
    }

    function getDateOffset (m) {
        // On Firefox.24 Date#getTimezoneOffset returns a floating point.
        // https://github.com/moment/moment/pull/1871
        return -Math.round(m._d.getTimezoneOffset() / 15) * 15;
    }

    // HOOKS

    // This function will be called whenever a moment is mutated.
    // It is intended to keep the offset in sync with the timezone.
    utils_hooks__hooks.updateOffset = function () {};

    // MOMENTS

    // keepLocalTime = true means only change the timezone, without
    // affecting the local hour. So 5:31:26 +0300 --[utcOffset(2, true)]-->
    // 5:31:26 +0200 It is possible that 5:31:26 doesn't exist with offset
    // +0200, so we adjust the time as needed, to be valid.
    //
    // Keeping the time actually adds/subtracts (one hour)
    // from the actual represented time. That is why we call updateOffset
    // a second time. In case it wants us to change the offset again
    // _changeInProgress == true case, then we have to adjust, because
    // there is no such time in the given timezone.
    function getSetOffset (input, keepLocalTime) {
    	var offset = this._offset || 0,
    	localAdjust;
    	if (input != null) {
    		if (typeof input === 'string') {
    			input = offsetFromString(input);
    		}
    		if (Math.abs(input) < 16) {
    			input = input * 60;
    		}
    		if (!this._isUTC && keepLocalTime) {
    			localAdjust = getDateOffset(this);
    		}
    		this._offset = input;
    		this._isUTC = true;
    		if (localAdjust != null) {
    			this.add(localAdjust, 'm');
    		}
    		if (offset !== input) {
    			if (!keepLocalTime || this._changeInProgress) {
    				add_subtract__addSubtract(this, create__createDuration(input - offset, 'm'), 1, false);
    			} else if (!this._changeInProgress) {
    				this._changeInProgress = true;
    				utils_hooks__hooks.updateOffset(this, true);
    				this._changeInProgress = null;
    			}
    		}
    		return this;
    	} else {
    		return this._isUTC ? offset : getDateOffset(this);
    	}
    }

    function getSetZone (input, keepLocalTime) {
    	if (input != null) {
    		if (typeof input !== 'string') {
    			input = -input;
    		}

    		this.utcOffset(input, keepLocalTime);

    		return this;
    	} else {
    		return -this.utcOffset();
    	}
    }

    function setOffsetToUTC (keepLocalTime) {
    	return this.utcOffset(0, keepLocalTime);
    }

    function setOffsetToLocal (keepLocalTime) {
    	if (this._isUTC) {
    		this.utcOffset(0, keepLocalTime);
    		this._isUTC = false;

    		if (keepLocalTime) {
    			this.subtract(getDateOffset(this), 'm');
    		}
    	}
    	return this;
    }

    function setOffsetToParsedOffset () {
    	if (this._tzm) {
    		this.utcOffset(this._tzm);
    	} else if (typeof this._i === 'string') {
    		this.utcOffset(offsetFromString(this._i));
    	}
    	return this;
    }

    function hasAlignedHourOffset (input) {
    	input = input ? local__createLocal(input).utcOffset() : 0;

    	return (this.utcOffset() - input) % 60 === 0;
    }

    function isDaylightSavingTime () {
    	return (
    		this.utcOffset() > this.clone().month(0).utcOffset() ||
    		this.utcOffset() > this.clone().month(5).utcOffset()
    		);
    }

    function isDaylightSavingTimeShifted () {
    	if (typeof this._isDSTShifted !== 'undefined') {
    		return this._isDSTShifted;
    	}

    	var c = {};

    	copyConfig(c, this);
    	c = prepareConfig(c);

    	if (c._a) {
    		var other = c._isUTC ? create_utc__createUTC(c._a) : local__createLocal(c._a);
    		this._isDSTShifted = this.isValid() &&
    		compareArrays(c._a, other.toArray()) > 0;
    	} else {
    		this._isDSTShifted = false;
    	}

    	return this._isDSTShifted;
    }

    function isLocal () {
    	return !this._isUTC;
    }

    function isUtcOffset () {
    	return this._isUTC;
    }

    function isUtc () {
    	return this._isUTC && this._offset === 0;
    }

    var aspNetRegex = /(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/;

    // from http://docs.closure-library.googlecode.com/git/closure_goog_date_date.js.source.html
    // somewhat more in line with 4.4.3.2 2004 spec, but allows decimal anywhere
    var create__isoRegex = /^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/;

    function create__createDuration (input, key) {
    	var duration = input,
            // matching against regexp is expensive, do it on demand
            match = null,
            sign,
            ret,
            diffRes;

            if (isDuration(input)) {
            	duration = {
            		ms : input._milliseconds,
            		d  : input._days,
            		M  : input._months
            	};
            } else if (typeof input === 'number') {
            	duration = {};
            	if (key) {
            		duration[key] = input;
            	} else {
            		duration.milliseconds = input;
            	}
            } else if (!!(match = aspNetRegex.exec(input))) {
            	sign = (match[1] === '-') ? -1 : 1;
            	duration = {
            		y  : 0,
            		d  : toInt(match[DATE])        * sign,
            		h  : toInt(match[HOUR])        * sign,
            		m  : toInt(match[MINUTE])      * sign,
            		s  : toInt(match[SECOND])      * sign,
            		ms : toInt(match[MILLISECOND]) * sign
            	};
            } else if (!!(match = create__isoRegex.exec(input))) {
            	sign = (match[1] === '-') ? -1 : 1;
            	duration = {
            		y : parseIso(match[2], sign),
            		M : parseIso(match[3], sign),
            		d : parseIso(match[4], sign),
            		h : parseIso(match[5], sign),
            		m : parseIso(match[6], sign),
            		s : parseIso(match[7], sign),
            		w : parseIso(match[8], sign)
            	};
        } else if (duration == null) {// checks for null or undefined
        	duration = {};
        } else if (typeof duration === 'object' && ('from' in duration || 'to' in duration)) {
        	diffRes = momentsDifference(local__createLocal(duration.from), local__createLocal(duration.to));

        	duration = {};
        	duration.ms = diffRes.milliseconds;
        	duration.M = diffRes.months;
        }

        ret = new Duration(duration);

        if (isDuration(input) && hasOwnProp(input, '_locale')) {
        	ret._locale = input._locale;
        }

        return ret;
    }

    create__createDuration.fn = Duration.prototype;

    function parseIso (inp, sign) {
        // We'd normally use ~~inp for this, but unfortunately it also
        // converts floats to ints.
        // inp may be undefined, so careful calling replace on it.
        var res = inp && parseFloat(inp.replace(',', '.'));
        // apply sign while we're at it
        return (isNaN(res) ? 0 : res) * sign;
    }

    function positiveMomentsDifference(base, other) {
    	var res = {milliseconds: 0, months: 0};

    	res.months = other.month() - base.month() +
    	(other.year() - base.year()) * 12;
    	if (base.clone().add(res.months, 'M').isAfter(other)) {
    		--res.months;
    	}

    	res.milliseconds = +other - +(base.clone().add(res.months, 'M'));

    	return res;
    }

    function momentsDifference(base, other) {
    	var res;
    	other = cloneWithOffset(other, base);
    	if (base.isBefore(other)) {
    		res = positiveMomentsDifference(base, other);
    	} else {
    		res = positiveMomentsDifference(other, base);
    		res.milliseconds = -res.milliseconds;
    		res.months = -res.months;
    	}

    	return res;
    }

    function createAdder(direction, name) {
    	return function (val, period) {
    		var dur, tmp;
            //invert the arguments, but complain about it
            if (period !== null && !isNaN(+period)) {
            	deprecateSimple(name, 'moment().' + name  + '(period, number) is deprecated. Please use moment().' + name + '(number, period).');
            	tmp = val; val = period; period = tmp;
            }

            val = typeof val === 'string' ? +val : val;
            dur = create__createDuration(val, period);
            add_subtract__addSubtract(this, dur, direction);
            return this;
        };
    }

    function add_subtract__addSubtract (mom, duration, isAdding, updateOffset) {
    	var milliseconds = duration._milliseconds,
    	days = duration._days,
    	months = duration._months;
    	updateOffset = updateOffset == null ? true : updateOffset;

    	if (milliseconds) {
    		mom._d.setTime(+mom._d + milliseconds * isAdding);
    	}
    	if (days) {
    		get_set__set(mom, 'Date', get_set__get(mom, 'Date') + days * isAdding);
    	}
    	if (months) {
    		setMonth(mom, get_set__get(mom, 'Month') + months * isAdding);
    	}
    	if (updateOffset) {
    		utils_hooks__hooks.updateOffset(mom, days || months);
    	}
    }

    var add_subtract__add      = createAdder(1, 'add');
    var add_subtract__subtract = createAdder(-1, 'subtract');

    function moment_calendar__calendar (time, formats) {
        // We want to compare the start of today, vs this.
        // Getting start-of-today depends on whether we're local/utc/offset or not.
        var now = time || local__createLocal(),
        sod = cloneWithOffset(now, this).startOf('day'),
        diff = this.diff(sod, 'days', true),
        format = diff < -6 ? 'sameElse' :
        diff < -1 ? 'lastWeek' :
        diff < 0 ? 'lastDay' :
        diff < 1 ? 'sameDay' :
        diff < 2 ? 'nextDay' :
        diff < 7 ? 'nextWeek' : 'sameElse';
        return this.format(formats && formats[format] || this.localeData().calendar(format, this, local__createLocal(now)));
    }

    function clone () {
    	return new Moment(this);
    }

    function isAfter (input, units) {
    	var inputMs;
    	units = normalizeUnits(typeof units !== 'undefined' ? units : 'millisecond');
    	if (units === 'millisecond') {
    		input = isMoment(input) ? input : local__createLocal(input);
    		return +this > +input;
    	} else {
    		inputMs = isMoment(input) ? +input : +local__createLocal(input);
    		return inputMs < +this.clone().startOf(units);
    	}
    }

    function isBefore (input, units) {
    	var inputMs;
    	units = normalizeUnits(typeof units !== 'undefined' ? units : 'millisecond');
    	if (units === 'millisecond') {
    		input = isMoment(input) ? input : local__createLocal(input);
    		return +this < +input;
    	} else {
    		inputMs = isMoment(input) ? +input : +local__createLocal(input);
    		return +this.clone().endOf(units) < inputMs;
    	}
    }

    function isBetween (from, to, units) {
    	return this.isAfter(from, units) && this.isBefore(to, units);
    }

    function isSame (input, units) {
    	var inputMs;
    	units = normalizeUnits(units || 'millisecond');
    	if (units === 'millisecond') {
    		input = isMoment(input) ? input : local__createLocal(input);
    		return +this === +input;
    	} else {
    		inputMs = +local__createLocal(input);
    		return +(this.clone().startOf(units)) <= inputMs && inputMs <= +(this.clone().endOf(units));
    	}
    }

    function diff (input, units, asFloat) {
    	var that = cloneWithOffset(input, this),
    	zoneDelta = (that.utcOffset() - this.utcOffset()) * 6e4,
    	delta, output;

    	units = normalizeUnits(units);

    	if (units === 'year' || units === 'month' || units === 'quarter') {
    		output = monthDiff(this, that);
    		if (units === 'quarter') {
    			output = output / 3;
    		} else if (units === 'year') {
    			output = output / 12;
    		}
    	} else {
    		delta = this - that;
            output = units === 'second' ? delta / 1e3 : // 1000
                units === 'minute' ? delta / 6e4 : // 1000 * 60
                units === 'hour' ? delta / 36e5 : // 1000 * 60 * 60
                units === 'day' ? (delta - zoneDelta) / 864e5 : // 1000 * 60 * 60 * 24, negate dst
                units === 'week' ? (delta - zoneDelta) / 6048e5 : // 1000 * 60 * 60 * 24 * 7, negate dst
                delta;
            }
            return asFloat ? output : absFloor(output);
        }

        function monthDiff (a, b) {
        // difference in months
        var wholeMonthDiff = ((b.year() - a.year()) * 12) + (b.month() - a.month()),
            // b is in (anchor - 1 month, anchor + 1 month)
            anchor = a.clone().add(wholeMonthDiff, 'months'),
            anchor2, adjust;

            if (b - anchor < 0) {
            	anchor2 = a.clone().add(wholeMonthDiff - 1, 'months');
            // linear across the month
            adjust = (b - anchor) / (anchor - anchor2);
        } else {
        	anchor2 = a.clone().add(wholeMonthDiff + 1, 'months');
            // linear across the month
            adjust = (b - anchor) / (anchor2 - anchor);
        }

        return -(wholeMonthDiff + adjust);
    }

    utils_hooks__hooks.defaultFormat = 'YYYY-MM-DDTHH:mm:ssZ';

    function toString () {
    	return this.clone().locale('en').format('ddd MMM DD YYYY HH:mm:ss [GMT]ZZ');
    }

    function moment_format__toISOString () {
    	var m = this.clone().utc();
    	if (0 < m.year() && m.year() <= 9999) {
    		if ('function' === typeof Date.prototype.toISOString) {
                // native implementation is ~50x faster, use it when we can
                return this.toDate().toISOString();
            } else {
            	return formatMoment(m, 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
            }
        } else {
        	return formatMoment(m, 'YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
        }
    }

    function format (inputString) {
    	var output = formatMoment(this, inputString || utils_hooks__hooks.defaultFormat);
    	return this.localeData().postformat(output);
    }

    function from (time, withoutSuffix) {
    	if (!this.isValid()) {
    		return this.localeData().invalidDate();
    	}
    	return create__createDuration({to: this, from: time}).locale(this.locale()).humanize(!withoutSuffix);
    }

    function fromNow (withoutSuffix) {
    	return this.from(local__createLocal(), withoutSuffix);
    }

    function to (time, withoutSuffix) {
    	if (!this.isValid()) {
    		return this.localeData().invalidDate();
    	}
    	return create__createDuration({from: this, to: time}).locale(this.locale()).humanize(!withoutSuffix);
    }

    function toNow (withoutSuffix) {
    	return this.to(local__createLocal(), withoutSuffix);
    }

    function locale (key) {
    	var newLocaleData;

    	if (key === undefined) {
    		return this._locale._abbr;
    	} else {
    		newLocaleData = locale_locales__getLocale(key);
    		if (newLocaleData != null) {
    			this._locale = newLocaleData;
    		}
    		return this;
    	}
    }

    var lang = deprecate(
    	'moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.',
    	function (key) {
    		if (key === undefined) {
    			return this.localeData();
    		} else {
    			return this.locale(key);
    		}
    	}
    	);

    function localeData () {
    	return this._locale;
    }

    function startOf (units) {
    	units = normalizeUnits(units);
        // the following switch intentionally omits break keywords
        // to utilize falling through the cases.
        switch (units) {
        	case 'year':
        	this.month(0);
        	/* falls through */
        	case 'quarter':
        	case 'month':
        	this.date(1);
        	/* falls through */
        	case 'week':
        	case 'isoWeek':
        	case 'day':
        	this.hours(0);
        	/* falls through */
        	case 'hour':
        	this.minutes(0);
        	/* falls through */
        	case 'minute':
        	this.seconds(0);
        	/* falls through */
        	case 'second':
        	this.milliseconds(0);
        }

        // weeks are a special case
        if (units === 'week') {
        	this.weekday(0);
        }
        if (units === 'isoWeek') {
        	this.isoWeekday(1);
        }

        // quarters are also special
        if (units === 'quarter') {
        	this.month(Math.floor(this.month() / 3) * 3);
        }

        return this;
    }

    function endOf (units) {
    	units = normalizeUnits(units);
    	if (units === undefined || units === 'millisecond') {
    		return this;
    	}
    	return this.startOf(units).add(1, (units === 'isoWeek' ? 'week' : units)).subtract(1, 'ms');
    }

    function to_type__valueOf () {
    	return +this._d - ((this._offset || 0) * 60000);
    }

    function unix () {
    	return Math.floor(+this / 1000);
    }

    function toDate () {
    	return this._offset ? new Date(+this) : this._d;
    }

    function toArray () {
    	var m = this;
    	return [m.year(), m.month(), m.date(), m.hour(), m.minute(), m.second(), m.millisecond()];
    }

    function toObject () {
    	var m = this;
    	return {
    		years: m.year(),
    		months: m.month(),
    		date: m.date(),
    		hours: m.hours(),
    		minutes: m.minutes(),
    		seconds: m.seconds(),
    		milliseconds: m.milliseconds()
    	};
    }

    function moment_valid__isValid () {
    	return valid__isValid(this);
    }

    function parsingFlags () {
    	return extend({}, getParsingFlags(this));
    }

    function invalidAt () {
    	return getParsingFlags(this).overflow;
    }

    addFormatToken(0, ['gg', 2], 0, function () {
    	return this.weekYear() % 100;
    });

    addFormatToken(0, ['GG', 2], 0, function () {
    	return this.isoWeekYear() % 100;
    });

    function addWeekYearFormatToken (token, getter) {
    	addFormatToken(0, [token, token.length], 0, getter);
    }

    addWeekYearFormatToken('gggg',     'weekYear');
    addWeekYearFormatToken('ggggg',    'weekYear');
    addWeekYearFormatToken('GGGG',  'isoWeekYear');
    addWeekYearFormatToken('GGGGG', 'isoWeekYear');

    // ALIASES

    addUnitAlias('weekYear', 'gg');
    addUnitAlias('isoWeekYear', 'GG');

    // PARSING

    addRegexToken('G',      matchSigned);
    addRegexToken('g',      matchSigned);
    addRegexToken('GG',     match1to2, match2);
    addRegexToken('gg',     match1to2, match2);
    addRegexToken('GGGG',   match1to4, match4);
    addRegexToken('gggg',   match1to4, match4);
    addRegexToken('GGGGG',  match1to6, match6);
    addRegexToken('ggggg',  match1to6, match6);

    addWeekParseToken(['gggg', 'ggggg', 'GGGG', 'GGGGG'], function (input, week, config, token) {
    	week[token.substr(0, 2)] = toInt(input);
    });

    addWeekParseToken(['gg', 'GG'], function (input, week, config, token) {
    	week[token] = utils_hooks__hooks.parseTwoDigitYear(input);
    });

    // HELPERS

    function weeksInYear(year, dow, doy) {
    	return weekOfYear(local__createLocal([year, 11, 31 + dow - doy]), dow, doy).week;
    }

    // MOMENTS

    function getSetWeekYear (input) {
    	var year = weekOfYear(this, this.localeData()._week.dow, this.localeData()._week.doy).year;
    	return input == null ? year : this.add((input - year), 'y');
    }

    function getSetISOWeekYear (input) {
    	var year = weekOfYear(this, 1, 4).year;
    	return input == null ? year : this.add((input - year), 'y');
    }

    function getISOWeeksInYear () {
    	return weeksInYear(this.year(), 1, 4);
    }

    function getWeeksInYear () {
    	var weekInfo = this.localeData()._week;
    	return weeksInYear(this.year(), weekInfo.dow, weekInfo.doy);
    }

    addFormatToken('Q', 0, 0, 'quarter');

    // ALIASES

    addUnitAlias('quarter', 'Q');

    // PARSING

    addRegexToken('Q', match1);
    addParseToken('Q', function (input, array) {
    	array[MONTH] = (toInt(input) - 1) * 3;
    });

    // MOMENTS

    function getSetQuarter (input) {
    	return input == null ? Math.ceil((this.month() + 1) / 3) : this.month((input - 1) * 3 + this.month() % 3);
    }

    addFormatToken('D', ['DD', 2], 'Do', 'date');

    // ALIASES

    addUnitAlias('date', 'D');

    // PARSING

    addRegexToken('D',  match1to2);
    addRegexToken('DD', match1to2, match2);
    addRegexToken('Do', function (isStrict, locale) {
    	return isStrict ? locale._ordinalParse : locale._ordinalParseLenient;
    });

    addParseToken(['D', 'DD'], DATE);
    addParseToken('Do', function (input, array) {
    	array[DATE] = toInt(input.match(match1to2)[0], 10);
    });

    // MOMENTS

    var getSetDayOfMonth = makeGetSet('Date', true);

    addFormatToken('d', 0, 'do', 'day');

    addFormatToken('dd', 0, 0, function (format) {
    	return this.localeData().weekdaysMin(this, format);
    });

    addFormatToken('ddd', 0, 0, function (format) {
    	return this.localeData().weekdaysShort(this, format);
    });

    addFormatToken('dddd', 0, 0, function (format) {
    	return this.localeData().weekdays(this, format);
    });

    addFormatToken('e', 0, 0, 'weekday');
    addFormatToken('E', 0, 0, 'isoWeekday');

    // ALIASES

    addUnitAlias('day', 'd');
    addUnitAlias('weekday', 'e');
    addUnitAlias('isoWeekday', 'E');

    // PARSING

    addRegexToken('d',    match1to2);
    addRegexToken('e',    match1to2);
    addRegexToken('E',    match1to2);
    addRegexToken('dd',   matchWord);
    addRegexToken('ddd',  matchWord);
    addRegexToken('dddd', matchWord);

    addWeekParseToken(['dd', 'ddd', 'dddd'], function (input, week, config) {
    	var weekday = config._locale.weekdaysParse(input);
        // if we didn't get a weekday name, mark the date as invalid
        if (weekday != null) {
        	week.d = weekday;
        } else {
        	getParsingFlags(config).invalidWeekday = input;
        }
    });

    addWeekParseToken(['d', 'e', 'E'], function (input, week, config, token) {
    	week[token] = toInt(input);
    });

    // HELPERS

    function parseWeekday(input, locale) {
    	if (typeof input !== 'string') {
    		return input;
    	}

    	if (!isNaN(input)) {
    		return parseInt(input, 10);
    	}

    	input = locale.weekdaysParse(input);
    	if (typeof input === 'number') {
    		return input;
    	}

    	return null;
    }

    // LOCALES

    var defaultLocaleWeekdays = 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_');
    function localeWeekdays (m) {
    	return this._weekdays[m.day()];
    }

    var defaultLocaleWeekdaysShort = 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_');
    function localeWeekdaysShort (m) {
    	return this._weekdaysShort[m.day()];
    }

    var defaultLocaleWeekdaysMin = 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_');
    function localeWeekdaysMin (m) {
    	return this._weekdaysMin[m.day()];
    }

    function localeWeekdaysParse (weekdayName) {
    	var i, mom, regex;

    	this._weekdaysParse = this._weekdaysParse || [];

    	for (i = 0; i < 7; i++) {
            // make the regex if we don't have it already
            if (!this._weekdaysParse[i]) {
            	mom = local__createLocal([2000, 1]).day(i);
            	regex = '^' + this.weekdays(mom, '') + '|^' + this.weekdaysShort(mom, '') + '|^' + this.weekdaysMin(mom, '');
            	this._weekdaysParse[i] = new RegExp(regex.replace('.', ''), 'i');
            }
            // test the regex
            if (this._weekdaysParse[i].test(weekdayName)) {
            	return i;
            }
        }
    }

    // MOMENTS

    function getSetDayOfWeek (input) {
    	var day = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
    	if (input != null) {
    		input = parseWeekday(input, this.localeData());
    		return this.add(input - day, 'd');
    	} else {
    		return day;
    	}
    }

    function getSetLocaleDayOfWeek (input) {
    	var weekday = (this.day() + 7 - this.localeData()._week.dow) % 7;
    	return input == null ? weekday : this.add(input - weekday, 'd');
    }

    function getSetISODayOfWeek (input) {
        // behaves the same as moment#day except
        // as a getter, returns 7 instead of 0 (1-7 range instead of 0-6)
        // as a setter, sunday should belong to the previous week.
        return input == null ? this.day() || 7 : this.day(this.day() % 7 ? input : input - 7);
    }

    addFormatToken('H', ['HH', 2], 0, 'hour');
    addFormatToken('h', ['hh', 2], 0, function () {
    	return this.hours() % 12 || 12;
    });

    function meridiem (token, lowercase) {
    	addFormatToken(token, 0, 0, function () {
    		return this.localeData().meridiem(this.hours(), this.minutes(), lowercase);
    	});
    }

    meridiem('a', true);
    meridiem('A', false);

    // ALIASES

    addUnitAlias('hour', 'h');

    // PARSING

    function matchMeridiem (isStrict, locale) {
    	return locale._meridiemParse;
    }

    addRegexToken('a',  matchMeridiem);
    addRegexToken('A',  matchMeridiem);
    addRegexToken('H',  match1to2);
    addRegexToken('h',  match1to2);
    addRegexToken('HH', match1to2, match2);
    addRegexToken('hh', match1to2, match2);

    addParseToken(['H', 'HH'], HOUR);
    addParseToken(['a', 'A'], function (input, array, config) {
    	config._isPm = config._locale.isPM(input);
    	config._meridiem = input;
    });
    addParseToken(['h', 'hh'], function (input, array, config) {
    	array[HOUR] = toInt(input);
    	getParsingFlags(config).bigHour = true;
    });

    // LOCALES

    function localeIsPM (input) {
        // IE8 Quirks Mode & IE7 Standards Mode do not allow accessing strings like arrays
        // Using charAt should be more compatible.
        return ((input + '').toLowerCase().charAt(0) === 'p');
    }

    var defaultLocaleMeridiemParse = /[ap]\.?m?\.?/i;
    function localeMeridiem (hours, minutes, isLower) {
    	if (hours > 11) {
    		return isLower ? 'pm' : 'PM';
    	} else {
    		return isLower ? 'am' : 'AM';
    	}
    }


    // MOMENTS

    // Setting the hour should keep the time, because the user explicitly
    // specified which hour he wants. So trying to maintain the same hour (in
    // a new timezone) makes sense. Adding/subtracting hours does not follow
    // this rule.
    var getSetHour = makeGetSet('Hours', true);

    addFormatToken('m', ['mm', 2], 0, 'minute');

    // ALIASES

    addUnitAlias('minute', 'm');

    // PARSING

    addRegexToken('m',  match1to2);
    addRegexToken('mm', match1to2, match2);
    addParseToken(['m', 'mm'], MINUTE);

    // MOMENTS

    var getSetMinute = makeGetSet('Minutes', false);

    addFormatToken('s', ['ss', 2], 0, 'second');

    // ALIASES

    addUnitAlias('second', 's');

    // PARSING

    addRegexToken('s',  match1to2);
    addRegexToken('ss', match1to2, match2);
    addParseToken(['s', 'ss'], SECOND);

    // MOMENTS

    var getSetSecond = makeGetSet('Seconds', false);

    addFormatToken('S', 0, 0, function () {
    	return ~~(this.millisecond() / 100);
    });

    addFormatToken(0, ['SS', 2], 0, function () {
    	return ~~(this.millisecond() / 10);
    });

    addFormatToken(0, ['SSS', 3], 0, 'millisecond');
    addFormatToken(0, ['SSSS', 4], 0, function () {
    	return this.millisecond() * 10;
    });
    addFormatToken(0, ['SSSSS', 5], 0, function () {
    	return this.millisecond() * 100;
    });
    addFormatToken(0, ['SSSSSS', 6], 0, function () {
    	return this.millisecond() * 1000;
    });
    addFormatToken(0, ['SSSSSSS', 7], 0, function () {
    	return this.millisecond() * 10000;
    });
    addFormatToken(0, ['SSSSSSSS', 8], 0, function () {
    	return this.millisecond() * 100000;
    });
    addFormatToken(0, ['SSSSSSSSS', 9], 0, function () {
    	return this.millisecond() * 1000000;
    });


    // ALIASES

    addUnitAlias('millisecond', 'ms');

    // PARSING

    addRegexToken('S',    match1to3, match1);
    addRegexToken('SS',   match1to3, match2);
    addRegexToken('SSS',  match1to3, match3);

    var token;
    for (token = 'SSSS'; token.length <= 9; token += 'S') {
    	addRegexToken(token, matchUnsigned);
    }

    function parseMs(input, array) {
    	array[MILLISECOND] = toInt(('0.' + input) * 1000);
    }

    for (token = 'S'; token.length <= 9; token += 'S') {
    	addParseToken(token, parseMs);
    }
    // MOMENTS

    var getSetMillisecond = makeGetSet('Milliseconds', false);

    addFormatToken('z',  0, 0, 'zoneAbbr');
    addFormatToken('zz', 0, 0, 'zoneName');

    // MOMENTS

    function getZoneAbbr () {
    	return this._isUTC ? 'UTC' : '';
    }

    function getZoneName () {
    	return this._isUTC ? 'Coordinated Universal Time' : '';
    }

    var momentPrototype__proto = Moment.prototype;

    momentPrototype__proto.add          = add_subtract__add;
    momentPrototype__proto.calendar     = moment_calendar__calendar;
    momentPrototype__proto.clone        = clone;
    momentPrototype__proto.diff         = diff;
    momentPrototype__proto.endOf        = endOf;
    momentPrototype__proto.format       = format;
    momentPrototype__proto.from         = from;
    momentPrototype__proto.fromNow      = fromNow;
    momentPrototype__proto.to           = to;
    momentPrototype__proto.toNow        = toNow;
    momentPrototype__proto.get          = getSet;
    momentPrototype__proto.invalidAt    = invalidAt;
    momentPrototype__proto.isAfter      = isAfter;
    momentPrototype__proto.isBefore     = isBefore;
    momentPrototype__proto.isBetween    = isBetween;
    momentPrototype__proto.isSame       = isSame;
    momentPrototype__proto.isValid      = moment_valid__isValid;
    momentPrototype__proto.lang         = lang;
    momentPrototype__proto.locale       = locale;
    momentPrototype__proto.localeData   = localeData;
    momentPrototype__proto.max          = prototypeMax;
    momentPrototype__proto.min          = prototypeMin;
    momentPrototype__proto.parsingFlags = parsingFlags;
    momentPrototype__proto.set          = getSet;
    momentPrototype__proto.startOf      = startOf;
    momentPrototype__proto.subtract     = add_subtract__subtract;
    momentPrototype__proto.toArray      = toArray;
    momentPrototype__proto.toObject     = toObject;
    momentPrototype__proto.toDate       = toDate;
    momentPrototype__proto.toISOString  = moment_format__toISOString;
    momentPrototype__proto.toJSON       = moment_format__toISOString;
    momentPrototype__proto.toString     = toString;
    momentPrototype__proto.unix         = unix;
    momentPrototype__proto.valueOf      = to_type__valueOf;

    // Year
    momentPrototype__proto.year       = getSetYear;
    momentPrototype__proto.isLeapYear = getIsLeapYear;

    // Week Year
    momentPrototype__proto.weekYear    = getSetWeekYear;
    momentPrototype__proto.isoWeekYear = getSetISOWeekYear;

    // Quarter
    momentPrototype__proto.quarter = momentPrototype__proto.quarters = getSetQuarter;

    // Month
    momentPrototype__proto.month       = getSetMonth;
    momentPrototype__proto.daysInMonth = getDaysInMonth;

    // Week
    momentPrototype__proto.week           = momentPrototype__proto.weeks        = getSetWeek;
    momentPrototype__proto.isoWeek        = momentPrototype__proto.isoWeeks     = getSetISOWeek;
    momentPrototype__proto.weeksInYear    = getWeeksInYear;
    momentPrototype__proto.isoWeeksInYear = getISOWeeksInYear;

    // Day
    momentPrototype__proto.date       = getSetDayOfMonth;
    momentPrototype__proto.day        = momentPrototype__proto.days             = getSetDayOfWeek;
    momentPrototype__proto.weekday    = getSetLocaleDayOfWeek;
    momentPrototype__proto.isoWeekday = getSetISODayOfWeek;
    momentPrototype__proto.dayOfYear  = getSetDayOfYear;

    // Hour
    momentPrototype__proto.hour = momentPrototype__proto.hours = getSetHour;

    // Minute
    momentPrototype__proto.minute = momentPrototype__proto.minutes = getSetMinute;

    // Second
    momentPrototype__proto.second = momentPrototype__proto.seconds = getSetSecond;

    // Millisecond
    momentPrototype__proto.millisecond = momentPrototype__proto.milliseconds = getSetMillisecond;

    // Offset
    momentPrototype__proto.utcOffset            = getSetOffset;
    momentPrototype__proto.utc                  = setOffsetToUTC;
    momentPrototype__proto.local                = setOffsetToLocal;
    momentPrototype__proto.parseZone            = setOffsetToParsedOffset;
    momentPrototype__proto.hasAlignedHourOffset = hasAlignedHourOffset;
    momentPrototype__proto.isDST                = isDaylightSavingTime;
    momentPrototype__proto.isDSTShifted         = isDaylightSavingTimeShifted;
    momentPrototype__proto.isLocal              = isLocal;
    momentPrototype__proto.isUtcOffset          = isUtcOffset;
    momentPrototype__proto.isUtc                = isUtc;
    momentPrototype__proto.isUTC                = isUtc;

    // Timezone
    momentPrototype__proto.zoneAbbr = getZoneAbbr;
    momentPrototype__proto.zoneName = getZoneName;

    // Deprecations
    momentPrototype__proto.dates  = deprecate('dates accessor is deprecated. Use date instead.', getSetDayOfMonth);
    momentPrototype__proto.months = deprecate('months accessor is deprecated. Use month instead', getSetMonth);
    momentPrototype__proto.years  = deprecate('years accessor is deprecated. Use year instead', getSetYear);
    momentPrototype__proto.zone   = deprecate('moment().zone is deprecated, use moment().utcOffset instead. https://github.com/moment/moment/issues/1779', getSetZone);

    var momentPrototype = momentPrototype__proto;

    function moment__createUnix (input) {
    	return local__createLocal(input * 1000);
    }

    function moment__createInZone () {
    	return local__createLocal.apply(null, arguments).parseZone();
    }

    var defaultCalendar = {
    	sameDay : '[Today at] LT',
    	nextDay : '[Tomorrow at] LT',
    	nextWeek : 'dddd [at] LT',
    	lastDay : '[Yesterday at] LT',
    	lastWeek : '[Last] dddd [at] LT',
    	sameElse : 'L'
    };

    function locale_calendar__calendar (key, mom, now) {
    	var output = this._calendar[key];
    	return typeof output === 'function' ? output.call(mom, now) : output;
    }

    var defaultLongDateFormat = {
    	LTS  : 'h:mm:ss A',
    	LT   : 'h:mm A',
    	L    : 'DD/MM/YYYY',
    	LL   : 'D MMMM, YYYY',
    	LLL  : 'D MMMM, YYYY h:mm A',
    	LLLL : 'dddd, MMMM D, YYYY h:mm A'
    };

    function longDateFormat (key) {
    	var format = this._longDateFormat[key],
    	formatUpper = this._longDateFormat[key.toUpperCase()];

    	if (format || !formatUpper) {
    		return format;
    	}

    	this._longDateFormat[key] = formatUpper.replace(/MMMM|MM|DD|dddd/g, function (val) {
    		return val.slice(1);
    	});

    	return this._longDateFormat[key];
    }

    var defaultInvalidDate = 'Invalid date';

    function invalidDate () {
    	return this._invalidDate;
    }

    var defaultOrdinal = '%d';
    var defaultOrdinalParse = /\d{1,2}/;

    function ordinal (number) {
    	return this._ordinal.replace('%d', number);
    }

    function preParsePostFormat (string) {
    	return string;
    }

    var defaultRelativeTime = {
    	future : 'in %s',
    	past   : '%s ago',
    	s  : 'a few seconds',
    	m  : 'a minute',
    	mm : '%d minutes',
    	h  : 'an hour',
    	hh : '%d hours',
    	d  : 'a day',
    	dd : '%d days',
    	M  : 'a month',
    	MM : '%d months',
    	y  : 'a year',
    	yy : '%d years'
    };

    function relative__relativeTime (number, withoutSuffix, string, isFuture) {
    	var output = this._relativeTime[string];
    	return (typeof output === 'function') ?
    	output(number, withoutSuffix, string, isFuture) :
    	output.replace(/%d/i, number);
    }

    function pastFuture (diff, output) {
    	var format = this._relativeTime[diff > 0 ? 'future' : 'past'];
    	return typeof format === 'function' ? format(output) : format.replace(/%s/i, output);
    }

    function locale_set__set (config) {
    	var prop, i;
    	for (i in config) {
    		prop = config[i];
    		if (typeof prop === 'function') {
    			this[i] = prop;
    		} else {
    			this['_' + i] = prop;
    		}
    	}
        // Lenient ordinal parsing accepts just a number in addition to
        // number + (possibly) stuff coming from _ordinalParseLenient.
        this._ordinalParseLenient = new RegExp(this._ordinalParse.source + '|' + (/\d{1,2}/).source);
    }

    var prototype__proto = Locale.prototype;

    prototype__proto._calendar       = defaultCalendar;
    prototype__proto.calendar        = locale_calendar__calendar;
    prototype__proto._longDateFormat = defaultLongDateFormat;
    prototype__proto.longDateFormat  = longDateFormat;
    prototype__proto._invalidDate    = defaultInvalidDate;
    prototype__proto.invalidDate     = invalidDate;
    prototype__proto._ordinal        = defaultOrdinal;
    prototype__proto.ordinal         = ordinal;
    prototype__proto._ordinalParse   = defaultOrdinalParse;
    prototype__proto.preparse        = preParsePostFormat;
    prototype__proto.postformat      = preParsePostFormat;
    prototype__proto._relativeTime   = defaultRelativeTime;
    prototype__proto.relativeTime    = relative__relativeTime;
    prototype__proto.pastFuture      = pastFuture;
    prototype__proto.set             = locale_set__set;

    // Month
    prototype__proto.months       =        localeMonths;
    prototype__proto._months      = defaultLocaleMonths;
    prototype__proto.monthsShort  =        localeMonthsShort;
    prototype__proto._monthsShort = defaultLocaleMonthsShort;
    prototype__proto.monthsParse  =        localeMonthsParse;

    // Week
    prototype__proto.week = localeWeek;
    prototype__proto._week = defaultLocaleWeek;
    prototype__proto.firstDayOfYear = localeFirstDayOfYear;
    prototype__proto.firstDayOfWeek = localeFirstDayOfWeek;

    // Day of Week
    prototype__proto.weekdays       =        localeWeekdays;
    prototype__proto._weekdays      = defaultLocaleWeekdays;
    prototype__proto.weekdaysMin    =        localeWeekdaysMin;
    prototype__proto._weekdaysMin   = defaultLocaleWeekdaysMin;
    prototype__proto.weekdaysShort  =        localeWeekdaysShort;
    prototype__proto._weekdaysShort = defaultLocaleWeekdaysShort;
    prototype__proto.weekdaysParse  =        localeWeekdaysParse;

    // Hours
    prototype__proto.isPM = localeIsPM;
    prototype__proto._meridiemParse = defaultLocaleMeridiemParse;
    prototype__proto.meridiem = localeMeridiem;

    function lists__get (format, index, field, setter) {
    	var locale = locale_locales__getLocale();
    	var utc = create_utc__createUTC().set(setter, index);
    	return locale[field](utc, format);
    }

    function list (format, index, field, count, setter) {
    	if (typeof format === 'number') {
    		index = format;
    		format = undefined;
    	}

    	format = format || '';

    	if (index != null) {
    		return lists__get(format, index, field, setter);
    	}

    	var i;
    	var out = [];
    	for (i = 0; i < count; i++) {
    		out[i] = lists__get(format, i, field, setter);
    	}
    	return out;
    }

    function lists__listMonths (format, index) {
    	return list(format, index, 'months', 12, 'month');
    }

    function lists__listMonthsShort (format, index) {
    	return list(format, index, 'monthsShort', 12, 'month');
    }

    function lists__listWeekdays (format, index) {
    	return list(format, index, 'weekdays', 7, 'day');
    }

    function lists__listWeekdaysShort (format, index) {
    	return list(format, index, 'weekdaysShort', 7, 'day');
    }

    function lists__listWeekdaysMin (format, index) {
    	return list(format, index, 'weekdaysMin', 7, 'day');
    }

    locale_locales__getSetGlobalLocale('en', {
    	ordinalParse: /\d{1,2}(th|st|nd|rd)/,
    	ordinal : function (number) {
    		var b = number % 10,
    		output = (toInt(number % 100 / 10) === 1) ? 'th' :
    		(b === 1) ? 'st' :
    		(b === 2) ? 'nd' :
    		(b === 3) ? 'rd' : 'th';
    		return number + output;
    	}
    });

    // Side effect imports
    utils_hooks__hooks.lang = deprecate('moment.lang is deprecated. Use moment.locale instead.', locale_locales__getSetGlobalLocale);
    utils_hooks__hooks.langData = deprecate('moment.langData is deprecated. Use moment.localeData instead.', locale_locales__getLocale);

    var mathAbs = Math.abs;

    function duration_abs__abs () {
    	var data           = this._data;

    	this._milliseconds = mathAbs(this._milliseconds);
    	this._days         = mathAbs(this._days);
    	this._months       = mathAbs(this._months);

    	data.milliseconds  = mathAbs(data.milliseconds);
    	data.seconds       = mathAbs(data.seconds);
    	data.minutes       = mathAbs(data.minutes);
    	data.hours         = mathAbs(data.hours);
    	data.months        = mathAbs(data.months);
    	data.years         = mathAbs(data.years);

    	return this;
    }

    function duration_add_subtract__addSubtract (duration, input, value, direction) {
    	var other = create__createDuration(input, value);

    	duration._milliseconds += direction * other._milliseconds;
    	duration._days         += direction * other._days;
    	duration._months       += direction * other._months;

    	return duration._bubble();
    }

    // supports only 2.0-style add(1, 's') or add(duration)
    function duration_add_subtract__add (input, value) {
    	return duration_add_subtract__addSubtract(this, input, value, 1);
    }

    // supports only 2.0-style subtract(1, 's') or subtract(duration)
    function duration_add_subtract__subtract (input, value) {
    	return duration_add_subtract__addSubtract(this, input, value, -1);
    }

    function absCeil (number) {
    	if (number < 0) {
    		return Math.floor(number);
    	} else {
    		return Math.ceil(number);
    	}
    }

    function bubble () {
    	var milliseconds = this._milliseconds;
    	var days         = this._days;
    	var months       = this._months;
    	var data         = this._data;
    	var seconds, minutes, hours, years, monthsFromDays;

        // if we have a mix of positive and negative values, bubble down first
        // check: https://github.com/moment/moment/issues/2166
        if (!((milliseconds >= 0 && days >= 0 && months >= 0) ||
        	(milliseconds <= 0 && days <= 0 && months <= 0))) {
        	milliseconds += absCeil(monthsToDays(months) + days) * 864e5;
        days = 0;
        months = 0;
    }

        // The following code bubbles up values, see the tests for
        // examples of what that means.
        data.milliseconds = milliseconds % 1000;

        seconds           = absFloor(milliseconds / 1000);
        data.seconds      = seconds % 60;

        minutes           = absFloor(seconds / 60);
        data.minutes      = minutes % 60;

        hours             = absFloor(minutes / 60);
        data.hours        = hours % 24;

        days += absFloor(hours / 24);

        // convert days to months
        monthsFromDays = absFloor(daysToMonths(days));
        months += monthsFromDays;
        days -= absCeil(monthsToDays(monthsFromDays));

        // 12 months -> 1 year
        years = absFloor(months / 12);
        months %= 12;

        data.days   = days;
        data.months = months;
        data.years  = years;

        return this;
    }

    function daysToMonths (days) {
        // 400 years have 146097 days (taking into account leap year rules)
        // 400 years have 12 months === 4800
        return days * 4800 / 146097;
    }

    function monthsToDays (months) {
        // the reverse of daysToMonths
        return months * 146097 / 4800;
    }

    function as (units) {
    	var days;
    	var months;
    	var milliseconds = this._milliseconds;

    	units = normalizeUnits(units);

    	if (units === 'month' || units === 'year') {
    		days   = this._days   + milliseconds / 864e5;
    		months = this._months + daysToMonths(days);
    		return units === 'month' ? months : months / 12;
    	} else {
            // handle milliseconds separately because of floating point math errors (issue #1867)
            days = this._days + Math.round(monthsToDays(this._months));
            switch (units) {
            	case 'week'   : return days / 7     + milliseconds / 6048e5;
            	case 'day'    : return days         + milliseconds / 864e5;
            	case 'hour'   : return days * 24    + milliseconds / 36e5;
            	case 'minute' : return days * 1440  + milliseconds / 6e4;
            	case 'second' : return days * 86400 + milliseconds / 1000;
                // Math.floor prevents floating point math errors here
                case 'millisecond': return Math.floor(days * 864e5) + milliseconds;
                default: throw new Error('Unknown unit ' + units);
            }
        }
    }

    // TODO: Use this.as('ms')?
    function duration_as__valueOf () {
    	return (
    		this._milliseconds +
    		this._days * 864e5 +
    		(this._months % 12) * 2592e6 +
    		toInt(this._months / 12) * 31536e6
    		);
    }

    function makeAs (alias) {
    	return function () {
    		return this.as(alias);
    	};
    }

    var asMilliseconds = makeAs('ms');
    var asSeconds      = makeAs('s');
    var asMinutes      = makeAs('m');
    var asHours        = makeAs('h');
    var asDays         = makeAs('d');
    var asWeeks        = makeAs('w');
    var asMonths       = makeAs('M');
    var asYears        = makeAs('y');

    function duration_get__get (units) {
    	units = normalizeUnits(units);
    	return this[units + 's']();
    }

    function makeGetter(name) {
    	return function () {
    		return this._data[name];
    	};
    }

    var milliseconds = makeGetter('milliseconds');
    var seconds      = makeGetter('seconds');
    var minutes      = makeGetter('minutes');
    var hours        = makeGetter('hours');
    var days         = makeGetter('days');
    var months       = makeGetter('months');
    var years        = makeGetter('years');

    function weeks () {
    	return absFloor(this.days() / 7);
    }

    var round = Math.round;
    var thresholds = {
        s: 45,  // seconds to minute
        m: 45,  // minutes to hour
        h: 22,  // hours to day
        d: 26,  // days to month
        M: 11   // months to year
    };

    // helper function for moment.fn.from, moment.fn.fromNow, and moment.duration.fn.humanize
    function substituteTimeAgo(string, number, withoutSuffix, isFuture, locale) {
    	return locale.relativeTime(number || 1, !!withoutSuffix, string, isFuture);
    }

    function duration_humanize__relativeTime (posNegDuration, withoutSuffix, locale) {
    	var duration = create__createDuration(posNegDuration).abs();
    	var seconds  = round(duration.as('s'));
    	var minutes  = round(duration.as('m'));
    	var hours    = round(duration.as('h'));
    	var days     = round(duration.as('d'));
    	var months   = round(duration.as('M'));
    	var years    = round(duration.as('y'));

    	var a = seconds < thresholds.s && ['s', seconds]  ||
    	minutes === 1          && ['m']           ||
    	minutes < thresholds.m && ['mm', minutes] ||
    	hours   === 1          && ['h']           ||
    	hours   < thresholds.h && ['hh', hours]   ||
    	days    === 1          && ['d']           ||
    	days    < thresholds.d && ['dd', days]    ||
    	months  === 1          && ['M']           ||
    	months  < thresholds.M && ['MM', months]  ||
    	years   === 1          && ['y']           || ['yy', years];

    	a[2] = withoutSuffix;
    	a[3] = +posNegDuration > 0;
    	a[4] = locale;
    	return substituteTimeAgo.apply(null, a);
    }

    // This function allows you to set a threshold for relative time strings
    function duration_humanize__getSetRelativeTimeThreshold (threshold, limit) {
    	if (thresholds[threshold] === undefined) {
    		return false;
    	}
    	if (limit === undefined) {
    		return thresholds[threshold];
    	}
    	thresholds[threshold] = limit;
    	return true;
    }

    function humanize (withSuffix) {
    	var locale = this.localeData();
    	var output = duration_humanize__relativeTime(this, !withSuffix, locale);

    	if (withSuffix) {
    		output = locale.pastFuture(+this, output);
    	}

    	return locale.postformat(output);
    }

    var iso_string__abs = Math.abs;

    function iso_string__toISOString() {
        // for ISO strings we do not use the normal bubbling rules:
        //  * milliseconds bubble up until they become hours
        //  * days do not bubble at all
        //  * months bubble up until they become years
        // This is because there is no context-free conversion between hours and days
        // (think of clock changes)
        // and also not between days and months (28-31 days per month)
        var seconds = iso_string__abs(this._milliseconds) / 1000;
        var days         = iso_string__abs(this._days);
        var months       = iso_string__abs(this._months);
        var minutes, hours, years;

        // 3600 seconds -> 60 minutes -> 1 hour
        minutes           = absFloor(seconds / 60);
        hours             = absFloor(minutes / 60);
        seconds %= 60;
        minutes %= 60;

        // 12 months -> 1 year
        years  = absFloor(months / 12);
        months %= 12;


        // inspired by https://github.com/dordille/moment-isoduration/blob/master/moment.isoduration.js
        var Y = years;
        var M = months;
        var D = days;
        var h = hours;
        var m = minutes;
        var s = seconds;
        var total = this.asSeconds();

        if (!total) {
            // this is the same as C#'s (Noda) and python (isodate)...
            // but not other JS (goog.date)
            return 'P0D';
        }

        return (total < 0 ? '-' : '') +
        'P' +
        (Y ? Y + 'Y' : '') +
        (M ? M + 'M' : '') +
        (D ? D + 'D' : '') +
        ((h || m || s) ? 'T' : '') +
        (h ? h + 'H' : '') +
        (m ? m + 'M' : '') +
        (s ? s + 'S' : '');
    }

    var duration_prototype__proto = Duration.prototype;

    duration_prototype__proto.abs            = duration_abs__abs;
    duration_prototype__proto.add            = duration_add_subtract__add;
    duration_prototype__proto.subtract       = duration_add_subtract__subtract;
    duration_prototype__proto.as             = as;
    duration_prototype__proto.asMilliseconds = asMilliseconds;
    duration_prototype__proto.asSeconds      = asSeconds;
    duration_prototype__proto.asMinutes      = asMinutes;
    duration_prototype__proto.asHours        = asHours;
    duration_prototype__proto.asDays         = asDays;
    duration_prototype__proto.asWeeks        = asWeeks;
    duration_prototype__proto.asMonths       = asMonths;
    duration_prototype__proto.asYears        = asYears;
    duration_prototype__proto.valueOf        = duration_as__valueOf;
    duration_prototype__proto._bubble        = bubble;
    duration_prototype__proto.get            = duration_get__get;
    duration_prototype__proto.milliseconds   = milliseconds;
    duration_prototype__proto.seconds        = seconds;
    duration_prototype__proto.minutes        = minutes;
    duration_prototype__proto.hours          = hours;
    duration_prototype__proto.days           = days;
    duration_prototype__proto.weeks          = weeks;
    duration_prototype__proto.months         = months;
    duration_prototype__proto.years          = years;
    duration_prototype__proto.humanize       = humanize;
    duration_prototype__proto.toISOString    = iso_string__toISOString;
    duration_prototype__proto.toString       = iso_string__toISOString;
    duration_prototype__proto.toJSON         = iso_string__toISOString;
    duration_prototype__proto.locale         = locale;
    duration_prototype__proto.localeData     = localeData;

    // Deprecations
    duration_prototype__proto.toIsoString = deprecate('toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)', iso_string__toISOString);
    duration_prototype__proto.lang = lang;

    // Side effect imports

    addFormatToken('X', 0, 0, 'unix');
    addFormatToken('x', 0, 0, 'valueOf');

    // PARSING

    addRegexToken('x', matchSigned);
    addRegexToken('X', matchTimestamp);
    addParseToken('X', function (input, array, config) {
    	config._d = new Date(parseFloat(input, 10) * 1000);
    });
    addParseToken('x', function (input, array, config) {
    	config._d = new Date(toInt(input));
    });

    // Side effect imports


    utils_hooks__hooks.version = '2.10.6';

    setHookCallback(local__createLocal);

    utils_hooks__hooks.fn                    = momentPrototype;
    utils_hooks__hooks.min                   = min;
    utils_hooks__hooks.max                   = max;
    utils_hooks__hooks.utc                   = create_utc__createUTC;
    utils_hooks__hooks.unix                  = moment__createUnix;
    utils_hooks__hooks.months                = lists__listMonths;
    utils_hooks__hooks.isDate                = isDate;
    utils_hooks__hooks.locale                = locale_locales__getSetGlobalLocale;
    utils_hooks__hooks.invalid               = valid__createInvalid;
    utils_hooks__hooks.duration              = create__createDuration;
    utils_hooks__hooks.isMoment              = isMoment;
    utils_hooks__hooks.weekdays              = lists__listWeekdays;
    utils_hooks__hooks.parseZone             = moment__createInZone;
    utils_hooks__hooks.localeData            = locale_locales__getLocale;
    utils_hooks__hooks.isDuration            = isDuration;
    utils_hooks__hooks.monthsShort           = lists__listMonthsShort;
    utils_hooks__hooks.weekdaysMin           = lists__listWeekdaysMin;
    utils_hooks__hooks.defineLocale          = defineLocale;
    utils_hooks__hooks.weekdaysShort         = lists__listWeekdaysShort;
    utils_hooks__hooks.normalizeUnits        = normalizeUnits;
    utils_hooks__hooks.relativeTimeThreshold = duration_humanize__getSetRelativeTimeThreshold;

    var _moment = utils_hooks__hooks;

    return _moment;

}));
/*
 *               ~ CLNDR v1.2.16 ~
 * ==============================================
 *       https://github.com/kylestetz/CLNDR
 * ==============================================
 *  created by kyle stetz (github.com/kylestetz)
 *        &available under the MIT license
 * http://opensource.org/licenses/mit-license.php
 * ==============================================
 *
 * This is the fully-commented development version of CLNDR.
 * For the production version, check out clndr.min.js
 * at https://github.com/kylestetz/CLNDR
 *
 * This work is based on the
 * jQuery lightweight plugin boilerplate
 * Original author: @ajpiano
 * Further changes, comments: @addyosmani
 * Licensed under the MIT license
 */

 (function (factory) {

 	if (typeof define === 'function' && define.amd) {

    // AMD. Register as an anonymous module.
    define(['jquery', 'moment'], factory);
} else if (typeof exports === 'object') {

    // Node/CommonJS
    factory(require('jquery'), require('moment'));
} else {

    // Browser globals
    factory(jQuery, moment);
}

}(function ($, moment) {

  // This is the default calendar template. This can be overridden.
  var clndrTemplate = "<div class='clndr-controls'>" +
  "<div class='clndr-control-button'><span class='clndr-previous-button'>previous</span></div><div class='month'><%= month %> <%= year %></div><div class='clndr-control-button rightalign'><span class='clndr-next-button'>next</span></div>" +
  "</div>" +
  "<table class='clndr-table' border='0' cellspacing='0' cellpadding='0'>" +
  "<thead>" +
  "<tr class='header-days'>" +
  "<% for(var i = 0; i < daysOfTheWeek.length; i++) { %>" +
  "<td class='header-day'><%= daysOfTheWeek[i] %></td>" +
  "<% } %>" +
  "</tr>" +
  "</thead>" +
  "<tbody>" +
  "<% for(var i = 0; i < numberOfRows; i++){ %>" +
  "<tr>" +
  "<% for(var j = 0; j < 7; j++){ %>" +
  "<% var d = j + i * 7; %>" +
  "<td class='<%= days[d].classes %>'><div class='day-contents'><%= days[d].day %>" +
  "</div></td>" +
  "<% } %>" +
  "</tr>" +
  "<% } %>" +
  "</tbody>" +
  "</table>";

  var pluginName = 'clndr';

  var defaults = {
  	template: clndrTemplate,
  	weekOffset: 0,
  	startWithMonth: null,
  	clickEvents: {
  		click: null,
  		nextMonth: null,
  		previousMonth: null,
  		nextYear: null,
  		previousYear: null,
  		today: null,
  		onMonthChange: null,
  		onYearChange: null
  	},
  	targets: {
  		nextButton: 'clndr-next-button',
  		previousButton: 'clndr-previous-button',
  		nextYearButton: 'clndr-next-year-button',
  		previousYearButton: 'clndr-previous-year-button',
  		todayButton: 'clndr-today-button',
  		day: 'day',
  		empty: 'empty'
  	},
  	classes: {
  		today: "today",
  		event: "event",
  		past: "past",
  		lastMonth: "last-month",
  		nextMonth: "next-month",
  		adjacentMonth: "adjacent-month",
  		inactive: "inactive",
  		selected: "selected"
  	},
  	events: [],
  	extras: null,
  	dateParameter: 'date',
  	multiDayEvents: null,
  	doneRendering: null,
  	render: null,
  	daysOfTheWeek: null,
  	showAdjacentMonths: true,
  	adjacentDaysChangeMonth: false,
  	ready: null,
  	constraints: null,
  	forceSixRows: null,
  	trackSelectedDate: false,
  	selectedDate: null,
  	lengthOfTime: {
  		months: null,
  		days: null,
  		interval: 1
  	}
  };

  // The actual plugin constructor
  function Clndr( element, options ) {
  	this.element = element;

    // merge the default options with user-provided options
    this.options = $.extend(true, {}, defaults, options);

    // if there are events, we should run them through our addMomentObjectToEvents function
    // which will add a date object that we can use to make life easier. This is only necessary
    // when events are provided on instantiation, since our setEvents function uses addMomentObjectToEvents.
    if(this.options.events.length) {
    	if(this.options.multiDayEvents) {
    		this.options.events = this.addMultiDayMomentObjectsToEvents(this.options.events);
    	} else {
    		this.options.events = this.addMomentObjectToEvents(this.options.events);
    	}
    }

    // this used to be a place where we'd figure out the current month, but since
    // we want to open up support for arbitrary lengths of time we're going to
    // store the current range in addition to the current month.
    if(this.options.lengthOfTime.months || this.options.lengthOfTime.days) {
      // we want to establish intervalStart and intervalEnd, which will keep track
      // of our boundaries. Let's look at the possibilities...
      if(this.options.lengthOfTime.months) {
        // gonna go right ahead and annihilate any chance for bugs here.
        this.options.lengthOfTime.days = null;
        // the length is specified in months. Is there a start date?
        if(this.options.lengthOfTime.startDate) {
        	this.intervalStart = moment(this.options.lengthOfTime.startDate).startOf('month');
        } else if(this.options.startWithMonth) {
        	this.intervalStart = moment(this.options.startWithMonth).startOf('month');
        } else {
        	this.intervalStart = moment().startOf('month');
        }
        // subtract a day so that we are at the end of the interval. We always
        // want intervalEnd to be inclusive.
        this.intervalEnd = moment(this.intervalStart).add(this.options.lengthOfTime.months, 'months').subtract(1, 'days');
        this.month = this.intervalStart.clone();
    } else if(this.options.lengthOfTime.days) {
        // the length is specified in days. Start date?
        if(this.options.lengthOfTime.startDate) {
        	this.intervalStart = moment(this.options.lengthOfTime.startDate).startOf('day');
        } else {
        	this.intervalStart = moment().weekday(0).startOf('day');
        }
        this.intervalEnd = moment(this.intervalStart).add(this.options.lengthOfTime.days - 1, 'days').endOf('day');
        this.month = this.intervalStart.clone();
    }
} else {
	this.month = moment().startOf('month');
	this.intervalStart = moment(this.month);
	this.intervalEnd = moment(this.month).endOf('month');
}

if(this.options.startWithMonth) {
	this.month = moment(this.options.startWithMonth).startOf('month');
	this.intervalStart = moment(this.month);
	this.intervalEnd = moment(this.month).endOf('month');
}

    // if we've got constraints set, make sure the interval is within them.
    if(this.options.constraints) {
      // first check if the start date exists & is later than now.
      if(this.options.constraints.startDate) {
      	var startMoment = moment(this.options.constraints.startDate);
      	if(this.intervalStart.isBefore(startMoment, 'month')) {
          // try to preserve the date by moving only the month...
          this.intervalStart.set('month', startMoment.month()).set('year', startMoment.year());
          this.month.set('month', startMoment.month()).set('year', startMoment.year());
      }
  }
      // make sure the intervalEnd is before the endDate
      if(this.options.constraints.endDate) {
      	var endMoment = moment(this.options.constraints.endDate);
      	if(this.intervalEnd.isAfter(endMoment, 'month')) {
      		this.intervalEnd.set('month', endMoment.month()).set('year', endMoment.year());
      		this.month.set('month', endMoment.month()).set('year', endMoment.year());
      	}
      }
  }

  this._defaults = defaults;
  this._name = pluginName;

    // Some first-time initialization -> day of the week offset,
    // template compiling, making and storing some elements we'll need later,
    // & event handling for the controller.
    this.init();
}

Clndr.prototype.init = function () {
    // create the days of the week using moment's current language setting
    this.daysOfTheWeek = this.options.daysOfTheWeek || [];
    if(!this.options.daysOfTheWeek) {
    	this.daysOfTheWeek = [];
    	for(var i = 0; i < 7; i++) {
    		this.daysOfTheWeek.push( moment().weekday(i).format('dd').charAt(0) );
    	}
    }
    // shuffle the week if there's an offset
    if(this.options.weekOffset) {
    	this.daysOfTheWeek = this.shiftWeekdayLabels(this.options.weekOffset);
    }

    // quick & dirty test to make sure rendering is possible.
    if( !$.isFunction(this.options.render) ) {
    	this.options.render = null;
    	if (typeof _ === 'undefined') {
    		throw new Error("Underscore was not found. Please include underscore.js OR provide a custom render function.");
    	}
    	else {
        // we're just going ahead and using underscore here if no render method has been supplied.
        this.compiledClndrTemplate = _.template(this.options.template);
    }
}

    // create the parent element that will hold the plugin & save it for later
    $(this.element).html("<div class='clndr'></div>");
    this.calendarContainer = $('.clndr', this.element);

    // attach event handlers for clicks on buttons/cells
    this.bindEvents();

    // do a normal render of the calendar template
    this.render();

    // if a ready callback has been provided, call it.
    if(this.options.ready) {
    	this.options.ready.apply(this, []);
    }
};

Clndr.prototype.shiftWeekdayLabels = function(offset) {
	var days = this.daysOfTheWeek;
	for(var i = 0; i < offset; i++) {
		days.push( days.shift() );
	}
	return days;
};

  // This is where the magic happens. Given a starting date and ending date,
  // an array of calendarDay objects is constructed that contains appropriate
  // events and classes depending on the circumstance.
  Clndr.prototype.createDaysObject = function(startDate, endDate) {
    // this array will hold numbers for the entire grid (even the blank spaces)
    var daysArray = [];
    var date = startDate.clone();
    var lengthOfInterval = endDate.diff(startDate, 'days');

    // this is a helper object so that days can resolve their classes correctly.
    // Don't use it for anything please.
    this._currentIntervalStart = startDate.clone();

    // filter the events list (if it exists) to events that are happening last month, this month and next month (within the current grid view)
    this.eventsLastMonth = [];
    this.eventsThisInterval = [];
    this.eventsNextMonth = [];

    if(this.options.events.length) {

      // EVENT PARSING
      // here are the only two cases where we don't get an event in our interval:
      // startDate | endDate   | e.start   | e.end
      // e.start   | e.end     | startDate | endDate
      this.eventsThisInterval = $(this.options.events).filter( function() {
      	if(
      		this._clndrEndDateObject.isBefore(startDate) ||
      		this._clndrStartDateObject.isAfter(endDate)
      		) {
      		return false;
      } else {
      	return true;
      }
  }).toArray();

      if(this.options.showAdjacentMonths) {
      	var startOfLastMonth = startDate.clone().subtract(1, 'months').startOf('month');
      	var endOfLastMonth = startOfLastMonth.clone().endOf('month');
      	var startOfNextMonth = endDate.clone().add(1, 'months').startOf('month');
      	var endOfNextMonth = startOfNextMonth.clone().endOf('month');

      	this.eventsLastMonth = $(this.options.events).filter( function() {
      		if(
      			this._clndrEndDateObject.isBefore(startOfLastMonth) ||
      			this._clndrStartDateObject.isAfter(endOfLastMonth)
      			) {
      			return false;
      	} else {
      		return true;
      	}
      }).toArray();

      	this.eventsNextMonth = $(this.options.events).filter( function() {
      		if(
      			this._clndrEndDateObject.isBefore(startOfNextMonth) ||
      			this._clndrStartDateObject.isAfter(endOfNextMonth)
      			) {
      			return false;
      	} else {
      		return true;
      	}
      }).toArray();
      }
  }

    // if diff is greater than 0, we'll have to fill in last days of the previous month
    // to account for the empty boxes in the grid.
    // we also need to take into account the weekOffset parameter.
    // None of this needs to happen if the interval is being specified in days rather than months.
    if(!this.options.lengthOfTime.days) {
    	var diff = date.weekday() - this.options.weekOffset;
    	if(diff < 0) diff += 7;

    	if(this.options.showAdjacentMonths) {
    		for(var i = 0; i < diff; i++) {
    			var day = moment([startDate.year(), startDate.month(), i - diff + 1]);
    			daysArray.push( this.createDayObject(day, this.eventsLastMonth) );
    		}
    	} else {
    		for(var i = 0; i < diff; i++) {
    			daysArray.push( this.calendarDay({
    				classes: this.options.targets.empty + " " + this.options.classes.lastMonth
    			}) );
    		}
    	}
    }

    // now we push all of the days in the interval
    var dateIterator = startDate.clone();
    while( dateIterator.isBefore(endDate) || dateIterator.isSame(endDate, 'day') ) {
    	daysArray.push( this.createDayObject(dateIterator.clone(), this.eventsThisInterval) );
    	dateIterator.add(1, 'days');
    }

    // ...and if there are any trailing blank boxes, fill those in
    // with the next month first days.
    // Again, we can ignore this if the interval is specified in days.
    if(!this.options.lengthOfTime.days) {
    	while(daysArray.length % 7 !== 0) {
    		if(this.options.showAdjacentMonths) {
    			daysArray.push( this.createDayObject(dateIterator.clone(), this.eventsNextMonth) );
    		} else {
    			daysArray.push( this.calendarDay({
    				classes: this.options.targets.empty + " " + this.options.classes.nextMonth
    			}) );
    		}
    		dateIterator.add(1, 'days');
    	}
    }

    // if we want to force six rows of calendar, now's our Last Chance to add another row.
    // if the 42 seems explicit it's because we're creating a 7-row grid and 6 rows of 7 is always 42!
    if(this.options.forceSixRows && daysArray.length !== 42 ) {
    	while(daysArray.length < 42) {
    		if(this.options.showAdjacentMonths) {
    			daysArray.push( this.createDayObject(dateIterator.clone(), this.eventsNextMonth) );
    			dateIterator.add(1, 'days');
    		} else {
    			daysArray.push( this.calendarDay({
    				classes: this.options.targets.empty + " " + this.options.classes.nextMonth
    			}) );
    		}
    	}
    }

    return daysArray;
};

Clndr.prototype.createDayObject = function(day, monthEvents) {
	var eventsToday = [];
	var now = moment();
	var self = this;

    // validate moment date
    if (!day.isValid() && day.hasOwnProperty('_d') && day._d != undefined) {
    	day = moment(day._d);
    }

    var j = 0, l = monthEvents.length;
    for(j; j < l; j++) {
      // keep in mind that the events here already passed the month/year test.
      // now all we have to compare is the moment.date(), which returns the day of the month.
      var start = monthEvents[j]._clndrStartDateObject;
      var end = monthEvents[j]._clndrEndDateObject;
      // if today is the same day as start or is after the start, and
      // if today is the same day as the end or before the end ...
      // woohoo semantics!
      if( ( day.isSame(start, 'day') || day.isAfter(start, 'day') ) &&
      	( day.isSame(end, 'day') || day.isBefore(end, 'day') ) ) {
      	eventsToday.push( monthEvents[j] );
  }
}

var properties = {
	isInactive: false,
	isAdjacentMonth: false,
	isToday: false,
};
var extraClasses = "";

if(now.format("YYYY-MM-DD") == day.format("YYYY-MM-DD")) {
	extraClasses += (" " + this.options.classes.today);
	properties.isToday = true;
}
if(day.isBefore(now, 'day')) {
	extraClasses += (" " + this.options.classes.past);
}
if(eventsToday.length) {
	extraClasses += (" " + this.options.classes.event);
}
if(!this.options.lengthOfTime.days) {
	if(this._currentIntervalStart.month() > day.month()) {
		extraClasses += (" " + this.options.classes.adjacentMonth);
		properties.isAdjacentMonth = true;

		this._currentIntervalStart.year() === day.year()
		? extraClasses += (" " + this.options.classes.lastMonth)
		: extraClasses += (" " + this.options.classes.nextMonth);

	} else if(this._currentIntervalStart.month() < day.month()) {
		extraClasses += (" " + this.options.classes.adjacentMonth);
		properties.isAdjacentMonth = true;

		this._currentIntervalStart.year() === day.year()
		? extraClasses += (" " + this.options.classes.nextMonth)
		: extraClasses += (" " + this.options.classes.lastMonth);
	}
}

    // if there are constraints, we need to add the inactive class to the days outside of them
    if(this.options.constraints) {
    	if(this.options.constraints.startDate && day.isBefore(moment( this.options.constraints.startDate ))) {
    		extraClasses += (" " + this.options.classes.inactive);
    		properties.isInactive = true;
    	}
    	if(this.options.constraints.endDate && day.isAfter(moment( this.options.constraints.endDate ))) {
    		extraClasses += (" " + this.options.classes.inactive);
    		properties.isInactive = true;
    	}
    }

    // validate moment date
    if (!day.isValid() && day.hasOwnProperty('_d') && day._d != undefined) {
    	day = moment(day._d);
    }

    // check whether the day is "selected"
    if (this.options.selectedDate && day.isSame(moment(this.options.selectedDate), 'day')) {
    	extraClasses += (" " + this.options.classes.selected);
    }

    // we're moving away from using IDs in favor of classes, since when
    // using multiple calendars on a page we are technically violating the
    // uniqueness of IDs.
    extraClasses += " calendar-day-" + day.format("YYYY-MM-DD");

    // day of week
    extraClasses += " calendar-dow-" + day.weekday();

    return this.calendarDay({
    	day: day.date(),
    	classes: this.options.targets.day + extraClasses,
    	events: eventsToday,
    	date: day,
    	properties: properties
    });
};

Clndr.prototype.render = function() {
    // get rid of the previous set of calendar parts.
    // TODO: figure out if this is the right way to ensure proper garbage collection?
    this.calendarContainer.children().remove();

    var data = {};

    if(this.options.lengthOfTime.days) {
    	var days = this.createDaysObject(this.intervalStart.clone(), this.intervalEnd.clone());

    	data = {
    		daysOfTheWeek: this.daysOfTheWeek,
    		numberOfRows: Math.ceil(days.length / 7),
    		months: [],
    		days: days,
    		month: null,
    		year: null,
    		intervalStart: this.intervalStart.clone(),
    		intervalEnd: this.intervalEnd.clone(),
    		eventsThisInterval: this.eventsThisInterval,
    		eventsLastMonth: [],
    		eventsNextMonth: [],
    		extras: this.options.extras
    	};

    } else if(this.options.lengthOfTime.months) {

    	var months = [];
    	var eventsThisInterval = [];

    	for(i = 0; i < this.options.lengthOfTime.months; i++) {
    		var currentIntervalStart = this.intervalStart.clone().add(i, 'months');
    		var currentIntervalEnd = currentIntervalStart.clone().endOf('month');
    		var days = this.createDaysObject(currentIntervalStart, currentIntervalEnd);
        // save events processed for each month into a master array of events for
        // this interval
        eventsThisInterval.push(this.eventsThisInterval);
        months.push({
        	month: currentIntervalStart,
        	days: days
        });
    }

    data = {
    	daysOfTheWeek: this.daysOfTheWeek,
    	numberOfRows: _.reduce(months, function(memo, monthObj) {
    		return memo + Math.ceil(monthObj.days.length / 7);
    	}, 0),
    	months: months,
    	days: [],
    	month: null,
    	year: null,
    	intervalStart: this.intervalStart,
    	intervalEnd: this.intervalEnd,
    	eventsThisInterval: eventsThisInterval,
    	eventsLastMonth: this.eventsLastMonth,
    	eventsNextMonth: this.eventsNextMonth,
    	extras: this.options.extras
    };
} else {
      // get an array of days and blank spaces
      var days = this.createDaysObject(this.month.clone().startOf('month'), this.month.clone().endOf('month'));
      // this is to prevent a scope/naming issue between this.month and data.month
      var currentMonth = this.month;

      var data = {
      	daysOfTheWeek: this.daysOfTheWeek,
      	numberOfRows: Math.ceil(days.length / 7),
      	months: [],
      	days: days,
      	month: this.month.format('MMMM'),
      	year: this.month.year(),
      	eventsThisMonth: this.eventsThisInterval,
      	eventsLastMonth: this.eventsLastMonth,
      	eventsNextMonth: this.eventsNextMonth,
      	extras: this.options.extras
      };
  }

    // render the calendar with the data above & bind events to its elements
    if(!this.options.render) {
    	this.calendarContainer.html(this.compiledClndrTemplate(data));
    } else {
    	this.calendarContainer.html(this.options.render.apply(this, [data]));
    }

    // if there are constraints, we need to add the 'inactive' class to the controls
    if(this.options.constraints) {
      // in the interest of clarity we're just going to remove all inactive classes and re-apply them each render.
      for(var target in this.options.targets) {
      	if(target != this.options.targets.day) {
      		this.element.find('.' + this.options.targets[target]).toggleClass(this.options.classes.inactive, false);
      	}
      }

      var start = null;
      var end = null;

      if(this.options.constraints.startDate) {
      	start = moment(this.options.constraints.startDate);
      }
      if(this.options.constraints.endDate) {
      	end = moment(this.options.constraints.endDate);
      }
      // deal with the month controls first.
      // do we have room to go back?
      if(start && (start.isAfter(this.intervalStart) || start.isSame(this.intervalStart, 'day'))) {
      	this.element.find('.' + this.options.targets.previousButton).toggleClass(this.options.classes.inactive, true);
      }
      // do we have room to go forward?
      if(end && (end.isBefore(this.intervalEnd) || end.isSame(this.intervalEnd, 'day'))) {
      	this.element.find('.' + this.options.targets.nextButton).toggleClass(this.options.classes.inactive, true);
      }
      // what's last year looking like?
      if(start && start.isAfter(this.intervalStart.clone().subtract(1, 'years')) ) {
      	this.element.find('.' + this.options.targets.previousYearButton).toggleClass(this.options.classes.inactive, true);
      }
      // how about next year?
      if(end && end.isBefore(this.intervalEnd.clone().add(1, 'years')) ) {
      	this.element.find('.' + this.options.targets.nextYearButton).toggleClass(this.options.classes.inactive, true);
      }
      // today? we could put this in init(), but we want to support the user changing the constraints on a living instance.
      if(( start && start.isAfter( moment(), 'month' ) ) || ( end && end.isBefore( moment(), 'month' ) )) {
      	this.element.find('.' + this.options.targets.today).toggleClass(this.options.classes.inactive, true);
      }
  }

  if(this.options.doneRendering) {
  	this.options.doneRendering.apply(this, []);
  }
};

Clndr.prototype.bindEvents = function() {
	var $container = $(this.element);
	var self = this;
	var eventType = 'click';
	if (self.options.useTouchEvents === true) {
		eventType = 'touchstart';
	}

    // target the day elements and give them click events
    $container.on(eventType +'.clndr', '.'+this.options.targets.day, function(event) {
    	if(self.options.clickEvents.click) {
    		var target = self.buildTargetObject(event.currentTarget, true);
    		self.options.clickEvents.click.apply(self, [target]);
    	}
      // if adjacentDaysChangeMonth is on, we need to change the month here.
      if(self.options.adjacentDaysChangeMonth) {
      	if($(event.currentTarget).is( '.' + self.options.classes.lastMonth )) {
      		self.backActionWithContext(self);
      	} else if($(event.currentTarget).is( '.' + self.options.classes.nextMonth )) {
      		self.forwardActionWithContext(self);
      	}
      }
      // if trackSelectedDate is on, we need to handle click on a new day
      if (self.options.trackSelectedDate) {
        // remember new selected date
        self.options.selectedDate = self.getTargetDateString(event.currentTarget);

        // handle "selected" class
        $(event.currentTarget)
        .siblings().removeClass(self.options.classes.selected).end()
        .addClass(self.options.classes.selected);
    }
});
    // target the empty calendar boxes as well
    $container.on(eventType +'.clndr', '.'+this.options.targets.empty, function(event) {
    	if(self.options.clickEvents.click) {
    		var target = self.buildTargetObject(event.currentTarget, false);
    		self.options.clickEvents.click.apply(self, [target]);
    	}
    	if(self.options.adjacentDaysChangeMonth) {
    		if($(event.currentTarget).is( '.' + self.options.classes.lastMonth )) {
    			self.backActionWithContext(self);
    		} else if($(event.currentTarget).is( '.' + self.options.classes.nextMonth )) {
    			self.forwardActionWithContext(self);
    		}
    	}
    });

    // bind the previous, next and today buttons
    $container
    .on(eventType +'.clndr', '.'+this.options.targets.previousButton, { context: this }, this.backAction)
    .on(eventType +'.clndr', '.'+this.options.targets.nextButton, { context: this }, this.forwardAction)
    .on(eventType +'.clndr', '.'+this.options.targets.todayButton, { context: this }, this.todayAction)
    .on(eventType +'.clndr', '.'+this.options.targets.nextYearButton, { context: this }, this.nextYearAction)
    .on(eventType +'.clndr', '.'+this.options.targets.previousYearButton, { context: this }, this.previousYearAction);
}

  // If the user provided a click callback we'd like to give them something nice to work with.
  // buildTargetObject takes the DOM element that was clicked and returns an object with
  // the DOM element, events, and the date (if the latter two exist). Currently it is based on the id,
  // however it'd be nice to use a data- attribute in the future.
  Clndr.prototype.buildTargetObject = function(currentTarget, targetWasDay) {
    // This is our default target object, assuming we hit an empty day with no events.
    var target = {
    	element: currentTarget,
    	events: [],
    	date: null
    };
    // did we click on a day or just an empty box?
    if(targetWasDay) {
    	var dateString = this.getTargetDateString(currentTarget);
    	target.date = (dateString) ? moment(dateString) : null;

      // do we have events?
      if(this.options.events) {
        // are any of the events happening today?
        if(this.options.multiDayEvents) {
        	target.events = $.makeArray( $(this.options.events).filter( function() {
            // filter the dates down to the ones that match.
            return ( ( target.date.isSame(this._clndrStartDateObject, 'day') || target.date.isAfter(this._clndrStartDateObject, 'day') ) &&
            	( target.date.isSame(this._clndrEndDateObject, 'day') || target.date.isBefore(this._clndrEndDateObject, 'day') ) );
        }) );
        } else {
        	target.events = $.makeArray( $(this.options.events).filter( function() {
            // filter the dates down to the ones that match.
            return this._clndrStartDateObject.format('YYYY-MM-DD') == dateString;
        }) );
        }
    }
}

return target;
}

  // get moment date object of the date associated with the given target.
  // this method is meant to be called on ".day" elements.
  Clndr.prototype.getTargetDateString = function(target) {
    // Our identifier is in the list of classNames. Find it!
    var classNameIndex = target.className.indexOf('calendar-day-');
    if(classNameIndex !== -1) {
      // our unique identifier is always 23 characters long.
      // If this feels a little wonky, that's probably because it is.
      // Open to suggestions on how to improve this guy.
      return target.className.substring(classNameIndex + 13, classNameIndex + 23);
  }

  return null;
}

  // the click handlers in bindEvents need a context, so these are wrappers
  // to the actual functions. Todo: better way to handle this?
  Clndr.prototype.forwardAction = function(event) {
  	var self = event.data.context;
  	self.forwardActionWithContext(self);
  };

  Clndr.prototype.backAction = function(event) {
  	var self = event.data.context;
  	self.backActionWithContext(self);
  };

  // These are called directly, except for in the bindEvent click handlers,
  // where forwardAction and backAction proxy to these guys.
  Clndr.prototype.backActionWithContext = function(self) {
    // before we do anything, check if there is an inactive class on the month control.
    // if it does, we want to return and take no action.
    if(self.element.find('.' + self.options.targets.previousButton).hasClass('inactive')) {
    	return;
    }

    var yearChanged = null;

    if(!self.options.lengthOfTime.days) {
      // shift the interval by a month (or several months)
      self.intervalStart.subtract(self.options.lengthOfTime.interval, 'months').startOf('month');
      self.intervalEnd = self.intervalStart.clone().add(self.options.lengthOfTime.months || self.options.lengthOfTime.interval, 'months').subtract(1, 'days').endOf('month');

      if(!self.options.lengthOfTime.months) {
      	yearChanged = !self.month.isSame( moment(self.month).subtract(1, 'months'), 'year');
      }

      self.month = self.intervalStart.clone();
  } else {
      // shift the interval in days
      self.intervalStart.subtract(self.options.lengthOfTime.interval, 'days').startOf('day');
      self.intervalEnd = self.intervalStart.clone().add(self.options.lengthOfTime.days - 1, 'days').endOf('day');
      // this is useless, i think, but i'll keep it as a precaution for now
      self.month = self.intervalStart.clone();
  }

  self.render();

  if(!self.options.lengthOfTime.days && !self.options.lengthOfTime.months) {
  	if(self.options.clickEvents.previousMonth) {
  		self.options.clickEvents.previousMonth.apply( self, [moment(self.month)] );
  	}
  	if(self.options.clickEvents.onMonthChange) {
  		self.options.clickEvents.onMonthChange.apply( self, [moment(self.month)] );
  	}
  	if(yearChanged) {
  		if(self.options.clickEvents.onYearChange) {
  			self.options.clickEvents.onYearChange.apply( self, [moment(self.month)] );
  		}
  	}
  } else {
  	if(self.options.clickEvents.previousInterval) {
  		self.options.clickEvents.previousInterval.apply( self, [moment(self.intervalStart), moment(self.intervalEnd)] );
  	}
  	if(self.options.clickEvents.onIntervalChange) {
  		self.options.clickEvents.onIntervalChange.apply( self, [moment(self.intervalStart), moment(self.intervalEnd)] );
  	}
  }
};

Clndr.prototype.forwardActionWithContext = function(self) {
    // before we do anything, check if there is an inactive class on the month control.
    // if it does, we want to return and take no action.
    if(self.element.find('.' + self.options.targets.nextButton).hasClass('inactive')) {
    	return;
    }

    var yearChanged = null;

    if(!self.options.lengthOfTime.days) {
      // shift the interval by a month (or several months)
      self.intervalStart.add(self.options.lengthOfTime.interval, 'months').startOf('month');
      self.intervalEnd = self.intervalStart.clone().add(self.options.lengthOfTime.months || self.options.lengthOfTime.interval, 'months').subtract(1, 'days').endOf('month');

      if(!self.options.lengthOfTime.months) {
      	yearChanged = !self.month.isSame( moment(self.month).add(1, 'months'), 'year');
      }

      self.month = self.intervalStart.clone();
  } else {
      // shift the interval in days
      self.intervalStart.add(self.options.lengthOfTime.interval, 'days').startOf('day');
      self.intervalEnd = self.intervalStart.clone().add(self.options.lengthOfTime.days - 1, 'days').endOf('day');
      // this is useless, i think, but i'll keep it as a precaution for now
      self.month = self.intervalStart.clone();
  }

  self.render();

  if(!self.options.lengthOfTime.days && !self.options.lengthOfTime.months) {
  	if(self.options.clickEvents.nextMonth) {
  		self.options.clickEvents.nextMonth.apply( self, [moment(self.month)] );
  	}
  	if(self.options.clickEvents.onMonthChange) {
  		self.options.clickEvents.onMonthChange.apply( self, [moment(self.month)] );
  	}
  	if(yearChanged) {
  		if(self.options.clickEvents.onYearChange) {
  			self.options.clickEvents.onYearChange.apply( self, [moment(self.month)] );
  		}
  	}
  } else {
  	if(self.options.clickEvents.nextInterval) {
  		self.options.clickEvents.nextInterval.apply( self, [moment(self.intervalStart), moment(self.intervalEnd)] );
  	}
  	if(self.options.clickEvents.onIntervalChange) {
  		self.options.clickEvents.onIntervalChange.apply( self, [moment(self.intervalStart), moment(self.intervalEnd)] );
  	}
  }
};

Clndr.prototype.todayAction = function(event) {
	var self = event.data.context;

    // did we switch months when the today button was hit?
    var monthChanged = !self.month.isSame(moment(), 'month');
    var yearChanged = !self.month.isSame(moment(), 'year');

    self.month = moment().startOf('month');

    if(self.options.lengthOfTime.days) {
      // if there was a startDate specified, we should figure out what the weekday is and
      // use that as the starting point of our interval. If not, go to today.weekday(0)
      if(self.options.lengthOfTime.startDate) {
      	self.intervalStart = moment().weekday(self.options.lengthOfTime.startDate.weekday()).startOf('day');
      } else {
      	self.intervalStart = moment().weekday(0).startOf('day');
      }
      self.intervalEnd = self.intervalStart.clone().add(self.options.lengthOfTime.days - 1, 'days').endOf('day');

  } else if(self.options.lengthOfTime.months) {
      // set the intervalStart to this month.
      self.intervalStart = moment().startOf('month');
      self.intervalEnd = self.intervalStart.clone()
      .add(self.options.lengthOfTime.months || self.options.lengthOfTime.interval, 'months')
      .subtract(1, 'days')
      .endOf('month');
  } else if(monthChanged) {
      // reset the start interval for the current month
      self.intervalStart = moment().startOf('month');
      // no need to re-render if we didn't change months.
      self.render();

      // fire the today event handler regardless of whether the month changed.
      if(self.options.clickEvents.today) {
      	self.options.clickEvents.today.apply( self, [moment(self.month)] );
      }

      // fire the onMonthChange callback
      if(self.options.clickEvents.onMonthChange) {
      	self.options.clickEvents.onMonthChange.apply( self, [moment(self.month)] );
      }
      // maybe fire the onYearChange callback?
      if(yearChanged) {
      	if(self.options.clickEvents.onYearChange) {
      		self.options.clickEvents.onYearChange.apply( self, [moment(self.month)] );
      	}
      }
  }

  if(self.options.lengthOfTime.days || self.options.lengthOfTime.months) {
  	self.render();
      // fire the today event handler regardless of whether the month changed.
      if(self.options.clickEvents.today) {
      	self.options.clickEvents.today.apply( self, [moment(self.month)] );
      }
      if(self.options.clickEvents.onIntervalChange) {
      	self.options.clickEvents.onIntervalChange.apply( self, [moment(self.intervalStart), moment(self.intervalEnd)] );
      }
  }
};

Clndr.prototype.nextYearAction = function(event) {
	var self = event.data.context;
    // before we do anything, check if there is an inactive class on the month control.
    // if it does, we want to return and take no action.
    if(self.element.find('.' + self.options.targets.nextYearButton).hasClass('inactive')) {
    	return;
    }

    self.month.add(1, 'years');
    self.intervalStart.add(1, 'years');
    self.intervalEnd.add(1, 'years');

    self.render();

    if(self.options.clickEvents.nextYear) {
    	self.options.clickEvents.nextYear.apply( self, [moment(self.month)] );
    }
    if(self.options.lengthOfTime.days || self.options.lengthOfTime.months) {
    	if(self.options.clickEvents.onIntervalChange) {
    		self.options.clickEvents.onIntervalChange.apply( self, [moment(self.intervalStart), moment(self.intervalEnd)] );
    	}
    } else {
    	if(self.options.clickEvents.onMonthChange) {
    		self.options.clickEvents.onMonthChange.apply( self, [moment(self.month)] );
    	}
    	if(self.options.clickEvents.onYearChange) {
    		self.options.clickEvents.onYearChange.apply( self, [moment(self.month)] );
    	}
    }
};

Clndr.prototype.previousYearAction = function(event) {
	var self = event.data.context;
    // before we do anything, check if there is an inactive class on the month control.
    // if it does, we want to return and take no action.
    console.log(self.element.find('.' + self.options.targets.previousYear));
    if(self.element.find('.' + self.options.targets.previousYearButton).hasClass('inactive')) {
    	return;
    }

    self.month.subtract(1, 'years');
    self.intervalStart.subtract(1, 'years');
    self.intervalEnd.subtract(1, 'years');

    self.render();

    if(self.options.clickEvents.previousYear) {
    	self.options.clickEvents.previousYear.apply( self, [moment(self.month)] );
    }
    if(self.options.lengthOfTime.days || self.options.lengthOfTime.months) {
    	if(self.options.clickEvents.onIntervalChange) {
    		self.options.clickEvents.onIntervalChange.apply( self, [moment(self.intervalStart), moment(self.intervalEnd)] );
    	}
    } else {
    	if(self.options.clickEvents.onMonthChange) {
    		self.options.clickEvents.onMonthChange.apply( self, [moment(self.month)] );
    	}
    	if(self.options.clickEvents.onYearChange) {
    		self.options.clickEvents.onYearChange.apply( self, [moment(self.month)] );
    	}
    }
};

Clndr.prototype.forward = function(options) {
	if(!this.options.lengthOfTime.days) {
      // shift the interval by a month (or several months)
      this.intervalStart.add(this.options.lengthOfTime.interval, 'months').startOf('month');
      this.intervalEnd = this.intervalStart.clone().add(this.options.lengthOfTime.months || this.options.lengthOfTime.interval, 'months').subtract(1, 'days').endOf('month');
      this.month = this.intervalStart.clone();
  } else {
      // shift the interval in days
      this.intervalStart.add(this.options.lengthOfTime.interval, 'days').startOf('day');
      this.intervalEnd = this.intervalStart.clone().add(this.options.lengthOfTime.days - 1, 'days').endOf('day');
      this.month = this.intervalStart.clone();
  }

  this.render();

  if(options && options.withCallbacks) {
  	if(this.options.lengthOfTime.days || this.options.lengthOfTime.months) {
  		if(this.options.clickEvents.onIntervalChange) {
  			this.options.clickEvents.onIntervalChange.apply( this, [moment(this.intervalStart), moment(this.intervalEnd)] );
  		}
  	} else {
  		if(this.options.clickEvents.onMonthChange) {
  			this.options.clickEvents.onMonthChange.apply( this, [moment(this.month)] );
  		}
        // We entered a new year
        if (this.month.month() === 0 && this.options.clickEvents.onYearChange) {
        	this.options.clickEvents.onYearChange.apply( this, [moment(this.month)] );
        }
    }
}

return this;
}

Clndr.prototype.back = function(options) {
	if(!this.options.lengthOfTime.days) {
      // shift the interval by a month (or several months)
      this.intervalStart.subtract(this.options.lengthOfTime.interval, 'months').startOf('month');
      this.intervalEnd = this.intervalStart.clone().add(this.options.lengthOfTime.months || this.options.lengthOfTime.interval, 'months').subtract(1, 'days').endOf('month');
      this.month = this.intervalStart.clone();
  } else {
      // shift the interval in days
      this.intervalStart.subtract(this.options.lengthOfTime.interval, 'days').startOf('day');
      this.intervalEnd = this.intervalStart.clone().add(this.options.lengthOfTime.days - 1, 'days').endOf('day');
      this.month = this.intervalStart.clone();
  }

  this.render();

  if(options && options.withCallbacks) {
  	if(this.options.lengthOfTime.days || this.options.lengthOfTime.months) {
  		if(this.options.clickEvents.onIntervalChange) {
  			this.options.clickEvents.onIntervalChange.apply( this, [moment(this.intervalStart), moment(this.intervalEnd)] );
  		}
  	} else {
  		if(this.options.clickEvents.onMonthChange) {
  			this.options.clickEvents.onMonthChange.apply( this, [moment(this.month)] );
  		}
        // We went all the way back to previous year
        if (this.month.month() === 11 && this.options.clickEvents.onYearChange) {
        	this.options.clickEvents.onYearChange.apply( this, [moment(this.month)] );
        }
    }
}

return this;
}

  // alternate names for convenience
  Clndr.prototype.next = function(options) {
  	this.forward(options);
  	return this;
  }

  Clndr.prototype.previous = function(options) {
  	this.back(options);
  	return this;
  }

  Clndr.prototype.setMonth = function(newMonth, options) {
    // accepts 0 - 11 or a full/partial month name e.g. "Jan", "February", "Mar"
    if(!this.options.lengthOfTime.days && !this.options.lengthOfTime.months) {
    	this.month.month(newMonth);
    	this.intervalStart = this.month.clone().startOf('month');
    	this.intervalEnd = this.intervalStart.clone().endOf('month');
    	this.render();
    	if(options && options.withCallbacks) {
    		if(this.options.clickEvents.onMonthChange) {
    			this.options.clickEvents.onMonthChange.apply( this, [moment(this.month)] );
    		}
    	}
    } else {
    	console.log('You are using a custom date interval; use Clndr.setIntervalStart(startDate) instead.');
    }
    return this;
}

Clndr.prototype.setIntervalStart = function(newDate, options) {
    // accepts a date string or moment object
    if(this.options.lengthOfTime.days) {
    	this.intervalStart = moment(newDate).startOf('day');
    	this.intervalEnd = this.intervalStart.clone().add(this.options.lengthOfTime.days - 1, 'days').endOf('day');
    } else if(this.options.lengthOfTime.months) {
    	this.intervalStart = moment(newDate).startOf('month');
    	this.intervalEnd = this.intervalStart.clone().add(this.options.lengthOfTime.months || this.options.lengthOfTime.interval, 'months').subtract(1, 'days').endOf('month');
    	this.month = this.intervalStart.clone();
    }

    if(this.options.lengthOfTime.days || this.options.lengthOfTime.months) {
    	this.render();

    	if(options && options.withCallbacks) {
    		if(this.options.clickEvents.onIntervalChange) {
    			this.options.clickEvents.onIntervalChange.apply( this, [moment(this.intervalStart), moment(this.intervalEnd)] );
    		}
    	}
    } else {
    	console.log('You are using a custom date interval; use Clndr.setIntervalStart(startDate) instead.');
    }
    return this;
}

Clndr.prototype.nextYear = function(options) {
	this.month.add(1, 'year');
	this.intervalStart.add(1, 'year');
	this.intervalEnd.add(1, 'year');
	this.render();
	if(options && options.withCallbacks) {
		if(this.options.clickEvents.onYearChange) {
			this.options.clickEvents.onYearChange.apply( this, [moment(this.month)] );
		}
		if(this.options.clickEvents.onIntervalChange) {
			this.options.clickEvents.onIntervalChange.apply( this, [moment(this.intervalStart), moment(this.intervalEnd)] );
		}
	}
	return this;
}

Clndr.prototype.previousYear = function(options) {
	this.month.subtract(1, 'year');
	this.intervalStart.subtract(1, 'year');
	this.intervalEnd.subtract(1, 'year');
	this.render();
	if(options && options.withCallbacks) {
		if(this.options.clickEvents.onYearChange) {
			this.options.clickEvents.onYearChange.apply( this, [moment(this.month)] );
		}
		if(this.options.clickEvents.onIntervalChange) {
			this.options.clickEvents.onIntervalChange.apply( this, [moment(this.intervalStart), moment(this.intervalEnd)] );
		}
	}
	return this;
}

Clndr.prototype.setYear = function(newYear, options) {
	this.month.year(newYear);
	this.intervalStart.year(newYear);
	this.intervalEnd.year(newYear);
	this.render();
	if(options && options.withCallbacks) {
		if(this.options.clickEvents.onYearChange) {
			this.options.clickEvents.onYearChange.apply( this, [moment(this.month)] );
		}
		if(this.options.clickEvents.onIntervalChange) {
			this.options.clickEvents.onIntervalChange.apply( this, [moment(this.intervalStart), moment(this.intervalEnd)] );
		}
	}
	return this;
}

Clndr.prototype.setEvents = function(events) {
    // go through each event and add a moment object
    if(this.options.multiDayEvents) {
    	this.options.events = this.addMultiDayMomentObjectsToEvents(events);
    } else {
    	this.options.events = this.addMomentObjectToEvents(events);
    }

    this.render();
    return this;
};

Clndr.prototype.addEvents = function(events) {
    // go through each event and add a moment object
    if(this.options.multiDayEvents) {
    	this.options.events = $.merge(this.options.events, this.addMultiDayMomentObjectsToEvents(events));
    } else {
    	this.options.events = $.merge(this.options.events, this.addMomentObjectToEvents(events));
    }

    this.render();
    return this;
};

Clndr.prototype.removeEvents = function(matchingFunction) {
	for (var i = this.options.events.length-1; i >= 0; i--) {
		if(matchingFunction(this.options.events[i]) == true) {
			this.options.events.splice(i, 1);
		}
	}

	this.render();
	return this;
};

Clndr.prototype.addMomentObjectToEvents = function(events) {
	var self = this;
	var i = 0, l = events.length;
	for(i; i < l; i++) {
      // add the date as both start and end, since it's a single-day event by default
      events[i]._clndrStartDateObject = moment( events[i][self.options.dateParameter] );
      events[i]._clndrEndDateObject = moment( events[i][self.options.dateParameter] );
  }
  return events;
}

Clndr.prototype.addMultiDayMomentObjectsToEvents = function(events) {
	var self = this;
	var i = 0, l = events.length;
	for(i; i < l; i++) {
      // if we don't find the startDate OR endDate fields, look for singleDay
      if(!events[i][self.options.multiDayEvents.endDate] && !events[i][self.options.multiDayEvents.startDate]) {
      	events[i]._clndrEndDateObject = moment( events[i][self.options.multiDayEvents.singleDay] );
      	events[i]._clndrStartDateObject = moment( events[i][self.options.multiDayEvents.singleDay] );
      } else {
        // otherwise use startDate and endDate, or whichever one is present.
        events[i]._clndrEndDateObject = moment( events[i][self.options.multiDayEvents.endDate] || events[i][self.options.multiDayEvents.startDate] );
        events[i]._clndrStartDateObject = moment( events[i][self.options.multiDayEvents.startDate] || events[i][self.options.multiDayEvents.endDate] );
    }
}
return events;
}

Clndr.prototype.calendarDay = function(options) {
	var defaults = { day: "", classes: this.options.targets.empty, events: [], date: null };
	return $.extend({}, defaults, options);
}

$.fn.clndr = function(options) {
	if(this.length === 1) {
		if(!this.data('plugin_clndr')) {
			var clndr_instance = new Clndr(this, options);
			this.data('plugin_clndr', clndr_instance);
			return clndr_instance;
		}
	} else if(this.length > 1) {
		throw new Error("CLNDR does not support multiple elements yet. Make sure your clndr selector returns only one element.");
	}
}

}));

/*global $, console */
var Mushi = {};

$(document).ready(function(){
	Mushi.common.init();
	var pageName = $('body').attr('rel');

	if( (pageName !== 'undefined') && (pageName !== '') ) {
		Mushi[pageName].init();
	}
});

/*global $, Mushi, console, _ */

Mushi.checkin = {
	init: function () {
		console.log('Checkin started');
		this.addFieldset('.checkin form');
		this.addPeople('.more-people');
		this.docTab('.tab-radio');
	},

	addFieldset : function (target) {
		var count = $('> ul', target).find('> li').length;
		var html = $('#checkin-template').html();
		html = html.replace(/checkin\[ID\]/gmi, 'checkin['+count+']');

		var template = _.template(html);

		var li = $('<li>').html(template);

		$('> ul', target).append(li);

		this.validateCheckin('.checkin form');
	},

	addPeople : function (target) {
		$(target).bind('click', function () {
			Mushi.checkin.addFieldset('.checkin form');
			var newElement = $('.checkin form > ul > li:last-child');

			newElement.addClass('-recent');
			$('body, html').animate({ scrollTop : $('.checkin form > ul > li:last-child').position().top - $('header.top').height() - $('form.reserva.-top').height() }, function () {
				newElement.removeClass('-recent');
			});
			return false;
		});
	},

	validateCheckin: function (target) {
		var options = {
			errorElement : 'em',
			rules : {
				'nome[]': "required",
				'email[]' : {
					required : true,
					email : true
				},
				'telefone[]': "required",
				'nascimento[]': "required",
				'sexo[]': "required",
				'nacionalidade[]': "required",
				'pais[]': "required",
				'estado[]': "required",
				'cidade[]': "required",
				'endereco[]': "required",
				'numero[]': "required"
			},
			messages : {
				'nome[]': "Preencha o seu nome",
				'email[]': "Preencha o seu e-mail",
				'telefone[]': "Preencha o seu telefone",
				'nascimento[]': "Preencha o campo",
				'sexo[]': "Escolha um genero",
				'nacionalidade[]': "Escolha uma nacionalidade",
				'pais[]': "Escolha um país",
				'estado[]': "Escolha um estado",
				'cidade[]': "Escolha uma cidade",
				'endereco[]': "Preencha um endereço",
				'numero[]': "Preencha um número"
			}
		};
		$(target).validate(options);
	},

	docTab: function (target) {
		var theHeight = $('.tabs').height();
		$('.tabs').height(theHeight);

		$(target).bind('change', function () {
			var field = $($(this).data('target')),
			parent = $(this.parentNode.parentNode.parentNode.parentNode);

			$('.tabs', parent).stop(false, false).animate({ opacity : 0 }, function () {
				$('ul.tab', parent).removeClass('-active');
				field.addClass('-active');

				$(this).stop(false, false).animate({ opacity : 1 });
			});
			return false;
		});
	}
};

/*global $, Mushi, console, Swiper, moment */

Mushi.common = {
	init: function () {
		console.log('Mushi started');
		this.openMenu();
		this.reservaToggle();
		this.validateReserva('form.reserva.-top');
		this.validateNewsletter('form.newsletter');
		this.numberAnimate('form.reserva.-top .number');
		this.calendars('form.reserva .calendar-input');
		this.initGalleries('.gallery .swiper-container');
		this.innerParallax('.inner > header, .read > header');
		this.loginPanel('form.reserva.-top a.tour-agent');
	},

	openMenu: function () {
		$('header.top .btn-menu').bind('click', function () {
			$('.holder-menu').toggleClass('-active');
			return false;
		});
	},

	reservaToggle: function () {
		$('form.reserva.-top > .container > ul > li:first-child').bind('click', function () {
			if ($(window).width() <= 720) {
				if (!$('form.reserva.-top').hasClass('-active')) {
					$('form.reserva.-top').stop(false, false).animate({ height : $('form.reserva.-top > .container').height() }, function () {
						$('form.reserva.-top').addClass('-active');
					});
				} else {
					$('form.reserva.-top').stop(false, false).animate({ height : $('form.reserva.-top > .container > ul > li:first-child').height() }, function () {
						$('form.reserva.-top').removeClass('-active');
					});
				}
			}
		});
	},

	validateReserva: function (target) {
		var options = {
			errorElement : 'em',
			rules : {
				checkin : "required",
				checkout : "required"
			},
			messages : {
				checkin: "Selecione uma data",
				checkout: "Selecione uma data"
			},
			submitHandler: function(form) {
				var chegada_input = $(form).find("input[name=arrivaldate]");
				var chegada_Ar = chegada_input.val().split('/');
				//converte formato da  data para yyyymmdd para o envio ao https://securebr.e-gds.com/MussuloResortByMantra
				var chegada_to_send = chegada_Ar[2] + chegada_Ar[1] + chegada_Ar[0];

				chegada_input.val(chegada_to_send);
				
				var saida_input = $(form).find("input[name=departuredate]"); 
				var saida_Ar = saida_input.val().split('/');
				var saida_to_send = saida_Ar[2] + saida_Ar[1] + saida_Ar[0];
				
				saida_input.val(saida_to_send);
				form.submit();
				// return false;
			}
		};
		$(target).validate(options);
	},

	validateNewsletter: function (target) {
		var options = {
			errorElement : 'em',
			rules : {
				email : {
					required : true,
					email : true
				}
			},
			messages : {
				email: "Por favor, preencha o seu e-mail"
			}
		};
		$(target).validate(options);
	},

	numberAnimate: function (target) {
		var up = $('button.up', target),
		down = $('button.down', target);

		up.bind('click', function () {
			var input = $('input[type="hidden"]', this.parentNode),
			mask = $('.mask span', this.parentNode),
			value = input.val();

			value++;

			input.val(value);

			$(mask).stop(false, false).animate({ top : '-200%' }, 100, function () {
				$(this).html(value);
				$(this).css('top', '200%');
				$(this).animate({ top : '50%' });
			});

			return false;
		});

		down.bind('click', function () {
			var input = $('input[type="hidden"]', this.parentNode),
			mask = $('.mask span', this.parentNode),
			value = input.val();

			if (value-- > 0) {
				input.val(value);
				$(mask).stop(false, false).animate({ top : '200%' }, 100, function () {
					$(this).html(value);
					$(this).css('top', '-200%');
					$(this).animate({ top : '50%' });
				});
			}

			return false;
		});
	},

	calendars: function (target) {
		var options_checkin = {
			clickEvents : {
				click : function (result) {
					$('.calendar-input', $('#calendar_checkin').parent()).val(result.date.format('L'));
					$('.calendar-input', $('#calendar_checkout').parent()).val(result.date.add(1,'days').format('L'));
					$('.calendar').removeClass('-active');
				}
			}
		},
		options_checkout = {
			clickEvents : {
				click : function (result) {
					$('.calendar-input', $('#calendar_checkout').parent()).val(result.date.format('L'));
					$('.calendar').removeClass('-active');
				}
			}
		};
		$('#calendar_checkin').clndr(options_checkin);
		$('#calendar_checkout').clndr(options_checkout);

		$(target).on('focus', function () {
			var parent = $(this).parent();
			$('.calendar').removeClass('-active');
			$('.calendar', parent).addClass('-active');
		});

		$('span', target.parentNode).on('click', function () {
			var parent = $(this).parent();
			$('.calendar').removeClass('-active');
			$('.calendar', parent).addClass('-active');
		});

	},

	initGalleries: function (target) {
		var i = 0;

		$(target).each(function () {
			var parent = $(this).parent(),
			index = i++,
			swiper,
			options = {
				loop: false,
				prevButton: $('button.prev', parent),
				nextButton: $('button.next', parent),
				slidesPerView : 4,
				spaceBetween : 20
			};

			$(window).on('load', function () {
				console.log($(this).width());
				if ($(this).width() <= 768) {
					options = {
						loop: false,
						prevButton: $('button.prev', parent),
						nextButton: $('button.next', parent),
						slidesPerView : 1,
						spaceBetween : 20
					};
					swiper = new Swiper($(target), options);
				}
			});

			swiper = new Swiper($(target), options);

			$('a', this).fancybox({
				overlayShow  : true,
				scrolling : 'yes'
			});
		});
	},

	innerParallax : function (target) {
		if ($(target).length > 0) {
			$(window).bind('scroll', function () {
				var bodyScroll = $('body').scrollTop();
				if (bodyScroll <= $(target).position().top + $(target).height()) {
					$(target).css('background-position', '50% ' + (50 - (bodyScroll / 5)) + '%');
				}
			});
		}
	},

	loginPanel : function (target) {
		$(target).bind('click', function (event) {
			if ($(event.target).hasClass('tour-agent') || $(event.target).is('strong')) {
				$(this).toggleClass('-active');
				return false;
			}
		});
	}
};

/*global $, Mushi, console */

Mushi.contato = {
	init: function () {
		console.log('Contato started');
		this.validateContato('.contato form');
	},

	validateContato: function (target) {
		var options = {
			errorElement : 'em',
			rules : {
				nome: "required",
				mensagem: "required",
				email : {
					required : true,
					email : true
				}
			},
			messages : {
				nome: "Preencha o seu nome",
				email: "Preencha o seu e-mail",
				mensagem: "Preencha a sua mensagem"
			}
		};
		$(target).validate(options);
	}
};

/*global $, Mushi, console, Modernizr */

Mushi.galeria = {
	init: function () {
		console.log('Galeria started');
		this.fixElementsHeight('section.galeria .album article');
		this.directionAwareHover('section.galeria .album article');
		this.fancybox('section.galeria .album');
	},

	directionAwareHover: function (target) {
		var that = {
			support : Modernizr.csstransitions,
			_getDir : function ($el, coordinates) {
				// the width and height of the current div
				var w = $el.width(),
				h = $el.height(),

					// calculate the x and y to get an angle to the center of the div from that x and y.
					// gets the x value relative to the center of the DIV and "normalize" it
					x = (coordinates.x - $el.offset().left - (w / 2)) * (w > h ? (h / w) : 1),
					y = (coordinates.y - $el.offset().top  - (h / 2)) * (h > w ? (w / h) : 1),

					// the angle and the direction from where the mouse came in/went out clockwise (TRBL=0123);
					// first calculate the angle of the point,
					// add 180 deg to get rid of the negative values
					// divide by 90 to get the quadrant
					// add 3 and do a modulo by 4  to shift the quadrants to a proper clockwise TRBL (top/right/bottom/left) **/
					direction = Math.round((((Math.atan2(y, x) * (180 / Math.PI)) + 180) / 90) + 3) % 4;

					return direction;

				},

				_getStyle : function (direction) {
					var fromStyle, toStyle,
					slideFromTop = { left : '0px', top : '-100%' },
					slideFromBottom = { left : '0px', top : '100%' },
					slideFromLeft = { left : '-100%', top : '0px' },
					slideFromRight = { left : '100%', top : '0px' },
					slideTop = { top : '0px' },
					slideLeft = { left : '0px' },
					inverse = false;

					switch (direction) {
						case 0:
					// from top
					fromStyle = !inverse ? slideFromTop : slideFromBottom;
					toStyle = slideTop;
					break;
					case 1:
					// from right
					fromStyle = !inverse ? slideFromRight : slideFromLeft;
					toStyle = slideLeft;
					break;
					case 2:
					// from bottom
					fromStyle = !inverse ? slideFromBottom : slideFromTop;
					toStyle = slideTop;
					break;
					case 3:
					// from left
					fromStyle = !inverse ? slideFromLeft : slideFromRight;
					toStyle = slideLeft;
					break;
				}
				return { from : fromStyle, to : toStyle };
			},

			_applyAnimation : function (el, styleCSS, speed) {
				$.fn.applyStyle = this.support ? $.fn.css : $.fn.animate;
				el.stop().applyStyle(styleCSS, $.extend(true, [], { duration : speed + 'ms' }));
			}
		};

		//Mouse enter / Mouse leave
		$(target).on('mouseenter.hoverdir, mouseleave.hoverdir', function (event) {
			var $el = $(this),
			$hoverElem = $el.find('span.brackets'),
			direction = that._getDir($el, { x : event.pageX, y : event.pageY }),
			styleCSS = that._getStyle(direction);

			if (event.type === 'mouseenter') {
				$hoverElem.hide().css(styleCSS.from);
				$hoverElem.show(0, function () {
					var $el = $(this);
					if (that.support) {
						$el.css('transition', 'all 300ms ease');
					}
					that._applyAnimation($el, styleCSS.to, 300);
				});
			} else {
				if (that.support) {
					$hoverElem.css('transition', 'all 300ms ease');
				}
				that._applyAnimation($hoverElem, styleCSS.from, 300);
			}
		});
	},

	fixElementsHeight: function (target) {
		$(window).bind('resize load', function () {
			$(target).each(function () {
				$(this).height($(this).innerWidth());
			});
		});
	},

	fancybox: function (target) {
		$('a', target).fancybox({
			overlayShow  : true,
			scrolling : 'yes'
		});
	}
};

/*global $, Mushi, console, Swiper, self, Modernizr */

Mushi.home = {
	featuredSwiper : null,

	init: function () {
		console.log('Home started');
		this.swiperFeatured('section.featured');
		this.parallaxFeatured('section.featured .swiper-slide');
		this.paraibadisiacoTabs();
		this.fixElementsHeight('section.destination article');
		this.directionAwareHover('section.destination article');
		this.destinationHover('section.destination article');
	},

	swiperFeatured: function (target) {
		var options = {
			autoplay: 4000,
			loop: true,
			pagination: target + ' .swiper-pagination',
			paginationClickable: true,
			autoplayDisableOnInteraction: false,
			prevButton: target + ' button.prev',
			nextButton: target + ' button.next',
			onInit: function () {
				var i = 0,
				thumbUrl;
				$('.swiper-slide', this.parentNode).each(function () {
					thumbUrl = $(this).css('background-image');
					$('.swiper-pagination .swiper-pagination-bullet:eq(' + i + ')', this.parentNode.parentNode)
					.append('<span class="bg"></span>')
					.find('span.bg')
					.css('background-image', thumbUrl);
					i++;
				});
			}
		};

		this.featuredSwiper = new Swiper(target, options);
	},

	parallaxFeatured: function (target) {
		$(window).bind('scroll', function () {
			var bodyScroll = $('body').scrollTop();
			if (bodyScroll <= $(target).position().top + $(target).height()) {
				$(target).css('background-position', '50% ' + (50 - bodyScroll) + '%');
			}
		});
	},

	directionAwareHover: function (target) {
		var that = {
			support : Modernizr.csstransitions,
			_getDir : function ($el, coordinates) {
				// the width and height of the current div
				var w = $el.width(),
				h = $el.height(),

					// calculate the x and y to get an angle to the center of the div from that x and y.
					// gets the x value relative to the center of the DIV and "normalize" it
					x = (coordinates.x - $el.offset().left - (w / 2)) * (w > h ? (h / w) : 1),
					y = (coordinates.y - $el.offset().top  - (h / 2)) * (h > w ? (w / h) : 1),

					// the angle and the direction from where the mouse came in/went out clockwise (TRBL=0123);
					// first calculate the angle of the point,
					// add 180 deg to get rid of the negative values
					// divide by 90 to get the quadrant
					// add 3 and do a modulo by 4  to shift the quadrants to a proper clockwise TRBL (top/right/bottom/left) **/
					direction = Math.round((((Math.atan2(y, x) * (180 / Math.PI)) + 180) / 90) + 3) % 4;

					return direction;

				},

				_getStyle : function (direction) {
					var fromStyle, toStyle,
					slideFromTop = { left : '0px', top : '-100%' },
					slideFromBottom = { left : '0px', top : '100%' },
					slideFromLeft = { left : '-100%', top : '0px' },
					slideFromRight = { left : '100%', top : '0px' },
					slideTop = { top : '0px' },
					slideLeft = { left : '0px' },
					inverse = false;

					switch (direction) {
						case 0:
					// from top
					fromStyle = !inverse ? slideFromTop : slideFromBottom;
					toStyle = slideTop;
					break;
					case 1:
					// from right
					fromStyle = !inverse ? slideFromRight : slideFromLeft;
					toStyle = slideLeft;
					break;
					case 2:
					// from bottom
					fromStyle = !inverse ? slideFromBottom : slideFromTop;
					toStyle = slideTop;
					break;
					case 3:
					// from left
					fromStyle = !inverse ? slideFromLeft : slideFromRight;
					toStyle = slideLeft;
					break;
				}
				return { from : fromStyle, to : toStyle };
			},

			_applyAnimation : function (el, styleCSS, speed) {
				$.fn.applyStyle = this.support ? $.fn.css : $.fn.animate;
				el.stop().applyStyle(styleCSS, $.extend(true, [], { duration : speed + 'ms' }));
			}
		};

		//Mouse enter / Mouse leave
		$(target).on('mouseenter.hoverdir, mouseleave.hoverdir', function (event) {
			var $el = $(this),
			$hoverElem = $el.find('span.brackets'),
			direction = that._getDir($el, { x : event.pageX, y : event.pageY }),
			styleCSS = that._getStyle(direction);

			if (event.type === 'mouseenter') {
				$hoverElem.hide().css(styleCSS.from);
				$hoverElem.show(0, function () {
					var $el = $(this);
					if (that.support) {
						$el.css('transition', 'all 300ms ease');
					}
					that._applyAnimation($el, styleCSS.to, 300);
				});
			} else {
				if (that.support) {
					$hoverElem.css('transition', 'all 300ms ease');
				}
				that._applyAnimation($hoverElem, styleCSS.from, 300);
			}
		});
	},

	fixElementsHeight: function (target) {
		$(window).bind('resize load', function () {
			$(target).each(function () {
				$(this).height($(this).innerWidth());
			});
		});
	},

	paraibadisiacoTabs: function () {

		$('section.paraibadisiaco article:first').addClass('-active');

		$('section.paraibadisiaco article.-active').stop(false, false).animate(
			{ width : '50%' },
			{ duration : 300, easing : 'linear' }
			);

		$('section.paraibadisiaco article').bind('mouseover', function () {
			$('section.paraibadisiaco article').removeClass('-active');
			$(this).addClass('-active');

			$('section.paraibadisiaco article').stop(false, false).animate(
				{ width : '25%' },
				{ duration : 300, easing : 'linear' }
				);

			$(this).stop(false, false).animate(
				{ width : '50%' },
				{ duration : 300, easing : 'linear' }
				);
		});
	},

	destinationHover: function (target) {
		$(target).on('mouseenter mouseleave', function (event) {
			if (event.type === 'mouseenter') {
				$('a', this).stop(false, false).animate({ backgroundSize : '125%' }, 600);
			} else {
				$('a', this).stop(false, false).animate({ backgroundSize : '100%' }, 600);
			}
		});
	}
};

/*global $, Mushi, console */

Mushi.inclusive = {
	init: function () {
		console.log('All Inclusive started');
	}
};

/*global $, Mushi, console, google */

// Mushi.localizacao = {
// 	init: function () {
// 		console.log('Localização started');
// 		this.initMap('google-map');
// 		this.initTabs('.localizacao .map nav button');

// 	},

// 	initMap: function (target) {
// 		var map = new google.maps.Map(document.getElementById(target), {
// 			center: { lat: -7.31238, lng: -34.817111 },
// 			zoom: 8
// 		});
// 	},

// 	initTabs: function (target) {
// 		$(target).bind('click', function () {
// 			$(target).removeClass('-active');
// 			$(this).addClass('-active');
// 			Mushi.localizacao.initMap('google-map');
// 		});
// 	}
 
// };

/*global $, Mushi, console */

Mushi.pacotes = {
	init: function () {
		console.log('Pacotes started');
		this.accordionMenu();
	},

	accordionMenu: function () {
		$('.pacotes aside > ul > li > a').bind('click', function () {
			var submenu = $('ul.submenu', this.parentNode);
				//$('.pacotes aside > ul > li').removeClass('-active');

				$('.pacotes aside .submenu').stop(false, false).slideUp(500).delay(500).end().removeClass('-active');

				submenu.stop(false, false).parent().delay(500).addClass('-active');
				submenu.stop(false, false).hide().slideDown(500);

				return false;
			});
	}
};

/*global $, Mushi, console */

Mushi.resort = {
	init: function () {
		console.log('Resort started');
		this.tabsInit();
	},

	tabsInit : function () {
		$('.tabs article > header').bind('click', function () {
			var newHeight = $('.content', this.parentNode).innerHeight();

			$('.tabs .container').stop(false, false).animate({ height : 0 });

			$('.tabs article').removeClass('-active');
			$(this).parent().addClass('-active');

			$('article.-active .container').stop(false, false).animate({ height : newHeight }, function () {
				//$('body, html').animate({ scrollTop : $('article.-active').position().top - $('header.top').height() - $('form.reserva.-top').height() });
			});
		});
	}
};

$('.maps').click(function () {
    $('.maps iframe').css("pointer-events", "auto");
});

$( ".maps" ).mouseleave(function() {
  $('.maps iframe').css("pointer-events", "none"); 
});

window.onload=function() {

  // get tab container
  	var container = document.getElementById("tabContainer");
		var tabcon = document.getElementById("tabscontent");
		//alert(tabcon.childNodes.item(1));
    // set current tab
    var navitem = document.getElementById("tabHeader_1");
		
    //store which tab we are on
    var ident = navitem.id.split("_")[1];
		//alert(ident);
    navitem.parentNode.setAttribute("data-current",ident);
    //set current tab with class of activetabheader
    navitem.setAttribute("class","tabActiveHeader");

    //hide two tab contents we don't need
   	 var pages = tabcon.getElementsByTagName("div");
    	for (var i = 1; i < pages.length; i++) {
     	 pages.item(i).style.display="none";
		};

    //this adds click event to tabs
    var tabs = container.getElementsByTagName("li");
    for (var i = 0; i < tabs.length; i++) {
      tabs[i].onclick=displayPage;
    }
}

// on click of one of tabs
function displayPage() {
  var current = this.parentNode.getAttribute("data-current");
  //remove class of activetabheader and hide old contents
  document.getElementById("tabHeader_" + current).removeAttribute("class");
  document.getElementById("tabpage_" + current).style.display="none";

  var ident = this.id.split("_")[1];
  //add class of activetabheader to new active tab and show contents
  this.setAttribute("class","tabActiveHeader");
  document.getElementById("tabpage_" + ident).style.display="block";
  this.parentNode.setAttribute("data-current",ident);
}