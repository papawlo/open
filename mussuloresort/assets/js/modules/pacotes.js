/*global $, Mushi, console */

Mushi.pacotes = {
	init: function () {
		console.log('Pacotes started');
		this.accordionMenu();
	},

	accordionMenu: function () {
		$('.pacotes aside > ul > li > a').bind('click', function () {
			var submenu = $('ul.submenu', this.parentNode);
				//$('.pacotes aside > ul > li').removeClass('-active');

			$('.pacotes aside .submenu').stop(false, false).slideUp(500).delay(500).end().removeClass('-active');

			submenu.stop(false, false).parent().delay(500).addClass('-active');
			submenu.stop(false, false).hide().slideDown(500);

			return false;
		});
	}
};
