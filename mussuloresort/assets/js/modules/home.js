/*global $, Mushi, console, Swiper, self, Modernizr */

Mushi.home = {
	featuredSwiper : null,

	init: function () {
		console.log('Home started');
		this.swiperFeatured('section.featured');
		this.parallaxFeatured('section.featured .swiper-slide');
		this.paraibadisiacoTabs();
		this.fixElementsHeight('section.destination article');
		this.directionAwareHover('section.destination article');
		this.destinationHover('section.destination article');
	},

	swiperFeatured: function (target) {
		var options = {
			autoplay: 4000,
			loop: true,
			pagination: target + ' .swiper-pagination',
			paginationClickable: true,
			autoplayDisableOnInteraction: false,
			prevButton: target + ' button.prev',
			nextButton: target + ' button.next',
			onInit: function () {
				var i = 0,
					thumbUrl;
				$('.swiper-slide', this.parentNode).each(function () {
					thumbUrl = $(this).css('background-image');
					$('.swiper-pagination .swiper-pagination-bullet:eq(' + i + ')', this.parentNode.parentNode)
						.append('<span class="bg"></span>')
						.find('span.bg')
						.css('background-image', thumbUrl);
					i++;
				});
			}
		};

		this.featuredSwiper = new Swiper(target, options);
	},

	parallaxFeatured: function (target) {
		$(window).bind('scroll', function () {
			var bodyScroll = $('body').scrollTop();
			if (bodyScroll <= $(target).position().top + $(target).height()) {
				$(target).css('background-position', '50% ' + (50 - bodyScroll) + '%');
			}
		});
	},

	directionAwareHover: function (target) {
		var that = {
			support : Modernizr.csstransitions,
			_getDir : function ($el, coordinates) {
				// the width and height of the current div
				var w = $el.width(),
					h = $el.height(),

					// calculate the x and y to get an angle to the center of the div from that x and y.
					// gets the x value relative to the center of the DIV and "normalize" it
					x = (coordinates.x - $el.offset().left - (w / 2)) * (w > h ? (h / w) : 1),
					y = (coordinates.y - $el.offset().top  - (h / 2)) * (h > w ? (w / h) : 1),

					// the angle and the direction from where the mouse came in/went out clockwise (TRBL=0123);
					// first calculate the angle of the point,
					// add 180 deg to get rid of the negative values
					// divide by 90 to get the quadrant
					// add 3 and do a modulo by 4  to shift the quadrants to a proper clockwise TRBL (top/right/bottom/left) **/
					direction = Math.round((((Math.atan2(y, x) * (180 / Math.PI)) + 180) / 90) + 3) % 4;

				return direction;

			},

			_getStyle : function (direction) {
				var fromStyle, toStyle,
					slideFromTop = { left : '0px', top : '-100%' },
					slideFromBottom = { left : '0px', top : '100%' },
					slideFromLeft = { left : '-100%', top : '0px' },
					slideFromRight = { left : '100%', top : '0px' },
					slideTop = { top : '0px' },
					slideLeft = { left : '0px' },
					inverse = false;

				switch (direction) {
				case 0:
					// from top
					fromStyle = !inverse ? slideFromTop : slideFromBottom;
					toStyle = slideTop;
					break;
				case 1:
					// from right
					fromStyle = !inverse ? slideFromRight : slideFromLeft;
					toStyle = slideLeft;
					break;
				case 2:
					// from bottom
					fromStyle = !inverse ? slideFromBottom : slideFromTop;
					toStyle = slideTop;
					break;
				case 3:
					// from left
					fromStyle = !inverse ? slideFromLeft : slideFromRight;
					toStyle = slideLeft;
					break;
				}
				return { from : fromStyle, to : toStyle };
			},

			_applyAnimation : function (el, styleCSS, speed) {
				$.fn.applyStyle = this.support ? $.fn.css : $.fn.animate;
				el.stop().applyStyle(styleCSS, $.extend(true, [], { duration : speed + 'ms' }));
			}
		};

		//Mouse enter / Mouse leave
		$(target).on('mouseenter.hoverdir, mouseleave.hoverdir', function (event) {
			var $el = $(this),
				$hoverElem = $el.find('span.brackets'),
				direction = that._getDir($el, { x : event.pageX, y : event.pageY }),
				styleCSS = that._getStyle(direction);

			if (event.type === 'mouseenter') {
				$hoverElem.hide().css(styleCSS.from);
				$hoverElem.show(0, function () {
					var $el = $(this);
					if (that.support) {
						$el.css('transition', 'all 300ms ease');
					}
					that._applyAnimation($el, styleCSS.to, 300);
				});
			} else {
				if (that.support) {
					$hoverElem.css('transition', 'all 300ms ease');
				}
				that._applyAnimation($hoverElem, styleCSS.from, 300);
			}
		});
	},

	fixElementsHeight: function (target) {
		$(window).bind('resize load', function () {
			$(target).each(function () {
				$(this).height($(this).innerWidth());
			});
		});
	},

	paraibadisiacoTabs: function () {

		$('section.paraibadisiaco article:first').addClass('-active');

		$('section.paraibadisiaco article.-active').stop(false, false).animate(
			{ width : '50%' },
			{ duration : 300, easing : 'linear' }
		);

		$('section.paraibadisiaco article').bind('mouseover', function () {
			$('section.paraibadisiaco article').removeClass('-active');
			$(this).addClass('-active');

			$('section.paraibadisiaco article').stop(false, false).animate(
				{ width : '25%' },
				{ duration : 300, easing : 'linear' }
			);

			$(this).stop(false, false).animate(
				{ width : '50%' },
				{ duration : 300, easing : 'linear' }
			);
		});
	},

	destinationHover: function (target) {
		$(target).on('mouseenter mouseleave', function (event) {
			if (event.type === 'mouseenter') {
				$('a', this).stop(false, false).animate({ backgroundSize : '125%' }, 600);
			} else {
				$('a', this).stop(false, false).animate({ backgroundSize : '100%' }, 600);
			}
		});
	}
	
};
