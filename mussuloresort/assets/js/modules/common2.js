/*global $, Mushi, console, Swiper, moment */

Mushi.common = {
	init: function () {
		console.log('Mushi started');
		this.openMenu();
		this.reservaToggle();
		this.validateReserva('form.reserva.-top');
		this.validateNewsletter('form.newsletter');
		this.numberAnimate('form.reserva.-top .number');
		this.calendars('form.reserva .calendar-input');
		this.initGalleries('.gallery .swiper-container');
		this.innerParallax('.inner > header, .read > header');
		this.loginPanel('form.reserva.-top a.tour-agent');
	},

	openMenu: function () {
		$('header.top .btn-menu').bind('click', function () {
			$('.holder-menu').toggleClass('-active');
			return false;
		});
	},

	reservaToggle: function () {
		$('form.reserva.-top > .container > ul > li:first-child').bind('click', function () {
			if ($(window).width() <= 720) {
				if (!$('form.reserva.-top').hasClass('-active')) {
					$('form.reserva.-top').stop(false, false).animate({ height : $('form.reserva.-top > .container').height() }, function () {
						$('form.reserva.-top').addClass('-active');
					});
				} else {
					$('form.reserva.-top').stop(false, false).animate({ height : $('form.reserva.-top > .container > ul > li:first-child').height() }, function () {
						$('form.reserva.-top').removeClass('-active');
					});
				}
			}
		});
	},

	validateReserva: function (target) {
		var options = {
			errorElement : 'em',
			rules : {
				checkin : "required",
				checkout : "required"
			},
			messages : {
				checkin: "Selecione uma data",
				checkout: "Selecione uma data"
			}
		};
		$(target).validate(options);
	},

	validateNewsletter: function (target) {
		var options = {
			errorElement : 'em',
			rules : {
				email : {
					required : true,
					email : true
				}
			},
			messages : {
				email: "Por favor, preencha o seu e-mail"
			}
		};
		$(target).validate(options);
	},

	numberAnimate: function (target) {
		var up = $('button.up', target),
			down = $('button.down', target);

		up.bind('click', function () {
			var input = $('input[type="hidden"]', this.parentNode),
				mask = $('.mask span', this.parentNode),
				value = input.val();

			value++;

			input.val(value);

			$(mask).stop(false, false).animate({ top : '-200%' }, 100, function () {
				$(this).html(value);
				$(this).css('top', '200%');
				$(this).animate({ top : '50%' });
			});

			return false;
		});

		down.bind('click', function () {
			var input = $('input[type="hidden"]', this.parentNode),
				mask = $('.mask span', this.parentNode),
				value = input.val();

			if (value-- > 0) {
				input.val(value);
				$(mask).stop(false, false).animate({ top : '200%' }, 100, function () {
					$(this).html(value);
					$(this).css('top', '-200%');
					$(this).animate({ top : '50%' });
				});
			}

			return false;
		});
	},

	calendars: function (target) {
		var options_checkin = {
			clickEvents : {
				click : function (result) {
					$('.calendar-input', $('#calendar_checkin').parent()).val(result.date.format('L'));
					$('.calendar').removeClass('-active');
				}
			}
		},
			options_checkout = {
				clickEvents : {
					click : function (result) {
						$('.calendar-input', $('#calendar_checkout').parent()).val(result.date.format('L'));
						$('.calendar').removeClass('-active');
					}
				}
			};
		$('#calendar_checkin').clndr(options_checkin);
		$('#calendar_checkout').clndr(options_checkout);

		$(target).on('focus', function () {
			var parent = $(this).parent();
			$('.calendar').removeClass('-active');
			$('.calendar', parent).addClass('-active');
		});

		$('span', target.parentNode).on('click', function () {
			var parent = $(this).parent();
			$('.calendar').removeClass('-active');
			$('.calendar', parent).addClass('-active');
		});

	},

	initGalleries: function (target) {
		var i = 0;

		$(target).each(function () {
			var parent = $(this).parent(),
				index = i++,
				swiper,
				options = {
					loop: false,
					prevButton: $('button.prev', parent),
					nextButton: $('button.next', parent),
					slidesPerView : 4,
					spaceBetween : 20
				};

			$(window).on('load', function () {
				console.log($(this).width());
				if ($(this).width() <= 768) {
					options = {
						loop: false,
						prevButton: $('button.prev', parent),
						nextButton: $('button.next', parent),
						slidesPerView : 1,
						spaceBetween : 20
					};
					swiper = new Swiper($(target), options);
				}
			});

			swiper = new Swiper($(target), options);

			$('a', this).fancybox({
				overlayShow  : true,
				scrolling : 'yes'
			});
		});
	},

	innerParallax : function (target) {
		if ($(target).length > 0) {
			$(window).bind('scroll', function () {
				var bodyScroll = $('body').scrollTop();
				if (bodyScroll <= $(target).position().top + $(target).height()) {
					$(target).css('background-position', '50% ' + (50 - (bodyScroll / 5)) + '%');
				}
			});
		}
	},

	loginPanel : function (target) {
		$(target).bind('click', function (event) {
			if ($(event.target).hasClass('tour-agent') || $(event.target).is('strong')) {
				$(this).toggleClass('-active');
				return false;
			}
		});
	}

};
