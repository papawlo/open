/*global $, Mushi, console, google */

Mushi.localizacao = {
	init: function () {
		console.log('Localização started');
		this.initMap('google-map');
		this.initTabs('.localizacao .map nav button');
	},

	initMap: function (target) {
		var map = new google.maps.Map(document.getElementById(target), {
			center: { lat: -7.145679, lng: -34.9479617 },
			zoom: 3
		});
	},

	initTabs: function (target) {
		$(target).bind('click', function () {
			$(target).removeClass('-active');
			$(this).addClass('-active');
			Mushi.localizacao.initMap('google-map');
		});
	}
};
