/*global $, Mushi, console */

Mushi.contato = {
	init: function () {
		console.log('Contato started');
		this.validateContato('.contato form');
	},

	validateContato: function (target) {
		var options = {
			errorElement : 'em',
			rules : {
				nome: "required",
				mensagem: "required",
				email : {
					required : true,
					email : true
				}
			},
			messages : {
				nome: "Preencha o seu nome",
				email: "Preencha o seu e-mail",
				mensagem: "Preencha a sua mensagem"
			}
		};
		$(target).validate(options);
	}
};
