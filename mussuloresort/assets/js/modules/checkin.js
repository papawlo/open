/*global $, Mushi, console, _ */

Mushi.checkin = {
	init: function () {
		console.log('Checkin started');
		this.addFieldset('.checkin form');
		this.addPeople('.more-people');
		this.docTab('.tab-radio');
	},

	addFieldset : function (target) {
		var template = _.template($('#checkin-template').html());
		$('> ul', target).append('<li></li>').find('li').html(template);

		this.validateCheckin('.checkin form');
	},

	addPeople : function (target) {
		$(target).bind('click', function () {
			Mushi.checkin.addFieldset('.checkin form');
			var newElement = $('.checkin form > ul > li:last-child');

			newElement.addClass('-recent');
			$('body, html').animate({ scrollTop : $('.checkin form > ul > li:last-child').position().top - $('header.top').height() - $('form.reserva.-top').height() }, function () {
				newElement.removeClass('-recent');
			});
			return false;
		});
	},

	validateCheckin: function (target) {
		var options = {
			errorElement : 'em',
			rules : {
				'nome[]': "required",
				'email[]' : {
					required : true,
					email : true
				},
				'telefone[]': "required",
				'nascimento[]': "required",
				'sexo[]': "required",
				'nacionalidade[]': "required",
				'pais[]': "required",
				'estado[]': "required",
				'cidade[]': "required",
				'endereco[]': "required",
				'numero[]': "required"
			},
			messages : {
				'nome[]': "Preencha o seu nome",
				'email[]': "Preencha o seu e-mail",
				'telefone[]': "Preencha o seu telefone",
				'nascimento[]': "Preencha o campo",
				'sexo[]': "Escolha um genero",
				'nacionalidade[]': "Escolha uma nacionalidade",
				'pais[]': "Escolha um país",
				'estado[]': "Escolha um estado",
				'cidade[]': "Escolha uma cidade",
				'endereco[]': "Preencha um endereço",
				'numero[]': "Preencha um número"
			}
		};
		$(target).validate(options);
	},

	docTab: function (target) {
		var theHeight = $('.tabs').height();
		$('.tabs').height(theHeight);

		$(target).bind('change', function () {
			var field = $($(this).data('target')),
				parent = $(this.parentNode.parentNode.parentNode.parentNode);

			$('.tabs', parent).stop(false, false).animate({ opacity : 0 }, function () {
				$('ul.tab', parent).removeClass('-active');
				field.addClass('-active');

				$(this).stop(false, false).animate({ opacity : 1 });
			});
			return false;
		});
	}
};
