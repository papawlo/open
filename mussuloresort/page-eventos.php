<?php $_pagename = 'eventos'; include "header.php"; ?>
<?php $eventos = get_option( 'eventos' ); ?>
<?php $descricao_eventos = $eventos['descricao_eventos']; ?>
<?php $galeria_fotos = $eventos['galeria_fotos']; ?>
<?php $sala_1_titulo = $eventos['sala_1_titulo']; ?>
<?php $sala_1_descricao = $eventos['sala_1_descricao']; ?>
<?php $sala_2_titulo = $eventos['sala_2_titulo']; ?>
<?php $sala_2_descricao = $eventos['sala_2_descricao']; ?>
<?php $sala_3_titulo = $eventos['sala_3_titulo']; ?>
<?php $sala_3_descricao = $eventos['sala_3_descricao']; ?>
<?php $capas = get_option( 'capas' ); ?>
<?php $capa_grupos_eventos = $capas['capa_grupos_eventos']; ?>
<?php $image1 = wp_get_attachment_image_src( $capa_grupos_eventos, full ); ?>

	<section class="eventos inner">
		<header style="background-image: url(<?php echo $image1[0]; ?>)"	></header>
		<div class="container">
			<div class="title col-xs-12 col-sm-4 col-md-3">
				<h1>Grupos<br /> e Eventos</h1>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="post">
					<p><?php echo $descricao_eventos; ?></p>
				</div>
				<div class="gallery -desktop">
					<div class="swiper-container">
						<ul class="swiper-wrapper">
							<?php echo g_fotos($galeria_fotos); ?>
							<?php foreach ( explode( ',', $galeria_fotos ) as $image_id ) {
									$logo = wp_get_attachment_image_src( $image_id , full ); 
									?>
									<li class="swiper-slide">
										<a href="<?php echo $logo[0]; ?>">
											<img src="<?php echo $logo[0]; ?>" alt="">
										</a>
									</li>
							<?php	
								}  
							?>
						</ul>
					</div>
					<button class="prev"></button>
					<button class="next"></button>
				</div>
			</div>
		</div>

		<div class="salas">
			<article class="col-xs-12 col-sm-4">
				<h1><?php echo $sala_1_titulo; ?></h1>
				<?php echo $sala_1_descricao; ?>
			</article>
			<article class="col-xs-12 col-sm-4">
				<h1><?php echo $sala_2_titulo; ?></h2>
				<?php echo $sala_2_descricao; ?>
			</article>
			<article class="col-xs-12 col-sm-4">
				<h1><?php echo $sala_3_titulo; ?></h1>
				<?php echo $sala_3_descricao; ?>
			</article>
			<footer>
				<h1>Para maiores informações contate nossos assessores</h1>
				<a href="mailto:eventos@mussulobymantra.com.br">eventos@mussulobymantra.com.br</a>
				<span class="phone">+55 (83) 3298-2750</span>
			</footer>
		</div>
	</section>

<?php get_footer() ?>
