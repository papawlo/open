<?php
/**
 * Sets content width.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 600;
}

/**
 * Odin Classes.
 */
require_once get_template_directory() . '/core/classes/class-bootstrap-nav.php';
require_once get_template_directory() . '/core/classes/class-shortcodes.php';
require_once get_template_directory() . '/core/classes/class-thumbnail-resizer.php';
require_once get_template_directory() . '/core/classes/class-theme-options.php';
require_once get_template_directory() . '/core/classes/class-options-helper.php';
require_once get_template_directory() . '/core/classes/class-post-type.php';
require_once get_template_directory() . '/core/classes/class-taxonomy.php';
require_once get_template_directory() . '/core/classes/class-metabox.php';
require_once get_template_directory() . '/core/classes/abstracts/abstract-front-end-form.php';
require_once get_template_directory() . '/core/classes/class-contact-form.php';
require_once get_template_directory() . '/core/classes/class-post-form.php';
require_once get_template_directory() . '/core/classes/class-user-meta.php';

/**
 * Odin Widgets.
 */
require_once get_template_directory() . '/core/classes/widgets/class-widget-like-box.php';

if ( ! function_exists( 'odin_setup_features' ) ) {

	/**
	 * Setup theme features.
	 *
	 * @since  2.2.0
	 *
	 * @return void
	 */
	function odin_setup_features() {


		add_theme_support( 'post-thumbnails' );
		add_image_size( 'fullx475', 99999, 475, true );
		add_image_size( '1024x285', 1024, 285, true );
		add_image_size( '500x340', 500, 340, true );
		/**
		 * Add support for multiple languages.
		 */
		load_theme_textdomain( 'odin', get_template_directory() . '/languages' );

		/**
		 * Register nav menus.
		 */
		register_nav_menus(
			array(
				'main-menu' => __( 'Main Menu', 'odin' ),
				'menu-forms' => __( 'Menu Forms', 'odin' ),
				'menu-footer' => __( 'Menu Footer', 'odin' ),
			)
		);

		add_filter('single_template', function($original){
  global $post;
  $post_name = $post->post_name;
  $post_type = $post->post_type;
  $base_name = 'single-' . $post_type . '-' . $post_name . '.php';
  $template = locate_template($base_name);
  if ($template && ! empty($template)) return $template;
  return $original;
});

		/*
		 * Add post_thumbnails suport.
		 */
		add_theme_support( 'post-thumbnails' );

		/**
		 * Add feed link.
		 */
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Support Custom Editor Style.
		 */
		add_editor_style( 'assets/css/editor-style.css' );

		/**
		 * Add support for infinite scroll.
		 */
		add_theme_support(
			'infinite-scroll',
			array(
				'type'           => 'scroll',
				'footer_widgets' => false,
				'container'      => 'content',
				'wrapper'        => false,
				'render'         => false,
				'posts_per_page' => get_option( 'posts_per_page' )
			)
		);

		/**
		 * Support The Excerpt on pages.
		 */
		// add_post_type_support( 'page', 'excerpt' );

		/**
		 * Switch default core markup for search form, comment form, and comments to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption'
			)
		);

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
	}
}

add_action( 'after_setup_theme', 'odin_setup_features' );



/**
 * Flush Rewrite Rules for new CPTs and Taxonomies.
 *
 * @since  2.2.0
 *
 * @return void
 */
function odin_flush_rewrite() {
	flush_rewrite_rules();
}

add_action( 'after_switch_theme', 'odin_flush_rewrite' );



/* Módulos - CPTs e Taxonomias */
$template_modulos = 'inc/modulos/';

$banners = $template_modulos.'banners.php';
include($banners);

$galeria = $template_modulos.'galeria.php';
include($galeria);

$pacotes_promocoes = $template_modulos.'pacotes_promocoes.php';
include($pacotes_promocoes);

/*--opções----*/
$template_opcoes = 'inc/opcoes/extras-estaticos.php';
include($template_opcoes);

/*--forms----*/
$template_forms = 'inc/forms/';

$contato = $template_forms.'contato.php';
include($contato);



function g_fotos($galeria)
{
	if (strlen($galeria) > 0) {
		$quantidade = strlen($galeria) - 1;
		if ($galeria[$quantidade] == ",") {
			$explosion = explode(",", $galeria);
			for ($i = 0; $i < count($explosion)-1; $i++) {
				if (strlen($explosion[$i]) > 0) {
					if ($i < (count($explosion)-2) ) {
						$valor .= $explosion[$i] . ",";
					} else {								
						$valor .= $explosion[$i];
					}
				}
			}
			$galeria = $valor;
		}
	}
}


/*---- widgets -----*/
// function widgets_init() {

// 	register_sidebar( array(
// 		'name'          => 'Seção 1 - ERP',
// 		'id'            => 'section_1_erp',
// 		'before_widget' => '',
// 		'after_widget'  => '',
// 		'before_title'  => '<h2 class="title-section">',
// 		'after_title'   => '</h2>',
// 	) );

// 	/* **** kash **** */

// 	register_sidebar( array(
// 		'name'          => 'Seção 1 - Kash',
// 		'id'            => 'section_1_kash',
// 		'before_widget' => '',
// 		'after_widget'  => '',
// 		'before_title'  => '<h2 class="title-section">',
// 		'after_title'   => '</h2>',
// 	) );

// }
// add_action( 'widgets_init', 'widgets_init' );


/*--- Menus Personalizados---*/
function register_custom_menu_pages() {
	add_menu_page( 'Home Destino', 'Home Destino', 'manage_options', 'themes.php?page=extras-estaticos&tab=home_destino', '', 'dashicons-text', 4 );
	add_menu_page( 'Home Paraibadisiaco', 'Home Paraibadisiaco', 'manage_options', 'themes.php?page=extras-estaticos&tab=home_paraibadisiaco', '', 'dashicons-text', 5 );
	add_menu_page( 'O Resort', 'O Resort', 'manage_options', 'themes.php?page=extras-estaticos&tab=o_resort', '', 'dashicons-text', 6 );
	add_menu_page( 'All Incluse', 'All Incluse', 'manage_options', 'themes.php?page=extras-estaticos&tab=all_inclusive', '', 'dashicons-text', 7);
	add_menu_page( 'Acomodações', 'Acomodações', 'manage_options', 'themes.php?page=extras-estaticos&tab=acomodacoes', '', 'dashicons-text', 8 );
	add_menu_page( 'Localização', 'Localização', 'manage_options', 'themes.php?page=extras-estaticos&tab=localizacao', '', 'dashicons-text', 9);
	add_menu_page( 'Eventos', 'Eventos', 'manage_options', 'themes.php?page=extras-estaticos&tab=eventos', '', 'dashicons-text', 10);
	add_menu_page( 'Pacotes & Promoções', 'Pacotes & Promoções', 'manage_options', 'themes.php?page=extras-estaticos&tab=pacotes_promocoes', '', 'dashicons-text', 11);
	add_menu_page( 'Contato', 'Contato', 'manage_options', 'themes.php?page=extras-estaticos&tab=contato', '', 'dashicons-text', 12);
	add_menu_page( 'Capas', 'Capas', 'manage_options', 'themes.php?page=extras-estaticos&tab=capas', '', 'dashicons-text', 13);
}

add_action( 'admin_menu', 'register_custom_menu_pages' );

/**
 * Load site scripts.
 *
 * @since  2.2.0
 *
 * @return void
 */
function odin_enqueue_scripts() {
	$template_url = get_template_directory_uri();

	// Loads Odin main stylesheet.
//	wp_enqueue_style( 'odin-style', get_stylesheet_uri(), array(), null, 'all' );
	// wp_enqueue_style( 'bootstrap-css', $template_url . '/assets/js/vendor/bootstrap/bootstrap.css', array(), null, 'all' );
	// wp_enqueue_style( 'main-css', $template_url . '/assets/css/main.css', array(), null, 'all' );
	// wp_enqueue_style( 'bxslider-css', $template_url . '/assets/js/plugins/bx-slider/jquery.bxslider.css', array(), null, 'all' );
		
	// jQuery.
	// wp_enqueue_script( 'jquery-js', $template_url . '/assets/js/vendor/jquery.js', array(), null, true );
 //  wp_enqueue_script( 'modernizr-js', $template_url . '/assets/js/vendor/custom.modernizr.js', array(), null, true );
 //  wp_enqueue_script( 'nwmatcher-js', $template_url . '/assets/js/vendor/nwmatcher-1.2.5-min.js', array(), null, true );
 //  wp_enqueue_script( 'selectivizr-js', $template_url . '/assets/js/vendor/selectivizr-min.js', array(), null, true );
 //  wp_enqueue_script( 'bootstrap-js', $template_url . '/assets/js/vendor/bootstrap.min.js', array(), null, true );
 //  wp_enqueue_script( 'smoothscroll-js', $template_url . '/assets/js/vendor/smoothscroll.js', array(), null, true );
 //  wp_enqueue_script( 'placeholder-js', $template_url . '/assets/js/vendor/jquery.placeholder.js', array(), null, true );
 //  wp_enqueue_script( 'bxslider-js', $template_url . '/assets/js/plugins/bx-slider/jquery.bxslider.min.js', array(), null, true );
 //  wp_enqueue_script( 'script-js', $template_url . '/assets/js/scripts.js', array(), null, true );
  

	// Load Thread comments WordPress script.
	if ( is_singular() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'odin_enqueue_scripts', 1 );

/**
 * Odin custom stylesheet URI.
 *
 * @since  2.2.0
 *
 * @param  string $uri Default URI.
 * @param  string $dir Stylesheet directory URI.
 *
 * @return string      New URI.
 */
function odin_stylesheet_uri( $uri, $dir ) {
	return $dir . '/assets/css/style.css';
}

add_filter( 'stylesheet_uri', 'odin_stylesheet_uri', 10, 2 );

/**
 * Core Helpers.
 */
require_once get_template_directory() . '/core/helpers.php';

/**
 * WP Custom Admin.
 */
require_once get_template_directory() . '/inc/admin.php';

/**
 * Comments loop.
 */
require_once get_template_directory() . '/inc/comments-loop.php';

/**
 * WP optimize functions.
 */
require_once get_template_directory() . '/inc/optimize.php';

/**
 * Custom template tags.
 */
require_once get_template_directory() . '/inc/template-tags.php';
