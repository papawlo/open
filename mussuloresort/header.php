<!doctype html>
	<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
	<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
	<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
	<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php if (is_home()) {
			echo ' Mussulo Resort  - Seu Resort All Inclusive ';
		} else{ echo the_title().' - Mussulo Resort'; } ?></title>
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_url') ?>/favicon.png">
		<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/mushi.css?v=2">
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_TDotUkf7YaKVnAAx7h5wjMO2lcoamJc"></script>
		<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/mushi.js"></script>
		<link rel="shortcut icon" type="image/png" href="<?php bloginfo('template_url') ?>/assets/img/favicon.ico"/>
		<?php wp_head(); ?>
		<script> (function(i, s, o, g, r, a, m) { i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function() { (i[r].q = i[r].q || []).push(arguments) }, i[r].l = 1 * new Date(); a = s.createElement(o), m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m) })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga'); ga('create', 'UA-53623404-1', 'auto'); ga('send', 'pageview'); </script>
	</head>
	<body rel="<?php echo $_pagename ?>">
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

		<?php get_template_part('inc/top'); ?>
		<?php get_template_part('inc/top', 'reserva'); ?>
