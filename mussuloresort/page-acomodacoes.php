<?php $_pagename = 'resort'; include "header.php"; ?>
<?php $capas = get_option( 'capas' ); ?>
<?php $acomodacoes = get_option( 'acomodacoes' ); ?>
<?php $capa_acomodacoes = $capas['capa_acomodacoes']; ?>
<?php $descricao = $acomodacoes['descricao']; ?>
<?php $desc_geral_bangalo = $acomodacoes['desc_geral_bangalo']; ?>
<?php $dimensao_deluxe = $acomodacoes['dimensao_deluxe']; ?>
<?php $hospedes_deluxe = $acomodacoes['hospedes_deluxe']; ?>
<?php $descricao_deluxe = $acomodacoes['descricao_deluxe']; ?>
<?php $fotos_deluxe = $acomodacoes['fotos_deluxe']; ?>
<?php $background_deluxe = $acomodacoes['background_deluxe']; ?>
<?php $up_background_deluxe = $acomodacoes['up_background_deluxe']; ?>
<?php echo g_fotos($fotos_deluxe); ?>
<?php $dimensao_master = $acomodacoes['dimensao_master']; ?>
<?php $hospedes_master = $acomodacoes['hospedes_master']; ?>
<?php $descricao_master = $acomodacoes['descricao_master']; ?>
<?php $fotos_master = $acomodacoes['fotos_master']; ?>
<?php $background_master = $acomodacoes['background_master']; ?>
<?php $up_background_master = $acomodacoes['up_background_master']; ?>
<?php echo g_fotos($fotos_master); ?>
<?php $image1 = wp_get_attachment_image_src( $capa_acomodacoes, full ); ?>
<?php $image2 = wp_get_attachment_image_src( $background_deluxe, full ); ?>
<?php $image3 = wp_get_attachment_image_src( $up_background_deluxe, full ); ?>
<?php $image4 = wp_get_attachment_image_src( $background_master, full ); ?>
<?php $image5 = wp_get_attachment_image_src( $up_background_master, full ); ?>


	<section class="resort acomodacoes inner">
		<header style="background-image: url(<?php echo $image1[0]; ?>)"	></header>
		<div class="container">
			<div class="title col-xs-12 col-sm-4 col-md-3">
				<h1>Acomodações</h1>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="post">
					<p><?php echo $descricao; ?></p>
					<h4>Todos os bangalôs possuem:</h4>
					<ul>
						<?php echo $desc_geral_bangalo; ?>
					</ul>
				</div>
			</div>
		</div>
					
					<img src="<?php echo $image[0]; ?>" alt="<?php echo $acomodacoes['titulo1'];  ?>" class="img-responsive">

		<div class="areas tabs">
			<h1>Veja nossos Bangalôs</h1>
			<article class="-active">
				<header style="background-image: url(<?php echo $image2[0]; ?>)">
					<img src="<?php echo $image3[0]; ?>" alt="Beach Club">
					<span class="brackets -top"></span>
					<span class="brackets -bottom"></span>
				</header>
				<div class="container">
					<div class="content col-xs-12 col-md-8 col-md-offset-2">
						<h2>Deluxe</h2>
						<div class="post">
						</div>
						<ul class="espec">
							<li class="area col-xs-12 col-sm-4">
								<span><?php echo $dimensao_deluxe; ?></span>
								<span>m<strong style="font-family:lato">²</strong></span>
							</li>
							<li class="capacidade col-xs-12 col-sm-4">
								<span>Capacidade</span>
								<span><?php echo $hospedes_deluxe; ?></span>
								<span>Hospedes</span>
							</li>
							<li class="estrutura col-xs-12 col-sm-4">
								<?php echo $descricao_deluxe; ?>
							</li>
						</ul>
						<div class="gallery -desktop">
							<div class="swiper-container">
								<ul class="swiper-wrapper">
									<?php
										foreach ( explode( ',', $fotos_deluxe ) as $image_id ) {
									?>
										<?php $logo = wp_get_attachment_image_src( $image_id , full ); ?>
										<li class="swiper-slide">
										<a href="<?php echo $logo[0]; ?>">
											<img src="<?php echo $logo[0]; ?>" alt="<?php the_title(); ?>">
										</a>
									</li>
								
									<?php } ?>
								</ul>
							</div>
							<button class="prev"></button>
							<button class="next"></button>
						</div>
					</div>
				</div>
			</article>

			<article>
				<header style="background-image: url(<?php echo $image4[0]; ?>)">
					<img src="<?php echo $image5[0]; ?>" alt="Beach Club">
					<span class="brackets -top"></span>
					<span class="brackets -bottom"></span>
				</header>
				<div class="container">
					<div class="content col-xs-12 col-md-8 col-md-offset-2">
						<h2>Master</h2>
						<div class="post">
						</div>
						<ul class="espec">
							<li class="area col-xs-12 col-sm-4">
								<span><?php echo $dimensao_master; ?></span>
								<span>m<strong style="font-family:lato">²</strong></span>
							</li>
							<li class="capacidade col-xs-12 col-sm-4">
								<span>Capacidade</span>
								<span><?php echo $hospedes_master; ?></span>
								<span>Hospedes</span>
							</li>
							<li class="estrutura col-xs-12 col-sm-4">
								<?php echo $descricao_master; ?>
							</li>
						</ul>
						<div class="gallery -desktop">
							<div class="swiper-container">
								<ul class="swiper-wrapper">
									<?php
										foreach ( explode( ',', $fotos_master ) as $image_id ) {
									?>
										<?php $logo = wp_get_attachment_image_src( $image_id , full ); ?>
										<li class="swiper-slide">
										<a href="<?php echo $logo[0]; ?>">
											<img src="<?php echo $logo[0]; ?>" alt="<?php the_title(); ?>">
										</a>
									</li>
								
									<?php } ?>
								</ul>
							</div>
							<button class="prev"></button>
							<button class="next"></button>
						</div>
					</div>
				</div>
			</article>

		</div>
	</section>

<?php get_footer() ?>
