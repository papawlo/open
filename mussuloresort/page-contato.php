<?php $_pagename = 'contato'; include "header.php"; ?>
<?php $contato = get_option( 'contato' ); ?>
<?php $endereco = $contato['endereco']; ?>
<?php $telefone = $contato['telefone']; ?>
<?php $fax = $contato['fax']; ?>
<?php $reservas_individuais = $contato['reservas_individuais']; ?>
<?php $reservas_grupos = $contato['reservas_grupos']; ?>
<?php $central_atendimento = $contato['central_atendimento']; ?>
<?php $rh = $contato['rh']; ?>
<?php $marketing = $contato['marketing']; ?>
<?php $gerencia_geral = $contato['gerencia_geral']; ?>
<?php $capas = get_option( 'capas' ); ?>
<?php $capa_contato = $capas['capa_contato']; ?>
<?php $image1 = wp_get_attachment_image_src( $capa_contato, full ); ?>

	<section class="contato inner">
		<header style="background-image: url(<?php echo $image1[0]; ?>)"	></header>
		<div class="title col-xs-12 col-sm-4 col-md-3">
			<h1>Contato</h1>
		</div>
		<div class="col-xs-12 col-sm-8 col-md-9">
			<div class="address">
				<h2>Mussulo Resort by Mantra</h2>
				<p><?php echo $endereco; ?></p>
				<p><strong>Tel.:</strong> <?php echo $telefone; ?><br /> <strong>Fax:</strong> <?php echo $fax; ?></p>
				<ul class="emails row">
					<li class="col-xs-12 col-sm-6">
						<h3>Reservas individuais</h3>
						<span><?php echo $reservas_individuais; ?></span>
					</li>
					<li class="col-xs-12 col-sm-6">
						<h3>RH</h3>
						<span><?php echo $rh; ?></span>
					</li>
					<li class="col-xs-12 col-sm-6">
						<h3>Reservas Grupos e Eventos</h3>
						<span><?php echo $reservas_grupos; ?></span>
					</li>
					<li class="col-xs-12 col-sm-6">
						<h3>Marketing</h3>
						<span><?php echo $marketing; ?></span>
					</li>
					<li class="col-xs-12 col-sm-6">
						<h3>Central de Atendimento</h3>
						<span><?php echo $central_atendimento; ?></span>
					</li>
					<li class="col-xs-12 col-sm-6">
						<h3>Gerencia geral</h3>
						<span><?php echo $gerencia_geral; ?></span>
					</li>
				</ul>
			</div>
		</div>
		
			<h1>Fale Conosco</h1>
			<?php
if(isset($_POST['contato_email'])) {
   /* ------------------------------------------------ */
    if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($_POST['contato_email']))) {
        $hasError = true;
    } else {
        $email= trim($_POST['contato_email']);
    }
    $contato_nome = $_POST['contato_nome'];
    $contato_telefone = $_POST['contato_telefone'];
    $contato_assunto = $_POST['contato_assunto'];
    $contato_mensagem = $_POST['contato_mensagem'];
    /* ------------------------------------------------ */
    if(!isset($hasError)) {
        if (!isset($emailTo) || ($emailTo == '') ){
            $emailTo = "bruno@modernizr.com.br";
        }
        $subject = 'Mala Direta - Site Mussulo';
        $body = "Nome: $contato_nome  \n\n Email: $email \n\n Telefone: $contato_telefone \n\n Assunto: $contato_assunto \n\n mensagem: $contato_mensagem \n\n Enviado Através do formulário de Contato";
        $headers = 'From: bruno@modernizr.com.br <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;

        wp_mail($emailTo, $subject, $body, $headers);
        $emailSent = true;
        echo '<div class="alert alert-success" role="alert" style="padding:10px 0 5px 0;margin:0;width: 100%;float: left; text-align:center; position:absolute; top:135px;"><p class="container">Contato realizado com sucesso!<p></div>';
    }
}
?>
<form action="#" method="post">
			<ul class="col-xs-12 col-sm-8 col-sm-offset-4 col-md-offset-3">
				<li class="col-xs-12 col-sm-6">
					<label for="contato_nome">Nome:</label>
					<input type="text" id="contato_nome" name="contato_nome">
				</li>
				<li class="col-xs-12 col-sm-6">
					<label for="contato_email">Email:</label>
					<input type="email" id="contato_email" name="contato_email">
				</li>
				<li class="col-xs-12 col-sm-6">
					<label for="contato_telefone">Telefone:</label>
					<input type="text" id="contato_telefone" name="contato_telefone">
				</li>
				<li class="col-xs-12 col-sm-6">
					<label for="contato_assunto">Assunto:</label>
					<input type="text" id="contato_assunto" name="contato_assunto">
				</li>
				<li class="col-xs-12">
					<label for="contato_mensagem">Mensagem:</label>
					<textarea name="contato_mensagem" id="contato_mensagem"></textarea>
				</li>
				<li class="col-xs-12">
					<input type="submit" name="send" value="Enviar" class="button-default">
				</li>
			</ul>
		</form>
	</section>

<?php get_footer() ?>
