<footer class="footer">
	<h1 class="partners"><a href="http://www.mantra-group.com/operaciones/hospitality/mussulo-resort" alt="Mussolo Resort" target="_blank"><img src="<?php bloginfo('template_url') ?>/assets/img/mussulo-resort.png" alt="Mussulo Resort, Mantra Group"></a><a href="http://www.mantra-group.com/" alt="Mantra Group" target="_blank"><img src="<?php bloginfo('template_url') ?>/assets/img/mantra.png" alt=""></a></h1>

	<p>Litoral Sul da Paraíba, Município de Conde, Brasil</p>
	<nav class="social">
		<?php get_template_part('inc/nav', 'social'); ?>
	</nav>
</footer>
<style type="text/css">
<!--
#goog-gt-tt {display:none !important;}
.goog-te-banner-frame {display:none !important;}
.goog-te-menu-value:hover {text-decoration:none !important;}
body {top:0 !important;}
#google_translate_element2 {display:none!important;}
-->
</style>

<div id="google_translate_element2"></div>
<script type="text/javascript">
function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'pt',autoDisplay: false}, 'google_translate_element2');}
</script><script type="text/javascript" src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>


<script type="text/javascript">
/* <![CDATA[ */
function GTranslateFireEvent(element,event){try{if(document.createEventObject){var evt=document.createEventObject();element.fireEvent('on'+event,evt)}else{var evt=document.createEvent('HTMLEvents');evt.initEvent(event,true,true);element.dispatchEvent(evt)}}catch(e){}}function doGTranslate(lang_pair){if(lang_pair.value)lang_pair=lang_pair.value;if(lang_pair=='')return;var lang=lang_pair.split('|')[1];var teCombo;var sel=document.getElementsByTagName('select');for(var i=0;i<sel.length;i++)if(sel[i].className=='goog-te-combo')teCombo=sel[i];if(document.getElementById('google_translate_element2')==null||document.getElementById('google_translate_element2').innerHTML.length==0||teCombo.length==0||teCombo.innerHTML.length==0){setTimeout(function(){doGTranslate(lang_pair)},500)}else{teCombo.value=lang;GTranslateFireEvent(teCombo,'change');GTranslateFireEvent(teCombo,'change')}}
/* ]]> */
</script>

