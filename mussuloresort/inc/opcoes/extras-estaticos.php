<?php
// Include the Odin_Theme_Options class.
require_once get_template_directory() . '/core/classes/class-theme-options.php';

function odin_theme_settings_example() {

    $settings = new Odin_Theme_Options(
        'extras-estaticos', // Slug/ID of the Settings Page (Required)
        'Dados Extras', // Settings page name (Required)
        'manage_options' // Page capability (Optional) [default is manage_options]
    );

    $settings->set_tabs(
        array(
            array(
                'id' => 'home_destino', // Slug/ID of the Settings tab (Required)
                'title' => __( 'Home Destino', 'odin' ), // Settings tab title (Required)
            ),
            array(
                'id' => 'home_paraibadisiaco', // Slug/ID of the Settings tab (Required)
                'title' => __( 'Home Paraibadisiaco', 'odin' ), // Settings tab title (Required)
            ),
            array(
                'id' => 'o_resort', // Slug/ID of the Settings tab (Required)
                'title' => __( 'O Resort', 'odin' ), // Settings tab title (Required)
            ),
            array(
                'id' => 'all_inclusive',
                'title' => __( 'All Inclusive', 'odin' )
            ),
            array(
                'id' => 'acomodacoes',
                'title' => __( 'Acomodações', 'odin' )
            ),
            array(
                'id' => 'localizacao',
                'title' => __( 'Localização', 'odin' )
            ),
            array(
                'id' => 'eventos',
                'title' => __( 'Eventos', 'odin' )
            ),
            array(
                'id' => 'pacotes_promocoes',
                'title' => __( 'Pacotes & Promoções', 'odin' )
            ),
            array(
                'id' => 'contato',
                'title' => __( 'Contato', 'odin' )
            ),
            array(
                'id' => 'capas',
                'title' => __( 'Capas', 'odin' )
            ),
        )
    );

    $settings->set_fields(
        array(

            'home_destino_section' => array( // Slug/ID of the section (Required)
                'tab'   => 'home_destino', // Tab ID/Slug (Required)
                'title' => __( 'Home Destino', 'odin' ), // Section title (Required)
                'fields' => array( // Section fields (Required)


                    array(
                        'id'            => 'pre_titulo', // Obrigatório
                        'label'         => __( 'Pré Título', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Insira o título' )
                        ),
                        'description' => __( 'Pré título da home destino', 'odin' ), // Opcional
                    ),
                    array(
                        'id'            => 'titulo_destino', // Obrigatório
                        'label'         => __( 'Título Destino', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Insira o título' )
                        ),
                        'description' => __( 'Título da home destino', 'odin' ), // Opcional
                    ),    
                    array(
                        'id'          => 'background_all_inclusive', // Obrigatório
                        'label'       => __( 'Background All Inclusive', 'odin' ), // Obrigatório
                        'type'        => 'image', // Obrigatório
                        'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                        'description' => __( 'Insira o background do All Inclusive', 'odin' ), // Opcional
                    ),
                    array(
                        'id'          => 'sobreposto_all_inclusive', // Obrigatório
                        'label'       => __( 'Sobreposto All Inclusive', 'odin' ), // Obrigatório
                        'type'        => 'image', // Obrigatório
                        'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                        'description' => __( 'Insira o sobreposto do All Inclusive', 'odin' ), // Opcional
                    ),
                    array(
                        'id'          => 'background_o_resort', // Obrigatório
                        'label'       => __( 'Background O Resort', 'odin' ), // Obrigatório
                        'type'        => 'image', // Obrigatório
                        'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                        'description' => __( 'Insira o background do O Resort', 'odin' ), // Opcional
                    ),
                    array(
                        'id'          => 'sobreposto_o_resort', // Obrigatório
                        'label'       => __( 'Sobreposto O Resort', 'odin' ), // Obrigatório
                        'type'        => 'image', // Obrigatório
                        'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                        'description' => __( 'Insira o sobreposto do O Resort', 'odin' ), // Opcional
                    ),
                    array(
                        'id'          => 'background_acomodacoes', // Obrigatório
                        'label'       => __( 'Background Acomodações', 'odin' ), // Obrigatório
                        'type'        => 'image', // Obrigatório
                        'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                        'description' => __( 'Insira o background do Acomodações', 'odin' ), // Opcional
                    ),
                    array(
                        'id'          => 'sobreposto_acomodacoes', // Obrigatório
                        'label'       => __( 'Sobreposto Acomodações', 'odin' ), // Obrigatório
                        'type'        => 'image', // Obrigatório
                        'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                        'description' => __( 'Insira o sobreposto do Acomodações', 'odin' ), // Opcional
                    ),
                )
            ),
            'home_paraibadisiaco_section' => array( // Slug/ID of the section (Required)
                'tab'   => 'home_paraibadisiaco', // Tab ID/Slug (Required)
                'title' => __( 'Home Paraibadisiaco', 'odin' ), // Section title (Required)
                'fields' => array( // Section fields (Required)


                    array(
                        'id'            => 'titulo_paraibadisiaco', // Obrigatório
                        'label'         => __( 'Título Paraibadisiaco', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Insira o título' )
                        ),
                        'description' => __( 'Título da home paraibadisiaco', 'odin' ), // Opcional
                    ),
                    array(
                        'id'            => 'pos_titulo_1', // Obrigatório
                        'label'         => __( 'Pós Título 1', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Insira o título' )
                        ),
                        'description' => __( 'Pós título 1 da home paraibadisiaco', 'odin' ), // Opcional
                    ),
                    array(
                        'id'            => 'pos_titulo_2', // Obrigatório
                        'label'         => __( 'Pós Título 2', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Insira o título' )
                        ),
                        'description' => __( 'Pós título 2 da home paraibadisiaco', 'odin' ), // Opcional
                    ),
                    array(
                        'id'            => 'pos_titulo_3', // Obrigatório
                        'label'         => __( 'Pós Título 3', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Insira o título' )
                        ),
                        'description' => __( 'Pós título 3 da home paraibadisiaco', 'odin' ), // Opcional
                    ),
                )
            ),
            'o_resort_section' => array( // Slug/ID of the section (Required)
                'tab'   => 'o_resort', // Tab ID/Slug (Required)
                'title' => __( 'O Resort', 'odin' ), // Section title (Required)
                'fields' => array( // Section fields (Required)

                    
                    array(
                        'id'            => 'titulo_o_resort', // Obrigatório
                        'label'         => __( 'Título O Resort', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Título da Área O Resort' )
                        ),
                    ),
                    array(
                        'id'            => 'descricao_o_resort', // Obrigatório
                        'label'         => __( 'Descrição o Resort', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Descreva aqui a o Resort' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Descrição da Área o Resort', 'odin' ), // Opcional
                    ),
                    array(
                        'id'          => 'galeria_o_resort', // Required
                        'label'       => __( 'Galeria do Resort', 'odin' ), // Required
                        'type'        => 'image_plupload', // Required
                        // 'attributes' => array(), // Optional (html input elements)
                        // 'default'    => '', // Optional (file url)
                        'description' => __( 'Adicione imagens à Área o Resort', 'odin' ), // Optional
                    ),
                    array(
                        'id'          => 'banner_beach_club', // Obrigatório
                        'label'       => __( 'Banner Beach Club', 'odin' ), // Obrigatório
                        'type'        => 'image', // Obrigatório
                        'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                        'description' => __( 'Insira a imagem do banner beach club', 'odin' ), // Opcional
                    ),

                    array(
                        'id'          => 'up_banner_beach_club', // Obrigatório
                        'label'       => __( 'Sobreposto Banner Beach Club', 'odin' ), // Obrigatório
                        'type'        => 'image', // Obrigatório
                        'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                        'description' => __( 'Insira a imagem acima do banner beach club', 'odin' ), // Opcional
                    ),
                    array(
                        'id'            => 'titulo_beach_club', // Obrigatório
                        'label'         => __( 'Título Beach Club', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Título da Área Beach Club' )
                        ),
                    ),
                     array(
                        'id'            => 'descricao_beach_club', // Obrigatório
                        'label'         => __( 'Descrição Beach Club', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Descreva aqui a Área Beach Club' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Descrição da Área Beach Club', 'odin' ), // Opcional
                    ),
                    array(
                        'id'          => 'galeria_beach_club', // Required
                        'label'       => __( 'Galeria Beach Club', 'odin' ), // Required
                        'type'        => 'image_plupload', // Required
                        // 'attributes' => array(), // Optional (html input elements)
                        // 'default'    => '', // Optional (file url)
                        'description' => __( 'Adicione imagens à Área Beach Club', 'odin' ), // Optional
                    ),
                    array(
                        'id'          => 'banner_kids_club', // Obrigatório
                        'label'       => __( 'Banner Kids Club', 'odin' ), // Obrigatório
                        'type'        => 'image', // Obrigatório
                        'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                        'description' => __( 'Insira a imagem do banner kids club', 'odin' ), // Opcional
                    ),

                    array(
                        'id'          => 'up_banner_kids_club', // Obrigatório
                        'label'       => __( 'Sobreposto Banner Kids Club', 'odin' ), // Obrigatório
                        'type'        => 'image', // Obrigatório
                        'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                        'description' => __( 'Insira a imagem acima do banner kids club', 'odin' ), // Opcional
                    ),
                    array(
                        'id'            => 'titulo_kids_club', // Obrigatório
                        'label'         => __( 'Título Kids Club', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Título da Área Kids Club' )
                        ),
                    ),
                    array(
                        'id'            => 'descricao_kids_club', // Obrigatório
                        'label'         => __( 'Descrição Kids Club', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Descreva aqui a Área Kids Club' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Descrição da Área Kids Club', 'odin' ), // Opcional
                    ),
                    array(
                        'id'          => 'galeria_kids_club', // Required
                        'label'       => __( 'Galeria Kids Club', 'odin' ), // Required
                        'type'        => 'image_plupload', // Required
                        // 'attributes' => array(), // Optional (html input elements)
                        // 'default'    => '', // Optional (file url)
                        'description' => __( 'Adicione imagens à Área Kids Club', 'odin' ), // Optional
                    ),
                    array(
                        'id'          => 'banner_health_club', // Obrigatório
                        'label'       => __( 'Banner Health Club', 'odin' ), // Obrigatório
                        'type'        => 'image', // Obrigatório
                        'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                        'description' => __( 'Insira a imagem do banner health club', 'odin' ), // Opcional
                    ),

                    array(
                        'id'          => 'up_banner_health_club', // Obrigatório
                        'label'       => __( 'Sobreposto Banner Health Club', 'odin' ), // Obrigatório
                        'type'        => 'image', // Obrigatório
                        'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                        'description' => __( 'Insira a imagem acima do banner health club', 'odin' ), // Opcional
                    ),
                    array(
                        'id'            => 'titulo_health_club', // Obrigatório
                        'label'         => __( 'Título Health Club', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Título da Área Health Club' )
                        ),
                    ),
                    array(
                        'id'            => 'descricao_health_club', // Obrigatório
                        'label'         => __( 'Descrição Health Club', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Descreva aqui a Área Health Club' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Descrição da Área Health Club', 'odin' ), // Opcional
                    ),
                    array(
                        'id'          => 'galeria_health_club', // Required
                        'label'       => __( 'Galeria Health Club', 'odin' ), // Required
                        'type'        => 'image_plupload', // Required
                        // 'attributes' => array(), // Optional (html input elements)
                        // 'default'    => '', // Optional (file url)
                        'description' => __( 'Adicione imagens à Área Health Club', 'odin' ), // Optional
                    ),
                )
            ),
            'all_inclusive_section' => array( // Slug/ID of the section (Required)
                'tab'   => 'all_inclusive', // Tab ID/Slug (Required)
                'title' => __( 'All Inclusive', 'odin' ), // Section title (Required)
                'fields' => array( // Section fields (Required)

                    array(
                        'id'            => 'descricao_all_inclusive', // Obrigatório
                        'label'         => __( 'Descrição All Inclusive', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Descreva aqui as all inclusive' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Descrição da all inclusive', 'odin' ), // Opcional
                    ),
                    array(
                        'id'          => 'background_banner', // Obrigatório
                        'label'       => __( 'Background Banner', 'odin' ), // Obrigatório
                        'type'        => 'image', // Obrigatório
                        'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                        'description' => __( 'Insira a imagem do background do banner', 'odin' ), // Opcional
                    ),

                    array(
                        'id'          => 'up_background_banner', // Obrigatório
                        'label'       => __( 'Sobreposto Background Banner', 'odin' ), // Obrigatório
                        'type'        => 'image', // Obrigatório
                        'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                        'description' => __( 'Insira a imagem acima do background do banner', 'odin' ), // Opcional
                    ),
                    array(
                        'id'            => 'descricao_restaurante', // Obrigatório
                        'label'         => __( 'Descrição Restaurante', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Descreva aqui o restaurante' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Descrição do restaurante', 'odin' ), // Opcional
                    ),
                    array(
                        'id'          => 'galeria_fotos', // Required
                        'label'       => __( 'Galeria de Fotos', 'odin' ), // Required
                        'type'        => 'image_plupload', // Required
                        // 'attributes' => array(), // Optional (html input elements)
                        // 'default'    => '', // Optional (file url)
                        'description' => __( 'Adicione imagens à Galeria', 'odin' ), // Optional
                    ),
                )
            ),
            'acomodacoes_section' => array( // Slug/ID of the section (Required)
                'tab'   => 'acomodacoes', // Tab ID/Slug (Required)
                'title' => __( 'Acomodações', 'odin' ), // Section title (Required)
                'fields' => array( // Section fields (Required)

                    array(
                        'id'            => 'descricao', // Obrigatório
                        'label'         => __( 'Descrição', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Descreva aqui a acomodação' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Descrição da Acomodação', 'odin' ), // Opcional
                    ),

                    array(
                        'id'            => 'desc_geral_bangalo', // Obrigatório
                        'label'         => __( 'Descrição Geral Bangalo', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Os bangalôs possuem...' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Itens do Bangalô', 'odin' ), // Opcional
                    ),

                    array(
                        'id'          => 'background_deluxe', // Obrigatório
                        'label'       => __( 'Background Deluxe', 'odin' ), // Obrigatório
                        'type'        => 'image', // Obrigatório
                        'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                        'description' => __( 'Insira a imagem do background', 'odin' ), // Opcional
                    ),

                    array(
                        'id'          => 'up_background_deluxe', // Obrigatório
                        'label'       => __( 'Sobreposto Background Deluxe', 'odin' ), // Obrigatório
                        'type'        => 'image', // Obrigatório
                        'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                        'description' => __( 'Insira a imagem acima do background', 'odin' ), // Opcional
                    ),

                    array(
                        'id'          => 'dimensao_deluxe', // Obrigatório
                        'label'       => __( 'Dimensão Bangalô Deluxe', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Possui ... m²!' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Dimensão em m²', 'odin' ), // Opcional
                    ),

                    array(
                        'id'            => 'hospedes_deluxe', // Obrigatório
                        'label'         => __( 'Hospedes Bangalô Deluxe', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Acomoda ... hospedes' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Capacidade de acomodação de hospedes', 'odin' ), // Opcional
                    ),

                    array(
                        'id'            => 'descricao_deluxe', // Obrigatório
                        'label'         => __( 'Descrição Bangalô Deluxe', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Descreva aqui o Bangalô Deluxe' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Descrição do Bangalô Deluxe', 'odin' ), // Opcional
                    ),

                    array(
                        'id'          => 'fotos_deluxe', // Required
                        'label'       => __( 'Fotos Bangalô Deluxe', 'odin' ), // Required
                        'type'        => 'image_plupload', // Required
                        // 'attributes' => array(), // Optional (html input elements)
                        // 'default'    => '', // Optional (file url)
                        'description' => __( 'Selecione quantas fotos precisar', 'odin' ), // Optional
                    ),

                    array(
                        'id'          => 'background_master', // Obrigatório
                        'label'       => __( 'Background Master', 'odin' ), // Obrigatório
                        'type'        => 'image', // Obrigatório
                        'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                        'description' => __( 'Insira a imagem do background', 'odin' ), // Opcional
                    ),

                    array(
                        'id'          => 'up_background_master', // Obrigatório
                        'label'       => __( 'Sobreposto Background master', 'odin' ), // Obrigatório
                        'type'        => 'image', // Obrigatório
                        'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                        'description' => __( 'Insira a imagem acima do background', 'odin' ), // Opcional
                    ),

                    array(
                        'id'          => 'dimensao_master', // Obrigatório
                        'label'       => __( 'Dimensão Bangalô Master', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Possui ... m²!' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Dimensão em m²', 'odin' ), // Opcional
                    ),

                    array(
                        'id'            => 'hospedes_master', // Obrigatório
                        'label'         => __( 'Hospedes Bangalô Master', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Acomoda ... hospedes' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Capacidade de acomodação de hospedes', 'odin' ), // Opcional
                    ),

                    array(
                        'id'            => 'descricao_master', // Obrigatório
                        'label'         => __( 'Descrição Bangalô Master', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Descreva aqui o Bangalô Master' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Descrição do Bangalô Master', 'odin' ), // Opcional
                    ),

                    array(
                        'id'          => 'fotos_master', // Required
                        'label'       => __( 'Fotos Bangalô Master', 'odin' ), // Required
                        'type'        => 'image_plupload', // Required
                        // 'attributes' => array(), // Optional (html input elements)
                        // 'default'    => '', // Optional (file url)
                        'description' => __( 'Selecione quantas fotos precisar', 'odin' ), // Optional
                    ),
                    
                )
            ),
            'localizacao_section' => array( // Slug/ID of the section (Required)
                'tab'   => 'localizacao', // Tab ID/Slug (Required)
                'title' => __( 'Localização', 'odin' ), // Section title (Required)
                'fields' => array( // Section fields (Required)

                    array(
                        'id'            => 'descricao_localizacao', // Obrigatório
                        'label'         => __( 'Descrição Localização', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Descreva aqui a localização' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Descrição da localização', 'odin' ), // Opcional
                    ),
                    array(
                        'id'            => 'descricao_paraibadisiaco', // Obrigatório
                        'label'         => __( 'Descrição Paraibadisiaco', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Descreva aqui o local paraibadisiaco' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Descrição do local paraibadisiaco', 'odin' ), // Opcional
                    ),
                    array(
                        'id'          => 'galeria_fotos', // Required
                        'label'       => __( 'Galeria de Fotos', 'odin' ), // Required
                        'type'        => 'image_plupload', // Required
                        // 'attributes' => array(), // Optional (html input elements)
                        // 'default'    => '', // Optional (file url)
                        'description' => __( 'Adicione imagens à Galeria', 'odin' ), // Optional
                    ),
                )
            ),
            'eventos_section' => array( // Slug/ID of the section (Required)
                'tab'   => 'eventos', // Tab ID/Slug (Required)
                'title' => __( 'Eventos', 'odin' ), // Section title (Required)
                'fields' => array( // Section fields (Required)

                    array(
                        'id'            => 'descricao_eventos', // Obrigatório
                        'label'         => __( 'Descrição Eventos', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Descreva aqui a localização' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Descrição eventos', 'odin' ), // Opcional
                    ),

                    array(
                        'id'          => 'galeria_fotos', // Required
                        'label'       => __( 'Galeria de Fotos', 'odin' ), // Required
                        'type'        => 'image_plupload', // Required
                        // 'attributes' => array(), // Optional (html input elements)
                        // 'default'    => '', // Optional (file url)
                        'description' => __( 'Adicione imagens à Galeria', 'odin' ), // Optional
                    ),

                    array(
                        'id'            => 'sala_1_titulo', // Obrigatório
                        'label'         => __( 'Título da Sala 1', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Título da Sala 1' )
                        ),
                    ),

                    array(
                        'id'            => 'sala_1_descricao', // Obrigatório
                        'label'         => __( 'Descrição da Sala 1', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Descrição da Sala 1' )
                        ),
                    ),

                    array(
                        'id'            => 'sala_2_titulo', // Obrigatório
                        'label'         => __( 'Título da Sala 2', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Título da Sala 2' )
                        ),
                    ),

                    array(
                        'id'            => 'sala_2_descricao', // Obrigatório
                        'label'         => __( 'Descrição da Sala 2', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Descrição da Sala 2' )
                        ),
                    ),

                    array(
                        'id'            => 'sala_3_titulo', // Obrigatório
                        'label'         => __( 'Título da Sala 3', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Título da Sala 3' )
                        ),
                    ),

                    array(
                        'id'            => 'sala_3_descricao', // Obrigatório
                        'label'         => __( 'Descrição da Sala 3', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Descrição da Sala 3' )
                        ),
                    ),

                )
            ),
            'pacotes_promocoes_section' => array( // Slug/ID of the section (Required)
                'tab'   => 'pacotes_promocoes', // Tab ID/Slug (Required)
                'title' => __( 'Pacotes & Promoções', 'odin' ), // Section title (Required)
                'fields' => array( // Section fields (Required)

                    array(
                        'id'            => 'descricao_pacotes_promocoes', // Obrigatório
                        'label'         => __( 'Descrição Pacotes & Promoções', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'description' => __( 'Descrição sobre Pacotes & Promoções', 'odin' ), // Opcional
                    ),
                )
            ),
            'contato_section' => array( // Slug/ID of the section (Required)
                'tab'   => 'contato', // Tab ID/Slug (Required)
                'title' => __( 'Contato', 'odin' ), // Section title (Required)
                'fields' => array( // Section fields (Required)

                    array(
                        'id'            => 'endereco', // Obrigatório
                        'label'         => __( 'Endereço', 'odin' ), // Obrigatório
                        'type'        => 'editor', // Obrigatório
                        'description' => __( 'Insira o endereço', 'odin' ), // Opcional
                    ),

                    array(
                        'id'            => 'telefone', // Obrigatório
                        'label'         => __( 'Telefone', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Insira o telefone' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Telefone de contato', 'odin' ), // Opcional
                    ),

                    array(
                        'id'            => 'fax', // Obrigatório
                        'label'         => __( 'Fax', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Insira o fax' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'Fax para contato', 'odin' ), // Opcional
                    ),

                    array(
                        'id'            => 'reservas_individuais', // Obrigatório
                        'label'         => __( 'Reservas Individuais', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Insira o e-mail' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'E-mail para reservas individuais', 'odin' ), // Opcional
                    ),

                    array(
                        'id'            => 'reservas_grupos', // Obrigatório
                        'label'         => __( 'Reservas Grupos e Eventos', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Insira o e-mail' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'E-mail para reservas de grupos e eventos', 'odin' ), // Opcional
                    ),

                    array(
                        'id'            => 'central_atendimento', // Obrigatório
                        'label'         => __( 'Central de Atendimento', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Insira o e-mail' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'E-mail da central de atendimento', 'odin' ), // Opcional
                    ),

                    array(
                        'id'            => 'rh', // Obrigatório
                        'label'         => __( 'Recursos Humanos', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Insira o e-mail' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'E-mail dos recursos humanos', 'odin' ), // Opcional
                    ),
                    array(
                        'id'            => 'marketing', // Obrigatório
                        'label'         => __( 'Marketing', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Insira o e-mail' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'E-mail do setor de marketing', 'odin' ), // Opcional
                    ),

                    array(
                        'id'            => 'gerencia_geral', // Obrigatório
                        'label'         => __( 'Gerencia Geral', 'odin' ), // Obrigatório
                        'type'        => 'text', // Obrigatório
                        'attributes'  => array( // Opcional (atributos para input HTML/HTML5)
                            'placeholder' => __( 'Insira o e-mail' )
                        ),
                        'default'     => '', // Opcional
                        'description' => __( 'E-mail da gerencia geral', 'odin' ), // Opcional
                    ),
                )
            ),
            'capas_section' => array( // Slug/ID of the section (Required)
                'tab'   => 'capas', // Tab ID/Slug (Required)
                'title' => __( 'Capas', 'odin' ), // Section title (Required)
                'fields' => array( // Section fields (Required)

                    array(
                        'id'          => 'capa_o_resort', // Required
                        'label'       => __( 'Capa O Resort', 'odin' ), // Required
                        'type'        => 'image', // Required
                        // 'attributes' => array(), // Optional (html input elements)
                        // 'default'    => '', // Optional (file url)
                        'description' => __( 'Insira a capa da página "O Resort"', 'odin' ), // Optional
                    ),

                    array(
                        'id'          => 'capa_acomodacoes', // Required
                        'label'       => __( 'Capa Acomodações', 'odin' ), // Required
                        'type'        => 'image', // Required
                        // 'attributes' => array(), // Optional (html input elements)
                        // 'default'    => '', // Optional (file url)
                        'description' => __( 'Insira a capa da página "Acomodações"', 'odin' ), // Optional
                    ),

                    array(
                        'id'          => 'capa_all_inclusive', // Required
                        'label'       => __( 'Capa All Inclusive', 'odin' ), // Required
                        'type'        => 'image', // Required
                        // 'attributes' => array(), // Optional (html input elements)
                        // 'default'    => '', // Optional (file url)
                        'description' => __( 'Insira a capa da página "All Inclusive"', 'odin' ), // Optional
                    ),

                    array(
                        'id'          => 'capa_pacotes_promocoes', // Required
                        'label'       => __( 'Capa Pacotes & Promoções', 'odin' ), // Required
                        'type'        => 'image', // Required
                        // 'attributes' => array(), // Optional (html input elements)
                        // 'default'    => '', // Optional (file url)
                        'description' => __( 'Insira a capa da página "Pacotes & Promoções"', 'odin' ), // Optional
                    ),

                    array(
                        'id'          => 'capa_grupos_eventos', // Required
                        'label'       => __( 'Capa Grupos e Eventos', 'odin' ), // Required
                        'type'        => 'image', // Required
                        // 'attributes' => array(), // Optional (html input elements)
                        // 'default'    => '', // Optional (file url)
                        'description' => __( 'Insira a capa da página "Grupos e Eventos"', 'odin' ), // Optional
                    ),

                    array(
                        'id'          => 'capa_localizacao', // Required
                        'label'       => __( 'Capa Localização', 'odin' ), // Required
                        'type'        => 'image', // Required
                        // 'attributes' => array(), // Optional (html input elements)
                        // 'default'    => '', // Optional (file url)
                        'description' => __( 'Insira a capa da página "Localização"', 'odin' ), // Optional
                    ),

                    array(
                        'id'          => 'capa_contato', // Required
                        'label'       => __( 'Capa Contato', 'odin' ), // Required
                        'type'        => 'image', // Required
                        // 'attributes' => array(), // Optional (html input elements)
                        // 'default'    => '', // Optional (file url)
                        'description' => __( 'Insira a capa da página "Contato"', 'odin' ), // Optional
                    ),
                )
            ),
        )
    );
}

add_action( 'init', 'odin_theme_settings_example', 1 );