<header class="top">
	<h1 class="logo col-xs-5 col-sm-2"><a href="<?php bloginfo('url') ?>" title="Mussulo Resort by Mantra Group"><img src="<?php bloginfo('template_url') ?>/assets/img/mussulo-resort.png" alt="Mussulo Resort"></a></h1>
	<?php get_template_part('inc/nav', 'menu'); ?>
	<h2 class="mantra col-xs-12 col-sm-2 hidden-xs"><img src="<?php bloginfo('template_url') ?>/assets/img/mantra-group.png" alt="Mantra Group"></h2>
	<nav class="lang">
		<span class="current pt-br">Português (Brasil)</span>
		<div class="langs">
    <a href="#" onclick="doGTranslate('pt|pt');return false;" title="Portuguese" class="pt-br">Português (Brasil)</a>
    <a href="#" onclick="doGTranslate('pt|en');return false;" title="English" class="en-us">English (International)</a>
    <a href="#" onclick="doGTranslate('pt|es');return false;" title="Spanish" class=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/espanha.gif" style="width: 20px;height: 26px;margin-right: 3px;" alt=""></a> 
			
		</div>
	</nav>
</header>
