<section class="featured swiper-container">
	<div class="swiper-wrapper">
		<?php
			$args = array('post_type' => 'banners','order' => 'asc');
			query_posts($args);
				if (have_posts()) :
				while (have_posts()) : the_post();
				global $post, $wpdb;
				$image = get_post_meta( $post->ID,'image', true ); 
				$link = get_post_meta( $post->ID,'link', true ); 
		?>
			<?php $image = wp_get_attachment_image_src( $image, full ); ?>
			<div class="swiper-slide" style="background-image: url(<?php echo $image[0]; ?>)">
			<a href="<?php echo $link; ?>" class="full"></a>
			</div>
		<?php endwhile; ?>
		<?php
			else : 
				echo ' ';
			endif;
			wp_reset_query();
		?>
	</div>
	<div class="holder">
		<button class="prev"></button>
		<button class="next"></button>
		<div class="swiper-pagination"></div>
	</div>
</section>
