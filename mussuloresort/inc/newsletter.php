<?php
if(isset($_POST['email_news'])) {
   /* ------------------------------------------------ */
    if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($_POST['email_news']))) {
        $hasError = true;
    } else {
        $email= trim($_POST['email_news']);
    }

    /* ------------------------------------------------ */
    if(!isset($hasError)) {
        if (!isset($emailTo) || ($emailTo == '') ){
            $emailTo = "bruno@modernizr.com.br";
        }
        $subject = 'Mala Direta - Site Mussulo';
        $body = "Email: $email  \n\n Enviado Através da Mala Direta";
        $headers = 'From: bruno@modernizr.com.br <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;

        wp_mail($emailTo, $subject, $body, $headers);
        $emailSent = true;
        echo '<div class="alert alert-success" role="alert" style="padding:10px 0 5px 0;margin:0;width: 100%;float: left; text-align:center"><p class="container">Email Cadastrado com Sucesso!<p></div>';
    }
}
?>
<form action="#" method="post" class="newsletter">
    <div class="container">
        <div class="col-xs-12 col-sm-4 col-sm-offset-1">
            <h1>Cadastre-se em nossa <span>Newsletter</span></h1>
        </div>
        <fieldset class="col-xs-12 col-sm-5">
            <div class="holder">
                <input type="email" name="email_news" required="required" placeholder="DIGITE SEU EMAIL">
                <button class="glyphicon glyphicon-envelope"></button>
            </div>
        </fieldset>
    </div>
</form>
