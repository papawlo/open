<form action="https://securebr.e-gds.com/MussuloResortByMantra/light/default.aspx"  method="get" class="reserva -top">
	<div class="container">
		<ul>
			<li class="col-xs-12 col-sm-1">
				<h3><span>Sua reserva</span> online</h3>
			</li>
			<li class="col-xs-12 col-sm-5 dates">
				<div class="col-xs-12 col-sm-6">
					<span></span>
					<input type="text" name="arrivaldate" class="calendar-input" placeholder="Data de entrada">
					<div id="calendar_checkin" class="calendar"></div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<span></span>
					<input type="text" name="departuredate" class="calendar-input" placeholder="Data de saída">
					<div id="calendar_checkout" class="calendar"></div>
				</div>
			</li>
			<li class="col-xs-12 col-sm-3 people">
				<div class="number col-xs-6">
					<label for="">Adultos</label>
					<div class="holder">
						<input type="hidden" name="adults" value="0">
						<div class="mask"><span>0</span></div>
						<button class="up"><span>Mais</span></button>
						<button class="down"><span>Menos</span></button>
					</div>
				</div>
				<div class="number col-xs-6">
					<label for="">Crianças</label>
					<div class="holder">
						<input type="hidden" name="childrens" value="0">
						<div class="mask"><span>0</span></div>
						<button class="up"><span>Mais</span></button>
						<button class="down"><span>Menos</span></button>
					</div>
				</div>
			</li>
			<li class="submit col-xs-12 col-sm-1">
				<button class="search glyphicon glyphicon-search"></button>
			</li>
			<li class="plus col-xs-12 col-sm-2">
				<div class="col-xs-6 col-lg-5">
					<a href="<?php bloginfo('url') ?>/checkin" class="check-in">
						<strong>Web</strong><br /> check-in
					</a>
				</div>
				<div class="col-xs-6 col-lg-7">
					<a href="#" class="tour-agent">
						<strong>Agente</strong><br /> de viagens
						<!-- <div class="login">
							<ul>
								<li>
									<div class="holder">
										<label for="login-username"><span class="glyphicon glyphicon-user"></span></label>
										<input type="text" name="username" id="login-username">
									</div>
								</li>
								<li>
									<div class="holder">
										<label for="login-password"><span class="glyphicon glyphicon-lock"></span></label>
										<input type="password" name="password" id="login-password">
									</div>
								</li>
								<li>
									<input type="submit" name="send" value="Ok">
								</li>
							</ul>
						</div> -->
					</a>
				</div>
			</li>
		</ul>
	</div>
</form>
