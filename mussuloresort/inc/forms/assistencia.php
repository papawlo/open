// <?php function odin_contact_form() {

//     $form = new Odin_Contact_Form(
//         'contact_form',
//         'voce@email.com'
//     );

//     $form->set_fields(
//         array(
//             array(
//                 'fields' => array(
//                     array(
//                         'id'          => 'sender_name', // Required
//                         'label'       => __( 'Nome', 'odin' ), // Required
//                         'type'        => 'text', // Required
//                         'required'    => true, // Optional (bool)
//                         'attributes'  => array( // Optional (html input elements)
//                             'placeholder' => __( 'Digite o seu nome' )
//                         )
//                     ),
//                 )
//             ),
//             array(
//                 'fields' => array(
//                     array(
//                         'id'          => 'sender_email', // Required
//                         'label'       => __( 'E-mail', 'odin' ), // Required
//                         'type'        => 'email', // Required
//                         'required'    => true, // Optional (bool)
//                         'attributes'  => array( // Optional (html input elements)
//                             'placeholder' => __( 'Digite o seu e-mail!' )
//                         ),
//                         'description' => __( 'Precisa ser um endere&ccedil;o de e-mail v&aacute;lido', 'odin' ) // Optional
//                     ),
//                     array(
//                         'id'          => 'sender_message', // Required
//                         'label'       => __( 'Mensagem', 'odin' ), // Required
//                         'type'        => 'textarea', // Required
//                         'required'    => true, // Optional (bool)
//                         'attributes'  => array( // Optional (html input elements)
//                             'placeholder' => __( 'Digite a sua mensagem' )
//                         ),
//                     ),
//                     array(
//                         'id'          => 'sender_file', // Required
//                         'label'       => __( 'Arquivo', 'odin' ), // Required
//                         'type'        => 'file', // Required
//                         'required'    => true, // Optional (bool)
//                     )
//                 )
//             )
//         )
//     );

//     $form->set_subject( __( 'Email enviado por [sender_name] <[sender_email]>', 'odin' ) );

//     $form->set_content_type( 'html' );

//     $form->set_reply_to( 'sender_email' );

//     return $form;
// }

// add_action( 'init', array( odin_contact_form(), 'init' ), 1 ); ?>