<?php
function odin_galeria_cpt() {
    $galeria = new Odin_Post_Type('Galeria','galeria');
    $galeria->set_labels(
        array('menu_name' => __( 'Galeria', 'odin' ))
    );
    $galeria->set_arguments(
        array(
            'supports' => array( 'title'  ),
            'register_meta_box_cb'   => 'galeria_metabox'
            )
    );
}
add_action( 'init', 'odin_galeria_cpt', 1 );

function galeria_metabox() {

    $galeria = new Odin_Metabox(
        'galeria', // Slug/ID of the Metabox (Required)
        'Campos Extras', // Metabox name (Required)
        'galeria', // Slug of Post Type (Optional)
        'normal', // Context (options: normal, advanced, or side) (Optional)
        'high' // Priority (options: high, core, default or low) (Optional)
    );

     $galeria->set_fields(
   array(

            array(
                'id'          => 'foto_galeria', // Obrigatório
                'label'       => __( 'Foto Galeria', 'odin' ), // Obrigatório
                'type'        => 'image', // Obrigatório
                'default'     => '', // Opcional (deve ser o id de uma imagem em mídia)
                'description' => __( 'Insira a foto que será integrada à galeria.', 'odin' ), // Opcional
            )
        )
    );
}
add_action( 'init', 'galeria_metabox', 1 );
?>