<?php
function odin_pacotes_promocoes_cpt() {
    $pacotes_promocoes = new Odin_Post_Type('Pacotes & Promoções','pacotes_promocoes');
    $pacotes_promocoes->set_labels(
        array('menu_name' => __( 'Pacotes & Promoções', 'odin' ))
    );
    $pacotes_promocoes->set_arguments(
        array(
            'supports' => array( 'title' ),
            'register_meta_box_cb'   => 'pacotes_promocoes_metabox'
            )
    );
}
add_action( 'init', 'odin_pacotes_promocoes_cpt', 1 );

function pacotes_promocoes_metabox() {

    $pacotes_promocoes = new Odin_Metabox(
        'nocitia', // Slug/ID of the Metabox (Required)
        'Campos Extras', // Metabox name (Required)
        'pacotes_promocoes', // Slug of Post Type (Optional)
        'normal', // Context (options: normal, advanced, or side) (Optional)
        'high' // Priority (options: high, core, default or low) (Optional)
    );

     $pacotes_promocoes->set_fields(
        array(

            array(
                'id'            => 'selecionador', // Obrigatório
                'label'         => __( 'Selecão de Categoria', 'odin' ), // Obrigatório
                'type'          => 'select', // Obrigatório
                // 'attributes' => array(), // Opcional (atributos para input HTML/HTML5)
                'default'       => 'pacotes', // Opcional
                'description'   => __( 'Escolha a categoria da postagem', 'odin' ), // Opcional
                'options'       => array( // Obrigatório (adicione aque os ids e títulos)
                    'pacotes'   => 'Pacotes',
                    'promocoes'   => 'Promoções',
                ),
            ),

            array(
                'id'          => 'imagem_capa', // Required
                'label'       => __( 'Imagem da Capa: 500x340', 'odin' ), // Required
                'type'        => 'image', // Required
                // 'attributes' => array(), // Optional (html input elements)
                // 'default'    => '', // Optional (file url)
                'description' => __( 'Imagem da capa do pacote', 'odin' ), // Optional
            ),

            array(
                'id'          => 'imagem_interna', // Required
                'label'       => __( 'Imagem Interna: 1024x285', 'odin' ), // Required
                'type'        => 'image', // Required
                // 'attributes' => array(), // Optional (html input elements)
                // 'default'    => '', // Optional (file url)
                'description' => __( 'Imagem Interna do pacote', 'odin' ), // Optional
            ),

            array(
                'id'            => 'descricao', // Obrigatório
                'label'         => __( 'Descrição', 'odin' ), // Obrigatório
                'type'        => 'editor', // Obrigatório
                'description' => __( 'Descrição do Pacote', 'odin' ), // Opcional
            ),

            array(
                'id'            => 'link_reserva', // Obrigatório
                'label'         => __( 'Link Reserva', 'odin' ), // Obrigatório
                'type'        => 'text', // Obrigatório
                'description' => __( 'Insira o link do botão "RESERVAR"', 'odin' ), // Opcional
            ),

            )
    );
}
add_action( 'init', 'pacotes_promocoes_metabox', 1 );

 ?>