<?php
function odin_downloads_cpt() {
    $downloads = new Odin_Post_Type('Downloads','downloads');
    $downloads->set_labels(
        array('menu_name' => __( 'Downlaods', 'odin' ))
    );
    $downloads->set_arguments(
        array(
            'supports' => array( 'title'),
            'register_meta_box_cb'   => 'downloads_metabox'
            )
    );
}
add_action( 'init', 'odin_downloads_cpt', 1 );

function downloads_metabox() {

    $downloads = new Odin_Metabox(
        'downloads', // Slug/ID of the Metabox (Required)
        'Campos Extras', // Metabox name (Required)
        'downloads', // Slug of Post Type (Optional)
        'normal', // Context (options: normal, advanced, or side) (Optional)
        'high' // Priority (options: high, core, default or low) (Optional)
    );

     $downloads->set_fields(
        array(
            
            array(
                'id'          => 'arquivo', // Obrigatório
                'label'       => __( 'Anexar Arquivo', 'odin' ), // Obrigatório
                'type'        => 'upload', // Obrigatório
                'default'     => '', // Opcional (url de um arquivo)
                'description' => __( 'Descrition Example', 'odin' ), // Opcional
            ),

            array(
                'id'   => 'Lista de Arquivos', // Obrigatório
                'label'=> __( 'Text Example', 'odin' ), // Obrigatório
                'type' => 'title', // Obrigatório
            )

            )
    );
}
add_action( 'init', 'downloads_metabox', 1 );
?>