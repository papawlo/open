<?php
function odin_banners_cpt() {
    $banners = new Odin_Post_Type('banners','banners');
    $banners->set_labels(
        array('menu_name' => __( 'banners', 'odin' ))
    );
    $banners->set_arguments(
        array(
            'supports' => array( 'title'  ),
            'register_meta_box_cb'   => 'banners_metabox'
            )
    );
}
add_action( 'init', 'odin_banners_cpt', 1 );

function banners_metabox() {

    $banners = new Odin_Metabox(
        'banners', // Slug/ID of the Metabox (Required)
        'Campos Extras', // Metabox name (Required)
        'banners', // Slug of Post Type (Optional)
        'normal', // Context (options: normal, advanced, or side) (Optional)
        'high' // Priority (options: high, core, default or low) (Optional)
    );

     $banners->set_fields(
        array(

            array(
                    'id'          => 'image', // Obrigatório
                    'label'       => __( 'imagem', 'odin' ), // Obrigatório
                    'type'        => 'image',
                    'description' => __( 'Anexar Banner', 'odin' ), // Opcional
                ),
            array(
                    'id'          => 'link', // Obrigatório
                    'label'       => __( 'link', 'odin' ), // Obrigatório
                    'type'        => 'text',
                    'description' => __( 'inserir http://', 'odin' ), // Opcional
                ),

            )
    );
}
add_action( 'init', 'banners_metabox', 1 );
?>