<?php $home_destino = get_option( 'home_destino' ); ?>
<?php $pre_titulo = $home_destino['pre_titulo']; ?>
<?php $titulo_destino = $home_destino['titulo_destino']; ?>
<?php $background_all_inclusive = $home_destino['background_all_inclusive']; ?>
<?php $sobreposto_all_inclusive = $home_destino['sobreposto_all_inclusive']; ?>
<?php $background_o_resort = $home_destino['background_o_resort']; ?>
<?php $sobreposto_o_resort = $home_destino['sobreposto_o_resort']; ?>
<?php $background_acomodacoes = $home_destino['background_acomodacoes']; ?>
<?php $sobreposto_acomodacoes = $home_destino['sobreposto_acomodacoes']; ?>
<?php $image0 = wp_get_attachment_image_src( $background_all_inclusive, full ); ?>
<?php $image1 = wp_get_attachment_image_src( $sobreposto_all_inclusive, full ); ?>
<?php $image2 = wp_get_attachment_image_src( $background_o_resort, full ); ?>
<?php $image3 = wp_get_attachment_image_src( $sobreposto_o_resort, full ); ?>
<?php $image4 = wp_get_attachment_image_src( $background_acomodacoes, full ); ?>
<?php $image5 = wp_get_attachment_image_src( $sobreposto_acomodacoes, full ); ?>

<section class="destination">
	<header class="container">
		<h1><?php echo $pre_titulo; ?></h1>
		<h2><?php echo $titulo_destino; ?></h2>
	</header>
	<article class="col-xs-12 col-sm-4">
		<a href="<?php echo home_url().'/all-inclusive'; ?>" style="background-image: url(<?php echo $image0[0]; ?>)">
			<span class="brackets"></span>
			<img src="<?php echo $image1[0]; ?>" alt="All Inclusive">
		</a>
	</article>
	<article class="col-xs-12 col-sm-4">
		<a href="<?php echo home_url().'/o-resort'; ?>" style="background-image: url(<?php echo $image2[0]; ?>)">
			<span class="brackets"></span>
			<img src="<?php echo $image3[0]; ?>" alt="O Resort">
		</a>
	</article>
	<article class="col-xs-12 col-sm-4">
		<a href="<?php echo home_url().'/acomodacoes'; ?>" style="background-image: url(<?php echo $image4[0]; ?>)">
			<span class="brackets"></span>
			<img src="<?php echo $image5[0]; ?>" alt="Acomodações">
		</a>
	</article>
</section>
