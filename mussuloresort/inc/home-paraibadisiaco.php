<?php $home_paraibadisiaco = get_option( 'home_paraibadisiaco' ); ?>
<?php $titulo_paraibadisiaco = $home_paraibadisiaco['titulo_paraibadisiaco']; ?>
<?php $pos_titulo_1 = $home_paraibadisiaco['pos_titulo_1']; ?>
<?php $pos_titulo_2 = $home_paraibadisiaco['pos_titulo_2']; ?>
<?php $pos_titulo_3 = $home_paraibadisiaco['pos_titulo_3']; ?>

<section class="paraibadisiaco">
	<header>
		<h1><span><?php echo $titulo_paraibadisiaco; ?></span></h1>
		<h2><?php echo $pos_titulo_1; ?> <span><?php echo $pos_titulo_2; ?></span> <?php echo $pos_titulo_3; ?></h2>
	</header>
	<div class="container">
		<?php
								$args = array('post_type' => 'pacotes_promocoes','order' => 'asc','posts_per_page' => '3');
								query_posts($args);
									if (have_posts()) :
									while (have_posts()) : the_post();
									global $post, $wpdb; 
									$imagem_capa = get_post_meta( $post->ID,'imagem_capa', true );
									$selecionador = get_post_meta( $post->ID,'selecionador', true ); 
							?>	
								<?php $logo = wp_get_attachment_image_src( $imagem_capa , 'fullx475' ); ?>

							<article class="" style="background-image: url(<?php echo $logo[0]; ?>)">
								<h1><?php echo the_title(); ?></h1>
								<div class="holder">
									<h2><?php echo the_title(); ?></h2>
									<a href="<?php echo the_permalink(); ?>" class="button-default"><?php if( $selecionador == 'pacotes'){ ?>Ver Pacote <?php }else{ ?> Ver Promoção<?php } ?></a>
								</div>
								<span class="overlay"></span>
							</article>					
								
								
							<?php endwhile; ?>
							<?php
								else : 
									echo ' ';
								endif;
								wp_reset_query();
							?>
	</div>
</section>
