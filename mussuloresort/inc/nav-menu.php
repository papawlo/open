<button class="btn-menu col-xs-6 col-sm-3 col-lg-2">
	<span class="text hidden-xs">Menu</span>
	<span class="hamburguer"></span>
	<span class="arrow hidden-xs"></span>
</button>
<div class="holder-menu col-xs-12 col-sm-6 col-md-7 col-lg-6">
	<nav class="main-menu">
		<a href="<?php bloginfo('url') ?>/o-resort">O Resort</a>
		<a href="<?php bloginfo('url') ?>/acomodacoes">Acomodações</a>
		<a href="<?php bloginfo('url') ?>/all-inclusive">All Inclusive</a>
		<a href="<?php bloginfo('url') ?>/galeria">Galeria de Fotos</a>
		<a href="<?php bloginfo('url') ?>/pacotes">Promoções & Pacotes</a>
		<a href="<?php bloginfo('url') ?>/eventos">Grupos e Eventos</a>
		<a href="<?php bloginfo('url') ?>/localizacao">Localização</a>
		<a href="<?php bloginfo('url') ?>/contato">Contato</a>
	</nav>
	<?php get_template_part('inc/nav', 'social'); ?>
	<!-- <a href="#" class="private"><span class="glyphicon glyphicon-lock"></span> Área do Investidor</a> -->
</div>
