#Sabina - Sem roteiro

##Dependencies
Before runing the grunt task, make sure you have all the dependecies:

 - Ruby
   - Compass
 - NodeJS
   - Grunt
   - Bower
 - Composer

###Instalation
After download and install the latest version of [Ruby](https://www.ruby-lang.org/en/downloads/) and [NodeJS](http://nodejs.org/), open the terminal and type:

 - `gem update --system` then `gem install compass` to install compass
 - `npm install -g grunt-cli` to install grunt
 - `npm install -g bower` to install bower
 - `curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer` to install composer

Locate the project theme folder, then follow:

 - `grunt install` to install grunt dependencies
 - `bower install` to install bower dependencies
 - `composer install` to install composer dependencies

###If you need to create/modify the styles or scripts
Run `grunt` and start *creating* your project!