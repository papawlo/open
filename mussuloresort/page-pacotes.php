<?php $_pagename = 'pacotes'; include "header.php"; ?>
<?php $pacotes_promocoes = get_option( 'pacotes_promocoes' ); ?>
<?php $descricao_pacotes_promocoes = $pacotes_promocoes['descricao_pacotes_promocoes']; ?>
<?php $capas = get_option( 'capas' ); ?>
<?php $capa_pacotes_promocoes = $capas['capa_pacotes_promocoes']; ?>
<?php $image1 = wp_get_attachment_image_src( $capa_pacotes_promocoes, full ); ?>

	<section class="pacotes inner">
		<header style="background-image: url(<?php echo $image1[0]; ?>)"	></header>
		<div class="title col-xs-12 col-sm-4 col-md-3">
			<h1>Pacotes & Promoções</h1>
		</div>
		<div class="col-xs-12 col-sm-8 col-md-9">
			<div class="post">
				<p><?php echo $descricao_pacotes_promocoes; ?></p>
			</div>
		</div>

		<div class="main">
			<aside class="col-xs-12 col-sm-3 col-md-2">
				<ul>
					<li class="-active">
						<a href="#">Pacotes</a>
						<ul class="submenu">
							<?php
								$args = array('post_type' => 'pacotes_promocoes','order' => 'asc','meta_key' => 'selecionador', 'meta_value' => 'pacotes');
								query_posts($args);
									if (have_posts()) :
									while (have_posts()) : the_post();
									global $post, $wpdb; 
									$imagem_capa = get_post_meta( $post->ID,'imagem_capa', true ); 
							?>		
							<li>
								<a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a>
							</li>	
							<?php endwhile; ?>
							<?php
								else : 
									echo ' ';
								endif;
								wp_reset_query();
							?>
						</ul>
					</li>
					<li>
						<a href="#">Promoções</a>
						<ul class="submenu">
							<?php
								$args = array('post_type' => 'pacotes_promocoes','order' => 'asc','meta_key' => 'selecionador', 'meta_value' => 'promocoes');
								query_posts($args);
									if (have_posts()) :
									while (have_posts()) : the_post();
									global $post, $wpdb; 
									$imagem_capa = get_post_meta( $post->ID,'imagem_capa', true ); 
							?>		
							<li>
								<a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a>
							</li>	
							<?php endwhile; ?>
							<?php
								else : 
									echo ' ';
								endif;
								wp_reset_query();
							?>
						</ul>
					</li>
				</ul>
			</aside>

			<div class="-list col-xs-12 col-sm-9 col-md-10">
				<div class="row">
					
					
					<?php
								$args = array('post_type' => 'pacotes_promocoes','order' => 'asc');
								query_posts($args);
									if (have_posts()) :
									while (have_posts()) : the_post();
									global $post, $wpdb; 
									$imagem_capa = get_post_meta( $post->ID,'imagem_capa', true );
									$selecionador = get_post_meta( $post->ID,'selecionador', true );
							?>		
							<article class="col-xs-12 col-sm-6">
								<div class="thumb">
									<?php $imageUrl = wp_get_attachment_image_src($imagem_capa, '500x340'); ?>
									<?php $resizer = Odin_Thumbnail_Resizer::get_instance();
									$image1 = $resizer->process($imageUrl[0], '470', '320', true, true ); ?>
									<?php echo '<img class="wp-image-thumb img-responsive ' . sanitize_html_class( $class ) . '" src="' .$image1. '" width="' . esc_attr( $width ) . '" height="' . esc_attr( $height ) . '" alt="' . esc_attr( $alt ) . '" />'; ?>

 									<div class="holder">
										<a href="<?php echo the_permalink(); ?>" class="button-default"><?php if( $selecionador == 'pacotes'){ ?>Ver Pacote <?php }else{ ?> Ver Promoção<?php } ?></a>
									</div>
									<a href="<?php echo the_permalink(); ?>" class="overlay"></a>
								</div>
								<a href="<?php echo the_permalink(); ?>" class="permalink"><?php echo the_title(); ?></a>
							</article>						
								
								
							<?php endwhile; ?>
							<?php
								else : 
									echo ' ';
								endif;
								wp_reset_query();
							?>
					
				</div>
			</div>
		</div>
	</section>

<?php get_footer() ?>
