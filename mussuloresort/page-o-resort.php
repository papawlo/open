<?php $_pagename = 'resort'; include "header.php"; ?>
<?php $o_resort = get_option( 'o_resort' ); ?>
<?php $descricao_o_resort = $o_resort['descricao_o_resort']; ?>
<?php $galeria_o_resort = $o_resort['galeria_o_resort']; ?>
<?php echo g_fotos($galeria_o_resort); ?>
<?php $titulo_beach_club = $o_resort['titulo_beach_club']; ?>
<?php $descricao_beach_club = $o_resort['descricao_beach_club']; ?>
<?php $galeria_beach_club = $o_resort['galeria_beach_club']; ?>
<?php echo g_fotos($galeria_beach_club); ?>
<?php $titulo_kids_club = $o_resort['titulo_kids_club']; ?>
<?php $descricao_kids_club = $o_resort['descricao_kids_club']; ?>
<?php $galeria_kids_club = $o_resort['galeria_kids_club']; ?>
<?php echo g_fotos($galeria_kids_club); ?>
<?php $titulo_health_club = $o_resort['titulo_health_club']; ?>
<?php $descricao_health_club = $o_resort['descricao_health_club']; ?>
<?php $galeria_health_club = $o_resort['galeria_health_club']; ?>
<?php echo g_fotos($galeria_health_club); ?>
<?php $capas = get_option( 'capas' ); ?>
<?php $capa = $capas['capa_o_resort']; ?>
<?php $capa_o_resort = wp_get_attachment_image_src( $capa, full ); ?>
<?php $banner_beach_club = $o_resort['banner_beach_club']; ?>
<?php $up_banner_beach_club = $o_resort['up_banner_beach_club']; ?>
<?php $banner_kids_club = $o_resort['banner_kids_club']; ?>
<?php $up_banner_kids_club = $o_resort['up_banner_kids_club']; ?>
<?php $banner_health_club = $o_resort['banner_health_club']; ?>
<?php $up_banner_health_club = $o_resort['up_banner_health_club']; ?>
<?php $capa_o_resort = wp_get_attachment_image_src( $capa, full ); ?>
<?php $image1 = wp_get_attachment_image_src( $banner_beach_club, full ); ?>
<?php $image2 = wp_get_attachment_image_src( $up_banner_beach_club, full ); ?>
<?php $image3 = wp_get_attachment_image_src( $banner_kids_club, full ); ?>
<?php $image4 = wp_get_attachment_image_src( $up_banner_kids_club, full ); ?>
<?php $image5 = wp_get_attachment_image_src( $banner_health_club, full ); ?>
<?php $image6 = wp_get_attachment_image_src( $up_banner_health_club, full ); ?>

	<section class="resort inner">
		<header style="background-image: url(<?php echo $capa_o_resort[0]; ?>)"	></header>
		<div class="container">
			<div class="title col-xs-12 col-sm-4 col-md-3">
				<h1>O Resort</h1>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="post">
					<?php echo $descricao_o_resort; ?>
				</div>
				<ul class="espec">
					<li class="area col-xs-12 col-sm-4">
						<span>96</span>
						<span>mil m<strong style="font-family:lato">²</strong></span>
					</li>
					<li class="bangalo col-xs-12 col-sm-4">
						<span>101</span>
						<span>Magnificos Bangalôs</span>
					</li>
					<li class="estrutura col-xs-12 col-sm-4">
						<span>Estrutura pensada<br /> para o completo<br /> lazer e descanso.</span>
					</li>
				</ul>
				<div class="gallery -desktop">
					<div class="swiper-container">
						<ul class="swiper-wrapper">
							<?php
										foreach ( explode( ',', $galeria_o_resort ) as $image_id ) {
									?>
										<?php $logo = wp_get_attachment_image_src( $image_id , full ); ?>
										<li class="swiper-slide">
										<a href="<?php echo $logo[0]; ?>">
											<img src="<?php echo $logo[0]; ?>" alt="">
										</a>
									</li>
								
									<?php } ?>
						</ul>
					</div>
					<button class="prev"></button>
					<button class="next"></button>
				</div>

			</div>
		</div>

		<div class="areas tabs">
			<h1>Conheça nossas áreas</h1>
			<article class="-active">
				<header style="background-image: url(<?php echo $image1[0]; ?>)">
					<img src="<?php echo $image2[0]; ?>" alt="Beach Club">
					<span class="brackets -top"></span>
					<span class="brackets -bottom"></span>
				</header>
				<div class="container">
					<div class="content col-xs-12 col-md-8 col-md-offset-2">
						<h2><?php echo $titulo_beach_club; ?></h2>
						<div class="post">
							<?php echo $descricao_beach_club; ?></div>
						<div class="gallery -desktop">
							<div class="swiper-container">
								<ul class="swiper-wrapper">
									<?php
										foreach ( explode( ',', $galeria_beach_club ) as $image_id ) {
									?>
										<?php $logo = wp_get_attachment_image_src( $image_id , full ); ?>
										<li class="swiper-slide">
										<a href="<?php echo $logo[0]; ?>">
											<img src="<?php echo $logo[0]; ?>" alt="">
										</a>
									</li>
								
									<?php } ?>
								</ul>
							</div>
							<button class="prev"></button>
							<button class="next"></button>
						</div>
					</div>
				</div>
			</article>
			<article>
				<header style="background-image: url(<?php echo $image3[0]; ?>)">
					<img src="<?php echo $image4[0]; ?>" alt="Kids Club">
					<span class="brackets -top"></span>
					<span class="brackets -bottom"></span>
				</header>
				<div class="container">
					<div class="content col-xs-12 col-md-8 col-md-offset-2">
						<h2><?php echo $titulo_kids_club; ?></h2>
						<div class="post">
							<?php echo $descricao_kids_club; ?>					
						</div>
						<div class="gallery -desktop">
							<div class="swiper-container">
								<ul class="swiper-wrapper">
									<?php
										foreach ( explode( ',', $galeria_kids_club ) as $image_id ) {
									?>
										<?php $logo = wp_get_attachment_image_src( $image_id , full ); ?>
										<li class="swiper-slide">
										<a href="<?php echo $logo[0]; ?>">
											<img src="<?php echo $logo[0]; ?>" alt="">
										</a>
									</li>
								
									<?php } ?>
								</ul>
							</div>
							<button class="prev"></button>
							<button class="next"></button>
						</div>
					</div>
				</div>
			</article>
			<article>
				<header style="background-image: url(<?php echo $image5[0]; ?>)">
					<img src="<?php echo $image6[0]; ?>" alt="Beach Club">
					<span class="brackets -top"></span>
					<span class="brackets -bottom"></span>
				</header>
				<div class="container">
					<div class="content col-xs-12 col-md-8 col-md-offset-2">
						<h2><?php echo $titulo_health_club; ?></h2>
						<div class="post">
							<?php echo $descricao_health_club; ?>
						</div>
						<div class="gallery -desktop">
							<div class="swiper-container">
								<ul class="swiper-wrapper">
									<?php
										foreach ( explode( ',', $galeria_health_club ) as $image_id ) {
									?>
										<?php $logo = wp_get_attachment_image_src( $image_id , full ); ?>
										<li class="swiper-slide">
										<a href="<?php echo $logo[0]; ?>">
											<img src="<?php echo $logo[0]; ?>" alt="">
										</a>
									</li>
								
									<?php } ?>
								</ul>
							</div>
							<button class="prev"></button>
							<button class="next"></button>
						</div>
					</div>
				</div>
			</article>
			
		</div>
	</section>

<?php get_footer() ?>
