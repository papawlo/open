<?php $_pagename = 'inclusive'; include "header.php"; ?>
<?php $all_inclusive = get_option( 'all_inclusive' ); ?>
<?php $descricao_all_inclusive = $all_inclusive['descricao_all_inclusive']; ?>
<?php $descricao_restaurante = $all_inclusive['descricao_restaurante']; ?> 
<?php $galeria_fotos = $all_inclusive['galeria_fotos']; ?>
<?php echo g_fotos($galeria_fotos); ?>
<?php $capas = get_option( 'capas' ); ?>
<?php $capa_all_inclusive = $capas['capa_all_inclusive']; ?>
<?php $background_banner = $all_inclusive['background_banner']; ?>
<?php $background_up = $all_inclusive['up_background_banner']; ?>
<?php $image1 = wp_get_attachment_image_src( $capa_all_inclusive, full ); ?>
<?php $background_gastronomia = $all_inclusive['background_gastronomia']; ?>
<?php $up_background_gastronomia = $all_inclusive['up_background_gastronomia']; ?>
<?php $image2 = wp_get_attachment_image_src( $background_banner, full ); ?>
<?php $image3 = wp_get_attachment_image_src( $background_up, full ); ?>

	<section class="all-inclusive inner">
		<header style="background-image: url(<?php echo $image1[0]; ?>)"></header>
		<div class="container">
			<div class="title col-xs-12 col-sm-4 col-md-3">
				<h1>All Inclusive</h1>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="post">
				<?php echo $descricao_all_inclusive; ?>	
				</div>
			</div>
		</div>

		<div class="gastronomia tabs">
			<h1>Diversidade Gastronômica</h1>
			<article class="-active">
				<header style="background-image: url(<?php echo $image2[0]; ?>)">
					<img src="<?php echo $image3[0]; ?>" alt="Beach Club">
					<span class="brackets -top"></span>
					<span class="brackets -bottom"></span>
				</header>
				<div class="container">
					<div class="content col-xs-12 col-md-8 col-md-offset-2">
						<h2>Restaurante Rio Zare</h2>
						<div class="post">
							<?php echo $descricao_restaurante; ?>	
						</div>
						<div class="gallery -desktop">
							<div class="swiper-container">
								<ul class="swiper-wrapper">
									<?php echo g_fotos($galeria_fotos); ?>
									<?php foreach ( explode( ',', $galeria_fotos ) as $image_id ) {
											$logo = wp_get_attachment_image_src( $image_id , full ); 
									?>
									<li class="swiper-slide">
										<a href="<?php echo $logo[0]; ?>"><img src="<?php echo $logo[0]; ?>" alt=""></a>
									</li>
									<?php
										}  
									?>
								</ul>
							</div>
							<button class="prev"></button>
							<button class="next"></button>
						</div>
					</div>
				</div>
			</article>
		</div>
	</section>

<?php get_footer() ?>
