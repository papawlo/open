<?php $_pagename = 'localizacao'; include "header.php"; ?>
<?php $localizacao = get_option( 'localizacao' ); ?>
<?php $descricao_localizacao = $localizacao['descricao_localizacao']; ?>
<?php $descricao_paraibadisiaco = $localizacao['descricao_paraibadisiaco']; ?>
<?php $galeria_fotos = $localizacao['galeria_fotos'];  ?>
<?php $capas = get_option( 'capas' ); ?>
<?php $capa_localizacao = $capas['capa_localizacao']; ?>
<?php $image1 = wp_get_attachment_image_src( $capa_localizacao, full ); ?>

	<section class="localizacao inner">
		<header style="background-image: url(<?php echo $image1[0]; ?>)"	></header>
		<div class="title col-xs-12 col-sm-4 col-md-3">
			<h1>Localização</h1>
		</div>
		<div class="col-xs-12 col-sm-8 col-md-9">
			<div class="post">
				<p><?php echo $descricao_localizacao; ?></p>
			</div>
		</div>
		<div class="map col-xs-12">
			<h1 class="subtitle">Como chegar</h1>
		
			<div id="tabContainer">
    <div id="tabs_loc">

      <ul>
        <li id="tabHeader_1">Aeroporto <br /> de João Pessoa</li>
        <li id="tabHeader_2">Aeroporto <br / >de Recife</li>
        <li id="tabHeader_3">Aeroporto <br />de Natal</li>
      </ul>
    </div>
    <div id="tabscontent">
      <div class="tabpage embed-container maps" id="tabpage_1">
      	<iframe src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d126661.09863583301!2d-34.95259724652946!3d-7.222660110857679!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0x7ace8d3174243cb%3A0x1afd929845f8222e!2sAeroporto+Internacional+de+Jo%C3%A3o+Pessoa+-+Aeroporto%2C+Bayeux+-+PB!3m2!1d-7.1460184!2d-34.9489018!4m5!1s0x7acbeff736d6def%3A0xd5f24803e11d706c!2sMussulo+Resort%2C+Conde+-+PB!3m2!1d-7.3123796!2d-34.8171113!5e0!3m2!1spt-BR!2sbr!4v1441989110442" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
      <div class="tabpage embed-container maps" id="tabpage_2">
       <iframe src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d506094.2237053676!2d-35.24185676863517!3d-7.698049384414502!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0x7ab1e310230b959%3A0x6bcba1dcbb220871!2sAeroporto+Internacional+do+Recife%2FGuararapes+-+Gilberto+Freyre+-+Pra%C3%A7a+Ministro+Salgado+Filho%2C+s%2Fn+-+Imbiribeira%2C+Recife+-+PE%2C+51210-902%2C+Brasil!3m2!1d-8.1259315!2d-34.9240154!4m5!1s0x7acbeff736d6def%3A0xd5f24803e11d706c!2sMussulo+Resort%2C+Conde+-+PB!3m2!1d-7.3123796!2d-34.8171113!5e0!3m2!1spt-BR!2sbr!4v1442036188639" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
      <div class="tabpage embed-container maps" id="tabpage_3">
      <iframe src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d1014624.6257135504!2d-35.69305687357413!3d-6.599926899530384!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0x7b256455f25087d%3A0x402257c3fda49ba0!2sAeroporto+Internacional+de+Natal+-+Ema%C3%BAs%2C+Parnamirim+-+RN!3m2!1d-5.9051744!2d-35.2474118!4m5!1s0x7acbeff736d6def%3A0xd5f24803e11d706c!2sMussulo+Resort%2C+Conde+-+PB!3m2!1d-7.3123796!2d-34.8171113!5e0!3m2!1spt-BR!2sbr!4v1442036275359" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
       </div>
    </div>
  </div>
			<!-- <nav>
				<button class="-active" data-target="pb">Aeroporto<br /> de João Pessoa</button>
				<button data-target="pe">Aeroporto<br /> de Recife</button>
				<button data-target="rn">Aeroporto<br /> de Natal</button>
			</nav>
			<div id="google-map"></div> -->
		</div>
		<div class="paraibadisiaco col-xs-12">
			<div class="container">
				<h1 class="subtitle">Paraibadisíaco</h1>
				<div class="swiper-container">
					<div class="swiper-wrapper">
							<?php echo g_fotos($galeria_fotos); ?>
							<?php foreach ( explode( ',', $galeria_fotos ) as $image_id ) {
									$logo = wp_get_attachment_image_src( $image_id , full ); 
									echo '<div class="swiper-slide col-xs-12 col-sm-4">
										<img src="'. $logo[0] .'" alt="">
									</div>';
								}  
							?>
					</div>
					<div class="swiper-pagination"></div>
				</div>
				<p><?php echo $descricao_paraibadisiaco; ?></p>
			</div>
		</div>
	</section>

<?php get_footer() ?>
