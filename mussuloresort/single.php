<?php $_pagename = 'pacotes'; include "header.php"; ?>

	<section class="pacotes read">
		<header style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/resort-top.jpg)"	></header>
		<div class="title col-xs-12 col-sm-4 col-md-3">
			<h1>Pacotes & Promoções</h1>
		</div>
		<div class="col-xs-12 col-sm-8 col-md-9">
			<header class="thumb">
				<h1>Noite de Núpcias</h1>
				<img src="<?php bloginfo('template_url') ?>/assets/img/delete/pacote-top.jpg" alt="Pacote">
			</header>
			<div class="post">
				<p>Se hospedando por 07(sete) noites no Mussulo Resort by Mantra você pode escolher 01 passeio especialmente montados para que sua viagem seja repleta de lembranças inesquecíveis.</p>
				<p>Os roteiros para o passeio terão as seguintes opções: Entardecer na praia do Jacaré, Litoral sul com Praias da Costa do Conde: Coqueirinho, Tambaba e Praia Bela e City Tour Histórico e Panorâmico pelas praias de João Pessoa.</p>
				<p>Faça agora a sua reserva e conheça um pouco mais do Estado mais Paraibadisíaco do Brasil.</p>
				<p>Esta promoção está sujeita a disponibilidade - não é cumulativa com outras promoções, válida por tempo indeterminado, podendo ser alterada a qualquer momento sem prévio aviso.</p>
			</div>
			<div class="center">
				<a href="#" class="button-default">Reservar</a>
			</div>
			<div class="-list row">
				<h2>Outros pacotes</h2>
				<article class="col-xs-12 col-sm-4">
					<div class="thumb">
						<img src="<?php bloginfo('template_url') ?>/assets/img/delete/pacote-01.jpg" alt="Pacote 01">
						<div class="holder">
							<a href="#" class="button-default">Ver Pacote</a>
						</div>
						<a href="#" class="overlay"></a>
					</div>
					<a href="#" class="permalink">Noite de Nupcias</a>
				</article>
				<article class="col-xs-12 col-sm-4">
					<div class="thumb">
						<img src="<?php bloginfo('template_url') ?>/assets/img/delete/pacote-01.jpg" alt="Pacote 01">
						<div class="holder">
							<a href="#" class="button-default">Ver Pacote</a>
						</div>
						<a href="#" class="overlay"></a>
					</div>
					<a href="#" class="permalink">Noite de Nupcias</a>
				</article>
				<article class="col-xs-12 col-sm-4">
					<div class="thumb">
						<img src="<?php bloginfo('template_url') ?>/assets/img/delete/pacote-01.jpg" alt="Pacote 01">
						<div class="holder">
							<a href="#" class="button-default">Ver Pacote</a>
						</div>
						<a href="#" class="overlay"></a>
					</div>
					<a href="#" class="permalink">Noite de Nupcias</a>
				</article>
			</div>
			<div class="center">
				<a href="#" class="button-default">Veja mais pacotes</a>
			</div>
		</div>

	</section>

<?php get_footer() ?>
