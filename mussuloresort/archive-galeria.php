<?php $_pagename = 'galeria'; include "header.php"; ?>

	<section class="galeria inner">
		<div class="container">
			<div class="title col-xs-12">
				<h1>Galeria de Fotos</h1>
			</div>
		</div>
			<div class="album">
				<?php
					$args = array('post_type' => 'galeria','order' => 'asc', 'posts_per_page' => '100');
					query_posts($args);
					while (have_posts()) : the_post();
					global $post, $wpdb; 
					$foto_galeria = get_post_meta( $post->ID,'foto_galeria', true );
				?>
				<?php $gfoto = wp_get_attachment_image_src( $foto_galeria , 'fullx475' ); ?>
					<article class="col-xs-6 col-sm-3 col-md-2">
						<a href="<?php echo $gfoto[0]; ?>" style="background-image: url(<?php echo $gfoto[0]; ?>)">
							<span class="brackets"></span>
							<h1><?php echo the_title(); ?></h1>
						</a>
					</article>
				<?php endwhile; ?>
				<?php
					wp_reset_query();
				?>
			</div>
	</section>

<?php get_footer() ?>
