<?php get_header(); ?>
<?php
global $current_user;
global $wpdb;
?>

	<article id="contato">

	<header class="header-inner2">
		<h2>Atendimento <span>Fale conosco</span></h2>

		<img class="square-top" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/square-white-top.png" alt="">
		<img class="square-bottom" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/square-white-bottom.png" alt="">
		
		<a href="#atendimento" class="scroll"><img class="arrow-bottom" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/arrow-bottom.png" alt=""></a>
	
	</header>
	
	<div id="atendimento" class="container">

		<p class="pre">Deixe aqui sua mensagem, dúvida, elogio, crítica ou sugestão. Estamos sempre atentos.<small>*campos obrigatórios</small></p>
		
		<form id="fr-contato" class="fr-default" action="#" method="post">
			<fieldset class="row">
				
				<label class="col-lg-12 col-md-12 col-sm-12 col-xs-12" for="">Nome
					<span>*</span>
					<input type="text" name="nome_atendimento" id="nome_atendimento" placeholder="digite seu nome" required="required" />
				</label>

				<label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Telefone
					<input type="text" name="fone" id="fone" placeholder="digite seu telefone">
				</label>

				<label class="col-lg-12 col-md-12 col-sm-12 col-xs-12">E-mail
				<span>*</span>
					<input type="text" name="email" id="email" placeholder="digite seu e-mail"required="required" />
				</label>
				
				<label class="col-lg-3 col-md-3 col-sm-12 col-xs-12">Estado
					<span></span>
					<select name="cod_estados" id="cod_estados">
					<?php
					$estados = $wpdb->get_results ( "SELECT cod_estados, sigla FROM  kti_estados ORDER BY sigla" );
					foreach ( $estados as $estado )
					{
						 echo '<option value="'.$estado->cod_estados.'">'.$estado->sigla.'</option>';
					}
					?>
					</select>
				</label>

				<label class="col-lg-9 col-md-9 col-sm-12 col-xs-12">Cidade
					<select name="cod_cidades" id="cod_cidades">
						<option value="">-- Escolha um estado --</option>
					</select>	
				</label>
				<script src="http://www.google.com/jsapi"></script>
					<script type="text/javascript">
					  google.load('jquery', '1.3');
					</script>	
				<script type="text/javascript">
					$(function(){
						$('#cod_estados').change(function(){
							if( $(this).val() ) {
								$('#cod_cidades').hide();
								$('.carregando').show();
								$.getJSON('<?php echo get_template_directory_uri(); ?>/cidades.ajax.php?search=',{cod_estados: $(this).val(), ajax: 'true'}, function(j){
									var options = '<option value=""></option>';	
									for (var i = 0; i < j.length; i++) {
										options += '<option value="' + j[i].cod_cidades + '">' + j[i].nome + '</option>';
									}	
									$('#cod_cidades').html(options).show();
									$('.carregando').hide();
								});
							} else {
								$('#cod_cidades').html('<option value="">-- Escolha um estado --</option>');
							}
						});
					});
				</script>

				<label class="col-lg-12 col-md-12 col-sm-12 col-xs-12" for="">Mensagem
					<span>*</span>
					<textarea name="mensagem" id="mensagem" placeholder="digite sua mensagem"></textarea>
				</label>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<input type="submit" value="Entre em contato">
				</div>
				
			</fieldset>

		</form>
	</div>

</article>
<?php
get_footer();