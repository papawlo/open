<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initLayout() {
        Zend_Layout::startMvc();
    }

    protected function _initAcl() {
        $aclSetup = new App_Acl_Setup();
    }



    protected function _initRequest() {
        $this->bootstrap('FrontController');
        $front = $this->getResource('FrontController');
        $request = new Zend_Controller_Request_Http();
        $front->setRequest($request);
        return $request;
    }

    public function _initRouter() {
//
        $frontController = Zend_Controller_Front::getInstance();
//
        $router = $frontController->getRouter();
//
//        $indexRoute = new Zend_Controller_Router_Route(':index',
//                        array(
//                            'module' => 'default',
//                            'controller' => 'index',
//                            'action' => 'index'
//                        )
//        );
//
//        $router->addRoute('index', $indexRoute);
        $homeRoute = new Zend_Controller_Router_Route('home', array(
            'module' => 'default',
            'controller' => 'index',
            'action' => 'home'
                )
        );
        $router->addRoute('home', $homeRoute);

        $signinRoute = new Zend_Controller_Router_Route('signin', array(
            'module' => 'default',
            'controller' => 'index',
            'action' => 'signin'
                )
        );
        $router->addRoute('signin', $signinRoute);

        $loginRoute = new Zend_Controller_Router_Route('login', array(
            'module' => 'default',
            'controller' => 'index',
            'action' => 'login'
                )
        );
        $router->addRoute('login', $loginRoute);

        $loginAuthenticationRoute = new Zend_Controller_Router_Route('login/authenticatewith/:provider', array(
            'module' => 'default',
            'controller' => 'index',
            'action' => 'authenticatewith'
                )
        );
        $router->addRoute('loginAuthentication', $loginAuthenticationRoute);

        $hybridauth = new Zend_Controller_Router_Route('login/hybridauth', array(
            'module' => 'default',
            'controller' => 'index',
            'action' => 'hybridauth'
                )
        );
        $router->addRoute('hybridauth', $hybridauth);

        $logoutDefaultRoute = new Zend_Controller_Router_Route('logout', array(
            'module' => 'default',
            'controller' => 'index',
            'action' => 'logout'
                )
        );
        $router->addRoute('logout', $logoutDefaultRoute);
        $logoutRoute = new Zend_Controller_Router_Route('user/logout', array(
            'module' => 'user',
            'controller' => 'index',
            'action' => 'logout'
                )
        );
        $router->addRoute('user/logout', $logoutRoute);

        $userEventsRoute = new Zend_Controller_Router_Route('user/index/events', array(
            'module' => 'user',
            'controller' => 'index',
            'action' => 'events'
                )
        );
        $router->addRoute('user-index-events', $userEventsRoute);
        $userEventsListRoute = new Zend_Controller_Router_Route('user/index/events-list', array(
            'module' => 'user',
            'controller' => 'index',
            'action' => 'events-list'
                )
        );
        $router->addRoute('user-index-events-list', $userEventsListRoute);
        $userCalendarMonthRoute = new Zend_Controller_Router_Route('user/index/month', array(
            'module' => 'user',
            'controller' => 'index',
            'action' => 'month'
                )
        );
        $router->addRoute('user-index-calendar-month', $userCalendarMonthRoute);

        $userCalendarMonthDayRoute = new Zend_Controller_Router_Route('user/index/month-day', array(
            'module' => 'user',
            'controller' => 'index',
            'action' => 'month-day'
                )
        );
        $router->addRoute('user-index-calendar-month-day', $userCalendarMonthDayRoute);

        $userCalendarDayRoute = new Zend_Controller_Router_Route('user/index/day', array(
            'module' => 'user',
            'controller' => 'index',
            'action' => 'day'
                )
        );
        $router->addRoute('user-index-calendar-day', $userCalendarDayRoute);

        $userCalendarWeekRoute = new Zend_Controller_Router_Route('user/index/week', array(
            'module' => 'user',
            'controller' => 'index',
            'action' => 'week'
                )
        );
        $router->addRoute('user-index-calendar-week', $userCalendarWeekRoute);

        $userCalendarWeekDayRoute = new Zend_Controller_Router_Route('user/index/week-days', array(
            'module' => 'user',
            'controller' => 'index',
            'action' => 'week-days'
                )
        );
        $router->addRoute('user-index-calendar-week-day', $userCalendarWeekDayRoute);

        $userCalendarYearRoute = new Zend_Controller_Router_Route('user/index/year', array(
            'module' => 'user',
            'controller' => 'index',
            'action' => 'year'
                )
        );
        $router->addRoute('user-index-calendar-yeary', $userCalendarYearRoute);

        $userCalendarYearMotnhRoute = new Zend_Controller_Router_Route('user/index/year-day', array(
            'module' => 'user',
            'controller' => 'index',
            'action' => 'year-month'
                )
        );
        $router->addRoute('user-index-calendar-year-month', $userCalendarYearMotnhRoute);

        $userSettingsRoute = new Zend_Controller_Router_Route('user/settings', array(
            'module' => 'user',
            'controller' => 'settings',
            'action' => 'index'
                )
        );
        $router->addRoute('user-settings', $userSettingsRoute);
//        $userSettingsEditRoute = new Zend_Controller_Router_Route('user/settings/edit', array(
//            'module' => 'user',
//            'controller' => 'settings',
//            'action' => 'edit'
//                )
//        );
//        $router->addRoute('user-settings-edit', $userSettingsEditRoute);

        $datesPaneRoute = new Zend_Controller_Router_Route('user/dates-pane', array(
            'module' => 'user',
            'controller' => 'dates-pane',
            'action' => 'index'
                )
        );
        $router->addRoute('user/dates-pane', $datesPaneRoute);

        $daddDateFormRoute = new Zend_Controller_Router_Route('user/add-date-form/id/:id', array(
            'module' => 'user',
            'controller' => 'dates-pane',
            'action' => 'add-date-form',
            'id' => '/%d+'
                )
        );
        $router->addRoute('user-add-date-form', $daddDateFormRoute);

        $datesPaneEditRoute = new Zend_Controller_Router_Route('user/dates-pane/edit', array(
            'module' => 'user',
            'controller' => 'dates-pane',
            'action' => 'edit'
                )
        );
        $router->addRoute('user-dates-pane-edit', $datesPaneEditRoute);

        $datesPaneDeleteRoute = new Zend_Controller_Router_Route('user/dates-pane/delete', array(
            'module' => 'user',
            'controller' => 'dates-pane',
            'action' => 'delete'
                )
        );
        $router->addRoute('user-dates-pane-delete', $datesPaneDeleteRoute);

//        $datesPaneAutorizaGoogleRoute = new Zend_Controller_Router_Route('user/dates-pane/autoriza-google', array(
//            'module' => 'user',
//            'controller' => 'dates-pane',
//            'action' => 'autoriza-google'
//                )
//        );
//        $router->addRoute('user-dates-pane-autoriza-google', $datesPaneAutorizaGoogleRoute);
        $datesPaneComAutocompleteRoute = new Zend_Controller_Router_Route('user/dates-pane/com-autocoplete', array(
            'module' => 'user',
            'controller' => 'dates-pane',
            'action' => 'com-autocomplete'
                )
        );
        $router->addRoute('user-dates-pane-com-autocomplete', $datesPaneComAutocompleteRoute);

        $userFotoRoute = new Zend_Controller_Router_Route('user/settings/foto', array(
            'module' => 'user',
            'controller' => 'settings',
            'action' => 'foto'
                )
        );

        $router->addRoute('user-settings-foto', $userFotoRoute);




        $erro404 = new Zend_Controller_Router_Route('error/404', array('module' => 'default',
            'controller' => 'error',
            'action' => 'notfound'
                )
        );

        $router->addRoute('error404', $erro404);

        $crop_image = new Zend_Controller_Router_Route('crop-image', array('module' => 'default',
            'controller' => 'index',
            'action' => 'crop-image'
                )
        );

        $router->addRoute('crop-image', $crop_image);

        $cronNotification = new Zend_Controller_Router_Route('notification', array('module' => 'default',
            'controller' => 'cron',
            'action' => 'notification'
                )
        );
        $router->addRoute('notification', $cronNotification);
    }

    protected function _initView() {
        $view = new Zend_View();
        $view->doctype('HTML5');
        $view->env = APPLICATION_ENV;
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
        $viewRenderer->setView($view);
        return $view;
    }

    protected function _initAutoload() {
        $moduleLoader = new Zend_Application_Module_Autoloader(array(
            'namespace' => '',
            'basePath' => APPLICATION_PATH));

        return $moduleLoader;
    }

    protected function _initConfig() {
        $config = new Zend_Config($this->getOptions());
        Zend_Registry::set('config', $config);
    }

    protected function _initNavigationConfig() {

        $frontController = Zend_Controller_Front::getInstance();
        $frontController->setRequest(new Zend_Controller_Request_Http());

        $request = $frontController->getRequest();

        $uri = $request->getRequestUri();

        if (strpos($uri, 'admin') === false) {
            $filename = realpath(APPLICATION_PATH . '/configs/navigation.xml');
        } else {
            $filename = realpath(APPLICATION_PATH . '/modules/admin/configs/navigation.xml');
        }

        if ($filename) {

            $config = new Zend_Config_Xml($filename);

            $this->registerPluginResource('view');
            $view = $this->bootstrap('view')->getResource('view');
            $container = new Zend_Navigation($config);
            $role = @Zend_Auth::getInstance()->getIdentity()->role ? @ Zend_Auth::getInstance()->getIdentity()->role : "guest";
            $acl = Zend_Registry::get('acl');
            $view->navigation($container)->setAcl($acl)->setRole($role);
        }
    }

}

