<?php

class Model_AtividadeEconomica extends App_Db_Table_Abstract {

    protected $_name = 'atividade_economica';
    protected $_primary = 'id_atividade_economica';

    /**
     * Dependent tables
     */
    protected $_dependentTables = array('estabelecimento_atividade');

}