<?php

class Model_Row_Notification extends Zend_Db_Table_Row_Abstract {

    private $_reminders = null;

    /**
     * @return Model_Rowset_Reminder
     */
    public function getReminders() {
        if (!$this->_reminders) {
            $this->_reminders = $this->findDependentRowset('Model_Reminder');
        }
        return $this->_reminders;
    }

}