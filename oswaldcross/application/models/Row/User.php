<?php

class Model_Row_User extends Zend_Db_Table_Row_Abstract {

    private $_authentcations = null;
    private $_relationships = null;
    private $_reminders = null;

    /**
     * @return Model_Rowset_Relatioship
     */
    public function getRelationships() {
        if (!$this->_relationships) {
            $this->_relationships = $this->findDependentRowset('Model_Relationship');
        }
        return $this->_relationships;
    }

    /**
     * @return Model_Rowset_Relatioship
     */
    public function getRelationshipsAtivo() {
        if (!$this->_relationships) {
            $relationshipModel = new Model_Relationship();

            $select = $relationshipModel->select()->where('status != ?', 'deletado');

            $this->_relationships = $this->findDependentRowset('Model_Relationship', null, $select);
        }
        return $this->_relationships;
    }

    /**
     * @return Model_Rowset_Authentications
     */
    public function getAuthentications() {
        if (!$this->_authentcations) {
            $this->_authentcations = $this->findDependentRowset('Model_Authentications');  // join table
        }

        return $this->_authentcations;
    }

    /**
     * @return Model_Rowset_Reminder
     */
    public function getRemindersByUser() {

        if (!$this->_reminders) {
            $relationshipModel = new Model_Relationship();
            $select = $relationshipModel->select()
                    ->setIntegrityCheck(false)
                    ->from(array('r' => 'relationship'), array())
                    ->joinInner(array('rm' => 'reminder'), 'r.id = rm.relationship_id')
                    ->where('r.users_id = ?', $this->id)
                    ->where('r.status != ?', 'deletado')
                    ->where('rm.status != ?', 'deletado');

            $this->_reminders = $relationshipModel->fetchAll($select);
        }

        return $this->_reminders;
    }

}