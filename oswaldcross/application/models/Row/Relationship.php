<?php

class Model_Row_Relationship extends Zend_Db_Table_Row_Abstract {

    private $_reminders = null;

    /**
     * @return Model_Rowset_Reminders
     */
    public function getReminders() {
        if (!$this->_reminders) {
            $this->_reminders = $this->findDependentRowset('Model_Reminder');
        }
        return $this->_reminders;
    }

    /**
     * @return Model_Rowset_Reminders
     */
    public function getRemindersAtivo() {
        if (!$this->_reminders) {
            $reminderModel = new Model_Reminder();

            $select = $reminderModel->select()->where('status != ?', 'deletado');

            $this->_reminders = $this->findDependentRowset('Model_Reminder', null, $select);
        }
        return $this->_reminders;
    }

}