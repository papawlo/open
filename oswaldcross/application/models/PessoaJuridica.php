<?php

class Model_PessoaJuridica extends App_Db_Table_Abstract {

    protected $_name = 'pessoa_juridica';
    protected $_primary = 'id';

    /**
     * Refence Map
     */
    protected $_referenceMap = array(
        'Cadastrado' => array(
            'columns' => 'cadastrado_por',
            'refTableClass' => 'Model_Usuario',
            'refColumns' => 'id'
        ),
        'Editado' => array(
            'columns' => 'editado_por',
            'refTableClass' => 'Model_Usuario',
            'refColumns' => 'id'
        )
    );

}