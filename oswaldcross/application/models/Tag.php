<?php

class Model_Tag extends App_Db_Table_Abstract {

    protected $_name = 'tag';
    protected $_primary = 'id_tag';

    /**
     * Dependent tables
     */
    protected $_dependentTables = array('produto_has_tags');

}