<?php

class Model_Role extends App_Db_Table_Abstract {

    protected $_name = 'role';
    protected $_primary = 'id';
     protected $_dependentTables = array('Model_Usuario', 'Model_Permission');
    
    /*protected $_referenceMap = array(
        'GruposPai' => array(
            'columns' => 'parent_grupo',
            'refTableClass' => 'Model_Grupo',
            'refColumns' => 'id_grupo'
        ),
       
    );*/
}