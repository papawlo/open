<?php

class Model_Pessoa extends App_Db_Table_Abstract {

    protected $_name = 'pessoa';
    protected $_primary = 'id';
    protected $_labels = array(
        'id' => 'ID',
        'nome' => 'Nome',
        'razao_social' => 'Razão Social',
        'cpf_cnpj' => 'CPF/CNPJ',
        'profissao' => 'Profissão',
        'cargo' => 'Cargo',
        'endereco' => 'Endereço',
        'bairro' => 'Bairro',
        'cidade' => 'Cidade',
        'uf' => 'UF',
        'cep' => 'CEP',
        'data_cadastro' => 'Data do Cadastro',
        'data_edicao' => 'Data da Edição',
        'cadastrado_por' => 'Cadastrado por',
        'editado_por' => 'Editado por',
        'status' => 'Status',
        'categoria_id' => 'Categoria',
    );

    /**
     * Refence Map
     */
    protected $_referenceMap = array(
        'Categorias' => array(
            'columns' => 'categoria_id',
            'refTableClass' => 'Model_Categoria',
            'refColumns' => 'id'
        ),
        'Cadastrado' => array(
            'columns' => 'cadastrado_por',
            'refTableClass' => 'Model_Usuario',
            'refColumns' => 'id'
        ),
        'Editado' => array(
            'columns' => 'editado_por',
            'refTableClass' => 'Model_Usuario',
            'refColumns' => 'id'
        )
    );

    public function getLabel($campo) {
        return $this->_labels[$campo];
    }

    /**
     * @return Model_Rowset_Usuario
     */
    public static function getUsuarioNomeById($id) {
        if ($id) {
            $usuarioModel = new Model_Usuario();
            $row = $usuarioModel->find($id);
            if ($row)
                return $row->current()->nome;
        }
        return false;
    }

    /**
     * @return Model_Rowset_Categoria
     */
    public static function getCategoriaNomeById($id) {
        if ($id) {
            $categoriaModel = new Model_Categoria();
            $row = $categoriaModel->find($id);
            if ($row)
                return $row->current()->nome;
        }
        return false;
    }

}