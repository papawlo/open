<?php

class Model_Bairro extends App_Db_Table_Abstract {

    protected $_name = 'bairros';
    protected $_primary = 'id_bairro';
    protected $_dependentTables = array('enderecos, estabelecimento_bairro_valor');

    /**
     * Refence Map
     */
    protected $_referenceMap = array(
        'Cidades' => array(
            'columns' => 'cidade',
            'refTableClass' => 'Model_Cidade',
            'refColumns' => 'id_cidade'
        )
    );

    public function getByCidade($cidade, $order = 'nome ASC', $limit = 0) {
        $modelBairro = new Model_Bairro();
        $select = $modelBairro->select()
                ->where('cidade = ?', $cidade)
                ->order($order);

        if ($limit) {
            $select->limit($limit);
        }
        
        return $this->fetchAll($select);
    }

    public function findBairro($bairro, $cidade = 0) {
        $modelBairro = new Model_Bairro();
        $select = $modelBairro->select()
                ->where('id_bairro = ?', $bairro);

        if ($cidade) {
            $select->where('cidade = ?', $cidade);
        }

        $select->limit(1);

        return $this->fetchRow($select);
    }

}