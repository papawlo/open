<?php

class Model_Users extends App_Db_Table_Abstract {

    protected $_name = 'users';
    protected $_primary = 'id';

    /**
     * Dependent tables
     */
    protected $_dependentTables = array('Model_Authentications', 'Model_UsersHasContacts', 'Model_Relationship');

    /**
     * Row class
     */
    protected $_rowClass = 'Model_Row_User';

    /**
     * Refence Map
     */
    protected $_referenceMap = array(
        'Foto' => array(
            'columns' => 'media_id',
            'refTableClass' => 'Model_Media',
            'refColumns' => 'id'
        ),
    );

    public function getDadosProviders($id) {
        $accountsRowset = $this->find($id);
        $user = $accountsRowset->current();

        return $user->findDependentRowset('Model_Authentications');
    }

    public static function login($email, $password) {

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();

        $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
        $authAdapter->setTableName('users')
                ->setIdentityColumn('email')
                ->setCredentialColumn('password')
                ->setCredentialTreatment('? AND status = "ativo"');

        $authAdapter->setIdentity($email)
                ->setCredential($password);

        $select = $authAdapter->getDbSelect();

        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($authAdapter);

        if ($result->isValid()) {

            $info = $authAdapter->getResultRowObject(null, 'password');
            $userModel = new Model_Users;
            $accountsRowset = $userModel->find($info->id);
            
            $user = $accountsRowset->current();
            $user->ip_last_login = $userModel->get_client_ip();
            $user->date_last_login = date('Y-m-d H:i:s');
            $user->save();

            $storage = $auth->getStorage();
            $storage->write($info);
            return true;
        } else {
            return false;
        }
    }

    public static function loginProvider($provider, $provider_uid) {
        $authenticationModel = new Model_Authentications;
        $authentication_info = $authenticationModel->find_by_provider_uid($provider, $provider_uid);
        if (!$authentication_info) {
            return false;
        }
        $usersModel = new Model_Users;
        $userRow = $usersModel->find($authentication_info->users_id)->current();

        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
        $authAdapter->setTableName('users')
                ->setIdentityColumn('id')
                ->setCredentialColumn('password')
                ->setCredentialTreatment('? AND status = "ativo"');

        $authAdapter->setIdentity($userRow->id)
                ->setCredential($userRow->password);

// get select object (by reference)
//        $select = $authAdapter->getDbSelect();
//        $select->where('active = "TRUE"');
// authenticate, this ensures that users.active = TRUE


        $auth = Zend_Auth::getInstance();

        $result = $auth->authenticate($authAdapter);

        // What to do when invalid
        if ($result->isValid()) {

            $info = $authAdapter->getResultRowObject(null, 'password');

            $storage = $auth->getStorage();
            $storage->write($info);
            return true;
        } else {
            return false;
        }

        return false;
    }

    function create($email, $password, $first_name, $last_name, $display_name, $gender, $photo_url, $birthDay, $birthMonth, $birthYear, $region, $endereco, $cidade, $zip, $pais) {

        $userRow = $this->createRow();
        $userRow->email = $email;
        $userRow->first_name = $first_name;
        $userRow->last_name = $last_name;
        $userRow->display_name = $display_name;
        $userRow->password = md5($password);
        $userRow->data_nascimento = $birthYear . '-' . $birthMonth . "-" . $birthDay;
        if ($gender == 'male') {
            $userRow->genero = 'm';
        }
        if ($gender == 'female') {
            $userRow->genero = 'f';
        }
        $userRow->foto_url = $photo_url;
        $userRow->endereco = $endereco;
        $userRow->regiao = $region;
        $userRow->cidade = $cidade;
        $userRow->estado = $zip;
        $userRow->pais = $pais;
        $userRow->created_at = date('Y-m-d H:i:s');
        return $userRow->save();
    }

//    function update($user_id, $email, $password, $first_name, $last_name) {
//        $sql = "UPDATE users SET email = '$email', password = '$password', first_name = '$first_name', last_name = '$last_name' WHERE id = '$user_id' LIMIT 1";
//        return mysql_query_excute($sql);
//        $userRow = $this->find($user_id)->current();
//        $userRow->first_name = $first_name;
//        $userRow->last_name = $last_name;
//        $userRow->email = $email;
//        $userRow->password = $password;
//        return $userRow->save();
//    }

    function find_by_id($id) {

        return $this->find($id)->current();
    }

    function find_by_email($email) {

        $select = $this->select()->where('email = ?', $email)->where('status = ?', 'ativo')->limit(1);
        return $this->fetchRow($select);
    }

    function find_by_email_and_password($email, $password) {

        $select = $this->select()->where('email = ?', $email)->where('password = ?', $password)->where('status = ?', 'ativo')->limit(1);
        return $result = $this->fetchRow($select);
    }

    function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
 
    return $ipaddress;
}

}