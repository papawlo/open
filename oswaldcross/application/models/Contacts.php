<?php

class Model_Contacts extends App_Db_Table_Abstract {

    protected $_name = 'contacts';
    protected $_primary = 'id';

    /**
     * Dependent tables
     */
    protected $_dependentTables = array('Model_UsersHasContacts');

//    protected $_rowClass = 'Model_Row_Contact';
}