<?php

class Model_Auth {

    public static function login($login, $senha) {
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();

        $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
        $authAdapter->setTableName('usuario')
                ->setIdentityColumn('login')
                ->setCredentialColumn('senha')
                ->setCredentialTreatment('MD5(?)');

        $authAdapter->setIdentity($login)
                ->setCredential($senha);

        $select = $authAdapter->getDbSelect();
        $select->where('status = "ativo"');

        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($authAdapter);
//        print_r($result);
        if ($result->isValid()) {

            $info = $authAdapter->getResultRowObject(null, 'senha');
//            print_r($info);
//            exit();


            $storage = $auth->getStorage();
            $storage->write($info);
            return $result->getCode();
        } else {
            return $result->getCode();
        }
    }

}
