<?php
class Model_Estado extends Zend_Db_Table_Abstract
{
    protected $_name = 'estados';

    protected $_primary = 'id_estado';

    protected $_dependentTables = array('cidades,enderecos');
}