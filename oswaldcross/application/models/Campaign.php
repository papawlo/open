<?php

class Model_Campaign extends App_Db_Table_Abstract {

    protected $_name = 'campaign';
    protected $_primary = 'id';

    /**
     * Refence Map
     */
    protected $_referenceMap = array(
        'Advertiser' => array(
            'columns' => 'advertise_id',
            'refTableClass' => 'Model_Avertiser',
            'refColumns' => 'id'
        )
    );
}