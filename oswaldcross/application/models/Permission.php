<?php

class Model_Permission extends App_Db_Table_Abstract {

    protected $_name = 'permission';
    protected $_primary = 'id';

    protected $_referenceMap = array(
        'Role' => array(
            'columns' => 'role_id',
            'refTableClass' => 'Model_Role',
            'refColumns' => 'id'
        ),
        'Resource' => array(
            'columns' => 'resource_id',
            'refTableClass' => 'Model_Resource',
            'refColumns' => 'id'
        ),
	);	
}