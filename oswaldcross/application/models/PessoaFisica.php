<?php

class Model_PessoaFisica extends App_Db_Table_Abstract {

    protected $_name = 'pessoa_fisica';
    protected $_primary = 'id';
    
/**
     * Refence Map
     */
    protected $_referenceMap = array(
        'Categorias' => array(
            'columns' => 'categoria_id',
            'refTableClass' => 'Model_Categoria',
            'refColumns' => 'id'
        ),
        'Cadastrado' => array(
            'columns' => 'cadastrado_por',
            'refTableClass' => 'Model_Usuario',
            'refColumns' => 'id'
        ),
        'Editado' => array(
            'columns' => 'editado_por',
            'refTableClass' => 'Model_Usuario',
            'refColumns' => 'id'
        )
    );
}