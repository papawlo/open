<?php

class Model_UsersHasContacts extends App_Db_Table_Abstract {

    /**
     * The default table name
     */
    protected $_name = 'users_has_contacts';
    protected $_primary = array('users_id', 'contacts_id');

    /**
     * Reference map
     */
    protected $_referenceMap = array
        (
        'Contacts' => array(
            'refTableClass' => 'Model_Contacts',
            'refColumns' => 'id',
            'columns' => 'contacts_id'
        ),
        'Users' => array(
            'refTableClass' => 'Model_Users',
            'refColumns' => 'id',
            'columns' => 'users_id'
        )
    );

}