<?php
class Model_Categoria extends Zend_Db_Table_Abstract
{
    protected $_name = 'categoria';
    protected $_primary = 'id';

    protected $_dependentTables = array('Model_PessoaFisica', 'Model_PessoaJuridica');
}