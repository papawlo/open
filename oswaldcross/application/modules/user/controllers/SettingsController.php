<?php

class User_SettingsController extends App_Controller_User {

    public function initialize() {
        $this->model = new Model_Users();
        $this->view->pageTitle = 'Settings';
        $this->view->pageRel = 'settings';
        /**
         * ISO-3166-1 http://en.wikipedia.org/wiki/ISO_3166-1
         */
        $countries = array(
            'AF' => 'Afghanistan',
            'AX' => 'Åland Islands',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'AS' => 'American Samoa',
            'AD' => 'Andorra',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctica',
            'AG' => 'Antigua and Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AW' => 'Aruba',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BT' => 'Bhutan',
            'BO' => 'Bolivia, Plurinational State of',
            'BA' => 'Bosnia and Herzegovina',
            'BW' => 'Botswana',
            'BV' => 'Bouvet Island',
            'BR' => 'Brazil',
            'IO' => 'British Indian Ocean Territory',
            'BN' => 'Brunei Darussalam',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodia',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'CV' => 'Cape Verde',
            'KY' => 'Cayman Islands',
            'CF' => 'Central African Republic',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CX' => 'Christmas Island',
            'CC' => 'Cocos (Keeling) Islands',
            'CO' => 'Colombia',
            'KM' => 'Comoros',
            'CG' => 'Congo',
            'CD' => 'Congo, the Democratic Republic of the',
            'CK' => 'Cook Islands',
            'CR' => 'Costa Rica',
            'CI' => "Côte d'Ivoire",
            'HR' => 'Croatia',
            'CU' => 'Cuba',
            'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'DK' => 'Denmark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'El Salvador',
            'GQ' => 'Equatorial Guinea',
            'ER' => 'Eritrea',
            'EE' => 'Estonia',
            'ET' => 'Ethiopia',
            'FK' => 'Falkland Islands (Malvinas)',
            'FO' => 'Faroe Islands',
            'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            'GF' => 'French Guiana',
            'PF' => 'French Polynesia',
            'TF' => 'French Southern Territories',
            'GA' => 'Gabon',
            'GM' => 'Gambia',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Greece',
            'GL' => 'Greenland',
            'GD' => 'Grenada',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GG' => 'Guernsey',
            'GN' => 'Guinea',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HM' => 'Heard Island and McDonald Islands',
            'VA' => 'Holy See (Vatican City State)',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran, Islamic Republic of',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IM' => 'Isle of Man',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JE' => 'Jersey',
            'JO' => 'Jordan',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KI' => 'Kiribati',
            'KP' => "Korea, Democratic People's Republic of",
            'KR' => 'Korea, Republic of',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyzstan',
            'LA' => "Lao People's Democratic Republic",
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libyan Arab Jamahiriya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macao',
            'MK' => 'Macedonia, the former Yugoslav Republic of',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'YT' => 'Mayotte',
            'MX' => 'Mexico',
            'FM' => 'Micronesia, Federated States of',
            'MD' => 'Moldova, Republic of',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'ME' => 'Montenegro',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'NL' => 'Netherlands',
            'AN' => 'Netherlands Antilles',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Niue',
            'NF' => 'Norfolk Island',
            'MP' => 'Northern Mariana Islands',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PK' => 'Pakistan',
            'PW' => 'Palau',
            'PS' => 'Palestinian Territory, Occupied',
            'PA' => 'Panama',
            'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            'PN' => 'Pitcairn',
            'PL' => 'Poland',
            'PT' => 'Portugal',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'RE' => 'Réunion',
            'RO' => 'Romania',
            'RU' => 'Russian Federation',
            'RW' => 'Rwanda',
            'BL' => 'Saint Barthélemy',
            'SH' => 'Saint Helena',
            'KN' => 'Saint Kitts and Nevis',
            'LC' => 'Saint Lucia',
            'MF' => 'Saint Martin (French part)',
            'PM' => 'Saint Pierre and Miquelon',
            'VC' => 'Saint Vincent and the Grenadines',
            'WS' => 'Samoa',
            'SM' => 'San Marino',
            'ST' => 'Sao Tome and Principe',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'RS' => 'Serbia',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SK' => 'Slovakia',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Islands',
            'SO' => 'Somalia',
            'ZA' => 'South Africa',
            'GS' => 'South Georgia and the South Sandwich Islands',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'SD' => 'Sudan',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard and Jan Mayen',
            'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syrian Arab Republic',
            'TW' => 'Taiwan, Province of China',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania, United Republic of',
            'TH' => 'Thailand',
            'TL' => 'Timor-Leste',
            'TG' => 'Togo',
            'TK' => 'Tokelau',
            'TO' => 'Tonga',
            'TT' => 'Trinidad and Tobago',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TC' => 'Turks and Caicos Islands',
            'TV' => 'Tuvalu',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'US' => 'United States',
            'UM' => 'United States Minor Outlying Islands',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VE' => 'Venezuela, Bolivarian Republic of',
            'VN' => 'Viet Nam',
            'VG' => 'Virgin Islands, British',
            'VI' => 'Virgin Islands, U.S.',
            'WF' => 'Wallis and Futuna',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe'
        );

        $this->view->paises = $countries;
    }

    protected function indexAction() {
        $this->view->pageRel = 'settings';
        $auth = Zend_Auth::getInstance();

        $info = $auth->getStorage()->read();

        if ($info->id) {
            $userModel = new Model_Users();
            $usersRowset = $userModel->find($info->id);
            $this->view->row = $usersRowset->current();
            $this->view->providers = $userModel->getDadosProviders($info->id);
        } else {
            $this->view->row = null;
        }
    }

    protected function beforeSave(&$row, $action) {
        $filters = array(
            '*' => 'StringTrim',
        );
        $validators = array(
            'email' => array(
                'allowEmpty' => false,
                new Zend_Validate_EmailAddress()
            ),
            'genero' => array(
                new Zend_Validate_InArray(
                        array(
                    'f' => 'f',
                    'm' => 'm'
                        )
                )
            ),
            'dia' => array('digits',
                new Zend_Validate_Digits(),
                'messages' => array(
                    'O dia deve conter apenas digitos'
                )
            ),
            'mes' => array('digits',
                new Zend_Validate_Digits(),
                'messages' => array(
                    'O mês deve conter apenas digitos'
                )
            ),
            'ano' => array('digits',
                new Zend_Validate_Digits(),
                'messages' => array(
                    'O ano deve conter apenas digitos'
                )
            )
        );
        $options = array(
            'notEmptyMessage' => "O campo '%%field%%' deve ser preenchido adequadamente"
        );

        $params = $this->getRequest()->getParams();
        $input = new Zend_Filter_Input($filters, $validators, $params, $options);
        if (!$input->isValid()) {


            foreach ($input->getMessages() as $campo => $messages) {
                foreach ($messages as $key => $message) {
                    $retorno = array("tipo" => "error", "tipo_msg" => "Ocorreu um erro no campo $campo!!", "msg_info" => $message);
                }
            }

            $this->_helper->json($retorno);
            exit;
        }
//        $dados = $this->getPost();


        $row->data_nascimento = $input->ano . '-' . $input->mes . '-' . $input->dia;


        $userAtual = $this->model->find($row->id)->current();
        $senhaAtual = $userAtual->password;

        $row->password = ($input->password) ? md5($input->password) : $senhaAtual;
    }

    public function fotoAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $output_dir = realpath(APPLICATION_PATH . '/../public/uploads/');

        $allowed = array('png', 'jpg', 'gif');

        $extension = pathinfo($_FILES['myfile']['name'], PATHINFO_EXTENSION);

        if (!in_array(strtolower($extension), $allowed)) {
            echo '{"status":"error","msg":"Tipo de arquivo não permitido"}';
            exit;
        }
//        print_r($_FILES["myfile"]);
//        exit();


        if (isset($_FILES["myfile"])) {
            //Filter the file types , if you want.
            if ($_FILES["myfile"]["error"] > 0) {
                echo "Error: " . $_FILES["myfile"]["error"] . "<br>";
            } else {

                $mediaUpload = new App_Helpers_Media($_FILES["myfile"]);
                $media_id = $mediaUpload->sendFile('user');
                if ($media_id) {
                    $auth = Zend_Auth::getInstance();

                    $info = $auth->getStorage()->read();

                    $userModel = new Model_Users();
                    $usersRowset = $userModel->find($info->id);
                    $row = $usersRowset->current();
                    $row->media_id = $media_id;
                    $row->save();

                    $this->_helper->json(array("status" => "success", "nome" => Model_Media::getThumbnail($media_id, 'user', 'site')));
                }
            }
        }
// A list of permitted file extensions
//        $allowed = array('png', 'jpg', 'gif', 'zip');
//
//        if (isset($_FILES['upl']) && $_FILES['upl']['error'] == 0) {
//
//            $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);
//
//            if (!in_array(strtolower($extension), $allowed)) {
//                echo '{"status":"error"}';
//                exit;
//            }
//            if (move_uploaded_file($_FILES['upl']['tmp_name'], PUBLIC_PATH . '/uploads/' . $_FILES['upl']['name'])) {
//                echo '{"status":"success"}';
//                exit;
//            }
//        }
//        try {
//        $adapter = new Zend_File_Transfer_Adapter_Http();
//        $adapter->addValidator('Count', false, 1)
//                ->addValidator('Size', false, array('max' => "2MB"))
//                ->addValidator('Extension', false, 'jpg,jpeg,png,gif');
//                ->addValidator('IsImage', false);
//        $files = $adapter->getFileInfo();
//        $errors = '';
//        if (!$adapter->receive()) {
//            $errors = 'unknown error ';
//            if ($adapter->hasErrors()) {
//                $this->_helper->json(array("status" => "error", "msg" => $adapter->getErrors()));
//                exit();
//            }
//        }
//        print_r($errors);
//        foreach ($files as $fieldname => $fileinfo) {
//            if (($adapter->isUploaded($fileinfo["name"])) && ($adapter->isValid($fileinfo['name']))) {
//                $mediaUpload = new App_Helpers_Media($fileinfo);
//                $media_id = $mediaUpload->sendFile('user');
//                if ($media_id) {
//                    $auth = Zend_Auth::getInstance();
//
//                    $info = $auth->getStorage()->read();
//
//                    $userModel = new Model_Users();
//                    $usersRowset = $userModel->find($info->id);
//                    $row = $usersRowset->current();
//                    $row->media_id = $media_id;
//                    $row->save();
//
//                    $this->_helper->json(array("status" => "success", "nome" => $fileinfo['name']));
//                }
//            } else {
//                $this->_helper->json(array("status" => "error", "msg" => $adapter->getErrors()));
//            }
//        }
//        } catch (Exception $ex) {
//           echo  $this->_helper->json(array("status" => "error", "msg" => $ex->getMessage()));
//            $this->addFlash("error", $ex->getMessage());
//        }
//          $this->_helper->json(array("status" => "teste"));
        exit;
    }

    public function validateAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $dados = $this->getPost();

        if ($dados) {

            $modelEstabelecimento = new Model_Usuario();

            switch ($dados['acao']) {

                case 'email':

                    $select = $modelEstabelecimento->select()->where('email = ?', $dados['dado'])
                            ->where('status != ?', 'deletado')
                            ->where('role IN (?)', 'cliente');

                    $result = $modelEstabelecimento->fetchAll($select);

                    $rowCount = count($result);

                    if ($rowCount > 0) {
                        echo 'invalid';
                    } else {
                        echo 'valid';
                    }

                    break;

                case 'login':

                    $select = $modelEstabelecimento->select()->where('login = ?', $dados['dado'])
                            ->where('status != ?', 'deletado')
                            ->where('role IN (?)', 'cliente');

                    $result = $modelEstabelecimento->fetchAll($select);

                    $rowCount = count($result);

                    if ($rowCount > 0) {
                        echo 'invalid';
                    } else {
                        echo 'valid';
                    }

                    break;
            }
        }
    }

    public function getCountry($code) {
        $code = strtoupper($code);
        if (array_key_exists($code, $countries)) {
            return $countries[$code];
        }
        return false;
    }

}

