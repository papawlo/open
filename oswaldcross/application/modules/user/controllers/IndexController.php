<?php

class User_IndexController extends App_Controller_User {

    public function initialize() {
        $this->model = new Model_Users();
        $this->view->pageTitle = 'Index';
    }

    public function indexAction() {
        $this->view->pageRel = 'index';
        $auth = Zend_Auth::getInstance();

        $info = $auth->getStorage()->read();

        $usersModel = new Model_Users();
        $userRowSet = $usersModel->find($info->id);
        $userRow = $userRowSet->current();
// provider like twitter, linkedin, do not provide the user email
// in this case, we should ask them to complete their profile before continuing
        if (!$userRow->email) {
            $this->_helper->redirector('index', 'settings', 'user');
        }
    }

    public function logoutAction() {

        Zend_Auth::getInstance()->clearIdentity();

// config and whatnot
        if (isset($_SESSION['token'])) {
            unset($_SESSION['token']);
        }

        $config = APPLICATION_PATH . "/../hybridauth/config.php";

        require_once( APPLICATION_PATH . "/../hybridauth/Hybrid/Auth.php" );

        try {
            $hybridauth = new Hybrid_Auth($config);

// logout the user from $provider
            $hybridauth->logoutAllProviders();
        } catch (Exception $e) {
            echo "<br /><br /><b>Oh well, we got an error :</b> " . $e->getMessage();

            echo "<hr /><h3>Trace</h3> <pre>" . $e->getTraceAsString() . "</pre>";
            exit;
        }
        // return to login page
        return $this->_redirect('/index');
    }

    public function eventsReminderAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $auth = Zend_Auth::getInstance();

        $info = $auth->getStorage()->read();

        $usersModel = new Model_Users();
        $userRowSet = $usersModel->find($info->id);
        $userRow = $userRowSet->current();
        $from = $this->_request->getParam('from', null);
        $dataFrom = date('d/m/Y', substr($from, 0, 10));
//        echo "From:" . $dataFrom;
//        echo "\n";
        list($diaFrom, $mesFrom, $anoFrom) = explode("/", $dataFrom);

        $to = $this->_request->getParam('to', null);
        $dataTo = date('d/m/Y', substr($to, 0, 10));
//        echo "To: " . $dataTo;
//        echo "\n";
        list($diaTo, $mesTo, $anoTo) = explode("/", $dataTo);



        $relationshipModel = new Model_Relationship();
        $select = $relationshipModel->select()
                ->setIntegrityCheck(false)
                ->from(array('r' => 'relationship'), array('data_inicio', 'titulo'))
                ->joinInner(array('rm' => 'reminder'), 'r.id = rm.relationship_id')
                ->where('r.users_id = ?', $userRow->id)
                ->where('r.status != ?', 'deletado')
                ->where('rm.status != ?', 'deletado');

//        echo $select->__toString();
//        exit;
        $dateReminderers = $relationshipModel->fetchAll($select);
        $events = array();
        if (count($dateReminderers)) {
            $events["success"] = 1;

//            print_r($dateReminderers->toArray());
//            exit();

            foreach ($dateReminderers as $dateReminder) {
//            $data_inicio = $dateReminder->data_dia_notification . '/' . $dateReminder->data_mes_notification . '/' . $dateReminder->data_ano_notification . "<br>\n";
//            echo $data_inicio;
                $a_d = '';
                if ($dateReminder->a_d == 'a') {
                    $a_d = '-';
                }
                if ($dateReminder->a_d == 'd') {
                    $a_d = '+';
                }
//            echo "A ou D: " . $a_d . "<br>\n";
                $repeticao = '';
                switch ($dateReminder->repeticao) {
                    case 'd':
                        $repeticao = 'days';
                        break;
                    case 's':
                        $repeticao = 'weeks';
                        break;
                    case 'm':
                        $repeticao = 'month';
                        break;
                    case 'a':
                        $repeticao = 'years';
                        break;
                    default:
                        break;
                }
//            echo "Interevalo:" . $dateReminder->intervalo . "<br>\n";
//            echo "Repeticao: " . $repeticao . "<br>\n";

                $Date = strtotime($dateReminder->data_inicio);
//            echo "ID: " . $dateReminder->data_inicio;
//            echo "\n";
//            echo "data inicio: " . date('d/m/Y', $Date);
//            echo "<br>\n";
                $data_dia_notification = date('d', strtotime("$a_d$dateReminder->intervalo $repeticao", $Date));
//            echo $data_dia_notification;
//            echo "-";
                $data_mes_notification = date('m', strtotime("$a_d$dateReminder->intervalo $repeticao", $Date));
//            echo $data_mes_notification;
//            echo "-";
                $data_ano_notification = date('Y', strtotime("$a_d$dateReminder->intervalo $repeticao", $Date));
//            echo $data_ano_notification;
//            echo "<br>\n\n";
                $start = strtotime($data_dia_notification . '-' . $data_mes_notification . '-' . $anoFrom . " 08:00:00");
//                echo "data notificacao: ";
//                echo date('d/m/Y', $start);
//                echo "<br>";
                $end = strtotime($data_dia_notification . '-' . $data_mes_notification . '-' . $anoFrom . " 18:00:00");
                $classEvent = array("event-success", "event-warning", "event-info", "event-success", "event-inverse", "event-special", "event-important");
//                srand((float) microtime() * 10000000); 
                shuffle($classEvent);
                $class_event = $classEvent[0];
                $events["result"][] = array(
                    "id" => $dateReminder->id,
                    "title" => $dateReminder->titulo,
                    "url" => "#",
                    "class" => $class_event, //"event-warning",
                    "start" => $start . "000",
                    "end" => $end . "000"
                );
//                echo "<br>";echo "<br>";
            }
        } else {
            $events = array("success" => "0", "error" => "error message here");
        }
        $this->_helper->json($events);
    }

    //Lista de Datas
    public function eventsAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $auth = Zend_Auth::getInstance();

        $info = $auth->getStorage()->read();

        $usersModel = new Model_Users();
        $userRowSet = $usersModel->find($info->id);
        $userRow = $userRowSet->current();
        $from = $this->_request->getParam('from', null);
        $dataFrom = date('d/m/Y', substr($from, 0, 10));

        list($diaFrom, $mesFrom, $anoFrom) = explode("/", $dataFrom);

        $to = $this->_request->getParam('to', null);
        $dataTo = date('d/m/Y', substr($to, 0, 10));

        list($diaTo, $mesTo, $anoTo) = explode("/", $dataTo);

        $relationshipModel = new Model_Relationship();
        $select = $relationshipModel->select()
                ->setIntegrityCheck(false)
                ->from(array('r' => 'relationship'), array('id', 'data_inicio', 'titulo'))
                ->where('r.users_id = ?', $userRow->id)
                ->where('r.status != ?', 'deletado');

        $datesReminder = $relationshipModel->fetchAll($select);
        $events = array();
        if (count($datesReminder)) {
            $events["success"] = 1;

            foreach ($datesReminder as $dateReminder) {


                $Date = strtotime($dateReminder->data_inicio);
                list($diaTo, $mesTo, $anoTo) = explode("/", $dataTo);

                $data_dia_notification = date('d', $Date);

                $data_mes_notification = date('m', $Date);

                $data_ano_notification = date('Y', $Date);

                $start = strtotime($data_dia_notification . '-' . $data_mes_notification . '-' . $anoFrom . " 00:00:00");

                $end = strtotime($data_dia_notification . '-' . $data_mes_notification . '-' . $anoFrom . " 23:59:59");
                $classEvent = array("event-success", "event-warning", "event-info", "event-success", "event-inverse", "event-special", "event-important");

                shuffle($classEvent);
                $class_event = $classEvent[0];
                $events["result"][] = array(
                    "id" => $dateReminder->id,
                    "title" => $dateReminder->titulo,
                    "url" => "javascript:void(0)",
                    "class" => $class_event, //"event-warning",
                    "start" => $start . "000",
                    "end" => $end . "000",
                    "date" => $dateReminder->data_inicio
                );
            }
        } else {
            $events = array("success" => "0", "error" => "error message here");
        }
        $this->_helper->json($events);
    }

    public function eventsListAction() {
        $this->_helper->layout->disableLayout();
//        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function monthAction() {
        $this->_helper->layout->disableLayout();
//        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function monthDayAction() {
        $this->_helper->layout->disableLayout();
    }

    public function dayAction() {
        $this->_helper->layout->disableLayout();
    }

    public function weekAction() {
        $this->_helper->layout->disableLayout();
    }

    public function weekDaysAction() {
        $this->_helper->layout->disableLayout();
    }

    public function yearAction() {
        $this->_helper->layout->disableLayout();
    }

    public function yearMonthAction() {
        $this->_helper->layout->disableLayout();
    }

}

