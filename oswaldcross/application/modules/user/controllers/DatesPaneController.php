<?php

class User_DatesPaneController extends App_Controller_User {

    public function initialize() {
        $this->model = new Model_Relationship();
        $this->view->pageTitle = 'Relacionamento';
        $this->view->pageTitleList = 'Listagem';
        $this->view->pageTitleNew = 'Novo';
        $this->view->pageTitleEdit = 'Editar';
    }

    protected function indexAction() {
        $this->view->pageRel = 'dates-pane';
        $auth = Zend_Auth::getInstance();

        $info = $auth->getStorage()->read();

//        unset($_SESSION["token"]);exit;

        if ($info->id) {
            $userModel = new Model_Users();
            $usersRowset = $userModel->find($info->id);
            $userRow = $usersRowset->current();
            // provider like twitter, linkedin, do not provide the user email
            // in this case, we should ask them to complete their profile before continuing
            if (!$userRow->email) {
                $this->addFlash("info", "Informe seu email para continuar.<br />");
                $this->_helper->redirector('index', 'settings', 'user');
            }

            $this->view->rows = $userRow->getRelationshipsAtivo();
            $this->view->tipos = array("Esquema", "Pega", "Namoro", "Noivado", "Casamento", "Bodas");
        }
    }

    protected function beforeSave(&$row, $action) {
        $filters = array(
            '*' => 'StringTrim',
            'intervalo' => 'Digits'
        );
        $validators = array(
            'titulo' => array('allowEmpty' => false),
            'com' => array('allowEmpty' => false),
            'data_inicio' => array(
                'allowEmpty' => false
            ),
            'genero' => array(
                new Zend_Validate_InArray(
                        array(
                    'f' => 'f',
                    'm' => 'm'
                        )
                )
            ),
            'intervalo' => array('digits',
                new Zend_Validate_Digits(),
                'messages' => array(
                    'O intervalo deve conter apenas digitos'
                )
            )
        );
        $options = array(
            'notEmptyMessage' => "O campo '%%field%%' deve ser preenchido adequadamente"
        );

        $params = $this->getRequest()->getParams();
        $input = new Zend_Filter_Input($filters, $validators, $params, $options);
        if (!$input->isValid()) {
//            print_r($input->getMessages());
//            exit();


            foreach ($input->getMessages() as $campo => $messages) {
                 $campo = str_replace('data_inicio', "Data de Início", $campo);
                    $campo = str_replace('titulo', "Título", $campo);
                    $campo = str_replace('com', "Parceiro(a)", $campo);
                    $campo = str_replace('intervalo', "Intervalo", $campo);
                foreach ($messages as $key => $message) {
                    $message = str_replace('%data_inicio%', "Data de Início", $message);
                    $message = str_replace('%titulo%', "Título", $message);
                    $message = str_replace('%com%', "Parceiro(a)", $message);
                    $message = str_replace('%intervalo%', "Intervalo", $message);
                    $retorno = array("tipo" => "error", "tipo_msg" => "Ocorreu um erro no campo $campo!!", "msg_info" => $message);
                }
            }

            $this->_helper->json($retorno);
            exit;
        }

        $auth = Zend_Auth::getInstance();

        $info = $auth->getStorage()->read();


        $row->users_id = $info->id;
//        $data_formatada = str_replace('/', '-', $this->getRequest()->getPost('data_inicio', null));
//        $row->data_inicio = $mysqldate = date('Y-m-d H:i:s', strtotime($data_formatada));
        $data_explodida = explode('de', $this->getRequest()->getPost('data_inicio', null));
        $dia = trim($data_explodida[0]);

        $mes = trim($data_explodida[1]);

        $ano = trim($data_explodida[2]);

        $meses = array(1 => 'janeiro', 2 => 'fevereiro', 3 => 'março', 4 => 'abril', 5 => 'maio', 5 => 'junho',
            7 => 'julho', 8 => 'agosto', 9 => 'setembro', 10 => 'outubro', 11 => 'novembro', 12 => 'dezembro');
        $mes = array_search($mes, $meses);

        $row->data_inicio = date('Y-m-d H:i:s', strtotime($ano . '-' . $mes . '-' . $dia));
        /*  notifications */
        $notification_email = $this->getRequest()->getPost('notification_email', 0);
        if ($notification_email) {
            $row->notification_email = 1;
        } else {
            $row->notification_email = 0;
        }
        /*
         * 
         * 
        $notification_facebook = $this->getRequest()->getPost('notification_facebook', 0);
        if ($notification_facebook) {
            $row->notification_facebook = 1;
        } else {
            $row->notification_facebook = 0;
        }
        
        $notification_gcalendar = $this->getRequest()->getPost('notification_facebook', 0);
        if ($notification_gcalendar) {
            $row->notification_gcalendar = 1;
        } else {
            $row->notification_gcalendar = 0;
        }
         
         * 
         * 
         */
    }

    protected function afterSave(&$row, $action) {
        $reminder_ids = $this->getRequest()->getPost('reminder_id', null);
        $repeticaos = $this->getRequest()->getPost('repeticao', null);
        $intervalos = $this->getRequest()->getPost('intervalo', null);
        $a_ds = $this->getRequest()->getPost('a_d', null);

        foreach ($reminder_ids as $key => $reminder_id) {
            $reminderModel = new Model_Reminder;

            if (empty($reminder_id)) {
                $reminderRow = $reminderModel->createRow();
                $reminderRow->intervalo = $intervalos[$key];
                $reminderRow->repeticao = $repeticaos[$key];
                $reminderRow->a_d = $a_ds[$key];
                $reminderRow->relationship_id = $row->id;

                $a_d = '';
                if ($reminderRow->a_d == 'a') {
                    $a_d = '-';
                }
                if ($reminderRow->a_d == 'd') {
                    $a_d = '+';
                }

                $repeticao = '';
                switch ($reminderRow->repeticao) {
                    case 'd':
                        $repeticao = 'days';
                        break;
                    case 's':
                        $repeticao = 'weeks';
                        break;
                    case 'm':
                        $repeticao = 'month';
                        break;
                    case 'a':
                        $repeticao = 'years';
                        break;
                    default:
                        break;
                }

                $Date = strtotime($row->data_inicio);

                $reminderRow->data_dia_notification = date('d', strtotime("$a_d$reminderRow->intervalo $repeticao", $Date));
                $reminderRow->data_mes_notification = date('m', strtotime("$a_d$reminderRow->intervalo $repeticao", $Date));
                $reminderRow->data_ano_notification = date('Y', strtotime("$a_d$reminderRow->intervalo $repeticao", $Date));

                $reminderRow->save();
            } else {
                $reminderRow = $reminderModel->find($reminder_id)->current();
                $reminderRow->intervalo = $intervalos[$key];
                $reminderRow->repeticao = $repeticaos[$key];
                $reminderRow->a_d = $a_ds[$key];
                $a_d = '';
                if ($reminderRow->a_d == 'a') {
                    $a_d = '-';
                }
                if ($reminderRow->a_d == 'd') {
                    $a_d = '+';
                }

                $repeticao = '';
                switch ($reminderRow->repeticao) {
                    case 'd':
                        $repeticao = 'days';
                        break;
                    case 's':
                        $repeticao = 'weeks';
                        break;
                    case 'm':
                        $repeticao = 'month';
                        break;
                    case 'a':
                        $repeticao = 'years';
                        break;
                    default:
                        break;
                }

                $Date = strtotime($row->data_inicio);

                $reminderRow->data_dia_notification = date('d', strtotime("$a_d$reminderRow->intervalo $repeticao", $Date));
                $reminderRow->data_mes_notification = date('m', strtotime("$a_d$reminderRow->intervalo $repeticao", $Date));
                $reminderRow->data_ano_notification = date('Y', strtotime("$a_d$reminderRow->intervalo $repeticao", $Date));


                $reminderRow->save();
            }
        }
    }

    public function addDateFormAction() {
        $this->_helper->layout->disableLayout();

        $this->view->tipos = array("Esquema", "Pega", "Namoro", "Noivado", "Casamento", "Bodas");
    }

    public function autorizaGoogleAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        require_once 'google-api-php-client/src/Google_Client.php';
        require_once 'google-api-php-client/src/contrib/Google_CalendarService.php';

        $client = new Google_Client(array('use_objects' => true));
        $client->setApplicationName("Google Calendar Novo calendar");

// Visit https://code.google.com/apis/console?api=calendar to generate your
// client id, client secret, and to register your redirect uri.
        $client->setClientId('761533968443.apps.googleusercontent.com');
        $client->setClientSecret('7Wa7aU9U7YD7EIfcwFDJHJS7');
        $client->setRedirectUri('http://www.luvminder.com/user/dates-pane/autoriza-google');
        $client->setDeveloperKey('AIzaSyBn4IXHmwEJuRpc_K9cBMAaHwqUIXIU6u8');
        $service = new Google_CalendarService($client);

        if (isset($_GET['logout'])) {
            unset($_SESSION['token']);
        }

        if (isset($_GET['code'])) {
            $client->authenticate($_GET['code']);
            $_SESSION['token'] = $client->getAccessToken();
        }

        if (isset($_SESSION['token'])) {
            $client->setAccessToken($_SESSION['token']);
        }
        if ($client->getAccessToken()) {
            //LISTA CALENDÁRIOS
            $calList = $service->calendarList->listCalendarList();
            foreach ($calList->items as $calendar) {

                if ($calendar->primary) {
                    $time_zone = $calendar->getTimeZone();
                }
            }

//            CRIA NOVO CALENDÁRIO 'luvminder'
            $calendar = new Google_Calendar();
            $calendar->setSummary('luvminder');
            $calendar->setTimeZone($time_zone);
            $createdCalendar = $service->calendars->insert($calendar);
//            print_r($createdCalendar);
            $calendarId = $createdCalendar->getId();
            //add no banco os dados do calendario
            $googleCalendarAccessModel = new Model_GoogleCalendarAccess();

            $googleCalendarAccessNewRow = $googleCalendarAccessModel->createRow();
            //RECUPERA DADOS DO USUÁRIO NA SESSAO
            $auth = Zend_Auth::getInstance();
            $info = $auth->getStorage()->read();
            $user_id = $info->id;

            $newDataArray = array(
                'token' => $_SESSION['token'],
                'calendar_id' => $calendarId,
                'users_id' => $user_id,
                'updated_at' => date('Y-m-d H:i:s'),
                'status' => 'ativo');

            $googleCalendarAccessNewRow->setFromArray($newDataArray);
            $googleCalendarAccessNewRow->save();
//            NOVO EVENTO
//            $event = new Google_Event();
//            $event->setSummary('Aniversário de Casamento');
//            $event->setLocation('João Pessoa, PB');
//            $start = new Google_EventDateTime();
//            $start->setTimeZone($time_zone);
//            $start->setDateTime('2013-08-05T14:50:00');
//            $event->setStart($start);
//            $end = new Google_EventDateTime();
//            $end->setTimeZone($time_zone);
//            $end->setDateTime('2013-08-05T15:39:00');
//            $event->setEnd($end);
//
//            $createdEvent = $service->events->insert($calendarId, $event);
//            print_r($createdEvent);

            $_SESSION['token'] = $client->getAccessToken();
        }
        print_r($_SESSION);
    }

    public function comAutocompleteAction() {
// disable view and layout, we want some fanzy json returned
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $auth = Zend_Auth::getInstance();
        $info = $auth->getStorage()->read();
        $user_id = $info->id;

        $relationshipModel = new Model_Relationship();
        $select = $relationshipModel->select()->distinct()->from($relationshipModel, "com")->where("status = ?", 'ativo')->where("users_id = ?", $user_id);
        $comRow = $relationshipModel->fetchAll($select)->toArray();

        $buscadoRow2 = array();
        if ($comRow) {
            foreach ($comRow as $value) {
                $buscadoRowArray[] = $value['com'];
            }
            $buscadoRow2 = array_values($buscadoRowArray);
        }


        $valuesJson = Zend_Json::encode($buscadoRow2);
        echo $valuesJson;
    }

}

