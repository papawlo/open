<?php

class User_ReminderController extends App_Controller_User {

    public function initialize() {
        $this->model = new Model_Reminder();
        $this->view->pageTitle = 'Lembretes';
    }

    protected function indexAction() {
        $this->view->pageRel = 'index';
        $auth = Zend_Auth::getInstance();

        $info = $auth->getStorage()->read();

        $usersModel = new Model_Users();
        $userRowSet = $usersModel->find($info->id);
        $userRow = $userRowSet->current();
        // provider like twitter, linkedin, do not provide the user email
        // in this case, we should ask them to complete their profile before continuing
        if (!$userRow->email) {
            $this->_helper->redirector('index', 'settings', 'user');
        }
        $this->view->row = $userRow;
        if ($userRow->id) {
            $page = $this->_getParam('page', 1);
            $per_page = $this->_getParam('per_page', 20);

            $reminderRowset = $userRow->getRemindersByUser();
//            print_r($reminderRowset);
//            exit();

            $this->view->paginator = $this->paginator($reminderRowset, $per_page, $page);
        }

        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }

    protected function visualizarAction() {
        $this->_helper->layout->disableLayout();

        $id = $this->_request->getParam('id', 0);

        $rowSet = $this->model->find($id);
        $row = $rowSet->current();

        $this->view->row = $row;
    }

    protected function beforeSave(&$row, $action) {
        $row->user_id = array("relationship_id" => $row->relationship_id);
    }

//    protected function afterSave(&$row, $action) {
//        $this->params = array("relationship_id" => $row->relationship_id);
//    }
}

