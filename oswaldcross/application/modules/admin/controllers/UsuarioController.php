<?php

class Admin_UsuarioController extends App_Controller_Admin {

    public function initialize() {
        $this->model = new Model_Usuario();
        $this->view->pageTitle = 'Usuários';
        $this->view->pageTitleList = 'Listagem';
        $this->view->pageTitleNew = 'Novo';
        $this->view->pageTitleEdit = 'Editar';

        $role = new Model_Role();
        $_roles = $role->fetchAll("status ='ativo'");
        $this->view->roles = $_roles;
    }

    protected function indexAction() {

        $page = $this->_getParam('page', 1);
        $per_page = $this->_getParam('per_page', 20);

        $rows = $this->model->fetchAll($this->select);

        $this->view->paginator = $this->paginator($rows, $per_page, $page);

        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }

    protected function defineSelect() {
//        $this->select->where('role IN (?)', array('superadmin'));
    }

    protected function beforeSave(&$row, $action) {
        $dados = $this->getPost();
//        print_r($dados);
//        exit();
        $roleModel = new Model_Role();
        $roleRow = $roleModel->find($row->role_id)->current();
        $row->role = $roleRow->role;
        if ($action == 'new') {
            $row->data_cadastro = date('Y-m-d H:i:s');
            $row->status = 'ativo';
            $row->senha = ($dados['senha']) ? md5($dados['senha']) : md5('123456');
        } else {
            $userAtual = $this->model->find($row->id)->current();
            $senhaAtual = $userAtual->senha;

            $row->senha = ($dados['senha']) ? md5($dados['senha']) : $senhaAtual;
        }

        if (!$dados['login']) {
            $row->login = $dados['email'];
        }
//        print_r($row->toArray());
//        exit();
        }

    public function afterSave(&$row, $action) {
        //aqui fica o envio de e-mail.
    }

    public function validateAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $dados = $this->getPost();

        if ($dados) {

            $modelEstabelecimento = new Model_Usuario();

            switch ($dados['acao']) {

                case 'email':

                    $select = $modelEstabelecimento->select()->where('email = ?', $dados['dado'])
                            ->where('status != ?', 'deletado')
                            ->where('role IN (?)', 'superadmin');

                    $result = $modelEstabelecimento->fetchAll($select);

                    $rowCount = count($result);

                    if ($rowCount > 0) {
                        echo 'invalid';
                    } else {
                        echo 'valid';
                    }

                    break;

                case 'login':

                    $select = $modelEstabelecimento->select()->where('login = ?', $dados['dado'])
                            ->where('status != ?', 'deletado')
                            ->where('role IN (?)', 'superadmin');

                    $result = $modelEstabelecimento->fetchAll($select);

                    $rowCount = count($result);

                    if ($rowCount > 0) {
                        echo 'invalid';
                    } else {
                        echo 'valid';
                    }

                    break;
            }
        }
    }

}

