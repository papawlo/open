<?php

class Admin_MediaController extends App_Controller_Admin {

    public function initialize() {
        $this->model = new Model_Media();
        $this->view->pageTitle = 'Imagem';
    }

    public function imageViewAction() {
        $this->_helper->layout->disableLayout();
        $ajax = $this->isXmlHttpRequest();

        if ($ajax) {
            $id = $this->_request->getParam('id', 0);
            $slug = $this->_request->getParam('slug', 0);
            $controller = $this->_request->getParam('controlador', 0);
            if ($id) {

                $config = new Zend_Config_Ini(APPLICATION_PATH . "/modules/admin/configs/images.ini");

                $dimensions = array('width' => $config->$controller->$slug->width, 'height' => $config->$controller->$slug->height);

                $this->view->dimensions = $dimensions;
                $this->view->slug = $slug;
                $this->view->id = $id;
                $this->view->controlador = $controller;
            } else {
                $this->view->dimensions = array();
                $this->view->row = array();
            }
        } else {
            return $this->_helper->redirector->goToRoute(array('module' => 'admin', 'controller' => 'produto', 'action' => 'index'), null, true);
        }
    }

    public function cropImageAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $ajax = $this->isXmlHttpRequest();

        if ($ajax) {
            if ($this->isPost()) {
                $dados = $this->getPost();


                $id = (int) $dados['id'];
                $left = (int) $dados['x'];
                $top = (int) $dados['y'];
                $right = (int) $dados['x2'];
                $bottom = (int) $dados['y2'];
                $width = (int) $dados['w'];
                $height = (int) $dados['h'];
                $slug = $dados['slug'];
                $controlador = $dados['controlador'];
//1st: dynamically calculate the dimensions
                $crop_area_width = $right - $left;      // width of cropped selection
                $crop_area_height = $bottom - $top;      // height of cropped selection
                $mediaModel = new Model_Media();
                $row = $mediaModel->find($id)->current();

                $retorno = $mediaModel->customCrop($row, $controlador, $slug, $top, $left, $crop_area_width, $crop_area_height);
                $valuesJson = Zend_Json::encode($retorno);
                echo $valuesJson;
            }
        }
    }

}
