<?php

class Admin_CategoriaController extends App_Controller_Admin {

    public function initialize() {
        $this->model = new Model_Categoria();
        $this->view->pageTitle = 'Categoria';
        $this->view->pageTitleList = 'Listagem';
    }

    protected function indexAction() {

        $this->view->paginator = array();

        $page = $this->_getParam('page', 1);
        $per_page = $this->_getParam('per_page', 20);

        $select = $this->select;

        $this->view->paginator = $this->paginator($this->model->fetchAll($select), $per_page, $page);

        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }
    
    protected function beforeSave(&$row, $action) {
//        print_r($row);
//        exit;
    }

    protected function visualizarAction() {
        $this->_helper->layout->disableLayout();

        $id = $this->_request->getParam('id', 0);

        $rowSet = $this->model->find($id);
        $row = $rowSet->current();

        $this->view->row = $row;
    }

}

