<?php

class Admin_ResourceController extends App_Controller_Admin {

    public function initialize() {
        $this->model = new Model_Resource();
        $this->view->pageTitle = 'Resources';
        $this->view->pageTitleList = 'Listagem';
        $this->view->pageTitleNew = 'Novo';
        $this->view->pageTitleEdit = 'Editar';

        $modelResource = new Model_Resource();
        $_resources = $modelResource->fetchAll("id_parent IS NULL");

        $this->view->resourcesPai = $_resources;
    }

    protected function indexAction() {
        $page = $this->_getParam('page', 1);
        $pai = $this->_getParam('pai', 0);
        $per_page = $this->_getParam('per_page', 20);
        $select = $this->model->select()->from(array('r2' => 'resource'))
                        ->joinLeft(array('r1' => 'resource'), 'r2.id_parent = r1.id', array('parent' => 'r1.name'))
                        ->where('r2.status != ?', 'deletado')
                        ->order(array('r2.name ASC'));

        if($pai){

            $select->where('r2.id_parent = ?', $pai);
        }

        $rows = $this->model->fetchAll($select);

        $this->view->paginator = $this->paginator($rows, $per_page, $page);

        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }

    protected function viewAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->_getParam('id');
        if ($id) {
            $select = $this->model->select()->from(
                            array('r1' => 'resource'))->joinLeft(array('r2' => 'resource'), 'r1.id_parent = r2.id', array('parent' => 'r2.name'))->where('r1.id = ?', $id);

            $row = $this->model->fetchRow($select);
            $this->view->row = $row;
        }
    }

    protected function beforeSave(&$row, $action) {
        $dados = $this->getPost();
        $row->id_parent = $dados['id_parent'] == "null" ? NULL : $row->id_parent;
    }

    public function controllersAction() {
        // echo "teste";exit;
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);


        $dados = $this->_request->getPost();

        if ($dados) {
            $configuration = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
            // echo $dados['resource'];
            $pai = $this->model->find($dados['resource'])->current();
            // var_dump($pai->name);
            // exit;
            switch ($pai->name) {
                case 'admin':
                    $dir = $configuration->resources->frontController->adminModuleDirectory;
                    break;
                case 'default':

                    $dir = $configuration->resources->frontController->defaultModuleDirectory;
                    break;

                default:

                    break;
            }

           
            $select = $this->model->select()->where('status != ?','deletado');
            $result = $this->model->fetchAll($select)->toArray();
            $existente = array();
            foreach ($result as $key => $value) {
                $existente[] = $value['name'];        
            }
            
            $html = '<option value="">Selecione um Resource</option>';
            $html .= '<option value="index">index</option>';
            $exclude_list = array(".", "..");

            $controllerFiles = array_diff(scandir($dir), $exclude_list);

            foreach ($controllerFiles as $file) {
                // $controller = strtolower(substr($file, 0, strpos($file, "Controller")));
                $ajuste_um = substr($file, 0,strpos($file, "Controller"));
                $controller = strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-',$ajuste_um));

                if(!in_array($controller,$existente)){    
                    $html .= '<option value="' . $controller . '">' . $controller . '</option>';
                }
            }
            echo $html;
        }
    }

    public function camel2dashed($className) {
        return strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', $className));
    }

    public function actionsAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);


        $dados = $this->_request->getPost();

        if ($dados) {

            $configuration = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
            switch ($dados['module']) {
                case 'admin':
                    $ModuleDir = $configuration->resources->frontController->adminModuleDirectory;
                    break;
                case 'default':
                    $ModuleDir = $configuration->resources->frontController->defaultModuleDirectory;
                    break;
                default:
                    break;
            }
            $controllerDir = $ModuleDir . DIRECTORY_SEPARATOR . $dados['controller'];
            foreach (get_class_methods($controllerDir) as $action) {
//                 print_r($action);
// //                if (strstr($action, "Action") !== false) {
// //                    $actions[] = str_replace("Action", "", $action);
// //                }
            }

            $select = $this->model->select()->where('status != ?','deletado');
            $result = $this->model->fetchAll($select)->toArray();
            foreach ($result as $key => $value) {
                // var_dump($value);
            }


            $html = '<option value="">Selecione uma Action</option>';
            $exclude_list = array(".", "..");
            $controllerFiles = array_diff(scandir($dir), $exclude_list);

            foreach ($controllerFiles as $file) {

                $controller = strtolower(substr($file, 0, strpos($file, "Controller")));
                $html .= '<option value="' . $controller . '">' . $controller . '</option>';
            }
            // echo $html;
        }
    }

}