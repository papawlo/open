<?php

/**
 * Description of Dashboard
 *
 * @author qualitare
 */
class Admin_DashboardController extends App_Controller_Admin {

    public function initialize() {

        $this->view->pageTitle = 'DashBoard';
        $this->view->pageTitleList = 'Atualizações';
    }

    protected function afterInitialize() {
        
    }

    protected function indexAction() {
        $usersModel = new Model_Users();
        $page = 1;
        $per_page = 5;

        $rows_luvers = $usersModel->fetchAll("status='ativo'", "id DESC", $per_page);

        $this->view->paginator_luvers = $rows_luvers;
    }

    protected function numUsersAction() {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if (isset($_GET['start']) AND isset($_GET['end'])) {

            $start = $_GET['start'];
            $end = $_GET['end'];
            $data = array();

// Select the results with Idiorm

            $usersModel = new Model_Users();

            $select = $usersModel->select()
                    ->from('users', array("DATE_FORMAT(created_at,'%Y-%m-%d') as created_at", new Zend_Db_Expr('COUNT(*) as num_users')))
                    ->where('created_at >= ?', $start)
                    ->where('created_at <= ?', $end)
                    ->group('YEAR(created_at), MONTH(created_at), DAY(created_at)')
                    ->order('created_at');
//            print_r($select->__toString());
//            exit();


            $results = $usersModel->fetchAll($select)->toArray();

// Build a new array with the data
            $data = array();
            $num_users = 0;
            foreach ($results as $key => $value) {

                $data[$key]['label'] = $value['created_at'];
                $num_users += $value['num_users'];
                $data[$key]['value'] = $num_users;
            }

            echo json_encode($data);
        }
    }

    protected function numGenderAction() {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $usersModel = new Model_Users();

        $select = $usersModel->select()
                ->from('users', array("genero", new Zend_Db_Expr('COUNT(genero) as num_genero')))
                ->group('genero');

        $results = $usersModel->fetchAll($select)->toArray();

// Build a new array with the data
        $data = array();
        foreach ($results as $valor) {
            if ($valor['genero'] != '') {
                $data[$valor['genero']] = $valor['num_genero'];
            } else {
                $data['ni'] = $valor['num_genero'];
            }
        }

        echo json_encode($data);
    }

    protected function numGenderComAction() {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $relationshipModel = new Model_Relationship();

        $select = $relationshipModel->select()
                ->from('relationship', array("genero", new Zend_Db_Expr('COUNT(genero) as num_genero')))
                ->group('genero');

        $results = $relationshipModel->fetchAll($select)->toArray();

// Build a new array with the data
        $data = array();
        foreach ($results as $valor) {
            if ($valor['genero'] != '') {
                $data[$valor['genero']] = $valor['num_genero'];
            } else {
                $data['ni'] = $valor['num_genero'];
            }
        }

        echo json_encode($data);
    }

    protected function numUsersByAgeAction() {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $usersModel = new Model_Users();

        $select = $usersModel->select()
                ->from('users', array(new Zend_Db_Expr('YEAR(CURRENT_TIMESTAMP ) - YEAR( data_nascimento ) - ( RIGHT(CURRENT_TIMESTAMP , 5 ) < RIGHT( data_nascimento, 5 ) ) AS age')))
                ->where('data_nascimento IS NOT NULL')
                ->where("data_nascimento != '0000'")
                ->where("YEAR(data_nascimento ) < 2013")
                ->where("YEAR( data_nascimento ) != '0000'");

        $results = $usersModel->fetchAll($select)->toArray();
//        print_r($results);
//        exit();
// Build a new array with the data
        $data = array();

        $data['15-17'] = 0;
        $data['18-24'] = 0;
        $data['25-29'] = 0;
        $data['30-34'] = 0;
        $data['35-29'] = 0;

        foreach ($results as $valor) {

            switch ($valor['age']) {
                case ($valor['age'] >= 15 and $valor['age'] <= 17):
//                    echo '15-17';
                    $data['15-17'] += 1;
                    break;

                case ($valor['age'] >= 18 and $valor['age'] <= 24):
//                    echo '18-24';
                    $data['18-24'] += 1;
                    break;

                case ($valor['age'] >= 25 and $valor['age'] <= 29):
//                    echo '25-29';
                    $data['25-29'] += 1;
                    break;

                case ($valor['age'] >= 30 and $valor['age'] <= 34):
//                    echo '30-34';
                    $data['30-34'] += 1;
                    break;

                case ($valor['age'] >= 35 and $valor['age'] <= 39):
//                    echo '35-39';
                    $data['35-39'] += 1;
                    break;

                default:
                    $data['ni'] += 1;
                    break;
            }
           
            $ageData = array();
            foreach ($data as $key => $value) {
                $ageData[] = array(
                    'value' => $value,
                    'color' => '#' . $this->random_color(),
                    'label' => $key,
                    'labelColor' => 'black',
                    'labelFontSize' => '1em',
                    'labelAlign' => 'center'
                );
            }
        }

        echo json_encode($ageData);
    }

    protected function numUserByCountryAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersModel = new Model_Users();

        $select = $usersModel->select()
                ->from('users', array("pais", new Zend_Db_Expr('COUNT(pais) as num_pais')))
                ->group('pais');

        $results = $usersModel->fetchAll($select)->toArray();

// Build a new array with the data
        $data = array();
        $totalbrasil = 0;
        foreach ($results as $valor) {
            if ($valor ['pais'] == 'brasil' || $valor ['pais'] == 'Brasil' || $valor ['pais'] == 'Brazil') {
                $totalbrasil += $valor['num_pais'];
                $data['BR'] = $totalbrasil;
            } else if ($valor ['pais'] == '') {
                $data[$valor['pais']] = $valor['num_pais'];
            } else {
                $data['ni'] = $valor['num_pais'];
            }
        }
//        $gfata = 

        echo json_encode($data);
    }

    protected function random_color_part() {
        return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
    }

    protected function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

}

