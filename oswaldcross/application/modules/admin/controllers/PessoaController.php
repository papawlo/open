<?php

class Admin_PessoaController extends App_Controller_Admin {

    public function initialize() {
        $this->model = new Model_Pessoa();
        $this->view->pageTitle = 'Pessoa ';
        $this->view->pageTitleList = 'Listagem';

        $modelCategoria = new Model_Categoria();
        $_categorias = $modelCategoria->fetchAll("status = 'ativo'");

        $tipo = $this->_getParam('tipo', null);
        $this->view->tipo = $tipo;

        $this->view->categorias = $_categorias;

        $this->view->estados = array("AC" => "Acre", "AL" => "Alagoas", "AM" => "Amazonas", "AP" => "Amapá", "BA" => "Bahia", "CE" => "Ceará", "DF" => "Distrito Federal", "ES" => "Espírito Santo", "GO" => "Goiás", "MA" => "Maranhão", "MT" => "Mato Grosso", "MS" => "Mato Grosso do Sul", "MG" => "Minas Gerais", "PA" => "Pará", "PB" => "Paraíba", "PR" => "Paraná", "PE" => "Pernambuco", "PI" => "Piauí", "RJ" => "Rio de Janeiro", "RN" => "Rio Grande do Norte", "RO" => "Rondônia", "RS" => "Rio Grande do Sul", "RR" => "Roraima", "SC" => "Santa Catarina", "SE" => "Sergipe", "SP" => "São Paulo", "TO" => "Tocantins");
    }

    protected function indexAction() {
        $ajax = $this->isXmlHttpRequest();

        if ($ajax) {
            $this->_helper->layout->disableLayout();
        }

        $page = $this->_getParam('page', 1);
        $per_page = $this->_getParam('per_page', '1');
        $by = $this->_getParam('by', null);
        $order = $this->_getParam('order', null);
        $tipo = $this->_getParam('tipo', null);
        $categoria = $this->_getParam('categoria', null);
        $nome = $this->_getParam('nome', null);
        $params = array();

        $select = $this->select;

        if ($by && $order) {
            $select->order(array($by . ' ' . $order));
            $this->view->by = $by;
            $this->view->order = $order;
            $params["by"] = $by;
            $params["order"] = $order;
        }
        if ($categoria) {
            $select->where('categoria_id= ?', $categoria);
            $this->view->categoria = $categoria;
//            $this->params = array("categoria" => $categoria);
            $params["categoria"] = $categoria;
        }
        if ($tipo) {
            $select->where('tipo= ?', $tipo);
            $this->view->tipo = $tipo;
//            $this->params = array("tipo" => $tipo);
            $params['tipo'] = $tipo;
        }

        if (!empty($nome) && isset($nome)) {
            $select->where('nome LIKE ?', $nome . '%');
        }

        $this->view->search_params = $params;


        $this->view->paginator = $this->paginator($this->model->fetchAll($select), $per_page, $page);

        $this->view->messages = $this->_helper->flashMessenger->getMessages();
        if ($ajax) {
            $this->render('partial/pessoal-index', null, true);
        }
    }

    protected function visualizarAction() {
        $this->_helper->layout->disableLayout();

        $id = $this->_request->getParam('id', 0);

        $rowSet = $this->model->find($id);
        $row = $rowSet->current();

        $this->view->row = $row;
    }

    protected function beforeSave(&$row, $action) {
        $auth = Zend_Auth::getInstance();

        $info = $auth->getStorage()->read();
        if ($action == 'new') {
            $row->data_cadastro = date('Y-m-d H:i:s');
            $row->cadastrado_por = $info->id;
        }
        if ($action == 'edit') {
            $row->data_edicao = date('Y-m-d H:i:s');
            $row->editado_por = $info->id;
        }
    }

    public function exportXlsAction() {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);



        $request = $this->getRequest();
        if ($request->isPost()) {
            $por = $this->_getParam('by', null);
            $order = $this->_getParam('order', null);
            $tipo = $this->_getParam('tipo', null);
            $categoria = $this->_getParam('categoria', null);

            $select = $this->select;

            if ($por && $order) {
                $select->order(array($por . ' ' . $order));
                $this->view->por = $por;
                $this->view->order = $order;
            }

            if ($categoria) {
                $select->where('categoria_id= ?', $categoria);
                $this->view->categoria = $categoria;
            }
            if ($tipo) {
                $select->where('tipo= ?', $tipo);
                $this->view->tipo = $tipo;
            }

            $campos = $this->_getParam('campos', null);


            // Create new PHPExcel object
            $objPHPExcel = new PHPExcel();

            // Set properties
            $objPHPExcel->getProperties()->setCreator("Marcio Paulo");
            $objPHPExcel->getProperties()->setLastModifiedBy("Marcio Paulo");
            $objPHPExcel->getProperties()->setTitle("Lista de Pessoas para exportação");
            $objPHPExcel->getProperties()->setSubject("Lista de Pessoas Fundação Oswaldo Cruz");
//        $objPHPExcel->getProperties()->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.");
            // Add some data
            $data = $this->model->fetchAll($this->select);

            $objPHPExcel->setActiveSheetIndex(0);
            $column = 'A';
            $line = 1;

            //imprime o cabecalho
            foreach ($data AS $row) {
                if ($line == 1) {
                    foreach ($campos as $campo) {
                        //pega o nome das colunas
                        $objPHPExcel->getActiveSheet()->SetCellValue($column . $line, $this->model->getLabel($campo));
                        $column++;
                    }
                }
                $line++;
                break;
            }
            foreach ($data AS $row) {
                $column = 'A';
                $valor = '';
                foreach ($campos as $campo) {
                    if ($campo == 'cadastrado_por' || $campo == 'editado_por') {
                        if ($campo) {
                            $valor = Model_Pessoa::getUsuarioNomeById($row->$campo);
                        }
                    } elseif ($campo == 'categoria_id') {
                        $valor = Model_Pessoa::getCategoriaNomeById($row->$campo);
                    } else {
                        $valor = $row->$campo;
                    }
                    $objPHPExcel->getActiveSheet()->SetCellValue($column . $line, $this->mb_strtoupper_new($valor));
                    $column++;
                }
                $line++;
            }

            // Rename sheet
            $objPHPExcel->getActiveSheet()->setTitle('Pessoas');


            // Save Excel 2007 file
            $filename = "lista-" . date("m-d-Y") . ".xls";

            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $this->getResponse()->setRawHeader("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8")
                    ->setRawHeader("Content-Disposition: attachment; filename=$filename")
                    ->setRawHeader("Content-Transfer-Encoding: binary")
                    ->setRawHeader("Expires: 0")
                    ->setRawHeader("Cache-Control: must-revalidate, post-check=0, pre-check=0")
                    ->setRawHeader("Pragma: public")
                    ->sendResponse();
            $objWriter->save('php://output');
        }
    }

    function mb_strtoupper_new($str, $e = 'utf-8') {
        if (function_exists('mb_strtoupper')) {
            return mb_strtoupper($str, $e = 'utf-8');
        } else {
            foreach ($str as &$char) {
                $char = utf8_decode($char);
                $char = strtr(char, "abcdefghýijklmnopqrstuvwxyz" .
                        "\x9C\x9A\xE0\xE1\xE2\xE3" .
                        "\xE4\xE5\xE6\xE7\xE8\xE9" .
                        "\xEA\xEB\xEC\xED\xEE\xEF" .
                        "\xF0\xF1\xF2\xF3\xF4\xF5" .
                        "\xF6\xF8\xF9\xFA\xFB\xFC" .
                        "\xFE\xFF", "ABCDEFGHÝIJKLMNOPQRSTUVWXYZ" .
                        "\x8C\x8A\xC0\xC1\xC2\xC3\xC4" .
                        "\xC5\xC6\xC7\xC8\xC9\xCA\xCB" .
                        "\xCC\xCD\xCE\xCF\xD0\xD1\xD2" .
                        "\xD3\xD4\xD5\xD6\xD8\xD9\xDA" .
                        "\xDB\xDC\xDE\x9F");
                $char = utf8_encode($char);
            }
            return $str;
        }
    }

}

