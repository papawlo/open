<?php

class Admin_UsersController extends App_Controller_Admin {

    public function initialize() {
        $this->model = new Model_Users();
        $this->view->pageTitle = 'Luvrs';
        $this->view->pageTitleList = 'Listagem';
        $this->view->pageTitleNew = 'Novo';
        $this->view->pageTitleEdit = 'Editar';
    }

    protected function indexAction() {

        $page = $this->_getParam('page', 1);
        $per_page = $this->_getParam('per_page', 20);

        $rows = $this->model->fetchAll($this->select);

        $this->view->paginator = $this->paginator($rows, $per_page, $page);

        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }

    protected function visualizarAction() {
        $this->_helper->layout->disableLayout();

        $id = $this->_request->getParam('id', 0);

        $rowSet = $this->model->find($id);
        $row = $rowSet->current();

        $this->view->row = $row;
    }

    protected function beforeSave(&$row, $action) {
        $dados = $this->getPost();
        $row->data_nascimento = $dados['ano'] . '-' . $dados['mes'] . '-' . $dados['dia'];
    
        $cidadeEstadoPais = $this->getAddressByName($dados['endereco']);
        
        $row->logradouro = isset($dados['logradouro']) && !empty($dados['logradouro']) ? $dados['logradouro'] : $cidadeEstadoPais['logradouro'];
        $row->numero = isset($dados['numero']) && !empty($dados['numero']) ? $dados['numero'] : $cidadeEstadoPais['numero'];
        $row->bairro = isset($dados['bairro']) && !empty($dados['bairro']) ? $dados['bairro'] : $cidadeEstadoPais['bairro'];
        $row->cidade = isset($dados['cidade']) && !empty($dados['cidade']) ? $dados['cidade'] : $cidadeEstadoPais['cidade'];
        $row->estado = isset($dados['estado']) && !empty($dados['estado']) ? $dados['estado'] : $cidadeEstadoPais['estado'];
        $row->pais = isset($dados['pais']) && !empty($dados['pais']) ? $dados['pais'] : $cidadeEstadoPais['pais'];
        $row->cep = isset($dados['cep']) && !empty($dados['cep']) ? $dados['cep'] : $cidadeEstadoPais['cep'];
        $row->lng = isset($dados['lng']) && !empty($dados['lng']) ? $cidadeEstadoPais['lng'] : "";
        $row->lat = isset($dados['lat']) && !empty($dados['lat']) ? $cidadeEstadoPais['lat'] : "";

        if ($action == 'new') {
            $row->password = ($dados['password']) ? md5($dados['password']) : md5('123456');
            $row->created_at = date('Y-m-d H:i:s');
            $row->status = 'ativo';
        } else {
            $userAtual = $this->model->find($row->id)->current();
            $senhaAtual = $userAtual->password;

            $row->password = ($dados['password']) ? md5($dados['password']) : $senhaAtual;
        }
    }

//public function afterSave(&$row, $action) {
//        $request = $this->getRequest();
//        if ($request->isPost()) {
//            $dados = $this->getPost();
//            try {
//                $adapter = new Zend_File_Transfer_Adapter_Http();
//                $adapter->addValidator('Count', false, 1)
//                        ->addValidator('Size', false, array('max' => "2MB"))
//                        ->addValidator('Extension', false, 'jpg,png,gif')
//                        ->addValidator('IsImage', false);
//
//                $files = $adapter->getFileInfo();
//
//                foreach ($files as $fieldname => $fileinfo) {
//                    if (($adapter->isUploaded($fileinfo["name"])) && ($adapter->isValid($fileinfo['name']))) {
//                        $mediaUpload = new App_Helpers_Media($fileinfo);
//                        $dados['foto'] = $mediaUpload->sendFile($this->_request->getControllerName());
//                        if ($dados['foto']) {
//
////                            $modelMedia = new Model_Media();
////                            $modelMedia->removeImagem($row->old_id_media);
//                        }
//                    }
//                }
//            } catch (Exception $ex) {
//                $this->addFlash("error", $ex->getMessage());
//            }
    /*
      $modelAuthentication = new Model_Authentication();
      $modelEnderecos = new Model_Enderecos();

      if ($action == 'new') {
      $dados['id_usuario'] = $row->id_usuario;
      } else {
      $rowDadoUsuario = $this->model->getDados($row->id_usuario);
      }

      //parada do endereço #TENSO
      if (isset($dados['endereco']) && !empty($dados['endereco'])) {

      $enderecoRow = $modelEnderecos->find($dados['endereco']);
      } else {
      $enderecoRow = $modelEnderecos->createRow();

      $estadoModel = new Model_Estado ();
      $enderecoRow->uf = $estadoModel->find($dados['estado'])->current()->uf;

      $enderecoRow->cidade_id = $dados['cidade'];
      $enderecoRow->nomeslog = $dados['endereco_input'];
      $enderecoRow->nomeclog = $dados['logradouro'] . " " . $dados['endereco_input'];
      if (!$dados['bairro']) {

      $bairroModel = new Model_Bairro();
      $bairroRow = $bairroModel->createRow();
      $bairroRow->nome = $dados['bairro_input'];
      $bairroRow->uf = $estadoModel->find($dados['estado'])->current()->uf;
      $bairroRow->cidade = $dados['cidade'];
      $enderecoRow->bairro_id = $bairroRow->save();
      } else {

      $enderecoRow->bairro_id = $dados['bairro'];
      }

      $enderecoRow->cep = $dados['cep'];
      $enderecoRow->logradouro = $dados['logradouro'];
      $enderecoRow->uf_cod = $dados['estado'];
      $enderecoRow->logracompl = $dados['logradouro'] . " " . $dados['endereco_input'];

      $rowDadoUsuario->endereco = $enderecoRow->save();
      }
      $rowDadoUsuario->setFromArray($dados);
      $rowDadoUsuario->save();
      }
     * 
     */
//  }

    public function validateEmailAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if ($this->getRequest()->isXmlHttpRequest()) {
            if ($this->getRequest()->isPost()) {
                $newemail = $this->getRequest()->getPost('newemail', null);
                $user_id = $this->getRequest()->getPost('user_id', null);

                if ($newemail) {

                    $select = $this->model->select()->where('email = ?', $newemail)->where('id !=?', $user_id);

                    $result = $this->model->fetchAll($select);

                    $rowCount = count($result);

                    if ($rowCount > 0) {
                        echo 'invalid';
                    } else {
                        echo 'valid';
                    }
                } else {
                    echo 'valid';
                }
            }
        } else {
            echo 'você não tem permissão';
        }
    }

    public function getAddressAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $latitude = '48.283273';
        $longitude = '14.295041';
        $geocode = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $latitude . ',' . $longitude . '&sensor=false&language=pt-BR');

        $output = json_decode($geocode);

        for ($j = 0; $j < count($output->results[0]->address_components); $j++) {
            echo '<b>' . $output->results[0]->address_components[$j]->types[0] . ': </b>  ' . $output->results[0]->address_components[$j]->long_name . '<br/>';
        }
    }

// Get STATE from Google GeoData
    function getAddressByNameAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $address = $this->getRequest()->getPost('endereco', null);
        $address = str_replace(" ", "+", "$address");
        $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";
//        echo $url;exit;
        $result = file_get_contents("$url");
        $json = json_decode($result);
        print_r($json);
        exit;
        foreach ($json->results as $result) {
            foreach ($result->address_components as $addressPart) {
                if ((in_array('locality', $addressPart->types)) && (in_array('political', $addressPart->types)))
                    $city = $addressPart->long_name;
                else if ((in_array('administrative_area_level_1', $addressPart->types)) && (in_array('political', $addressPart->types)))
                    $state = $addressPart->long_name;
                else if ((in_array('country', $addressPart->types)) && (in_array('political', $addressPart->types)))
                    $country = $addressPart->long_name;
            }
        }

        if (($city != '') && ($state != '') && ($country != ''))
            $address = $city . ', ' . $state . ', ' . $country;
        else if (($city != '') && ($state != ''))
            $address = $city . ', ' . $state;
        else if (($state != '') && ($country != ''))
            $address = $state . ', ' . $country;
        else if ($country != '')
            $address = $country;

// return $address;
        echo "$country/$state/$city";
    }

// Get STATE from Google GeoData
    function getAddressByName($address) {
        $address = str_replace(" ", "+", "$address");
        $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";

        $result = file_get_contents("$url");
        $json = json_decode($result);
        $numero = $cep = $logradouro = $bairro = $city = $state = $country = "";
        foreach ($json->results as $result) {
            foreach ($result->address_components as $addressPart) {
                if (in_array('street_number', $addressPart->types))
                    $numero = $addressPart->long_name;
                else if (in_array('route', $addressPart->types))
                    $logradouro = $addressPart->long_name;
                else if ((in_array('sublocality', $addressPart->types)) && (in_array('political', $addressPart->types)))
                    $bairro = $addressPart->long_name;
                else if ((in_array('locality', $addressPart->types)) && (in_array('political', $addressPart->types)))
                    $city = $addressPart->long_name;
                else if ((in_array('administrative_area_level_1', $addressPart->types)) && (in_array('political', $addressPart->types)))
                    $state = $addressPart->long_name;
                else if ((in_array('country', $addressPart->types)) && (in_array('political', $addressPart->types)))
                    $country = $addressPart->short_name;
                else if (in_array('postal_code', $addressPart->types))
                    $cep = $addressPart->long_name;
            }


            $lat = $result->geometry->location->lat;

            $lng = $result->geometry->location->lng;
        }
        $endereco = array();

        $endereco['numero'] = $numero != '' ? $numero : "";
        $endereco['logradouro'] = $logradouro != '' ? $logradouro : "";
        $endereco['bairro'] = $bairro != '' ? $bairro : "";
        $endereco['cidade'] = $city != '' ? $city : "";
        $endereco['estado'] = $state != '' ? $state : "";
        $endereco['pais'] = $country != '' ? $country : "";
        $endereco['cep'] = $cep != '' ? $cep : "";
        $endereco['lat'] = $lat != '' ? $lat : "";
        $endereco['lng'] = $lng != '' ? $lng : "";


        return $endereco;
    }

}