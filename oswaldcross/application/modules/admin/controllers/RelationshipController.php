<?php

class Admin_RelationshipController extends App_Controller_Admin {

    public function initialize() {
        $this->model = new Model_Relationship();
        $this->view->pageTitle = 'Relacionamento';
        $this->view->pageTitleList = 'Listagem';
        $this->view->pageTitleNew = 'Novo';
        $this->view->pageTitleEdit = 'Editar';
    }

    protected function indexAction() {
        $user_id = $this->_getParam('user_id', false);
        $page = $this->_getParam('page', 1);
        $per_page = $this->_getParam('per_page', 5);
        $this->view->user_id = $user_id;
        $this->view->paginator = array();
        if ($user_id) {
            $userModel = new Model_Users();
            $usersRowset = $userModel->find($user_id);
            $userRow = $usersRowset->current();

            $relationshipRowset = $userRow->getRelationshipsAtivo();

            $this->view->paginator = $this->paginator($relationshipRowset, $per_page, $page);
        } else {

            $this->view->paginator = $this->paginator($this->model->fetchAll(), $per_page, $page);
        }


        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }

    protected function visualizarAction() {
        $this->_helper->layout->disableLayout();

        $id = $this->_request->getParam('id', 0);

        $rowSet = $this->model->find($id);
        $row = $rowSet->current();

        $this->view->row = $row;
    }

    protected function beforeSave(&$row, $action) {
        $dados = $this->getPost();
        $row->data_inicio = $dados['ano'] . '-' . $dados['mes'] . '-' . $dados['dia'];

        /*  notifications */
        $notification_email = $this->getRequest()->getPost('notification_email', 0);
        if ($notification_email) {
            $row->notification_email = 1;
        } else {
            $row->notification_email = 0;
        }

        $notification_facebook = $this->getRequest()->getPost('notification_facebook', 0);
        if ($notification_facebook) {
            $row->notification_facebook = 1;
        } else {
            $row->notification_facebook = 0;
        }

        $notification_gcalendar = $this->getRequest()->getPost('notification_gcalendar', 0);
        if ($notification_gcalendar) {
            $row->notification_gcalendar = 1;
        } else {
            $row->notification_gcalendar = 0;
        }
    }

    protected function afterSave(&$row, $action) {
        $this->params = array("user_id" => $row->users_id);
    }

    protected function afterDelete() {
        $user_id = $this->_getParam('user_id', false);
        $this->params = array("user_id" => $user_id);
    }

}

