<?php

class Admin_PessoaFisicaController extends App_Controller_Admin {

    public function initialize() {
        $this->model = new Model_PessoaFisica();
        $this->view->pageTitle = 'Pessoa Fisica';
        $this->view->pageTitleList = 'Listagem';
        
        $modelCategoria = new Model_Categoria();
        $_categorias= $modelCategoria->fetchAll("status = 'ativo'");
        
        $this->view->categorias = $_categorias;
        
        $this->view->estados = array("AC" => "Acre", "AL" => "Alagoas", "AM" => "Amazonas", "AP" => "Amapá", "BA" => "Bahia", "CE" => "Ceará", "DF" => "Distrito Federal", "ES" => "Espírito Santo", "GO" => "Goiás", "MA" => "Maranhão", "MT" => "Mato Grosso", "MS" => "Mato Grosso do Sul", "MG" => "Minas Gerais", "PA" => "Pará", "PB" => "Paraíba", "PR" => "Paraná", "PE" => "Pernambuco", "PI" => "Piauí", "RJ" => "Rio de Janeiro", "RN" => "Rio Grande do Norte", "RO" => "Rondônia", "RS" => "Rio Grande do Sul", "RR" => "Roraima", "SC" => "Santa Catarina", "SE" => "Sergipe", "SP" => "São Paulo", "TO" => "Tocantins");
    }

    protected function indexAction() {

        $page = $this->_getParam('page', 1);
        $per_page = $this->_getParam('per_page', 20);

        $this->view->paginator = $this->paginator($this->model->fetchAll($this->select), $per_page, $page);

        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }

    protected function visualizarAction() {
        $this->_helper->layout->disableLayout();

        $id = $this->_request->getParam('id', 0);

        $rowSet = $this->model->find($id);
        $row = $rowSet->current();

        $this->view->row = $row;
    }

    protected function beforeSave(&$row, $action) {
        $auth = Zend_Auth::getInstance();

        $info = $auth->getStorage()->read();
        if ($action == 'new') {
            $row->data_cadastro = date('Y-m-d H:i:s');
            $row->cadastrado_por = $info->id;
        }
        if ($action == 'edit') {
            $row->data_edicao = date('Y-m-d H:i:s');
            $row->editado_por = $info->id;
        }
    }

}

