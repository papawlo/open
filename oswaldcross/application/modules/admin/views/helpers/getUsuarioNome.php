<?php

class Admin_View_Helper_GetUsuarioNome extends Zend_View_Helper_Abstract {

    public function getUsuarioNome($id) {
        return Model_Pessoa::getUsuarioNomeById($id);
    }

}

?>
