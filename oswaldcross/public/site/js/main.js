var Namespace = {};

require([
    "jquery", 
    'libs/jquery.tools.min',
    'libs/jquery.form',
    'libs/jquery.maskedinput-1.3.min',
    'libs/jquery.validate.min',
//    'libs/messages_pt_BR',
    'libs/jquery.mCustomScrollbar.min',
    'libs/bootstrap-fileupload.min',
    'libs/jquery.maskMoney.min',
    //    'libs/jquery.selectbox-0.2.min',
    'libs/jquery.blockUI',
    /* Libs de terceiros: Jquery.tools, jquery.validate, etc. */
    /*'libs/lib',*/
    
    'modules/common',
    // Paginas especificas 
    'modules/buscas',
    'modules/cadastro',
    'modules/carrinho',
    'modules/contato',
    'modules/empresa',
    'modules/enderecoentrega',
    'modules/home',
    'modules/mapa',
    'modules/meusdados',
    'modules/missao',
    'modules/quemsomos',
    'modules/servicos',   
    ], function($) {
   
            
        Namespace.common.init();
        /* 
        * Adicionar o atributo rel em cada pagina, dessa forma voce pode ter um js (home.js) para cada pagina.
        */
        var pageName = $('body').attr('rel');
        //        var pageName = 'empresa';
//                                    alert(pageName);
        if( (pageName != 'undefined') && (pageName != '') ) {
            Namespace[pageName].init();
        }
      
        
    });