
Namespace.enderecoentrega = {
    init: function() {
//        require('libs/jquery.maskMoney.min');
        console.log('Iniciado: endereco-entrega');
        $(document).ready(function(){
            var bFlag, trocoDiv;
            bFlag = $('.rd_troco');
            trocoDiv = $('#troco');
            bFlag.click(function(){
                if($(this).attr("id") == "formapagamento1"){
                    trocoDiv.show();
                }else{
                    trocoDiv.hide();
                }
            })
            $(".money").maskMoney({
                symbol:'R$ ', 
                showSymbol:true, 
                thousands:'.', 
                decimal:',', 
                symbolStay: false,
                allowZero: true
            });
            
        });
        
    }
}