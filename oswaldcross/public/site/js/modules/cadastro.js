Namespace.cadastro = {
    init: function() {
        console.log('Iniciado: cadastro');
        $(function() {

            $("#estado").change(function(){
                Namespace.common.getCidades($(this).val(), null);                 
            });
            

            $(".bt_termouso").overlay({
                mask: '#000', 
                onLoad: function() {
                    $("#expose_termo").mCustomScrollbar("update");
                }
            });
            $(".bt_conheca_van").overlay({
                mask: '#000', 
                onLoad: function() {
                    $("#expose_cadastro_forn").mCustomScrollbar("update");
                }
            });
        
            $("#expose_cadastro_forn").mCustomScrollbar({
                scrollInertia:0           
            });
        
            $("#expose_termo").mCustomScrollbar({
                scrollInertia:0           
            });
            $(".bt_vantagens").overlay({
                mask: '#000'            
            });

        
            $('[placeholder]').focus(function() {
                var input = $(this);
                if (input.val() == input.attr('placeholder')) {
                    input.val('');
                    input.removeClass('placeholder');
                }
	
            }).blur(function() {
                var input = $(this);
                if ( (input.val() == '') || (input.val()) == input.attr('placeholder')) {
                    input.addClass('placeholder');
                    input.val(input.attr('placeholder'));
                }
            }).blur();
		   
            $('[placeholder]').parents('form').submit(function() {
                $(this).find('[placeholder]').each(function() {
                    var input = $(this);
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                });
            });

            $.validator.addMethod("checkmail", function(value) {
                var result;
                jQuery.ajax({
                    url: base_url+'/index/validate/',
                    type: 'POST',
                    async: false,
                    data: {
                        acao: "email", 
                        dado: value
                    },
                    success: function(data) {
                        result= data;
                        console.log(data);
                        if (data == 'invalid') {  
                            result =  false;
                        }  
                        if (data == 'valid') {
                            result =  true;  
                        }
                        
                    }            
                });   
                return result;
            });
            $.validator.addMethod("checkusuario", function(value) {
                var result;
                jQuery.ajax({
                    url: base_url+'/index/validate/',
                    type: 'POST',
                    async: false,
                    data: {
                        acao: "login", 
                        dado: value
                    },
                    success: function(data) {
                        result= data;
                        console.log(data);
                        if (data == 'invalid') {  
                            result =  false;
                        }  
                        if (data == 'valid') {
                            result =  true;  
                        }
                        
                    }            
                });   
                return result;
            });

            $('.f_cadastro').validate({
                // define regras para os campos
                onkeyup: false,
                onclick: false,
                rules: {
                    nome: {
                        required: true,
                        minlength: 2
                    },
                    razao_social: {
                        required: true
                    },
                    nome_fantasia: {
                        required: true
                    },
                    atividade_economica: {
                        required: true
                    },
                    cnpj: {
                        required: true
                    },
                    insc_municipal: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true,
                        checkmail:true
                    },
                    email_confirmacao: {
                        required: true,
                        email: true,
                        equalTo: "#email"
                    },
                    cidade: {
                        required: true
                    },
                    endereco: {
                        required: true
                    },
                    telefone: {
                        required: true
                    },
                    rg: {
                        required: true,
                        digits:true
                    },
                    expeditor: {
                        required: true
                    },
                    cep: {
                        required: true
                    },
                    cpf: {
                        required: true
                    },
                    usuario: {
                        required: true,
                        checkusuario: true
                    },
                    senha: {
                        required: true
                    },
                    senha_c: {
                        required: true,
                        equalTo: "#senha"
                    },
                    plano: {
                        required: true
                    },
                    termos: {
                        required: true
                    }
                },
                // define messages para cada campo
                messages: {
                    nome: " (Digite seu nome)",
                    razao_social: " (Digite a razão social)",
                    atividade_economica: " (Digite a atividade econômica)",
                    nome_fantasia: " (Digite o nome fantasia)",
                    responsavel: " (Digite o Responsável)",
                    endereco: " (Digite seu endereco)",
                    cep: " (Digite seu CEP)",
                    cnpj: " (Digite seu CNPJ)",
                    email: {
                        required:" (Digite seu e-mail)",  
                        checkmail:" (Esse email já foi cadastrado)"
                    },
                    email_confirmacao: " (Confirme seu e-mail)",
                    insc_municipal: " (Digite sua inscriçãoo municipal)",
                    telefone: " (Digite seu telefone)",
                    rg: " (Digite sua identidade)",
                    orgao_expedidor: " (Digite o orgão expeditor)",
                    cpf: " (Digite seu cpf)",
                    cidade: " (Digite sua cidade)",
                    usuario:{
                        required: " (Digite seu usuário)",
                        checkusuario:" (Esse usuário já existe)"
                    },
                    senha: " (Digite sua senha)",
                    senha_c: " (Confirme sua senha)",
                    plano: " (Escolha um plano)",
                    termos: " (Você deve concordar com os termos)"
	            
                },
                errorElement: 'em'	        
            });
        });
    }
}