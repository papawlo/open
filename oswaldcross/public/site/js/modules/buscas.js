Namespace.buscas = {
    init: function() {
        console.log('Iniciado: buscas');
        $("#slt-ordem").change(function(){            
            $('#frmordem').submit();
        })  ;    
        $(".btn-abre-busca-detalhada").click(function(){ 
            $('.busca_detalhada').slideDown();
        }) ;     
        $(".btn-fecha-busca-detalhada").click(function(){            
            $('.busca_detalhada').slideUp();
        })     ;
        
        $('.dest_busca').scrollable({
            easing: 'linear',
            circular:true,
            size: 3
        });
        
        $("#grupo").change(function(){
            
            jQuery.ajax({
                url: base_url+'/subgrupos',
                type: 'POST',
                data: {
                    grupo: $(this).val()
                },
                success: function(data, textStatus, xhr) {
                    $('.cmb_subgrupo').attr('disabled', false).html(data);
                }
            });
                            	  
        })
    }
}