Namespace.meusdados = {
    init: function() {
        console.log('Iniciado: meusdados');
   
        $("#estado").change(function(){
            Namespace.common.getCidades($(this).val(), null);                 
        });
           
        $(".bt_crop_foto").overlay({
            mask: '#000',
            closeOnClick:false
        });
        $("#cidade").change(function(){
            Namespace.common.getBairros($(this).val(), null); 
            $('select.cmb_bairro').attr('disabled', true).html('<option value="">Selecione uma Cidade</option>');           
        });

        $(".close").click(function(){
            $(this).parent().fadeOut(500).delay(2000).queue(function(){
                $(this).remove()
            })
            return false;
        })
        
        // Get accurate measurements from that.
        var sizeX = $("#width-original").val();
        var sizeY =  $("#height-original").val();

        var aspectRatioX = 60;
        var aspectRatioY = 60;
        var ratio = aspectRatioX / aspectRatioY;
        
        $('#cropbox-view').Jcrop({
            onChange: showPreview,
            onSelect: showPreview,
            aspectRatio: ratio,
            onRelease: hidePreview,
            boxWidth: 300,
            trueSize: [sizeX,sizeY]
        });
       
        
        var $preview = $('#preview');
        // Our simple event handler, called from onChange and onSelect
        // event handlers, as per the Jcrop invocation above
        function showPreview(coords)
        {
            if (parseInt(coords.w) > 0)
            {
                var rx = aspectRatioX / coords.w;
                var ry = aspectRatioY / coords.h;

                $preview.css({
                    width: Math.round(rx * sizeX) + 'px',
                    height: Math.round(ry * sizeY) + 'px',
                    marginLeft: '-' + Math.round(rx * coords.x) + 'px',
                    marginTop: '-' + Math.round(ry * coords.y) + 'px'
                }).show();
      
                jQuery('#x').val(coords.x);
                jQuery('#y').val(coords.y);
                jQuery('#x2').val(coords.x2);
                jQuery('#y2').val(coords.y2);
                jQuery('#w').val(coords.w);
                jQuery('#h').val(coords.h);
            }
        }

        function hidePreview()
        {
            $preview.stop().fadeOut('fast');
        }
        function checkCoords()
        {
            if (parseInt($('#w').val())) return true;
            alert('Please select a crop region then press submit.');
            return false;
        };
        $("#frm-cropimage").submit(function(event) {
            /* stop form from submitting normally */            
            event.preventDefault();
            $( "#result" ).html('<div align="center"><img src="'+base_url+'/public/assets/img/loader.gif"></div>');
            if(checkCoords()){              
                /* get some values from elements on the page: */
                var $form = $( this ),
                dados = $form.serialize(),
                url = $form.attr( 'action' );
 
                /* Send the data using post */
                $.post( url, dados , function(data){
                    if(data.tipo=='success'){               

                        $( "#result" ).empty().append('Salvo');
                        $( ".usuario_foto" ).attr('src', data.msg) //Set the source so it begins fetching
                        .each(function() {
                            //Cache fix for browsers that don't trigger .load()
                            if(this.complete) $(this).trigger('load');
                        });
                    }else{
                        $( "#result" ).empty().append('Erro');
                    }
                }, "json");
            }
        });
        
    },
    getBairros: function (cidade, bairro) {
        jQuery.ajax({
            url: base_url+'/index/bairros',
            type: 'POST',
            data: {
                cidade: cidade, 
                bairro: bairro
            },
            success: function(data, textStatus, xhr) {
                $('select.cmb_bairro').attr('disabled', false).html(data);
            }
        });
                                    	  
    }
}