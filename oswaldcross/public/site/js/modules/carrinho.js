Namespace.carrinho = {
    init: function() {        
        console.log('Iniciado: Empresa');
        $(document).ready(function(){
            
            //            $('.cmb_bairro').dropkick();
            $('.excluir').live('click', function(){
                var answer = confirm("Deseja mesmo excluir esse ítem?")
                if (answer){
                    var indice = $(this).attr('data-indice');    
                    Namespace.carrinho.delProduto(indice);        
                    return false;
                }
            })
            $('.atualizar').live('click', function(){
                var indice = $(this).attr('data-indice');
                var qtde = $("#qtde_produto"+indice).val();
                //                alert(idbairro);
                Namespace.carrinho.updProduto(indice, qtde);        
                return false;
            })
            
            $("#btn_calcula-frete").live('click', function(){
                var cep = $("#inp-cep-frete").val();   
                if(cep){
                    Namespace.carrinho.updCarrinho(cep);        
                }
                return false;
            })
	    
        })
    },
    delProduto:function(indice) {

        jQuery.ajax({
            url: base_url+'/udcarrinho',
            type: 'POST',
            data: {
                indice: indice, 
                opcao: 'excluir'
            },
            success: function(data, textStatus, xhr) {
                //                console.log(data);
                //                console.log(textStatus);
                //                console.log(xhr);
              
                $("#tbl-carrinho").html($(data).find("#tbl-carrinho"));   
                Namespace.common.getItensCarrinho();
               
            },
            beforeSend: function(){              
                Namespace.carrinho.showLoading();              
            },
            complete: function(){
                Namespace.carrinho.hideLoading();
                $('.cep').mask2('99999-999');
            }
        });
        return false;
    },
    updProduto:function(indice, qtde) {
        jQuery.ajax({
            url: base_url+'/udcarrinho',
            type: 'POST',
            data: {
                qtde: qtde, 
                indice: indice,               
                opcao: 'edit'
            },
            beforeSend: function(){              
                Namespace.carrinho.showLoading();              
            },
            success: function(data, textStatus, xhr) {    
                //                console.log(data);
                console.log(textStatus);
                //                console.log(xhr);  
                $("#tbl-carrinho").html($(data).find("#tbl-carrinho"));   
            },
            complete: function(){
                Namespace.carrinho.hideLoading();
                $('.cep').mask2('99999-999');
            }
        });
        return false;
    },
    updCarrinho : function(cep){
        jQuery.ajax({
            url: base_url+'/udcarrinho',
            type: 'POST',
            data: {
                cep: cep,
                opcao: 'cep'
            },
            beforeSend: function(){              
                Namespace.carrinho.showLoading();              
            },
            success: function(data, textStatus, xhr) {
                //                console.log(data);
                //                console.log(textStatus);
                //                console.log(xhr);  
                $("#tbl-carrinho").html($(data).find("#tbl-carrinho"));     
                             
            },
            complete: function(){
                Namespace.carrinho.hideLoading();
                $('.cep').mask2('99999-999');
            }
        });
        return false;
    },
    exibeResposta : function(tipo, resposta){
        $(".expose_produto_add").overlay({
            mask: '#000',
            load: true
        });       
    
    },
    showLoading : function(){
        $('#frm_carrinho').block({ 
            message: '<img src="'+base_url+'/site/img/loading_carrinho.gif"/>', 
            css: {
                border: '1px solid #a00'
            } 
        }); 
    },
    hideLoading : function(){
        $('#frm_carrinho').unblock(); 
    }
}