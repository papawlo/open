Namespace.common = {
    init: function() {
        console.log('Iniciado: common');
        this.getItensCarrinho();


       
        $(function() {
            var flagDetalhada = false;
            
            $("#btn_busca_detalhada").click(function(e){
                flagDetalhada = true ;
                $('#frmbuscas').validate();                
            })
           
            $('#frmbuscas').validate({
                rules: {
                    termo : 'required',
                    cep : {
                        required : true,
                        minlength:9,
                        maxlength:9
                    }                  
                },
                messages: {
                    termo : 'digite o termo buscado',                    
                    cep : 'insira o cep'                    
                },               

                errorPlacement: function(error, element) {
                    $(element).tooltip().show();
            
                },
                unhighlight: function(element, errorClass, validClass) {                       

                },
                submitHandler: function(form){

                    if(flagDetalhada){
                        var input = $("<input>").attr("type", "hidden").attr("name", "detalhada").val("1");
                        $('#frmbuscas').append($(input));
                    }
                    form.submit();
                } 
            });

       
            // $("#escolha-cidade").selectbox();
 
             
            if ( $('.cep').length ) {
                $('.cep').mask2('99999-999');
            }

            if ( $('.cpf').length ) {
                $('.cpf').mask2('999.999.999-99');
            }
            if ( $('.cnpj').length ) {
                $('.cnpj').mask2('99.999.999/9999-99');
            }

            if ( $('.fone').length ) {
                $('.fone').mask2('(99) 9999-9999');
            }

            if ( $('.bt-searchcep').length ) {
                $('.bt-searchcep').live('click', function() {
                    if($('#cep').val() == ""){
                        alert("digite o seu CEP");
                        $('#cep').focus();
                    }else{
                        Namespace.common.buscaCep( $('#cep').val() );
                    }
                });
            }
        
            $(".bt_politica").overlay({
                mask: '#000', 
                onLoad: function() {
                    $("#text_privacidade").mCustomScrollbar("update");
                }
            });
        
            $("#text_privacidade").mCustomScrollbar({
                scrollInertia:0           
            });
        
            $(".l_condicoes").overlay({
                mask: '#000', 
                onLoad: function() {
                    $("#text_uso").mCustomScrollbar("update");
                }
            });
        
            $("#text_uso").mCustomScrollbar({
                scrollInertia:0           
            });
        
            $(".bt_passos").overlay({
                mask: '#000',
                onBeforeLoad: function() {
                    var passo = $(this.getTrigger()).attr("id");
                    scrob.data("scrollable").seekTo(passo.substring(5,6)-1, 1000);
                }
            });
        
            $(".bt_esqueceu").overlay({
                mask: '#000'          
            });

       
            var scrob = $('.scroll_passos').scrollable({
                speed: 1000, 
                next:'.next_passos', 
                prev:'.prev_passos'
            }).navigator({
                navi:'.navi'
            });      
            function split( val ) {
                return val.split( /,\s*/ );
            }
            function extractLast( term ) {
                return split( term ).pop();
            }
            // js stuff
            $("#termo").bind( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                    $( this ).data( "autocomplete" ).menu.active ) {
                    event.preventDefault();
                }
            }).autocomplete({
                source: base_url + "/index/autocomplete",
                minLength: 3,
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( "" );
                    return false;
                }
            });
        
        });

    },
    exposeclose : function (expose, target) {
        target.hide();
        expose.expose();
        $.mask.close();
    },
    getCidades : function(estado, cidade ) {
        jQuery.ajax({
            url: base_url+'/index/cidades',
            type: 'POST',
            data: {
                estado: estado,
                cidade: cidade
            },
            success: function(data, textStatus, xhr) {

                $('#cidade').html(data);

            },
            error: function(data, textStatus, xhr){
//                console.log(data);
            //                console.log(textStatus);
            //                console.log(xhr);
            }
        });
    },
    getBairros : function(cidade, bairro) {
        jQuery.ajax({
            url: base_url+'/index/bairros',
            type: 'POST',
            data: {
                cidade: cidade, 
                bairro: bairro
            },
            success: function(data, textStatus, xhr) {
                    
                $("#bairro").val(data.id_bairro);
                $('#inp_bairro').val(data.nome);

            },
            error: function(data, textStatus, xhr){
//                console.log(data);
            //                console.log(textStatus);
            //                console.log(xhr);
            }
        });
    },
    getItensCarrinho : function() {
        jQuery.ajax({
            url: base_url+'/index/itenscarrinho',
            type: 'GET',            
            success: function(data, textStatus, xhr) {
                $('#qtde-itens-carrinho').html(data);
            }
        });
    },
    validateDadosUsuario : function( obj, acao, dado) {
        jQuery.ajax({
            url: base_url+'/index/validate/',
            type: 'POST',
            data: {
                acao: acao, 
                dado: dado
            },
            success: function(data, textStatus, xhr) {
                console.log(data);
                if (data == 'invalid') {                
                    obj.parent().append('<em for="'+acao+'" generated="true" class="error validate" style="display: block;"> (Esse '+acao+' já existe)</em>');
                } else {
                    obj.parent().find('em.validate').remove();
                }
            }
        });   
    },    
    buscaCep:function (cep) {

        jQuery.ajax({
            url: base_url+'/index/cep',
            type: 'GET',
            data: {
                cep: cep
            },
            beforeSend: function(){
                $("#loading").show();
                $("#nao-encontrado").hide();
            },
            complete: function(){
                $("#loading").hide();
            },
            success: function(data, textStatus, xhr) {
                if(data.resultado == 1){
                    $('#endereco_input').val(data.nomeslog).attr('disabled', true);                
                    $('#endereco').val(data.id_endereco);
                    $('#estado').val(data.uf_cod);

                    $('#logradouro').val(data.logradouro).attr('disabled', true);
                    $('#cidade').attr('disabled', true);
                    $('#estado').attr('disabled', true);
                    $('#inp_bairro').attr('disabled', true);
                    Namespace.common.getCidades( data.uf_cod, data.cidade_id);
                    Namespace.common.getBairros( data.cidade_id, data.bairro_id );
                }
                if(data.resultado==2){                
                    $('#logradouro').val(data.logradouro).attr('disabled', false);               
                    $('#endereco').val('').attr('disabled', false);
                    $('#inp_bairro').attr('disabled', false);
                    $('#endereco_input').attr('disabled', false);
                    $('#estado').val(data.estado_cod);

                    Namespace.common.getCidades( data.estado_cod, data.id_cidade);
                //                    Namespace.common.getBairros( data.id_cidade, data.bairro_id, url+'/bairros','#bairro',false );
                }
            
                if(data.resultado==3){
                    //                 errors = { personalid: "Please enter an ID to check" };
                    $('#endereco_input').val(data.nomeslog).attr('disabled', false);   
                    $('#inp_bairro').val(data.nomeslog).attr('disabled', false);   
                    $('#estado').attr('disabled', false); 
                    $('#cidade').attr('disabled', false); 
                    $('#numero').attr('disabled', false); 
                    $('#logradouro').attr('disabled', false); 
                    $('#complemento').attr('disabled', false); 
                    $('#nao-encontrado').show();
                }

            }        
        });
    },
    validaCep:function(cep){
        exp = /\d{2}\.\d{3}\-\d{3}/
        if(!exp.test(cep.value))
            alert('Numero de Cep Invalido!');		
    }
}
