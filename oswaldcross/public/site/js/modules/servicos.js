Namespace.servicos = {
    init: function() {
        console.log('Iniciado: servicos');
        $(document).ready(function(){
	        
            $(".l_serv h3:first").addClass("active");
            $(".l_serv .pane:not(:first)").hide();
	    
            $(".l_serv h3").click(function(){
                $(this).next(".pane").slideToggle("slow")
                .siblings(".pane:visible").slideUp("slow");
                $(this).toggleClass("active");
                $(this).siblings("h3").removeClass("active");
            });
	    
        });
    }
}