Namespace.busca_detalhada = {
    init: function() {
        console.log('Iniciado: busca detalhada');
                
        $("#grupo").change(function(){
            
            jQuery.ajax({
                url: base_url+'/subgrupos',
                type: 'POST',
                data: {grupo: $(this).val()},
                success: function(data, textStatus, xhr) {
                    $('.cmb_subgrupo').attr('disabled', false).html(data);
                }
            });
                            	  
        })
    }
}