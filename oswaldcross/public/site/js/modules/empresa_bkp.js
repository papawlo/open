Namespace.empresa = {
    init: function() {
        console.log('Iniciado: Empresa');
        $(document).ready(function(){
            
            $('.bt_selecionar.aberto').bind('click', function(){
                var qtde_max = parseInt($(this).attr("data-qtde-max")) ;
                var qtde = parseInt($(this).parent().children(".quant").children(".qtdeproduto").attr('value'));

                var id_produto = $(this).attr('data-produto');

                if(qtde <= qtde_max)    {                    
                    Namespace.empresa.addProduto(qtde,id_produto);        
                }else{
                    alert("Você ultrapassou a quantidade máximo de entrega do produto")
                }
                return false;
            }); 
            
            $("#produto_zera_carrinho").bind('click',function(){
                var id_produto = $("#produto_zera_carrinho").attr('data-produto');
                var qtde = $("#produto_zera_carrinho").attr('data-qtde');
                Namespace.empresa.addProduto(qtde, id_produto, 1);
                return false;
            });
                
            $("#expose_nao_logado").overlay({
                mask: '#000'
            });
            $("#expose_form_esvazia_carrinho").overlay({
                mask: '#000'
            });
            
            $("#frm-login").submit(function(event) {
                /* stop form from submitting normally */            
                event.preventDefault();
                if($("#p_login").val() !='' && $("#p_password").val() !=''){          
                    $( "#result" ).html('<div  align="center"><img src="'+base_url+'/public/assets/img/loader.gif"></div>');
                    /* get some values from elements on the page: */
                    var $form = $( this ),
                    dados = $form.serialize(),
                    url = $form.attr( 'action' );
 
                    /* Send the data using post */
                    $.post( url, dados , function(data){
                        if(data.tipo=='success'){                                          
                            location.reload(); 
                        }else{
                            $( "#result" ).empty().append('Usuário ou senha incorreta');
                        }
                    }, "json");
                }else{
                    alert("preencha os campos")
                }
            });
            
            var y_fixo = $("#carrinho_flutuante").offset().top;
            //            alert(y_fixo);
            $(window).scroll(function () {
                //                alert($(document).scrollTop());
                if($(document).scrollTop() > 356){
                    
                    $("#carrinho_flutuante").animate({
                        top: 20+$(document).scrollTop()+"px"
                    },{
                        duration:500,
                        queue:false
                    }
                    );
                }else{
                      
                    $("#carrinho_flutuante").animate({
                        top: y_fixo+"px"
                    },{
                        duration:500,
                        queue:false
                    }
                    );
                }
            });
        
        })
    },
    addProduto:function(qtde,produto, zera) {
        zera = zera ? zera : 0;
        jQuery.ajax({
            url: base_url+'/addproduto',
            type: 'POST',
            data: {
                idproduto: produto, 
                qtde: qtde,
                zera: zera
            },
            success: function(data, textStatus, xhr) {
                if (data == 'ok') {
                    Namespace.common.getItensCarrinho();
                    Namespace.empresa.exibeResposta(1,"Adicionado com sucesso",produto);
                } else if (data == 'je') {
                    Namespace.empresa.exibeResposta(2,"Produto já existe no carrinho",produto);
                } else if (data == 'nl') {
                    Namespace.empresa.exibeResposta(3,'Você deve estar logado',produto);
                } else if (data == 'lm') {
                    Namespace.empresa.exibeResposta(4,"Você ultrapassou a quantidade máximo de entrega do produto",produto);
                } else if (data == 'fc') {
                    Namespace.empresa.exibeResposta(5,"Estabelecimento fechado", produto);
                } else if (data == 'ed') {
                    Namespace.empresa.exposeNovoCarrinho("O pedido deve ser do mesmo estabelecimento. Você deseja adicionar o produto em seu carrinho perder os outros?", produto, qtde);
                }else {
                    Namespace.empresa.exibeResposta(0,"erro",produto);
                }
            
            }
        });
        return false;
    },
    exibeResposta : function(tipo, resposta, produto){
        
        switch(tipo){
            case 1:
                $("#res_"+produto).html('<img src="'+base_url+'/site/img/tick.png">');
                $("#expose_form_esvazia_carrinho").overlay().close(); 
                break;
            case 2:
                alert('2');
                $("#res_"+produto).html('<img src="'+base_url+'/site/img/tick.png">');
                $("#informacoes .text_mensagem p").text(resposta);
                $("#informacoes").overlay().load();
                break;
            case 3:
                $("#res_"+produto).html('<img src="'+base_url+'/site/img/no.png">');
                $("#expose_nao_logado").overlay().load();
                break;
            case 4:
                $("#res_"+produto).html('<img src="'+base_url+'/site/img/no.png">');
                $("#informacoes .text_mensagem p").text(resposta);
                $("#informacoes").overlay().load();
                break;
            case 5:
                $("#res_"+produto).html('<img src="'+base_url+'/site/img/no.png">');
                $("#informacoes .text_mensagem p").text(resposta);
                $("#informacoes").overlay().load();
                break;
            case 6:
                $("#res_"+produto).html('<img src="'+base_url+'/site/img/no.png">');
                $("#expose_form_esvazia_carrinho .text_mensagem p").text(resposta);
                $("#expose_form_esvazia_carrinho").overlay().load();
                break;
        }
    },    
    exposeNovoCarrinho : function(resposta, produto, qtde){
        $("#res_"+produto).html('<img src="'+base_url+'/site/img/no.png">');
        $("#expose_form_esvazia_carrinho .text_mensagem p").text(resposta);
        $("#produto_zera_carrinho").attr('data-produto',produto);
        $("#produto_zera_carrinho").attr('data-qtde',qtde);
        $("#expose_form_esvazia_carrinho").overlay().load(); 
    },    
    confirmaNovoCarrinho : function(produto, qtde, zera){        
        Namespace.empresa.addProduto(qtde,produto,zera); 
    }
}