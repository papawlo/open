Namespace.contato = {
	init: function() {
		console.log('Iniciado: contato');
		$('[placeholder]').focus(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
				input.removeClass('placeholder');
			}
		      
			if (input.attr('name') == 'telefone') {
				input.mask('(99) 9999-9999');
			}
		}).blur(function() {
			var input = $(this);
			if ( (input.val() == '') || (input.val()) == input.attr('placeholder')) {
				input.addClass('placeholder');
				input.val(input.attr('placeholder'));
			}
		}).blur();
		   
			$('[placeholder]').parents('form').submit(function() {
			$(this).find('[placeholder]').each(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
					input.val('');
				}
			});
		});

		$('.form_contato').validate({
	        rules: {
	            nome: {
	                required: true,
	                minlength: 2
	            },
	            email: {
	                required: true,
	                email: true
	            },
	            cidade: 'required',
	            mensagem: 'required'
	        },
	        messages: {
	            nome: " (Digite seu nome)",
	            email: " (Digite seu email)",
	            cidade: " (Digite sua cidade)",
	            mensagem: " (Digite sua mensagem)"
	        },
	        errorElement: 'em'	        
	    });
	}
}