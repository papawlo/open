
$(document).ready(function() {
    $('.btn').tooltip({
        container: 'body'
    });
    $('.ordenador').tooltip()
    
    $('.datepicker').datepicker({
        language: 'pt-BR', 
        format: 'dd/mm/yyyy'
    });

    $('.editor').wysihtml5({
        locale: "pt-BR"
    });

    $('.bt-launchEditor').bind('click', function(event) {
        event.preventDefault();
        return launchEditor('editableimage1', $(this).attr('rel'));
    });

    /* Commons */
    $("form").validate({
        meta: "validate",
        errorPlacement: function(error, element) {
            element.closest('.control-group').addClass('error');
            //error.insertAfter(element);
            element.closest('.controls').append(error);
        },
        success: function(label) {
            label.closest('.control-group').removeClass('error');
            label.closest('.control-group').find('span.help-inline').remove();
        },
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        errorElement: 'span',
        errorClass: 'help-inline'
    });

    $(".money").maskMoney({
        symbol: 'R$ ',
        showSymbol: true,
        thousands: '.',
        decimal: ',',
        symbolStay: false,
        allowZero: true
    });
    $('.cep').mask('99999-999');
    $('.cpf').mask('999.999.999-99');
    $('.cnpj').mask('99.999.999/9999-99');
    $('.fone').mask('(99) 9999-9999');
    $('.time').mask('99:99');



    if ($('.bt-delete').size()) {
        $('.bt-delete').bind('click', function(event) {
            event.preventDefault();
            var bt = $(this);

            $('#deleteConfirm').modal();
            $('#deleteConfirm').modal('show');

            $('#deleteConfirm').find('.bt-confirmar').live('click', function() {
                if (bt.attr('href')) {
                    location.href = bt.attr('href');
                } else {
                    return true;
                }

            });
        });
    }

    if ($('.confirmation').size()) {
        $('.confirmation').bind('click', function(event) {
            event.preventDefault();
            var bt = $(this);

            $('#modalConfirmation').modal();
            $('#modalConfirmation').modal('show');

            $('#modalConfirmation').find('.bt-confirmar').live('click', function() {
                //location.href = bt.attr('href');
                return true;
            });
        });
    }

    if ($('.bt-removeImage').size()) {
        $('.bt-removeImage').bind('click', function(event) {
            event.preventDefault();
            var bt = $(this);

            $('#deleteConfirm').modal();
            $('#deleteConfirm').modal('show');

            $('#deleteConfirm').find('.bt-confirmar').bind('click', function() {
                //location.href = bt.attr('href');
                bt.parents('.control-group').fadeOut(400, function() {
                    $(this).remove();
                    $('#deleteConfirm').modal('hide')
                });
            });
        })
    }

    if ($('.bt-view').size()) {
        $('.bt-view').bind('click', function(event) {
            event.preventDefault();
            var bt = $(this);
            var url, target;

            if (bt.attr('href')) {
                url = bt.attr('href');
                target = bt.attr('data-target')
            } else {
                return true;
            }
            $(target).html('<div align="center"><img src="' + base_url + '/public/assets/img/loader.gif"></div>');
            $(target).modal('show');
            $(target).load(url);

            return false;
        });
    }

});


function getCidadesAdmin(estado, cidade, url, target, bloqueia) {
    jQuery.ajax({
        url: url,
        type: 'POST',
        data: {
            estado: estado,
            cidade: cidade
        },
        success: function(data, textStatus, xhr) {
            $(target).attr("disabled", false);
            $(target).val("");
            $(target).html(data);
            if (bloqueia) {
                $(target).attr("readonly", true);
            } else {
                $(target).attr("readonly", false);
            }

        }
    });

}
function getBairrosAdmin(cidade, bairro, url, target, bloqueia) {
    jQuery.ajax({
        url: url,
        type: 'POST',
        data: {
            bairro: bairro,
            cidade: cidade
        },
        success: function(data, textStatus, xhr) {

            if (data) {
                $(target).html(data);
                someCampoBairro();
                if (bloqueia) {
                    $(target).attr("disabled", true)
                }
            } else {
                apareceCampoBairro();
            }
        }
    });

}


function validateDadosAdminas(obj, acao, dado) {

    jQuery.ajax({
        url: base_url + '/admin/cliente/validate/',
        type: 'POST',
        data: {
            acao: acao,
            dado: dado
        },
        success: function(data, textStatus, xhr) {
            //console.log(data);
            if (data == 'invalid') {
                obj.closest('.control-group').addClass('error');
                obj.parents('.controls').append('<span class="help-inline">Já existe um ' + acao + ' com esse valor.</span>');
            } else {
                obj.closest('.control-group').removeClass('error');
                obj.closest('.help-inline').remove();
            }
        }
    });


}

function validateEmail(obj, newemail, user_id) {

    jQuery.ajax({
        url: base_url + '/admin/users/validate-email/',
        type: 'POST',
        data: {
            newemail: newemail,
            user_id: user_id
        },
        success: function(data, textStatus, xhr) {
            console.log(data);
            if (data == 'invalid') {
                obj.parents('.control-group').addClass('error');
                obj.parent().parent().append('<span class="help-inline">O Email já está sendo usado. Escolha outro email</span>');
            } else {
                obj.parents('.control-group').removeClass('error');
                obj.parent().parent().find('.help-inline').remove();
            }
        }
    });
}
function procuraEndereco(obj, endereco) {

    jQuery.ajax({
        url: base_url + '/admin/users/get-address-by-name/',
        type: 'POST',
        data: {
            endereco: endereco
        },
        success: function(data, textStatus, xhr) {
            console.log(data);
            if (data == 'invalid') {
                obj.parents('.control-group').addClass('error');
                obj.parent().append('<span class="help-inline">Email não encontrado</span>');
            } else {
                obj.parents('.control-group').removeClass('error');
                obj.parent().find('.help-inline').remove();
            }
        }
    });

}
