
$(document).ready(function() {

    /* datepicker */
    //    $('.input-append.date').datepicker({
    //        format: 'dd/mm/yyyy'
    //    });


    // Gambi pra colocar o cursor de mouse como botão no Add- pode apagar depois
    $('#reminder h4').css('cursor', 'pointer');

    // plugin upload imagem
    $('.fileupload').fileupload({
        uploadtype: 'image'
    });

    // plugin upload imagem
    $('#reminder h4').click(function() {
        $('#loader').show();
        $.get(base_url + 'dates-pane/add-date-form', function(data) {
            $('#btn-add').after(data).slideDown(1200, function() {
                $('#loader').fadeOut('fast')
                $('#painel_data_new').fadeIn();
                bindValidation($("#painel_data_new"));
            });
        });

    });
    if ($("#endereco").length > 0) {
        var autocomplete = new google.maps.places.Autocomplete($("#endereco")[0], {});

        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            console.log(place.address_components);
        });
    }

    $(".frm-dates-pane").each(function() {
        bindValidation(this);
    });

    function bindValidation(formulario) {
        $(formulario).validate({
            rules: {
                titulo: {
                    required: true
                },
                com: {
                    required: true
                }
            },
            errorPlacement: function(error, element) {
                element.closest('.control-group').addClass('error');
                element.closest('.controls').append(error);
            },
            success: function(label) {
                label.closest('.control-group').removeClass('error');
                label.closest('.control-group').find('span.help-inline').remove();
            },
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            submitHandler: function(form) {
                var cod = $(form).find('input[name="id"]').val();
                var action = $(form).attr('action');

                var posting = $.post(action, $(form).serialize(), function(data) {
                    console.log(data); // John
                }, "json");
                /* Put the results in a div */
                posting.done(function(data) {
                    var alertTipo = "alert-" + data.tipo;
                    if (data.id) {
                        cod = data.id;
                        $('#result_new').attr('id', 'result_' + cod);
                        $('#remindes_new').attr('id', 'remindes_' + cod);
                        $('#result-delete_new').attr('id', 'result-delete_' + cod);
                    }
                    $('#result_' + cod + ' .alert').addClass(alertTipo).removeClass("hide");
                    $("." + alertTipo + " h4").html(data.tipo_msg)
                    $("." + alertTipo + " p").html(data.msg_info)
                });
            },
            errorElement: 'span',
            errorClass: 'help-inline'
        });
    }




    $("#settings .form-horizontal").validate({
        rules: {
            first_name: {
                required: true
            },
            email: {
                required: true,
                email: true
            }

        },
        errorPlacement: function(error, element) {
            element.closest('.control-group').addClass('error');
            element.closest('.controls').append(error);
        },
        success: function(label) {
            label.closest('.control-group').removeClass('error');
            label.closest('.control-group').find('span.help-inline').remove();
        },
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        submitHandler: function(form) {

            var posting = $.post(base_url + 'settings/edit', $(form).serialize(), function(data) {
                console.log(data);
            }, "json");
            /* Put the results in a div */
            posting.done(function(data) {
                var alertTipo = "alert-" + data.tipo;
                $('#result .alert').addClass(alertTipo).removeClass("hide");
                $("." + alertTipo + " h4").html(data.tipo_msg)
                $("." + alertTipo + " p").html(data.msg_info)
            });
        },
        errorElement: 'span',
        errorClass: 'help-inline'
    });

    /* mini upload imagem*/
    var ul = $('#upload ul');

    $('#drop a').click(function() {
        // Simulate a click on the file input button
        // to show the file browser dialog
        $(this).parent().find('input').click();
    });

    // Initialize the jQuery File Upload plugin
    $('#upload').fileupload({
        limitMultiFileUploads: 1,
        // This element will accept file drag/drop uploading
        dropZone: $('#drop'),
        // This function is called when a file is added to the queue;
        // either via the browse button, or via drag/drop:
        add: function(e, data) {

            var tpl = $('<li class="working"><input type="text" value="0" data-width="48" data-height="48"' +
                    ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');

            // Append the file name and file size
            tpl.find('p').text(data.files[0].name)
                    .append('<i>' + formatFileSize(data.files[0].size) + '</i>');

            // Add the HTML to the UL element
            data.context = tpl.appendTo(ul);

            // Initialize the knob plugin
            tpl.find('input').knob();

            // Listen for clicks on the cancel icon
            tpl.find('span').click(function() {

                if (tpl.hasClass('working')) {
                    jqXHR.abort();
                }

                tpl.fadeOut(function() {
                    tpl.remove();
                });

            });

            // Automatically upload the file once it is added to the queue
            var jqXHR = data.submit();
        },
        progress: function(e, data) {

            // Calculate the completion percentage of the upload
            var progress = parseInt(data.loaded / data.total * 100, 10);

            // Update the hidden input field and trigger a change
            // so that the jQuery knob plugin knows to update the dial
            data.context.find('input').val(progress).change();

            if (progress == 100) {
                data.context.removeClass('working');
            }
        },
        fail: function(e, data) {
            // Something has gone wrong!
            data.context.addClass('error');
        }

    });


    // Prevent the default action when a file is dropped on the window
    $(document).on('drop dragover', function(e) {
        e.preventDefault();
    });

    // Helper function that formats the file sizes
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    }
    /* fim do mini upload imagem*/

    str_to_append = '<div class="control-group reminder-list"  id="">' +
            '<label class="control-label" for="intervalo">Intervalo:</label>' +
            '<div class="controls controls-row">' +
            '<input type="text" class="span1" id="intervalo" name="intervalo[]" value="">   ' +
            '<input type="hidden" id="reminder_id" name="reminder_id[]" value="">' +
            '<select id="repeticao" name="repeticao[]" class="span1">' +
            '<option value="d">dia</option>' +
            '<option value="s">semana</option>' +
            '<option value="m">mês</option>' +
            '<option value="a">ano</option>' +
            '</select>' +
            '<select id="a_d" name="a_d[]" class="span1">' +
            '<option value="a">antes</option>' +
            '<option value="d">depois</option>' +
            '</select>' +
            '<div class="span1">' +
            '<a href="#" rel="" class="btn bt-delete" data-reminder-id="" data-relationship-id="" role="button" data-toggle="modal" data-target="#deleteConfirm"  style="margin-left: 10px"><i class="icon-trash"></i> </button>' +
            '</div>' +
            '</div>' +
            '</div>';

    $('.btn-add-reminder').live('click', function(event) {
        event.preventDefault();
        var bt = $(this);
        var newRemind = bt.parent().parent().prev('.reminder-list:last');
        newRemind.after(str_to_append);
        return false;

    });

    if ($('.bt-delete-reminder').size()) {
        $('.bt-delete-reminder').bind('click', function(event) {
            event.preventDefault();
            var bt = $(this);

            $('#deleteConfirm').find('.bt-confirmar').live('click', function(event) {
                event.preventDefault();
                var url = bt.attr('rel');
                var reminderId = bt.attr('data-reminder-id');
                var relationshipId = bt.attr('data-relationship-id');
                if (url) {
                    var posting = $.post(url, {
                        id: reminderId
                    }, function(data) {
                        console.log(data); // John
                    }, "json");
                    /* Put the results in a div */
                    posting.done(function(data) {
                        var alertTipo = "alert-" + data.tipo;
                        $('#result-delete_' + relationshipId + ' .alert').addClass(alertTipo).removeClass("hide");
                        $("." + alertTipo + " h4").html(data.tipo_msg);
                        $("." + alertTipo + " p").html(data.msg_info);
                        $("#remindes_" + reminderId).fadeOut("slow");
                        $('#deleteConfirm').modal('hide');

                    });
                } else {
                    return true;
                }

            });
        });
    }

    $('.btn-delete-dates-pane').bind('click', function(event) {
        event.preventDefault();
        var bt = $(this);
        var url = bt.attr('rel');
        var datesPaneId = bt.attr('data-dates-pane-id');

        $('#deleteConfirm').find('.bt-confirmar').live('click', function(event) {
            event.preventDefault();

            if (url && datesPaneId) {
                var posting = $.post(url, {
                    id: datesPaneId
                }, function(data) {
                    console.log(data); // John
                }, "json");
                /* Put the results in a div */
                posting.done(function(data) {
                    var alertTipo = "alert-" + data.tipo;
                    $('#result-delete .alert').addClass(alertTipo).removeClass("hide");
                    $("." + alertTipo + " h4").html(data.tipo_msg);
                    $("." + alertTipo + " p").html(data.msg_info);
                    $('#deleteConfirm').modal('hide');

                    $('#painel_data_' + datesPaneId).slideDown(1200, function() {
                        $('#painel_data_' + datesPaneId).fadeOut('slow');
                    });
                });
            } else {
                alert('fu');
                return true;
            }

        });
    });


    //autocomplete do nome parceiro(a)
    $('.parceiro').typeahead([
        {
            name: 'parceiros',
            prefetch:  base_url + 'dates-pane/com-autocomplete/'
        }
    ]);



});



function editDataRelationship() {

    jQuery.ajax({
        url: base_url + '/user/relatioship/edit/',
        type: 'POST',
        success: function(data, textStatus, xhr) {
            console.log(data);

        }
    });

}

