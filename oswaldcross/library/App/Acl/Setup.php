<?php

class App_Acl_Setup {

    protected $_acl;
    protected $_roles;
    protected $_permissions;
    protected $_resources;
    protected $_db;

    public function __construct() {
        $this->_acl = new Zend_Acl();
        $this->_initialize();
    }

    protected function _initialize() {
        //$this->_setupVariables();
        $this->_setupRoles();
        $this->_setupResources();
        $this->_setupPrivileges();
        $this->_saveAcl();
    }

    protected function _setupVariables() {
        $configuration = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
        $this->_db = Zend_Db::factory($configuration->resources->db);

        $roles = $this->_db->fetchAll($this->_db->select()->from('role'));

        $select_resources = $this->_db->select()->from(array('r1' => 'resource'))
                ->joinLeft(array('r2' => 'resource'), 'r1.id_parent = r2.id_resource', array('parent' => 'r2.name'))
                ->order('r1.id_parent');

        $resources = $this->_db->fetchAll($select_resources);

        $select_permissions = $this->_db->select()
                ->from(array('p' => 'permission'))
                ->joinLeft(array('r' => 'resource'), 'p.id_resource = r.id_resource', array('resource' => 'r.name'))
                ->joinLeft(array('r2' => 'resource'), 'r.id_parent = r2.id_resource', array('resource_pai' => 'r2.name'))
                ->join(array('ro' => 'role'), 'p.id_role = ro.id_role', array('role' => 'ro.name'))
                ->order('resource');


        /* $select_permissions = $this->_db->select()
          ->from(array('p'=>'permission'))
          ->join(array('r'=>'resource'),'p.id_resource = r.id_resource',array('resource'=>'r.name'))
          ->join(array('r2'=>'resource'),'r.id_parent = r2.id_resource',array('resource_pai'=>'r2.name'))
          ->join(array('ro'=>'role'),'p.id_role = ro.id_role',array('role'=>'ro.name'))
          ->order('resource'); */

        $permissions = $this->_db->fetchAll($select_permissions);

        $this->_roles = $roles;
        $this->_resources = $resources;
        $this->_permissions = $permissions;
    }

    protected function _setupRoles() {
        $this->_acl->addRole(new Zend_Acl_Role('superadmin'));
        $this->_acl->addRole(new Zend_Acl_Role('admin'));
        $this->_acl->addRole(new Zend_Acl_Role('secretaria'));
        $this->_acl->addRole(new Zend_Acl_Role('guest'));
        $this->_acl->addRole(new Zend_Acl_Role('user'));

//        foreach ($this->_roles as $role) {
//            $this->_acl->addRole(new Zend_Acl_Role($role['name']));
//        }
    }

    protected function _setupResources() {
//        foreach ($this->_resources as $key => $value) {
//
//            if ($value['parent'] == null) {
//                // $this->_acl->addResource(new Zend_Acl_Resource($value['name']));
//            } else {
//                // $this->_acl->addResource(new Zend_Acl_Resource($value['parent'].':'.$value['name']), $value['parent']);        
//            }
//        }

        /* DEFAULT */
        $this->_acl->addResource(new Zend_Acl_Resource('default'));
        $this->_acl->addResource(new Zend_Acl_Resource('default:index'), 'default');
        $this->_acl->addResource(new Zend_Acl_Resource('default:auth'), 'default');
        $this->_acl->addResource(new Zend_Acl_Resource('default:error'), 'default');
        /* USER */
        $this->_acl->addResource(new Zend_Acl_Resource('user'));
        $this->_acl->addResource(new Zend_Acl_Resource('user:index'), 'default');
        $this->_acl->addResource(new Zend_Acl_Resource('user:auth'), 'default');
        $this->_acl->addResource(new Zend_Acl_Resource('user:error'), 'default');

        /* ADMIN */
        $this->_acl->addResource(new Zend_Acl_Resource('admin'));
        $this->_acl->addResource(new Zend_Acl_Resource('admin:auth'), 'admin');
        $this->_acl->addResource(new Zend_Acl_Resource('admin:index'), 'admin');
        $this->_acl->addResource(new Zend_Acl_Resource('admin:usuario'), 'admin');
        $this->_acl->addResource(new Zend_Acl_Resource('admin:users'), 'admin');
        $this->_acl->addResource(new Zend_Acl_Resource('admin:pessoa-fisica'), 'admin');
        $this->_acl->addResource(new Zend_Acl_Resource('admin:pessoa-juridica'), 'admin');
        $this->_acl->addResource(new Zend_Acl_Resource('admin:pessoa'), 'admin');
        $this->_acl->addResource(new Zend_Acl_Resource('admin:categoria'), 'admin');
        $this->_acl->addResource(new Zend_Acl_Resource('admin:contacts'), 'admin');
        $this->_acl->addResource(new Zend_Acl_Resource('admin:relationship'), 'admin');
        $this->_acl->addResource(new Zend_Acl_Resource('admin:reminder'), 'admin');
        $this->_acl->addResource(new Zend_Acl_Resource('admin:notification'), 'admin');
        $this->_acl->addResource(new Zend_Acl_Resource('admin:dashboard'), 'admin');
        $this->_acl->addResource(new Zend_Acl_Resource('admin:configuracoes'), 'admin');

        $this->_acl->addResource(new Zend_Acl_Resource('admin:role'), 'admin');
        $this->_acl->addResource(new Zend_Acl_Resource('admin:resource'), 'admin');
        $this->_acl->addResource(new Zend_Acl_Resource('admin:permission'), 'admin');

        $this->_acl->addResource(new Zend_Acl_Resource('admin:advertiser'), 'admin');
        $this->_acl->addResource(new Zend_Acl_Resource('admin:campaign'), 'admin');


        $this->_acl->addResource(new Zend_Acl_Resource('admin:media'), 'admin');
    }

    protected function _setupPrivileges() {

        //COMENTADO PARA O ACL DINAMICO
        // foreach ($this->_permissions as $key => $value) {
        //     if($value['resource_pai']){
        //         $this->_acl->allow($value['role'],$value['resource_pai'].':'.$value['resource'],array($value['permission']));
        //     }else{
        //         $this->_acl->allow($value['role'],$value['resource'],array($value['permission']));
        //     }
        //     foreach ($this->_roles as $key => $value) {
        //         $this->_acl->allow($value['name'],'admin:auth',array('index','logout','login'));
        //     }
        // }   




        $this->_acl->allow('guest', 'default:index', array('index'))
                ->allow('guest', 'default:auth', array('index', 'login'))
                ->allow('guest', 'default:error', array('error', 'forbidden'));



        $this->_acl->allow('superadmin', 'admin:index', array('index'))
                ->allow('superadmin', 'admin:dashboard', array('index', 'num-users', 'num-gender', 'num-gender-com', 'num-user-by-country', 'num-users-by-age', 'num-users-by-age'))
                ->allow('superadmin', 'admin:auth', array('index', 'login', 'logout'))
                ->allow('superadmin', 'admin:usuario', array('index', 'new', 'edit', 'delete', 'status', 'cidades', 'validate'))
                ->allow('admin', 'admin:usuario', array('index', 'new', 'edit', 'delete', 'status', 'validate'))
                ->allow('superadmin', 'admin:users', array('index', 'new', 'edit', 'delete', 'visualizar', 'status', 'validate-email', 'get-address-by-name'))
                ->allow('superadmin', 'admin:pessoa-fisica', array('index', 'new', 'edit', 'delete', 'visualizar', 'status'))
                ->allow('superadmin', 'admin:pessoa-juridica', array('index', 'new', 'edit', 'delete', 'visualizar', 'status'))
                ->allow(array('superadmin','admin'), 'admin:pessoa', array('index', 'new', 'edit', 'delete', 'visualizar', 'status', 'export-xls'))
//                ->allow('admin', 'admin:pessoa', array('index', 'new', 'edit', 'delete', 'visualizar', 'status', 'export-xls'))
                ->allow(array('superadmin','admin'), 'admin:categoria', array('index', 'new', 'edit', 'delete', 'visualizar', 'status'))
                ->allow('superadmin', 'admin:relationship', array('index', 'new', 'edit', 'delete', 'visualizar', 'status'))
                ->allow('superadmin', 'admin:reminder', array('index', 'edit', 'delete', 'visualizar', 'status'))
                ->allow('superadmin', 'admin:notification', array('index', 'edit', 'delete', 'visualizar', 'status'))
                ->allow('superadmin', 'admin:configuracoes', array('index'))
                ->allow('superadmin', 'admin:role', array('index', 'new', 'edit', 'delete', 'status', 'visualizar'))
                ->allow('superadmin', 'admin:resource', array('index', 'new', 'edit', 'delete', 'status', 'visualizar', 'controllers', 'actions'))
                ->allow('superadmin', 'admin:permission', array('index', 'new', 'edit', 'delete', 'status', 'visualizar', 'controllers', 'actions'))
                ->allow('superadmin', 'admin:advertiser', array('index', 'new', 'edit', 'visualizar', 'delete', 'status'))
                ->allow('superadmin', 'admin:campaign', array('index', 'new', 'edit', 'visualizar', 'delete', 'status'))
                ->allow('superadmin', 'admin:media', array('image-view', 'crop-image'));
    }

    protected function _saveAcl() {
        $registry = Zend_Registry::getInstance();
        $registry->set('acl', $this->_acl);
    }

}
