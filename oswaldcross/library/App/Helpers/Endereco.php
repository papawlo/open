<?php

class App_Helpers_Endereco {

    static public  function getLogradouroCompleto($id) {
        $enderecoModel = new Model_Enderecos();

        $enderecoRow = $enderecoModel->find($id)->current();
        return $enderecoRow->nomeclog;
    }

}

?>
