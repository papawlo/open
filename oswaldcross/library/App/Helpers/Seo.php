<?php

class App_Helpers_Seo extends Zend_View_Helper_Abstract {

    public function seo() {

        $concatTitle = function() {
                    $title = null;
                    if (null !== $this->view->title) {
                        $title = $this->view->title . ' - ';
                    }
                    $section = null;
                    if (null !== $this->view->section) {
                        $section = $this->view->section . ' - ';
                    }
                    return $title . $section . Zend_Registry::get('config')->resources->view->title;
                };

        if (null !== $this->view->description) {
            $description = $this->view->description;
        } else {
            $description = Zend_Registry::get('config')->resources->view->meta->name->Description;
        }
        if (null !== $this->view->keywords) {
            $keywords = $this->view->keywords;
        } else {
            $keywords = Zend_Registry::get('config')->resources->view->meta->name->Keywords;
        }
        if (null !== $this->view->image_src) {
            $image_src = $this->view->image_src;
        } else {
            $image_src = Zend_Registry::get('config')->resources->view->meta->name->image_src;
        }
        if (null !== $this->view->robots) {
            $robots = $this->view->robots;
        } else {
            $robots = Zend_Registry::get('config')->resources->view->meta->name->Robots;
        }

        $html = '<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="' . substr(Zend_Registry::get('config')->resources->locale->default, 0, stripos(Zend_Registry::get('config')->resources->locale->default, "_")) . '"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="' . substr(Zend_Registry::get('config')->resources->locale->default, 0, stripos(Zend_Registry::get('config')->resources->locale->default, "_")) . '"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="' . substr(Zend_Registry::get('config')->resources->locale->default, 0, stripos(Zend_Registry::get('config')->resources->locale->default, "_")) . '"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js" lang="' . substr(Zend_Registry::get('config')->resources->locale->default, 0, stripos(Zend_Registry::get('config')->resources->locale->default, "_")) . '"> <!--<![endif]-->
    <head>
        <meta charset="' . Zend_Registry::get('config')->resources->view->charset . '">
        <meta http-equiv="X-UA-Compatible" content="' . Zend_Registry::get('config')->resources->view->charset . '">';
//        $html = '<html lang="' . substr(Zend_Registry::get('config')->resources->view->meta->name->locale, 0, stripos(Zend_Registry::get('config')->resources->view->meta->name->locale, "_")) . '" xmlns="http://www.w3.org/1999/xhtml" itemscope itemtype="http://schema.org/Blog" xmlns:fb="http://ogp.me/ns/fb#">' . PHP_EOL;
        $html .= '' . PHP_EOL;
        $html .= '<title>' . $concatTitle() . '</title>' . PHP_EOL;
//        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=' . Zend_Registry::get('config')->resources->view->meta->name->charset . '" />' . PHP_EOL;
        $html .= '<meta name="description" content="' . $description . '" />' . PHP_EOL;
        $html .= '<meta name="keywords" content="' . $keywords . '" />' . PHP_EOL;
        $html .= '<meta name="robots" content="' . $robots . '" />' . PHP_EOL;
//        $html .= '<meta content="' . $concatTitle() . '" />' . PHP_EOL;
//        $html .= '<meta content="' . Zend_Registry::get('config')->resources->view->title . '" />' . PHP_EOL;
//        $html .= '<meta content="' . $this->view->getAbsolutePath() . $image_src . '" />' . PHP_EOL;
//        $html .= '<meta content="article" />' . PHP_EOL;
//        $html .= '<meta content="' . $description . '" />' . PHP_EOL;
//        $html .= '<meta content="' . $this->view->getAbsolutePath() . $_SERVER['REQUEST_URI'] . '" />' . PHP_EOL;
//        $html .= '<link href="' . $this->view->getAbsolutePath() . $image_src . '" rel="image_src" />' . PHP_EOL;
        $canonical = '';

# Remove the last & and anything after...
# Should be close to what I'm saying it does (lol)
# (It's my bed time)

        if (strpos($_SERVER['REQUEST_URI'], '?') !== FALSE) {
            $find_last_amp = strrpos($_SERVER['REQUEST_URI'], '?');
            $find_canonical = '';
            $find_canonical = substr($_SERVER['REQUEST_URI'], 0, $find_last_amp);
            $canonical = $find_canonical;
        }

# If there's not an & keep the whole thing...
        else {
            $canonical = $_SERVER['REQUEST_URI'];
        }
        $html .= '<link href="' . $this->view->getAbsolutePath() . $canonical . '" rel="canonical" />' . PHP_EOL;
        $html .= '<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">' . PHP_EOL;

        return $html;
    }

}