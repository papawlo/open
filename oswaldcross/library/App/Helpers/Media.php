<?php

class App_Helpers_Media {

//    public $_barra = "/"; //linux
    public $_barra = "\\"; //windows
    private $_publicDir = '/public';
    private $_fileName = '';
    private $_fileType = '';
    private $_fileTmpName = '';
    private $_fileNewName = '';
    private $_path = '';
    private $_destinationFolder = '';
    private $_originFolder = '';

    public function __construct($file = '') {

        $this->_destinationFolder = realpath(APPLICATION_PATH . '/../public/uploads/');

        if ($file) {
            $this->_fileName = $file["name"];
            $this->_fileType = $file["type"];
            $this->_fileTmpName = $file["tmp_name"];

            $this->generateNewUploadFolder();

            $this->_fileNewName = $this->sanitize_file_name($this->_fileName);
        }
    }

    public function getThumbnail($media, $controller, $slug) {
        $frontController = Zend_Controller_Front::getInstance();

        $config = new Zend_Config_Ini(APPLICATION_PATH . "/modules/user/configs/images.ini");

        if (isset($media->filename) && isset($config->$controller->$slug->path) && isset($slug)) {
            if (file_exists(PUBLIC_PATH . $config->$controller->$slug->path . $slug . '-' . $media->filename))
                return $frontController->getBaseUrl() . $this->_publicDir . $config->$controller->$slug->path . $slug . '-' . $media->filename;
        }
        return false;
    }

    public function removeImagem($media) {

        if ($media) {
            $config = new Zend_Config_Ini(APPLICATION_PATH . "\modules\user\configs\images.ini");

            $sizes = explode(',', $config->user->sizes);
            foreach ($sizes as $size) {
                $imageCompletePath = $this->_destinationFolder . $config->user->$size->path . $size . '-' . $media->filename;
                unlink($imageCompletePath);
            }

            unlink($this->_destinationFolder . '/' . $media->path . '/' . $media->filename);
        }

        return true;
    }

    public function sendFile($controller) {
        $newFile = $this->_destinationFolder . '/' . $this->_fileNewName;


        $result = move_uploaded_file($this->_fileTmpName, $newFile);

        if ($result === true) {
            $this->generateThumbnails($controller);

            try {

                $_media = new Model_Media();

                $row = $_media->createRow();
                $row->filename = $this->_fileNewName;
                $row->filetype = $this->_fileType;
                $row->path = $this->_path;
                $row->url = str_replace('/home/jobsqual/public_html', 'http://jobs.qualitare.com', $newFile);
                $row->data_cadastro = date('Y-m-d H:i:s');

                return $row->save();
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }
    }

    private function generateNewUploadFolder() {
        $ano = date('Y');
        $mes = date('m');

        $uploadDir = $this->_destinationFolder . '/' . $ano;

        if (!file_exists($uploadDir)) {
            mkdir($uploadDir, 0777);
            chmod($uploadDir, 0777);
        }

        $uploadDir .= '/' . $mes;

        if (!file_exists($uploadDir)) {
            mkdir($uploadDir, 0777);
            chmod($uploadDir, 0777);
        }

        $this->_path = $ano . '/' . $mes;

        $this->_destinationFolder = $uploadDir;
    }

    private function generateThumbnails($controller) {
        $config = new Zend_Config_Ini(APPLICATION_PATH . "\modules\user\configs\images.ini");

        $sizes = explode(',', $config->$controller->sizes);
        $uploadedFile = $this->_destinationFolder . '/' . $this->_fileNewName;
        $uploadedFile = str_replace('\\', "/", $uploadedFile);

        $destino = realpath(APPLICATION_PATH . '/../public');
        $this->_destinationFolder = str_replace('\\', "/", $destino);

        if (file_exists($uploadedFile)) {
            $imageObj = new Zend_Image($uploadedFile, new App_Image_Driver_Gd());

            foreach ($sizes as $size) {
                $imageResized = $this->_destinationFolder . $config->$controller->$size->path . $size . '-' . $this->_fileNewName;
                $imageResized = str_replace('\\', "/", $imageResized);
                $transform = new Zend_Image_Transform($imageObj);
                if (($size != "big") && ($size != "medium") && ($size != "small")) {
                    $new_image = $transform->fitOut($config->$controller->$size->width, $config->$controller->$size->height)->center()->crop($config->$controller->$size->width, $config->$controller->$size->height);
                } else {
                    $new_image = $transform->fitIn($config->$controller->$size->width, $config->$controller->$size->height);
                }
                $new_image->save($imageResized);
            }
        }
    }

    public function customCrop($media, $controller, $slug, $top, $left, $crop_area_width, $crop_area_height) {
        // get the config file
        $config = new Zend_Config_Ini(APPLICATION_PATH . "/modules/user/configs/images.ini");

        $width = $config->$controller->$slug->width;
        $height = $config->$controller->$slug->height;

        $this->_destinationFolder = realpath(PUBLIC_PATH . $config->$controller->$slug->path);

//        $image_file = str_replace('http://jobs.qualitare.com/mandeme1/', realpath(PUBLIC_PATH), $media->url);
//        $image_file = str_replace('\\', "/", $image_file);

        $image_file = PUBLIC_PATH . "/uploads/" . $media->path . $this->_barra . $media->filename;
        $imageObj = new Zend_Image($image_file, new App_Image_Driver_Gd());

        $transform = new Zend_Image_Transform($imageObj);

        $new_image_cropada = $transform
                ->top($top)
                ->left($left)
                ->crop($crop_area_width, $crop_area_height)
                ->resize($width, $height);

        $imageResized = $this->_destinationFolder . '\\' . $config->$controller->$slug->slug . '-' . $media->filename;
//        $imageResized = str_replace('/', "\\", $imageResized);
        $imageResized = str_replace('\\', "/", $imageResized);


        if ($new_image_cropada->save($imageResized)) {
            $imageResized_file = str_replace(PUBLIC_PATH, 'http://jobs.qualitare.com/mandeme1/public/', $imageResized);

            $imageResized_url = str_replace('\\', "/", $imageResized_file);

            return array('tipo' => 'success', 'msg' => $imageResized_url . "?lastmod=" . time());
        }
        return array('tipo' => 'error', 'msg' => "não salvou");
    }

    /**
     * Function: sanitize
     * Returns a sanitized string, typically for URLs.
     *
     * Parameters:
     *     $string - The string to sanitize.
     *     $force_lowercase - Force the string to lowercase?
     *     $anal - If set to *true*, will remove all non-alphanumeric characters.
     */
    function sanitize_file_name($string, $force_lowercase = true, $anal = false) {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
            "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
            "â€”", "â€“", ",", "<", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = $this->replace_accents($clean);
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean;
        return ($force_lowercase) ?
                (function_exists('mb_strtolower')) ?
                        mb_strtolower($clean, 'UTF-8') :
                        strtolower($clean)  :
                $clean;
    }

    public function getDimensions($media, $controller = null, $slug = null) {
        $image_file = "";

        if ($media && $controller && $slug) {

            $config = new Zend_Config_Ini(APPLICATION_PATH . "/modules/user/configs/images.ini");

            $image_file = realpath(PUBLIC_PATH) . $config->$controller->$slug->path . $slug . '-' . $media->filename;

            $image_file = str_replace('\\', "/", $image_file);
        } else {

//            $image_file = str_replace('\\', "/", $image_file);
            $image_file = PUBLIC_PATH . "/uploads/" . $media->path . $this->_barra . $media->filename;
        }

        $imageObj = new Zend_Image($image_file, new Zend_Image_Driver_Gd);

        return array(
            "width" => $imageObj->getWidth(),
            "height" => $imageObj->getHeight()
        );
    }

    public function __toString() {
        $retorno = $this->_fileName . '<br />';
        $retorno .= $this->_fileType . '<br />';
        $retorno .= $this->_fileTmpName . '<br />';
        $retorno .= $this->_fileNewName . '<br />';
        $retorno .= $this->_destinationFolder;

        return $retorno;
    }

    public function replace_accents($var) { //replace for accents catalan spanish and more
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
        $var = str_replace($a, $b, $var);
        return $var;
    }

    public static function Unaccent($string) {
        if (extension_loaded('intl') === true) {
            $string = Normalizer::normalize($string, Normalizer::FORM_KD);
        }

        if (strpos($string = htmlentities($string, ENT_QUOTES, 'UTF-8'), '&') !== false) {
            $string = html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|caron|cedil|circ|grave|lig|orn|ring|slash|tilde|uml);~i', '$1', $string), ENT_QUOTES, 'UTF-8');
        }

        return $string;
    }

}

?>
