<?php

/**
 * @author slovacus
 * @todo Terminar
 */
/* https://github.com/luismayta/Zrt/blob/master/Service/LastVisited.php */
class App_Helpers_LastVisited {

    /**
     * Example use:
     * App_Helpers_LastVisited::saveThis($this->_request->getRequestUri());
     */
    public static function saveThis() {
        $url = Zend_Controller_Front::getInstance()->getRequest();
        $lastPg = new Zend_Session_Namespace('history');

        if ($url->getActionName() != 'login' && $url->getActionName() != "itenscarrinho")
            $lastPg->last = $url->getRequestUri();
    }

    /**
     * I typically use redirect:
     * $this->_redirect(App_Helpers_LastVisited::getLastVisited());
     */
    public static function getLastVisited() {
        $lastPg = new Zend_Session_Namespace('history');
        if (!empty($lastPg->last)) {
            $path = $lastPg->last;
//            $lastPg->unsetAll();
            return $path;
        }

        return '';
    }

}

?>