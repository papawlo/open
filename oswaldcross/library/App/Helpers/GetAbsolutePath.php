<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GetApsolutePath
 *
 * @author qualitare
 */
Class App_Helpers_GetAbsolutePath extends Zend_View_Helper_Abstract {

    public function getAbsolutePath() {
        $server = Zend_Controller_Front::getInstance()->getRequest()->getServer();
        $protocol = (!empty($server['HTTPS']) && $server['HTTPS'] !== 'off' || $server['SERVER_PORT'] == 443) ? "https" : "http";
        $exp = explode("/", $protocol);
        return strtolower(trim(array_shift($exp))) . '://' . $server['HTTP_HOST'];
    }

}
