<?php

class App_Db_Table_Abstract extends Zend_Db_Table_Abstract {

    /**
     * Filtra os itens do array de dados, verificando se as
     * chaves correspondem a campos da tabela
     *
     * @param array $data Array de dados
     * @return array Array de dados filtrados
     * */
    protected function _filterData(array $data) {
        $cols = $this->info('cols');
        $filteredData = array();
        if ($data) {
            foreach ($data as $key => $value) {
                if (in_array($key, $cols)) {
                    $filteredData[$key] = $value;
                }
            }
        }
        return $filteredData;
    }

    public function insert(array $data) {
        $data = $this->_filterData($data);
        return parent::insert($data);
    }

    public function update(array $data, $where) {
        $data = $this->_filterData($data);
        return parent::update($data, $where);
    }

    /**
     * Gera restricoes SQL de acordo com a(s) chave(s) primária(s)
     * @param  String|array $pkey chave(s) primária(s)
     *                            [nome_do_campo => valor]
     * @return String|array SQL where
     * */
    protected function _generateRestrictionsFromPrimaryKeys($pkey) {
        $where = array();
        if (is_array($this->_primary)) {
            foreach ($this->_primary as $key) {
                if (is_array($pkey)) {
                    $where[] = $this->getAdapter()
                            ->quoteInto($this->_name . '.' . $key . ' = ?', $pkey[$key]);
                } else {
                    $where = $this->getAdapter()
                            ->quoteInto($this->_name . '.' . $key . ' = ?', $pkey);
                }
            }
        } else {
            $where = $this->getAdapter()
                    ->quoteInto($this->_name . '.' . $this->_primary . ' = ?', $pkey);
        }
        return $where;
    }

    /**
     * Update de um registro único, baseado na chave primária
     * @param array $data Dados a serem atualizados
     * @param string|array $pkey Valor da chave primária
     * */
    public function updateRow(array $data, $pkey) {
        $where = $this->_generateRestrictionsFromPrimaryKeys($pkey);
        return $this->update($data, $where);
    }

    /**
     * Delete de um registro único, baseado na chave primária
     * @param string|array $pkey Valor da chave primária
     * */
    public function deleteRow($pkey) {
        $where = $this->_generateRestrictionsFromPrimaryKeys($pkey);
        return $this->delete($where);
    }

    public function count($where = null) {
        $select = $this->select();

        if ($where !== null) {
            $this->_where($select, $where);
        }

        $select->from($this->_name, array('count' => 'COUNT(*)'));

        $count = $this->getAdapter()->fetchRow($select);
        return intval($count['count']);
    }

    /**
     * Gera automaticamente um Zend_Db_Table_Select fazendo join nas tabelas
     * baseado nas informações do atributo $_referenceMap
     *
     * @param array $columns Lista de campos a serem retornados
     *  do banco de dados (usado para ganho de performance).
     *  Se for  null, todos as colunos serão retornados.
     * @param array $referenceTables Lista de "rules" das tabelas a serem
     *  acrescentadas na query (usado para ganho de performance)
     *  Se null, todas as tabelas serão acrescentadas na query.
     * return Zend_Db_Table_Select
     */
    public function selectWithJoins(array $columns = null, array $referenceTables = null) {
        $select = $this->select();

        // Verifica se há restrições nos campos a serem acrescentados
        $retrieveColumns = '*';
        if ($columns) {
            $tableCols = $this->info('cols');
            $retrieveColumns = array_intersect($tableCols, $columns);
        }
        $select->from($this->_name, $retrieveColumns);

        // Verifica se há tabelas a serem acrescentadas com join na query
        // baseado no $_referenceMap
        if (count($this->_referenceMap) > 0) {
            $select->setIntegrityCheck(false);
            if (!isset($tableCols)) {
                $tableCols = $this->info('cols');
            }
            foreach ($this->_referenceMap as $rule => $rm) {
                // Verifica se há restrições nas tabelas a serem
                // acrescentadas
                if (null === $referenceTables ||
                        in_array($rule, $referenceTables)) {
                    $refTableClass = new $rm['refTableClass'];

                    // Campos das tabelas a serem acrescentadas
                    // são renomeadas
                    // para "NomeDaRule_NomeDoCampo"
                    $refTableColumns = array();
                    foreach ($refTableClass->info('cols') as $col) {
                        $colAlias = "{$rule}_{$col}";
                        // Verifica se há restrições nos campos a
                        // serem acrescentados
                        if (!$columns || in_array($colAlias, $columns)) {
                            $refTableColumns[$colAlias] = $col;
                        }
                    }

                    $usedAliases = array($this->_name);

                    // Se não há campos a serem acrescentados,
                    // a tabela não será acrescentada
                    if (count($refTableColumns)) {
                        $refTableName = $refTableClass->info(self::NAME);

                        // Se refTable for igual a outra tabela, muda os aliases
                        $refTableAlias = $refTableName;
                        $i = 2;
                        while (in_array($refTableAlias, $usedAliases)) {
                            $refTableAlias = $refTableName . '_' . $i++;
                        }
                        $usedAliases[] = $refTableAlias;

                        if (is_string($rm['refColumns'])) {
                            // Se a chave primária é única
                            $joinOnString =
                                    "{$refTableAlias}.{$rm['refColumns']}"
                                    . " = {$this->_name}.{$rm['columns']}";
                        } else if (is_array($rm['refColumns'])) {
                            // Se a chave primária é concatenada
                            $joinOnArray = array();
                            foreach ($rm['refColumns'] as $key => $rc) {
                                $joinOnArray[] = "{$refTableAlias}.{$rc} = "
                                        . "{$this->_name}.{$rm['columns'][$k]}";
                            }
                            $joinOnString = implode(' AND ', $joinOnArray);
                        }

                        $select->joinLeft(
                                $refTableName, $joinOnString, $refTableColumns
                        );
                    }
                }
            }
        }

        return $select;
    }

    /**
     * Faz uma busca no banco de dados com joins nas tabelas relacionadas
     * baseado no atributo $_referenceMap
     *
     * @param string|array $where  OPTIONAL An SQL WHERE clause.
     * @param string|array $order  OPTIONAL An SQL ORDER BY clause.
     * @param array    $columns OPTIONAL Campos a serem retornados
     * @param array    $referenceTables OPTIONAL Tabelas a serem acrescentadas
     * @param mixed    $fetchMode OPTIONAL Formato do resultado da busca
     * @param int      $count OPTIONAL limite de itens para retorno 
     * @param int      $offset OPTIONAL offset de itens para retorno
     * @param bool     $paginate OPTIONAL Retorna o adpatador para ser manuseado
     * @return mixed     Depende do valor de $fetchMode.
     */
    public function fetchWithJoins($where = null, $order = null, array $columns = null, array $referenceTables = null, $fetchMode = Zend_Db::FETCH_OBJ, $count = null, $offset = null, $paginate = false) {
        $select = $this->selectWithJoins($columns, $referenceTables);

        if ($where !== null) {
            $this->_where($select, $where);
        }

        if ($order) {
            $select->order($order);
        }

        if ($count) {
            $select->limit($count);
        }

        if ($offset) {
            $select->limit($count, $offset);
        }
        return ($paginate) ? $select : $this->getAdapter()->fetchAll($select, null, $fetchMode);
//        return $this->getAdapter()->fetchAll($select, null, $fetchMode);
    }

    /**
     * Faz uma busca usando a chave primária no banco de dados com joins
     * nas tabelas relacionadas baseado no atributo $_referenceMap
     *
     * @param string|array $pkey  Valor da chave primária
     * @param array    $columns OPTIONAL Campos a serem retornados
     * @param array    $referenceTables OPTIONAL Tabelas a serem acrescentadas
     * @param mixed    $fetchMode OPTIONAL Formato do resultado da busca
     * @return mixed     Depende do valor de $fetchMode.
     */
    public function findWithJoins($pkey, array $columns = null, array $referenceTables = null, $fetchMode = Zend_Db::FETCH_OBJ) {
        // Ver posts anteriores para entender este método 
        $where = $this->_generateRestrictionsFromPrimaryKeys($pkey);

        $result = $this->fetchWithJoins($where, null, $columns, $referenceTables, $fetchMode);
        return count($result) == 1 ? current($result) : $result;
    }

}

?>
