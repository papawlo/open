<?php

Class Config {

    private static $_config = null;

    CONST DIRECTORY_SEPARATOR = "/";

    /**
     * @example Globals::getConfig('engine')->layout->layout;
     * @example Globals::getConfig('engine', 'production')->layout;
     * @param string $filename
     * @param string $section
     * @return array|string|boolean
     */
    public static function getConfig($filename = null, $section = null) {
        self::$_config = new Zend_Config_Ini(
                        APPLICATION_PATH . "/" . 'configs' . "/" . $filename . '.ini', $section
        );
        return self::$_config;
    }

}